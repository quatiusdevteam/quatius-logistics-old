var lastBannerComponentWidth = 0;

$(document).ready(function(){
    lastBannerComponentWidth = $(window).width();
    $( window ).resize( function(){
        if ($(this).width() != lastBannerComponentWidth) {
            lastBannerComponentWidth = $(this).width();
            changeBannerFontSize(lastBannerComponentWidth)
        }
    });
    changeBannerFontSize(lastBannerComponentWidth);

});

function changeBannerFontSize(curWidth){
    var selectBanners = $(".component.banner-slideshow .full-banner-slider,.component.banner-slideshow .col-xs-8");
    if (curWidth < 768){

        $.each(selectBanners.find('.full_text span'),function(){
            if ($(this).hasClass('font-scale')) return;

            var size = parseInt($(this).css('font-size'));

            var newScale = size/10;
            if (size > 16){
                $(this).attr('data-font-size',$(this).css('font-size'))
                    .css('font-size',newScale+"vw")
                    .addClass('font-scale');
            }
        });
    }else{
        $.each(selectBanners.find('.full_text span.font-scale'),function(){
            $(this).css('font-size',$(this).data('font-size'));
            $(this).removeClass('font-scale');
        });
    }
}

function setSlickCarousel(jItm, settings){

    if (jItm.hasClass('slick-initialized'))
    {
        jItm.slick('unslick')
    }
    else
    {
        if(settings !== undefined){
//             dots: false,
//             speed: 300,
//             slidesToShow: 1,
//             slidesToScroll: 1,
//             autoplay: true,
//             infinite: true,
//             autoplaySpeed: 2000,
//             responsive: [
//                 {
//                     breakpoint: setPixels,
//                     settings: {
//                         slidesToShow: 1,
//                         slidesToScroll: 1
//                     }
//                 }
//             ]
            jItm.slick(settings);
        }
        else{
            jItm.slick();
        }

        jItm.on('init', function(event, slick){
            //console.log("slick");
        });

        
    }
}

function slickMobile(jItm, shows, setPixels){
    setPixels = setPixels === undefined?768:setPixels;
    
    shows = shows === undefined?1:shows;

    var setting = {
            dots: false,
            speed: 300,
            slidesToShow: shows,
            slidesToScroll: 1,
            autoplay: true,
            infinite: true,
            autoplaySpeed: 2000,
            responsive: [
                {
                    breakpoint: setPixels,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        };

    setSlickCarousel(jItm, setting);
}

function autoplayVideoSlide(itm){
    $(itm).on('beforeChange', function(event, slick, currentSlide, nextSlide){
        
        var curItm = $(this).find('.banner-item[data-slick-index="'+currentSlide+'"]');
        var nextItm = $(this).find('.banner-item[data-slick-index="'+nextSlide+'"]');
        
        $(this).slick('slickPlay');
        var vidCur = curItm.find("video");
        if (vidCur.length) {
            vidCur.get(0).pause();
        }
        var vidNext = nextItm.find("video");
        
        if (vidNext.length) {
            $(this).slick('slickPause');
            vidNext.get(0).currentTime = 0.0;
            if (vidNext.get(0).hasAttribute('data-autoplay')){
                if (vidNext.get(0).hasAttribute('mute')){
                    vidNext.get(0).muted = true;
                }
                
                vidNext.get(0).play().catch(function() {
                    console.log("activeVid play mute required");
                    setTimeout(function(){
                        vidNext.parents('.slick-slider').slick('slickPlay');
                    }, 500);
                });
            }
        }
    });
    $(itm).bind("ended", function() {
        $(this).parents('.slick-slider').slick('slickNext');
    });

    $(itm).find("video").bind("timeupdate", function() {
        if (this.currentTime > 1 && this.currentTime + 1  > this.duration)
            $(this).parents('.slick-slider').slick('slickNext');
    });
}

// function slickCarousel(jItm)
// {
//     if (jItm.hasClass('slick-initialized'))
//     {
//         jItm.slick('unslick')
//     }
//     else
//     {
        
//         jItm.on('init', function(event, slick){
//             //console.log("slick");
//         });

//     jItm.slick({
//         dots: false,
//         speed: 300,
//         slidesToShow: 4,
//         slidesToScroll: 1,
//         autoplay: true,
//         infinite: true,
//         autoplaySpeed: 2000,
//         responsive: [
//         {
//             breakpoint: 1200,
//             settings: {
//                 slidesToShow: 4,
//                 slidesToScroll: 1,
//             }
//             },
//             {
//             breakpoint: 992,
//             settings: {
//                 slidesToShow: 4,
//                 slidesToScroll: 1,
//             }
//             },
//             {
//             breakpoint: 768,
//             settings: {
//                 slidesToShow: 3,
//                 slidesToScroll: 1
//             }
//             },
//             {
//             breakpoint: 580,
//             settings: {
//                 slidesToShow: 2,
//                 slidesToScroll: 1
//             }
//             }
//             ,
//             {
//             breakpoint: 420,
//             settings: {
//                 slidesToShow: 2,
//                 slidesToScroll: 1
//             }
//             }
//         ]
//         });
//     }
// }