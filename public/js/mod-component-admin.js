function initComp() {
  setMediaTrigger();

  $("button.comp-remove").popover({
    html: true,
    content:
      '<div class="text-center"><button type="button" class="btn btn-xs btn-warning" onclick="jQuery(this).parents(\'.edit-component\').find(\'.delete-placement\').val(1); jQuery(this).parents(\'.edit-component-area\').hide()">Yes</button></div>',
    title: "Remove this component?",
    trigger: "focus",
    placement: "left"
  });

  $(".code-editor:visible").each(function() {
    CodeMirror.fromTextArea(this, {
      lineNumbers: true,
      matchBrackets: true,
      htmlMode: true,
      mode: "application/x-httpd-php",
      indentUnit: 4,
      indentWithTabs: true,
      theme: "eclipse"
    }).on("blur", (cm, evt) => {
      cm.save();
    });
  });

  $(".json-editor:visible").each(function() {
    CodeMirror.fromTextArea(this, {
      lineNumbers: true,
      matchBrackets: true,
      json: true,
      mode: "application/json",
      indentUnit: 4,
      indentWithTabs: true,
      theme: "eclipse"
    }).on("blur", (cm, evt) => {
      cm.save();
    });
  });
}

function startDragingComponent(itm) {}

function addComponent(itm) {
  var jItm = jQuery(itm);
  var index = jQuery("#component-area").children().length;
  jQuery.get(baseAdminCompUrl + "/create/" + index + "/" + jItm.val(), function(
    content
  ) {
    jQuery("#component-area").append(content);
    jQuery(itm).val("");
    initComp();
  });
}

function togglePublish(itm) {
  var compContainer = $(itm).parents(".edit-component");
  compContainer
    .removeClass("panel-primary")
    .removeClass("panel-warning")
    .removeClass("panel-default");

  var curMode = parseInt(compContainer.data("published"));
  curMode = curMode >= 2 ? 0 : curMode + 1;
  compContainer.data("published", curMode + "");
  compContainer.attr("data-published", curMode + "");
  compContainer.find(".comp-published").val(curMode);

  if (curMode == 1) {
    compContainer.addClass("panel-primary");
  } else if (curMode == 2) {
    compContainer.addClass("panel-warning");
  } else {
    compContainer.addClass("panel-default");
  }

  $("button.comp-settings").popover("hide");
}

function toggleCompExpand(itm) {
  var compContainer = $(itm).parents(".edit-component");
  compContainer
    .find(".indicate-expand")
    .removeClass("fa-minus")
    .removeClass("fa-plus");

  if (compContainer.find(".panel-body:visible").length) {
    compContainer.find(".indicate-expand").addClass("fa-plus");
    compContainer.find(".panel-body").slideUp(500);
  } else {
    compContainer.find(".indicate-expand").addClass("fa-minus");
    compContainer.find(".panel-body").slideDown(500, function() {
      initComp();
    });
  }
}

function selectComponent(itm) {
  var jItm = jQuery(itm);
  var index = jQuery("#component-area").children().length;

  jQuery.get(baseAdminCompUrl + "/edit/" + jItm.val() + "/" + index, function(
    content
  ) {
    var compContent = $(content);
    jQuery("#component-area").append(compContent);
    jQuery(itm).val("");
    compContent.find("input.component-id").val("");
    compContent
      .find("input.component-name")
      .val(compContent.find("input.component-name").val());
    initComp();
  });
}

function saveTemplate(itm) {
  var compName = $("#template_name")
    .val()
    .trim();
  if (compName == "") {
    return;
  }
  $.post(
    baseAdminCompUrl +
      "/template/" +
      $("#SaveTemplateDialog").data("id") +
      "/copy/" +
      $("#template_replace").val(),
    {
      name: compName,
      init: $("#template_publish_init").val(),
      selectable_by: $("#selectable_by").val()
    },
    function(response) {
      if (response.new_id != undefined) {
        $("#template_replace").append(
          $('<option value="' + response.new_id + '">' + compName + "</option>")
        );
      } else {
        $(
          "#template_replace option[value=" + $("#template_replace").val() + "]"
        ).text(compName);
      }
    }
  ),
    "json";
}

function deleteTemplate(itm) {
  $("#template_replace").focus();
  if ($("#template_replace").val() == 0) return;

  $.ajax({
    url: baseAdminCompUrl + "/template/" + $("#template_replace").val(),
    type: "DELETE",
    success: function(data) {
      $(
        "#template_replace option[value=" + $("#template_replace").val() + "]"
      ).remove();
    }
  });
}

function checkSelectedComponent(itm) {
  var type = $(itm).find(':selected').parent().data('type');

  if (type == "base"){
    addComponent(itm);
  }
  else{
    selectComponent(itm);
  }
}
