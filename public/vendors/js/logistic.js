//--------------------------------
function printPDF(printURL){

    setTimeout(() => {            
        printJS({
            printable: printURL,
            type: 'pdf',
            showModal: true, 
            maxWidth: 400,
            // modalMessage: "Document Loading...",
            onError: (err) => console.log(err),
            fallbackPrintable: () => console.log("FallbackPrintable"),      
            // onPrintDialogClose: () => console.log('The print dialog was closed')
        });
        
        // console.log("TimeOut Executado");
    }, 3000);
    
    // updateConnIndicator('{{URL("connote/numConnsIndicator")}}', 'created-indicator');
    
}


// Fuction to convert a string to capitalize
//--------------------------------------------------
function toCapitalizeStr(inStr){
    arr = inStr.split(" ");
    
    for (var i = 0; i < arr.length; i++) {
        arr[i] = arr[i].charAt(0).toUpperCase() + arr[i].slice(1).toLowerCase();    
    }
    outStr = arr.join(" ");;

    return outStr;
}


function updateConnIndicator(url, idEle){
    $.ajax({
        url: url,
        context: document.body
    }).done(function(data) {   
        $("#"+idEle).html(data.num_conns);
        if (data.num_conns_pending <= 0){
            $("#"+idEle).addClass("hidden");
        }else{
            $("#"+idEle).removeClass("hidden");
        }
    });

}