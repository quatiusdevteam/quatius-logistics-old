var addresses = [];
var list_state_city_postcode = [];

function loadRemoteAddress(callback){
    $.getJSON(baseUrl+"address/all", function(response){
        addresses = response.data;
        var addressArea = $('.ct-address-area');
        if (addressArea.length == 0){
            $('body').append($('<div class="ct-address-area">'));
            addressArea = $('.ct-address-area');
        }
        else{
            addressArea.empty();
        }
        addressArea.append(getLayoutAddresses());

        if (callback) callback(response);
    });
}

function loadRemotePostalCodes(countryCode="AU", callback){
    list_state_city_postcode = [];
    $.getJSON(baseUrl+"address/postal/"+countryCode, function(response){
        let arr = response.data;
        for (let i=0; i< arr.length; i++){
            list_state_city_postcode[i] = toCapitalizeStr(arr[i][1])+", "+arr[i][2]+" "+ arr[i][0];                    
        }
        if (callback) callback(response);
    });
}


function getAddress(id){
    for(let address of addresses){
        if (id == address.id)
            return address;
    }
    return null;
}

function showAddress(id){
    return getLayoutAddress(getAddress(id));
}

function getLayoutAddress(address){
    if (address){
        return $('<div class="ct-address" onclick="onclickAddress(this);" data-id="'+address.id+'" data-type="'+address.address_type+'"><div class="spacer">' 
            +'<span id="ct-company">'+ address.company + '</span> <span id="ct-first-name" class="ct-first-name">' + address.first_name + '</span> <span id="ct-last-name">' +address.last_name +'</span><br>'
            +'<span id="ct-address-1">'+address.address_1 + '</span> <span id="ct-address-2">' +address.address_2 +'</span><br>'
            +'<span id="ct-city">'+address.city + '</span> <span id="ct-state">'+ address.state + '</span> <span id="ct-postcode">' +address.postcode +'</span><br>'
            +'<i class="fa fa-phone"></i>&nbsp; <span id="ct-phone-1">'+address.phone_1 +'</span><br/>'
            +'<i class="fa fa-envelope"></i>&nbsp; <span id="ct-email">'+address.email +'</span></div></div>');
    }

    return "";
}

function getLayoutAddresses(){
    var listDiv = $('<div class="ct-addresses hide">');
    for(let address of addresses){
        listDiv.append(getLayoutAddress(address));
    }
    return listDiv;
}

function getRawListAddresses(isArray=false){
    var listAddrs = [];

    for(let address of addresses){
        str = [address.first_name, address.last_name, address.company, address.phone_1, address.email, address.address_1, address.address_2, address.city, address.state, address.postcode];
        if (! isArray){            
            str = str.join(', ');
        }
        listAddrs.push(str); 
    }
    return listAddrs;
}


function onclickAddress(itm){
    console.log('onclickAddress - not implemented', itm);
}

$(document).ready(()=>{
    loadRemoteAddress((response)=>{
    });
});