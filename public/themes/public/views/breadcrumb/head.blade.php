@if($menu->active && !(Request::path() == '/' || Request::path() == 'home'))
	<?php  		
		Theme::breadcrumb()->crumbs = array();
 		Theme::breadcrumb()->add('Home', trans_url('/'))
 		->add($menu->name, ($menu->url=='#' || $menu->url=='')? '#': trans_url($menu->url));
 	?>
		
	@if($menu->hasChildren())
		@include('public::breadcrumb.tail', array('menus' => $menu->getChildren()))
	@endif
@endif