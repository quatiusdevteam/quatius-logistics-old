<div class="page-content">
	@if ($page->heading !="")
		<h2>{!!$page->heading!!}</h2>
	@endif
		
	@if ($page->compiler && ($page->compiler == 'blade' || $page->compiler == 'php'))
		{!! Theme::blader($page->content, ['page' => $page]) !!}
	@else
		{!!$page->content!!}
	@endif
</div>