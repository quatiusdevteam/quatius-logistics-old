<ul class="dropdown-menu">

    @foreach ($menus as $menu)
		<?php 
			if(!$menu->checkAccess(user())) continue;
			
			$childView = $menu->hasChildren()?view('public::menu.sub.main', array('menus' => $menu->getChildren())):"";
		?>
		
	 	@if ($menu->description)
	 		{!! Theme::blader($menu->description, ['menu' => $menu]) !!}
	 	@else
	 		@if (strpos($childView,"li")!==false)
			    <li class="dropdown {{ $menu->active ?: '' }}">
			        <a href="{{$menu->url=='#'? '#': trans_url($menu->url)}}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
			            <i class="{{{ $menu->icon or '' }}}"></i> <span>{{$menu->name}}</span>
			             <span class="caret"></span>
			        </a>
			        {!!$childView!!}
			    </li>
			    @else
			    <li class="{{ $menu->active ?: '' }}">
			        <a href="{{$menu->url=='#'? '#': trans_url($menu->url)}}" target="{{$menu->target}}">
			            <i class="{{{ $menu->icon or '' }}}"></i>
			            <span>{{$menu->name}}</span>
			        </a>
			    </li>
	   		@endif
	    @endif
    @endforeach
</ul>
