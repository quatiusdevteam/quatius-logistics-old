<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Inherit from another theme
    |--------------------------------------------------------------------------
    |
    | Set up inherit from another if the file is not exists,
    | this is work with "layouts", "partials", "views" and "widgets"
    |
    | [Notice] assets cannot inherit.
    |
    */

    'inherit' => null, //default

    /*
    |--------------------------------------------------------------------------
    | Listener from events
    |--------------------------------------------------------------------------
    |
    | You can hook a theme when event fired on activities
    | this is cool feature to set up a title, meta, default styles and scripts.
    |
    | [Notice] these event can be override by package config.
    |
    */

    'events'  => [

        // Before event inherit from package config and the theme that call before,
        // you can use this event to set meta, breadcrumb template or anything
        // you want inheriting.
        'before'             => function ($theme) {
            // You can remove this line anytime.
            $theme->setTitle(trans('cms.name'));

            $theme->asset()->add('jquery', 'packages/jquery/js/jquery.min.js');
            // Breadcrumb template.
            // $theme->breadcrumb()->setTemplate('
            //     <ul class="breadcrumb">
            //     @foreach ($crumbs as $i => $crumb)
            //         @if ($i != (count($crumbs) - 1))
            //         <li><a href="{{ $crumb["url"] }}">{{ $crumb["label"] }}</a><span class="divider">/</span></li>
            //         @else
            //         <li class="active">{{ $crumb["label"] }}</li>
            //         @endif
            //     @endforeach
            //     </ul>
            // ');
        },

        // Listen on event before render a theme,
        // this event should call to assign some assets,
        // breadcrumb template.
        'beforeRenderTheme'  => function ($theme) {
            //You may use this event to set up your assets.

            //You may use this event to set up your assets.

        	//$theme->asset()->usepath()->add('styles', 'css/style.css');
        	/*
        	$theme->asset()->add('jquery', 'packages/jquery/js/jquery.min.js'); */
        },

        // Listen on event before render a layout,
        // this should call to assign style, script for a layout.
        'beforeRenderLayout' => [

            'default' => function ($theme) {
            	$theme->asset()->add('vendor_public', 'css/vendor_public.css');
            	$theme->asset()->add('styles', 'css/vendor_public.css');
            	$theme->asset()->usepath()->add('styles', 'css/style.css');
            },
            
            'scholl'    => function ($theme) {
            	$theme->asset()->add('allcss', 'build/css/all-363f48c9ce.css');
            	$theme->asset()->usepath()->add('template', 'css/scholl-template.css');
            	$theme->asset()->usepath()->add('styles', 'css/custom.css');
            	
            	$theme->asset()->add('alljs', 'build/js/all-9bdaff5330.js');
            },
            
            'user'    => function ($theme) {
            	$theme->asset()->add('vendor_public', 'css/vendor_public.css');
            	$theme->asset()->add('styles', 'css/vendor_public.css');
            	$theme->asset()->usepath()->add('styles', 'css/style.css');
            },

            'public'  => function ($theme) {
            	$theme->asset()->add('vendor_public', 'css/vendor_public.css');
            	$theme->asset()->add('styles', 'css/vendor_public.css');
            	$theme->asset()->usepath()->add('styles', 'css/style.css');
            	
            },

            'home'    => function ($theme) {
                $theme->asset()->add('vendor_public', 'css/vendor_public.css');
            	$theme->asset()->add('ionicons', 'packages/ionicons/css/ionicons.min.css');
                $theme->asset()->usepath()->add('styles', 'css/style.css');
                $theme->asset()->add('jquery', 'packages/jquery/js/jquery.min.js');
            },

        ],

    ],

];
