<!DOCTYPE html>
<html lang="en">
  <head>
        <meta charset="UTF-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

		{!! Meta::tag('robots') !!}

		{!! Meta::tag('site_name', config('app.name', 'Web Development')) !!}
		{!! Meta::tag('url', Request::url()); !!}
		{!! Meta::tag('locale', 'en_EN') !!}

		{!! Meta::tag('title', config('app.name', 'Web Development')) !!}
		{!! Meta::tag('description') !!}
		<meta name="keywords" content="{{Theme::place('keywords')}}">

		{!! Meta::tag('image', Theme::asset()->url('img/logo.png')) !!}

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="{{asset('apple-touch-icon.png')}}">
        <link href="{{ asset('css/vendor_public.css') }}" rel="stylesheet">
        <link href="{{ asset('css/hover.css') }}" rel="stylesheet">
		<link href="{{ asset('css/logistic.css') }}" rel="stylesheet">
		<link href="{{ asset('css/aos.css') }}" rel="stylesheet">
        {!! Theme::partial('_header') !!}
    <link rel="icon" href="{{asset('/favicon.ico')}}">

    <title>{{ Meta::get('title', config('app.name', 'Web Development')) }}</title>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113096909-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-113096909-1');
	</script>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{asset('build/css/all-363f48c9ce.css')}}">
    <link rel="stylesheet" href="{{url('/')}}{{ elixir('css/all.css') }}">
    <script type="text/javascript" src="{{asset('build/js/all-9bdaff5330.js')}}"></script>

    {!! Theme::asset()->styles() !!}

	@if(!env("APP_DEBUG"))
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-GBGJ2MQ2ET"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-GBGJ2MQ2ET');
        </script>
		
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-W5H5V3V');</script>
		<!-- End Google Tag Manager -->

    @endif
		
  </head>
  <body>
	@if(!env("APP_DEBUG"))
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src=https://www.googletagmanager.com/ns.html?id=GTM-W5H5V3V
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->

	@endif

  	{!! Theme::partial('header') !!}
	@if(Theme::has('showcase'))
		<div class="col-xs-12 showcase">
	    	{!!Theme::place('showcase')!!}
		</div>
	@endif

	<div id="area-content">
	{!! Theme::partial('breadcrumb') !!}
	
	<div class="container system-alert">
		@include('flash::message')
    </div>

	@if(Theme::has('content-header'))
	<div class="container content-header">
		<div class="col-xs-12">
			{!!Theme::place('content-header')!!}
			
		</div>
	</div>
	
	@endif
	<div class="clearfix"></div>
	@if(Theme::has('fullwidth'))
	<div class="full-width">
		<div class="col-xs-12">
			{!!Theme::place('fullwidth')!!}
			
		</div>
	</div>
	
	@endif
	<div class="clearfix"></div>
	{!! Theme::content() !!}
	
	@if(Theme::has('content-footer'))
	<div class="container content-footer">
		<div class="col-xs-12">
				{!!Theme::place('content-footer')!!}
		</div>
	</div>
	@endif
	
    </div>
	@if(Theme::has('page-bottom'))
	<div class="page-bottom" style="display: inline-block ; width:100%">
		{!!Theme::place('page-bottom')!!}
	</div>
	@endif
    {!! Theme::partial('footer') !!}
    
	
    <!-- Bootstrap core JavaScript
    
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript">
    var baseUrl = "{{url('/')}}";
    </script>
    {!! Theme::partial('_footer') !!}
    <script type="text/javascript">
		$('#flash-overlay-modal').modal();
	</script>
	
    {!! Theme::asset()->container('footer')->scripts() !!}

    {!! Theme::asset()->scripts() !!}
  </body>
  <script src="{{ asset('js/aos.js') }}"></script>	
  <script>
  	AOS.init({
	  duration: 800
	});
		
  </script>
</html>
