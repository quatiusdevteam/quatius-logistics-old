
<div id="wrap">
@script
<script type="text/javascript">
		jQuery(document).ready(function(){
		jQuery('.order-preview .order-item [data-toggle=\"popover\"]').popover();
		jQuery('.page-header .dropdown').hover(
			function(){ // mouseenter 
			},
			function(){ // mouseleave
				if (jQuery(this).parents('.collapse.in').length == 0)
				{
					jQuery(this).find('[data-toggle=\"dropdown\"]').attr('aria-expanded', 'false');
					jQuery(this).removeClass('open');
				}
			}
		);

	});
</script>
@endscript

<header>
	<!-- header-area start -->
	<div id="sticker" class="header-area">
		<div class="container">
			<div class="row">
				<div id="top-link" class="small">
					<div class="col-sm-4 col-xs-3 small-link" >
	    				<div class="row">
	    					@if (Auth::guest())
	    					<div id="login" class="col-xs-4">
	    						<div class="row">
	    							<a href="{!!url('login')!!}">
		            					<span class="fa fa-key"></span>
		            					<span class="hidden-xs">Login</span>
		            				</a>
	    						</div>
	    					</div>
	    					@else
	    					<div id="login" class="col-xs-4">
	    						<div class="row">
	    							<a href="{!!url('selfmanage')!!}">
		            					<span class="fa fa-briefcase"></span>
		            					<span class="hidden-xs">My portal</span>
		            				</a>
	    						</div>
	    					</div>
	    					@endif
	    					<div id="login" class="col-xs-4">
	    						<div class="row">
	    							<a href="#">
		            					<span class="fa fa-trophy"></span>
		            					<span class="hidden-xs">Career</span>
		            				</a>
	    						</div>
	    					</div>
	    					<div id="login" class="col-xs-4">
	    						<div class="row">
	    							<a href="{!!url('contact-us')!!}">
		            					<span class="fa fa-envelope"></span>
		            					<span class="hidden-xs">Contact Us</span>
		            				</a>
	    						</div>
	    					</div>
	    				</div>
	    			</div>
	    			<div class="col-sm-8 col-xs-9">
	    				<div class="row pull-right">
	    					<div class="panel panel-primary">
							  <div class="hvr-sweep-to-right panel-heading" data-aos="fade-down" data-aos-delay="1800">
							  		<h6 class="small narrow">
							  			<a href="{!!url('contact-us')!!}">
							  				<span class="fa fa-calculator"></span> GET QUOTE
							  			</a>
							  		</h6>
							  </div>
							</div>
	    				</div>
	    			</div>
				</div>
				<div class="col-md-3">
					<div class="row">
						<a class="navbar-brand page-scroll" href="home"><img src="{{asset('images/quatius-logo.png')}}" /></a>
					</div>
				</div>
				<div class="mainmenu col-md-9">
					<div class="row pull-right">
						<nav>
							{!!Menu::menu('footer', 'public::menu.main')!!}
						</nav>
					</div>
										
				</div>
			</div>
		</div>
	</div>
	<!-- // end header -->
	<!-- mobile header -->
	<div class="mobile-menu-area">
		<div class="container">
			<div class="">
				<div class="col-xs-12">
				<div id="top-link" class="small">
					<div class="col-sm-4 col-xs-4 small-link pull-left" >
					<div class="row">
	    					@if (!Auth::guest())
	    					<div id="login" class="col-xs-4">
	    						<div class="row">
	    							<a href="{!!url('login')!!}">
		            					<span class="fa fa-key"></span>
		            					<span class="hidden-xs">Login</span>
		            				</a>
	    						</div>
	    					</div>
	    					@else
	    					<div id="portal" class="col-xs-4">
	    						<div class="row">
	    							<a href="{!!url('selfmanage')!!}">
		            					<span class="fa fa-briefcase"></span>
		            					<span class="hidden-xs">My portal</span>
		            				</a>
	    						</div>
	    					</div>
	    					@endif
	    					<div id="login" class="col-xs-4">
	    						<div class="row">
	    							<a href="#">
		            					<span class="fa fa-trophy"></span>
		            					<span class="hidden-xs">Career</span>
		            				</a>
	    						</div>
	    					</div>
	    					<div id="login" class="col-xs-4">
	    						<div class="row">
	    							<a href="{!!url('contact-us')!!}">
		            					<span class="fa fa-envelope"></span>
		            					<span class="hidden-xs">Contact Us</span>
		            				</a>
	    						</div>
	    					</div>
	    				</div>
	    			</div>
	    			<div class="col-sm-8 col-xs-8">
	    				<div class="row pull-right">
	    					<div class="panel panel-primary">
							  <div class="panel-heading" data-aos="fade-down" data-aos-delay="1500">
							  		<h6 class="small narrow">
							  			<a href="{!!url('contact-us')!!}">
							  				<span class="fa fa-calculator"></span> GET QUOTE
							  			</a>
							  		</h6>
							  </div>
							</div>
	    				</div>
	    			</div>
				</div>
				<div class="mobile-menu row" >
					<div class="navbar-header page-scroll">
		                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#logistic-nav-1">
		                    <span class="sr-only">Toggle navigation</span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                </button>

				<script>
                                    var button = document.querySelector('.icon-bar');
                                    button.addEventListener('click', function (){
                                       button.classList.toggle('open');
                                    });
				</script>

		                <a class="navbar-brand page-scroll" href="home"><img src="{{asset('images/quatius-logo.png')}}" /></a>
		            </div>
		
		            <!-- Collect the nav links, forms, and other content for toggling -->
		            <div class="collapse navbar-collapse" id="logistic-nav-1">
						{!!Menu::menu('footer', 'public::menu.mobile')!!}
		                
		            </div>
				</div>
			</div>
			</div>
		</div>
	</div>
	<!-- // end mobile area -->
</header>
				
	
</div>

