<!-- Fonts
		============================================ -->		
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,300,700' rel='stylesheet' type='text/css'>

		

		<!-- CSS  -->
				
		<!-- font-awesome CSS
		============================================ -->
        <link rel="stylesheet" href="{{asset('vendors/css/font-awesome.min.css')}}">
		
		<!-- owl.carousel CSS
		============================================ -->
        <link rel="stylesheet" href="{{asset('vendors/css/owl.carousel.css')}}">
		
		<!-- animate CSS
		============================================ -->
        <link rel="stylesheet" href="{{asset('vendors/css/animate.css')}}">
		
		<!-- fancybox CSS
		============================================ -->
        <link rel="stylesheet" href="{{asset('vendors/css/fancybox/jquery.fancybox.css')}}">			
		
		<!-- meanmenu CSS
		============================================ -->		
        <link rel="stylesheet" href="{{asset('vendors/css/meanmenu.min.css')}}">
		
		
		
		<!-- normalize CSS
		============================================ -->		
        <link rel="stylesheet" href="{{asset('vendors/css/normalize.css')}}">
		
		<!-- main CSS
		============================================ -->		
        <link rel="stylesheet" href="{{asset('vendors/css/main.css')}}">		
        <link rel="stylesheet" href="{{asset('vendors/css/style.css')}}">	
		
		<!-- responsive CSS
		============================================ -->			
        <link rel="stylesheet" href="{{asset('vendors/css/responsive.css')}}">
			
        
        
        <!-- custom CSS -->
        <link rel="stylesheet" href="{{asset('css/logistic.public.css')}}">