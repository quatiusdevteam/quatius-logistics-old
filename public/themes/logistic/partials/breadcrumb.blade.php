<div class="container text-right" id="area-breadcrumb">
	<?php
    
    /*Theme::breadcrumb()->setTemplate('
    <ul class="breadcrumb">
    @foreach ($crumbs as $i => $crumb)
        @if ($i != (count($crumbs) - 1))
        <li><a href="{{ $crumb["url"] }}">{{ $crumb["label"] }}</a><span class="divider"></span></li>
        @else
        <li class="active">{{ $crumb["label"] }}</li>
        @endif
    @endforeach
    </ul>');*/
    
    Theme::breadcrumb()->setTemplate('
	<nav aria-label="breadcrumb">
	<ul class="breadcrumb">
		<?php foreach ($crumbs as $i => $crumb):?>
			@if ($i != (count($crumbs) - 1))
			<li class="breadcrumb-item"><a href="{{ $crumb["url"] }}">{{ $crumb["label"] }}</a><span class="divider"></span></li>
			@else
			<li class="breadcrumb-item active" aria-current="page">{{ $crumb["label"] }}</li>
			@endif
		<?php endforeach; ?>
	</ul>
	</nav>');
    ?>
    
	{!! Theme::breadcrumb()->render() !!}
</div>