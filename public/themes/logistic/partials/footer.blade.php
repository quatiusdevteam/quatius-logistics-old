
<div class="page-footer">	
	<div class="footer-copyright">
		<div class="container">
			<div class="col-md-3 col-sm-3 hidden-12">
				<div class="row">
					<img src="{{asset('images/footer-logo.png')}}" alt="Quatius Logistics" title="Quatius Logistics">
				</div>
			</div>
			<div class="col-md-3 col-sm-3 hidden-12">
				<div class="row">
					<h5 class="footer-title">Quick Links</h5>
					{!!Menu::menu('footer', 'public::menu.footer')!!}
				</div>
			</div>
			<div class="col-md-3 col-sm-3 hidden-12">
				<div class="row">
					<h5 class="footer-title">Information</h5>
					{!!Menu::menu('policies', 'public::menu.footer')!!}
				</div>
			</div>
			<div class="col-md-3 col-sm-3 hidden-12">
				<div class="row">
					<h5 class="footer-title">Contact Us</h5>
					<div class="body">
						<p>414 Lower Dandenong Road,<br />Braeside, Victoria 3195</p>
						<p class="tel">1300 899 299</p>
						<p><a href="mailto:info@quatiuslogistics.com.au">info@quatiuslogistics.com</a></p>
					</div>
				</div>
			</div>
			<div class="col-xs-12">
				<p class="copyright">COPYRIGHT @<?php echo date('Y');?> <a href="www.quatiuslogistics.com">WWW.QUATIUSLOGISTICS.COM</a> ALRIGHT RESERVED.</p>
			</div>
		</div>
	</div>
</div>