<!-- bootstrap js -->       
		<!-- owl.carousel.min js -->
        <script src="{{asset('vendors/js/owl.carousel.min.js')}}"></script>
		
		<!-- meanmenu js -->
        <script src="{{asset('vendors/js/jquery.meanmenu.js')}}"></script>
		
		<!-- jquery.countdown js -->
        <script src="{{asset('vendors/js/jquery.countdown.min.js')}}"></script>
		
		<!-- parallax js -->
        <script src="{{asset('vendors/js/parallax.js')}}"></script>	

		<!-- jquery.collapse js -->
        <script src="{{asset('vendors/js/jquery.collapse.js')}}"></script>
		
		<!-- jquery.easing js -->
        <script src="{{asset('vendors/js/jquery.easing.1.3.min.js')}}"></script>	
		
		<!-- jquery.scrollUp js -->
        <script src="{{asset('vendors/js/jquery.scrollUp.min.js')}}"></script>	
		
		<!-- jquery.appear js -->
		<script src="{{asset('vendors/js/jquery.appear.js')}}"></script>	
		
		<!-- jquery.mixitup js -->
		<script src="{{asset('vendors/js/jquery.mixitup.min.js')}}"></script>

		<!-- fancybox js -->
		<script src="{{asset('vendors/js/fancybox/jquery.fancybox.pack.js')}}"></script>			

		<!-- jquery.counterup js -->
        <script src="{{asset('vendors/js/jquery.counterup.min.js')}}"></script>
        <script src="{{asset('vendors/js/waypoints.min.js')}}"></script>	

		<!-- rs-plugin js -->
		<script type="text/javascript" src="{{asset('vendors/lib/rs-plugin/js/jquery.themepunch.tools.min.js')}}"></script>   
		<script type="text/javascript" src="{{asset('vendors/lib/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>
		<script src="{{asset('vendors/lib/rs-plugin/rs.home.js')}}"></script>		
		
		<!-- wow js -->
        <script src="{{asset('vendors/js/wow.js')}}"></script>		
		<script>
			new WOW().init();
		</script>
		
		<!-- plugins js -->
        <script src="{{asset('vendors/js/plugins.js')}}"></script>
		
		<!-- main js -->
        <script src="{{asset('vendors/js/main.js')}}"></script>