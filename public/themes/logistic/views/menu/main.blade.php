
<ul id="nav">					
@foreach ($menus as $menu)
    <?php 
        if(!$menu->checkAccess(user())) continue;
    ?>
    
    @if ($menu->description)
        {!! renderBlade($menu->description, ['menu' => $menu]) !!}
    @else
	
        @if ($menu->hasChildren())
        <li class="treeview {{ $menu->active ?'active':'' }}">
            <a href="{{trans_url($menu->url)}}" >
                <i class="{{{ $menu->icon ?: '' }}}"></i> <span>{{$menu->name}}</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            @include('menu::menu.sub.side', array('menus' => $menu->getChildren()))
        </li>
        @else
        <li class="{{ $menu->active ?'active':'' }}">
            <a href="{{trans_url($menu->url)}}">
                <i class="{{{ $menu->icon ?: '' }}}"></i>
                <span>{{$menu->name}}</span>
            </a>
        </li>
        @endif
    @endif
@endforeach
</ul>