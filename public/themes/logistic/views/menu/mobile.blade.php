<ul class="nav navbar-nav navbar-right">
	@for ($i=0;  $i < $menus->count(); $i++)
		<?php 
			if(!$menus->get($i)->checkAccess(user())) continue;
		?>
		@if($menus->has($i))
			<li class="treeview {{ $menus->get($i)->active ?: '' }}">
				<a class="page-scroll" href="{{$menus->get($i)->url}}" target="{{ $menus->get($i)->target ?: '' }}">{{$menus->get($i)->name}}</span></a>
			</li>
		@else
			<li>&nbsp;</li>
		@endif
	@endfor
</ul>