<div class="body">
	<ul>
		@for ($i=0;  $i < $menus->count(); $i++)
			<?php 
				if(!$menus->get($i)->checkAccess(user())) continue;
			?>
			@if($menus->has($i))
				<li><a href="{{$menus->get($i)->url}}" target="{{ $menus->get($i)->target ?: '' }}"><span>{{$menus->get($i)->name}}</span></a></li>
			@else
				<li>&nbsp;</li>
			@endif
		@endfor
	</ul>
		
</div>