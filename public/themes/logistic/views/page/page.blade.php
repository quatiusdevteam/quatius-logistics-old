@if (!(Request::path() != '/' || Request::path() != '/home'))
	<?php Theme::breadcrumb()->add('Home', url('/'))->add($page->title, url('/'));?>
@endif

@if(!empty($page->content))
<div class="container">
	<div class="col-xs-12">
		@if ($page->heading !="")
			<h2>{!!$page->heading!!}</h2>
		@endif
			
		@if ($page->compiler && ($page->compiler == 'blade' || $page->compiler == 'php'))
			{!! Theme::blader($page->content, ['page' => $page]) !!}
		@else
			{!!$page->content!!}
		@endif
	</div>
</div>
@endif