
<?php
$numOfcount = $menus->count();
$firstMenu = ceil($numOfcount/2);
$i = 0;
?>
<div class="col-md-3 col-sm-4  col-xs-12 ">
	<h5 class="footer-title">Help</h5>
	<ul>
		@for ($i=0; $i < $firstMenu; $i++)
			<li><a href="{{$menus->get($i)->url}}" target="{{ $menus->get($i)->target or '' }}"><span>{{$menus->get($i)->name}}</span></a></li>
		@endfor
		
	</ul>
</div>
<div class="col-md-3 col-sm-4  col-xs-12 ">
	<h5 class="footer-title hidden-xs">&nbsp;</h5>
	<ul>
		@for (; $i < $firstMenu*2; $i++)
			@if($menus->has($i))
				<li><a href="{{$menus->get($i)->url}}" target="{{ $menus->get($i)->target or '' }}"><span>{{$menus->get($i)->name}}</span></a></li>
			@else
				<li>&nbsp;</li>
			@endif
		@endfor
	</ul>

</div>
