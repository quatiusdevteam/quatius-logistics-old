
@includeScript('js/jquery.suggest.js')

@script
<script>
var dataSources = <?php echo json_encode($listProduct);?>;
$(document).ready(function(){
$('.typeahead').typeahead({
	  hint: true,
	  highlight: true,
	  minLength: 1
	},
	{
	  name: 'dataSources',
	  source: substringMatcher(dataSources)
	});
	
});
var substringMatcher = function(strs) {
	  return function findMatches(q, cb) {
	    var matches, substringRegex;
	    matches = [];
	    substrRegex = new RegExp(q, 'i');
	    $.each(strs, function(i, str) {
	      if (substrRegex.test(str)) {
	        matches.push(str);
	      }
	    });
	    cb(matches);
	  };
	};
</script>
@endscript
<form action="{{url('/')}}/search/products">
	    <div class=" input-group area-search">
		  <input type="text" name="search" id="searchProduct" required class="form-control typeahead tt-query" placeholder="Search for..." autocomplete="off" data-provide="typeahead" spellcheck="false">
	      <span class="input-group-btn">
	        <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
	      </span>
	    </div>
    </form>
