
<div class="page-footer">
	<div class="footer-menu">
		<div class="container">
{!!Menu::menu('footer', 'public::menu.footer')!!}			
			<div class="col-md-3 col-sm-4 col-md-offset-3 col-xs-12 footer-payment">
				<h5 class="footer-title">Payment Options</h5>
				<img style="height: 20px" src="{{ asset('themes/public/assets/img/pay-methods.png') }}">
			</div>
			{{--
			<div class="col-md-3 col-sm-4 col-xs-12 ">
				<h5 class="footer-title col-xs-hidden">Newsletter</h5>
				<span>Subscribe to newsletter</span>
				<form action="#">
					<div class=" input-group input-group-sm">
						<span class="input-group-addon" style="padding: 4px 10px; font-size: 12px; color: #999;"><span class="glyphicon glyphicon-envelope"></span></span>
					  <input autocomplete="off" type="email" name="subcribe-email" required class="form-control" placeholder="Email">
				      <span class="input-group-btn">
				        <button class="btn btn-default" style="padding: 4px 10px; font-size: 16px; color: #999;" type="submit"><span class="glyphicon glyphicon-arrow-right"></span></button>
				      </span>
				    </div>
				    <label style="font-weight: normal; font-size: 12px;"><input autocomplete="off" required type="checkbox"> I authorize the treatment of my personal data for marketing purposes(newsletters, news, promotions) by Scholl Austrialia
				    </label>
			    </form>
			</div>
			--}}
		</div>
	</div>
	<div class="footer-copyright">
		<div class="container">
			<div class="col-sm-9 col-xs-12 ">
				<h6>&copy; 2016 Scholl Footwear</h6><br />
				<span>
				Scholl<sup>&reg;</sup> and Logo Scholl, Gelactiv, Bioprint, Memory Cushion, Biomechanics and Pescura.<br>
They are registered trademarks of Reckitt Benckiser Group, used under license. Managed by triboo digitale 
				</span>
			</div>
			<!-- 
			<div class="col-sm-3 col-xs-12 ">
				<img class="pull-right" style="height: 20px" src="{{url('/')}}/images/social-footer.png">
			</div>
			 -->
		</div>
	</div>
</div>