<div class="container text-right" id="area-breadcrumb">
	<?php Theme::breadcrumb()->setTemplate('<ul class="breadcrumb">
    foreach ($crumbs as $i => $crumb)
        @if ($i != (count($crumbs) - 1))
        <li><a href="{{ $crumb["url"] }}">{{ $crumb["label"] }}</a><span class="divider"></span></li>
        @else
        <li class="active">{{ $crumb["label"] }}</li>
        @endif
    @endforeach
    </ul>');?>
	{!! Theme::breadcrumb()->render() !!}
</div>