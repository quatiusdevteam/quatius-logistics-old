
<div id="wrap">
@script
<script type="text/javascript">
		jQuery(document).ready(function(){
		jQuery('.order-preview .order-item [data-toggle=\"popover\"]').popover();
		jQuery('.page-header .dropdown').hover(
			function(){ // mouseenter 
			},
			function(){ // mouseleave
				if (jQuery(this).parents('.collapse.in').length == 0)
				{
					jQuery(this).find('[data-toggle=\"dropdown\"]').attr('aria-expanded', 'false');
					jQuery(this).removeClass('open');
				}
			}
		);

	});
</script>
@endscript


	<div class="navbar-wrapper page-header">
	<div class="bar-account navbar navbar-fixed-top">
		<div class="container">
			<div class="col-xs-6 col-sm-4">
				@include("public::menu.search")
			</div>
			<div class="col-xs-6 col-sm-8">
				<div class="row nav-bar-account">
					<ul class="">
					@if (Auth::guest())
						<li>
							<a href="{{url('/')}}" role="button"><span class="glyphicon glyphicon-home"></span> <span class="navbar-collapse collapse">Home &nbsp; &nbsp;</span></a>
		                </li>
	                	<li>
	                  		<a href="{{url('/')}}/login" role="button"><span class="glyphicon glyphicon-user"></span> <span class="navbar-collapse collapse">Login &nbsp; &nbsp;</span></a>
	                  	</li>
	                  	<li>
	                  		<a href="{{url('/')}}/register" role="button"><span class="glyphicon glyphicon-edit"></span> <span class="navbar-collapse collapse">Sign Up &nbsp; &nbsp;</span></a>
	                  	</li>
	                @else
	                	<li>
	                  		<a href="{{url('/')}}" role="button"><span class="glyphicon glyphicon-home"></span> <span class="navbar-collapse collapse">Welcome <span style="text-transform: capitalize;">{{ Auth::user()->name }}</span></span></a>
	                  	</li>
	                  	<li class="dropdown">
	                  		<a href="{{url('/')}}/account/profile" role="button"><span class="glyphicon glyphicon-user"></span> <span class="navbar-collapse collapse">My Account</span></a>
	                  		<ul class="dropdown-menu">	                  			
	                  			<li><a href="{{url('/')}}/account/profile#profile" role="button"> Profile</a></li>
	                  			<li><a href="{{url('/')}}/account/profile#activity" role="button"> Order Histories</a></li>	                  				                  			
	                  			<li><a href="{{url('/')}}/logout" role="button"> Logout</a></li>
		                  	</ul>
	                  	</li>
	                 @endif
	                 
	                 @can("user.add_to_favourite")
	                  	<li>
	                  		<a href="{{url('/')}}/user-save/product?name=My+Favourite" role="button"><span class="glyphicon glyphicon-star"></span> <span class="navbar-collapse collapse">My Favourite</span> <span class="favorite-count badge">{{\App\Modules\Product\Models\Product::countSaveModel()}}</span></a>
	                  	</li>
	                 @endcan
	                 
	                 @can("order.add_to_cart")
	                  	<li class="dropdown <?php echo count(app('OrderSession')->getProductGroups()) == 0 ?"no-item" : "";?>">
		                  <a href="{{url('/')}}/order" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-list-alt"></span> <span class="navbar-collapse collapse">My Order</span > <span class="order-preview-count badge">{{count(app('OrderSession')->getProductGroups())}}</span></a>
		                  <ul class="dropdown-menu order-preview">
		                  	@foreach(app('OrderSession')->getProductGroups() as $prod)
		                  		
		                  		<li class="order-item" data-id="{{$prod->id}}" data-slug="{{$prod->getSlug()}}"><div class="prod-image-area"><img class="img-responsive" src="{{$prod->getThumbMedia()->getThumbUrl()}}"></div><div class="product-detail"><a href="#" data-link="{{url('/')}}/product.html/" onclick="jQuery(this).attr('href', jQuery(this).attr('data-link')+jQuery(this).parents('[data-slug]').attr('data-slug')) "><span class="product-name">{{$prod->name}}</span></a></div><button role="button" onclick='removeOrderPreviewItem(this)' class="btn btn-remove btn-xs btn-link pull-right"><span class="glyphicon glyphicon-remove"></span></button></li>
		                  		
		                    @endforeach
		                  	<li class="order-item-tmpl" data-id="0" data-slug=""><div class="prod-image-area"><img src=""></div><div class="product-detail"><a href="#" data-link="{{url('/')}}/product.html/" onclick="jQuery(this).attr('href', jQuery(this).attr('data-link')+jQuery(this).parents('[data-slug]').attr('data-slug')) "><span class="product-name"></span></a></div><button role="button" onclick='removeOrderPreviewItem(this)' class="btn btn-remove btn-xs btn-link pull-right"><span class="glyphicon glyphicon-remove"></span></button></li>
		                    
		                    <li class="btn-view-order"><a href="{{url('/')}}/order" role="button">View Order</a></li>
		                  </ul>
		                </li>
		           	@endcan
		           	
					</ul>
				</div>
			</div>
		</div>
	</div>
      <div class="container bar-category">
        <nav class="navbar navbar-static-top">
          <div class="container">
            <div class="navbar-header" style="margin-right: 0px;">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="{{url('/')}}" style="margin-top: 3px;"><img src="{{ asset('themes/public/assets/img/scholl-logo.png') }}"></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav col-xs-12">
              	@include('Product::partials.menu_category')
<?php /*              	@foreach ($categories as $category)
	              	@if ($category->sub_categories->count() > 0)
	              	<li class="dropdown">
	                	<a href="{{url('/category/')}}/{{$category->slug}}">{{$category->name}}</a>
                		<ul class="dropdown-menu">
		                    @foreach ($category->sub_categories as $sub_category)
		                    	<li><a href="{{url('/category/')}}/{{$sub_category->slug}}">{{$sub_category->name}}</a></li>
		                    @endforeach
						</ul>
					@else
					<li>
		            	<a href="{{url('/category/')}}/{{$category->slug}}">{{$category->name}}</a>
	                @endif
	                </li>
              	@endforeach
          */ ?>
              </ul>
          	</div>
          </div>
        </nav>
      </div>
	<div class="bar-livefeed">
		<div class="container">
		<div class="col-xs-12 text-center"><span class="headline">FREE DELIVERY AND RETURNS ON ALL ORDERS OVER $200</span></div>
		</div>
	</div>
    </div>
</div>

