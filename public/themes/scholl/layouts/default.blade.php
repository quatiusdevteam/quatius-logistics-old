<!DOCTYPE html>
<html lang="en">
  <head>
        <meta charset="UTF-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        
        <meta name="description"		content="{!!Theme::place('description')!!}">
        <meta name="keywords" 			content="{!!Theme::place('keywords')!!}">
        <meta property="og:url"			content="{!!Request::fullUrl()!!}" />
		<meta property="og:type"		content="website" />
		<meta property="og:title"		content="{!!Theme::getTitle()!!}" />
		<meta property="og:description"	content="{!!Theme::place('description')!!}" />
		<meta property="og:image"		content="{!!Theme::place('image_path')!!}" />
        
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="{{asset('apple-touch-icon.png')}}">
        <link href="{{ asset('css/vendor_public.css') }}" rel="stylesheet">
    <link rel="icon" href="{{url('/')}}/favicon.ico">

    <title>{{ Theme::getTitle() }}</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{asset('build/css/all-363f48c9ce.css')}}">
    <link rel="stylesheet" href="{{url('/')}}{{ elixir('css/all.css') }}">
    <script type="text/javascript" src="{{asset('build/js/all-9bdaff5330.js')}}"></script>

    {!! Theme::asset()->styles() !!}
  </head>
  <body>
  
  	{!! Theme::partial('header') !!}
	<div class="system-alert">
		@include('flash::message')
    </div>

	@if(Theme::has('showcase'))
		<div class="showcase">
	    	{!!Theme::place('showcase')!!}
		</div>
	@endif

	<div id="area-content" style="margin-top: 20px;">
	{!! Theme::partial('breadcrumb') !!}
	
	@if(Theme::has('content-header'))
	<div class="container content-header">
		<div class="col-xs-12">
			{!!Theme::place('content-header')!!}
		</div>
	</div>
	
	@endif
	{!! Theme::content() !!}
	
	@if(Theme::has('content-footer'))
	<div class="container content-footer">
		<div class="col-xs-12">
				{!!Theme::place('content-footer')!!}
		</div>
	</div>
	@endif
	
    </div>
	@if(Theme::has('page-bottom'))
	<div class="page-bottom" style="display: inline-block ; width:100%">
		{!!Theme::place('page-bottom')!!}
	</div>
	@endif
    {!! Theme::partial('footer') !!}
    
	
    <!-- Bootstrap core JavaScript
    
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript">
    var baseUrl = "{{url('/')}}";
    </script>
    {!! Theme::asset()->scripts() !!}
    <script type="text/javascript" src="{{asset('js/ajax_order.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/ajax_favourite.js')}}"></script>
    
    <script type="text/javascript">
		$.ajaxSetup({
		   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
		});

		$('#flash-overlay-modal').modal();
	</script>
	
    {!! Theme::asset()->container('footer')->scripts() !!}

  </body>
</html>
