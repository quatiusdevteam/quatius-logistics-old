@if($page->content != "" && $page->content!="<p><br></p>")
<section class="content">

<div class="box">
	@if ($page->heading !="")
	<div class="box-header with-border">
		<h3 class="box-title">{!!$page->heading!!}</h3>
		<!-- <div class="box-tools">
			<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
		</div> -->
	</div>
	@endif

	<div class="box-body">
		@if ($page->compiler && ($page->compiler == 'blade' || $page->compiler == 'php'))
			{!! Theme::blader($page->content, ['page' => $page]) !!}
		@else
			{!!$page->content!!}
		@endif
	</div>
</div>
</section>
@endif