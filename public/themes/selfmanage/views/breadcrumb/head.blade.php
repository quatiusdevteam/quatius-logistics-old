@if($menu->active && !(Request::path() == '/' || Request::path() == 'home' || Request::path() == 'selfmanage'))
	<?php  		
		Theme::breadcrumb()->crumbs = array();
 		Theme::breadcrumb()->add('<i class="fa fa-dashboard"></i> Dashboard', trans_url('home'))
		->add(($menu->icon?'<i class="'.$menu->icon.'"></i> ':'').$menu->name, $menu->url=='#'? '#': trans_url($menu->url));
 	?>
		
	@if($menu->hasChildren())
		@include('public::breadcrumb.tail', array('menus' => $menu->getChildren()))
	@endif
@endif