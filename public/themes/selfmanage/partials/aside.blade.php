
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC0AAAAtCAQAAACQEyoRAAAAKklEQVR42u3MMQ0AAAwDoNW/6alo0gMEkKuJWq1Wq9VqtVqtVqvV6on6AX/3AC4PlglFAAAAAElFTkSuQmCC" class="img-circle" alt="User Image" />
      </div>
      <div class="pull-left info">
        <p>{!!user('web')->name!!}</p>

        <a href="{{URL::to('home')}}"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>
    <!-- search form -->
    <!-- <form action="@if(user('web')->hasRole('admin')) 
    				{{URL::to('selfmanage/admin/search')}}
    			  @else
    					{{URL::to('selfmanage/booking/search')}}
    			  @endif
    			  " method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Consignment number..."/>
        <span class="input-group-btn">
          <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
        </span>
      </div>
    </form> -->
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">&nbsp;</li>
      {!!Menu::menu('manage', 'menu::menu.side')!!}
    </ul>
    <br /><br />    
    <ul class="sidebar-menu" style="position: fixed; bottom: 0px; width: 230px;">
    	<li class="header " style="color: #fff; text-align: center;">
        
          <h3><a href="contact-us" title="click here to contact us!">QUATIUS LOGISTICS <br />HOTLINE</a></h3>
          <h6>If you require more information, <br />please call</h6>
          <h3 class="text-red"><a href="tel:1300899299"><i class="fa fa-phone"></i>&nbsp; 1300 899 299</a></h3>
          
    	</li>
    </ul>    
  </section>
  <!-- /.sidebar -->
</aside>
