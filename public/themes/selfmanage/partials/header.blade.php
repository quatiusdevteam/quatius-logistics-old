<header class="main-header">
    <!-- Logo -->
    <a href="{{ Trans::to('/') }}" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
     <span class="logo-mini"><img src="{{URL::to('/')}}/images/quatius-logo-mini.png"></span>
    <!-- logo for regular state and mobile devices -->
    <!-- <span class="logo-lg">{!!trans('cms.name.html')!!}</span>-->
    <span class="logo-lg"><img src="{{URL::to('/')}}/images/quatius-logo.png" class="img-responsive"></span>
    
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <!-- Notifications: style can be found in dropdown.less -->
                
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    
                    <span>Hello, <strong>{!!user('web')->name!!}</strong></span>
                    <span class="fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu">
                        
                        <!-- Menu Footer-->
                        <li class="user-footer" style="background-color: #1b1f2a;">
                        	<a href="{{ URL::to('account/profile') }}" style="color: #fff">
                        		<span class="fa fa-gears"></span> Profile
                        	</a>
                        	<a href="{{ URL::to('account/edit/password') }}" style="color: #fff">
                        		<span class="fa fa-pencil"></span> Change password
                        	</a>
                        	<a href="{{ URL::to('logout') }}?role=web" style="color: #fff">
                        		<span class="fa fa-gears"></span> Sign out
                        	</a>
                        	
                             <!-- 
                             <div class="pull-left">
                                <a href="{{ URL::to('selfmanage/profile') }}" class="btn btn-default btn-flat">View Profile</a>
                            </div> 
                            <div class="pull-right">
                                <a href="{{ URL::to('logout') }}?role=web" class="btn btn-default btn-flat">Sign Out</a>
                            </div>
                             -->
                        </li>
                    </ul>
                </li>
                  <!-- Control Sidebar Toggle Button -->
                 <!--  <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                  </li> -->
            </ul>
        </div>
    </nav>
</header>
