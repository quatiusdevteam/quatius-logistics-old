      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          {!!trans('cms.version')!!}
        </div>
        <strong>Copyright © 2022 Quatius Logistics</strong>
      </footer>