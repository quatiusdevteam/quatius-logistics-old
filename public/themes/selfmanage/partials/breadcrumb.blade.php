
<?php 
    $lastCrumb = null;
    $activeCrumb = Cache::remember('breadcrumb-'.Request::path(), 60, function () {
        $repoMenu = app('Litepie\Contracts\Menu\MenuRepository');
        $rootMenus = $repoMenu->rootMenues();
        $activeMenu = null;
        foreach ($rootMenus as $menu)
        {
            $submenu = $repoMenu->scopeQuery(function ($query) {
                return $query->orderBy('order', 'ASC');
            })->all()->toMenu($menu->key);
            foreach ($submenu as $menuchild)
            {
                if ($menuchild->active)
                {
                    $activeMenu = $menuchild;
                    break;
                }
            }
            if ($activeMenu)
                break;
        }
        
        return $activeMenu;
    });
?>
@if($activeCrumb)
	@include('public::breadcrumb.head', array('menu' => $activeCrumb))

@php
    $lastCrumb = Theme::breadcrumb()->getCrumbs()?array_slice(Theme::breadcrumb()->getCrumbs(), -1)[0]:null;
@endphp
<section class="content-header">
<h1>
    {!!$lastCrumb["label"]!!}
</h1>

<ol class="breadcrumb" id="area-breadcrumb">
    <?php Theme::breadcrumb()->setTemplate('
    <?php foreach ($crumbs as $i => $crumb):?>
        @if ($i != (count($crumbs) - 1))
        <li><a href="{{ $crumb["url"] }}">{!! $crumb["label"] !!}</a><span class="divider"></span></li>
        @else
        <li class="active">{!! $crumb["label"] !!}</li>
        @endif
    <?php endforeach;
    ');?>
    {!! Theme::breadcrumb()->render() !!}
</ol>

</section>
@endif