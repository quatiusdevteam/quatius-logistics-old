<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>{!! Theme::getTitle() !!}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>

    <link href="{{ asset('css/vendor_admin.css') }}" rel="stylesheet">

    @if(!env("APP_DEBUG"))
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-GBGJ2MQ2ET"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-GBGJ2MQ2ET');
        </script>
		
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-W5H5V3V');</script>
		<!-- End Google Tag Manager -->

    @endif

    <script>
        var baseUrl = "{{URL('/')}}";
    </script>

    {!! Theme::asset()->styles() !!}
    {!! Theme::asset()->scripts() !!}
    </head>
    <?php
        $contents = Theme::content();
        $hasWrapper = strpos($contents, ' class="content-wrapper"') !== false;
    ?>
    
    <body class="sidebar-mini skin-blue">
        @if(!env("APP_DEBUG"))
            <!-- Google Tag Manager (noscript) -->
            <noscript><iframe src=https://www.googletagmanager.com/ns.html?id=GTM-W5H5V3V
            height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
            <!-- End Google Tag Manager (noscript) -->

        @endif

        <div class="wrapper">
            {!! Theme::partial('header') !!}
            {!! Theme::partial('aside') !!}
            <div id="area-content" style="min-height: 110px">
                @if(!$hasWrapper)
                 <div class="content-wrapper">
                    {!! Theme::partial('breadcrumb') !!}
                    <div class="system-alert">
                        @include('flash::message')
                    </div>


                @endif
                    
            	@if(Theme::has('content-header'))
            	<div class="content-header">
            		{!!Theme::place('content-header')!!}
            	</div>
            	
            	@endif
            	{!! $contents !!}
            	
            	@if(Theme::has('content-footer'))
            	<div class="content-footer">
            		{!!Theme::place('content-footer')!!}
            	</div>
            	@endif
            	@if(!$hasWrapper)
                 </div>
                @endif
            </div>
            <div class="clearfix"></div>
            {!! Theme::partial('footer') !!}
        </div>
    </body>
{!! Theme::asset()->container('footer')->scripts() !!}
<script src="{{ asset('js/vendor_admin.js') }}"></script>
<script src="{{ asset('js/summernote.min.js') }}"></script>
{!! Theme::asset()->container('extra')->scripts() !!}
</html>
