-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 13, 2022 at 02:22 PM
-- Server version: 5.6.51
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `logcom_logistics`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_1` varchar(24) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `credit_limit` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `params` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `name`, `account_code`, `email`, `phone_1`, `credit_limit`, `params`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Wiegand, Bauch and Predovic', '50243', 'npadberg@hotmail.com', '(517) 580-5221', '0.0000', NULL, '2022-03-30 05:25:21', '2022-03-30 05:25:21', NULL),
(2, 'Quatius Logistics', '50001', 'info@quatiuslogistics.com', '0401333122', '5000.0000', NULL, '2022-04-14 06:25:21', '2022-04-14 06:25:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `link_id` int(10) UNSIGNED DEFAULT NULL,
  `link_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `nick` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_1` varchar(24) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_2` varchar(24) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_1` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_2` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'VIC',
  `country` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'AU',
  `postcode` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `link_id`, `link_type`, `address_type`, `nick`, `company`, `title`, `first_name`, `middle_name`, `last_name`, `email`, `phone_1`, `phone_2`, `address_1`, `address_2`, `city`, `state`, `country`, `postcode`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 2, 'Quatius\\Account\\Models\\Account', '', NULL, NULL, NULL, 'Song', NULL, 'Su', 'song.su@quatiuslogistics.com', '0458388386', NULL, '414 Lower Dandenong Road', NULL, 'Braeside', 'VIC', 'AU', '3195', NULL, '2022-05-08 18:12:25', '2022-05-08 18:12:25'),
(7, 2, 'Quatius\\Account\\Models\\Account', NULL, NULL, NULL, NULL, 'Jack', NULL, 'John', 'song.su@quatiuslogistics.com', '0458388386', NULL, '414 Lower Dandenong Road', NULL, 'Braeside', 'VIC', 'AU', '3195', NULL, '2022-05-11 15:25:47', '2022-05-11 15:25:47'),
(8, 2, 'Quatius\\Account\\Models\\Account', '', NULL, NULL, NULL, 'Anna', NULL, 'Uy', 'anna.uy@soniq.com', '01040615555', NULL, '19 FESTIVAL CRESCENT', NULL, 'Keysborough', 'VIC', 'AU', '3173', NULL, '2022-05-11 15:36:30', '2022-05-11 15:36:30'),
(9, 2, 'Quatius\\Account\\Models\\Account', '', NULL, NULL, NULL, 'Alice', NULL, 'LUO', 'Alice.Luo@quatiuslogistics.com', '0426937787', NULL, '414 Lower Dandenong Rd', NULL, 'Braeside', 'VIC', 'AU', '3195', NULL, '2022-05-11 15:43:39', '2022-05-11 15:43:39');

-- --------------------------------------------------------

--
-- Table structure for table `components`
--

CREATE TABLE `components` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `register_service` text COLLATE utf8mb4_unicode_ci,
  `editable_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `detachable_by` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'component.allow-detach-code',
  `published` tinyint(1) DEFAULT '1',
  `publish_start` datetime DEFAULT NULL,
  `publish_end` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `components`
--

INSERT INTO `components` (`id`, `type`, `name`, `data`, `register_service`, `editable_by`, `detachable_by`, `published`, `publish_start`, `publish_end`, `created_at`, `updated_at`) VALUES
(1, 'page-images', 'Page Image Viewer', '', NULL, '', 'superuser', 1, NULL, NULL, '2016-09-06 18:56:07', '2016-09-06 18:56:07'),
(2, 'custom-html', 'about breadcrumb', '{\"compiler\":\"html\",\"content\":\"<div class=\\\"row\\\">\\r\\n\\t<div class=\\\"breadcrumb-area about-us\\\">\\r\\n\\t\\t<div class=\\\"container\\\">\\t\\t\\t\\r\\n\\t\\t\\t<div class=\\\"col-md-12 breadcrumb-padding\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"breadcrumb-title\\\">\\r\\n\\t\\t\\t\\t\\t<h1>About Us<\\/h1>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t<div class=\\\"breadcrumb-list\\\">\\r\\n\\t\\t\\t\\t\\t<ul class=\\\"breadcrumb\\\">\\r\\n\\t\\t\\t\\t\\t\\t<li>You are here: <\\/li>\\r\\n\\t\\t\\t\\t\\t\\t<li><a href=\\\"home.html\\\">Home<\\/a><\\/li>\\t\\t\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t<li>About Us<\\/li>\\r\\n\\t\\t\\t\\t\\t<\\/ul>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<\\/div>\\t\\t\\t\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\"}', NULL, '', 'component.allow-detach', 1, NULL, NULL, '2016-09-06 19:24:33', '2016-11-17 13:19:38'),
(3, 'custom-html', 'service block', '{\"compiler\":\"html\",\"content\":\"<div class=\\\"row\\\">\\r\\n\\t<div class=\\\"services\\\">\\r\\n\\t\\t<div class=\\\"container\\\">\\r\\n\\t\\t\\t<div class=\\\"col-md-12 col-xs-12\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"row\\\">\\r\\n\\t\\t\\t\\t\\t<div class=\\\"col-md-3 col-sm-6 col-xs-12\\\">\\r\\n\\t\\t\\t\\t\\t\\t<div class=\\\"services-box ex01\\\" data-aos=\\\"fade-down\\\" data-aos-delay=\\\"100\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"freight\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<h2>Freight Management<\\/h2>\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<p>This is a nationwide door to door freight transport service to any metro and country destination throughout Australia. We can move cartons, pallets, and any other freight promptly and professionally via our network of providers.<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"readmore\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<a href=\\\"services.html#Freight_Management\\\"><button class=\\\"hvr-sweep-to-right btn btn-primary\\\">Read more<\\/button><\\/a>\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t<div class=\\\"col-md-3 col-sm-6 col-xs-12\\\">\\t\\r\\n\\t\\t\\t\\t\\t\\t<div class=\\\"services-box  ex02\\\" data-aos=\\\"fade-down\\\" data-aos-delay=\\\"300\\\">\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"warehouse\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<h2>Warehouse and Distribution<\\/h2>\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<p>Quatius Logistics has multiple warehouses throughout Australia which are integrated with our nationwide distribution network, giving us the ability to effectively ...<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"readmore\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<a href=\\\"services.html#Warehouse_Distribution\\\"><button class=\\\"hvr-sweep-to-right btn btn-primary\\\">Read more<\\/button><\\/a>\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t<div class=\\\"col-md-3 col-sm-6 col-xs-12\\\">\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t<div class=\\\"services-box ex03\\\" data-aos=\\\"fade-down\\\" data-aos-delay=\\\"900\\\">\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"consultation\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<h2>Consultation Services<\\/h2>\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<p>By utilising our expertise, we can also look for smarter ways to do things to save you time and money by incorporating our extensive network.<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"readmore\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<a href=\\\"services.html#Consulting_Services\\\"><button class=\\\"hvr-sweep-to-right btn btn-primary\\\">Read more<\\/button><\\/a>\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t<div class=\\\"col-md-3 col-sm-6 col-xs-12\\\">\\t\\r\\n\\t\\t\\t\\t\\t\\t<div class=\\\"services-box ex04\\\" data-aos=\\\"fade-down\\\" data-aos-delay=\\\"600\\\">\\t\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"b2b\\\">\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<h2>B2B Logistics in Asia<\\/h2>\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<p>Quatius Logistics offers a comprehensive solutions for express delivery, freight transport and supply chain requirements throughout Asia.<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"readmore\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<a href=\\\"services.html#Consolidation_Services\\\"><button class=\\\"hvr-sweep-to-right btn btn-primary\\\">Read more<\\/button><\\/a>\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\"}', NULL, NULL, 'component.allow-detach', 1, NULL, NULL, '2016-09-06 19:27:14', '2022-05-10 19:59:25'),
(8, 'custom-html', 'about us2', '{\"compiler\":\"html\",\"content\":\"<div class=\\\"row\\\">\\r\\n\\t<div class=\\\"counter-area fix\\\">\\r\\n\\t\\t<div class=\\\"container\\\">\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t<div class=\\\"col-xs-12 col-sm-4 col-md-4 col-lg-4\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"single-counter wow fadeInDown\\\" data-wow-duration=\\\"1.5s\\\" data-wow-delay=\\\".3s\\\">\\t\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t<h5>NUMBER AND DATES<\\/h5>\\r\\n\\t\\t\\t\\t\\t<h2>BASE ON FACTS AND FIGURE<\\/h2>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<\\/div>\\t\\t\\t\\t\\r\\n\\t\\t\\t<div class=\\\"col-xs-12 col-sm-4 col-md-4 col-lg-4\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"single-counter wow fadeInDown\\\" data-wow-duration=\\\"1.5s\\\" data-wow-delay=\\\".5s\\\">\\r\\n\\t\\t\\t\\t\\t<h1 class=\\\"counter\\\">1998<\\/h1>\\r\\n\\t\\t\\t\\t\\t<h3>FOUNDED<\\/h3>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<div class=\\\"col-xs-12 col-sm-4 col-md-4 col-lg-4\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"single-counter wow fadeInDown\\\" data-wow-duration=\\\"1.5s\\\" data-wow-delay=\\\".5s\\\">\\r\\n\\t\\t\\t\\t\\t<h1 class=\\\"counter\\\">10,998<\\/h1>\\r\\n\\t\\t\\t\\t\\t<h3>CUSTOMERS BOOKING<\\/h3>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<div class=\\\"clearfix\\\"><\\/div>\\r\\n\\t\\t\\t<div class=\\\"hidden-xs col-sm-4 col-md-4 col-lg-4\\\">\\r\\n\\t\\t\\t\\t<div>\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<div class=\\\"col-xs-12 col-sm-4 col-md-4 col-lg-4\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"single-counter wow fadeInDown\\\" data-wow-duration=\\\"1.5s\\\" data-wow-delay=\\\".5s\\\">\\r\\n\\t\\t\\t\\t\\t<h1 class=\\\"counter\\\">80,220<\\/h1>\\r\\n\\t\\t\\t\\t\\t<h3>PICKUP \\/ SENT<\\/h3>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<div class=\\\"col-xs-12 col-sm-4 col-md-4 col-lg-4\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"single-counter wow fadeInDown\\\" data-wow-duration=\\\"1.5s\\\" data-wow-delay=\\\".5s\\\">\\r\\n\\t\\t\\t\\t\\t<h1><span class=\\\"counter\\\">100<\\/span>%<\\/h1>\\r\\n\\t\\t\\t\\t\\t<h3>CUSTOMERS SATISFACTION<\\/h3>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<\\/div>\\t\\t\\t\\t\\t\\t\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\"}', NULL, '', 'component.allow-detach', 1, NULL, NULL, '2016-09-06 19:27:14', '2016-11-15 11:54:15'),
(9, 'custom-html', 'contact breadcrumb', '{\"compiler\":\"html\",\"content\":\"<div class=\\\"row\\\">\\r\\n\\t<div class=\\\"breadcrumb-area contact-us\\\">\\r\\n\\t\\t<div class=\\\"container\\\">\\t\\t\\t\\r\\n\\t\\t\\t<div class=\\\"col-md-12 breadcrumb-padding\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"breadcrumb-title\\\">\\r\\n\\t\\t\\t\\t\\t<h1>Contact Us<\\/h1>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t<div class=\\\"breadcrumb-list\\\">\\r\\n\\t\\t\\t\\t\\t<ul class=\\\"breadcrumb\\\">\\r\\n\\t\\t\\t\\t\\t\\t<li>You are here: <\\/li>\\r\\n\\t\\t\\t\\t\\t\\t<li><a href=\\\"home.html\\\">Home<\\/a><\\/li>\\t\\t\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t<li>Contact Us<\\/li>\\r\\n\\t\\t\\t\\t\\t<\\/ul>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<\\/div>\\t\\t\\t\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\"}', NULL, NULL, 'component.allow-detach', 1, NULL, NULL, '2016-09-06 20:56:30', '2022-05-05 17:37:07'),
(11, 'custom-html', 'service breadcrumb', '{\"compiler\":\"html\",\"content\":\"<div class=\\\"row\\\">\\r\\n\\t<div class=\\\"breadcrumb-area service\\\">\\r\\n\\t\\t<div class=\\\"container\\\">\\t\\t\\t\\r\\n\\t\\t\\t<div class=\\\"col-md-12 breadcrumb-padding\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"breadcrumb-title\\\">\\r\\n\\t\\t\\t\\t\\t<h1>Services<\\/h1>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t<div class=\\\"breadcrumb-list\\\">\\r\\n\\t\\t\\t\\t\\t<ul class=\\\"breadcrumb\\\">\\r\\n\\t\\t\\t\\t\\t\\t<li>You are here: <\\/li>\\r\\n\\t\\t\\t\\t\\t\\t<li><a href=\\\"home.html\\\">Home<\\/a><\\/li>\\t\\t\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t<li>Services<\\/li>\\r\\n\\t\\t\\t\\t\\t<\\/ul>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<\\/div>\\t\\t\\t\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\"}', NULL, '', 'component.allow-detach', 1, NULL, NULL, '2016-09-07 14:43:12', '2016-11-17 13:20:46'),
(12, 'custom-html', 'freight management', '{\"compiler\":\"html\",\"content\":\"<div class=\\\"service-dev-area first\\\" id=\\\"Freight_Management\\\">\\r\\n\\t<div class=\\\"container\\\">\\r\\n\\t\\t<div class=\\\"row\\\">\\r\\n\\t\\t\\t<div class=\\\"col-xs-12 col-sm-6 col-md-6\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"service-dev-img\\\">\\r\\n\\t\\t\\t\\t\\t<img src=\\\"images\\/freight-management-01.jpg\\\" alt=\\\"Freight Management & Logistics Specialist\\\" \\/>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<div class=\\\"col-xs-12 col-sm-6 col-md-6\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"service-dev-text\\\">\\r\\n\\t\\t\\t\\t\\t<h2>Freight Management & Logistics Specialist<\\/h2>\\r\\n\\t\\t\\t\\t\\t<p>Quatius Logistics is your one point of contact for all your transport needs.<\\/p>\\r\\n\\t\\t\\t\\t\\t<p>We deliver wherever your goods need to go, whether it be around town, or around the world through our extensive network. Our specialist team is here to improve your business and reduce your cost by integrating our customers\\u2019 supply chain efficiently. We have proven success with implementing carriers for various cargo goods.<\\/p>\\r\\n\\t\\t\\t\\t\\t<p>No matter what and where your goods need to go, Quatius Logistics can meet all your freight needs through our Total Logistics Solution that is customized specially for your requirements. Because of our creative and flexible solutions, no task is too small or big for Quatius Logistics.<\\/p>\\r\\n\\t\\t\\t\\t\\t<p>Please Contact Us today for quotes and more information.<\\/p>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\"}', NULL, '', 'component.allow-detach', 1, NULL, NULL, '2016-09-07 15:09:08', '2016-11-14 17:03:55'),
(13, 'custom-html', 'warehouse distribution', '{\"compiler\":\"html\",\"content\":\"<div class=\\\"service-dev-area wow fadeInLeft\\\" data-wow-duration=\\\"1.5s\\\" data-wow-delay=\\\".3s\\\" id=\\\"Warehouse_Distribution\\\">\\r\\n\\t<div class=\\\"container\\\">\\r\\n\\t\\t<div class=\\\"row\\\">\\r\\n\\t\\t\\t<div class=\\\"col-xs-12 col-sm-6 col-md-6 pull-right\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"service-dev-img\\\">\\r\\n\\t\\t\\t\\t\\t<img src=\\\"images\\/warehouse-distribution.jpg\\\" alt=\\\"Warehouse Distribution\\\" \\/>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<div class=\\\"col-xs-12 col-sm-6 col-md-6\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"service-dev-text\\\">\\r\\n\\t\\t\\t\\t\\t<h2>WAREHOUSING \\/ DISTRIBUTION<\\/h2>\\r\\n\\t\\t\\t\\t\\t<p>Quatius Logistics has multiple warehouses throughout Australia which are integrated with our nationwide distribution network, giving us the ability to effectively implement warehousing and distribution systems once your freight has arrived. We provide short or long term warehousing to store small, large and oversize freight, pick-and-pack, and distribution to local, country or interstate areas. Our comprehensive warehousing and distribution services include:<\\/p>\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t<span class=\\\"fa fa-check\\\"><span>\\r\\n\\t\\t\\t\\t\\t\\tSafe and Secure Warehousing across Australia\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t<span class=\\\"fa fa-check\\\"><span>\\r\\n\\t\\t\\t\\t\\t\\t3PL warehousing and distribution service\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t<span class=\\\"fa fa-check\\\"><span>\\r\\n\\t\\t\\t\\t\\t\\tContainer storage, Packing & Unpacking Services\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t<span class=\\\"fa fa-check\\\"><span>\\r\\n\\t\\t\\t\\t\\t\\tContainer and freight transport via air, sea and road transport\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\"}', NULL, '', 'component.allow-detach', 1, NULL, NULL, '2016-09-07 15:20:38', '2016-11-14 17:13:18'),
(14, 'custom-html', 'consultation services', '{\"compiler\":\"html\",\"content\":\"<div class=\\\"service-dev-area wow fadeInRight\\\" data-wow-duration=\\\"1.5s\\\" data-wow-delay=\\\".7s\\\" id=\\\"Consulting_Services\\\">\\r\\n\\t<div class=\\\"container\\\">\\r\\n\\t\\t<div class=\\\"row\\\">\\r\\n\\t\\t\\t<div class=\\\"col-xs-12 col-sm-6 col-md-6\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"service-dev-img\\\">\\r\\n\\t\\t\\t\\t\\t<img src=\\\"images\\/consultation-services.jpg\\\" alt=\\\"Consultation Services\\\" \\/>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<div class=\\\"col-xs-12 col-sm-6 col-md-6\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"service-dev-text\\\">\\r\\n\\t\\t\\t\\t\\t<h2>CONSULTATION SERVICES<\\/h2>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t<span class=\\\"service_title\\\">Supply-chain Advisory<\\/span>\\r\\n\\t\\t\\t\\t\\t\\tBy utilising our expertise, we can also look for smarter ways to do things to save you time and money by incorporating our extensive network.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t<span class=\\\"service_title\\\">Customs Brokerage and Clearance<\\/span>\\r\\n\\t\\t\\t\\t\\t\\tWith extensive experience in international trade export and import logistics, we offer clearance of goods through Customs and Quarantine barriers, handle the preparation of documents, calculate payments and facilitate all correspondence between yourself and the appropriate government authorities.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\"}', NULL, '', 'component.allow-detach', 1, NULL, NULL, '2016-09-07 15:31:35', '2016-11-14 16:45:20'),
(15, 'custom-html', 'b2b logistics', '{\"compiler\":\"html\",\"content\":\"<div class=\\\"service-dev-area last wow fadeInLeft\\\" data-wow-duration=\\\"1.5s\\\" data-wow-delay=\\\".9s\\\" id=\\\"Consolidation_Services\\\">\\r\\n\\t<div class=\\\"container\\\">\\r\\n\\t\\t<div class=\\\"row\\\">\\r\\n\\t\\t\\t<div class=\\\"col-xs-12 col-sm-6 col-md-6 pull-right\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"service-dev-img\\\">\\r\\n\\t\\t\\t\\t\\t<img src=\\\"images\\/b2b-logistics.jpg\\\" alt=\\\"B2B Logistics Asia\\\" \\/>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<div class=\\\"col-xs-12 col-sm-6 col-md-6\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"service-dev-text\\\">\\r\\n\\t\\t\\t\\t\\t<h2>B2B LOGISTICS IN ASIA<\\/h2>\\r\\n\\t\\t\\t\\t\\t<p>Quatius Logistics offers a comprehensive solutions for express delivery, freight transport and supply chain requirements throughout Asia. Drawing on our local infrastructure and expertise, backed by Quatius global group, we can provide customers operating in Asia with freight transport services via our logistics facilities across those regions. Quatius Logistics provide consolidation services to customers across a wide range of industries such as retail, manufacturing, FMCG, food and beverage. Because of the scale and volume of our freight business, we are able to generate more efficiency for our customers.<\\/p>\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tWith expert consolidators we\\u2019ll work with you to design and implement a cost-effective and efficient consolidation program that optimises the distribution of your goods. Our comprehensive suite of consolidation services includes:\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t<p>\\t\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t<span class=\\\"fa fa-check\\\"><\\/span>\\r\\n\\t\\t\\t\\t\\t\\t Receipt, dispatch\\r\\n\\t\\t\\t\\t\\t<\\/p>\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t<p>\\t\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t<span class=\\\"fa fa-check\\\"><\\/span>\\r\\n\\t\\t\\t\\t\\t\\t Cargo care\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t<p>\\t\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t<span class=\\\"fa fa-check\\\"><\\/span>\\r\\n\\t\\t\\t\\t\\t\\t Tallying\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t<p>\\t\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t<span class=\\\"fa fa-check\\\"><\\/span>\\r\\n\\t\\t\\t\\t\\t\\t Container packing\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t<p>\\t\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t<span class=\\\"fa fa-check\\\"><\\/span>\\r\\n\\t\\t\\t\\t\\t\\t Origin part load consolidation services\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t<p>\\t\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t<span class=\\\"fa fa-check\\\"><\\/span>\\r\\n\\t\\t\\t\\t\\t\\t Origin full load pick up to multiple part load delivery\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t<p>\\t\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t<span class=\\\"fa fa-check\\\"><\\/span>\\r\\n\\t\\t\\t\\t\\t\\t Operation of international consolidation centres for vendor and original equipment manufacturer (OEM) parts\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t<p>\\t\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t<span class=\\\"fa fa-check\\\"><\\/span>\\r\\n\\t\\t\\t\\t\\t\\t Customs clearance, documentation\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t<p>\\t\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t<span class=\\\"fa fa-check\\\"><\\/span>\\r\\n\\t\\t\\t\\t\\t\\t Storage and transport\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tWhether you are shipping domestically or abroad, we have the industry expertise, networks and global reach to provide a freight solution that works for you. Talk to us about how we can support you with transport and logistics services in Asia.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\"}', NULL, '', 'component.allow-detach', 1, NULL, NULL, '2016-09-07 15:39:02', '2016-11-14 17:17:13'),
(16, 'custom-html', 'home1', '{\"compiler\":\"html\",\"content\":\"<div class=\\\"row\\\">\\r\\n\\t<div class=\\\"breadcrumb-area home\\\">\\r\\n\\t\\t<div class=\\\"container\\\">\\t\\t\\t\\r\\n\\t\\t\\t<div class=\\\"col-md-12 breadcrumb-padding\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"col-md-6 col-sm-6 col-xs-12\\\">\\r\\n\\t\\t\\t\\t\\t<div class=\\\"row\\\">\\r\\n\\t\\t\\t\\t\\t\\t<h5 data-aos=\\\"fade-up\\\" data-aos-delay=\\\"400\\\">Welcome To<\\/h5>\\r\\n\\t\\t\\t\\t\\t\\t<h2 data-aos=\\\"fade-left\\\" data-aos-delay=\\\"800\\\">Quatius Logistics<\\/h2>\\r\\n\\t\\t\\t\\t\\t\\t<h6 data-aos=\\\"fade-right\\\" data-aos-delay=\\\"1100\\\">Your Total Freight Management and Logistics Solution<\\/h6>\\r\\n\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t<!--\\r\\n\\t\\t\\t\\t<div class=\\\"col-md-6 col-sm-6 col-xs-12 form-booking\\\">\\r\\n\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t<h3>Get a Quick Quote<h3>\\r\\n\\t\\t\\t\\t\\t\\t<h6>* for detail quote use extended version<\\/h6>\\r\\n\\t\\t\\t\\t\\t\\t<form class=\\\"form-horizontal\\\">\\t\\t\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"form-group\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<label for=\\\"inputEmail3\\\" class=\\\"col-sm-3 control-label\\\" style=\\\"text-align:left\\\">Truckload<\\/label>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<div class=\\\"col-sm-9\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t  <div class=\\\"btn-group\\\" style=\\\"width:100%\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t  <button style=\\\"width:100%\\\" class=\\\"btn btn-default btn-sm dropdown-toggle\\\" type=\\\"button\\\" data-toggle=\\\"dropdown\\\" aria-haspopup=\\\"true\\\" aria-expanded=\\\"false\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<Span class=\\\"pull-left\\\">Large button<\\/span><span class=\\\"fa fa-angle-down pull-right\\\" style=\\\"font-size:20px\\\"><\\/span>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t  <\\/button>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t  <ul class=\\\"dropdown-menu\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t...\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t  <\\/ul>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"form-group\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<label for=\\\"inputEmail3\\\" class=\\\"col-sm-3 control-label\\\" style=\\\"text-align:left\\\">Commodity<\\/label>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<div class=\\\"col-sm-9\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t  <div class=\\\"btn-group\\\" style=\\\"width:100%\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t  <button style=\\\"width:100%\\\" class=\\\"btn btn-default btn-sm dropdown-toggle\\\" type=\\\"button\\\" data-toggle=\\\"dropdown\\\" aria-haspopup=\\\"true\\\" aria-expanded=\\\"false\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<Span class=\\\"pull-left\\\">Large button<\\/span><span class=\\\"fa fa-angle-down pull-right\\\" style=\\\"font-size:20px\\\"><\\/span>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t  <\\/button>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t  <ul class=\\\"dropdown-menu\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t...\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t  <\\/ul>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"form-group\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<label for=\\\"inputEmail3\\\" class=\\\"col-sm-3 control-label\\\" style=\\\"text-align:left\\\">DIST (MILES)<\\/label>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<div class=\\\"col-sm-9\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<input type=\\\"range\\\" class=\\\"form-control\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"form-group\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<label for=\\\"inputEmail3\\\" class=\\\"col-sm-3 control-label\\\" style=\\\"text-align:left\\\">Refrigerated<\\/label>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<div class=\\\"col-sm-9\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<div class=\\\"radio\\\" style=\\\"padding-left:5px\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t  <div class=\\\"col-xs-6\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<input id=\\\"male\\\" type=\\\"radio\\\" name=\\\"gender\\\" value=\\\"male\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<label for=\\\"male\\\">Yes<\\/label>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t  <\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t  <div class=\\\"col-xs-6\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<input id=\\\"male\\\" type=\\\"radio\\\" name=\\\"gender\\\" value=\\\"male\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<label for=\\\"male\\\">No<\\/label>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t  <\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t<\\/form>\\r\\n\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t-->\\r\\n\\t\\t\\t<\\/div>\\t\\t\\t\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\"}', NULL, NULL, 'component.allow-detach', 1, NULL, NULL, '2016-09-07 17:33:05', '2022-05-11 13:03:11'),
(17, 'custom-html', 'about us1', '{\"compiler\":\"html\",\"content\":\"<div class=\\\"row\\\">\\r\\n\\t<div class=\\\"about-intensy-area\\\">\\r\\n\\t\\t<div class=\\\"container\\\">\\t\\t\\r\\n\\t\\t\\t<div class=\\\"col-md-12 col-xs-12\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"row\\\">\\r\\n\\t\\t\\t\\t\\t<div class=\\\"col=md-4 col-sm-4 col-xs-12\\\">\\r\\n\\t\\t\\t\\t\\t\\t<div class=\\\"single-features-1\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"features-icon\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<span class=\\\"custom_badge\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<i class=\\\"fa fa-thumbs-up\\\" style=\\\"width:100px;\\\"><\\/i>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<span>\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/span><\\/span><\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"features-info-text\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<h4>INDUSTRY LEADING LOGISTICS<\\/h4>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<p>Quatius have hand picked our providers specific to our customers\\u2019 requirements creating industry leading reliability and customer service levels.<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t<div class=\\\"col=md-4 col-sm-4 col-xs-12\\\">\\r\\n\\t\\t\\t\\t\\t\\t<div class=\\\"single-features-1\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"features-icon\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<span class=\\\"custom_badge\\\"><i class=\\\"fa fa-user-md\\\" style=\\\"width:100px;\\\"><\\/i><span>\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/span><\\/span><\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"features-info-text\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<h4>EXPERT TEAM<\\/h4>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<p>Our expert team of professionals derived from both operational and sales backgrounds across International,  Local trucking and 3PL skillsets combine to provide a complete end to end logistics experience. <\\/p>\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t<div class=\\\"col=md-4 col-sm-4 col-xs-12\\\">\\r\\n\\t\\t\\t\\t\\t\\t<div class=\\\"single-features-1\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"features-icon\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<span class=\\\"custom_badge\\\"><i class=\\\"fa fa-shield\\\" style=\\\"width:100px;\\\"><\\/i><span>\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/span><\\/span><\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"features-info-text\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<h4>ROCK-SOLID RELIABILITY<\\/h4>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<p>Quatius\\u2019 established  longevity in the Australasian marketplace ensure that a reliable, trustworthy service is extended to our customers, covering a vast range of commodities and services.<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<\\/div>\\t\\t\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\"}', NULL, NULL, 'component.allow-detach', 1, NULL, NULL, '2016-09-11 14:06:09', '2022-05-10 19:59:25'),
(18, 'custom-html', 'home2', '{\"compiler\":\"html\",\"content\":\"<div class=\\\"row\\\">\\r\\n\\t<div class=\\\"testimonial-area home-contact-area fix\\\">\\r\\n\\t\\t<div class=\\\"container\\\">\\r\\n\\t\\t\\t<div class=\\\"row\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"col-md-12 \\\">\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t<h6>Call Us at<\\/h6>\\r\\n\\t\\t\\t\\t\\t<h2>1300 899 299<\\/h2>\\r\\n\\t\\t\\t\\t\\t<h6>Complete Logistics and Storage Solutions<\\/h6>\\r\\n\\t\\t\\t\\t\\t<button class=\\\"hvr-sweep-to-right btn btn-primary\\\" onclick=\\\"location.href=\'contact-us.html\';\\\">CONTACT US<\\/button>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\"}', NULL, NULL, 'component.allow-detach', 1, NULL, NULL, '2016-09-11 14:58:13', '2022-05-10 19:59:25'),
(19, 'custom-html', 'feature block', '{\"compiler\":\"blade\",\"content\":\"<div class=\\\"row\\\">\\r\\n\\t<div class=\\\"feature-bottom\\\">\\r\\n\\t\\t<div class=\\\"container\\\">\\r\\n\\t\\t\\t<div class=\\\"row\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"col-md-8 col-xs-12\\\" data-aos=\\\"fade-right\\\">\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t<center>\\r\\n\\t\\t\\t\\t\\t\\t<h2>YOUR FREIGHT MANAGEMENT &amp; LOGISTIC SPECIALIST<\\/h2>\\r\\n\\t\\t\\t\\t\\t\\t<p>Quatius Logistics is your one point of contact for all your transport needs.<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t<p>We deliver wherever your goods need to go, whether it be around town, or around the world through our extensive network. Our specialist team is here to improve your business and reduce your cost by integrating our customers\\u2019 supply chain efficiently. We have proven success with implementing carriers for various cargo goods.<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t<p>No matter what and where your goods need to go, Quatius Logistics can meet all your freight needs through our Total Logistics Solution that is customized specially for your requirements. Because of our creative and flexible solutions, no task is too small or big for Quatius Logistics.<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t<p>Please <a href=\\\"contact-us.html\\\">Contact Us<\\/a> today for quotes and more information.<\\/p>\\r\\n\\t\\t\\t\\t\\t<\\/center>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t<div class=\\\"col-md-4 col-xs-12\\\">\\r\\n\\t\\t\\t\\t\\t<img data-aos=\\\"fade-left\\\" src=\\\"{{asset(\'images\\/freight.jpg\')}}\\\">\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\"}', NULL, NULL, 'component.allow-detach', 1, NULL, NULL, '2016-11-08 19:15:20', '2022-05-11 23:51:27'),
(21, 'custom-html', 'about us3', '{\"compiler\":\"html\",\"content\":\"<div class=\\\"row\\\">\\r\\n\\t<div class=\\\"about-intensy-area\\\">\\r\\n\\t\\t<div class=\\\"container\\\">\\r\\n\\t\\t\\t<div class=\\\"col-md-12 col-xs-12\\\">\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t<div class=\\\"col-md-8 col-sm-8 col-xs-12\\\">\\r\\n\\t\\t\\t\\t\\t\\t<h4><strong>WHO WE ARE<\\/strong><\\/h4>\\r\\n\\t\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t\\tQuatius Logistics is part of the Quatius Global Group, which is an integrated manufacturer, importer and exporter of Audiovisual, food and beverages, beauty and healthcare products. Because of our global presence, established networks and great buying power, we can efficiently manage the transportation of products for the retail, commercial and industrial sectors at cost effective prices.\\r\\n\\t\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t\\tBy utilising our experience knowledge in the industry, we can provide customized solutions for our clients with different needs while offering excellent customer service. Reliability and providing Quality Reassurance are our core business values. This is why Quatius Logistics is the freight choice of many Australian companies, large or small, and we are proud of it.\\r\\n\\t\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t<div class=\\\"col-md-4 col-sm-4 col-xs-12\\\">\\r\\n\\t\\t\\t\\t\\t\\t<img src=\\\"images\\/office1.jpg\\\" class=\\\"img-responsive\\\">\\t\\t\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<div class=\\\"clearfix\\\"><\\/div>\\r\\n\\t\\t\\t<br><br>\\r\\n\\t\\t\\t<div class=\\\"col-md-12 col-xs-12\\\">\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t<div class=\\\"col-md-8 col-sm-8 col-xs-12\\\">\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t<h4><strong>WHY CHOOSE QUATIUS LOGISTICS?<\\/strong><\\/h4>\\r\\n\\t\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t\\t<strong>1. Focus on improving clients\\u2019 businesses and provide the best economical freight rates<\\/strong><br>\\r\\n\\t\\t\\t\\t\\t\\t\\tWe source the best transport option for your business at the most competitive prices. From our comprehensive network of over 30 providers in Australia, our clients can rest assured that they are receiving the best, customised solution for their different businesses requirements.\\r\\n\\t\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t\\t<strong>2. Excellent Customer Services and Quality Assurance<\\/strong><br>\\r\\n\\t\\t\\t\\t\\t\\t\\tA well resourced customer service team that looks after clients with a personal approach to solve your logistics needs. Our commitment is to deliver freight consignments on time and damage-free and to ensure our transport system performs and maintains its quality.\\r\\n\\t\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t\\t<strong>3. Proven Success in Managing the Transportation of Different Types of Goods<\\/strong><br>\\r\\n\\t\\t\\t\\t\\t\\t\\tAs a long time exporter of fragile glass packaged products such as wine and food related products through the Asian region and the importer of larger Audio\\/ visual components such as TVs, with warehousing solutions to satisfy the Quatius Global range of products, Quatius Logistics believes its proven model will provide a successful model for our clients.\\r\\n\\t\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t<div class=\\\"col-md-4 col-sm-4 col-xs-12\\\">\\r\\n\\t\\t\\t\\t\\t\\t<img src=\\\"images\\/warehouse1.jpg\\\" class=\\\"img-responsive\\\">\\t\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\"}', NULL, '', 'component.allow-detach', 1, NULL, NULL, '2016-11-14 16:28:39', '2016-11-15 11:54:15'),
(22, 'custom-html', 'About client block', '{\"compiler\":\"html\",\"content\":\"<div class=\\\"row\\\">\\r\\n\\t<div class=\\\"client-block fix\\\">\\r\\n\\t\\t<div class=\\\"container\\\">\\r\\n\\t\\t\\t<div class=\\\"row\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"col-xs-12 \\\">\\r\\n\\t\\t\\t\\t\\t<div class=\\\"owl-carousel\\\">\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t          <div class=\\\"owl-carousel--item\\\">\\r\\n\\t\\t\\t\\t            <img src=\\\"images\\/clients\\/soniq-logo.png\\\">\\r\\n\\t\\t\\t\\t          <\\/div>\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t          <div class=\\\"owl-carousel--item\\\">\\r\\n\\t\\t\\t\\t            <img src=\\\"images\\/clients\\/klass-logo.png\\\">\\r\\n\\t\\t\\t\\t          <\\/div>\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t          <div class=\\\"owl-carousel--item\\\">\\r\\n\\t\\t\\t\\t            <img src=\\\"images\\/clients\\/jb-logo.png\\\">\\r\\n\\t\\t\\t\\t          <\\/div>\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t          <div class=\\\"owl-carousel--item\\\">\\r\\n\\t\\t\\t\\t            <img src=\\\"images\\/clients\\/scholl-logo.png\\\">\\r\\n\\t\\t\\t\\t          <\\/div>  \\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t          <div class=\\\"owl-carousel--item\\\">\\r\\n\\t\\t\\t\\t            <img src=\\\"images\\/clients\\/fst-logo.png\\\">\\r\\n\\t\\t\\t\\t          <\\/div>\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t          <div class=\\\"owl-carousel--item\\\">\\r\\n\\t\\t\\t\\t            <img src=\\\"images\\/clients\\/alldocks-logo.png\\\">\\r\\n\\t\\t\\t\\t          <\\/div>  \\t\\r\\n      \\t\\t\\t\\t<\\/div>\\t\\r\\n                <\\/div>\\r\\n            <\\/div>\\r\\n        <\\/div>    \\r\\n    <\\/div>\\r\\n<\\/div>\\r\\n\\r\\n\\r\\n\\r\\n<script src=\\\"https:\\/\\/dl.dropboxusercontent.com\\/u\\/1499732\\/owl-carousel\\/owl.carousel.min.js\\\"><\\/script>\\r\\n\\r\\n<script>\\r\\nvar owl = $(\'.owl-carousel\');\\r\\nowl.owlCarousel({\\r\\n    items: 6,\\r\\n    margin: 10,\\r\\n    loop: true,\\r\\n    center: true,\\r\\n    mouseDrag: true,\\r\\n    touchDrag: true,\\r\\n    nav: false,\\r\\n    dots: false,\\r\\n    lazyLoad: true,\\r\\n    autoplay: true,\\r\\n    autoplayTimeout: 5000,\\r\\n    autoplayHoverPause: true,\\r\\n    autoWidth: true\\r\\n});\\r\\n$(document).keydown(function (e) {\\r\\n    if (e.keyCode === 37) {\\r\\n        owl.trigger(\'prev.owl.carousel\');\\r\\n    } else if (e.keyCode === 39) {\\r\\n        owl.trigger(\'next.owl.carousel\');\\r\\n    }\\r\\n});\\r\\n\\/\\/# sourceURL=pen.js\\r\\n<\\/script>\\r\\n\"}', NULL, '', 'component.allow-detach', 1, NULL, NULL, '2016-11-15 11:54:15', '2017-02-23 18:38:20'),
(23, 'custom-html', 'terms & conditions breadcrum', '{\"compiler\":\"html\",\"content\":\"<div class=\\\"row\\\">\\r\\n\\t<div class=\\\"breadcrumb-area policy\\\">\\r\\n\\t\\t<div class=\\\"container\\\">\\t\\t\\t\\r\\n\\t\\t\\t<div class=\\\"col-md-12 breadcrumb-padding\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"breadcrumb-title\\\">\\r\\n\\t\\t\\t\\t\\t<h1>Terms and Conditions<\\/h1>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t<div class=\\\"breadcrumb-list\\\">\\r\\n\\t\\t\\t\\t\\t<ul class=\\\"breadcrumb\\\">\\r\\n\\t\\t\\t\\t\\t\\t<li>You are here: <\\/li>\\r\\n\\t\\t\\t\\t\\t\\t<li><a href=\\\"home.html\\\">Home<\\/a><\\/li>\\t\\t\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t<li>Terms and Conditions<\\/li>\\r\\n\\t\\t\\t\\t\\t<\\/ul>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<\\/div>\\t\\t\\t\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\"}', NULL, '', 'component.allow-detach', 1, NULL, NULL, '2018-06-04 16:18:06', '2018-06-04 16:18:06');
INSERT INTO `components` (`id`, `type`, `name`, `data`, `register_service`, `editable_by`, `detachable_by`, `published`, `publish_start`, `publish_end`, `created_at`, `updated_at`) VALUES
(24, 'custom-html', 'terms and condition', '{\"compiler\":\"html\",\"content\":\"<style>\\r\\n\\t#termsconditions p{\\r\\n\\t\\ttext-align: justify;\\r\\n\\t\\tpadding-bottom: 15px;\\r\\n\\t}\\r\\n<\\/style>\\r\\n<div class=\\\"service-dev-area first\\\" id=\\\"termsconditions\\\">\\r\\n\\t<div class=\\\"container\\\">\\t\\t\\t\\r\\n\\t\\t<div class=\\\"row\\\">\\r\\n\\t\\t\\t<p>\\r\\n\\t\\t\\t\\tThese terms and conditions must be read having regard to the provisions of the Australian Consumer Law (set out in Schedule 2 of the Competition and Consumer Act 2010) to the extent that those provisions are applicable to consumers as defined under Section 3 of that Schedule 2. These terms and conditions do not have the effect of excluding, restricting or modifying rights under the Australian Consumer Law which cannot be excluded, restricted or modified by agreement.\\r\\n\\t\\t\\t<\\/p>\\t\\t\\t\\r\\n\\t\\t\\t<ol type=\\\"1\\\">\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<ol type=\\\"a\\\">\\r\\n\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\tAll and any business undertaken by Quatius Logistics Pty Ltd (hereinafter called \\u201cthe Company\\u201d) is transacted subject to these conditions each of which shall be deemed to be incorporated in and to be a condition of any agreement between the Company and its customers. The Company only deals with goods subject to these conditions. The Company is not a common carrier and shall accept no liability as such;\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\tThe Company in its sole and absolute discretion may refuse to deal with any goods without assigning any reason therefore.\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t\\t<\\/ol>\\t\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tAny instructions given to the Company may in the absolute discretion of the Company be complied with by the Company as agent for the customer as disclosed principal or by the Company as principal contractor by its own servants performing part or all of the relevant services or by the Company employing or instructing or entrusting the goods to others on such other conditions as they may stipulate to perform part or all of the services. The customer shall be bound by such other conditions and shall release the Company from liability and indemnify the Company against any claims arising out of their acceptance.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tCustomers entering into transactions of any kind with the Company expressly warrant that they are either the owners or the authorised agents of the owners of any and all goods or property the subject matter of the transaction. By entering into the transaction they accept these conditions for themselves and for all other parties on whose behalf they are acting and they warrant that they have authority so to do.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tSubject to express instructions in writing given by the customer and by the Company, the Company reserves to itself complete freedom of choice of means route and procedure to be followed in the handling and transportation of goods. If in the Company\\u2019s opinion it is necessary or desirable in the customer\\u2019s interests to depart from any express instructions, the Company shall be at liberty to do so.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tExcept where the Company is instructed in writing to pack the goods the customer warrants that all goods have been properly and sufficiently packed and\\/or prepared.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tThe Company is entitled to retain and be paid all brokerages, commissions, allowances and other remunerations retained by or paid to Ship Forwarding Agents (or Freight Forwarders) and Insurance Brokers.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tQuotations are given on the basis of immediate acceptance and subject to the right of      withdrawal before acceptance and revision after acceptance. If any changes occur in the rates of customs duty, freight, warehousing, insurance premiums or other charges applicable to the goods, quotations and charges shall be subject to revision accordingly with or without notice.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tThe customer, and the senders, owners and consignees of any goods and their agents, if any, shall be deemed to be bound by and to warrant the accuracy of all descriptions, values, dimensions, weights and other particulars furnished to the Company for customs, consular, road transport and other purposes and shall jointly and severally indemnify the Company against all losses, damages, expenses and fines arising from any inaccuracy or omission, even if such inaccuracy or omission is not due to any negligence, willful act or omission.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tThe customer and the senders, owners and consignees and their agents, if any, shall be jointly and severally liable for any duty, tax, impost, excise, levy, penalty, deposit or outlay of whatsoever nature levied by any Government or the authorities at any port or place in connection with the goods and for any payments, fines, expenses, loss or damage incurred or sustained by the Company in connection therewith and shall indemnify the Company, its servants and agents from all claims by third parties howsoever arising in connection with the goods.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tWhen goods are accepted or dealt with upon instructions to collect freight, duties, charges or other expenses from the consignee or any other person the customer shall remain responsible for the same if they are not paid by such consignee or other person.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tThe customer shall be responsible for the timely return of any container to the person who owns or has a right to possession of the container in a clean and undamaged condition.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tThe customer agrees to indemnify the Company against any claim, liability or expense, including detention or demurrage charges, which arise as a result of : a failure to return the container, or a delay in the return of the container beyond the customary period allowed for container returns, or any damage to the container, or the container being returned in a dirty or contaminated condition, regardless of who failed or delayed in the return of the container or where or by whom the container was damaged, made dirty or contaminated.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tThe customer shall indemnify the Company against any claim, liability or expense which arises as a result of delay in loading or unloading of the customer\\u2019s goods, or any waiting time, detention or demurrage for any truck or any other conveyance whatsoever.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<ol type=\\\"a\\\">\\r\\n\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\tThe Company\\u2019s charges to the customer including freight shall be deemed fully earned on receipt of the goods by the Company and shall be paid and non-returnable in any event and whether goods are lost or not lost or a voyage or flight is broken up or abandoned. If there shall be a forced interruption or abandonment of a voyage or flight at the port or airport of shipment or elsewhere, any forwarding of the goods or any part thereof shall be at the risk and expense of the customer and of the sender, owner and consignee.\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\tAll unpaid charges shall be paid in full and without any offset, counterclaim or deduction, in the currency of the place of receipt of the goods or at the Company\\u2019s option, in the currency of the place of delivery at the TT selling rate in effect on the day of payment. If the date determined above falls on a day which banks are closed for business, the rate ruling on the next succeeding business day shall govern.\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\tThe Company\\u2019s charges including freight have been calculated on the basis of particulars furnished by, or on behalf of the customer. The Company may at any time open any container or any other package or unit in order to re-weigh, re-measure or re-value the contents and if the particulars furnished by or on behalf of the customer are incorrect, it is agreed that a sum equal to either five times the difference between the correct freight and the freight charged, or double the correct freight less the freight charged, whichever sum is smaller, shall be payable as liquidated damages to the Company.\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t<\\/li>\\t\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t<\\/ol>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tNo insurance will be effected except upon express instructions as to the risks to be insured against and the value or values to be declared in writing by the customer and all insurances effected by the Company subject to the usual exceptions and conditions of the policies of the insurance company or underwriters accepting the risk. The Company shall not be under any obligation to effect a separate insurance on each consignment but may declare it on any open or general policy. Should the insurers dispute their liability for any reason the insured shall have recourse against the insurers only and the Company shall not be under any responsibility or liability in relation thereto, notwithstanding that the premium upon the policy may not be at the same rate as that charged by the Company or paid to the Company by its customer. \\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tThe Company shall not be liable for any loss, damage, cost, expense, penalty or fine suffered by the customer or any other person, howsoever caused or arising, whether: \\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t<ol type=\\\"a\\\">\\r\\n\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\tFor loss of or damage to goods unless such loss or damage occurs whilst the goods are in the actual custody of the Company and under its actual control and unless such loss or damage is due to the willful neglect or default of the Company or its own servants;\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\tFor any delay in delivery, forwarding or transit or failure to deliver goods, any deterioration, contamination, evaporation or any consequential loss or loss of market however caused;\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\tFor failure to follow instructions given to it by or on behalf of the customer whether or not such failure is willful or negligent;\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\tIn any way connected with marks, numbers, brands, contents, quality, value, weight, dimensions or description of any goods;\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\tFor loss or damage resulting from fire, water, explosion or theft whether caused by negligence of the Company\\u2019s servants or otherwise;\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t\\t<\\/ol>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<ol type=\\\"a\\\">\\r\\n\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\tThe Company shall not be liable under any circumstances for any claim, cost, expense, loss or damage, penalty or fine resulting from or attributable to any quotation, statement, representation or information whether oral or in writing howsoever, where so ever or to whomsoever made or given by or on behalf of the Company or by any servant, employee or agent of the Company as to the classification of, the liability for or the amount, scale or rate of customs and\\/or excise duty or other impost, tax or rate applicable to any goods or property whatsoever.\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\tThe Company does not accept responsibility or liability in relation to any decision taken or liability incurred on the basis of any such quotation, statement, representation or information.\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t\\t<\\/ol>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tUnless a Convention or law limiting the Company\\u2019s liability to a greater amount compulsorily applies, liability of the Company arising out of any one incident whether or not there has been any declaration of value of the goods, for breach of warranty implied into these terms and conditions by the Australian Consumer Law or howsoever arising, is limited to any of the following as determined by the Company:\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t<ol type=\\\"a\\\">\\r\\n\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\tThe supplying of the services again; or\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\tThe payment of the cost of having the services supplied again; or\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\tThe lesser of A$200.00 for loss of or damage to any such goods, packages or units or A$2.00 per kilogram of the gross weight for loss of or damage to any such goods, packages or units or A$20.00 per package or unit lost or damaged.\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\tFor the purposes of this clause the word \\u201cpackage\\u201d shall include the contents within that \\u201cpackage\\u201d for the purpose of calculating any limitation of liability, even if separate particulars have been provided or incorporated in any document of the Company.\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t\\t<\\/ol>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tThe Company shall be discharged of all liability unless suit is brought in the proper forum and written notice thereof received by the Company within six months after delivery of the goods or the date when the goods should have been delivered. In the event that the said time period shall be found contrary to any Convention or law compulsorily applicable the period prescribed by such Convention or law shall then apply but in that circumstance only.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<ol type=\\\"a\\\">\\r\\n\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\tIn the case of carriage by sea, the value will not be declared or inserted in the Bill of Lading for the purpose of extending the Ship owners\\u2019 liability under  the Carriage of Goods by Sea Act 1991 except upon express instructions given in writing by the customer.\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\tIn the case of carriage by air, no optional declaration of value to increase the Air Carrier\\u2019s liability under the Civil Aviation (Carrier\\u2019s Liability) Act 1959 will be made except upon express instructions given in writing by the customer;\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\tIn all other cases where there is a choice of tariff rates according to the extent of the liability assumed by carriers, warehousemen or others no declaration of value (where optional) will be made for the purposes of extending liability and goods will be forwarded or dealt on the basis of minimum charges unless express instructions in writing to the contrary are given by the customer.\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t\\t<\\/ol>\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tInstructions to collect payment on delivery (COD) in cash or otherwise are accepted by the Company upon the condition that the Company in the matter of such collection will be liable for the exercise of due care and skill only.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tPerishable goods, which are not taken up immediately upon arrival or which are insufficiently addressed or marked or otherwise not identifiable may be sold or otherwise disposed of without any notice to the customer and payment or tender of the net proceeds of any sale after deduction of charges shall be equivalent to delivery. All charges and expenses arising in connection with the sale or disposal of the goods shall be paid by the customer.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tNon-perishable goods which cannot be delivered either because they are insufficiently or incorrectly addressed or because they are not collected or accepted by the consignee may be sold or returned at the Company\\u2019s option at any time after the expiration of 21 days from a notice in writing sent to the address which the customer gave to the Company on delivery of the goods. All charges and expenses arising in connection with the sale or return of the goods shall be paid by the customer. A communication from any agent or correspondent of the Company to the effect that the goods cannot be delivered for any reason shall be conclusive evidence of that fact.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tExcept under special arrangements previously made in writing the Company will not accept or deal with any noxious, dangerous, hazardous or inflammable or explosive goods or any goods likely to cause damage. Any person delivering such goods to the Company or causing the Company to handle or deal with any such goods shall be liable for all loss or damage caused thereby and shall indemnify the Company against all penalties, claims, damages, costs and expenses arising in connection therewith and the goods may be destroyed or otherwise dealt with at the sole discretion of the Company or any other person in whose custody they may be at the relevant time. If such goods are accepted under arrangements previously made in writing they may nevertheless be so destroyed or otherwise dealt with if they become dangerous to other goods or property. The expression \\u201cgoods likely to cause damage\\u201d includes goods likely to harbor or encourage vermin or other pests.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tExcept under special arrangements previously made in writing the Company will not accept bullion, coins, precious stones, jewellery, valuables, antiques, pictures, livestock or plants and the Company will accept no liability whatsoever for any such goods.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tPending forwarding and delivery goods may be warehoused or otherwise held at any place or places at the sole discretion of the Company at the customer\\u2019s or owner\\u2019s risk and expense.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tAll goods and documents relating to goods shall be subject to a particular and general lien for monies due either in respect of such goods or any particular or general balance of other monies due from the customer, the sender, owner or consignee to the Company. If any monies due to the Company are not paid within one calendar month after notice has been given to the person from whom the monies are due that such goods are detained, they may be sold by auction or otherwise at the sole discretion of the Company and at the expense of such person and the proceeds applied in or towards satisfaction of such particular and general lien. The customer agrees and acknowledges the Company is entitled in its discretion to register its particular and general lien as a security interest on the register established under the Personal Property Securities Act 2009 (Cth) and the Company has provided consideration for that security interest, by delivery of its promises under this agreement.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<ol type=\\\"a\\\">\\r\\n\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\tBy entering into any agreement to which these conditions apply, the customer on its own behalf and as agent of the owner, sender and consignee agrees and further offers to exclude or limit the liability of all servants, employees and agents of the Company and all subcontractors (including servants, employees and agents of the subcontractors) in respect of the goods and subject to the agreement to the extent that each such subcontractor, servant, employee and agent shall be protected by and entitled to the full benefit of all provisions in these conditions excluding or restricting tortious liability of any kind;\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\tThe offer hereinbefore referred to shall be accepted by the act of each such subcontractors, servant employee or agent in performing any function in relation to or affecting the goods the subject of the agreement;\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\tFor the purposes of the foregoing provisions of this clause the Company is and shall be deemed to be acting as agent on behalf of and trustee for the benefit of all persons who are or become its subcontractors, servants, employees or agents from time to time and all such persons shall to this extent be and be deemed to be parties to the agreement concerned.\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\tThe customer undertakes that no claim or allegation shall be made by the customer or any other person in respect of the goods or the services which imposes or attempts to impose any liability whatsoever and howsoever arising (including negligence) against any servant, employee and agent or any subcontractor (including servants, employees and agents of the subcontractor).If such a claim should nevertheless be made, the customer shall indemnify the Company and the person against whom the claim is made against the consequences of such claim or allegation.\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t\\t<\\/ol>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tIn addition to and without prejudice to the foregoing the customer undertakes that it shall in any event indemnify the Company against all loss, damage, liability, claim, cost, expense, penalty or fine suffered or incurred by the Company arising directly or indirectly from or in connection with the customer\\u2019s instructions or their implementation or the goods including containers.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tWithout prejudice to any other condition, the Company shall have the right to enforce any liability of the customer under these conditions or to recover any sums to be paid by the customer under these conditions not only against or from the customer but also if it thinks it against or from the sender and\\/or owner and\\/or consignee of the goods.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tThe use of a customer\\u2019s own form shall in no way derogate from these conditions the whole of which shall, notwithstanding anything contained in any such form, constitute terms of the agreement so entered into. Any provision in any such form which is contrary to any provision of these conditions shall to the extent of such inconsistency be inapplicable.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tThe goods shall be deemed to have been delivered as described unless notice of loss or of damage to the goods indicating the general nature of such loss or damage shall have been given in writing to the Company or to its representative at the place of delivery before or at the time of removal of the goods by a representative of the person entitled to delivery thereof or if the loss or damage be not apparent within three consecutive days thereafter.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tNo agent or employee of the Company has the Company\\u2019s authority to alter or vary these conditions.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tAll the rights, immunities and exemptions from liability in these terms and conditions shall continue to have their full force and affect in all circumstances and not withstanding any breach of this contract or of any of these terms and conditions by the Company or any other person entitled to the benefit of such provisions and irrespective of whether such may constitute a fundamental breach of contract or a breach of a fundamental term.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t<p>\\r\\n\\t\\t\\t\\t\\t\\tThese conditions shall be governed by and construed in accordance with the laws of the State or Territory in which this contract was made.\\r\\n\\t\\t\\t\\t\\t<\\/p>\\r\\n\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t<\\/ol>\\t\\t\\t\\r\\n\\t\\t<\\/div>\\r\\n\\t\\t<div class=\\\"clearfix\\\"><br \\/><\\/div>\\r\\n\\t\\t<div class=\\\"row\\\">\\r\\n\\t\\t\\t<div class=\\\"pull-right\\\">Last updated: Tuesday, June 05, 2018<\\/div> \\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\\r\\n\"}', NULL, '', 'component.allow-detach', 1, NULL, NULL, '2018-06-04 16:18:06', '2018-06-04 16:18:06'),
(27, 'custom-html', 'career breadcrumb', '{\"compiler\":\"blade\",\"content\":\"<div class=\\\"row\\\">\\r\\n\\t<div class=\\\"breadcrumb-area\\\" style=\\\"background: rgba(0, 0, 0, 0) url(https:\\/\\/quatiuslogistics.com\\/medias\\/pages\\/career\\/Main-Banner.png) no-repeat scroll center center \\/ cover; padding-top: 83px;\\\">\\r\\n\\t\\t<div class=\\\"container\\\">\\t\\t\\t\\r\\n\\t\\t\\t<div class=\\\"col-md-12 breadcrumb-padding\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"breadcrumb-title\\\">\\r\\n\\t\\t\\t\\t\\t<h1>Career<\\/h1>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t<div class=\\\"breadcrumb-list\\\">\\r\\n\\t\\t\\t\\t\\t<ul class=\\\"breadcrumb\\\">\\r\\n\\t\\t\\t\\t\\t\\t<li>You are here: <\\/li>\\r\\n\\t\\t\\t\\t\\t\\t<li><a href=\\\"home.html\\\">Home<\\/a><\\/li>\\t\\t\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t<li>Career<\\/li>\\r\\n\\t\\t\\t\\t\\t<\\/ul>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<\\/div>\\t\\t\\t\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\"}', NULL, NULL, 'component.allow-detach', 1, NULL, NULL, '2022-05-03 20:05:35', '2022-05-05 13:29:24'),
(28, 'custom-html', 'Text Block', '{\"compiler\":\"blade\",\"content\":\"<style>\\r\\n  .job-short{\\r\\n   border: 1px solid #777;\\r\\n    border-radius: 5px;\\r\\n    margin: 30px 0;\\r\\n    padding: 15px;\\r\\n  }\\r\\n  .job-short .job-title{\\r\\n    font-size: 24px;\\r\\n  }\\r\\n  .job-short .job-location{\\r\\n    font-size: 13px;\\r\\n    color: blue;\\r\\n    padding-top: 10px;\\r\\n  }\\r\\n<\\/style>\\r\\n<div class=\\\"row\\\">\\r\\n\\t<div class=\\\"feature-bottom\\\">\\r\\n\\t\\t<div class=\\\"container\\\">\\r\\n          \\t<h3 style=\\\"text-align:center;\\\">1 job available<\\/h3>\\r\\n\\t\\t\\t<div class=\\\"row job-short\\\">              \\r\\n              <div class=\\\"col-sm-10\\\">\\r\\n                <div class=\\\"job-title\\\">National &amp; Group Co-ordinator<\\/div>\\r\\n                <div class=\\\"job-location\\\"><i class=\\\"fa fa-map-marker\\\"><\\/i> Melbourne, AUSTRALIA<\\/div>\\r\\n              <\\/div>\\r\\n              <div class=\\\"col-sm-2\\\"><button class=\\\"hvr-sweep-to-right btn btn-primary  pull-right\\\" onclick=\\\"location.href=\'national-group-coordinator\';\\\">VIEW ROLE<\\/button><\\/div>\\r\\n          <\\/div>\\r\\n          <!--<div class=\\\"row job-short\\\">              \\r\\n              <div class=\\\"col-sm-10\\\">\\r\\n                <div class=\\\"job-title\\\">Web senior discovery consultant<\\/div>\\r\\n                <div class=\\\"job-location\\\"><i class=\\\"fa fa-map-marker\\\"><\\/i> Melbourne, AUSTRALIA<\\/div>\\r\\n              <\\/div>\\r\\n              <div class=\\\"col-sm-2\\\"><button class=\\\"hvr-sweep-to-right btn btn-primary  pull-right\\\" onclick=\\\"location.href=\'career-senior-web-consultant\';\\\">VIEW ROLE<\\/button><\\/div>\\r\\n          <\\/div>\\r\\n          <div class=\\\"row job-short\\\">              \\r\\n              <div class=\\\"col-sm-10\\\">\\r\\n                <div class=\\\"job-title\\\">Web senior discovery consultant<\\/div>\\r\\n                <div class=\\\"job-location\\\"><i class=\\\"fa fa-map-marker\\\"><\\/i> Melbourne, AUSTRALIA<\\/div>\\r\\n              <\\/div>\\r\\n              <div class=\\\"col-sm-2\\\"><button class=\\\"hvr-sweep-to-right btn btn-primary  pull-right\\\" onclick=\\\"location.href=\'career-senior-web-consultant\';\\\">VIEW ROLE<\\/button><\\/div>\\r\\n          <\\/div>\\r\\n          <div class=\\\"row job-short\\\">              \\r\\n              <div class=\\\"col-sm-10\\\">\\r\\n                <div class=\\\"job-title\\\">Web senior discovery consultant<\\/div>\\r\\n                <div class=\\\"job-location\\\"><i class=\\\"fa fa-map-marker\\\"><\\/i> Melbourne, AUSTRALIA<\\/div>\\r\\n              <\\/div>\\r\\n              <div class=\\\"col-sm-2\\\"><button class=\\\"hvr-sweep-to-right btn btn-primary pull-right\\\" onclick=\\\"location.href=\'career-senior-web-consultant\';\\\">VIEW ROLE<\\/button><\\/div>\\r\\n          <\\/div>-->\\r\\n      <\\/div>\\r\\n  <\\/div>\\r\\n<\\/div>\"}', NULL, NULL, 'component.allow-detach', 1, NULL, NULL, '2022-05-03 20:14:36', '2022-05-08 21:19:19'),
(29, 'custom-html', 'Text Block', '{\"compiler\":\"blade\",\"content\":\"<div class=\\\"row\\\">\\r\\n\\t<div class=\\\"feature-bottom\\\">\\r\\n\\t\\t<div class=\\\"container\\\">\\r\\n\\t\\t\\t<div class=\\\"row\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"col-md-6 col-xs-12\\\" data-aos=\\\"fade-right\\\">\\t\\t\\t\\t\\t\\t\\t\\t\\t\\r\\n                  <div class=\\\"service-dev-text\\\">\\r\\n\\t\\t\\t\\t\\t<h2 style=\\\"font-weight:normal; line-height:54px;\\\">Looking to work at Quatius Logistics?<\\/h2>\\r\\n\\t\\t\\t\\t\\t<p>We are always looking for people with unique skills. Send us your CV and we will get in touch when we have an opening that matches your expectations.<\\/p>\\t\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t<br>\\r\\n                  \\t<p><button class=\\\"hvr-sweep-to-right btn btn-primary\\\" onclick=\\\"location.href=\'career-application\';\\\">EMAIL US<\\/button><\\/p>\\r\\n                  <\\/div>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t<div class=\\\"col-md-6 col-xs-12\\\">\\r\\n\\t\\t\\t\\t\\t<img data-aos=\\\"fade-left\\\" src=\\\"https:\\/\\/quatiuslogistics.com\\/medias\\/pages\\/career\\/Image.png\\\" style=\\\"top:0px\\\">\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<\\/div>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\"}', NULL, NULL, 'component.allow-detach', 1, NULL, NULL, '2022-05-03 20:14:36', '2022-05-08 20:40:17'),
(30, 'custom-html', 'Text Block', '{\"compiler\":\"blade\",\"content\":\"<div class=\\\"row\\\">\\r\\n\\t<div class=\\\"breadcrumb-area\\\" style=\\\"background: rgba(0, 0, 0, 0) url(https:\\/\\/quatiuslogistics.com\\/medias\\/pages\\/career\\/Main-Banner.png) no-repeat scroll center center \\/ cover; padding-top: 83px;\\\">\\r\\n\\t\\t<div class=\\\"container\\\">\\t\\t\\t\\r\\n\\t\\t\\t<div class=\\\"col-md-12 breadcrumb-padding\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"breadcrumb-title\\\">\\r\\n\\t\\t\\t\\t\\t<h1>Career - Senior Website Consultant<\\/h1>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t<div class=\\\"breadcrumb-list\\\">\\r\\n\\t\\t\\t\\t\\t<ul class=\\\"breadcrumb\\\">\\r\\n\\t\\t\\t\\t\\t\\t<li>You are here: <\\/li>\\r\\n\\t\\t\\t\\t\\t\\t<li><a href=\\\"home\\\">Home<\\/a><\\/li>\\t\\t\\t\\t\\t\\t\\t\\r\\n                      <li><a href=\\\"career\\\">Career<\\/a><\\/li>\\r\\n                      <li>National &amp; Group Co-ordinator<\\/li>\\r\\n\\t\\t\\t\\t\\t<\\/ul>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<\\/div>\\t\\t\\t\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\"}', NULL, NULL, 'component.allow-detach', 1, NULL, NULL, '2022-05-04 15:32:58', '2022-05-08 20:44:55'),
(31, 'custom-html', 'job title', '{\"compiler\":\"blade\",\"content\":\"<style>\\r\\n  .job-location{\\r\\n    font-size: 13px;\\r\\n    color: blue;\\r\\n    padding-top: 10px;\\r\\n  }\\r\\n  .job-section{\\r\\n    margin-left: -15px;\\r\\n    margin-right: -15px;\\r\\n    padding-top:50px;\\r\\n    padding-bottom:50px;\\r\\n  }\\r\\n  .job-section ul{\\r\\n    padding-left:15px;\\r\\n    list-style: initial;\\r\\n  }\\r\\n  #job-requirement{\\r\\n    background-color: #eee;\\r\\n  }\\r\\n<\\/style>\\r\\n<div id=\\\"job-title\\\" class=\\\"job-section\\\"> \\r\\n\\t<div class=\\\"container\\\">\\r\\n\\t\\t<h1>National &amp; Group Co-ordinator<\\/h1>\\r\\n        <h5>at Quatius, Fulltime position<\\/h5>\\r\\n        <div class=\\\"job-location\\\"><i class=\\\"fa fa-map-marker\\\"><\\/i> Melbourne, AUSTRALIA<\\/div>  \\r\\n\\t<\\/div>\\r\\n<\\/div>\"}', NULL, NULL, 'component.allow-detach', 1, NULL, NULL, '2022-05-04 18:00:25', '2022-05-08 20:34:38'),
(32, 'custom-html', 'Header Breadcrumb', '{\"compiler\":\"html\",\"content\":\"        <h3 style=\\\"margin-top: 0px;\\\">\\r\\n        <i class=\\\"fa fa-cubes\\\"><\\/i> Connotes <small> Manage Connotes<\\/small>\\r\\n        <\\/h3>\\r\\n <ol class=\\\"breadcrumb\\\">\\r\\n    <li><a href=\\\"http:\\/\\/quatiuslogistics.com\\/admin\\\"><i class=\\\"fa fa-dashboard\\\"><\\/i> Home <\\/a><\\/li>\\r\\n    <li class=\\\"active\\\">Manage Connotes<\\/li>\\r\\n<\\/ol>\",\"init-published\":\"1\"}', NULL, '', 'component.allow-detach', 3, NULL, NULL, '2022-05-05 13:15:16', '2022-05-05 13:15:16');
INSERT INTO `components` (`id`, `type`, `name`, `data`, `register_service`, `editable_by`, `detachable_by`, `published`, `publish_start`, `publish_end`, `created_at`, `updated_at`) VALUES
(34, 'custom-form', 'Custom Form', '{\"compiler\":\"blade\",\"content\":\"<div class=\\\"row\\\">\\r\\n\\t<div class=\\\"help-area\\\">\\r\\n\\t\\t<div class=\\\"container\\\">\\t\\t\\r\\n\\t\\t\\t<div class=\\\"col-md-12 col-xs-12\\\">\\r\\n\\t\\t\\t\\r\\n\\t\\t\\t\\t<div class=\\\"row\\\">\\r\\n\\t\\t\\t\\t\\t<div class=\\\"col-md-4 col-sm-4 col-xs-12 mini-contact\\\">\\r\\n\\t\\t\\t\\t\\t\\t<div class=\\\"features-info-text\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t<h4>CALL US NOW<\\/h4>\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"col-sm-3 col-xs-12\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<img src=\\\"images\\/contact-icon.png\\\" alt=\\\"Contact Quatius Logistics\\\" title=\\\"Contact Quatius Logistics\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"col-sm-9 col-xs-12\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<p>Hotline: 1300 899 299<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<p>Tel: (61)3 8787 8459<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t<div class=\\\"col-md-4 col-sm-4 col-xs-12 mini-contact\\\">\\r\\n\\t\\t\\t\\t\\t\\t<div class=\\\"features-info-text\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t<h4>WELCOME TO VISIT US<\\/h4>\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"col-sm-3 col-xs-12\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<img src=\\\"images\\/logistics-icon.png\\\" alt=\\\"Quatius Logistics\\\" title=\\\"Quatius Logistics\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"col-sm-9 col-xs-12\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<p>Quatius Logistics, your freight management and logistic specilist.<\\/p>\\t\\t\\t\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t<div class=\\\"col-md-4 col-sm-4 col-xs-12 mini-contact\\\">\\r\\n\\t\\t\\t\\t\\t\\t<div class=\\\"features-info-text\\\">\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t\\t<h4>QUATIUS LOGISTICS<\\/h4>\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"col-sm-3 col-xs-12\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<img src=\\\"images\\/map-icon.png\\\" alt=\\\"Quatius Logistics Location\\\" title=\\\"Contact Quatius Logistics\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"col-sm-9 col-xs-12\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<p>1-11 Remington Drive, Dandenong South, VIC, 3175<\\/p>\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<a href=\\\"#\\\" id=\\\"display_map\\\">Display on Map<\\/a>\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/div>\\t\\t\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t<div class=\\\"clearfix\\\"><\\/div>\\r\\n\\t\\t\\t\\r\\n\\t\\t\\t\\t<div class=\\\"row\\\">\\r\\n\\t\\t\\t\\t\\t<div class=\\\"col-md-4 col-sm-4 col-xs-12 pull-right\\\">\\r\\n\\t\\t\\t\\t\\t\\t<!-- <h3>Welcome to Visit Us<\\/h3> -->\\r\\n\\t\\t\\t\\t\\t\\t<div class=\\\"contact-accordion\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t<h3 class=\\\"open\\\">Australia<\\/h3>\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"contact-content\\\"><br>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<ul class=\\\"footer-top-contact\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<div id=\\\"dandenong\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<span class=\\\"contact-text\\\"><strong>Dandenong VIC Warehouse<\\/strong><\\/span>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<span class=\\\"contact-text\\\">1-11 Remington Drive,<br>Dandenong South VIC 3175<\\/span>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<\\/div><\\/li><br>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<div id=\\\"braeside\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<span class=\\\"contact-text\\\"><strong>Braeside VIC Warehouse<\\/strong><\\/span>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<span class=\\\"contact-text\\\">414-426 Lower Dandenong Road,<br>Braeside VIC 3195<\\/span>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<\\/div><\\/li><br>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<div id=\\\"Altona\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<span class=\\\"contact-text\\\"><strong>Truganina VIC Warehouse<\\/strong><\\/span>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<span class=\\\"contact-text\\\">Warehouse A, 45 National Drive,<br>Truganina VIC 3029<\\/span>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<\\/div><\\/li><br>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<div id=\\\"Eastern_NSW\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<span class=\\\"contact-text\\\"><strong>Regents Park NSW Warehouse<\\/strong><\\/span>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<span class=\\\"contact-text\\\">Block G , 391 Park Road,<br>Regents Park NSW 2143 <\\/span>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<\\/div><\\/li><br>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<!--<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<div id=\\\"Richlands_QLD\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<span class=\\\"contact-text\\\"><strong>Richlands QLD Warehouse<\\/strong><\\/span>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<span class=\\\"contact-text\\\">60 Fulcrum Street,<br\\/>Richlands QLD 4077 <\\/span>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<\\/li><br \\/>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t-->\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<div id=\\\"Richlands_QLD\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<span class=\\\"contact-text\\\"><strong>Richlands QLD Warehouse<\\/strong><\\/span>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<span class=\\\"contact-text\\\">70 Fulcrum Street,<br>Richlands QLD 4077 <\\/span>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<\\/div><\\/li><br>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<div id=\\\"Gillman_SA\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<span class=\\\"contact-text\\\"><strong>Gillman SA Warehouse<\\/strong><\\/span>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<span class=\\\"contact-text\\\">2 Portsmouth Court,<br>Gillman SA 5013 <\\/span>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<\\/div><\\/li>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<\\/ul>\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t<h3>New Zealand<\\/h3>\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"contact-content\\\"><br>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<ul class=\\\"footer-top-contact\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<span class=\\\"contact-text\\\">84 Kerrs Road, Manukau, Auckland, <br>New Zealand, 2150<\\/span>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<span class=\\\"contact-text\\\">P.O.Box 53181,<br>Auckland Airport, 2150<\\/span>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<\\/li><br>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<li><span class=\\\"contact-text\\\">Tel: (64) 9 266 7512<\\/span><\\/li>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<li><span class=\\\"contact-text\\\">Fax: (64) 9 266 7105<\\/span><\\/li>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<\\/ul>\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t<h3>Hong Kong<\\/h3>\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"contact-content\\\"><br>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<ul class=\\\"footer-top-contact\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<span class=\\\"contact-text\\\">Units 2605, 26\\/F, Greenfield Tower, Concodial Plaza, 1 Science Museum Rd., Tsimshatsui East, Kowloon, Hong Kong<\\/span>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<li><span class=\\\"contact-text\\\">Tel: (852) 2312 2666<\\/span><\\/li>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<li><span class=\\\"contact-text\\\">Fax: (852) 2312 2266 <\\/span><\\/li>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<\\/ul>\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t<h3>China- Shenzhen<\\/h3>\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"contact-content\\\"><br>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<ul class=\\\"footer-top-contact\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<span class=\\\"contact-text\\\">4\\/F, Bldg#6, Zhongguan Honghualing Industrial South Park II, 1213 Liuxian Ave., Nanshan District, Shenzhen, (518055), P.R. China<\\/span>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<li><span class=\\\"contact-text\\\">Tel: (86) 0755 3690 2888  (86) 0755 2665 7933 <\\/span><\\/li>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<\\/ul>\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t<h3>China- Lianyungang<\\/h3>\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"contact-content\\\"><br>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<ul class=\\\"footer-top-contact\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<li>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<span class=\\\"contact-text\\\">No. 6 East Huanghai Road, Lingang Industrial Area, Lianyungang Economic &amp; Technological Development Zone, Lianyungang, Jiangsu, P.R. China<\\/span>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<\\/li>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<\\/ul>\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/div>\\t\\t\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t<\\/div>\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t<div class=\\\"col-md-8 col-sm-8 col-xs-12 contact-form-ac\\\" id=\\\"contact_form_ac\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t{!! view(\'public::notifications\')!!}\\r\\n\\t\\t\\t\\t\\t\\t\\t<form action=\\\"{{url(\'component\\/\'.$component->id)}}\\\" method=\\\"POST\\\" name=\\\"send_enquiry\\\" id=\\\"send_enquiry\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t{{ csrf_field() }}\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"row\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<div class=\\\"col-md-6 col-sm-6 col-xs-12\\\">\\t\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<div class=\\\"control-level\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<p><label for=\\\"company_name\\\">COMPANY NAME<\\/label> *<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<input id=\\\"company_name\\\" name=\\\"company_name\\\" class=\\\"form-control\\\" type=\\\"text\\\" required=\\\"required\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<div class=\\\"col-md-6 col-sm-6 col-xs-12\\\">\\t\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<div class=\\\"control-level\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<p><label for=\\\"regards\\\">REGARDS<\\/label> *<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<input id=\\\"regards\\\" name=\\\"regards\\\" class=\\\"form-control\\\" type=\\\"text\\\" required=\\\"required\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<\\/div>\\t\\t\\t\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"row\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<div class=\\\"col-md-6 col-sm-6 col-xs-12\\\">\\t\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<div class=\\\"row\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<div class=\\\"col-md-6 col-xs-12\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<div class=\\\"control-level\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<p><label for=\\\"first_name\\\">FIRST NAME<\\/label> *<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<input id=\\\"first_name\\\" name=\\\"first_name\\\" class=\\\"form-control\\\" type=\\\"text\\\" required=\\\"required\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<div class=\\\"col-md-6 col-xs-12\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<div class=\\\"control-level\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<p><label for=\\\"last_name\\\">LAST NAME<\\/label> *<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<input id=\\\"last_name\\\" name=\\\"last_name\\\" class=\\\"form-control\\\" type=\\\"text\\\" required=\\\"required\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<div class=\\\"col-md-12 col-xs-12\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<div class=\\\"control-level\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<p><label for=\\\"email\\\">EMAIL<\\/label> *<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<input id=\\\"email\\\" name=\\\"email\\\" class=\\\"form-control\\\" type=\\\"email\\\" required=\\\"required\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<div class=\\\"col-md-12 col-xs-12\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<div class=\\\"control-level\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<p><label for=\\\"phone\\\">CONTACT NUMBER<\\/label> *<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<input id=\\\"phone\\\" name=\\\"phone\\\" class=\\\"form-control\\\" type=\\\"tel\\\" required=\\\"required\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<div class=\\\"col-md-6 col-sm-6 col-xs-12\\\">\\t\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<div class=\\\"control-level\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<p><label for=\\\"message\\\">MESSAGE<\\/label> *<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<textarea id=\\\"message\\\" name=\\\"message\\\" class=\\\"form-control\\\" rows=\\\"5\\\" required=\\\"required\\\"><\\/textarea>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<br>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t{!! Captcha::render() !!}\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<\\/div>\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t<div class=\\\"row\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<div class=\\\"col-md-6 col-sm-6 col-xs-12\\\">\\t\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<div class=\\\"control-level\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<p>We will contact you within three working days.<\\/p>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<div class=\\\"col-md-6 col-sm-6 col-xs-12\\\">\\t\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t<div class=\\\"control-level submit-button\\\">\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<!-- <input class=\\\"readon\\\" type=\\\"button\\\" value=\\\"Send Email\\\"\\/> -->\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<button class=\\\"hvr-sweep-to-right btn btn-primary\\\" type=\\\"submit\\\" data-action=\\\"CREATE\\\" data-form=\\\"#send_enquiry\\\" data-load-to=\\\"#contact_form_ac\\\">SEND<\\/button>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t\\t\\t<\\/form>\\r\\n\\t\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t\\t<\\/div>\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<\\/div>\\t\\t\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\",\"validate_json\":\"{\\\"first_name\\\":\\\"required|max:255\\\",\\\"email\\\":\\\"required|email|max:255\\\",\\\"g-recaptcha-response\\\":\\\"required|recaptcha\\\"}\",\"submit_execute\":\"flash(\'Thanks, Your email has been sent.\');\\r\\n\\r\\n$data[\'company_name\'] = $request->get(\'company_name\', \'\');\\r\\n$data[\'regards\'] = $request->get(\'regards\', \'\');\\r\\n$data[\'first_name\'] = $request->get(\'first_name\', \'\');\\r\\n$data[\'last_name\'] = $request->get(\'last_name\', \'\');\\r\\n$data[\'email\'] = $request->get(\'email\', \'\');\\r\\n$data[\'phone\'] = $request->get(\'phone\', \'\');\\r\\n$data[\'msg_content\'] = $request->get(\'message\', \'\');\\r\\nMail::send(\'logistics.emails.enquiry\', $data, function($message) use ($request){\\r\\n    \\/\\/$message->from($request->get(\'email\', \'\'),\'Online - \'.$request->get(\'first_name\', \'\'));\\r\\n    $message->subject(\\\"Customer enquiry\\\");\\r\\n    $message->to(env(\'APP_EMAIL_INFO_ADDRESS\',\'info@quatiuslogistics.com\'));\\r\\n});\\r\\n\\r\\n\\r\\n\\/\\/sendMailView(\\\"Customer enquiry\\\",\'logistics.emails.enquiry\', $data, \'info@quatiuslogistics.com\', $data[\'email\']);\",\"submit_admin_execute\":null,\"show_preview\":\"1\",\"admin_view\":null,\"admin_edit\":null,\"admin_save\":null}', NULL, 'superuser', 'component.allow-detach-code', 1, NULL, NULL, '2022-05-05 17:37:07', '2022-05-10 16:53:57'),
(35, 'custom-html', 'job line', '{\"compiler\":\"blade\",\"content\":\"<div id=\\\"job-line\\\"> \\r\\n\\t<div class=\\\"container\\\">\\r\\n\\t\\t\\r\\n     <hr style=\\\"border-top: 1px solid #777;#333: 0;margin-bottom: 0;margin-top: 0;\\\">\\r\\n   \\r\\n\\t<\\/div>\\r\\n<\\/div>\"}', NULL, NULL, 'component.allow-detach', 1, NULL, NULL, '2022-05-08 19:34:09', '2022-05-08 20:08:01'),
(36, 'custom-html', 'job requirement', '{\"compiler\":\"blade\",\"content\":\"<div id=\\\"job-description\\\" class=\\\"job-section\\\">  \\r\\n\\t<div class=\\\"container\\\">\\r\\n      <h3>DESCRIPTION<\\/h3>\\r\\n      <p>\\r\\n        The National &amp; Group Co-Ordinator will manage \\/ supervisor the daily supply chain process from customer order through to the completion of the customer\\u2019s request, ensuring optimisation of both the customer\\u2019s expectation and Quatius logistics\\u2019 profitability. \\r\\n      <\\/p><br>\\r\\n      <p>\\r\\n        Through the day-to-day running of the operation and monitoring of best practice approaches within the industry, National &amp; Group Co-Ordinator will also be integral as part of the executive in developing strategic short to long term direction through to implementation with the goal of continual improvement of customer satisfaction objectives. Ultimately maximising Quatius Logistics Company profitability and efficiencies. \\r\\n      <\\/p>\\r\\n  <\\/div>\\r\\n<\\/div>\"}', NULL, NULL, 'component.allow-detach', 1, NULL, NULL, '2022-05-08 19:40:20', '2022-05-08 20:08:01'),
(37, 'custom-html', 'Text Block', '{\"compiler\":\"blade\",\"content\":\"<div id=\\\"job-requirement\\\" class=\\\"job-section\\\">\\r\\n  <div class=\\\"container\\\">\\t\\r\\n      <h3>REQUIREMENTS<\\/h3>\\r\\n      <ul type=\\\"disc\\\" class=\\\"job-list\\\">\\r\\n        <li>3+ years experience in the logistics industry<\\/li>\\r\\n        <li>Strong leadership capabilities<\\/li>\\r\\n        <li>Likes working in a results-orientated team environment<\\/li>\\r\\n        <li>Demonstrates initiative, enthusiasm, and a willingness to innovate processes<\\/li>\\r\\n        <li>Has exceptional attention to detail<\\/li>\\r\\n        <li>Familiarity with stock control, dispatch, OH&amp;S\\/WHS policies and general warehouse processes<\\/li>\\r\\n        <li>Hands on, physically fit with a reliable work history<\\/li>\\r\\n        <li>Forklift Licensel<\\/li>\\r\\n    <\\/ul>\\r\\n  <\\/div>\\r\\n<\\/div>\"}', NULL, NULL, 'component.allow-detach', 1, NULL, NULL, '2022-05-08 20:08:02', '2022-05-08 20:34:38'),
(38, 'custom-html', 'job benefit', '{\"compiler\":\"blade\",\"content\":\"<div id=\\\"job-benefit\\\" class=\\\"job-section\\\">\\r\\n  <div class=\\\"container\\\">\\t\\r\\n      <h3>REMUNERATION &amp; BENEFITS<\\/h3>\\r\\n      <ul type=\\\"disc\\\" class=\\\"job-list\\\">\\r\\n        <li>Full-time role.<\\/li>\\r\\n        <li>Competitive remuneration package based on applicant experience<\\/li>        \\r\\n    <\\/ul><br>\\r\\n    <p>If you are hardworking, organised, reliable, proactive, have a positive attitude along with good time management skills we would love to hear from you.<\\/p>\\r\\n  <\\/div>\\r\\n<\\/div>\"}', NULL, NULL, 'component.allow-detach', 1, NULL, NULL, '2022-05-08 20:27:06', '2022-05-08 20:35:07'),
(39, 'custom-html', 'Text Block', '{\"compiler\":\"blade\",\"content\":\"<div id=\\\"job-benefit\\\" class=\\\"job-section\\\">\\r\\n  <div class=\\\"container\\\">\\r\\n    <div style=\\\"text-align:center\\\">\\r\\n      \\t<button class=\\\"hvr-sweep-to-right btn btn-primary\\\" onclick=\\\"location.href=\'career-application?id=1\';\\\">APPLY NOW<\\/button>\\r\\n    <\\/div>\\r\\n  <\\/div>\\r\\n<\\/div>\"}', NULL, NULL, 'component.allow-detach', 1, NULL, NULL, '2022-05-08 20:37:18', '2022-05-10 21:35:27'),
(40, 'custom-html', 'Text Block', '{\"compiler\":\"blade\",\"content\":\"<div class=\\\"row\\\">\\r\\n\\t<div class=\\\"breadcrumb-area\\\" style=\\\"background: rgba(0, 0, 0, 0) url(https:\\/\\/quatiuslogistics.com\\/medias\\/pages\\/career\\/Main-Banner.png) no-repeat scroll center center \\/ cover; padding-top: 83px;\\\">\\r\\n\\t\\t<div class=\\\"container\\\">\\t\\t\\t\\r\\n\\t\\t\\t<div class=\\\"col-md-12 breadcrumb-padding\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"breadcrumb-title\\\">\\r\\n\\t\\t\\t\\t\\t<h1>Career - Application Form<\\/h1>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t\\t<div class=\\\"breadcrumb-list\\\">\\r\\n\\t\\t\\t\\t\\t<ul class=\\\"breadcrumb\\\">\\r\\n\\t\\t\\t\\t\\t\\t<li>You are here: <\\/li>\\r\\n\\t\\t\\t\\t\\t\\t<li><a href=\\\"home\\\">Home<\\/a><\\/li>\\t\\t\\t\\t\\t\\t\\t\\r\\n                      <li><a href=\\\"career\\\">Career<\\/a><\\/li>\\r\\n                      <li>Application form<\\/li>\\r\\n\\t\\t\\t\\t\\t<\\/ul>\\r\\n\\t\\t\\t\\t<\\/div>\\r\\n\\t\\t\\t<\\/div>\\t\\t\\t\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\"}', NULL, NULL, 'component.allow-detach', 1, NULL, NULL, '2022-05-08 20:42:35', '2022-05-08 20:46:25'),
(43, 'custom-form', 'Custom Form', '{\"compiler\":\"blade\",\"content\":\"<style>\\r\\n\\t.job-location{\\r\\n\\t\\tfont-size: 13px;\\r\\n\\t\\tcolor: blue;\\r\\n\\t\\tpadding-top: 10px;\\r\\n\\t  }\\r\\n\\t  .job-section{\\r\\n\\t\\tmargin-left: -15px;\\r\\n\\t\\tmargin-right: -15px;\\r\\n\\t\\tpadding-top:50px;\\r\\n\\t\\t\\/*padding-bottom:50px;*\\/\\r\\n\\t  }\\r\\n\\t  .job-section ul{\\r\\n\\t\\tpadding-left:15px;\\r\\n\\t\\tlist-style: initial;\\r\\n\\t  }\\r\\n\\t  #job-requirement{\\r\\n\\t\\tbackground-color: #eee;\\r\\n\\t  }\\r\\n\\t.custom_dropzone{\\r\\n\\tmin-height: 80px !important;\\r\\n    border-radius: 0;\\r\\n    background: #ededed;\\r\\n    border: 0;    \\r\\n\\tbackground: #ededed;\\t\\r\\n    box-shadow: inset 0 1px 1px rgb(0 0 0 \\/ 8%);\\r\\n    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;\\r\\n    text-align: center;\\r\\n    padding-top: 30px !important;\\r\\n\\t}\\r\\n\\t.fa{\\r\\n\\t\\tpadding-right: 5px;\\r\\n\\t}\\r\\n\\t\\r\\n\\t#dropzone.has-error{\\r\\n\\t\\tcolor: #a94442;\\r\\n\\t\\tborder: 1px solid #a94442;\\r\\n\\t}\\r\\n\\t\\r\\n\\t#dropzone {\\r\\n\\t  position: relative;\\r\\n\\t  min-height: 80px !important;\\r\\n\\t  color: #555;\\t  \\t \\r\\n\\t  background-color: #ededed; \\t\\r\\n\\t  text-align: center;\\t  \\r\\n\\t}\\r\\n\\r\\n#dropzone.hover {\\r\\n  border: 1px solid #777;  \\r\\n}\\r\\n\\r\\n#dropzone div {\\r\\n  position: absolute;\\r\\n  top: 0;\\r\\n  right: 0;\\r\\n  bottom: 0;\\r\\n  left: 0;\\r\\n  padding-top:30px;\\t\\r\\n}\\r\\n\\r\\n#dropzone [type=\\\"file\\\"] {\\r\\n  cursor: pointer;\\r\\n  position: absolute;\\r\\n  opacity: 0;\\r\\n  top: 0;\\r\\n  right: 0;\\r\\n  bottom: 0;\\r\\n  left: 0;\\r\\n}\\r\\n\\t\\r\\n<\\/style>\\r\\n\\r\\n@if($component->hasParams(\'job_list.\' . Request::get(\'id\', 0) . \\\".title\\\"))\\r\\n<div id=\\\"job-title\\\" class=\\\"job-section\\\"> \\r\\n\\t<div class=\\\"container\\\">\\r\\n\\t\\t<h1>{{$component->getParams(\'job_list.\' . Request::get(\'id\', 0) . \\\".title\\\",\\\"\\\")}}<\\/h1>\\r\\n        <h5>{{$component->getParams(\'job_list.\' . Request::get(\'id\', 0) . \\\".position\\\",\\\"\\\")}}<\\/h5>\\r\\n        <div class=\\\"job-location\\\"><i class=\\\"fa fa-map-marker\\\"><\\/i> {{$component->getParams(\'job_list.\' . Request::get(\'id\', 0) . \\\".location\\\",\\\"\\\")}}<\\/div>  \\r\\n\\t\\t<div id=\\\"hr\\\" style=\\\"padding: 25px 0;\\\"><hr style=\\\"border-top:1px solid #777\\\"><\\/div>\\r\\n\\t\\t<div onClick=\\\"history.back();\\\" style=\\\"cursor:pointer\\\"><i class=\\\"fa fa-long-arrow-left\\\"><\\/i> Back to Job Overview<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\\r\\n@endif\\r\\n\\r\\n<div id=\\\"job-form\\\" class=\\\"job-section\\\">  \\r\\n\\t<div class=\\\"container\\\">\\r\\n{!! view(\'public::notifications\')!!}\\r\\n\\t{!!Form::vertical_open_for_files()->id(\'job-application\')\\r\\n\\t->action(url(\'component\\/\'.$component->id))\\r\\n\\t->method(\'POST\')\\r\\n\\t->class(\'in-line\')!!}\\r\\n<div class=\\\"row\\\">\\r\\n\\t<div class=\\\"col-sm-6\\\">\\r\\n\\t\\t<div class=\\\"control-level\\\">\\r\\n\\t\\t{!! Form::text(\'first_name\')->label(\'FIRST NAME\')-> placeholder(\'Enter your first name\')->required()!!}\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\t\\r\\n\\t<div class=\\\"col-sm-6\\\">\\r\\n\\t\\t<div class=\\\"control-level\\\">\\r\\n\\t\\t{!! Form::text(\'last_name\')->label(\'LAST NAME\')-> placeholder(\'Enter your last name\')->required()!!}\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\t\\r\\n<\\/div>\\r\\n<div class=\\\"row\\\">\\r\\n\\t<div class=\\\"col-sm-6\\\">\\r\\n\\t\\t<div class=\\\"control-level\\\">\\r\\n\\t\\t{!! Form::tel(\'phone\')->label(\'PHONE\')-> placeholder(\'Enter your telephone\')->required()!!}\\r\\n\\t\\t<\\/div>\\t\\r\\n\\t<\\/div>\\t\\r\\n\\t<div class=\\\"col-sm-6\\\">\\r\\n\\t\\t<div class=\\\"control-level\\\">\\r\\n\\t\\t{!! Form::text(\'location\')->label(\'LOCATION\')-> placeholder(\'Enter your location\')!!}\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\t\\r\\n<\\/div>\\t\\r\\n<div class=\\\"row\\\">\\r\\n\\t<div class=\\\"col-sm-12\\\">\\r\\n\\t\\t<div class=\\\"control-level\\\">\\r\\n\\t\\t{!! Form::email(\'email\')->label(\'EMAIL ADDRESS\')-> placeholder(\'Enter your email.\')->required()!!}\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\\r\\n\\t\\t<!--\\r\\n<div calss=\\\"row\\\">\\r\\n\\t<div class=\\\"form-group required\\\">\\r\\n\\t\\t<label for=\\\"resume\\\" class=\\\"control-label\\\">RESUME\\/CV<sup>*<\\/sup><\\/label>\\r\\n\\t\\t<div id=\\\"resume\\\" class=\\\"dropzone custom_dropzone\\\"><span id=\\\"dropzone-desc\\\">Upload a file or drag and drop here<\\/span><\\/div>\\r\\n\\t<\\/div>\\t\\t\\r\\n<\\/div>\\r\\n\\t\\t-->\\r\\n<div calss=\\\"row\\\">\\r\\n\\t<div class=\\\"form-group required\\\">\\r\\n\\t\\t<label for=\\\"resume\\\" class=\\\"control-label\\\">RESUME\\/CV<sup>*<\\/sup><\\/label>\\r\\n\\t\\t<div id=\\\"dropzone\\\">\\r\\n\\t\\t  <div>Upload a file or drag and drop here<\\/div>\\r\\n\\t\\t  <input type=\\\"file\\\" name=\\\"resume\\\" id=\\\"resume\\\" accept=\\\"application\\/msword,application\\/vnd.openxmlformats-officedocument.wordprocessingml.document,application\\/pdf\\\" required \\/>\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\\r\\n\\t\\t\\r\\n<div class=\\\"row\\\">\\r\\n\\t<div class=\\\"col-sm-12\\\">\\r\\n\\t\\t<div class=\\\"control-level\\\">\\r\\n\\t\\t{!! Form::textarea(\'cover_letter\')->label(\'COVER LETTER\')->rows(6)-> placeholder(\'\')!!}\\r\\n\\t\\t<\\/div>\\r\\n\\t<\\/div>\\r\\n<\\/div>\\r\\n<div class=\\\"row\\\">\\r\\n\\t<div class=\\\"col-sm-12\\\">\\r\\n\\t\\t{!! Captcha::render() !!}\\r\\n\\t<\\/div>\\r\\n<\\/div>\\r\\n<div class=\\\"row\\\">\\r\\n\\t<div class=\\\"col-sm-12\\\">\\r\\n\\t<div style=\\\"text-align:center; padding-top:30px;\\\">\\r\\n\\t\\t<button class=\\\"hvr-sweep-to-right btn btn-primary\\\" id=\\\"submit\\\">SUBMIT APPLICATION<\\/button>\\r\\n\\t<\\/div>\\r\\n<\\/div>\\r\\n\\t{!! Form::hidden(\'id\')->value(Request::get(\'id\',0)) !!}\\r\\n\\t\\r\\n{{ csrf_field() }}                                \\r\\n{!! Form::close() !!}\\r\\n<\\/div>\\r\\n<\\/div>\\r\\n<\\/div>\\r\\n@script\\r\\n<script>\\r\\n\\t\\/*\\t\\r\\n\\t$(document).ready(function () {\\r\\n        $(\\\"#resume\\\").dropzone({\\r\\n            maxFiles: 1,\\r\\n\\t\\t\\tautoProcessQueue: false,\\r\\n\\t\\t\\taddRemoveLinks: true,\\r\\n\\t\\t\\theaders: {\'X-CSRFToken\': $(\'meta[name=csrf-token]\').attr(\'content\')},\\r\\n            url: \\\"{{url(\'component\\/\'.$component->id)}}\\\",\\r\\n            success: function (file, response) {\\r\\n                \\r\\n            },\\r\\n\\t\\t\\tinit: function() {\\r\\n\\t\\t\\t\\tthis.on(\\\"addedfile\\\", function(file) { \\r\\n\\t\\t\\t\\t  $(\\\"form#job-application\\\").append(\\\"file\\\", file); \\r\\n\\t\\t\\t\\t\\t$(\\\"#dropzone-desc\\\").hide();\\r\\n\\t\\t\\t\\t});\\t\\t\\t\\t\\r\\n        \\t}\\r\\n\\t\\t})\\r\\n   })\\r\\n\\t*\\/\\r\\n\\t\\r\\n\\t\\r\\n\\t$(function() {\\r\\n  \\/*\\r\\n  $(\'#dropzone\').on(\'dragover\', function() {\\r\\n    $(this).addClass(\'hover\');\\r\\n  });\\r\\n  \\r\\n  $(\'#dropzone\').on(\'dragleave\', function() {\\r\\n    $(this).removeClass(\'hover\');\\r\\n  });\\r\\n  *\\/\\r\\n  $(\'#dropzone input\').on(\'change\', function(e) {\\r\\n    var file = this.files[0];\\r\\n\\t$(\\\"#dropzone\\\").parent(\\\".form-group\\\").removeClass(\\\"has-error\\\");\\r\\n\\t  $(\\\"#dropzone\\\").removeClass(\\\"has-error\\\");\\r\\n\\t\\/*\\r\\n    $(\'#dropzone\').removeClass(\'hover\');\\r\\n    *\\/\\r\\n    if (this.accept && $.inArray(file.type, this.accept.split(\\/, ?\\/)) == -1) {\\r\\n\\t\\tconsole.log(file.type);\\r\\n      $(\\\"#dropzone\\\").parent(\\\".form-group\\\").addClass(\\\"has-error\\\");\\r\\n\\t\\t$(\\\"#dropzone\\\").addClass(\\\"has-error\\\");\\r\\n\\t\\t$(\\\"#dropzone\\\").find(\\\"div\\\").html(\\\"Only support doc\\/docx, pdf\\\");\\r\\n\\t\\t$(\\\"#resume\\\").val(\'\'); \\r\\n\\t\\treturn false;\\r\\n    }\\r\\n    $(\'#dropzone div\').html(file.name);\\r\\n    \\r\\n     \\r\\n    \\r\\n\\t  \\/*\\r\\n\\t  $(\'#dropzone\').addClass(\'dropped\');\\r\\n\\t  $(\'#dropzone img\').remove();\\r\\n    if ((\\/^image\\\\\\/(gif|png|jpeg)$\\/i).test(file.type)) {\\r\\n      var reader = new FileReader(file);\\r\\n\\r\\n      reader.readAsDataURL(file);\\r\\n      \\r\\n      reader.onload = function(e) {\\r\\n        var data = e.target.result,\\r\\n            $img = $(\'<img \\/>\').attr(\'src\', data).fadeIn();\\r\\n        \\r\\n        $(\'#dropzone div\').html($img);\\r\\n      };\\r\\n    } else {\\r\\n      \\r\\n      console.log(file);\\r\\n      $(\'#dropzone div\').html(file.name);\\r\\n    }\\r\\n\\t*\\/\\r\\n  });\\r\\n});\\r\\n<\\/script>\\r\\n@endscript\",\"validate_json\":\"{\\\"first_name\\\":\\\"required|max:255\\\", \\\"last_name\\\":\\\"required|max:255\\\",\\\"phone\\\":\\\"required|max:255\\\", \\\"email\\\":\\\"required|email|max:255\\\",\\\"g-recaptcha-response\\\":\\\"required|recaptcha\\\",\\\"resume\\\":\\\"required|mimes:doc,docx,pdf\\\"}\",\"submit_execute\":\"flash(\'Thanks, Your application has been sent.\');\\r\\n\\t\\r\\n\\t$data[\'Job_title\'] = $component->getParams(\'job_list.\' .  $request->get(\'id\', \'0\') . \\\".title\\\",\\\"\\\");\\r\\n\\t$data[\'first_name\'] = $request->get(\'first_name\', \'\');\\r\\n\\t$data[\'last_name\'] = $request->get(\'last_name\', \'\');\\r\\n\\t$data[\'phone\'] = $request->get(\'phone\', \'\');\\r\\n\\t$data[\'location\'] = $request->get(\'location\', \'\');\\r\\n\\t$data[\'email\'] = $request->get(\'email\', \'\');\\r\\n\\t$data[\'cover_letter\'] = $request->get(\'cover_letter\', \'\');\\r\\n\\r\\n\\tMail::send(\'logistics.emails.enquiry\', $data, function($message) use ($request){    \\r\\n\\t\\t$resume = $request->file(\'resume\');\\r\\n\\t\\t$message->subject(\\\"Apply job\\\");\\r\\n\\t\\t$message->attach($resume ->getRealPath(),\\r\\n                [\\r\\n                    \'as\' => $resume ->getClientOriginalName(),\\r\\n                    \'mime\' => $resume ->getClientMimeType(),\\r\\n                ]);\\r\\n\\t\\t$message->to([\'vey.chea@soniq.com\',\'kevin.tia@soniq.com\']);\\r\\n\\t\\t\\r\\n\\t});\",\"submit_admin_execute\":null,\"show_preview\":\"0\",\"admin_view\":null,\"admin_edit\":\"{!!Form::textarea(\'components[\'.$index.\'][data][jobs]\')->value($component->getParams(\'jobs\',\'\'))!!}\",\"jobs\":\"{\\\"1\\\":{\\\"title\\\":\\\"National & Group Co-ordinator\\\",\\\"position\\\":\\\"at Quatius, Fulltime position\\\",\\\"location\\\":\\\"Melbourne AUSTRALIA\\\"}}\",\"admin_save\":\"$job_list = json_decode($component->getParams(\'jobs\',\'\'),true);\\r\\n$component->setParams(\'job_list\', $job_list);\\r\\n$component->save();\",\"job_list\":{\"1\":{\"title\":\"National & Group Co-ordinator\",\"position\":\"at Quatius, Fulltime position\",\"location\":\"Melbourne AUSTRALIA\"}}}', NULL, 'superuser', 'component.allow-detach-code', 1, NULL, NULL, '2022-05-08 20:59:08', '2022-05-10 21:34:24'),
(46, 'custom-form', 'Custom Form', '{\"compiler\":\"blade\",\"content\":\"<script src=\\\"https:\\/\\/unpkg.com\\/dropzone@5\\/dist\\/min\\/dropzone.min.js\\\"><\\/script>\\r\\n<link rel=\\\"stylesheet\\\" href=\\\"https:\\/\\/unpkg.com\\/dropzone@5\\/dist\\/min\\/dropzone.min.css\\\" type=\\\"text\\/css\\\" \\/>\\r\\n\\r\\n<div id=\\\"id_dropzone\\\" class=\\\"dropzone\\\">upload here<\\/div>\\r\\n\\r\\n<script>\\r\\n\\t$(document).ready(function () {\\r\\n        $(\\\"#id_dropzone\\\").dropzone({\\r\\n            maxFiles: 1,\\r\\n            url: \\\"{{url(\'component\\/\'.$component->id)}}\\\",\\r\\n            success: function (file, response) {\\r\\n                console.log(response);\\r\\n            }\\r\\n        });\\r\\n   })\\r\\n   \\r\\n<\\/script>\",\"validate_json\":\"{\\\"name\\\":\\\"required|max:255\\\",\\\"email\\\":\\\"required|email|max:255\\\",\\\"msg\\\":\\\"required\\\",\\\"g-recaptcha-response\\\":\\\"required|recaptcha\\\"}\",\"submit_execute\":null,\"submit_admin_execute\":null,\"show_preview\":\"1\",\"admin_view\":null,\"admin_edit\":null,\"admin_save\":null}', NULL, 'superuser', 'component.allow-detach-code', 0, NULL, NULL, '2022-05-10 18:22:04', '2022-05-10 19:09:27'),
(47, 'custom-form', 'client block', '{\"compiler\":\"blade\",\"content\":\"<div class=\\\"row\\\">\\r\\n\\t<div class=\\\"client-block fix\\\">\\r\\n\\t\\t<div class=\\\"container\\\">\\r\\n\\t\\t\\t<div class=\\\"row\\\">\\r\\n\\t\\t\\t\\t<div class=\\\"col-xs-12 \\\">\\r\\n\\t\\t\\t\\t\\t<div class=\\\"owl-carousel\\\">\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t          <div class=\\\"owl-carousel--item\\\">\\r\\n\\t\\t\\t\\t            <img src=\\\"{{asset(\'images\\/clients\\/soniq-logo.png\')}}\\\">\\r\\n\\t\\t\\t\\t          <\\/div>\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t          <div class=\\\"owl-carousel--item\\\">\\r\\n\\t\\t\\t\\t            <img src=\\\"{{asset(\'images\\/clients\\/klass-logo.png\')}}\\\">\\r\\n\\t\\t\\t\\t          <\\/div>\\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t          <div class=\\\"owl-carousel--item\\\">\\r\\n\\t\\t\\t\\t            <img src=\\\"{{asset(\'images\\/clients\\/jb-logo.png\')}}\\\">\\r\\n\\t\\t\\t\\t          <\\/div>\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t          <div class=\\\"owl-carousel--item\\\">\\r\\n\\t\\t\\t\\t            <img src=\\\"{{asset(\'images\\/clients\\/scholl-logo.png\')}}\\\">\\r\\n\\t\\t\\t\\t          <\\/div>  \\t\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t          <div class=\\\"owl-carousel--item\\\">\\r\\n\\t\\t\\t\\t            <img src=\\\"{{asset(\'images\\/clients\\/fst-logo.png\')}}\\\">\\r\\n\\t\\t\\t\\t          <\\/div>\\t\\t\\t\\t\\r\\n\\t\\t\\t\\t          <div class=\\\"owl-carousel--item\\\">\\r\\n\\t\\t\\t\\t            <img src=\\\"{{asset(\'images\\/clients\\/alldocks-logo.png\')}}\\\">\\r\\n\\t\\t\\t\\t          <\\/div>  \\t\\r\\n      \\t\\t\\t\\t<\\/div>\\t\\r\\n                <\\/div>\\r\\n            <\\/div>\\r\\n        <\\/div>    \\r\\n    <\\/div>\\r\\n<\\/div>\\r\\n\\r\\n\\r\\n@script\\r\\n<script>\\r\\nvar owl = $(\'.owl-carousel\');\\r\\nowl.owlCarousel({\\r\\n    items: 6,\\r\\n    margin: 10,\\r\\n    loop: true,\\r\\n    center: true,\\r\\n    mouseDrag: true,\\r\\n    touchDrag: true,\\r\\n    nav: false,\\r\\n    dots: false,\\r\\n    lazyLoad: true,\\r\\n    autoplay: true,\\r\\n    autoplayTimeout: 5000,\\r\\n    autoplayHoverPause: true,\\r\\n    autoWidth: true\\r\\n});\\r\\n$(document).keydown(function (e) {\\r\\n    if (e.keyCode === 37) {\\r\\n        owl.trigger(\'prev.owl.carousel\');\\r\\n    } else if (e.keyCode === 39) {\\r\\n        owl.trigger(\'next.owl.carousel\');\\r\\n    }\\r\\n});\\r\\n\\/\\/# sourceURL=pen.js\\r\\n<\\/script>\\r\\n@endscript\",\"validate_json\":\"{}\",\"submit_execute\":null,\"submit_admin_execute\":null,\"show_preview\":\"1\",\"admin_view\":null,\"admin_edit\":null,\"admin_save\":null}', NULL, 'superuser', 'component.allow-detach-code', 1, NULL, NULL, '2022-05-10 19:59:25', '2022-05-11 23:50:28'),
(51, 'banner-slideshow', 'Images Block', '{\"view\":\"slider\"}', NULL, NULL, 'component.allow-detach', 1, NULL, NULL, '2022-05-11 23:52:39', '2022-05-11 23:52:39');

-- --------------------------------------------------------

--
-- Table structure for table `component_placements`
--

CREATE TABLE `component_placements` (
  `id` int(10) UNSIGNED NOT NULL,
  `component_id` int(10) UNSIGNED NOT NULL,
  `url` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `component_placement_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `component_placement_id` int(10) UNSIGNED DEFAULT NULL,
  `position` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'content-footer',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `component_placements`
--

INSERT INTO `component_placements` (`id`, `component_id`, `url`, `component_placement_type`, `component_placement_id`, `position`, `ordering`, `created_at`, `updated_at`) VALUES
(3, 2, NULL, 'Lavalite\\Page\\Models\\Page', 2, 'showcase', 0, '2016-09-06 19:24:33', '2022-03-30 01:01:22'),
(9, 8, NULL, 'Lavalite\\Page\\Models\\Page', 2, 'fullwidth', 2, '2016-09-06 19:27:14', '2022-03-30 01:01:22'),
(11, 9, NULL, 'Lavalite\\Page\\Models\\Page', 3, 'showcase', 0, '2016-09-06 20:56:30', '2022-05-05 17:37:07'),
(13, 1, NULL, 'Lavalite\\Page\\Models\\Page', 5, 'content-header', 0, '2016-09-07 14:36:27', '2016-09-07 14:36:27'),
(14, 11, NULL, 'Lavalite\\Page\\Models\\Page', 5, 'showcase', 1, '2016-09-07 14:43:12', '2016-09-07 14:43:12'),
(15, 12, NULL, 'Lavalite\\Page\\Models\\Page', 5, 'fullwidth', 2, '2016-09-07 15:09:08', '2016-09-07 15:09:08'),
(16, 13, NULL, 'Lavalite\\Page\\Models\\Page', 5, 'fullwidth', 3, '2016-09-07 15:20:38', '2016-09-07 15:20:38'),
(17, 14, NULL, 'Lavalite\\Page\\Models\\Page', 5, 'fullwidth', 4, '2016-09-07 15:31:35', '2016-09-07 15:31:35'),
(18, 15, NULL, 'Lavalite\\Page\\Models\\Page', 5, 'fullwidth', 5, '2016-09-07 15:39:02', '2016-09-07 15:39:02'),
(19, 16, NULL, 'Lavalite\\Page\\Models\\Page', 1, 'showcase', 0, '2016-09-07 17:33:06', '2022-05-11 13:03:11'),
(21, 3, NULL, 'Lavalite\\Page\\Models\\Page', 1, 'showcase', 1, '2016-09-11 13:55:30', '2022-05-11 13:03:11'),
(22, 17, NULL, 'Lavalite\\Page\\Models\\Page', 1, 'fullwidth', 2, '2016-09-11 14:06:09', '2022-05-11 13:03:11'),
(23, 18, NULL, 'Lavalite\\Page\\Models\\Page', 1, 'fullwidth', 3, '2016-09-11 14:58:13', '2022-05-11 13:03:11'),
(24, 19, NULL, 'Lavalite\\Page\\Models\\Page', 1, 'fullwidth', 4, '2016-11-08 19:15:20', '2022-05-11 13:03:11'),
(26, 21, NULL, 'Lavalite\\Page\\Models\\Page', 2, 'fullwidth', 3, '2016-11-14 16:28:39', '2022-03-30 01:01:22'),
(27, 22, NULL, 'Lavalite\\Page\\Models\\Page', 2, 'fullwidth', 4, '2016-11-15 11:54:15', '2022-03-30 01:01:22'),
(28, 17, NULL, 'Lavalite\\Page\\Models\\Page', 2, 'fullwidth', 1, '2016-11-15 11:59:18', '2022-03-30 01:01:22'),
(29, 1, NULL, 'Lavalite\\Page\\Models\\Page', 6, 'showcase', 0, '2018-06-04 14:47:00', '2018-06-04 16:18:06'),
(30, 23, NULL, 'Lavalite\\Page\\Models\\Page', 6, 'showcase', 1, '2018-06-04 16:18:06', '2018-06-04 16:18:06'),
(31, 24, NULL, 'Lavalite\\Page\\Models\\Page', 6, 'fullwidth', 2, '2018-06-04 16:18:06', '2018-06-04 16:18:06'),
(36, 27, NULL, 'Lavalite\\Page\\Models\\Page', 9, 'showcase', 0, '2022-05-03 20:05:35', '2022-05-05 13:29:24'),
(37, 28, NULL, 'Lavalite\\Page\\Models\\Page', 9, 'showcase', 1, '2022-05-03 20:14:36', '2022-05-05 13:29:24'),
(38, 29, NULL, 'Lavalite\\Page\\Models\\Page', 9, 'showcase', 2, '2022-05-03 20:14:36', '2022-05-05 13:29:24'),
(39, 30, NULL, 'Lavalite\\Page\\Models\\Page', 10, 'showcase', 0, '2022-05-04 15:32:59', '2022-05-08 19:22:58'),
(40, 31, NULL, 'Lavalite\\Page\\Models\\Page', 10, 'showcase', 1, '2022-05-04 18:00:25', '2022-05-08 19:41:24'),
(42, 34, NULL, 'Lavalite\\Page\\Models\\Page', 3, 'content-footer', 1, '2022-05-05 17:37:07', '2022-05-05 17:46:45'),
(43, 35, NULL, 'Lavalite\\Page\\Models\\Page', 10, 'showcase', 2, '2022-05-08 19:34:09', '2022-05-08 19:42:16'),
(44, 36, NULL, 'Lavalite\\Page\\Models\\Page', 10, 'showcase', 3, '2022-05-08 19:40:20', '2022-05-08 19:40:20'),
(45, 37, NULL, 'Lavalite\\Page\\Models\\Page', 10, 'showcase', 4, '2022-05-08 20:08:02', '2022-05-08 20:09:01'),
(46, 38, NULL, 'Lavalite\\Page\\Models\\Page', 10, 'showcase', 5, '2022-05-08 20:27:06', '2022-05-08 20:27:39'),
(47, 39, NULL, 'Lavalite\\Page\\Models\\Page', 10, 'content-header', 6, '2022-05-08 20:37:18', '2022-05-08 20:37:18'),
(48, 40, NULL, 'Lavalite\\Page\\Models\\Page', 11, 'showcase', 0, '2022-05-08 20:42:35', '2022-05-08 20:42:35'),
(51, 43, NULL, 'Lavalite\\Page\\Models\\Page', 11, 'content-header', 1, '2022-05-08 20:59:08', '2022-05-10 20:14:28'),
(54, 46, NULL, 'Lavalite\\Page\\Models\\Page', 11, 'showcase', 2, '2022-05-10 18:22:04', '2022-05-10 18:22:04'),
(55, 47, NULL, 'Lavalite\\Page\\Models\\Page', 1, 'fullwidth', 5, '2022-05-10 19:59:25', '2022-05-11 13:03:11'),
(56, 51, NULL, 'Lavalite\\Page\\Models\\Page', 1, 'content-header', 6, '2022-05-11 23:52:39', '2022-05-11 23:52:39');

-- --------------------------------------------------------

--
-- Table structure for table `connotes`
--

CREATE TABLE `connotes` (
  `id` int(10) UNSIGNED NOT NULL,
  `account_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `courier_consignment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `consignment` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `manifest_id` int(10) UNSIGNED DEFAULT NULL,
  `reference` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `items` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `weight` decimal(10,4) NOT NULL DEFAULT '0.1000',
  `width` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `height` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `length` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `parcel_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'carton',
  `declared_value` decimal(8,2) NOT NULL DEFAULT '0.00',
  `atl` tinyint(4) NOT NULL DEFAULT '0',
  `remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `instruction` text COLLATE utf8mb4_unicode_ci,
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'AUD',
  `is_printed` tinyint(4) NOT NULL DEFAULT '0',
  `params` text COLLATE utf8mb4_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `couriers`
--

CREATE TABLE `couriers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `label` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `access_key` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'custom',
  `params` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `couriers`
--

INSERT INTO `couriers` (`id`, `name`, `label`, `description`, `access_key`, `note`, `type`, `params`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'pronto', '', '', '6jYvYj0An1', '', 'pronto', '{\"consignment\":{\"init\":900433000000,\"current\":900433000000}}', 2, NULL, '2022-03-29 19:06:56', '2022-05-11 15:23:38');

-- --------------------------------------------------------

--
-- Table structure for table `logistic_histories`
--

CREATE TABLE `logistic_histories` (
  `id` int(10) UNSIGNED NOT NULL,
  `logistic_histories_id` int(10) UNSIGNED NOT NULL,
  `logistic_histories_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_type_id` int(10) UNSIGNED NOT NULL,
  `value` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comments` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `params` text COLLATE utf8mb4_unicode_ci,
  `by_user` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `manifests`
--

CREATE TABLE `manifests` (
  `id` int(10) UNSIGNED NOT NULL,
  `account_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `manifest_no` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` text COLLATE utf8mb4_unicode_ci,
  `sender_address_id` int(10) UNSIGNED DEFAULT NULL,
  `pickup_address_id` int(10) UNSIGNED DEFAULT NULL,
  `courier_id` int(10) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `medias`
--

CREATE TABLE `medias` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_path` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `source_url` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_size` int(11) NOT NULL DEFAULT '0',
  `mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `width` int(11) DEFAULT '0',
  `height` int(11) DEFAULT '0',
  `downloadable` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `allow_stretch` tinyint(4) NOT NULL DEFAULT '1',
  `use_cloud` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `media_links`
--

CREATE TABLE `media_links` (
  `media_link_id` int(10) UNSIGNED NOT NULL,
  `media_link_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `media_id` int(10) UNSIGNED NOT NULL,
  `ordering` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `grouping` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'image',
  `params` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `media_resizes`
--

CREATE TABLE `media_resizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `media_id` int(10) UNSIGNED NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `use_cloud` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL,
  `key` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `target` enum('_blank','_self') COLLATE utf8mb4_unicode_ci DEFAULT '_self',
  `order` int(11) DEFAULT NULL,
  `uload_folder` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `parent_id`, `key`, `url`, `icon`, `permission`, `role`, `name`, `description`, `target`, `order`, `uload_folder`, `status`, `deleted_at`, `created_at`, `updated_at`) VALUES
(0, 27, NULL, 'manifest', 'fa fa-cubes', '', NULL, 'Manifests', '', '_self', 3, NULL, 0, NULL, '2022-04-13 05:46:13', '2022-05-04 15:03:16'),
(1, 0, 'admin', 'admin/', NULL, NULL, NULL, 'Admin', NULL, NULL, 1, NULL, 1, NULL, NULL, NULL),
(2, 0, 'main', '', NULL, NULL, NULL, 'Main Menu', 'Website main menu', NULL, 2, NULL, 1, NULL, NULL, NULL),
(3, 0, 'footer', '', NULL, NULL, NULL, 'Footer', 'Footer menu', NULL, 3, NULL, 1, NULL, NULL, NULL),
(4, 1, 'content', 'admin/page/page', 'fa fa-book', NULL, NULL, 'Content', NULL, NULL, 1, NULL, 1, NULL, NULL, '2016-11-09 15:50:59'),
(5, 4, NULL, 'admin/page/page', 'fa fa-book', NULL, NULL, 'Pages', NULL, NULL, 0, NULL, 1, NULL, NULL, '2016-11-09 15:50:59'),
(7, 4, NULL, 'admin/menu/menu', 'fa fa-bars', NULL, NULL, 'Menu', NULL, NULL, 2, NULL, 1, NULL, NULL, '2022-05-05 13:13:33'),
(8, 2, NULL, '', NULL, NULL, NULL, 'Home', NULL, NULL, 7, NULL, 1, NULL, NULL, NULL),
(9, 2, NULL, 'about-us', NULL, '', '', 'About Us', NULL, NULL, 8, NULL, 1, NULL, NULL, '2022-05-04 20:45:16'),
(10, 2, NULL, 'contact', NULL, '', '', 'Contact Us', NULL, NULL, 9, NULL, 1, NULL, NULL, '2022-05-04 20:45:23'),
(11, 1, NULL, 'admin', 'fa fa-dashboard', NULL, NULL, 'Dashboard', '', NULL, 0, NULL, 0, NULL, NULL, '2017-07-19 18:05:16'),
(12, 1, NULL, 'admin/user/user', 'fa fa-users', '', '[\"superuser\",\"superuser\"]', 'Users', '', NULL, 3, NULL, 1, NULL, NULL, '2022-05-04 17:44:02'),
(13, 12, NULL, 'admin/user/user', 'fa fa-users', NULL, NULL, 'Users', NULL, NULL, 0, NULL, 1, NULL, NULL, '2016-11-09 15:50:59'),
(14, 12, NULL, 'admin/user/role', 'fa fa-user-plus', NULL, NULL, 'Roles', NULL, NULL, 1, NULL, 1, NULL, NULL, '2016-11-09 15:50:59'),
(15, 12, NULL, 'admin/user/permission', 'fa fa-check-circle-o', NULL, NULL, 'Permissions', NULL, NULL, 2, NULL, 1, NULL, NULL, '2016-11-09 15:50:59'),
(16, 3, NULL, '/', '', '', '', 'Home', '', '_self', 0, NULL, 1, NULL, NULL, '2022-04-20 20:08:53'),
(17, 3, NULL, 'about-us', NULL, '', '', 'About Us', NULL, '_self', 1, NULL, 1, NULL, NULL, '2022-05-04 20:46:05'),
(18, 3, NULL, 'services', NULL, '', '', 'Services', NULL, '_self', 2, NULL, 1, NULL, NULL, '2022-05-04 20:46:11'),
(19, 3, NULL, 'login', '', '', '[\"guest\",\"guest\"]', 'Login', '', '_self', 3, NULL, 1, NULL, '2016-09-06 18:54:39', '2022-04-20 19:43:03'),
(20, 3, NULL, 'contact-us', NULL, '', '', 'Contact Us', NULL, '_self', 5, NULL, 1, NULL, '2016-09-06 18:54:50', '2022-05-04 20:46:17'),
(21, 0, 'selfmanage', NULL, NULL, NULL, NULL, 'Self Manage', '', '_self', 0, NULL, 1, NULL, '2016-10-03 16:59:38', '2016-10-03 17:00:28'),
(22, 21, NULL, '/selfmanage/booking/search', 'fa fa-pencil-square-o', NULL, NULL, 'Manage Booking', '', '_self', 1, NULL, 1, NULL, '2016-10-04 12:33:22', '2021-07-28 20:44:30'),
(23, 21, NULL, '/selfmanage/booking', 'fa fa-location-arrow', NULL, NULL, 'New Booking', '', '_self', 0, NULL, 1, NULL, '2016-10-04 12:36:52', '2021-07-28 20:44:30'),
(24, 21, NULL, '/selfmanage/booking/history', 'fa fa-history', NULL, NULL, 'View Booking History', '', '_self', 2, NULL, 1, NULL, '2016-10-04 12:46:54', '2021-07-28 20:44:30'),
(25, 21, NULL, '/selfmanage/dashboard', 'fa fa fa-dashboard', NULL, NULL, 'Dashboard', '', '_self', 0, NULL, 1, '2017-07-19 14:00:00', '2016-10-04 12:49:54', '2016-10-04 13:28:50'),
(26, 21, NULL, '/selfmanage/manage-address', 'fa fa-flag-checkered', NULL, NULL, 'Manage Address', '', '_self', 4, NULL, 1, NULL, '2016-10-04 12:54:36', '2016-10-10 12:07:52'),
(27, 0, 'manage', NULL, NULL, NULL, NULL, 'Logistic', '', '_self', 0, NULL, 1, NULL, '2016-10-18 17:15:34', '2022-04-13 05:44:59'),
(28, 27, NULL, 'connote', 'fa fa-cube', '', NULL, 'Tracking Connotes', '', '_self', 2, NULL, 1, NULL, '2016-10-18 17:16:23', '2022-05-04 15:03:16'),
(29, 27, NULL, 'address', 'fa fa-book', '', '', 'Address Books', NULL, '_self', 4, NULL, 1, NULL, '2016-10-23 11:55:23', '2022-05-08 18:51:59'),
(30, 27, NULL, 'connote-create', 'fa fa-cube', '', '', 'Create Connotes', '{!! view(\'Logistic::connote.partials.connote-indicator\',compact([\'menu\']))!!}', '_self', 1, NULL, 1, NULL, '2016-11-02 16:58:52', '2022-05-04 15:03:16'),
(31, 0, 'policies', NULL, NULL, NULL, NULL, 'Policy', '', '_self', 0, NULL, 1, NULL, '2018-06-04 16:14:43', '2018-06-04 16:14:43'),
(32, 31, NULL, 'tnc.html', '', NULL, NULL, 'Terms and Conditions', '', '_self', NULL, NULL, 1, NULL, '2018-06-04 16:15:12', '2018-06-04 16:16:25'),
(33, 21, NULL, '/selfmanage/proof-of-delivery', 'fa fa-truck', NULL, NULL, 'Proof of Delivery', '', '_self', 3, NULL, 1, NULL, '2021-07-28 20:44:24', '2021-07-28 20:45:01'),
(34, 27, NULL, 'account/users', 'fa fa-users', '', '', 'Users', '', '_self', 5, NULL, 1, NULL, '2022-04-19 21:40:42', '2022-05-04 15:03:16'),
(35, 27, NULL, 'home', 'fa fa-dashboard', '', NULL, 'Dashboard', '', '_self', 0, NULL, 1, NULL, '2022-04-20 14:30:47', '2022-05-04 15:03:16'),
(36, 3, NULL, 'home', '', '', '[\"registered\",\"registered\"]', 'My Account', '', '_self', 4, NULL, 1, NULL, '2022-04-20 19:43:25', '2022-04-20 19:43:34'),
(37, 38, NULL, 'admin/accounts', 'fa fa-users', '', '', 'Accounts', '', '_self', 2, NULL, 1, NULL, '2022-04-21 16:19:55', '2022-05-05 13:13:33'),
(38, 1, NULL, '#', 'fa fa-truck', '', '[\"superuser\",\"superuser\"]', 'Logistics', '', '_self', 2, NULL, 1, NULL, '2022-04-21 16:23:53', '2022-05-04 17:42:34'),
(39, 38, NULL, 'admin/logistic/connotes', 'fa fa-cube', '', '', 'Connotes', '', '_self', 0, NULL, 1, NULL, '2022-04-21 16:25:49', '2022-05-05 13:13:33'),
(40, 38, NULL, 'admin/logistic/manifests', 'fa fa-cubes', '', '', 'Manifests', '', '_self', 1, NULL, 1, NULL, '2022-04-21 16:26:40', '2022-05-05 13:13:33'),
(41, 27, NULL, 'account/users', 'fa fa-book', '', '', 'Manage Accounts', NULL, '_self', 6, NULL, 0, NULL, '2022-05-04 15:03:04', '2022-05-05 13:11:49'),
(42, 4, NULL, 'admin/medias', 'fa fa-picture-o', '', '', 'Medias', NULL, '_self', 1, NULL, 1, NULL, '2022-05-05 13:13:21', '2022-05-05 13:13:33');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_item_groups`
--

CREATE TABLE `model_item_groups` (
  `group_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `grouptable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_data` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `grouptable_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `banner` mediumtext COLLATE utf8mb4_unicode_ci,
  `view` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'page',
  `compiler` enum('php','blade','twif','none') COLLATE utf8mb4_unicode_ci DEFAULT 'none',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `upload_folder` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `heading` text COLLATE utf8mb4_unicode_ci,
  `title` text COLLATE utf8mb4_unicode_ci,
  `content` text COLLATE utf8mb4_unicode_ci,
  `keyword` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `images` text COLLATE utf8mb4_unicode_ci,
  `abstract` text COLLATE utf8mb4_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `name`, `slug`, `order`, `banner`, `view`, `compiler`, `status`, `upload_folder`, `heading`, `title`, `content`, `keyword`, `description`, `images`, `abstract`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '{\"en\":\"home.guest\"}', 'home', 0, NULL, 'page', 'blade', 1, '{\"folder\":\"pages\\/0000\\/00\\/00\\/000000000\",\"encrypted\":\"pages\\/ekkRckUjhEdn4G\"}', '{\"en\":null}', '{\"en\":\"Home\"}', '{\"en\":\"<p><br><\\/p>\"}', '{\"en\":null}', '{\"en\":null}', '[]', NULL, NULL, NULL, '2022-05-11 23:49:32'),
(2, '{\"en\":\"About Us\"}', 'about-us', 0, NULL, 'page', 'blade', 1, '{\"folder\":\"pages\\/2016\\/09\\/07\\/052059188\",\"encrypted\":\"pages\\/VzzgueUKhBNxlb\"}', '{\"en\":\"\"}', '{\"en\":\"About Us\"}', '{\"en\":\"\"}', '{\"en\":\"\"}', '{\"en\":\"\"}', '[]', NULL, NULL, NULL, '2022-03-30 01:01:22'),
(3, '{\"en\":\"Contact Us\"}', 'contact-us', 0, NULL, 'page', 'blade', 1, '{\"folder\":\"pages\\/2016\\/09\\/07\\/065509368\",\"encrypted\":\"pages\\/WKKrTwU7hzgxgq\"}', '{\"en\":null}', '{\"en\":\"Contact Us\"}', '{\"en\":\"<div class=\\\"modal fade\\\" tabindex=\\\"-1\\\" role=\\\"dialog\\\" id=\\\"contact_us_map\\\">\\r\\n\\r\\n  <div class=\\\"modal-dialog modal-lg\\\" role=\\\"document\\\">\\r\\n\\r\\n    <div class=\\\"modal-content\\\">\\r\\n\\r\\n      <div class=\\\"modal-header\\\">\\r\\n\\r\\n        <h4 class=\\\"modal-title\\\">Quatius Logistics Location<\\/h4>\\r\\n\\r\\n\\r\\n      <\\/div>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n      <div class=\\\"modal-body\\\" id=\\\"map\\\" style=\\\"position: relative; overflow: hidden;\\\"><div style=\\\"height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; background-color: rgb(229, 227, 223);\\\"><div style=\\\"overflow: hidden;\\\"><\\/div><div class=\\\"gm-style\\\" style=\\\"position: absolute; z-index: 0; left: 0px; top: 0px; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px;\\\"><div tabindex=\\\"0\\\" aria-label=\\\"Map\\\" aria-roledescription=\\\"map\\\" role=\\\"region\\\" style=\\\"position: absolute; z-index: 0; left: 0px; top: 0px; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; cursor: url(&quot;https:\\/\\/maps.gstatic.com\\/mapfiles\\/openhand_8_8.cur&quot;), default; touch-action: pan-x pan-y;\\\"><div style=\\\"z-index: 1; position: absolute; left: 50%; top: 50%; width: 100%; transform: translate(0px, 0px);\\\"><div style=\\\"position: absolute; left: 0px; top: 0px; z-index: 100; width: 100%;\\\"><div style=\\\"position: absolute; left: 0px; top: 0px; z-index: 0;\\\"><div style=\\\"position: absolute; z-index: 984; transform: matrix(1, 0, 0, 1, -43, -200);\\\"><div style=\\\"position: absolute; left: 0px; top: 0px; width: 256px; height: 256px;\\\"><div style=\\\"width: 256px; height: 256px;\\\"><\\/div><\\/div><\\/div><\\/div><\\/div><div style=\\\"position: absolute; left: 0px; top: 0px; z-index: 101; width: 100%;\\\"><\\/div><div style=\\\"position: absolute; left: 0px; top: 0px; z-index: 102; width: 100%;\\\"><\\/div><div style=\\\"position: absolute; left: 0px; top: 0px; z-index: 103; width: 100%;\\\"><div style=\\\"position: absolute; left: 0px; top: 0px; z-index: -1;\\\"><div style=\\\"position: absolute; z-index: 984; transform: matrix(1, 0, 0, 1, -43, -200);\\\"><div style=\\\"width: 256px; height: 256px; overflow: hidden; position: absolute; left: 0px; top: 0px;\\\"><\\/div><\\/div><\\/div><div style=\\\"width: 27px; height: 43px; overflow: hidden; position: absolute; left: 371px; top: 126px; z-index: 169; animation-duration: 700ms; animation-iteration-count: infinite; animation-name: _gm854;\\\"><img alt=\\\"\\\" src=\\\"https:\\/\\/maps.gstatic.com\\/mapfiles\\/api-3\\/images\\/spotlight-poi2.png\\\" draggable=\\\"false\\\" style=\\\"position: absolute; left: 0px; top: 0px; width: 27px; height: 43px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;\\\"><\\/div><\\/div><div style=\\\"position: absolute; left: 0px; top: 0px; z-index: 0;\\\"><\\/div><\\/div><div style=\\\"z-index: 3; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px; touch-action: pan-x pan-y;\\\"><div style=\\\"z-index: 4; position: absolute; left: 50%; top: 50%; width: 100%; transform: translate(0px, 0px);\\\"><div style=\\\"position: absolute; left: 0px; top: 0px; z-index: 104; width: 100%;\\\"><\\/div><div style=\\\"position: absolute; left: 0px; top: 0px; z-index: 105; width: 100%;\\\"><\\/div><div style=\\\"position: absolute; left: 0px; top: 0px; z-index: 106; width: 100%;\\\"><span id=\\\"CE6DE0E7-6DFB-4604-82EF-591BF5389ADA\\\" style=\\\"display: none;\\\">To navigate, press the arrow keys.<\\/span><div aria-label=\\\"Quatius Logistics!\\\" role=\\\"img\\\" tabindex=\\\"-1\\\" style=\\\"width: 27px; height: 43px; overflow: hidden; position: absolute; left: 371px; top: 126px; z-index: 169;\\\"><img alt=\\\"\\\" src=\\\"https:\\/\\/maps.gstatic.com\\/mapfiles\\/transparent.png\\\" draggable=\\\"false\\\" usemap=\\\"#gmimap0\\\" style=\\\"width: 27px; height: 43px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;\\\"><map name=\\\"gmimap0\\\" id=\\\"gmimap0\\\"><area log=\\\"miw\\\" coords=\\\"13.5,0,4,3.75,0,13.5,13.5,43,27,13.5,23,3.75\\\" shape=\\\"poly\\\" tabindex=\\\"-1\\\" title=\\\"Quatius Logistics!\\\" style=\\\"display: inline; position: absolute; left: 0px; top: 0px; cursor: pointer; touch-action: none;\\\"><\\/map><\\/div><\\/div><div style=\\\"position: absolute; left: 0px; top: 0px; z-index: 107; width: 100%;\\\"><\\/div><\\/div><\\/div><div class=\\\"gm-style-moc\\\" style=\\\"z-index: 4; position: absolute; height: 100%; width: 100%; padding: 0px; border-width: 0px; margin: 0px; left: 0px; top: 0px; opacity: 0;\\\"><p class=\\\"gm-style-mot\\\"><\\/p><\\/div><\\/div><iframe aria-hidden=\\\"true\\\" frameborder=\\\"0\\\" tabindex=\\\"-1\\\" style=\\\"z-index: -1; position: absolute; width: 100%; height: 100%; top: 0px; left: 0px; border: none;\\\"><\\/iframe><div style=\\\"pointer-events: none; width: 100%; height: 100%; box-sizing: border-box; position: absolute; z-index: 1000002; opacity: 0; border: 2px solid rgb(26, 115, 232);\\\"><\\/div><\\/div><\\/div><\\/div>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n      <div class=\\\"modal-footer\\\">\\r\\n\\r\\n        <button type=\\\"button\\\" class=\\\"btn btn-default\\\" data-dismiss=\\\"modal\\\">Close<\\/button>        \\r\\n\\r\\n      <\\/div>\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n\\r\\n    <\\/div>\\r\\n\\r\\n\\r\\n\\r\\n<!-- \\/.modal-content -->\\r\\n\\r\\n  <\\/div>\\r\\n\\r\\n\\r\\n\\r\\n<!-- \\/.modal-dialog -->\\r\\n\\r\\n<\\/div>\\r\\n\\r\\n\\r\\n\\r\\n<!-- \\/.modal -->\\r\\n<style>\\r\\n\\t#contact_us_map{\\r\\n\\t\\theight: 100%;\\r\\n        margin: 0;\\r\\n        padding: 0;\\r\\n\\t}\\r\\n\\t#map{\\r\\n\\t\\twidth: 100%;\\r\\n\\t\\theight: 500px;\\r\\n\\t\\t\\r\\n\\t}\\r\\n<\\/style>\\r\\n\\r\\n<script>\\r\\n\\r\\n    function initMap() {\\r\\n\\r\\n        var myLatLng = {lat: -38.0326999, lng: 145.22646};\\r\\n\\r\\n\\t\\tvar tmpLatLng = {lat: -38.0298439, lng: 145.2182067};\\r\\n\\r\\n        var map = new google.maps.Map(document.getElementById(\'map\'), {\\r\\n\\r\\n          zoom: 16,\\r\\n\\r\\n          center: tmpLatLng\\r\\n\\r\\n        });\\r\\n\\r\\n        var marker = new google.maps.Marker({\\r\\n\\r\\n          position: myLatLng,\\r\\n\\r\\n          map: map,\\r\\n\\t\\t  \\r\\n          title: \'Quatius Logistics!\',\\r\\n\\t\\t  \\r\\n\\t\\t  animation: google.maps.Animation.BOUNCE\\r\\n        });\\r\\n\\t\\tjQuery(\'#contact_us_map\').on(\\\"shown.bs.modal\\\", function () {\\r\\n\\t\\t\\t\\tgoogle.maps.event.trigger(map, \\\"resize\\\");\\r\\n\\t\\t\\t});\\r\\n    }\\r\\n\\r\\n\\t\\r\\n\\r\\n\\tjQuery(document).ready(function(){\\r\\n\\r\\n\\t\\tjQuery(\'#contact_us_map\').modal(\'hide\');\\r\\n\\r\\n\\t\\tjQuery(\'#display_map\').click(function(){\\t\\t\\t\\r\\n\\t\\t\\tjQuery(\'#contact_us_map\').modal(\'show\');\\r\\n\\t\\t\\t\\r\\n\\t\\t});\\r\\n\\t\\t\\r\\n\\t});\\r\\n\\r\\n<\\/script>\\r\\n\\r\\n<script async=\\\"\\\" defer=\\\"\\\" src=\\\"https:\\/\\/maps.googleapis.com\\/maps\\/api\\/js?key=AIzaSyDNiVmjLLYGzp3oXxcpIf8pKO4HiZerzN0&amp;callback=initMap\\\">\\r\\n    <\\/script>\"}', '{\"en\":null}', '{\"en\":null}', '[]', NULL, NULL, NULL, '2022-05-10 16:53:56'),
(4, '{\"en\":\"Page Not found\"}', '404', 0, NULL, 'page', 'blade', 1, '{\"folder\":\"pages\\/2022\\/05\\/05\\/232158386\",\"encrypted\":\"pages\\/NllduKF9FkxMW5O\"}', '{\"en\":\"Page Not found\"}', '{\"en\":\"Page Not found\"}', '{\"en\":\"<p><br><\\/p>\"}', '{\"en\":null}', '{\"en\":null}', NULL, NULL, NULL, NULL, '2022-05-05 13:22:05'),
(5, '{\"en\":\"Services\"}', 'services', 50, NULL, 'page', '', 1, '{\"folder\":\"pages\\/2016\\/09\\/08\\/003359548\",\"encrypted\":\"pages\\/Yjjni9U7uPLxB\"}', '{\"en\":\"\"}', '{\"en\":\"Services\"}', '{\"en\":\"\"}', '{\"en\":\"\"}', '{\"en\":\"\"}', '[]', NULL, NULL, '2016-09-07 14:36:27', '2016-09-07 15:09:07'),
(6, '{\"en\":\"STANDARD TERMS AND CONDITIONS OF CONTRACT\"}', 'tnc', 50, NULL, 'page', '', 1, '{\"folder\":\"pages\\/2018\\/06\\/05\\/004512476\",\"encrypted\":\"pages\\/7ggnFjcBFrnV3e\"}', '{\"en\":\"\"}', '{\"en\":\"Terms & Conditions\"}', '{\"en\":\"<p style=\\\"text-align: center; \\\"><br><\\/p>\"}', '{\"en\":\"\"}', '{\"en\":\"\"}', '[]', NULL, NULL, '2018-06-04 14:46:59', '2018-06-04 16:18:06'),
(7, '{\"en\":\"Seyla\"}', 'seyla', 50, NULL, 'page', 'blade', 1, '{\"folder\":\"pages\\/2022\\/03\\/29\\/032223525\",\"encrypted\":\"pages\\/P22BuNs1Ipjk5q\"}', '{\"en\":null}', '{\"en\":null}', '{\"en\":\"<p><br><\\/p>\"}', '{\"en\":null}', '{\"en\":null}', '[]', NULL, '2022-05-05 13:16:38', '2022-03-28 16:22:47', '2022-05-05 13:16:38'),
(8, '{\"en\":\"Feature Page -- All pages\"}', 'allpages', 50, NULL, 'page', 'blade', 1, '{\"folder\":\"pages\\/2022\\/03\\/31\\/025155395\",\"encrypted\":\"pages\\/ekkDt3snsLYbX1\"}', NULL, '{\"en\":\"Feature Page -- All pages\"}', NULL, '{\"en\":\"\"}', '{\"en\":\"\"}', NULL, NULL, NULL, '2022-03-30 15:52:23', '2022-03-30 15:52:23'),
(9, '{\"en\":\"Career\"}', 'career', 50, NULL, 'page', 'blade', 1, '{\"folder\":\"pages\\/2022\\/05\\/04\\/060509334\",\"encrypted\":\"pages\\/X44VSoFlS1ogYo\"}', '{\"en\":null}', '{\"en\":\"Career\"}', '{\"en\":\"<p><br><\\/p>\"}', '{\"en\":null}', '{\"en\":null}', NULL, NULL, NULL, '2022-05-03 20:05:34', '2022-05-05 13:21:26'),
(10, '{\"en\":\"National & Group Co-ordinator\"}', 'national-group-coordinator', 50, NULL, 'page', 'blade', 1, '{\"folder\":\"pages\\/2022\\/05\\/05\\/013210462\",\"encrypted\":\"pages\\/NllduKF9FA1ALq\"}', '{\"en\":null}', '{\"en\":\"National & Group Co-ordinator\"}', '{\"en\":\"<p><br><\\/p>\"}', '{\"en\":null}', '{\"en\":null}', NULL, NULL, NULL, '2022-05-04 15:32:58', '2022-05-08 20:40:50'),
(11, '{\"en\":\"Career- Application Form\"}', 'career-application', 50, NULL, 'page', 'blade', 1, '{\"folder\":\"pages\\/2022\\/05\\/09\\/063759325\",\"encrypted\":\"pages\\/rRRWtKFWUKqVpD\"}', '{\"en\":null}', '{\"en\":\"Career- Application Form\"}', '{\"en\":\"<p><br><\\/p>\"}', '{\"en\":null}', '{\"en\":null}', NULL, NULL, NULL, '2022-05-08 20:38:14', '2022-05-08 20:46:25');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `slug`, `name`, `created_at`, `updated_at`) VALUES
(1, 'user.user.view', 'View user', NULL, NULL),
(2, 'user.user.create', 'Create user', NULL, NULL),
(3, 'user.user.edit', 'Update user', NULL, NULL),
(4, 'user.user.delete', 'Delete user', NULL, NULL),
(5, 'menu.menu.view', 'View Menu', NULL, NULL),
(6, 'menu.menu.create', 'Create Menu', NULL, NULL),
(7, 'menu.menu.edit', 'Update Menu', NULL, NULL),
(8, 'menu.menu.delete', 'Delete Menu', NULL, NULL),
(9, 'page.page.view', 'View Page', NULL, NULL),
(10, 'page.page.create', 'Create Page', NULL, NULL),
(11, 'page.page.edit', 'Update Page', NULL, NULL),
(12, 'page.page.delete', 'Delete Page', NULL, NULL),
(13, 'settings.setting.view', 'View Setting', NULL, NULL),
(14, 'settings.setting.create', 'Create Setting', NULL, NULL),
(15, 'settings.setting.edit', 'Update Setting', NULL, NULL),
(16, 'settings.setting.delete', 'Delete Setting', NULL, NULL),
(17, 'access.admin.backend', 'Access Back-end', '2022-05-04 17:41:30', '2022-05-04 17:41:30'),
(18, 'access.public.frontend', 'Access Front-end', '2022-05-04 20:24:16', '2022-05-04 20:24:16');

-- --------------------------------------------------------

--
-- Table structure for table `revisions`
--

CREATE TABLE `revisions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cast` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `old_value` text COLLATE utf8mb4_unicode_ci,
  `new_value` text COLLATE utf8mb4_unicode_ci,
  `revision_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revision_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'superuser', NULL, NULL, NULL),
(2, 'admin', '{\"access.admin.backend\":\"1\"}', NULL, '2022-05-04 17:41:43'),
(3, 'manager', '[]', NULL, '2022-04-20 16:24:51'),
(4, 'guest', NULL, NULL, NULL),
(5, 'registered', '{\"access.public.frontend\":\"1\"}', NULL, '2022-05-04 20:24:39');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(3, 5),
(2, 5),
(5, 1),
(7, 1),
(8, 5),
(9, 5),
(11, 2),
(11, 5),
(3, 2),
(17, 2),
(17, 5),
(2, 3),
(5, 3),
(5, 5),
(8, 2),
(21, 3),
(21, 5),
(22, 3),
(22, 5),
(23, 5);

-- --------------------------------------------------------

--
-- Table structure for table `saves`
--

CREATE TABLE `saves` (
  `user_id` int(11) NOT NULL,
  `saveable_id` int(10) UNSIGNED NOT NULL,
  `saveable_type` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skey` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('System','Default','User') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `upload_folder` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `status_types`
--

CREATE TABLE `status_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `publish_start` timestamp NULL DEFAULT NULL,
  `publish_end` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `reporting_to` int(11) DEFAULT '0',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` text COLLATE utf8mb4_unicode_ci,
  `status` enum('New','Active','Suspended') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'New',
  `api_token` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sex` enum('other','male','female') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_1` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_2` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postcode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` longtext COLLATE utf8mb4_unicode_ci,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `account_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `reporting_to`, `name`, `username`, `email`, `password`, `params`, `status`, `api_token`, `remember_token`, `sex`, `first_name`, `middle_name`, `last_name`, `designation`, `phone_1`, `phone_2`, `address_1`, `address_2`, `city`, `postcode`, `state`, `country`, `photo`, `permissions`, `deleted_at`, `created_at`, `updated_at`, `account_id`) VALUES
(1, 0, 'Super User', NULL, 'super@mail.com', '$2y$10$BSlZYDVMUcTLvkW77sRfI.gqB5CHQ1gvr.5i7WDb9dFqhU0KHym1K', NULL, 'Active', 'ictlPtSaLx6iXVLZYyMBDir4SeViViKT5kVQcrE0SpAENGSwQ9nQwykezrpe', 'RvXKEXfUGzJot0Amfi3a9zvF1kjvKu6l1Jljob3YDLzIBn61ZOtD60uUpRe4', NULL, NULL, NULL, NULL, 'Staff', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-09-14 14:00:00', '2022-05-04 17:37:13', NULL),
(2, 0, 'Admin', NULL, 'admin@quatiuslogistics.com', '$2y$10$BSlZYDVMUcTLvkW77sRfI.gqB5CHQ1gvr.5i7WDb9dFqhU0KHym1K', NULL, 'Active', '036af1AVMTgJeX2TNfPcFI7qKtzBDj7kyI5Ik0JJjorWdrc7zHB6z2bT1OQw', 'YTkRw0qqhb9eoG1TgCb3Qf1SMQfqG3tIsmHKUcy5UtwqjdNqF1Q7r1CRbz4b', NULL, NULL, NULL, NULL, 'Staff', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-09-14 14:00:00', '2017-10-25 17:33:46', NULL),
(3, 0, 'John Jiang', NULL, 'john.jiang@quatiuslogistics.com', '$2y$10$BSlZYDVMUcTLvkW77sRfI.gqB5CHQ1gvr.5i7WDb9dFqhU0KHym1K', NULL, 'Active', '4qQqEEH6OrZ1U9yC3ChoJYzotUc15npNl5WkVoXYTLiWSDGkDF5oOV464FiS', 'zuePZ74vMFvpQYYSEeBK2HN9L2Gr4s07rsVwlDjcQd6YRBCrl3yM6iZ6ScFd', NULL, NULL, NULL, NULL, 'Staff', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2015-09-14 14:00:00', '2022-04-28 17:55:03', 1),
(5, 0, 'Vey Chea1', NULL, 'vey.chea@soniq.com', '57756477679306585d32a9e3498aad59:HS4xIDQU48v70YJX3olNVxBy2y6No0cE', NULL, 'Active', 'ictlPtSaLx6iXVLZYyMBDir4SeViViKT5kVQcrE0SpAENGSwQ9nQwykez', 'eNtPWadJceBsluBxbY5hR81HwdQEVE0AYuLGmVCOY4nj3vYaDmqNfFThqPYE', NULL, NULL, NULL, NULL, 'Staff', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-09-25 14:33:37', '2022-02-24 16:54:30', NULL),
(6, 0, 'Jack Wilshare', NULL, 'jack@optusnet.com.au', '$2y$10$t3hqWuDyuTygcZ0M0Y86sO51JL9sbvBu//AmbvkRDq1BQhdIYw2.C', NULL, 'Active', 'e87becda20f3c39336a6663ccd375e4561cccb17', 'INULUPx3qI6Wg6ZTQG7o9EWt1kvONSHIbJqvRPH4c833DChHJvnKs3mY2i3O', NULL, NULL, NULL, NULL, 'Staff', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-10-25 17:33:32', '2017-10-25 17:54:48', NULL),
(7, 0, 'Jason Cheng', NULL, 'jason.cheng@soniq.com', 'cd3773267e58988bb9921e41b152ad3e:doQqDodmdxtdnp16Yvlk9QM6NeU53qNh', NULL, 'Active', '8p1IxxXVdYO2Zt20hT8gxs5FELwL44LSJB69QEd0NxbgRu8ToM9b8acxwjHt', 'iCTaiU3j7s9wOuqKLBPVZPHfkR9EsmMpageFk4tcC2tf5gZi4fmkpe7NCERt', NULL, NULL, NULL, NULL, 'Staff', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-21 18:52:21', '2022-03-08 15:27:08', NULL),
(8, 0, 'Kevin Tia', NULL, 'kevin.tia@soniq.com', '$2y$10$BSlZYDVMUcTLvkW77sRfI.gqB5CHQ1gvr.5i7WDb9dFqhU0KHym1K', NULL, 'Active', 'STa58i4OpfpST61Suq3IGD3DgGO02zfYAaqp5A5SFwpltCL0U6Dt43RSIgXM', 'gmcl2lm6Tq30sn2wOnDBpzkRjIjeU7V0Nj8zd8K12i6ip6xoucK7Ae3ck2IK', NULL, NULL, NULL, NULL, 'Staff', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-15 15:54:46', '2022-05-04 17:46:26', NULL),
(9, 0, 'QL Manager', NULL, 'manage@quatiuslogistics.com', '484fe9a541a62ad4ac060aac30b1c592:2KszDU6IFCqO9EsaR1JoKntgqAUzSMFk', NULL, 'Active', '6Vz0xSejc7G4FepunoxD0RXAkSbTXt3StjtZBHymN30UWDfUgKCdwcmHxnAD', 'hCcczYZ9kxZhZxPGdlwQiUNP6E7YgqH9MJeOJ838NTnBNMB6Whdt2dSeDW0b', NULL, NULL, NULL, NULL, 'Staff', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-02 19:40:14', '2021-09-02 19:58:32', NULL),
(11, 0, 'Seyla Keth', NULL, 'seyla.keth@quatius.com.au', '$2y$10$HU5s3xCsId8TPJ7c9fGi0urss0fF1hZQUYZhrW2nxB.8K4rPEJU4e', NULL, 'Active', '9XZjejSpuwqmTbBI69GwhqIkhrI4zrni4rePWLx0MOiub6LpdxIn1w3TvazW', 'RT4G11yhLCO9lX8J8ct4a1PeiaQLRUfTTBL6fuWJ4vlSYqoOrW61LjaACLJf', NULL, NULL, NULL, NULL, 'Staff', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-03-28 16:18:28', '2022-04-27 20:06:54', 2),
(17, 0, 'Seng', NULL, 'seng.kheang@soniq.com', '$2y$10$70QBlLab1Dmr6bIdz64qxub/A9b.nRhkaYZTjf4.YRZkkRi0hUsjq', '{\"logistic\":{\"prefer\":{\"sender\":57,\"pickup\":57}}}', 'Active', 'rL4hWUeXzk5nIx35lOwf13E8Fc5T9TNvh58oaj37yxuILKaK1cCV1Avo8XRO', '1vclRurpvwldKdvOncdNRiyYLNJE5ss1QA5jpyBm0FsaxUnVIBPg2hzXkhOQ', NULL, NULL, NULL, NULL, 'Staff', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-04-19 22:06:07', '2022-04-19 22:06:07', 1),
(21, 0, 'Song', NULL, 'song.su@quatiuslogistics.com', '$2y$10$Ih0L3d65nhsQx6gxR.tfaOSzGSTtE0M17I5nLFYmxvWt3fA.eJgOu', '{\"logistic\":{\"prefer\":{\"sender\":1,\"pickup\":1}}}', 'Active', 'pwGo7zilJ40HRA135CuoI741fP4Stv1N2mhPvo7h6N3XVxBbrW0KwK5Ooczu', 'FvoZUcHHEk9GzyHNWCT0BznjXfBwvKR0LwBIhwdHTgbZXlMv570OqYgoGDtF', NULL, NULL, NULL, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-05-08 18:12:26', '2022-05-08 18:12:26', 2),
(22, 0, 'Anna', NULL, 'anna.uy@soniq.com', '$2y$10$yRQ.B18nL35XmiMw8g8B4.eBzlnFhBKXiPMlohOhY9jvMbU99.zVy', '{\"logistic\":{\"prefer\":{\"sender\":8,\"pickup\":8}}}', 'Active', 'ZNbpyavaFSIrLRIl8uYeIzhjBA246CYM2ezbm1rre0IeHC4ieiTkg1BkvJHf', 'RHUppcMURiLHgFSSr4OhkcX2pPYAukqkjXgNeD0L83Dhn4gW0zTHocYO7MvD', NULL, NULL, NULL, NULL, 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-05-11 15:36:30', '2022-05-11 15:36:30', 2),
(23, 0, 'Alice', NULL, 'Alice.Luo@quatiuslogistics.com', '$2y$10$7imD/bjDkHUN6XodJmpdHOVS0UVIQoPOk6Cy06HGNozg1RwwAps.2', '{\"logistic\":{\"prefer\":{\"sender\":9,\"pickup\":9}}}', 'Active', 'Xmn0VJAf7LDjAI1v4M7uRhGq1VT8iMJxb8iPzd8Wd9dZzPkKsh13aYWTDADw', 'rwAp5amw4qYBjDXzMljJrhzNyuTOE61ljJxBnEgOStvS0oFDviMomO9Emo6G', NULL, NULL, NULL, NULL, 'staff', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2022-05-11 15:43:39', '2022-05-11 15:43:39', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `components`
--
ALTER TABLE `components`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `component_placements`
--
ALTER TABLE `component_placements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `component_placements_component_id_foreign` (`component_id`);

--
-- Indexes for table `connotes`
--
ALTER TABLE `connotes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `connotes_consignment_unique` (`consignment`),
  ADD KEY `connotes_manifest_id_index` (`manifest_id`);

--
-- Indexes for table `couriers`
--
ALTER TABLE `couriers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `couriers_access_key_unique` (`access_key`);

--
-- Indexes for table `logistic_histories`
--
ALTER TABLE `logistic_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `logistic_histories_status_type_id_index` (`status_type_id`);

--
-- Indexes for table `manifests`
--
ALTER TABLE `manifests`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `manifests_manifest_no_unique` (`manifest_no`);

--
-- Indexes for table `medias`
--
ALTER TABLE `medias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media_links`
--
ALTER TABLE `media_links`
  ADD KEY `media_links_media_id_foreign` (`media_id`);

--
-- Indexes for table `media_resizes`
--
ALTER TABLE `media_resizes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_resizes_media_id_foreign` (`media_id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191)),
  ADD KEY `password_resets_token_index` (`token`(191));

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_slug_unique` (`slug`);

--
-- Indexes for table `revisions`
--
ALTER TABLE `revisions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `revisions_revision_id_revision_type_index` (`revision_id`,`revision_type`(191)),
  ADD KEY `revisions_user_id_index` (`user_id`),
  ADD KEY `revisions_field_index` (`field`(191));

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD KEY `role_user_user_id_index` (`user_id`),
  ADD KEY `role_user_role_id_index` (`role_id`);

--
-- Indexes for table `saves`
--
ALTER TABLE `saves`
  ADD PRIMARY KEY (`user_id`,`saveable_id`,`saveable_type`),
  ADD KEY `saves_user_id_index` (`user_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_types`
--
ALTER TABLE `status_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `status_types_type_unique` (`type`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tags_name_unique` (`name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_api_token_unique` (`api_token`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `components`
--
ALTER TABLE `components`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `component_placements`
--
ALTER TABLE `component_placements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `connotes`
--
ALTER TABLE `connotes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `couriers`
--
ALTER TABLE `couriers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `logistic_histories`
--
ALTER TABLE `logistic_histories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `manifests`
--
ALTER TABLE `manifests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `medias`
--
ALTER TABLE `medias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `media_resizes`
--
ALTER TABLE `media_resizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `revisions`
--
ALTER TABLE `revisions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `status_types`
--
ALTER TABLE `status_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `component_placements`
--
ALTER TABLE `component_placements`
  ADD CONSTRAINT `component_placements_component_id_foreign` FOREIGN KEY (`component_id`) REFERENCES `components` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `connotes`
--
ALTER TABLE `connotes`
  ADD CONSTRAINT `connotes_manifest_id_foreign` FOREIGN KEY (`manifest_id`) REFERENCES `manifests` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `logistic_histories`
--
ALTER TABLE `logistic_histories`
  ADD CONSTRAINT `logistic_histories_status_type_id_foreign` FOREIGN KEY (`status_type_id`) REFERENCES `status_types` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `media_links`
--
ALTER TABLE `media_links`
  ADD CONSTRAINT `media_links_media_id_foreign` FOREIGN KEY (`media_id`) REFERENCES `medias` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `media_resizes`
--
ALTER TABLE `media_resizes`
  ADD CONSTRAINT `media_resizes_media_id_foreign` FOREIGN KEY (`media_id`) REFERENCES `medias` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
