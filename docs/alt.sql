alter table `accounts` add `_lft` int unsigned not null default '0', add `_rgt` int unsigned not null default '0', add `parent_id` int unsigned null
alter table `accounts` add index `accounts__lft__rgt_parent_id_index`(`_lft`, `_rgt`, `parent_id`)
ALTER TABLE `logistic_histories` ADD `occur_at` VARCHAR(150) NULL DEFAULT '' AFTER `by_user`;