<?php

return [
    'listeners'=>[
        
    ],
    'parcel'=>[
        "availables"=>[
            'carton'=>'Carton',
        ],
        "types"=>[
            'carton'=>[
                "name"=>"Carton",
                "inputs"=>[
                    'weight'=>[
                        "default"=>0.1,
                        "unit"=>"kg",
                        "min"=>0.1,
                        "max"=>22
                    ],
                    'width'=>[
                        "default"=>0,
                        "unit"=>"cm",
                        "min"=>5,
                        "max"=>100
                    ],
                    'height'=>[
                        "default"=>0,
                        "unit"=>"cm",
                        "min"=>5,
                        "max"=>100
                    ],
                    'length'=>[
                        "default"=>0,
                        "unit"=>"cm",
                        "min"=>5,
                        "max"=>100
                    ]
                ]
            ],
            'pallet'=>[
                "name"=>"Pallet",
                "inputs"=>[
                    'weight'=>[
                        "default"=>0.1,
                        "unit"=>"kg",
                        "min"=>0.1,
                        "max"=>22
                    ],
                    'width'=>[
                        "default"=>0,
                        "unit"=>"cm",
                        "min"=>5,
                        "max"=>100
                    ],
                    'height'=>[
                        "default"=>0,
                        "unit"=>"cm",
                        "min"=>5,
                        "max"=>100
                    ],
                    'length'=>[
                        "default"=>0,
                        "unit"=>"cm",
                        "min"=>5,
                        "max"=>100
                    ]
                ]
            ]
        ]
    ],
    'logo-print'=>public_path('img/quatius-logo.png'),
    'default_courier'=> 'pronto',
    'history'=>[
        'status-active' => 2, //public
        'status-public' => 2, //public
        'status-admin' => 1,
        'status-system' => 0
    ],
    
    'status'=>[
        'group'=>[
            'pending'=>[
                "ready to print", 
                "submitted", 
                "booking pending", 
                "booking cancel",
                "booking error"
            ],
            'transit'=>[
                "booking accepted", 
                "picked up shipment", 
                "arrive at processing center",
                "processing",
                "notify supplier", // choose the supplier for transit
                "supplier received", // supplier for transit
                "in transit",
                "transfer shipment",
                "change distination",
                "with customs",
                "with driver",
                "delivery attempted",
                "awaiting collection",
                "delayed" ,
                "partially completed",
                "awaiting pick up",
                "other", // internal processing
            ],
            'undeliverable'=>[
                "delivery error",
                "returned to sender",
                "lost",
                "damaged",
                "destroyed",
                "cancel",
                "rejection",
                "undeliverable",
                "unknown",
            ],
            'complete'=>[
                "completed",
                "delivered",
                "customer pickup",
                "pod updated",
            ]
        ]
    ],
    'courier' =>[

    ],
    'couriers'=>[
        'custom'=>[
            'class'=>'Quatius\Logistic\Models\Couriers\CourierCustom',
            'logo-print'=>'',
            'timezone'=>null, // default UTC
            'init_params'=>[
            ]
        ],
        'pronto'=>[
            'class'=>'App\Modules\Pronto\Models\CourierPronto',
            'logo-print'=> '', // public_path('img/4px.png'),
            'timezone'=> 'Australia/Melbourne',
            'init_params'=>[
                'consignment'=>[
                    'init'=>900433000000
                    ]
                ]
        ]
    ],
    'connote'=>[
        'notify'=>[
            'enable'=>env("LOGISTIC_CONNOTE_NOTIFY", false),
            'accepted' => env("LOGISTIC_CONNOTE_NOTIFY_ACCEPTED", true),
            'with-driver' => env("LOGISTIC_CONNOTE_NOTIFY_WITH_DRIVER", true),
            'delivered' => env("LOGISTIC_CONNOTE_NOTIFY_DELIEVERED", true),
            'damaged' => env("LOGISTIC_CONNOTE_NOTIFY_DAMAGED", true),
            'delay' => env("LOGISTIC_CONNOTE_NOTIFY_DELAY", true),
            'undeliverable' => env("LOGISTIC_CONNOTE_NOTIFY_UNDELIVERABLE", true),
        ],
        'create-status'=>"ready to print",
    ],
    'manifest'=>[
        'create-status'=>"created"
    ]
];


