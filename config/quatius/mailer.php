<?php

return [
	'admin' => env('APP_EMAIL_ADMIN_ADDRESS', 'admin@mail.com'),
    'from' => ['address' => env('APP_EMAIL_ADDRESS', 'noreply@mail.com'), 'name' => env('APP_EMAIL_NAME', 'online-web')],
];
