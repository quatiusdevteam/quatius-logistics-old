<?php

return [
    'user'=>[
        'model'=>'App\User',
        'foreign_key'=>'user_id'
    ],

    'activate-role'=>'staff',

    'role-types'=>[
        'staff' => [5],
        'admin' => [3,5] // 3 => manager, 5 => registered
    ]
];


