<?php

return [

    'secret' => env('PRONTO_SECRET','key1234'),
    'stock_sync_file' => env('PRONTO_STOCK_FILE_PATH',storage_path("app/pronto_stock_sync.csv")),
    'order_type' => env('PRONTO_ORDER_TYPE','edi'), // edi or loadso
    'accounts'=>[
		'1'=>'252766',
		'2'=>'252766',
		'3'=>'252766',
	],
    'supplier_zone'      => env('PRONTO_SUPPLIER_ZONE', storage_path('app/supplier_zone.csv')),
    'zone_code_file'    => env('PRONTO_ZONE_CODE_FILE', storage_path('app/zone_code.csv')),
    'ql_rate_file'    => env('PRONTO_QL_RATE_FILE', storage_path('app/ql_rate.csv')),
];
