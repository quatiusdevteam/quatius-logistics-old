<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ManifestTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $this->assertTrue(true);
        

    }
    public function testManifestStatus(){
        $manifest = createManifest(['manifest_no'=>'320213020'],true);
        $this->assertEquals('created',$manifest->latestHistory()->status->type);

        foreach($manifest->connotes as $connote){
            $connote->setStatus('submitted', [], false);
        }

        //$this->assertEquals('submitted',$manifest->latestHistory()->status->type); // test failed if at the same time
        //trigger via connote status
        //$this->assertEquals('submitted',$manifest->histories->sortByDesc('id')->first()->status->type);
    }
}
