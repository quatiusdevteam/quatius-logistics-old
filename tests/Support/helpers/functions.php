<?php

use Illuminate\Http\UploadedFile;
use Tymon\JWTAuth\Facades\JWTAuth;

function createUser($attributes = [])
{
	return factory(\App\User::class)->create($attributes);
}


function getApiUser()
{
	return Litepie\User\Models\Role::where('permissions','LIKE','%"access.api.frontend":"1"%')->first()->users->first();
}

function getSuperUser()
{
	return Litepie\User\Models\Role::whereName('superuser')->first()->users->first();
}

function getApiToken($user=null)
{
	$user = $user?:getApiUser();

	return JWTAuth::fromUser($user);
}

function getConnoteWithHistories($attrs=[])
{
	$connote = new \Quatius\Logistic\Models\Connote();
	$connote->forceFill($attrs);

	return $connote;
}

function createConnote($attributes = [], $saving = false){
	$manifest = new \Quatius\Logistic\Models\Manifest([
		'manifest_no' => generateManifect()
	]);

	$pickUp = new \Quatius\Address\Models\Address([
		'company'=>'SONIQ',
		'first_name'=>'Send',
		'last_name'=>'Doe',
		'phone_1'=>'0412999555',
		'phone_2'=>'',
		'email'=>'send.doe@mail-test.com',
		'address_1'=>'414 Lower Dandenong Road',
		'address_2'=>'',
		'city'=>'Braeside',
		'postcode'=>'3195',
		'state'=>'VIC',
		'country'=>'AU',
		'nick'=>''
	]);

	$sender = new \Quatius\Address\Models\Address([
		'company'=>'SONIQ',
		'first_name'=>'Sam',
		'last_name'=>'Smith',
		'phone_1'=>'0412955995',
		'phone_2'=>'',
		'email'=>'sam.smith@mail-test.com',
		'address_1'=>'414 Lower Dandenong Road',
		'address_2'=>'',
		'city'=>'Braeside',
		'postcode'=>'3195',
		'state'=>'VIC',
		'country'=>'AU',
		'nick'=>''
	]);

	if ($saving){
		$pickUp->save();
		$manifest->pickup_address_id = $pickUp->id;
		$sender->Save();
		$manifest->sender_address_id = $sender->id;
		$manifest->save();
	}

	$manifest->setRelation('pickup', $pickUp);
	$manifest->setRelation('sender', $sender);

	$receiver = new \Quatius\Address\Models\Address([
		'company'=>'SONIQ',
		'first_name'=>'John',
		'last_name'=>'Doe',
		'phone_1'=>'0412555999',
		'phone_2'=>'',
		'email'=>'john.doe@mail-test.com',
		'address_1'=>'178 Boundary Rd',
		'address_2'=>'',
		'city'=>'Braeside',
		'postcode'=>'3195',
		'state'=>'VIC',
		'country'=>'AU',
		'nick'=>'',
		'address_type'=>'receiver'
	]);

	$connote = factory(\Quatius\Logistic\Models\Connote::class)->make($attributes);
	if ($saving){
		$manifest->connotes()->save($connote);
		$connote->addresses()->save($receiver);
	}

	$connote->setRelation('manifest', $manifest);
	$connote->setRelation('receiver', $receiver);

	$manifest->setRelation('connotes', collect([$connote]));
	
	return $connote;
}

function createManifest($attributes = [], $saving = false){
	$connote = createConnote([
		'courier_consignment'=>'900433000100',
		'weight'=>2.1,
		'width'=>32,
		'height'=>22,
		'length'=>12,
		'declared_value'=>10.50,
		'currency'=>'AUD',
		'parcel_type'=>'carton',
		'atl'=>0,
		'reference'=>'is-testing-1',
		'instruction'=>'Placed in a testing area.'
	], $saving);

	$manifest = $connote->manifest;

	$connotes = $manifest->connotes;
	$connote2 = createConnote([
		'courier_consignment'=>'900433000101',
		'weight'=>1.1,
		'width'=>12,
		'height'=>32,
		'length'=>22,
		'declared_value'=>100.50,
		'currency'=>'AUD',
		'parcel_type'=>'carton',
		'atl'=>1,
		'reference'=>'is-testing-2',
		'instruction'=>'Placed next to testing area.'
	]);
	
	$receiver = new \Quatius\Address\Models\Address([
		'company'=>'SONIQ',
		'first_name'=>'Jame',
		'last_name'=>'Doe',
		'phone_1'=>'0412554449',
		'phone_2'=>'',
		'email'=>'jame.doe@mail-test.com',
		'address_1'=>'17 AZALEA CT',
		'address_2'=>'',
		'city'=>'KNOXFIELD',
		'postcode'=>'3180',
		'state'=>'VIC',
		'country'=>'AU',
		'nick'=>'',
		'address_type'=>'receiver'
	]);

	$manifest->fill($attributes);

	if ($saving){
		$manifest->save();
		
		$manifest->connotes()->save($connote2);
		$connote2->addresses()->save($receiver);
	}
	$connote2->setRelation('manifest', $manifest);
	$connote2->setRelation('receiver', $receiver);

	$connotes->push($connote2);

	$manifest->setRelation('connotes', $connotes);
	$manifest->setStatus('created');
	return $manifest;
}

