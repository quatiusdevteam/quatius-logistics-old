<?php

namespace Tests\Feature\Integration;

use App\Modules\Pronto\Models\CourierPronto;
use GuzzleHttp\Client;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Arr;

class Courier4pxTest extends TestCase
{
    use DatabaseTransactions;
    
    private $params = [
        'APP_KEY' =>"89a2ab04-3568-483a-9506-2c535f40e388",
        'APP_SECRET'=>"56554151-d833-4ad2-8f95-2fcf5bcc83e6",
        'version'=>"1.0.0",
        'format'=>"json",
        'language'=>"en"
    ];

    private $timestamp = 1532592413187;
    
    private $courier = null;
       
    protected function setUp(): void
    {
        parent::setUp();
        $this->courier = new CourierPronto();
        $this->courier->setParams('crediental', $this->params);
        $this->courier->setParams('consignment.init', 900133000000);
        $this->courier->setParams('consignment.current', 900133000000);
        $this->courier->setParams('api.enable', true);
        $this->courier->setParams('api.simluate', true);
        $this->courier->setParams('api.log.success', false);
        $this->courier->setParams('api.log.failure', false);
        config()->set('quatius.logistic.connote.notify.enable', false);

        
    
        //$this->http = new Client(['handler' => HandlerStack::create($mock)]);

        
        //$client = new \GuzzleHttp\Client();
        // $response = $client->request('GET', 'https://api.github.com/repos/guzzle/guzzle');

        // echo $response->getStatusCode(); // 200
        // echo $response->getHeaderLine('content-type'); // 'application/json; charset=utf8'
        // echo $response->getBody()->getContents(); // '{"id": 1420053, "name": "guzzle", ...}'

        // // Send an asynchronous request.
        // $request = new \GuzzleHttp\Psr7\Request('GET', 'http://httpbin.org');
        // $promise = $client->sendAsync($request)->then(function ($response) {
        //     echo 'I completed! ' . $response->getBody();
        // });

        // $promise->wait();
    }

    /**
     * @return void
     */
    public function testApiEncryptRaw()
    {
        $postData = ["deliveryOrderNo"=>"900433000005"];
        $plaintext = $this->courier->apiEncryptRaw("tr.order.tracking.get",$postData, $this->timestamp);
        $plaintextExpected='app_key89a2ab04-3568-483a-9506-2c535f40e388formatjsonmethodtr.order.tracking.gettimestamp1532592413187v1.0.0{"deliveryOrderNo":"900433000005"}56554151-d833-4ad2-8f95-2fcf5bcc83e6';
        
        $this->assertEquals($plaintext, $plaintextExpected, "Plain body format");

        $expectSigned = "6cf804551fb44f9c31ac8960cfdf1775";
        $this->assertEquals($expectSigned,$this->courier->apiSignBody($plaintext));

        $urlQuery = $this->courier->apiPostUrl("tr.order.tracking.get", $this->courier->apiSignBody($plaintext), $this->timestamp);

        $urlQueryExpected="http://open.au.4px.com/router/api/service?method=tr.order.tracking.get&app_key=89a2ab04-3568-483a-9506-2c535f40e388&v=1.0.0&timestamp=1532592413187&format=json&sign=".$expectSigned;
        
        $this->assertEquals($urlQueryExpected, $urlQuery, "Url Query with signed key");
        
        $mock = new MockHandler([
            new Response(200, [], File::get(__DIR__ . '/../../Support/stubs/4px/response-tr.order.tracking.get.json')),
        ]);
    
        $client = new Client(['handler' => HandlerStack::create($mock)]);
        $this->courier->setHttpClient($client); //set Mock client

        $data = $this->courier->apiPostRequest("tr.order.tracking.get",$postData);
        $this->assertNotEmpty($data, "should contains json");
    }
    
    public function testApiMSGTransCode(){
        $connote = factory(\Quatius\Logistic\Models\Connote::class)->make();
        
        $this->assertEquals("MSG_S_OK",$this->courier->getMSGTransCode(['businessLinkCode'=>'FPX_S_OK'], $connote)["comments"]);
        $this->assertEquals("Display Orginal",$this->courier->getMSGTransCode(['businessLinkCode'=>'FPX_S_XXX', 'trackingContent'=>'Display Orginal'], $connote)["comments"]);

        $this->assertEquals("MSG_M_RNA",$this->courier->getMSGTransCode(['businessLinkCode'=>'FPX_UNDIFINED', 'trackingContent'=>'Redirected to new address'], $connote)["comments"]);

        $this->assertEquals("MSG_S_OKSR",$this->courier->getMSGTransCode(['businessLinkCode'=>'FPX_S_OK', 'trackingContent'=>'Delivered with signature from J HELY'], $connote)["comments"]);
        $this->assertEquals("J HELY",$this->courier->getMSGTransCode(['businessLinkCode'=>'FPX_S_OK', 'trackingContent'=>'Delivered with signature from J HELY'], $connote)["value"]);
        
        $this->assertEquals("Australia Post",$this->courier->getMSGTransCode(['businessLinkCode'=>'FPX_O_IR', 'trackingContent'=>'Manifest accepted by Australia Post'], $connote)["value"]);
        
        $this->assertEquals("SHEFFIELD LPO",$this->courier->getMSGTransCode(['businessLinkCode'=>'FPX_D_FAD', 'trackingContent'=>'Awaiting collection at SHEFFIELD LPO'], $connote)["value"]);
        
        $this->assertEquals("delivered",$this->courier->getMSGStatus('MSG_S_OK'));
        $this->assertEquals("other",$this->courier->getMSGStatus('FPX_S_XXX'));

    }

    public function testApiTranformAConnoteTracking(){
        $data = File::get(__DIR__ . '/../../Support/stubs/4px/response-tr.order.tracking.get.json');
        $dataArr = json_decode($data, true);
        $consignment = Arr::get($dataArr, "data.deliveryOrderNo","");
        $trackings = Arr::get($dataArr, "data.trackingList",[]);

        $standardHist = $this->courier->tranformAConnoteTracking($trackings, factory(\Quatius\Logistic\Models\Connote::class)->make());
        
        $this->assertCount(11, $standardHist);

        $this->assertNotEmpty($standardHist[0]["created_at"]);
        $this->assertNotEmpty($standardHist[0]["status"]);
        $this->assertNotEmpty($standardHist[0]["comments"]);
    }
    
    public function testApiRemoteAConnoteTracking(){
        $connote = factory(\Quatius\Logistic\Models\Connote::class)->create(['courier_consignment'=>'900433000005']);
        
        $mock = new MockHandler([
            new Response(200, [], File::get(__DIR__ . '/../../Support/stubs/4px/response-tr.order.tracking.get.json')),
        ]);
    
        $client = new Client(['handler' => HandlerStack::create($mock)]);
        $this->courier->setHttpClient($client); //set Mock client

        $this->courier->updateRemoteTracking($connote);

        $this->assertCount(11, $connote->histories);
    }

    /*
    public function testApiPrepAddress(){
        $address = new \Quatius\Address\Models\Address([
            'company'=>'SONIQ',
            'first_name'=>'Sam',
            'last_name'=>'Smith',
            'phone_1'=>'0412955995',
            'phone_2'=>'',
            'email'=>'sam.smith@mail-test.com',
            'address_1'=>'414 Lower Dandenong Road',
            'address_2'=>'',
            'city'=>'Braeside',
            'postcode'=>'3195',
            'state'=>'VIC',
            'country'=>'AU',
            'nick'=>''
        ]);

        $data = $this->courier->prepAddress($address);

        $this->assertEquals([
            "first_name" => "Sam",
            "last_name" => "Smith",
            "company" => "SONIQ",
            "email" => "sam.smith@mail-test.com",
            "phone" => "0412955995",
            "street" => "414 Lower Dandenong Road",
            "city" => "Braeside",
            'state'=> 'Victoria',
            'country'=> 'Australia',
            "post_code" => "3195",
            "district" => ""
        ], $data);
    }

    public function testApiPrepCreateConnote(){
        $connote = createConnote([
            'courier_consignment'=>'900433000005',
            'weight'=>2.1,
            'width'=>32,
            'height'=>22,
            'length'=>12,
            'declared_value'=>10.50,
            'currency'=>'AUD',
            'reference'=>'is-testing'
        ]);
        $data = $this->courier->prepCreate($connote);
        $this->assertEquals(
        [
            "4px_tracking_no" => "900433000005",
            "ref_no" => "is-testing",
            "business_type" => "LOCAL",
            "cargo_type" => "1",
            "logistics_service_info" => [
              "logistics_product_code" => ""
            ],
            "return_info" => [
              "is_return_on_domestic" => "N"
            ],
            "parcel_list" => [
              "weight" => 2100,
              "length" => 12,
              "width" => 32,
              "height" => 22,
              "parcel_value" => 10.5,
              "currency" => "AUD",
            ],
            "is_insure" => "N",
            "sender" => [
              "first_name" => "Sam",
              "phone" => "0412955995",
              "post_code" => "3195",
              "country" => "Australia",
              "state" => "Victoria",
              "city" => "Braeside",
              "district" => "",
              "street" => "414 Lower Dandenong Road",
              "last_name" => "Smith",
              "company" => "SONIQ",
              "email" => "sam.smith@mail-test.com",
            ],
            "recipient_info" => [
              "first_name" => "John",
              "phone" => "0412555999",
              "post_code" => "3195",
              "country" => "Australia",
              "state" => "Victoria",
              "city" => "Braeside",
              "district" => "",
              "street" => "178 Boundary Rd",
              "last_name" => "Doe",
              "company" => "SONIQ",
              "email" => "john.doe@mail-test.com",
            ],
            "deliver_type_info" => [
              "deliver_type" => "1",
              "pick_up_info" => [
                "pick_up_address_info" => [
                  "first_name" => "Send",
                  "phone" => "0412999555",
                  "post_code" => "3195",
                  "country" => "Australia",
                  "state" => "Victoria",
                  "city" => "Braeside",
                  "district" => "",
                  "street" => "414 Lower Dandenong Road",
                  "last_name" => "Doe",
                  "company" => "SONIQ",
                  "email" => "send.doe@mail-test.com",
                ]
              ]
                ],
            "deliver_to_recipient_info" => [
              "deliver_type" => "HOME_DELIVERY",
            ]
        ], $data);
    }

    public function testApiRemoteCreateConnote(){
        $connote = createConnote([
            'courier_consignment'=>'900433000200',
            'weight'=>2.1,
            'width'=>32,
            'height'=>22,
            'length'=>12,
            'declared_value'=>10.50,
            'currency'=>'AUD',
            'reference'=>'is-testing'
        ]);

        
        $mock = new MockHandler([
            new Response(200, [], File::get(__DIR__ . '/../../Support/stubs/4px/response-tr.order.tracking.get.json')),
        ]);
    
        $client = new Client(['handler' => HandlerStack::create($mock)]);
        $this->courier->setHttpClient($client); //set Mock client

        //{"timestamp":"2022-06-14 09:53:03","status":500,"error":"Internal Server Error","message":"com.fpx.openapi.oauth2.except (truncated..
        $postData = $this->courier->prepCreate($connote);

        $dataResponse = $this->courier->apiPostRequest("ds.pds.order.create", $postData);

        //file_put_contents(__DIR__ . '/../../Support/stubs/4px/response-ds.pds.order.create.json', $dataResponse);
        
    }
    
    public function testApiRemoteCancelConnote(){
        $mock = new MockHandler([
            new Response(200, [], File::get(__DIR__ . '/../../Support/stubs/4px/response-tr.order.tracking.get.json')),
        ]);
    
        $client = new Client(['handler' => HandlerStack::create($mock)]);
        $this->courier->setHttpClient($client); //set Mock client
        
        //{"timestamp":"2022-06-14 09:49:45","status":500,"error":"Internal Server Error","message":"com.fpx.openapi.oauth2.except

        $dataResponse = $this->courier->apiPostRequest("ds.pds.order.cancel", [
            'request_no' => '900433000099',
            'cancel_reason' => 'Testing only, please cancel it.'
        ]);

        //file_put_contents(__DIR__ . '/../../Support/stubs/4px/response-ds.pds.order.cancel.json', $dataResponse);
    }
    
    */
    
    public function testApiPrepForecastAddress(){
        $address = new \Quatius\Address\Models\Address([
            'company'=>'SONIQ',
            'first_name'=>'Sam',
            'last_name'=>'Smith',
            'phone_1'=>'0412955995',
            'phone_2'=>'',
            'email'=>'sam.smith@mail-test.com',
            'address_1'=>'414 Lower Dandenong Road',
            'address_2'=>'',
            'city'=>'Braeside',
            'postcode'=>'3195',
            'state'=>'VIC',
            'country'=>'AU',
            'nick'=>''
        ]);

        $data = $this->courier->prepForecastAddress($address);

        $this->assertEquals([
            "firstName" => "Sam",
            "phone1" => "0412955995",
            "postCode" => "3195",
            "country" => "Australia",
            "province" => "Victoria",
            "suburb" => "Braeside",
            "address1" => "414 Lower Dandenong Road",
            "email" => "sam.smith@mail-test.com",
            "company" => "SONIQ",
        ], $data, 'Addess with no Prefix');

        $data = $this->courier->prepForecastAddress($address, 'receiver');
        
        $this->assertEquals([
            "receiverFirstName" => "Sam",
            "receiverPhone1" => "0412955995",
            "receiverPostCode" => "3195",
            "receiverCountry" => "Australia",
            "receiverProvince" => "Victoria",
            "receiverSuburb" => "Braeside",
            "receiverAddress1" => "414 Lower Dandenong Road",
            "receiverEmail" => "sam.smith@mail-test.com",
            "receiverCompany" => "SONIQ",
        ], $data, 'Addess with no Prefix');
    }

    public function testApiPrepForecastManifest(){
        $manifest = createManifest([], true);

        $data = $this->courier->prepForecastManifest($manifest);
        
        $mock = new MockHandler([
            new Response(200, [], File::get(__DIR__ . '/../../Support/stubs/4px/response-gos.api.forecastOrder.json')),
        ]);
        
        $client = new Client(['handler' => HandlerStack::create($mock)]);
        $this->courier->setHttpClient($client); //set Mock client
        
        $dataResponse = $this->courier->apiPostRequest("gos.api.forecastOrder", $data);

        $data = json_decode($dataResponse, true);
        $this->assertArrayHasKey('data', $data);
        
        $this->assertArrayHasKey('fpxTrackNos', $data['data']);
        
        $this->assertEquals(["901001177735","901001166795"], $data['data']['fpxTrackNos']);
    }

    public function testApiCreateManifest(){
        $manifest = createManifest(['manifest_no'=>'320213020'],true);

        $mock = new MockHandler([
            new Response(200, [], File::get(__DIR__ . '/../../Support/stubs/4px/response-gos.api.forecastOrder.json')),
        ]);
        
        $client = new Client(['handler' => HandlerStack::create($mock)]);
        $this->courier->setHttpClient($client); //set Mock client

        $this->courier->createManifest($manifest, []);
        
        $this->assertEquals('901001177735', $manifest->connotes->get(0)->courier_consignment);
        $this->assertEquals('901001166795', $manifest->connotes->get(1)->courier_consignment);
    }

    public function testApiSubmitManifest(){
        $manifest = createManifest(['manifest_no'=>'320213020'],true);

        $mock = new MockHandler([
            new Response(200, [], File::get(__DIR__ . '/../../Support/stubs/4px/response-gos.api.submitOrder.json'))
            //new Response(200, [], '{"result":"1","msg":"","errors":[],"data":{"consignmentNoList":["901001177735"],"failFpxNoList":["901001166795"]}}'),
        ]);
        
        $client = new Client(['handler' => HandlerStack::create($mock)]);
        $this->courier->setHttpClient($client); //set Mock client

        $this->courier->submitManifest($manifest);

        $this->assertEquals('booking accepted', $manifest->connotes->get(0)->latestHistory()->status->type);
        $this->assertEquals('booking accepted', $manifest->connotes->get(0)->latestHistory()->status->type);
    }
    
    public function testApiRemoteLabelConnote(){
        $mock = new MockHandler([
            new Response(200, [], File::get(__DIR__ . '/../../Support/stubs/4px/response-gos.api.getOrderLabel.json')),
        ]);
    
        $client = new Client(['handler' => HandlerStack::create($mock)]);
        $this->courier->setHttpClient($client); //set Mock client

        $dataResponse = $this->courier->apiPostRequest("gos.api.getOrderLabel", [
            'consignment' => '901001177735',
        ]);

        $data = json_decode($dataResponse, true);
        $this->assertArrayHasKey('data', $data);
        $this->assertArrayHasKey('labelUrl', $data['data']);
        $this->assertEquals([
            "consignment"=> "901001177735",
            "labelUrl"=> "http://fpx-bss-oss-eu.oss-eu-central-1.aliyuncs.com/fpx-print-label-4863b823-b18a-4e59-bd3e-7d59854bee02.pdf"
        ],$data['data']);
    }
}
