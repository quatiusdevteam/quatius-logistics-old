<?php

namespace Tests\Feature\Integration;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Quatius\Logistic\Models\Courier;
use Illuminate\Support\Facades\File;

class CourierTest extends TestCase
{
    use DatabaseTransactions;

    private $courier = null;
       
    protected function setUp(): void
    {
        parent::setUp();
        $this->courier = new Courier();
        
        config()->set('quatius.logistic.connote.notify.enable', false);

    }

    public function testMsgMapping(){
        $this->assertEquals("MSG_O_IR", $this->courier->checkMsgCode('MSG_O_IR'));
        $this->assertEquals("MSG_M_OTH", $this->courier->checkMsgCode('MSG_O_XXXX'));
        $this->assertEquals("notify supplier", $this->courier->getMSGStatus('MSG_O_IR'));
        
        $this->assertEquals("Shipment delivered.", logistic_msg('MSG_S_OK'));
        $this->assertEquals('Shipment is some where', logistic_msg('Shipment is some where'), "Normal messages"); 
    }
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testSaveAConnoteStatuses(){
        $data = File::get(__DIR__ . '/../../Support/stubs/logistic-tracklist.json');
        $dataArr = json_decode($data, true);
        $connote = factory(\Quatius\Logistic\Models\Connote::class)->create();
        
        $connote->setStatus("submitted", ["created_at"=>"2022-04-26 11:43:51"]);
        $connote->setStatus("ready to print", ["created_at"=>"2022-04-26 11:46:51"]);
        $connote->setStatus("booking pending", ["created_at"=>"2022-04-26 11:50:51"]);
        
        $this->assertTrue($connote->exists());
        
        foreach($dataArr as $histdata){
            $this->courier->setConnoteStatus($connote, $histdata['status'], $histdata);
        }
        
        $this->assertCount(14, $connote->histories);

        foreach($dataArr as $histdata){ //re-inserts history
            $this->courier->setConnoteStatus($connote, $histdata['status'], $histdata);
        }

        $this->assertCount(14, $connote->histories);
    }
}
