<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('account_id')->nullable()->unsigned();
        });

        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('account_code')->nullable();
            $table->string('email', 255)->nullable();
            $table->string('phone_1',24)->nullable();
            $table->decimal('balance', 15, 4)->default(0)->nullable();
            $table->decimal('credit_limit', 15, 4)->default(0);
            $table->text('params')->nullable();
            $table->nestedSet();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('account_id');
        });
    }
}
