<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

//factory(\Quatius\Account\Models\Account::class)->create()->contacts()->saveMany(factory(\Quatius\Account\Models\AccountContact::class,4)->make())

$factory->define(Quatius\Account\Models\Account::class, function(Faker\Generator $faker) {
    return [
        'name' => $faker->company,
        'email' => $faker->email,
        'phone_1' => $faker->phoneNumber,
        'account_code' => $faker->numberBetween(10000, 90000),
    ];
});

$factory->define(Quatius\Address\Models\Address::class, function(Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'phone_1' => $faker->phoneNumber,
        'email' => $faker->email,
        'address_1' => $faker->streetAddress,
        'city' => $faker->city,
        'state' => $faker->randomElement(['VIC','ACT','QLD','NSW','TAS','WA','SA','NT']),
        'country' => 'AU',
        'postcode' => $faker->postcode
    ];
});