<?php

namespace Quatius\Account\Providers;

use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\ServiceProvider;
use Route;

class AccountServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
	 public function boot()
    {	
        Route::bind('account_id',function($value) {
	        return hashids_decode($value)?:abort(404,'Route Key Invalid');
	    });
    }
    
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        
        $this->loadMigrationsFrom(__DIR__.'/../Databases/migrations');
        $this->app->make(Factory::class)->load(__DIR__.'/../Databases/factories');

        $this->app->singleton('Quatius\Account\Repositories\AccountRepository', 'Quatius\Account\Repositories\Handlers\AccountHandler');
        $this->app->bind('account', 'Quatius\Account\Repositories\AccountRepository');
    }
}
