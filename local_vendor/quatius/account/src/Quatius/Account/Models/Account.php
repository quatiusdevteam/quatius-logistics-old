<?php

namespace Quatius\Account\Models;

use App\Jobs\SendInvite;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Notification;
use Kalnoy\Nestedset\NodeTrait;
use Prettus\Repository\Contracts\Transformable;
use Quatius\Account\Notifications\ActiveAccount;
use Quatius\Address\Traits\HasAddresses;
use Quatius\Framework\Traits\ModelFinder;
use Quatius\Framework\Traits\ParamSetter;
use Quatius\Account\Jobs\JobSendInvite;

class Account extends Model implements Transformable{

    use ModelFinder, ParamSetter, SoftDeletes, HasAddresses, Notifiable, NodeTrait;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
        'name',
        'account_code',
        'phone_1',
        'email',
        'credit_limit',
	];

    public function contacts()
	{
		return $this->hasMany(AccountContact::class);
	}

    public function transform()
    {
        $data = $this->toArray();
        if (isset($data['id']))
            $data['id'] = $this->getRouteKey();

        $data['pending_users'] = count($this->getParams('invites', []));
        return $data;
    }

    public function sendInvitation($email, $role){

        Notification::route('mail',[$email])->notify(new ActiveAccount($this->id, $this->name, $email, $role));

        $invites = $this->getParams('invites', []);

        //check exists
        $index = collect($invites)->where("email", $email)->keys()->first();
        if ($index !== null) {
            array_splice($invites, $index, 1); // remove index from invites
        }

        $invites[] = ["email"=>$email, "designation"=>$role, "created_at"=>date("Y-m-d H:i:s")];
        $this->setParams('invites', $invites);
        $this->save();
    }

    public function users(){
        return $this->hasMany(User::class);
    }
}
