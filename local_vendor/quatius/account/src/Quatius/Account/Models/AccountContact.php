<?php

namespace Quatius\Account\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Quatius\Account\Traits\HasAccount;
use Quatius\Framework\Traits\ModelFinder;
use Quatius\Framework\Traits\ParamSetter;

class AccountContact extends Model implements Transformable{

    use ModelFinder, ParamSetter, HasAccount;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
        'title',
        'company',
        'first_name',
        'middle_name',
        'last_name',
        'phone_1',
        'phone_2',
        'email',
        'address_1',
        'address_2',
        'city',
        'postcode',
        'state',
        'country',
        'nick',
        'address_type',
	];

    public function transform()
    {
        $data = $this->toArray();
        
        if (isset($data['id']))
            $data['id'] = $this->getRouteKey();

        if (isset($data['account_id']))
            $data['account_id'] = $this->getRouteKey();
               
        $data = array_map(function($v){
            return $v ?: '';
        },$data);
        
        return $data;
    }
}
