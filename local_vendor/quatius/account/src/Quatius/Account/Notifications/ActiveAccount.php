<?php

namespace Quatius\Account\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification as InNotification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Notification;
use Quatius\Account\Models\Account;

class ActiveAccount extends InNotification implements ShouldQueue
{
    use Dispatchable, Queueable;

    /**
     * The user.
     *
     * @var
     */
    public $accountId;
    public $accountName;
    public $email;
    public $role;
    public $tries = 5;

    /**
     * Create a notification instance.
     *
     * @param  string  $token
     * @return void
     */
    public function __construct($accountId, $accountName, $email,  $role="staff")
    {
        $this->accountId = $accountId;
        $this->accountName = $accountName;
        $this->email = $email;
        $this->role = $role;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

/**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $token = $this->getTokenFromPayload(["a"=>$this->accountId, "e"=>$this->email]);
        $mail = (new MailMessage)
        ->subject('Account Invite for '.$this->accountName)
        ->line('You are receiving this email because we received an invitation request to create your account.')
        ->action('Accept Invitation', url('account/invitation/'. $token));
        //->line('If you did not request a password reset, no further action is required.');
        return $mail;
    }

    public function displayName()
    {
        return "ActiveAccount:Invite ".$this->email ." for ".$this->accountName;
    }
    
    public function getTokenFromPayload($data){
        return app('Tymon\JWTAuth\Contracts\Providers\JWT')->encode($data);
    }

    // public static function sendInvitation($account, $email){
    //     $tmpEmail = $account->email;
    //     $account->email = $email;
    //     $account->notify(new ActiveAccount($account->id, $account->name, $email));
    // }

    public static function getDataFromToken($token){
        try{
            $payload = app('Tymon\JWTAuth\Contracts\Providers\JWT')->decode($token);
            return isset($payload['a'])?$payload:null; 
        }catch( \Exception $e){
            return null;
        }
    }
    // /**
    //  * Get the array representation of the notification.
    //  *
    //  * @param  mixed  $notifiable
    //  * @return array
    //  */
    // public function toArray($notifiable)
    // {
    //     return [
    //         //
    //     ];
    // }
}
