<?php
namespace Quatius\Account\Repositories\Presenters;

use Prettus\Repository\Presenter\FractalPresenter;

class AccountIndexPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AccountIndexTransformer();
    }
}
