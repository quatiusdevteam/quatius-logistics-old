<?php

namespace Quatius\Account\Repositories\Presenters;

use Quatius\Account\Models\Account;
use League\Fractal\TransformerAbstract;

/**
 * Class PostTransformer
 * @package namespace App\Modules\GenerateTransformers;
 */
class AccountIndexTransformer extends TransformerAbstract
{

    /**
     * Transform the Post entity
     * @param Quatius\Account\Models\Account $model
     *
     * @return array
     */
    public function transform(Account $model)
    {
        return $model->transform();
    }
}
