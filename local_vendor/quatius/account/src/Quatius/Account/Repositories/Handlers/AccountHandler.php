<?php

namespace Quatius\Account\Repositories\Handlers;

use Quatius\Account\Models\Account;
use Quatius\Account\Repositories\AccountRepository;
use Quatius\Framework\Repositories\QuatiusRepository;
use Request;

class AccountHandler extends QuatiusRepository implements AccountRepository
{
    private $_currentAccount = null;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return \Quatius\Account\Models\Account::class;
    }

    public function setDetail($account = null){
        $this->_currentAccount = $account;
    }
    
    public function user(){
        return Request::user();
    }

    public function userId($default=0){
        return $this->user()?$this->user()->id:$default;
    }

    public function getUserId($default=0){
        return $this->userId();
    }
    
    public function getAccountId($default=0){
        return $this->user()->account_id?:$default;
    }

    public function detail()
    {
        if (!$this->getAccountId(0)) return new Account();// throw new \Exception("No Account Found");

        if (!$this->_currentAccount && $this->user()){
            $this->_currentAccount = $this->find($this->getAccountId(0));
            
            if (!$this->_currentAccount) throw new \Exception("No Account Found");
        }
        return $this->_currentAccount;
    }
}
