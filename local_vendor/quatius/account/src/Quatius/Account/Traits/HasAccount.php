<?php
namespace Quatius\Account\Traits;

use Quatius\Account\Models\Account;
use DB;

trait HasAccount
{
	public function scopeByUserAccount($query, $id=null){
		return $query->whereAccountId($id?:app('account')->getAccountId());
	}

    public function account()
	{
		return $this->belongsTo(Account::class);
	}

	public function scopeAccountField($query, $field, $asName=""){
		$asName = $asName?:$field;

		$query->leftJoin(
		    DB::raw('(SELECT id as account_link_id , '.$field.' AS '.$asName.' FROM `accounts`) AS '.$asName.'_account_tb'),
		    'account_id', $asName.'_account_tb.account_link_id'
		);
    	return $query;
    }
}
