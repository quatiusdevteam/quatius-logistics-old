<?php
namespace Quatius\Account\Traits;

use DB;

trait HasUser
{
	public function scopeByUser($query, $id=null){
		return $query->whereUserId($id?:app('account')->getUserId());
	}

	public function scopeWithUserName($query, $fieldName="user_name"){
		if(!$fieldName) return $query;
		
		$query->leftJoin(
			DB::raw('(SELECT name as \''.$fieldName.'\', id as user_id FROM `users`) tb_'.$fieldName),
			'connotes.user_id','tb_'.$fieldName.'.user_id'
		);
		
		return $query;
	}

    public function user()
	{
		return $this->belongsTo(config('quatius.account.user.model',"App\User"), config('quatius.account.user.foreign_key',"user_id"));
	}
}
