<?php
namespace Quatius\Logistic\Traits;

use Quatius\Logistic\Models\StatusType;

trait HasStatusType
{
    public function status()
	{
		return $this->belongsTo(StatusType::class, "status_type_id");
	}
}
