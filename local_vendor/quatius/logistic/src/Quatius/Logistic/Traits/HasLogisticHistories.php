<?php

namespace Quatius\Logistic\Traits;

use Aws\History;
use Config;
use Quatius\Logistic\Models\LogisticHistory;
use DB;
use Illuminate\Support\Facades\Event;
use Quatius\Logistic\Models\StatusType;

trait HasLogisticHistories
{	
	private static $_status_types = null;
	public $saved_histories = null;

	public function getStatusTypes()
	{
		if (self::$_status_types === null){
			self::$_status_types = StatusType::get()->keyBy('type');
		}
		return self::$_status_types;
	}

	public function histories()
	{
	    return $this->morphMany(LogisticHistory::class,'logistic_histories');
	}
	
	public function getLastHistoryStatus($status=""){
		if (!$status)
			return $this->histories->sortByDesc('created_at')->first();

	    return $this->histories->where('status',$status)->sortByDesc('created_at')->first();
	}

	public function getStatusType($status){
        $statusObj = $this->getStatusTypes()->get($status);

        if (!$statusObj){
			$statusObj = StatusType::create(['type'=>$status, 'status'=>2]);
			self::$_status_types = null;
		}
        
        return $statusObj;
    }

	public function setStatus($status, $attributes=[], $checkDuplicateDate=false){
		$statusObj = $this->getStatusType($status);
		
		$histories = $this->histories;

		$newHistory = new LogisticHistory();
		
        $attributes["status_type_id"] = $statusObj->id;

		if($checkDuplicateDate && $oldHistory = $histories->where('status_type_id', $attributes["status_type_id"])->first()){
			return $oldHistory;
		}

		if (isset($attributes["created_at"]) || isset($attributes["updated_at"])){
			

			$timezone = isset($attributes["timezone"])? $attributes["timezone"]:null;
			$newHistory->timestamps = false;
			try{
				$newHistory->created_at = isset($attributes["created_at"])?\Carbon\Carbon::parse( $attributes["created_at"], $timezone)->setTimezone('UTC'):\Carbon\Carbon::now();
			}catch(\Exception $e){
				$newHistory->created_at = \Carbon\Carbon::now();
			}

			try{
				$newHistory->updated_at = isset($attributes["updated_at"])?\Carbon\Carbon::parse($attributes["updated_at"], $timezone)->setTimezone('UTC'):\Carbon\Carbon::now();
			}catch(\Exception $e){
				$newHistory->updated_at = \Carbon\Carbon::now();
			}
			
			if($oldHistory = $histories->where('status_type_id', $attributes["status_type_id"])->where('created_at', $newHistory->created_at)->first()){
				return $oldHistory;
			}

			unset($attributes["created_at"]);
			unset($attributes["updated_at"]);
			unset($attributes["timezone"]);
		}
		$data = isset($attributes['data'])?$attributes['data']:[];
		unset($attributes["data"]);

		$newHistory->fill($attributes);
		
		if ($data)
			$newHistory->setParams('data', $data);

		$newHistory->setRelation('status',$statusObj);
		$lastStatus = $this->latestHistory();
        $this->histories()->save($newHistory);
		
		$histories->push($newHistory);
		$this->setRelation('histories', $histories);

		$class = strtolower(basename(str_replace('\\', '/', get_class($this))));
		Event::dispatch("status-change.".$class.".".$status, [$this, $newHistory, $lastStatus]);

		return $newHistory;
    }

	public function getStatus(){
		$history= $this->latestHistory();
		if ($history) 
			return $history->status->type;
		return "";
	}

	public function addHistory($status, $attributes=[])
	{
	    $newHistory = $this->setStatus($status, $attributes);
	    
	    $this->load('histories');
	    return $newHistory;
	}

	public function latestHistory()
	{
		return $this->getLastHistoryStatus(null);
	}

    public function scopeStatusDateOf($query, $status="submitted", $fieldName=null){
    	return $this->scopeLatestStatus($query, $fieldName?:$status, $status, false);
    }

    public function scopeLatestStatus($query, $fieldName="status", $statusType=null, $showVal=true, $publishOnly = true){
        if(!$fieldName) return $query;

        $classPath = addslashes(get_class($this));
        $fieldDate = $fieldName.'_at';
		$fieldValue = $fieldName.'_value';
		$fieldId = $fieldName.'_id';
		
		$conditionAnd = "";
		if ($statusType !== null)
			$conditionAnd = " AND type ='$statusType'";

		if ($publishOnly !== null && $publishOnly)
			$conditionAnd .= " AND status >= ".config('quatius.logistic.history.status-active', 2);

		$selField = $showVal? ' type as \''.$fieldName.'\', ':'';

		// get Latest dates
		// SELECT logistic_histories_id as latest_date_id, max(created_at) as last_date from logistic_histories LEFT JOIN (SELECT type, id as status_type_id, status FROM status_types) asd USING (status_type_id) WHERE logistic_histories_type = '".$classPath."'".$conditionAnd

		$query->leftJoin(
		    DB::raw('(SELECT '.$selField .' last_date as \''.$fieldDate.'\', logistic_histories_id as history_link_id, value as \''.$fieldValue.'\' FROM `logistic_histories` LEFT JOIN status_types as status_type ON status_type_id = status_type.id, (SELECT logistic_histories_id as latest_date_id, max(created_at) as last_date from logistic_histories LEFT JOIN (SELECT type, id as status_type_id, status FROM status_types) asd USING (status_type_id) WHERE logistic_histories_type = \''.$classPath.'\' '.$conditionAnd.' GROUP BY logistic_histories_id) latest_date WHERE latest_date.latest_date_id = logistic_histories.logistic_histories_id AND latest_date.last_date = logistic_histories.created_at AND logistic_histories.logistic_histories_type = \''.$classPath.'\') AS tb_'.$fieldName),
		    $this->getTable().'.id','tb_'.$fieldName.'.history_link_id'
		);
		
    	return $query;
    }

	function historyTransform(){
		$histories = [];

		foreach($this->histories as $history){
			$histories[] = $history->transform();
		}
		
		return $histories;
	}
}
