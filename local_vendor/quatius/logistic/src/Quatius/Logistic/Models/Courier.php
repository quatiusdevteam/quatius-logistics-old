<?php

namespace Quatius\Logistic\Models;

use Config;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Quatius\Framework\Traits\ModelFinder;
use Quatius\Framework\Traits\ParamSetter;
use Quatius\Account\Traits\HasAccount;
use Quatius\Framework\Traits\PublishStatus;
use Quatius\Logistic\Models\Couriers\CourierCustom;
use Quatius\Logistic\Models\Couriers\CourierPronto;
use Nanigans\SingleTableInheritance\SingleTableInheritanceTrait;
use Quatius\Framework\Models\CsvImporter;
use Illuminate\Support\Arr;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;

class Courier extends Model implements Transformable{

    use ModelFinder, SingleTableInheritanceTrait, ParamSetter, SoftDeletes, HasAccount, PublishStatus;

    protected static $singleTableTypeField = 'type';

    public static $singleTableSubclasses = [CourierCustom::class, CourierPronto::class];

	public static $INPUT_ERROR = "booking error";
	public static $COURIER_ERROR = "unknown";
	public static $DELIVERY_ERROR = "delivery error";
	public static $SUBMITTED = "submitted";
	public static $PRINT_LABEL = "ready to print";
	public static $PENDING = "booking pending";
	public static $ACCEPTED = "booking accepted";
	public static $DELIVERED = "delivered";

    private $httpClient = null;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
     
    protected $attributes  = array(
        'name' => "Courier Name",
        'status' => 1,
        'type' => "custom",
        'params' => "{}"
    );
    
    protected $fillable = [
        'name',
        'label',
        'description',
        'note',
        'type',
        'params',
        'status',
        'ordering'
    ];
    
    protected $dates = ['deleted_at','created_at','updated_at'];

    public function initalise($connote){
        
    }

    public function setHttpClient($httpClient=null){
        $this->httpClient = $httpClient?: new \GuzzleHttp\Client();
    }

    public function getHttpClient(){
        if (!$this->httpClient)
            $this->httpClient = new \GuzzleHttp\Client();
        return $this->httpClient;
    }

    public function setSimulateResponse($content=''){

        $mock = new MockHandler([
            new Response(200, [], $content)
        ]);

        $client = new Client(['handler' => HandlerStack::create($mock)]);
        $this->setHttpClient($client);
    }
    public function tranformAConnoteTracking($trackings, $connote){
        if (!$trackings) return [];

        $filteredTracking = [];
        foreach($trackings as $tracking){
            $filteredTracking[] = $this->getMSGTransCode($tracking, $connote);
        }
        return $filteredTracking;
    }

    public function updateRemoteTracking($connote){
        throw new Exception("Not Implemented");
        
        $dataArr = []; //fetch remote data
        
        $tracking = $this->tranformAConnoteTracking(Arr::get($dataArr, "data.trackingList",[]), $connote);
        if ($tracking){
            foreach($tracking as $histdata){
                $connote->setStatus($histdata['status'], $histdata);
            }
        }
    }
    
    public function checkMsgCode($code){
        $codeMap = trans('Logistic::msg-status',[],'en');
        return isset($codeMap[$code])?$code:"MSG_M_OTH";
    }
    
    public function getMSGTransCode($tracking, $connote){
        $codeMap = trans('Logistic::msg-status',[],'en');
        $code = isset($codeMap[Arr::get($tracking, "code","")])?[Arr::get($tracking, "code","")]:"MSG_M_OTH";
        
        return [
            "occur_at"=> ucwords(Trim(Str_replace("4PX-","",Arr::get($tracking, "occurLocation","")))),
            "created_at"=> str_replace(": ",":",Arr::get($tracking, "occurDatetime","")),
            "comments"=> $code!='MSG_M_OTH'?$code:Arr::get($tracking, "trackingContent",""),
            "status"=> $this->getMSGStatus($code),
            "data"=>$tracking
        ];
    }

    public function getMSGStatus($code, $comment=""){
        $codeMap = trans('Logistic::msg-status',[],'en');

        return isset($codeMap[$code])?$codeMap[$code]:"other";
    }

    public function getStatusCode($code, $comment=""){
        return $code;
    }

    public function setItemCost($connote){
        $connote->price = 0;
    }

    public function getConsigmentNo($connote){
        return null;
    }

    public function labelPrinted($connote){ // indicate ready for pickup
        $connote->update(['is_printed'=>1]);
        //$submitHist = $connote->setStatus(Courier::$SUBMITTED);
        //$connote->submitted_at = $submitHist->created_at;
    }

    public function createManifest($manifest, $historyData=[]){
        foreach ($manifest->connotes as $connote){
            $connote->setStatus(Courier::$SUBMITTED, $historyData);
        }
        return true;
    }

    public function submitManifest($manifest){
        
    }
    
    public function createConnotes($connotes, $pickup=null, $sender=null, $historyData=[]){
    
    }

    public function cancelConnote($connote){
        
    }

    public function getLogoPath(){
        return config('quatius.logistic.couriers.'.$this->type.'.logo-print','');
    }

    public function hasLogo(){
        return file_exists($this->getLogoPath());
    }

    public function getLogoBase64(){
        return $this->hasLogo()?base64_encode(file_get_contents($this->getLogoPath())):'';
    }

    public function getConsigmentBarcode($connote){
        return getBarcode($connote->courier_consignment, $this->getParams('barcode.type', 'C128'));
    }

    public function Manifest(){
        return $this->hasMany(Manifest::class);
    }

    public function validate($connotes, $datas){
        // foreach($connotes as $connote){

        // }
        return true;
    }
    
    public function exportConnotes($wheres = [], $callbackConnote = null){
        $connoteQuery = app('connote')->with('account','sender','pickup','addresses')->latestStatus(); //  export any
        
        foreach ($wheres as $field=>$val){
            $connoteQuery->where($field, $val);
        }

        $connotes = $connoteQuery->get();
        $datas = [];
        $dataRow = [ // headers
            "receiverFirstName" =>"",
            "receiverStreetAddress1"=>"",
            "receiverCity" =>"",
            "receiverCountry" =>"",
            "receiverProvince" =>"",
            "receiverPostCode" =>"",
            "receiverContact" =>"",
            "receiverPhone1" =>"",
            "receiverEmail" =>"",
            "senderFirstName" =>"",
            "senderStreetAddress" =>"",
            "senderCity" =>"",
            "senderProvince" =>"",
            "senderCountry" =>"",
            "senderPostCode" =>"",
            "senderPhone1" =>"",
            "pickupFirstName" =>"",
            "pickupStreetAddress" =>"",
            "pickupCity" =>"",
            "pickupProvince" =>"",
            "pickupCountry" =>"",
            "pickupPostCode" =>"",
            "pickupPhone1" =>"",
            "pickupEmail" =>"",
            "parcelType" =>"",
            "NoOfItems" =>"",
            "refNo" =>"",
            "length" =>"",
            "width" =>"",
            "height" =>"",
            "weight" =>"",
            "instruction" => ""
        ];

        if ($callbackConnote) 
            $datas[] = $callbackConnote(null, $dataRow);

        foreach ($connotes as $connote){
            
            $dataRow = [
                "receiverFirstName" => $connote->receiver->first_name,
                "receiverStreetAddress1" => $connote->receiver->address_1,
                "receiverCity" => $connote->receiver->city,
                "receiverCountry" => $connote->receiver->country,
                "receiverProvince" => $connote->receiver->state,
                "receiverPostCode" => $connote->receiver->postcode,
                "receiverContact" => $connote->receiver->first_name,
                "receiverPhone1" => $connote->receiver->phone_1,
                "receiverEmail" => $connote->receiver->email,
                "senderFirstName" => $connote->sender->first_name,
                "senderStreetAddress" => $connote->sender->address_1,
                "senderCity" => $connote->sender->city,
                "senderProvince" => $connote->sender->state,
                "senderCountry" => $connote->sender->country,
                "senderPostCode" => $connote->sender->postcode,
                "senderPhone1" => $connote->sender->phone_1,
                "pickupFirstName" => $connote->pickup->first_name,
                "pickupStreetAddress" => $connote->pickup->address_1,
                "pickupCity" => $connote->pickup->city,
                "pickupProvince" => $connote->pickup->state,
                "pickupCountry" => $connote->pickup->country,
                "pickupPostCode" => $connote->pickup->postcode,
                "pickupPhone1" => $connote->pickup->phone_1,
                "pickupEmail" => $connote->pickup->email,
                "parcelType" => $connote->parcel_type,
                "NoOfItems" => $connote->items,
                "refNo" => $connote->reference,
                "length" => $connote->length,
                "width" => $connote->width,
                "height" => $connote->height,
                "weight" => round($connote->weight, 1),
                "instruction" => $connote->instruction
            ];
            
            if ($callbackConnote) {
                $dataRow = $callbackConnote($connote, $dataRow);
            }
            $datas[] = $dataRow;
            
        }

        return $datas;
    }

    public function export($action, $file , $ext="csv"){
        if ($file == "connotes")
            return $this->exportConnotes(["status"=>$action],  $callbackConnote = null);

        return "";
    }

    public function import($action, $filename , $request){
        $file = requestFile($request, 'local', $filename, 'logistic/imports/');

        if ($action == "connote-status")
        {
            if ($file != null){
                return $this->importConnoteStatus($file);
            }
        }
        
        app('log')->info('import - \''.$action.'\' : '.$filename, [$file]);
        return false;
    }

    public function importConnoteStatusMap(){
        return [
            "consignment"=>"consignment",
            "status"=>"status",
            "comments"=>"comments",
            "date"=>"date",
            "timezone"=>"timezone",
        ];
    }

    public function importConnoteStatus($file){
        $impCsv = new CsvImporter($file, true);
        $repo = app('connote');
        $fieldMap = $this->importConnoteStatusMap();

		while($dataRows = $impCsv->get(500))
		{
			foreach ($dataRows as $row)
			{
                if(isset($row[$fieldMap["consignment"]]) && !trim($row[$fieldMap["consignment"]])) continue;
                $status = isset($row[$fieldMap["status"]]) ? trim($row[$fieldMap["status"]]): "";
                if (!$status) continue;

                $connote = $repo->latestStatus()->where("courier_consignment", trim($row[$fieldMap["consignment"]]))->first();
                if (!$connote) continue;

                $data = [
                    "comments"=>isset($row[$fieldMap["comments"]]) ? trim($row[$fieldMap["comments"]]): "",
                ];

                if (isset($row[$fieldMap['date']])){
                    $data['created_at'] = $row[$fieldMap['date']];
                    $data["timezone"]= isset($row[$fieldMap['timezone']])?$row[$fieldMap['timezone']]:$this->getTimezone($connote);
                }
                $this->setConnoteStatus($connote, $status, $data);
			}
		}

        return true;
    }

    public function notify($action, $id, $value="", $extra=""){
        if ($action == "connote-status"){
            $connote = app('connote')->manifestField('courier_id')->latestStatus()->whereCourierId($this->id)->where('courier_consignment', $id)->first();
            
            if ($connote){
                $this->setConnoteStatus($connote, $value, ["comments"=>$extra]);
                return true;
            }
        }
        return false;
    }

    public function setConnoteStatus($connote, $status, $row=[]){
        $connote->setStatus($status, $row);
    }

    public function getTimezone($connote){
        return config('quatius.logistic.couriers.'.$this->type.'.timezone', null); // 'Australia/Melbourne';
    }

    public function transform()
    {
        $data = $this->toArray();
        if (isset($data['id']))
            $data['id'] = $this->getRouteKey();

        return $data;
    }

    public function getHashIdAttribute()
    {        
        return $this->getRouteKey();
    }
}
