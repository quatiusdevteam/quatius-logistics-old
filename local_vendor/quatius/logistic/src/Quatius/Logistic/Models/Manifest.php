<?php

namespace Quatius\Logistic\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Quatius\Account\Traits\HasAccount;
use Quatius\Address\Models\Address;
use Quatius\Address\Traits\HasAddresses;
use Quatius\Framework\Traits\ModelFinder;
use Quatius\Framework\Traits\ParamSetter;
use Quatius\Logistic\Traits\HasLogisticHistories;
use DB;
use Quatius\Account\Traits\HasUser;

class Manifest extends Model implements Transformable{

    use ModelFinder, ParamSetter, SoftDeletes, HasLogisticHistories, HasAccount, HasAddresses, HasUser;
    
    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
        'manifest_no',
        'account_id'
    ];

    public function sender()
	{
		return $this->belongsTo(Address::class, 'sender_address_id');
	}

    public function pickup()
	{
		return $this->belongsTo(Address::class, 'pickup_address_id');
	}

    public function connotes()
	{
		return $this->hasMany(Connote::class);
	}

    public function scopeNumConnotes($query)
	{
        // $query->leftJoin('select count(*) as num_connotes, manifest_id from connotes group by manifest_id');
        $query->leftJoin(
		    DB::raw('(SELECT count(*) as num_connotes, manifest_id FROM `connotes` group by manifest_id) AS tbl_manifest_count'),
		    'id','tbl_manifest_count.manifest_id'
		);
		return $query;
	}

    public function courier()
	{
		return $this->belongsTo(Courier::class);
	}

    public function transform()
    {
        $data = $this->toArray();

        if (isset($data['id']))
            $data['id'] = $this->getRouteKey();
        if (isset($data['account_id']))
            $data['account_id'] = hashids_encode($this->account_id);
        if (isset($data['courier_id']))
            $data['courier_id'] = hashids_encode($this->courier_id);
        return $data;
    }
}
