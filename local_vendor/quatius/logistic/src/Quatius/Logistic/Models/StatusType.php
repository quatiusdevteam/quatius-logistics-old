<?php

namespace Quatius\Logistic\Models;

use Illuminate\Database\Eloquent\Model;

class StatusType extends Model{
    
    public $timestamps = false;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
        'type',
        'status',
    ];
}
