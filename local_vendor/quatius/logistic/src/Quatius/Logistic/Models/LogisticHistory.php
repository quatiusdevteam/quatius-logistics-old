<?php

namespace Quatius\Logistic\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Quatius\Framework\Traits\ModelFinder;
use Quatius\Framework\Traits\ParamSetter;
use Quatius\Logistic\Traits\HasStatusType;
use Illuminate\Support\Arr;

class LogisticHistory extends Model implements Transformable{

    use ModelFinder, ParamSetter, SoftDeletes, HasStatusType;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
        'status_type_id',
	    'value',
	    'comments',
	    'by_user',
	    'occur_at'
    ];

    public function transform()
    {
        $datas = $this->toArray();
		$datas = Arr::except($datas, ['logistic_histories_id','logistic_histories_type','status_type_id']);
		$datas['comments'] = logistic_msg($this);
		if (isset($datas['status']) && is_array($datas['status'])){
			$datas['status'] = $datas['status']['type'];
		}
		return $datas;
    }
}
