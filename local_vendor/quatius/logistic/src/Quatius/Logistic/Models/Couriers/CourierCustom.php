<?php

namespace Quatius\Logistic\Models\Couriers;

use Quatius\Logistic\Models\Courier;

class CourierCustom extends Courier{
    protected $table = "couriers";

    protected static $singleTableType = 'custom';
    
    public function initalise($connote){
        parent::initalise($connote);
    }
    
    public function getItemCost($connote){
        $execute_item = $this->getParams('execute_item', '');
        $return = null;
        if ($execute_item){
            eval($execute_item);
        }
        
        return $return;
    }
    
    public function getConsigmentNo($connote){
        $execute_item = $this->getParams('execute_consigment', '');
        $return = null;
        if ($execute_item){
            eval($execute_item);
        }
        
        return $return;
    }
     
    public function fillRequest($request){
        
        if($request->has('execute_item')){
            $this->setParams('execute_item', $request->get('execute_item',''));
        }
        
        if($request->has('execute_consigment')){
            $this->setParams('execute_consigment', $request->get('execute_consigment',''));
        }
        
        if($request->has('admin_edit')){
            $this->setParams('admin_edit', $request->get('admin_edit',''));
        }
        
        if($request->has('admin_save')){
            $this->setParams('admin_save', $request->get('admin_save',''));
        }
        
        $admin_save = $this->getParams('admin_save', $request->get('admin_save',''));
        if ($admin_save){
            eval($admin_save);
        }
    }
    
    public function transform()
    {
        $data = $this->toArray();
        if (isset($data['id']))
            $data['id'] = $this->getRouteKey();

        return $data;
    }

    public function import($action, $filename , $request){
        if ($action == "carriers-rates"){
            $file = requestFile($request, 'local', $filename, 'logistic/imports/');
            
            return true;
        }
        elseif($action == "accounts-update"){
            $file = requestFile($request, 'local', $filename, 'logistic/imports/');

            return true;
        }

        return parent::import($action, $filename , $request);;
    }
}
