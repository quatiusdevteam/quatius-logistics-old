<?php

namespace Quatius\Logistic\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Quatius\Framework\Traits\ModelFinder;
use Quatius\Framework\Traits\ParamSetter;
use DB;
use Quatius\Account\Traits\HasAccount;
use Quatius\Account\Traits\HasUser;
use Quatius\Address\Models\Address;
use Quatius\Address\Traits\HasAddresses;
use Quatius\Logistic\Traits\HasLogisticHistories;

class Connote extends Model implements Transformable{

    use ModelFinder, ParamSetter, SoftDeletes, HasLogisticHistories, HasAccount, HasAddresses, HasUser;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
        'consignment',
        'parcel_type',
        'reference',
        'weight',
        'width',
        'height',
        'length',
        'atl',
        'instruction',
        'price',
        'declared_value',
        'remark',
        'account_id',
        'is_printed',
        'is_exported',
        'user_id',
        'courier_id',
        'sender_address_id',
        'pickup_address_id',
	];

	public function manifest(){
		return $this->belongsTo(Manifest::class);
	}

    public function getReceiverAttribute(){
        return $this->getAddressTypeRelation('receiver');
    }

    public function sender()
	{
		return $this->belongsTo(Address::class, 'sender_address_id');
	}

    public function pickup()
	{
		return $this->belongsTo(Address::class, 'pickup_address_id');
	}

    public function courier()
	{
		return $this->belongsTo(Courier::class);
	}

    /*public function getSenderAttribute(){
        if (isset($this->getRelations()['sender']))
            return $this->getRelations()['sender'];

        if (!$this->manifest || !$this->manifest->sender)
            return $this->getAddressTypeRelation('sender');

        $this->setRelation('sender', $this->manifest->sender);
        return $this->getRelations()['sender'];
    }

    public function getPickupAttribute(){
        if (isset($this->getRelations()['pickup']))
            return $this->getRelations()['pickup'];

        if (!$this->manifest || !$this->manifest->pickup)
            return $this->getAddressTypeRelation('pickup');

        $this->setRelation('pickup', $this->manifest->pickup);
        return $this->getRelations()['pickup'];
    }
*/
    public function transform()
    {
        $data = $this->toArray();
        if (isset($data['id']))
            $data['id'] = $this->getRouteKey();
        if (isset($data['status_id']))
            $data['status_id'] = hashids_encode($data['status_id']);
        if (isset($data['manifest_id']))
            $data['manifest_id'] = hashids_encode($this->manifest_id);
        if (isset($data['account_id']))
            $data['account_id'] = hashids_encode($this->account_id);
        if (isset($data['remark']))
            $data['remark'] = trans($data['remark']);
        
        if (isset($data['histories']))
            $data['histories'] = $this->historyTransform();
        if (isset($data['sender_address_id']))
            $data['sender_address_id'] = hashids_encode($this->sender_address_id);
        if (isset($data['pickup_address_id']))
            $data['pickup_address_id'] = hashids_encode($this->pickup_address_id);
        if (isset($data['courier_id']))
            $data['courier_id'] = hashids_encode($this->courier_id);

        return $data;
    }

    public function scopeManifestNo($query){
    	return $this->scopeManifestField($query, 'manifest_no');
    }

    public function scopeManifestField($query, $field){
		$query->leftJoin(
		    DB::raw('(SELECT id as manifest_link_id , '.$field.' FROM `manifests`) AS manifest_tb'),
		    'manifest_id','manifest_link_id'
		);
    	return $query;
    }
}
