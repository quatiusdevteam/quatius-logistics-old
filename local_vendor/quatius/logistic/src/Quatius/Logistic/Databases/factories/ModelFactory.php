<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

//factory(\Quatius\Logistic\Models\Connote::class, 3)->create(["account_id"=>1])
$factory->define(Quatius\Logistic\Models\Connote::class, function (Faker\Generator $faker) {
    return [
        'parcel_type' => $faker->randomElement(['carton', 'pallet']),
        'consignment'=> "".$faker->numberBetween(900000000, 990000000),
        'reference' => $faker->word(),
        'weight' => $faker->randomFloat(2, 0.2, 20),
        "width" => $faker->numberBetween(10, 200),
        "height" => $faker->numberBetween(10, 200),
        'length'=> $faker->numberBetween(10, 200),
        'atl' => $faker->randomElement([0, 1]),
        'instruction' => $faker->sentences(1, true),
        'price' => $faker->randomFloat(4, 5, 100)
    ];
});

$factory->define(Quatius\Logistic\Models\ConnoteContact::class, function(Faker\Generator $faker) {
    return [
        'type' => $faker->randomElement(['sender', 'receiver']),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'phone_1' => $faker->phoneNumber,
        'address_1' => $faker->streetAddress,
        'city' => $faker->city,
        'state' => $faker->randomElement(['VIC','ACT','QLD','NSW','TAS','WA','SA','NT']),
        'country' => 'Australia',
        'postcode' => $faker->postcode
    ];
});

$factory->define(Quatius\Logistic\Models\LogisticHistory::class, function(Faker\Generator $faker) {
    return [
        'created_at'=> $faker->date('Y-m-d H:i:s')
    ];
});