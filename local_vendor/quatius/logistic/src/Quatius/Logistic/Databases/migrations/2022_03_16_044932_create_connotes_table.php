<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConnotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manifests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('account_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('manifest_no', 100)->unique();
            $table->text('params')->nullable();
            $table->integer('sender_address_id')->unsigned()->nullable();
            $table->integer('pickup_address_id')->unsigned()->nullable();
            $table->integer('courier_id')->unsigned()->nullable();
            
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('connotes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('account_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('consignment', 100)->nullable()->unique();
            $table->string('courier_consignment')->nullable();
            $table->integer('courier_id')->unsigned()->nullable();
            $table->integer('manifest_id')->nullable()->unsigned()->index();
            $table->integer('sender_address_id')->unsigned()->nullable();
            $table->integer('pickup_address_id')->unsigned()->nullable();
            $table->string('reference')->nullable()->default('');
            $table->integer('items')->unsigned()->default(1);
            $table->decimal('weight', 10, 4)->default(0.1);
            $table->integer('width')->unsigned()->default(0);
            $table->integer('height')->unsigned()->default(0);
            $table->integer('length')->unsigned()->default(0);
            $table->string('parcel_type')->default("carton");
            $table->decimal('declared_value')->default(0);
            $table->tinyInteger('atl')->default(0); //Authorize to leave
            $table->string('remark')->nullable()->default(''); //change to error status code
            
            $table->text('instruction')->nullable();

            $table->decimal('price', 15, 4)->default(0);
            $table->string('currency')->default('AUD');
            
            $table->tinyInteger('is_printed')->default(0);
            $table->tinyInteger('is_exported')->default(0)->nullable();
            $table->text('params')->nullable();
            $table->foreign('manifest_id')->references('id')->on('manifests')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('status_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type', 150)->unique();
            $table->tinyInteger('status')->default(1); // 1=unpublished, 2=published, 
            $table->softDeletes();
        });

        Schema::create('logistic_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('logistic_histories_id')->unsigned();
            $table->string('logistic_histories_type');
            $table->integer('status_type_id')->unsigned()->index();
            $table->string('value',150)->nullable();
            $table->string('comments')->default('');
            $table->text('params')->nullable();
            $table->integer('by_user')->nullable()->unsigned();
            $table->string('occur_at')->nullable()->default('');
            $table->foreign('status_type_id')->references('id')->on('status_types')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logistic_histories');
        Schema::dropIfExists('status_types');
        Schema::dropIfExists('connotes');
        Schema::dropIfExists('manifests');
    }
}
