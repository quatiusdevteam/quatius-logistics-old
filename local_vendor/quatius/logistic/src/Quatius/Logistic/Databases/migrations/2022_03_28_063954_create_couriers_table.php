<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouriersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('couriers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',150)->default('');
            $table->string('label',150)->default('');
            $table->string('description',250)->nullable()->default('');
            $table->string('access_key',50)->nullable()->unique();
            $table->string('note',250)->nullable()->default('');
            $table->string('type',50)->default('custom');
            $table->text('params')->nullable();
            $table->tinyInteger('status')->default(1); // 1=unpublished, 2=published, 3=maintained
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('couriers');
    }
}
