<?php

namespace Quatius\Logistic\Providers;

use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;
use Quatius\Logistic\Commands\CmdRemoteTracking;
use Quatius\Logistic\Events\EventHistoryUpdated;
use Quatius\Logistic\Listeners\LogisticLisenters;
use Quatius\Logistic\Listeners\SendTrackingNotification;
use Quatius\Logistic\Middleware\AccessLevel;
use Quatius\Logistic\Models\Courier;
use Route;

class LogisticServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
	 public function boot()
    {	
        Route::bind('connote_id',function($value) {
	        return hashids_decode($value)?:abort(404,'Route Key Invalid');
	    });

        Route::bind('manifest_id',function($value) {
	        return hashids_decode($value)?:abort(404,'Route Key Invalid');
	    });

        if ($this->app->runningInConsole()) {
            $this->commands([
                CmdRemoteTracking::class,
            ]);
        }
    }
    
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        foreach(config("quatius.logistic.listeners", []) as $event=>$listen){
            if ($listen)
                Event::listen($event, $listen);
        }
        
        Event::subscribe(LogisticLisenters::class);

        $router = $this->app['router'];
        $router->pushMiddlewareToGroup('api', AccessLevel::class);
        $router->pushMiddlewareToGroup('web', AccessLevel::class);

        $this->loadMigrationsFrom(__DIR__.'/../Databases/migrations');
        $this->app->make(Factory::class)->load(__DIR__.'/../Databases/factories');

        $this->app->singleton('Quatius\Logistic\Repositories\ConnoteRepository', 'Quatius\Logistic\Repositories\Handlers\ConnoteHandler');
        $this->app->bind('connote', 'Quatius\Logistic\Repositories\ConnoteRepository');
        $this->app->singleton('Quatius\Logistic\Repositories\CourierRepository', 'Quatius\Logistic\Repositories\Handlers\CourierHandler');
        $this->app->bind('courier', 'Quatius\Logistic\Repositories\CourierRepository');

        //register Couriers subclass
        $courierClasses = [];
        foreach (config('quatius.logistic.couriers', []) as $courier){
            if (!isset($courier['class'])) continue;
                $courierClasses[] = $courier['class'];
        }
        Courier::$singleTableSubclasses = $courierClasses;
    }
}
