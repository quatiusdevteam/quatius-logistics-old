<?php

namespace Quatius\Logistic\Commands;

use Illuminate\Console\Command;

class CmdRemoteTracking extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'logistic:pull-tracking';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pull remote tracking history';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
            $numData = app('courier')->syncRemoteTracking();
            $message = " tracking received from Remote Server";

        } catch (\Exception $e) {
            $message = "Error: "+$e->getMessage();
        }

        $this->writeStatus($message, $numData);
    }

        /**
     * Format the status output for the queue worker.
     *
     * @param  \Illuminate\Contracts\Queue\Job  $job
     * @param  string  $status
     * @param  string  $type
     * @return void
     */
    protected function writeStatus($msg, $change, $type='info')
    {
        $this->output->writeln(sprintf(
            "<{$type}>[%s][%s] %s</{$type}> %s",
            \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            "0", str_pad("{$change}:", 6), $msg
        ));
    }
}
