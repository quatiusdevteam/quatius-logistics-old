<?php

namespace Quatius\Logistic\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;
use Quatius\Logistic\Events\EventHistoryUpdated;
use Quatius\Logistic\Models\Connote;
use Quatius\Logistic\Notifications\SendWithDriverNotification;

class ListenWithDriver
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  object  $connote
     * @return void
     */
    public function handle(Connote $connote)
    {
        //app('log')->info('ListenWithDriver', [$connote->receiver->email, $connote]);
        //Notification::route('mail',[$connote->receiver->email])->notify(new SendWithDriverNotification($connote));
    }
}
