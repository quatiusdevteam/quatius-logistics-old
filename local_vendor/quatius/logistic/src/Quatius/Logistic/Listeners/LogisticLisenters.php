<?php

namespace Quatius\Logistic\Listeners;

use Illuminate\Support\Facades\Notification;
use Quatius\Logistic\Jobs\JobSubmitApiManifest;

class LogisticLisenters
{
    public function subscribe($events){
        $events->listen(['status-change.connote.booking accepted'],function($connote){
            if (config('quatius.logistic.connote.notify.enable',true) && config('quatius.logistic.connote.notify.accepted', true))
                Notification::route("mail", [env("LOGISTIC_OVERRIDE_EMAIL", $connote->receiver->email)])->notify(new \Quatius\Logistic\Notifications\NotifySubmitted($connote));
        });
        $events->listen(['status-change.connote.with driver', 'status-change.connote.with driver'],function($connote){
            if (config('quatius.logistic.connote.notify.enable',true) && config('quatius.logistic.connote.notify.with-driver', true))
                Notification::route("mail", [env("LOGISTIC_OVERRIDE_EMAIL", $connote->receiver->email)])->notify(new \Quatius\Logistic\Notifications\NotifyWithDriver($connote));
        });
        $events->listen(['status-change.connote.delayed'],function($connote){
            if (config('quatius.logistic.connote.notify.enable',true) && config('quatius.logistic.connote.notify.delay', true))
                Notification::route("mail", [env("LOGISTIC_OVERRIDE_EMAIL", $connote->receiver->email)])->notify(new \Quatius\Logistic\Notifications\NotifyDelayed($connote));
        });
        $events->listen(['status-change.connote.delivered', 'status-change.connote.customer pickup', 'status-change.connote.completed', 'status-change.connote.pod updated' ],function($connote){
            if (config('quatius.logistic.connote.notify.enable',true) && config('quatius.logistic.connote.notify.delivered', true))
                Notification::route("mail", [env("LOGISTIC_OVERRIDE_EMAIL", $connote->receiver->email)])->notify(new \Quatius\Logistic\Notifications\NotifyDelivered($connote));
        });
        $events->listen(['status-change.connote.cancel','status-change.connote.delivery error','status-change.connote.rejection','status-change.connote.undeliverable','status-change.connote.unknown'],function($connote){
            if (config('quatius.logistic.connote.notify.enable',true) && config('quatius.logistic.connote.notify.undeliverable', true))
                Notification::route("mail", [env("LOGISTIC_OVERRIDE_EMAIL", $connote->receiver->email)])->notify(new \Quatius\Logistic\Notifications\NotifyCancelled($connote));
        });

        $events->listen(['status-change.connote.destroyed','status-change.connote.damaged','status-change.connote.lost'],function($connote){
            if (config('quatius.logistic.connote.notify.enable',true) && config('quatius.logistic.connote.notify.damaged', true))
                Notification::route("mail", [env("LOGISTIC_OVERRIDE_EMAIL", $connote->receiver->email)])->notify(new \Quatius\Logistic\Notifications\NotifyDamaged($connote));
        });
        
        $events->listen('status-change.manifest.submitted', function ($manifest) {
            if ($manifest->courier_id && $manifest->courier && $manifest->courier->getParams('api.enable', false)){
                JobSubmitApiManifest::dispatch($manifest->id);
            }
        });

        $events->listen('status-change.connote.*', function ($eventName, $datas) {
            $connote = $datas[0];
            $hist = $datas[1];
            $status = $hist->status->type;
            
            if (!($connote->manifest_id && $connote->manifest)) return; // manifest update
            
            $total = app('connote')->whereManifestId($connote->manifest_id)->count();

            if (array_search($status, config('quatius.logistic.status.group.complete')) !== false){
                if ($total == 1){
                    $connote->manifest->setStatus("completed",[]);
                    return;
                }

                $numOfCompleted = app('connote')->whereManifestId($connote->manifest_id)->latestStatus("status", null, true, false)->whereIn('status', config('quatius.logistic.status.group.complete',[]))->count();

                if ($numOfCompleted == $total)
                    $connote->manifest->setStatus("completed",[]);
                else
                    $connote->manifest->setStatus("incomplete",[]);
            }
            elseif (array_search($status, config('quatius.logistic.status.group.transit')) !== false){
                if ($total == 1){
                    $connote->manifest->setStatus("in transit",[]);
                    return;
                }

                $numOfTransit = app('connote')->whereManifestId($connote->manifest_id)->latestStatus("status", null, true, false)->whereIn('status', config('quatius.logistic.status.group.transit',[]))->count();
                if ($numOfTransit == $total)
                    $connote->manifest->setStatus("in transit",[]);
            }
            elseif ($status == 'submitted'){
                if ($total == 1){
                    $connote->manifest->setStatus($status,[]);
                    return;
                }

                $numOfSubmitted = app('connote')->whereManifestId($connote->manifest_id)->latestStatus("status", null, true, false)->whereStatus($status)->count();
                if ($numOfSubmitted == $total){
                    $connote->manifest->setStatus($status,[]);
                }
            }
            
        });
    }
}
