<?php

namespace Quatius\Logistic\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class JobPullRemoteTracking implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 2;

    private $connote;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($connote)
    {
        $this->connote = $connote;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!$this->connote) {
            app('log')->error("JobPullRemoteTracking: No Connote");
            return;
        }
-       config()->set('quatius.logistic.history.status-active', config('quatius.logistic.history.status-system', 0));
        $changes = app('courier')->syncARemoteTracking($this->connote);

        app('log')->info("JobPullRemoteTracking: ".$changes ." tracking received from ".$this->connote->manifest->courier->label, $this->connote->only(['id','consignment','courier_consignment']));
    }

    public function displayName()
    {
        return "JobPullRemoteTracking: ".$this->connote->id;
    }
}
