<?php

namespace Quatius\Logistic\Jobs;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\File;

class JobSubmitApiManifest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $manifestId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($manifestId)
    {
        $this->manifestId = $manifestId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $manifest = \Quatius\Logistic\Models\Manifest::with(['courier', 'connotes'])->find($this->manifestId);
        if (!$manifest) return;
        
        if ($manifest->courier_id && $manifest->courier){
            if ($manifest->courier->getParams('api.simulate', true)){
                $mock = new MockHandler([
                    new Response(200, [], File::get(base_path('tests/Support/stubs/4px/response-gos.api.submitOrder.json')))
                ]);

                $client = new Client(['handler' => HandlerStack::create($mock)]);
                
                $manifest->courier->setHttpClient($client); //set Mock client
            }


            $manifest->courier->submitManifest($manifest);
        }
    }

    public function displayName()
    {
        return "JobSubmitApiManifest: ".$this->manifestId;
    }
}
