<?php

namespace Quatius\Logistic\Middleware;

use Closure;
class AccessLevel
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        $middlewareGroup = $request->route()->gatherMiddleware();
        
        $filtered = array_intersect(["auth", "auth:web", "auth:api", "auth:admin.web"], $middlewareGroup);
        
        if ($filtered) // only Auth middleware group
        {
            $guard = array_values($filtered)[0];
            
            if ($request->user() && $request->user()->hasRole("superuser") || $guard == "auth:admin.web")
                config()->set('quatius.logistic.history.status-active', config('quatius.logistic.history.status-system', 0));
            else
                config()->set('quatius.logistic.history.status-active', config('quatius.logistic.history.status-admin', 1));
        }
        else
            config()->set('quatius.logistic.history.status-active', config('quatius.logistic.history.status-public', 2));

        return $next($request);
    }
}