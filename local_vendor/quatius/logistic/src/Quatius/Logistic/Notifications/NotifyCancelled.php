<?php

namespace Quatius\Logistic\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Notifications\Messages\MailMessage;

class NotifyCancelled extends Notification implements ShouldQueue
{
    use Queueable;
    public $tries = 1;

    private $connote;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($connote)
    {
        $this->connote = $connote;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mail = (new MailMessage)  
                ->subject('CONNOTE CANCELLED: Consignment-'.$this->connote->consignment)
                ->view('Logistic::connote.emails.cancelled-notification', ['connote' => $this->connote]);

        if(env("LOGISTIC_EMAIL_BCC", ""))
            $mail->bcc(env("LOGISTIC_EMAIL_BCC", ""));

        return $mail;        
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function displayName()
    {
        return "Notify Cancelled :Connote ".$this->connote->consignment;
    }
}
