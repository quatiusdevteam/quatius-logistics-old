<?php

namespace Quatius\Logistic\Repositories\Handlers;

use Exception;
use Quatius\Address\Models\Address;
use Quatius\Logistic\Models\Manifest;
use Quatius\Logistic\Repositories\ConnoteRepository;
use Quatius\Framework\Repositories\QuatiusRepository;

class ConnoteHandler extends QuatiusRepository implements ConnoteRepository
{

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return \Quatius\Logistic\Models\Connote::class;
    }

    /**
     * Manifect Model class name
     *
     * @return string
     */
    public function manifestModel()
    {
        return app(\Quatius\Logistic\Models\Manifest::class);
    }

    /**
     * Specify Model class name
     * @param array $connoteIds 
     * @param string $status 
     * @return Connotes
     */
    public function statusChange($connoteIds, $status='submitted', $postData = []){

        $connotes = $this->findWhereIn('id', $connoteIds);
        $status = trim($status);

        if ($connotes){
            $data = [
                'by_user'=>app('account')->userId(),
                'comments'=>isset($postData['comments'])?$postData['comments']:""
            ];

            foreach ($connotes as $connote){
                $connote->setStatus($status, $data);
            }
            return $connotes;
        }
        return collect([]);
    }

    public function createManifest($connoteIds, $status, $data){
        if ($status != config('quatius.logistic.manifest.create-status','submitted')) 
            return false;

        $connotes = $this->whereIn('id', $connoteIds)->get();
        $groupToManifest = $connotes->groupBy(function ($item, $key) {
            return $item->courier_id.'_'.$item->pickup_address_id.'_'.$item->sender_address_id;
        });
        
        $dataHist = [
            'by_user'=>app('account')->userId()
        ];

        if (!app('account')->detail()) return false;
        $manifests = collect([]);
        foreach($groupToManifest as $manifestConnotes){
    
            $manifest = new Manifest(["manifest_no"=>generateManifect()]);
            
            $manifest->account_id = app('account')->detail()?app('account')->detail()->id:null;
            $manifest->sender_address_id = $manifestConnotes->first()->sender_address_id;
            $manifest->pickup_address_id = $manifestConnotes->first()->pickup_address_id;
            $manifest->courier_id = $manifestConnotes->first()->courier_id;

            $courier = app('courier')->load($manifest->courier_id?:0);

            $manifest->user_id = app('account')->userId();

            $manifest->setRelation('connotes', $manifestConnotes);
            $manifest->save();
            
            foreach ($manifest->connotes as $connote){
                $connote->setRelation('manifest', $manifest);
                $connote->manifest_id = $manifest->id; //Important
                $connote->save();
            }
            
            $manifest->setStatus(config('quatius.logistic.manifest.create-status','created'), $dataHist);
            
            $courier->createManifest($manifest, $dataHist);

            $manifests->add($manifest);
        }
        // $courier = app('courier')->load(isset($data['courier_id'])?$data['courier_id']:0);
        // $dataHist = [
        //     'by_user'=>app('account')->userId()
        // ];
/*
        if ($courier && isset($data['sender_address_id']) && isset($data['pickup_address_id']) 
            
        }*/
        return $manifests;
    }

    public function saveFormData($data, $connote = null){

        array_walk($data, function(&$val){$val = trim($val);}); //trim each values

        $errorMsg = "";
        $errorInputs = [];
        $data = app('address')->formatSimplify($data, function($inputField, $msg, $val) use(&$errorInputs, &$errorMsg){
            $errorInputs[$inputField] = $msg;
            $errorMsg = $msg;
        });
        
        $data["atl"] = ($data["atl"] == "Y" || $data["atl"] == "y" || $data["atl"] == "1" || $data["atl"] == 1)?1:0;
        $data["remark"] =  $errorMsg;
        $data["user_id"] = app('account')->userId();
        unset($data['account_id']);
        $accountId = app('account')->getAccountId(null);
        if ($connote){
            $connote = $this->update($data, $connote->id);
            if (!$connote->consignment)
                $connote->consignment = generateConsignment($connote);
        }
        else{
            $data['account_id'] = $accountId;
            $connote = $this->create($data);
            $connote->consignment = generateConsignment($connote);
        }
        $connote->unsetParams("error"); 

        if (!!$this->byUserAccount(1)->where('id','<>', $connote->id)->whereReference($data["reference"])->count()){
            $errorInputs['reference'] = "Reference already Exists";
            $connote->remark =  "Reference already Exists";
        }

        if ($errorInputs){
            $connote->mergeParams("error.inputs", $errorInputs);
        }

        $connote->save();

        $receiver = $connote->addresses->where('address_type','receiver')->first();
        if ($receiver){
            $receiver->update($data);
        }else{
            $receiver = new Address($data);
            $receiver->address_type="receiver";
            $connote->addresses()->save($receiver);
        }

        $connote->setRelation("receiver", $receiver);
        return $connote;
    }

    public function validate($data){
        
        return '';
    }

    public function bookingRollback($connote, $status="", $data=[]){
        $pendingStatus = config('quatius.logistic.status.group.pending',[]);
        $pendingStatus[] = null;

        $isCancelable = true;

        if (isset($connote->status))
            $isCancelable = array_search($connote->status, $pendingStatus) !== false;
        
        if (!$isCancelable) return false;
        
        $manifest = $connote->manifest_id?$connote->manifest:null;
        $courier = $connote->courier_id?$connote->courier:null;
        if ($courier){
            if (!$courier->cancelConnote($connote)) // if api not permitted
                return false;
        }

        $connote->setParams('rollback.manifest_id', $manifest?$manifest->id:null);
        $connote->setParams('rollback.consignment',$connote->courier_consignment);
        $connote->consignment = null;
        $connote->courier_consignment = null;
        $connote->manifest_id = null;
        $connote->is_printed = 0;
        if (isset($data['comments']))
            $connote->remark = $data['comments'];
        $connote->save();

        if ($manifest && !$this->whereManifestId($manifest->id)->count())
            $manifest->delete();

        return true;
    }


    public function numConnotesIndicator(){
        return $this->byUser()->latestStatus()->where( function($query){
            $statusPending = config('quatius.logistic.status.group.pending',[]);
            if (($key = array_search('submitted', $statusPending)) !== false) {
                unset($statusPending[$key]);
            }
            if (($key = array_search('booking pending', $statusPending)) !== false) {
                unset($statusPending[$key]);
            }
            return  $query
                    ->orWhere('remark','<>','')
                    ->orWhereNull('status')
                    ->orWhereIn('status', $statusPending);                            
        })->count();
    }

    public function numConnotesDanger(){
        return $this->byUserAccount()->latestStatus()->where( function($query){
            return  $query->whereIn('status', config('quatius.logistic.status.group.undeliverable',[]))->where('status_value','=', null);
        })->count();
    }

    
}
