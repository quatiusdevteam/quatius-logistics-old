<?php

namespace Quatius\Logistic\Repositories\Handlers;

use Exception;
use Quatius\Framework\Repositories\QuatiusRepository;
use Quatius\Logistic\Repositories\CourierRepository;

class CourierHandler extends QuatiusRepository implements CourierRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return \Quatius\Logistic\Models\Courier::class;
    }
    
    public function load($id)
    {
        $courier = $id?$this->findWhere(['id'=>$id, "status"=>2])->first():null;

        if (!$courier)
            $courier = $this->findWhere(['type'=>config('quatius.logistic.default_courier',''), "status"=>2])->first();
            
        if (!$courier) {
            $classCourier = config('quatius.logistic.couriers.'.config('quatius.logistic.default_courier','').'.class','');
            if ($classCourier){
                $courier = app()->make($classCourier)->fill(['name'=>config('quatius.logistic.default_courier',''), "status"=>2]);
                $courier->params = json_encode(config('quatius.logistic.couriers.'.config('quatius.logistic.default_courier','').'.init_params',[]));
                $courier->save();
            }
        }
        
        return $courier;
    }

    public function syncARemoteTracking($id){
        $connote = null;
        if ($id instanceof \Quatius\Logistic\Models\Connote)
            $connote = $id;
        else
            $connote =  $this->with(['histories','manifest.courier'])->findOrFail($id);
        
        if (!$connote) return 0;
        
        $before = $connote->histories->count();
        $connote->manifest->courier->updateRemoteTracking($connote);
        $after = $connote->histories->count();
        return $after - $before;
    }

    public function syncRemoteTracking(){
        $lookupOnly = config('quatius.logistic.status.group.transit');
        config()->set('quatius.logistic.history.status-active', config('quatius.logistic.history.status-system', 0));
        $lookupOnly[] = 'booking pending'; // include pending
        $connotes = app('connote')->with(['histories','manifest.courier'])->latestStatus()->whereIn('status', $lookupOnly)->where('updated_at', '>', now()->subDays(30)->endOfDay())->get();
        $numOfData = 0;
        foreach ($connotes as $connote){
            try{
                $numOfData += $this->syncARemoteTracking($connote);
            }catch(\Exception $e){ //ignore errors

            }
        }

        return $numOfData;
    }
}
