<?php
namespace Quatius\Logistic\Repositories\Presenters;

use Prettus\Repository\Presenter\FractalPresenter;

class ConnoteIndexPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ConnoteIndexTransformer();
    }
}
