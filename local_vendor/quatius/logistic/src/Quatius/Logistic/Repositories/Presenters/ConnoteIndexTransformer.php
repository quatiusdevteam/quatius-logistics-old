<?php

namespace Quatius\Logistic\Repositories\Presenters;

use Quatius\Logistic\Models\Connote;
use League\Fractal\TransformerAbstract;

/**
 * Class PostTransformer
 * @package namespace App\Modules\GenerateTransformers;
 */
class ConnoteIndexTransformer extends TransformerAbstract
{

    /**
     * Transform the Post entity
     * @param Quatius\Logistic\Models\Connote $model
     *
     * @return array
     */
    public function transform(Connote $model)
    {
        return $model->transform();
    }
}
