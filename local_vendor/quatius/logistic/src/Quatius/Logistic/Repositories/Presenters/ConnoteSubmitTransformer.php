<?php

namespace Quatius\Logistic\Repositories\Presenters;

use Quatius\Logistic\Models\Connote;
use League\Fractal\TransformerAbstract;

/**
 * Class PostTransformer
 * @package namespace App\Modules\GenerateTransformers;
 */
class ConnoteSubmitTransformer extends TransformerAbstract
{

    /**
     * Transform the Post entity
     * @param Quatius\Logistic\Models\Connote $connote
     *
     * @return array
     */
    public function transform(Connote $connote)
    {
        $data = $connote->transform();

        $data['receiver'] =  $connote->receiver->company.' '.$connote->receiver->first_name.' '.$connote->receiver->last_name;
        $data['destination'] =  $connote->receiver->address_1.' '.$connote->receiver->address_2.' '.$connote->receiver->city.' '.$connote->receiver->state.'. '.$connote->receiver->postcode;
        unset($data['addresses']);

        return $data;
    }
}
