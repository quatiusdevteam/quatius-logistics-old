
@foreach ($menus as $menu)
    <?php 
        if(!$menu->checkAccess(user('admin.web'))) continue;
    ?>
    
    @if ($menu->description)
        {!! renderBlade($menu->description, ['menu' => $menu]) !!}
    @else
	
        @if ($menu->hasChildren())
        <li class="treeview {{ $menu->active ?'active':'' }}">
            <a href="{{$menu->url=='#'? '#': trans_url($menu->url)}}" >
                <i class="{{ $menu->icon ?: 'fa fa-angle-double-right' }}"></i> <span>{{$menu->name}}</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            @include('menu::menu.sub.admin', array('menus' => $menu->getChildren()))
        </li>
        @else
        <li class="{{ $menu->active ?'active':'' }}">
            <a href="{{$menu->url=='#'? '#': trans_url($menu->url)}}">
                <i class="{{ $menu->icon ?: 'fa fa-angle-double-right' }}"></i>
                <span>{{$menu->name}}</span>
            </a>
        </li>
        @endif
    @endif
@endforeach
