<div class="login-box">
<div class="container">
	<div class="col-md-4">
		<div class="row">
			<div class="panel panel-default">
			<div class="panel-body custom_box">
				<div class="col-md-12 col-xs-12">					
					<div class="row">
						<div class="col-xs-12">
							<div class="panel-logo">
								<a href="home"><img src="images/quatius-logo.png"></a>
							</div>
						</div>
						<div class="col-xs-12">
							<div class="row">
								<h3 class="login-box-msg" >WELCOME</h3>
							</div>
						</div>
						<div class="col-xs-12">
							<div class="row">
								
								{!!Form::vertical_open()
			                    ->id('login')
			                    ->method('POST')
			                    ->action('login')
			                    ->class('white-row')!!}
								{{ csrf_field() }}
			                    <div class="form-group has-feedback">
			                    	<span class="fa fa-envelope form-control-feedback"></span>
			                    	{!! Form::email('email','')
	                    			-> placeholder(trans('user::user.user.placeholder.email'))!!}
	                    			
	                    		</div>
	                    		<div class="form-group has-feedback">
	                    			{!! Form::password('password','')
	                    			-> placeholder(trans('user::user.user.placeholder.password'))!!}
	                    			<span class="fa fa-lock form-control-feedback"></span>
	                    		</div>
	                    		{!! Form::hidden(config('user.params.type'))!!}
	                    			                    		
                    			<div class="col-xs-12">
                    				<div class="small row custom-checkbox">
                    					<input type="checkbox" id="rememberme" name="rememberme"  value="1"> 
							<label for="rememberme"><span class="checkbox">Keep me signed in</span></label>
                    				</div>
                    			</div>
                    			<br />
                    			<!-- {!! Form::submit('Sign in')->class('hvr-sweep-to-right btn btn-primary form-control')!!} -->
                    			<button type="submit" class="hvr-sweep-to-right btn btn-primary form-control">Sign in</button>
                    			{!! Form::close() !!}
                    			<br />
                    			<div class="col-xs-12">
                    				<div class="row center-text">
                    					<p class="login-box-msg">
                    						<a href="{{trans_url("/password/reset?role=$guard")}}">Oops, I have forgotten my password </a>
                    					</p> 
							
                    					<p class="login-box-msg" style="border: 0; margin-top: 0; padding-top: 0;"><a class="btn btn-link" style="padding: 0;" href="{{trans_url('contact-us')}}">Request an account now</a></p>
                    				</div>
                    			</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
</div>
</div>
