
<div class="login-box">
<div class="container">
	<div class="col-md-4">
		<div class="row">
			<div class="panel panel-default">
			<div class="panel-body custom_box">
				<div class="col-md-12 col-xs-12">					
					<div class="row">
						<div class="col-xs-12">
							<div class="panel-logo">
								<a href="../login"><img src="../images/quatius-logo.png"></a>
							</div>
						</div>
						<div class="col-xs-12">
							<div class="row">
								<h3 class="login-box-msg" >Forgot Password</h3>
							</div>
						</div>
						<div class="col-xs-12">
							<div class="row">
								
								{!!Form::vertical_open()
			                    ->method('POST')
			                    ->action('password/email')!!}
			                    {!! csrf_field() !!}
	                    			                   
	                    		@if (Session::has('status'))
			                    <div class="alert alert-info">
			                        <strong>Info!</strong> {!!  Session::pull('status'); !!}
			                    </div>
			                   
			                    @endif
			                    	                    		                    			
                    			{!! Form::email('email','')
	                    		-> placeholder(trans('user::user.user.placeholder.email'))!!}
	                    		<span class="fa fa-envelope form-control-feedback"></span>
                    			<br />
                    			<!-- {!! Form::submit('Send password')->class('hvr-sweep-to-right btn btn-primary form-control')!!} -->
                    			<button type="submit" class="hvr-sweep-to-right btn btn-primary form-control">Send password</button>

                    			{!! Form::close() !!}
                    			<br />
                    			<div class="col-xs-12">
                    				<div class="row center-text">
                    					<p class="login-box-msg">
                    						<a class="btn btn-link" href="{{trans_url("/login?role=$guard")}}">Back to sign in</a><br>
                    					</p>                    					
                    				</div>
                    			</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
</div>
</div>




<!-- 
<div class="container">
    <div class="row profile">
        <div class="col-md-6  col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Reset <small>Password</small></div>
                <div class="panel-body">
                    {!!Form::vertical_open()
                    ->method('POST')
                    ->action('password/email')!!}
                    {!! csrf_field() !!}
                    @if (Session::has('status'))
                    <div class="alert alert-info">
                        <strong>Info!</strong> {!!  Session::pull('status'); !!}
                    </div>
                    @else
                    If you have forgotten your password - reset it.
                    @endif
                    {!!Form::text('email')!!}
                    <div class="row">
                        
                        <div class="col-xs-6">
                            {!! Form::hidden(config('user.params.type'))!!}
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Send password</button>
                        </div>
                        
                    </div>
                    {!!Form::Close()!!}
                    <br>
                    <a href="{{trans_url("/login?role=$guard")}}">Back to login</a><br>
                </div>
            </div>
        </div>
    </div>
</div>
 -->