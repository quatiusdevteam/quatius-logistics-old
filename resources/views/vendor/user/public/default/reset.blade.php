<div class="reset-box">
<div class="container">
	<div class="col-md-4">
		<div class="row">
			<div class="panel panel-default">
			<div class="panel-body custom_box">
				<div class="col-md-12 col-xs-12">					
					<div class="row">
						<div class="col-xs-12">
							<div class="panel-logo">
								<a href="home"><img src="../../images/quatius-logo.png"></a>
							</div>
						</div>
						<div class="col-xs-12">
							<div class="row">
								<h3 class="login-box-msg" >Reset Password</h3>
							</div>
						</div>
						<div class="col-xs-12">
							<div class="row">
								
								{!!Form::vertical_open()
			                    ->id('reset')
			                    ->method('POST')
			                    ->action('/password/reset')!!}
			                    {!! csrf_field() !!}
			                    {!! Form::hidden('token')->value($token) !!}
			                    {!! Form::hidden(config('user.params.type'))!!}
			
			                    {!! Form::email('email')
			                    -> placeholder(trans('user::user.user.placeholder.email'))!!}
			
			                    {!! Form::password('password')
			                    -> placeholder(trans('user::user.user.placeholder.password'))!!}
			
			                    {!! Form::password('password_confirmation')
			                    -> placeholder(trans('user::user.user.placeholder.password_confirmation'))!!}
			
			                    <!--{!! Form::submit(trans('user::user.reset'))!!}-->
					   			<button type="submit" class="hvr-sweep-to-right btn btn-primary form-control">Reset password</button>
			
			                    {!! Form::close() !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
</div>
</div>