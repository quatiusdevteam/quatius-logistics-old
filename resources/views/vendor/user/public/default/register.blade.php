@includeStyle('css/account.css')
{{$guard}}

<div class="container">
	<div class="col-md-6 col-md-offset-3">
		<div class="panel panel-default">
			<div class="panel-body custom_box">
				<div class="col-md-12 col-xs-12">					
					<div class="row">
						<div class="col-xs-12">
							<div class="row">
								<h3 class="title center-text" >SIGN UP</h3>
							</div>
						</div>
						<div class="col-xs-12">
							<div class="row">			
							@include('public::notifications')													
			                    {!!Form::vertical_open()			                    
			                    ->id('register')
			                    ->action('register')
			                    ->method('POST')
			                    ->class('white-row')!!}
	                    			                   
	                    		                			                    
			                    {!! Form::select('gender', '')			                    
			                    -> options(trans('user::user.user.options.sex'))
			                    -> placeholder(trans('user::user.user.placeholder.sex'))!!}
			                    
	                    		{!! Form::text('first_name', '')
	                    		->id('first_name')			                    
			                    -> placeholder(trans('user::user.user.placeholder.first_name'))!!}
			                    
			                    {!! Form::text('last_name', '')
			                    ->id('last_name')			                    
			                    -> placeholder(trans('user::user.user.placeholder.last_name'))!!}
			                    			                    	                    		
	                    		{!! Form::email('email', '')			                    
			                    -> placeholder(trans('user::user.user.placeholder.email'))!!}
			                    
			                    {!! Form::tel('phone', '')						        
						        -> placeholder(trans('user::user.user.placeholder.phone')) !!}
        						
        						{!! Form::text('address_1', '')        
        						-> placeholder(trans('user::user.user.placeholder.address_1')) !!}
        					
        						{!! Form::text('address_2', '')						        
						        -> placeholder(trans('user::user.user.placeholder.address_2')) !!}
						        
						        {!! Form::text('city', '')						        
						        -> placeholder(trans('user::user.user.placeholder.city')) !!}
						        <div class="col-xs-8">
						        	<div class="row">
						        	{!! Form::select('state', '')			                    
				                    -> options(trans('user::user.user.options.states'))
				                    -> placeholder(trans('user::user.user.placeholder.state'))!!}
						        	</div>
						        </div>
						        <div class="col-xs-4">
						        	<div style="margin-right: -15px;">
						        	{!! Form::text('postcode', '')						        
						        	-> placeholder(trans('user::user.user.placeholder.postcode')) !!}
						        	</div>
						        </div>
						        {!! Form::select('country', '')			                    
				                    -> options(trans('user::user.user.options.countries'))
				                    -> placeholder(trans('user::user.user.placeholder.country'))!!}
                    			<br /><br />
                    			{!! Form::password('password')			                    
			                    -> placeholder(trans('user::user.user.placeholder.password'))!!}
			                    {!! Form::password('password_confirmation')			                    
			                    -> placeholder(trans('user::user.user.placeholder.password_confirmation'))!!}
			                    
			                    {!! Form::hidden(config('user.params.type' ,'r'))->forceValue($guard)!!}
                    			{!! Captcha::render() !!}
                    			
                    			{!! Form::submit('Sign up')->class('btn btn-primary')!!}
                    			{!! Form::close() !!}
                    			<br />                  			
                    			<div class="col-xs-12">
                    				<div class="row center-text">
                    					<a class="btn btn-link" href="{{trans_url("/login?role=$guard")}}">Back to login</a><br>                    					
                    				</div>
                    			</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<!-- resources/views/auth/register.blade.php -->
<!-- 
{{$guard}}
<div class="container">
    <div class="row">
        <div class="col-md-6  col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    @include('public::notifications')
                    {!!Form::vertical_open()
                    ->id('contact')
                    ->action('register')
                    ->method('POST')
                    ->class('white-row')!!}

                    {!! Form::text('name')
                    -> label(trans('user::user.user.label.name'))
                    -> placeholder(trans('user::user.user.placeholder.name'))!!}

                    {!! Form::email('email')
                    -> label(trans('user::user.user.label.email'))
                    -> placeholder(trans('user::user.user.placeholder.email'))!!}

                    {!! Form::password('password')
                    -> label(trans('user::user.user.label.password'))
                    -> placeholder(trans('user::user.user.placeholder.password'))!!}

                    {!! Form::hidden(config('user.params.type' ,'r'))->forceValue($guard)!!}

                    {!! Captcha::render() !!}
                    <br />
                    {!! Form::submit(trans('user::user.signin'))->class('btn btn-primary')!!}
                    <br>
                    <br>

                    {!! Form::close() !!}
                    Already have an account ! <a href="{{trans_url('/login')}}"> Click to login </a>
                </div>
            </div>
        </div>
    </div>
</div>
 -->