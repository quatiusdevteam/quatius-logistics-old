@extends('public::curd.index')
@section('heading')

<i class="fa fa-calendar"></i> Logs <small> Laravel log viewer</small>
@stop
@section('title')
<div class="btn-group" style="float: right; margin: -10px 0px;">
  <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    {{$current_file?:"Select A File"}} <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    @foreach($folders as $folder)
      <li class="list-group-item">
        <?php
        \Rap2hpoutre\LaravelLogViewer\LaravelLogViewer::DirectoryTreeStructure( $storage_path, $structure );
        ?>

      </li>
    @endforeach
    @foreach($files as $file)
      <li><a href="?l={{ \Illuminate\Support\Facades\Crypt::encrypt($file) }}"
          class="list-group-item @if ($current_file == $file) llv-active @endif">
        {{$file}}
      </a></li>
    @endforeach
  </ul>
</div>

@stop
@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{!! URL::to('/') !!}"><i class="fa fa-dashboard"></i> {!! trans('cms.home') !!} </a></li>
    <li class="active">Laravel log viewer</li>
</ol>
@stop
@section('entry')

@stop
@section('tools')

<input type="checkbox" class="custom-control-input" id="darkSwitch" onchange="resetTheme();">
<label class="custom-control-label" for="darkSwitch" >Dark Mode</label>


    @if($current_file)
      <a class="btn btn-sm" href="?dl={{ \Illuminate\Support\Facades\Crypt::encrypt($current_file) }}{{ ($current_folder) ? '&f=' . \Illuminate\Support\Facades\Crypt::encrypt($current_folder) : '' }}">
        <span class="fa fa-download"></span> Download file
      </a>
      <a class="btn btn-sm" id="clean-log" href="?clean={{ \Illuminate\Support\Facades\Crypt::encrypt($current_file) }}{{ ($current_folder) ? '&f=' . \Illuminate\Support\Facades\Crypt::encrypt($current_folder) : '' }}">
        <span class="fa fa-square-o"></span> Clean file
      </a>
      <a class="btn btn-sm" id="delete-log" href="?del={{ \Illuminate\Support\Facades\Crypt::encrypt($current_file) }}{{ ($current_folder) ? '&f=' . \Illuminate\Support\Facades\Crypt::encrypt($current_folder) : '' }}">
        <span class="fa fa-trash-o"></span> Delete file
      </a>
      @if(count($files) > 1)
        <a class="btn btn-sm" id="delete-all-log" href="?delall=true{{ ($current_folder) ? '&f=' . \Illuminate\Support\Facades\Crypt::encrypt($current_folder) : '' }}">
          <span class="fa fa-trash"></span> Delete all files
        </a>
      @endif
    @endif
@stop
@section('content')
<div class="row log-viewer">
    <div class="col-xs-12 table-container">
      @if ($logs === null)
        <div>
          Log file >50M, please download it.
        </div>
      @else
        <table id="table-log" class="table table-striped" data-ordering-index="{{ $standardFormat ? 2 : 0 }}">
          <thead>
          <tr>
            @if ($standardFormat)
              <th>Level</th>
              <th>Context</th>
              <th>Date</th>
            @else
              <th>Line number</th>
            @endif
            <th>Content</th>
          </tr>
          </thead>
          <tbody>

          @foreach($logs as $key => $log)
            <tr data-display="stack{{{$key}}}">
              @if ($standardFormat)
                <td class="nowrap text-{{{$log['level_class']}}}">
                  <span class="fa fa-{{{$log['level_img']}}}" aria-hidden="true"></span>&nbsp;&nbsp;{{$log['level']}}
                </td>
                <td class="text">{{$log['context']}}</td>
              @endif
              <td class="date">{{{$log['date']}}}</td>
              <td class="text">
                @if ($log['stack'])
                  <button type="button"
                          class="float-right expand btn btn-outline-dark btn-sm mb-2 ml-2"
                          data-display="stack{{{$key}}}">
                    <span class="fa fa-search"></span>
                  </button>
                @endif
                {{{$log['text']}}}
                @if (isset($log['in_file']))
                  <br/>{{{$log['in_file']}}}
                @endif
                @if ($log['stack'])
                  <div class="stack" id="stack{{{$key}}}"
                       style="display: none; white-space: pre-wrap;">{{{ trim($log['stack']) }}}
                  </div>
                @endif
              </td>
            </tr>
          @endforeach

          </tbody>
        </table>
      @endif
    </div>
  </div>

@stop
@section('script')
<script>
    function initTheme() {
      const darkThemeSelected =
        localStorage.getItem('darkSwitch') !== null &&
        localStorage.getItem('darkSwitch') === 'dark';
      darkSwitch.checked = darkThemeSelected;
      darkThemeSelected ? $('.box-body').attr('data-theme', 'dark') :
        $('.box-body').attr('data-theme', '');
    }

    function resetTheme() {
      if (darkSwitch.checked) {
        $('.box-body').attr('data-theme', 'dark');
        localStorage.setItem('darkSwitch', 'dark');
      } else {
        $('.box-body').attr('data-theme', '');
        localStorage.removeItem('darkSwitch');
      }
    }
    // dark mode by https://github.com/coliff/dark-mode-switch
  const darkSwitch = document.getElementById('darkSwitch');

// this is here so we can get the body dark mode before the page displays
// otherwise the page will be white for a second... 
initTheme();

window.addEventListener('load', () => {
  if (darkSwitch) {
    initTheme();
    // darkSwitch.addEventListener('change', () => {
    //   resetTheme();
    // });
  }
});

// end darkmode js
      
$(document).ready(function () {
  $('.table-container tr').on('click', function () {
    $('#' + $(this).data('display')).toggle();
  });
  $('#table-log').DataTable({
    "order": [$('#table-log').data('orderingIndex'), 'desc'],
    "stateSave": true,
    "stateSaveCallback": function (settings, data) {
      window.localStorage.setItem("datatable", JSON.stringify(data));
    },
    "stateLoadCallback": function (settings) {
      var data = JSON.parse(window.localStorage.getItem("datatable"));
      if (data) data.start = 0;
      return data;
    }
  });
  $('#delete-log, #clean-log, #delete-all-log').click(function () {
    return confirm('Are you sure?');
  });
});

  </script>
@stop

@section('style')
<style>
  
    .date {
      min-width: 75px;
    }

    .text {
      word-break: break-all;
    }

    .list-group-item {
      word-break: break-word;
    }

    .folder {
      padding-top: 15px;
    }

    .nowrap {
      white-space: nowrap;
    }

    /**
    * DARK MODE CSS
    */

    [data-theme="dark"] {
      background-color: #151515;
      color: #cccccc;
    }

    [data-theme="dark"] .table td, [data-theme="dark"] .table th,[data-theme="dark"] .table thead th {
      border-color:#616161;
    }

    [data-theme="dark"] .page-item.disabled .page-link {
      color: #8a8a8a;
      background-color: #151515;
      border-color: #5a5a5a;
    }

    [data-theme="dark"] .page-link {
      background-color: #151515;
      border-color: #5a5a5a;
    }

    [data-theme="dark"] .page-item.active .page-link {
      color: #fff;
      background-color: #0568d2;
      border-color: #007bff;
    }

    [data-theme="dark"] .page-link:hover {
      color: #ffffff;
      background-color: #0051a9;
      border-color: #0568d2;
    }

    [data-theme="dark"] .form-control {
      border: 1px solid #464646;
      background-color: #151515;
      color: #bfbfbf;
    }

    [data-theme="dark"] .form-control:focus {
      color: #bfbfbf;
      background-color: #212121;
      border-color: #4a4a4a;
  }
  [data-theme="dark"] .table-striped>tbody>tr:nth-of-type(odd){
    background-color: #333;
  }
  .log-viewer button.btn{
    opacity: 0.3;
  }
  </style>
@stop
