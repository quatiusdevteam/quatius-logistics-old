<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
	<!--[if mso]>	
	<style type="text/css">
		body, table, td {font-family: Arial, Helvetica, sans-serif !important;}
	</style>
	<![endif]-->
    </head>
    
    <body style=" background: #090d18; margin: 0; padding: 1.5% 10%;">
		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background: #090d18; -webkit-font-smoothing:antialiased; -webkit-text-size-adjust:none; padding: 20px 0;">
			<tr>
				<td width="50%"><a href="{{url('/')}}"><img src="{{url('/')}}/images/quatius-logo.png" style="max-width: 100%; height: auto;"></a></td>
				<td width="50%" style="color: #fff; font-size: 1.5em; text-align: right; font-style: italic;">Hotline: 1300 765 663</td>
			</tr>
			<tr>
				<td colspan="2">
					<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background: #fff; color: #777; font-size: 1em; line-height: 1.35em; margin: 10px 0; padding: 20px;">
					<?php
				$formDatas = get_defined_vars()['__data'];
				foreach($formDatas as $field=>$value):?>
				<?php if(!is_scalar($value)) continue;?>
					<tr><th><?php echo ucfirst(str_replace("_"," ",$field)); ?></th><td><?php echo $value; ?></td></tr>
				<?php endforeach;?>	
					
				</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: center; color: #888; font-size: .85em; line-height: 1.35em;">
					<img src="{{url('/')}}/images/quatius-logo-mini.png" style="max-width: 100%; height: auto;"><br>
					<br>
					Quatius Logistics<br>					
					1-11 Remington Drive, Dandenong South, VIC, 3175<br>
					Tel: (61)3 8787 8459<br>
					Fax: (61)3 8782 0662
				</td>
			</tr>
		</table>
    </body>
</html>