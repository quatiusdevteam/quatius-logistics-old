<?php

return [

    'options'  => [

    ],

    'placeholder' => [

       'id'         => 'Enter Id',
'from'              => 'Enter From',
'to'                => 'Enter To',
'subject'           => 'Enter Subject',
'message'           => 'Enter Message',
'read'              => 'Enter Read',
'type'              => 'Enter Type',
'deleted_at'        => 'Enter Deleted at',
'created_at'        => 'Enter Created at',
'updated_at'        => 'Enter Updated at',

    ],

    'label' => [

        'id'        => 'Id',
'from'              => 'From',
'to'                => 'To',
'subject'           => 'Subject',
'message'           => 'Message',
'read'              => 'Read',
'type'              => 'Type',
'deleted_at'        => 'Deleted at',
'created_at'        => 'Created at',
'updated_at'        => 'Updated at',
    ],

    'name'            => 'Message',
    'names'           => 'Messages',

];
