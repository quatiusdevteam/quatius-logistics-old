<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Label language files for All Module
    |--------------------------------------------------------------------------
    |
    */

    'name'          => 'Quatius Logistics',
    'name.html'     => '<b>Quatius</b> Logistics',
    'name.short'    => '<b>Q</b>L',
    'admin.panel'   => 'Admin Panel',
    'dashboard'     => 'Dashboard',
    'all.rights'    => "<strong>Copyright © 2017 <a href='http://quatiuslogistics.com' target='_blank'>Quatius Logistics</a>.</strong> All rights reserved.",
    'version'       => '<b>Version</b> 1.0',
    'add'           => 'Add',
    'back'          => 'Back',
    'cancel'        => 'Cancel',
    'close'         => 'Close',
    'copy'          => 'Copy',
    'create'        => 'Create',
    'delete'        => 'Delete',
    'details'       => 'Details',
    'edit'          => 'Edit',
    'go'            => 'Go',
    'help'          => 'Help',
    'home'          => 'Home',
    'logs'          => 'Logs',
    'manage'        => 'Manage',
    'more'          => 'More',
    'new'           => 'New',
    'no'            => 'No',
    'opt'           => 'Options',
    'option'        => 'Option',
    'options'       => 'Options',
    'order'         => 'Order',
    'request'       => 'Request',
    'reset'         => 'Reset',
    'save'          => 'Save',
    'search'        => 'Search',
    'settings'      => 'Settings',
    'show'          => 'Show',
    'status'        => 'Status',
    'update'        => 'Update',
    'view'          => 'View',
    'yes'           => 'Yes',

    'flash.success' => 'Success',
    'flash.error'   => 'Error',
    'flash.warning' => 'Warning',
    'flash.info'    => 'Info',

];
