<?php

use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
         * Table: users
         */
        Schema::create('users', function ($table) {
            $table->increments('id')->unsigned();
            $table->integer('reporting_to')->nullable()->default(0);
            $table->string('name', 100)->nullable();
            $table->string('username', 150)->nullable()->unique();
            $table->string('email', 190)->unique();
            $table->string('password', 100);
            $table->text('params')->nullable();
            $table->enum('status', ['New', 'Active', 'Suspended'])->default('New');
            $table->string('api_token', 60)->unique();
            $table->string('remember_token', 255)->nullable();
            $table->enum('sex', ['other', 'male', 'female'])->nullable();
            
            $table->string('first_name', 100)->nullable();
            $table->string('middle_name', 100)->nullable();
            $table->string('last_name', 100)->nullable();
            $table->string('designation', 50)->nullable();
            $table->string('phone_1', 100)->nullable();
            $table->string('phone_2', 100)->nullable();
            $table->string('address_1', 255)->nullable();
            $table->string('address_2', 255)->nullable();
            $table->string('city', 100)->nullable();
            $table->string('postcode', 100)->nullable();
            $table->string('state', 100)->nullable();
            $table->string('country', 100)->nullable();
            $table->string('photo', 500)->nullable();
            $table->longText('permissions')->nullable();
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
