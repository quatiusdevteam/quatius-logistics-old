<?php

use Illuminate\Database\Seeder;

class carrier_table_seeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('carriers')->insert(
            [
                'name'  => 'Quatius Logistics'
            ]
        );
    }
}
