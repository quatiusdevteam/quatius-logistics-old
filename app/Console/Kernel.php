<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // cpanel
        // /usr/local/bin/php /home/hosting-username/laravel-folder/artisan schedule:run >> /dev/null 2>&1
//
        $schedule->command('logistic:pull-tracking')
                ->appendOutputTo(storage_path('logs/schedule.log'))
                ->hourly();

        $schedule->command('queue:work --once')
                ->appendOutputTo(storage_path('logs/queue_jobs.log'))
                ->withoutOverlapping();

        $schedule->command('queue:restart')
            ->everyFiveMinutes(); // update code changes

        // $schedule->command('queue:work --daemon')
        //     ->appendOutputTo(storage_path('logs/schedule.log'))
        //     ->everyMinute()
        //     ->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
