<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        View::addLocation(public_path(config('theme.themeDir').'/'.config('theme.map.admin.theme').'/views'));
    	View::addLocation(public_path(config('theme.themeDir').'/'.config('theme.map.public.theme').'/views'));
    	     
    	$this->loadViewsFrom(public_path(config('theme.themeDir').'/'.config('theme.map.admin.theme').'/views'), 'admin');
    	$this->loadViewsFrom(public_path(config('theme.themeDir').'/'.config('theme.map.public.theme').'/views'), 'public');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
