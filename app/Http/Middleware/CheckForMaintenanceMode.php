<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode as Middleware;

class CheckForMaintenanceMode extends Middleware
{
    /**
     * The URIs that should be reachable while maintenance mode is enabled.
     *
     * @var array
     */
    protected $except = [
        //SQLSTATE[23000]: Integrity constraint violation: 1048 Column 'editable_by' cannot be null (SQL: insert into `components` (`published`, `name`, `detachable_by`, `editable_by`, `type`, `data`, `updated_at`, `created_at`) values (1, Text Block, component.allow-detach, ?, custom-html, {\"compiler\":\"blade\",\"content\":\"<h1>Your content here..<\\/h1>\"}, 2022-05-05 23:22:52, 2022-05-05 23:22:52))
    ];
}
