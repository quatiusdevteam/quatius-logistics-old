<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Litepie\User\Traits\Auth\Common;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{
    /*
     |--------------------------------------------------------------------------
     | Password Reset Controller
     |--------------------------------------------------------------------------
     |
     | This controller is responsible for handling password reset requests
     | and uses a simple trait to include this behavior. You're free to
     | explore this trait and override any methods you wish to tweak.
     |
     */
   
    use SendsPasswordResetEmails, Common;
    
    /**
     * The password broker that should be used.
     *
     * @var string
     */
    protected $broker = 'user';
    
    /**
     * The password broker that should be used.
     *
     * @var string
     */
    protected $guard = 'user.web';
    
    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $guard = $request->get(config('user.params.type'), null);
        $this->setGuard($guard);
        $this->setRedirectTo();
        $this->setPasswordBroker();
        
        $this->middleware('guest');
        $this->setTheme();
        
        //if ($guard == 'admin.web')
            $this->setupTheme(config('theme.map.admin.theme'), config('theme.map.admin.blank'));
        //else
            //$this->setupTheme(config('theme.map.public.theme'), config('theme.map.public.layout'));
    }
    
    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    function showLinkRequestForm(Request $request)
    {
        return $this->theme->of($this->getView('password'), ['guard'=>$this->getGuard()])->render();
    }
    
    /**
     * Display the form to request a password reset link.
     * @return \Illuminate\Http\Response
     */
    function getEmail(Request $request)
    {
        return $this->theme->of($this->getView('password'), ['guard'=>$this->getGuard()])->render();
    }
    
    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
        //$this->validate($request, ['email' => 'required|email']);
        
        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = $this->broker()->sendResetLink(
            $request->only('email')
            );
        
        return $response == Password::RESET_LINK_SENT
        ? $this->sendResetLinkResponse($request, $response)
        : $this->sendResetLinkFailedResponse($request, $response);
    }
}
