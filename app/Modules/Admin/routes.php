<?php
Route::group(array('prefix'=>'selfmanage/admin','module' => 'Admin', 'namespace' => 'App\Modules\Admin\Controllers'), function() {

    Route::resource('/customer', 'CustomerController');
    Route::resource('/status', 'BookingStatusController');

	Route::resource('/bookings', 'AdminBookingController');
	Route::resource('/search', 'AdminBookingController@search');

    Route::post('/bookings/setaction', 'AdminBookingController@setAction');
	Route::get('/search_result/{tracking_number}', 'AdminBookingController@search_result');
	
});