<?php namespace App\Modules\Admin\Controllers;

use App\Http\Controllers\Controller;

use App\Modules\Myaccount\Models\ManageAddress;
use Illuminate\Http\Request;

use Form;
use Validator;
use App\Modules\Myaccount\Models\Customer;
use App\Modules\Myaccount\Models\User;
use GuzzleHttp\json_encode;

class CustomerController extends Controller {
	
	protected $default_customer_role = 5;
	
	public function __construct(){
		$this->middleware('web');
		$this->middleware('auth:web');
		$this->setupTheme(config('theme.map.registered.theme'), config('theme.map.registered.layout'));
	}
	
	/**
	 * Display a list of user.
	 *
	 * @return Response
	 */
	public function index(Request $request){
	
		if ($request->wantsJson()) {
			$query = User::whereDesignation('User')->whereNull('deleted_at')->get();			 
			return response()->json(['data' => $query], 200);
		}
		$this->theme->prependTitle(trans('user::user.user.names') . ' :: ');
		return $this->theme->of('Admin::user.index')->render();
	}
	
	
	/**
	 * Display user.
	 *
	 * @param Request $request
	 * @param int     $id
	 *
	 * @return Response
	 */
	public function show(Request $request, $user_id)
	{
		$user = User::findOrNew($user_id);
		if (!$user->exists) {
			return response()->view('Admin::user.new', compact('user'));
		}
		
		Form::populate($user);
		return response()->view('Admin::user.show', compact('user'));
		
		
		
		
	}
	
	/**
	 * Show the form for creating a new user.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		$user = new User();	
		Form::populate($user);
		
		return response()->view('Admin::user.create', compact('user'));
	
	}
	
	
	/**
	 * Create new user.
	 *
	 * @param Request $request
	 *$messages = $validator->messages();
	 * @return Response
	 */
	public function store(Request $request)
	{
		try {
			$validator = Validator::make($request->all(), [
					'sex'			   	   => 'required|not_in:0',
					'first_name'           => 'required|max:255',
					'last_name'            => 'required|max:255',
					'email'                => 'required|email|unique:users|max:255',
					'phone_1' 			   => 'min:8|max:11',
					'address_1'			   => 'required|max:255',
					'city'				   => 'required|max:255',
					'state'				   => 'required|not_in:0',
					'postcode' 			   => 'required|max:4|min:4',					
					'country'			   => 'required|not_in:0',
					//'pronto_user_id'	   => 'required|unique:users',
					'password'             => 'required|confirmed|min:6',
					]);
			if ($validator->fails()) {
				return response()->json($validator->messages(), 422);
			}
			
			$attributes            = $request->all();
			//$attributes['user_id'] = user_id('web');
			$attributes['designation'] 	= 'User';
			$attributes['password'] = bcrypt($attributes['password']);
			$attributes['status'] = 'Active';
			$attributes['api_token']	= $this->makeRandomTokenKey();
			$attributes['name'] = $attributes['first_name'] . " " . $attributes['last_name'];

			$user = User::create($attributes);
			if($user) {
			    $attributes['user_id']  = $user->id;
			    $attributes['phone']    = $attributes['phone_1'];
			    ManageAddress::create($attributes);

                return response()->json([
                    'message' => trans('messages.success.created', ['Module' => trans('Myaccount::selfmanage.label.address')]),
                    'code' => 204,
                    'redirect' => trans_url('/selfmanage/admin/customer/' . $user->id),
                ], 201);
            }
		} catch (Exception $e) {
			return response()->json([
					'message' => $e->getMessage(),
					'code'    => 400,
					], 400);
		}
	
	}
	/**
	 * Show user for editing.
	 *
	 * @param Request $request
	 * @param int     $id
	 *
	 * @return Response
	 */
	public function edit(Request $request, $user_id)
	{
		$user = User::findOrNew($user_id);
		Form::populate($user);		
		return response()->view('Admin::user.edit', compact('user'));
	}
	
	/**
	 * Update the address.
	 *
	 * @param Request $request
	 * @param int     $id
	 *
	 * @return Response
	 */
	public function update(Request $request, $user_id)
	{
		try {
	
			$attributes = $request->all();
							
			$validator = Validator::make($request->all(), [
					'sex'			   	   => 'required|not_in:0',
					'first_name'           => 'required|max:255',
					'last_name'            => 'required|max:255',
					'email'                => 'required|email|unique:users,email,'.$user_id . ',id',
					'phone_1' 			   => 'min:8|max:11',
					'address_1'			   => 'required|max:255',
					'city'				   => 'required|max:255',
					'state'				   => 'required|not_in:0',
					'postcode' 			   => 'required|max:4|min:4',
					'country'			   => 'required|not_in:0',
					//'pronto_user_id'	   => 'required|unique:users,pronto_user_id,'.$user_id . ',id',
					'password'             => 'sometimes|confirmed|min:6',
					]);
			if ($validator->fails()) {
				//return response()->json($validator->messages(), 422);
                return reseponse()->json(json_encode($attributes), 422);
			}

			if(empty($attributes['password'])){
				unset($attributes['password']);
				unset($attributes['password_confirmation']);
			}else{
				$attributes['password'] = bcrypt($attributes['password']);
			}
			$attributes['name'] = $attributes['first_name'] . " " . $attributes['last_name'];
			$user = User::findOrNew($user_id);
			$user->update($attributes);
			
			return response()->json([
					'message'  => trans('messages.success.updated', ['Module' => trans('Myaccount::selfmanage.label.address')]),
					'code'     => 204,
					'redirect' => trans_url('/selfmanage/admin/customer/' . $user->id),
					], 201);
					
	
		} catch (Exception $e) {
	
			return response()->json([
					'message'  => $e->getMessage(),
					'code'     => 400,
					'redirect' => trans_url('/selfmanage/admin/customer/' . $user->id),
					], 400);
	
		}
	
	}
	
	/**
	 * Remove the address.
	 *
	 * @param int $id
	 *
	 * @return Response
	 */
	public function destroy(Request $request, $user_id)
	{
		$user = User::find($user_id);
		try {
				
			$t = $user->delete();
	
			return response()->json([
					'message'  => trans('messages.success.deleted', ['Module' => trans('Myaccount::selfmanage.label.address')]),
					'code'     => 202,
					'redirect' => trans_url('/selfmanage/admin/customer/0'),
					], 202);
	
		} catch (Exception $e) {
	
			return response()->json([
					'message'  => $e->getMessage(),
					'code'     => 400,
					'redirect' => trans_url('/selfmanage/admin/customer/' . $user->id),
					], 400);
		}
	
	}
	
	public function makeRandomTokenKey(){
		return sha1(time());
	}
}