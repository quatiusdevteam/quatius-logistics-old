<?php namespace App\Modules\Admin\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Myaccount\Models\BookingStatus;

use Form;
class BookingStatusController extends Controller {
	
	public function __construct(){
		$this->middleware('web');
		$this->middleware('auth:web');
	
		$this->setupTheme(config('theme.map.registered.theme'), config('theme.map.registered.layout'));
	}
	
	public function index(Request $request){
	
		if ($request->wantsJson()) {			
			$query = BookingStatus::get();
			return response()->json(['data' => $query], 200);
		}
		return $this->theme->of('Admin::status.index')->render();
	}
	
	/**
	 * Display Status.
	 *
	 * @param Request $request
	 * @param int     $id
	 *
	 * @return Response
	 */
	public function show(Request $request, $status_id)
	{
			
		$status = BookingStatus::findOrNew($status_id);
		if (!$status->exists) {
			return response()->view('Admin::status.new', compact('status'));
		}
	
		Form::populate($status);
		return response()->view('Admin::status.show', compact('status'));
		
	}
	
	/**
	 * Show the form for creating a new status.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function create(Request $request){
		$status = new BookingStatus();
	
		Form::populate($status);
			
		return response()->view('Admin::status.create', compact('status'));
	
	}
	
	/**
	 * Create new status.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		try {
			$attributes           = $request->all();			
			$status               = BookingStatus::create($attributes);
				
			return response()->json([
					'message'  => trans('messages.success.created', ['Module' => trans('Myaccount::selfmanage.booking_status')]),
					'code'     => 204,
					'redirect' => trans_url('/selfmanage/admin/status/' . $status->id),
					], 201);
	
		} catch (Exception $e) {
			return response()->json([
					'message' => $e->getMessage(),
					'code'    => 400,
					], 400);
		}
	
	}
	
	/**
	 * Show booking status for editing.
	 *
	 * @param Request $request
	 * @param int     $id
	 *
	 * @return Response
	 */
	public function edit(Request $request, $status_id)
	{
		$status = BookingStatus::findOrNew($status_id);
		Form::populate($status);
	
		return response()->view('Admin::status.edit', compact('status'));
	}
	
	/**
	 * Update the address.
	 *
	 * @param Request $request
	 * @param int     $id
	 *
	 * @return Response
	 */
	public function update(Request $request, $status_id)
	{
		try {
	
			$attributes = $request->all();
				
			$status = BookingStatus::findOrNew($status_id);
			$status->update($attributes);
				
			return response()->json([
					'message'  => trans('messages.success.updated', ['Module' => trans('Myaccount::selfmanage.booking_status')]),
					'code'     => 204,
					'redirect' => trans_url('/selfmanage/admin/status/' . $status->id),
					], 201);
	
		} catch (Exception $e) {
	
			return response()->json([
					'message'  => $e->getMessage(),
					'code'     => 400,
					'redirect' => trans_url('/selfmanage/admin/status/' . $status->id),
					], 400);
	
		}
	
	}
	
	/**
	 * Remove the booking status.
	 *
	 * @param int $id
	 *
	 * @return Response
	 */
	public function destroy(Request $request, $status_id)
	{
		$status = BookingStatus::find($status_id);
		try {
				
			$t = $status->delete();
	
			return response()->json([
					'message'  => trans('messages.success.deleted', ['Module' => trans('Myaccount::selfmanage.booking_status')]),
					'code'     => 202,
					'redirect' => trans_url('/selfmanage/admin/status/0'),
					], 202);
	
		} catch (Exception $e) {
	
			return response()->json([
					'message'  => $e->getMessage(),
					'code'     => 400,
					'redirect' => trans_url('/selfmanage/admin/status/' . $status->id),
					], 400);
		}
	
	}
}