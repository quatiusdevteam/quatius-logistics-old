<?php namespace App\Modules\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Myaccount\Controllers\ManagingBookingController;
use Illuminate\Http\Request;
use Form;
use Auth;
use Validator;
use App\Modules\Myaccount\Models\Booking;
use GuzzleHttp\json_encode;
use App\Modules\Myaccount\Models\BookingStatus;
use App\User;


class AdminBookingController extends Controller{

	public function __construct(){
		$this->middleware('web');
		$this->middleware('auth:web');

		$this->setupTheme(config('theme.map.registered.theme'), config('theme.map.registered.layout'));
	}

	protected function validator(array $data){
		return Validator::make($data, [
				'first_name' => 'required|max:255',
				'last_name' => 'required|max:255',
				'name' => 'required|max:255',
				'phone_1' => 'min:8|max:10',
				'email' => 'required|email|max:255',
				'address_1' => 'required|max:255',
				'city' => 'required|max:255',
				'postcode' => 'required|max:4|min:4',
				]);
	}

	public function index(Request $request){

		if ($request->wantsJson()) {
			$user_id = Auth::user()->id;
			$query = Booking::withStatus()->get();
			return response()->json(['data' => $query], 200);
		}
		return $this->theme->of('Admin::booking.index')->render();
	}
	
	/**
	 * Display address.
	 *
	 * @param Request $request
	 * @param int     $id
	 *
	 * @return Response
	 */
	public function show(Request $request, $booking_id)
	{

		$booking = Booking::whereId($booking_id)->withStatus()->first();
		$booking_status = BookingStatus::get();
		if (is_null($booking)) {
			return response()->view('Admin::booking.new', compact('booking', 'sender_address', 'receiver_address'));
		}
		$can_edit = false;
		if($booking->allow_staff_change == 1){
			$can_edit = true;
		}
	
		$sender_address = $booking->addresses()->whereAddressType('source')->first();
		$receiver_address = $booking->addresses()->whereAddressType('destination')->first();
 		return response()->view('Admin::booking.show', compact('can_edit', 'booking_status', 'booking', 'sender_address', 'receiver_address'));
	
	}
	
	/**
	 * Display booking form.
	 *
	 * @param Request $request
	 * @param int     $id
	 *
	 * @return Response
	 */
	
	public function create(Request $request)
	{
		$customers = User::whereDesignation('User')->get();
		
		$sender_address = [];// = BookingAddress::whereAddress_type('source')->first();
		$receiver_address =[];// = BookingAddress::whereAddress_type('destination')->first();
		$booking = [];// = Booking::first();
		
		return response()->view('Admin::booking.create', compact('customers', 'booking', 'sender_address', 'receiver_address'));
	
	}
	
	/**
	 * Create new address.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		try {

            $customer_id = $request['selected_customer'];
            $booking_controller = new ManagingBookingController();
            $tracking_number = ManagingBookingController::generateTrackingNumber();
            $data = $booking_controller->prepareData($request->all());
            if($data) {
                $history['user_id'] = Auth::user()->id;
                $history['tracking_number'] = $tracking_number;
                $history['title'] = trans('Myaccount::selfmanage.title.new_booking');
                $history['description'] = ' <p>Request new booking:</p>
                                                <p>Item detail:' . json_encode($data->item) . '</p>
                                                <p>Sender detail: ' . json_encode($data->sender) . '</p>
                                                <p>Receiver detail: ' . json_encode($data->receiver) . '</p>';
                $data->item['user_id'] = $customer_id;
                $data->item['tracking_number'] = $tracking_number;
                $data->item['status_id'] = 1;

                $thisBooking = Booking::create($data->item);
                $thisBooking->addresses()->create($data->sender);
                $thisBooking->addresses()->create($data->receiver);
                $thisBooking->activitylogs()->create($history);

                return response()->json([
                    'message'  => trans('messages.success.create', ['Module' => trans('Myaccount::selfmanage.label.booking')]) ,
                    'code'     => 204,
                    'redirect' => trans_url('/selfmanage/admin/bookings/' . $thisBooking->id),
                ], 201);
            }
			return response()->json([
					'message'  => 'Unable to create booking' ,
					'code'     => 400,

					], 400);
		} catch (Exception $e) {
			return response()->json([
					'message' => $e->getMessage(),
					'code'    => 400,
					], 400);
		}
	}	
	
	/**
	 * Show booking for editing.
	 *
	 * @param Request $request
	 * @param int     $id
	 *
	 * @return Response
	 */
	public function edit(Request $request, $booking_id)
	{

		$booking = Booking::whereId($booking_id)->withStatus()->first();
		$sender_address = [];
		$receiver_address = [];
	
		if(!is_null($booking)){
			$sender_address = $booking->addresses()->whereAddressType('source')->first();
			$receiver_address = $booking->addresses()->whereAddressType('destination')->first();
		}
		return response()->view('Admin::booking.edit', compact('booking', 'sender_address', 'receiver_address'));
	}
	
	/**
	 * Update the booking.
	 *
	 * @param Request $request
	 * @param int     $id
	 *
	 * @return Response
	 */
	public function update(Request $request, $booking_id)
	{

	    try {

			$booking_controller = new ManagingBookingController();
			$data = $booking_controller->prepareData($request->all());

			$thisBooking = Booking::find($booking_id);
			if($thisBooking) {
                $thisBooking->update($data->item);
                $source_address_id = $thisBooking->addresses()->whereAddressType('source')->pluck('id');
                $destination_address_id = $thisBooking->addresses()->whereAddressType('destination')->pluck('id');
                $thisBooking->addresses()->find($source_address_id)->update($data->sender);
                $thisBooking->addresses()->find($destination_address_id)->update($data->receiver);

                /* Prepare history data */
                $history['user_id'] = Auth::user()->id;
                $history['tracking_number'] = $thisBooking->tracking_number;
                $history['title'] = trans('Myaccount::selfmanage.title.update_booking');
                $history['description'] = ' <p>Update booking:</p>
                                            <p>Item detail:' . json_encode($data->item) . '</p>
                                            <p>Sender detail: ' . json_encode($data->sender) . '</p>
                                            <p>Reciever detail: ' . json_encode($data->receiver) . '</p>';

                $thisBooking->activitylogs()->create($history);

                return response()->json([
                    'message' => trans('messages.success.updated', ['Module' => trans('Myaccount::selfmanage.label.booking')]),
                    'code' => 204,
                    'redirect' => trans_url('/selfmanage/booking/' . $thisBooking->id),
                ], 201);
            }else{
                return response()->json([
                    'message' => 'Unable to edit booking. Booking Id not found',
                    'code'    => 400,
                ], 400);
            }
		} catch (Exception $e) {
			return response()->json([
					'message' => $e->getMessage(),
					'code'    => 400,
					], 400);
		}
	
	}
	
	/**
	 * Remove the address.
	 *
	 * @param int $id
	 *
	 * @return Response
	 */
	public function destroy(Request $request, $booking_id)
	{
		$thisBooking = Booking::find($booking_id);
		try {
			/* Prepare history data */
			$history['user_id']	= $thisBooking->user_id;
			$history['tracking_number'] = $thisBooking->tracking_number;
			$history['title'] = trans('Myaccount::selfmanage.title.delete_booking');
			$history['description'] = ' <p>Delete booking:</p>';
			$thisBooking->activitylogs()->create($history);
				
			$thisBooking->addresses()->delete();
			$thisBooking->delete();
	
			return response()->json([
					'message'  => trans('messages.success.deleted', ['Module' => trans('Myaccount::selfmanage.label.address')]),
					'code'     => 202,
					'redirect' => trans_url('/selfmanage/booking/0'),
					], 202);
	
		} catch (Exception $e) {
	
			return response()->json([
					'message'  => $e->getMessage(),
					'code'     => 400,
					'redirect' => trans_url('/selfmanage/booking/' . $thisBooking->id),
					], 400);
		}
	
	}
	
	public function search(Request $request){
		
		$tracking_number = $request->get('q');						
		return $this->theme->of('Admin::booking.index', compact('tracking_number'))->render();
	}
	public function search_result($tracking_number){
		
		$query = Booking::where('Tracking_Number', 'like', '%' . $tracking_number . '%')->withStatus()->get();
		
		return response()->json(['data' => $query], 200);
		
	}
	
	public function setAction(Request $request){
		$tracking_number = $request->get('tracking_number');
		$setStatus = $request->get('sel_status');
		$remark = $request->get('remark');
		$booking = Booking::whereTrackingNumber($tracking_number)->first();
		if(!is_null($booking)){
			$status = BookingStatus::find($setStatus);
			// generate log
			$history['user_id']	= $booking->user_id;
			$history['tracking_number'] = $booking->tracking_number;
			$history['title'] = $status->status;
			$history['description'] = $remark;
			$booking->activitylogs()->create($history);
			// update status
			
			$booking->status_id = $setStatus;
			$booking->save();
			return response()->json([
					'message'  => trans('messages.success.updated', ['Module' => trans('Myaccount::selfmanage.label.booking')]),
					'code'     => 204,
					'redirect' => trans_url('/selfmanage/admin/bookings/0'),
					], 201);
		}
		return response()->json([
				'message' => "Unable to perform the request.",
				'code'    => 400,
				], 400);
	}
}
