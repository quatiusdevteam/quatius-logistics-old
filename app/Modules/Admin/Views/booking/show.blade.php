<div class="box-header with-border">
    <h3 class="box-title"> {{ trans('cms.view') }}   {{ trans('Myaccount::selfmanage.label.booking') }}  [{{ $booking->tracking_number}}]  </h3>
       <div class="box-tools pull-right">
        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#setBookingStatus" data-tracking="{{ $booking->tracking_number}}" data-status="{{ $booking->status_id}}"><i class="fa fa-cog"></i> {{ trans('Myaccount::selfmanage.label.status') }}</button>
        
        @if($can_edit)
        <button type="button" class="btn btn-primary btn-sm" data-action="EDIT" data-load-to='#entry-user' data-href='{{ trans_url('/selfmanage/admin/bookings') }}/{{$booking->id}}/edit'><i class="fa fa-pencil-square"></i> {{ trans('cms.edit') }}</button>
        <button type="button" class="btn btn-danger btn-sm" data-action="DELETE" data-load-to='#entry-user' data-datatable='#main-list' data-href='{{ trans_url('/selfmanage/admin/bookings') }}/{{$booking->id}}' >
            <i class="fa fa-times-circle"></i> {{ trans('cms.delete') }}
        </button>
        @endif
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body" >
    	
        {!!Form::vertical_open()
        ->id('user-user-show')
        ->method('PUT')
        ->action(trans_url('selfmanage/admin/bookings/'. $booking->id))!!}
            <div class="tab-content">
                @include('Myaccount::booking.partials.entry')
            </div>
        {!! Form::close() !!}
    
</div>

<div class="modal fade" id="setBookingStatus" tabindex="-1" role="dialog" aria-labelledby="modelTitle">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form id="set-action" method="POST" action="{!! trans_url('selfmanage/admin/bookings/setaction') !!}">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modelTitle"></h4>
      </div>
      <div class="modal-body">
        
          <div class="form-group">          	
            <label for="recipient-name" class="control-label">Action:</label>
           	<div class="btn-group">
			  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			    <span id="selected_status" data-id="0">Select Status</span> <span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu">
			    <?php foreach($booking_status as $status):?>
			    	<li data-id="<?php echo $status->id;?>" onclick="setStatus(this);"><a href="#"><?php echo $status->status;?></a></li> 
			    <?php endforeach;?>
			  </ul>
			</div>
          </div>
          <div class="form-group">
            <label for="message-text" class="control-label">Remark:</label>
            <textarea class="form-control" id="remark" name="remark"></textarea>
          </div>
                
      </div>
      <div class="modal-footer">
      	<input type="hidden" id="sel_status" name="sel_status">
      	<input type="hidden" id="tracking_number" name="tracking_number">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" data-action='UPDATE' data-form='#set-action' data-dismiss="modal" data-datatable='#main-list'>Save</button>
      </div>
      </form>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){    
    $('#setBookingStatus').on('show.bs.modal', function (event) {
    	  var button = $(event.relatedTarget) // Button that triggered the modal
    	  var tracking = button.data('tracking'); 
    	  var cur_status_id = button.data('status');
    	  $("#tracking_number").val(tracking);
    	  $("#sel_status").val(cur_status_id);
    	  var modal = $(this)
    	  var cur_status_txt = $('.modal-body li[data-id="' + cur_status_id + '"]>a').html();
    	  modal.find('.modal-title').text("{{ trans('Myaccount::selfmanage.label.action') }}: " + tracking)
    	  modal.find('.modal-body #selected_status').html(cur_status_txt)
    	  modal.find('.modal-body #selected_status').attr("data-id", cur_status_id);
    	  
    	})
});
function setStatus(itm){
	var sel_status_id = $(itm).data("id")
	var sel_status_text = $(itm).find("a").html();
	$("#setBookingStatus #selected_status").attr("data-id", sel_status_id);
	$("#setBookingStatus #selected_status").html(sel_status_text);
	$("#sel_status").val(sel_status_id);
}

</script>
<div class="box-footer" >
    &nbsp;
</div>
