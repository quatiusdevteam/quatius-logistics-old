@extends('admin::curd.index')
@section('heading')
<i class="fa fa-location-arrow"></i> {!! trans('Myaccount::selfmanage.label.booking') !!} 
@stop
@section('title')
{!! trans('Myaccount::selfmanage.title.previous_booking') !!}
@stop
@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{!! URL::to('admin') !!}"><i class="fa fa-dashboard"></i> {!! trans('cms.home') !!} </a></li>
    <li class="active">{!! trans('Myaccount::selfmanage.new_booking')!!}</li>
</ol>
@stop
@section('entry')
<div class="box box-warning" id='entry-user'>
</div>
@stop

@section('content')
<table id="main-list" class="display table responsive nowrap">
    <thead>
        <th>{!! trans('Myaccount::selfmanage.label.tracking_number')!!}</th>
        <th>{!! trans('Myaccount::selfmanage.label.status')!!}</th>
        <th>{!! trans('Myaccount::selfmanage.label.item_type')!!}</th>
        <th>{!! trans('Myaccount::selfmanage.label.num_item')!!}</th>
        <th>{!! trans('Myaccount::selfmanage.label.remark')!!}</th>
        
    </thead>
</table>
@stop

<!-- MODAL DIALOG -->
<div class="modal fade" tabindex="-1" role="dialog" id="available_address">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        
		<table id="address_list" class="display responsive nowrap">
		    <thead>
		        <th>{!! trans('Myaccount::selfmanage.label.company_name')!!}</th>
		        <th>{!! trans('Myaccount::selfmanage.label.contact_name')!!}</th>
		        <th>{!! trans('Myaccount::selfmanage.label.phone')!!}</th>
		        <th>{!! trans('Myaccount::selfmanage.label.email')!!}</th>
		        <th>{!! trans('Myaccount::selfmanage.label.address')!!}</th>
                <th>{!! trans('Myaccount::selfmanage.label.last_updated')!!}</th>
		        
		    </thead>
		</table>
		
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <input type="hidden" id="dest" name="dest" />
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



@section('script')
<script type="text/javascript">
var oTable;
$(document).ready(function(){
	<?php if(isset($tracking_number)):?>
		loadPreviousBooking('search', '<?php echo $tracking_number;?>');
	<?php else:?>
    	$('#entry-user').load('{{URL::to('selfmanage/admin/bookings/0')}}');
    	loadPreviousBooking('load');
    <?php endif;?>
    loadSavedAddress();
    
});

function loadSavedAddress(){
	$('#address_list').DataTable( {
        "ajax": '{{ URL::to('/selfmanage/manage-address/available-address') }}',
        "columns": [
        { "data": "company_name" },
        { "data": "first_name", "className": "hidden-xs" },
        { "data": "phone", "className": "hidden-xs" },
        { "data": "email", "className": "hidden-xs" },
        { "data": "address_1" },
        ],
        "columnDefs": [
						{
							"render": function ( data, type, row ) {
									return data + ' ' + row.last_name;
						 	},"targets": 1
						},
						{
							"render": function ( data, type, row ) {
									var address_1 = (data != null)?data : '';
									var address_2 = (row.address_2 != null)?row.address_2 : '';
									var city = (row.city != null)?row.city : '';
									var state = (row.state != null)?row.state : '';
									var country = (row.country != null)?row.country : '';
									var postcode = (row.postcode != null)?row.postcode : '';
									return address_1 + ' ' + address_2 + ' ' + city + ' ' + state + ' ' + country + ' ' + postcode;
						 	},"targets": 4
						}
                       ],
        "userLength": 50
    });
    
	$('#address_list').on( 'click', 'tr', function () {
        
        $(this).toggleClass("selected").siblings(".selected").removeClass("selected");
        var address = $('#address_list').DataTable().row( this ).data();
       
       var dest = $("#dest").val();
       var destination = $("#" + dest);
       destination.find("input.company_name").val(address.company_name);
       destination.find("input.first_name").val(address.first_name);
       destination.find("input.last_name").val(address.last_name);
       destination.find("input.phone").val(address.phone);
       destination.find("input.email").val(address.email);
       destination.find("input.address_1").val(address.address_1);
       destination.find("input.address_2").val(address.address_2);
       destination.find("input.city").val(address.city);
       destination.find("select.state").val(address.state);
       destination.find("select.country").val(address.country);
       destination.find("input.postcode").val(address.postcode);
       $('#available_address').modal('hide');
    });
}


function loadPreviousBooking(mode, q){
	if(mode == "search")
		var url = '{{ URL::to('/selfmanage/admin/search_result') }}/' + q;
	else
		var url = '{{ URL::to('/selfmanage/admin/bookings') }}';
	console.log(url);		 
	$('#main-list').DataTable( {
        "ajax": url,
        "columns": [
			        { "data": "tracking_number" },
			        { "data": "status"},
			        { "data": "type", "className": "hidden-xs" },
			        { "data": "number_of_item", "className": "hidden-xs" },
			        { "data": "remark", "className": "hidden-xs" }
			       ],
        
        "userLength": 50
    });
    $('#main-list tbody').on( 'click', 'tr', function () {
        
        $(this).toggleClass("selected").siblings(".selected").removeClass("selected");
        var d = $('#main-list').DataTable().row( this ).data();
        $('#entry-user').load('{{URL::to('selfmanage/admin/bookings')}}' + '/' + d.id);
        
    });
}
</script>
@stop
@section('style')
@stop
