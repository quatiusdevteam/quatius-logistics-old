<div class="box-header with-border">
    <h3 class="box-title"> {{ trans('cms.edit') }}  {{ trans('Myaccount::selfmanage.label.booking') }} [{!!$booking->tracking_number!!}] </h3>
    <div class="box-tools pull-right">
        <button type="button" class="btn btn-primary btn-sm" data-action='UPDATE' data-form='#edit-user'  data-load-to='#entry-user' data-datatable='#main-list'><i class="fa fa-floppy-o"></i> {{ trans('cms.save') }}</button>

        <span id='how-return'></span>
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body" >
    
        {!!Form::vertical_open()
        ->id('edit-user')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->action(URL::to('selfmanage/admin/bookings/'. $booking->id))!!}
        	{!! Form::hidden('return')
	        -> value('')
	       	!!}
        <div class="tab-content">
            @include('Myaccount::booking.partials.entry')
        </div>
        {!!Form::close()!!}
    
</div>
<script>

</script>
<div class="box-footer" >
    &nbsp;
</div>
