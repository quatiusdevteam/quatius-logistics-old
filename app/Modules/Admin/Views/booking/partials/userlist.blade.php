
<div class="box box-sucess" id="user-list">
    	<div class="row">
	        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	        	<div class="form-group required">
	        		<label for="sender_company_name" class="control-label">{!!trans('Myaccount::selfmanage.label.customer')!!} <sup>*</sup></label>
	        		<select name="selected_customer" id="selected_customer" class="form-control">
	        		<?php foreach($customers as $customer):?>
	        		<option value="<?php echo $customer->id;?>"><?php echo $customer->name;?> [<?php echo $customer->email;?>]</option>
	        		
	        		<?php endforeach;?>
	        		</select>
	        	</div>
	        </div>       
	    </div>
</div>