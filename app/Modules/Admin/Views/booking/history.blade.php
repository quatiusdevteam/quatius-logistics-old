@extends('admin::curd.index')
@section('heading')
<i class="fa fa-flag-checkered"></i> {!! trans('Myaccount::selfmanage.label.address') !!} <small> {!! trans('cms.manage') !!} {!! trans('Myaccount::selfmanage.label.address') !!}</small>
@stop
@section('title')
{!! trans('Myaccount::selfmanage.label.address') !!}
@stop
@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{!! URL::to('admin') !!}"><i class="fa fa-dashboard"></i> {!! trans('cms.home') !!} </a></li>
    <li class="active">{!! trans('Myaccount::selfmanage.manage_address')!!}</li>
</ol>
@stop
@section('entry')
<div id="entry-user">

</div>
@stop
@section('content')
<table id="main-list" class="display table  nowrap">
    <thead>
        <th>{!! trans('Myaccount::selfmanage.label.date')!!}</th>
        <th>{!! trans('Myaccount::selfmanage.label.tracking_number')!!}</th>
        <th>{!! trans('Myaccount::selfmanage.label.action')!!}</th>
        <th>{!! trans('Myaccount::selfmanage.label.description')!!}</th>        
        <th>{!! trans('Myaccount::selfmanage.label.remark')!!}</th>
        
    </thead>
</table>
@stop
@section('script')
<script type="text/javascript">
var oTable;
$(document).ready(function(){
	$('#main-list').DataTable( {
        "ajax": '{{ URL::to('/selfmanage/booking/history') }}',
        "columns": [
			        { "data": "created_at" },
			        { "data": "tracking_number" },
			        { "data": "title"},
			        { "data": "description", "className": "hidden-xs" },			        
			        { "data": "remark", "className": "hidden-xs" }
			       ],
        
        "userLength": 50
    });
    $('#main-list tbody').on( 'click', 'tr', function () {
        
        $(this).toggleClass("selected").siblings(".selected").removeClass("selected");
        var d = $('#main-list').DataTable().row( this ).data();
        $('#entry-user').load('{{URL::to('selfmanage/booking')}}' + '/' + d.booking_id);
        
    });

   
});
</script>
@stop
@section('style')
@stop
