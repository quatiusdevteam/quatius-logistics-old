@extends('admin::curd.index')
@section('heading')
<i class="fa fa-location-arrow"></i> {!! trans('Myaccount::selfmanage.booking_status') !!} 
@stop
@section('title')
{!! trans('Myaccount::selfmanage.booking_status') !!}
@stop
@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{!! URL::to('admin') !!}"><i class="fa fa-dashboard"></i> {!! trans('cms.home') !!} </a></li>
    <li class="active">{!! trans('Myaccount::selfmanage.booking_status')!!}</li>
</ol>
@stop
@section('entry')
<div class="box box-warning" id='entry-user'>
</div>
@stop

@section('content')
<table id="main-list" class="display table responsive nowrap">
    <thead>
        <th>{!! trans('Myaccount::selfmanage.label.pronto_id')!!}</th>
        <th>{!! trans('Myaccount::selfmanage.label.status')!!}</th>
        <th>{!! trans('Myaccount::selfmanage.label.allow_customer_change')!!}</th>
        <th>{!! trans('Myaccount::selfmanage.label.allow_staff_change')!!}</th>
        <th>{!! trans('Myaccount::selfmanage.label.last_update')!!}</th>
        
    </thead>
</table>
@stop



@section('script')
<script type="text/javascript">
var oTable;
$(document).ready(function(){
	$('#entry-user').load('{{URL::to('selfmanage/admin/status/0')}}');
	$('#main-list').DataTable( {
        "ajax": '{{ URL::to('/selfmanage/admin/status') }}',
        "columns": [
			        { "data": "pronto_status_id" },
			        { "data": "status"},
			        { "data": "allow_customer_change" },
			        { "data": "allow_staff_change" },
			        { "data": "updated_at"}
			       ],
        
        "userLength": 50
    });
    $('#main-list tbody').on( 'click', 'tr', function () {
        
        $(this).toggleClass("selected").siblings(".selected").removeClass("selected");
        var d = $('#main-list').DataTable().row( this ).data();
        $('#entry-user').load('{{URL::to('selfmanage/admin/status')}}' + '/' + d.id);
        
    });    
});


</script>
@stop
@section('style')
@stop
