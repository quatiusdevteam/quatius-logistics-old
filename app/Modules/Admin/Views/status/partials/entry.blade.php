<div class="box box-info" id="receiver">
    	<div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-12 col-lg-6">
	        {!! Form::text('pronto_status_id')
	        -> label(trans('Myaccount::selfmanage.label.pronto_id'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.pronto_id'))	        
	        -> required('required')!!}
	        </div>       
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('status')
	        -> label(trans('Myaccount::selfmanage.label.status'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.status'))	        
	        -> required('required')!!}
	        </div>
	    </div>    
	    <div class="row">	        
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('allow_customer_change')
	        -> label(trans('Myaccount::selfmanage.label.allow_customer_change'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.allow_customer_change'))	        
	        -> required('required')!!}
	        </div>
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		        {!! Form::text('allow_staff_change')
		        -> label(trans('Myaccount::selfmanage.label.allow_staff_change'))
		        -> placeholder(trans('Myaccount::selfmanage.placeholder.allow_staff_change'))	        
		        -> required('required')!!}	        
		   </div>   
	    </div>
		
</div>  