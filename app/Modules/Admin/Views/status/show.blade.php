<div class="box-header with-border">
    <h3 class="box-title"> {{ trans('cms.view') }}   {{ trans('Myaccount::selfmanage.label.booking_status') }}  [{{ $status->status}}]  </h3>
       <div class="box-tools pull-right">
        <button type="button" class="btn btn-success btn-sm" data-action='NEW' data-load-to='#entry-user' data-href='{{Trans::to('selfmanage/admin/status/create')}}'><i class="fa fa-times-circle"></i> {{ trans('cms.new') }}</button>

        <button type="button" class="btn btn-primary btn-sm" data-action="EDIT" data-load-to='#entry-user' data-href='{{ trans_url('/selfmanage/admin/status') }}/{{$status->id}}/edit'><i class="fa fa-pencil-square"></i> {{ trans('cms.edit') }}</button>
        <button type="button" class="btn btn-danger btn-sm" data-action="DELETE" data-load-to='#entry-user' data-datatable='#main-list' data-href='{{ trans_url('/selfmanage/admin/status') }}/{{$status->id}}' >
            <i class="fa fa-times-circle"></i> {{ trans('cms.delete') }}
        </button>
        
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body" >
    	
        {!!Form::vertical_open()
        ->id('user-user-show')
        ->method('PUT')
        ->action(trans_url('selfmanage/admin/status/'. $status->id))!!}
            <div class="tab-content">
                @include('Admin::status.partials.entry')
            </div>
        {!! Form::close() !!}
    
</div>


<div class="box-footer" >
    &nbsp;
</div>
