<div class="row">
    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-title">
    		<h4>{!!trans('Myaccount::selfmanage.login_detail')!!}</h4>
    	</div>
    </div>
    <div class="box box-success">
    	<div class="row">		 	
		 	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		 		{!! Form::email('email')
			        -> label(trans('Myaccount::selfmanage.label.email'))
			        -> placeholder(trans('Myaccount::selfmanage.placeholder.email'))
			        -> required('required')
			    !!}
		 	</div>
		 	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		 		{!! Form::text('name')
			        -> label(trans('Myaccount::selfmanage.label.name'))
			        -> placeholder(trans('Myaccount::selfmanage.placeholder.name'))
			        -> required('required')
			    !!}
		 	</div>
		</div>
		<div class="row">		 	
		 	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		 		{!! Form::password('password')
	                -> label(trans('Myaccount::selfmanage.label.password'))
	                -> placeholder(trans('Myaccount::selfmanage.placeholder.password'))
	                -> required('required')
	            !!}
		 	</div>
		 	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		 		{!! Form::password('password_confirmation')
	                -> label(trans('Myaccount::selfmanage.label.confirm_password'))
	                -> placeholder(trans('Myaccount::selfmanage.placeholder.confirm_password'))
	                -> required('required')
	            !!}
		 	</div>
		</div>
	</div>