	<div class="row">
    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-title">
    		<h4>{!!trans('Myaccount::selfmanage.login_detail')!!}</h4>
    	</div>
    </div>
    <div class="box box-success">
    	<div class="row">		 	
		 	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		 		{!! Form::email('email')
			        -> label(trans('Myaccount::selfmanage.label.email'))
			        -> placeholder(trans('Myaccount::selfmanage.placeholder.email'))
			        -> required('required')
			    !!}
		 	</div>
		 	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		 		<div class="row">
					<div class="col-xs-12 col-sm-6">
						{!! Form::text('pronto_user_id')
							-> label(trans('Myaccount::selfmanage.label.pronto_id'))
							-> placeholder(trans('Myaccount::selfmanage.placeholder.pronto_id'))
						!!}
					</div>
					<div class="col-xs-12 col-sm-6">
						{!! Form::select('discount')
							-> label(trans('Myaccount::selfmanage.label.discount'))
							-> options(trans('Myaccount::selfmanage.options.discount'))
							-> required('required')
						!!}
					</div>
				</div>
		 	</div>
		</div>
		<div class="row">		 	
		 	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		 		{!! Form::password('password')
	                -> label(trans('Myaccount::selfmanage.label.password'))
	                -> placeholder(trans('Myaccount::selfmanage.placeholder.password'))
	                -> required('required')
	            !!}
		 	</div>
		 	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		 		{!! Form::password('password_confirmation')
	                -> label(trans('Myaccount::selfmanage.label.confirm_password'))
	                -> placeholder(trans('Myaccount::selfmanage.placeholder.confirm_password'))
	                -> required('required')
	            !!}
		 	</div>
		</div>
	</div>
	<div class="row">
    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-title">
    		<h4>{!!trans('Myaccount::selfmanage.your_detail')!!}</h4>
    	</div>
    </div>
    <div class="box box-info">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				{!! Form::text('company_name')
			        -> label(trans('Myaccount::selfmanage.label.company_name'))
			        -> placeholder(trans('Myaccount::selfmanage.placeholder.company_name'))
			    !!}
			</div>
		</div>
		<div class="row">
		 	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		 		{!! Form::text('first_name')
			        -> label(trans('Myaccount::selfmanage.label.first_name'))
			        -> placeholder(trans('Myaccount::selfmanage.placeholder.first_name'))
			        -> required('required')
			    !!}
		 	</div>
		 	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		 		{!! Form::text('last_name')
			        -> label(trans('Myaccount::selfmanage.label.first_name'))
			        -> placeholder(trans('Myaccount::selfmanage.placeholder.first_name'))
			        -> required('required')
			    !!}
		 	</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		 		{!! Form::select('sex')
			        -> label(trans('Myaccount::selfmanage.label.gender'))	              
					-> options(trans('Myaccount::selfmanage.options.genders'))
					-> placeholder(trans('Myaccount::selfmanage.placeholder.gender'))
			        -> required('required')
			    !!}
		 	</div>
		 	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		 		{!! Form::tel('phone_1')
			        -> label(trans('Myaccount::selfmanage.label.phone'))
			        -> placeholder(trans('Myaccount::selfmanage.placeholder.phone'))
			        -> required('required')
			    !!}
		 	</div>
		</div>	        
		
		<div class="row">
		 	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		 		{!! Form::text('address_1')
			        -> label(trans('Myaccount::selfmanage.label.address_1'))
			        -> placeholder(trans('Myaccount::selfmanage.placeholder.address_1'))
			        -> required('required')
			    !!}
		 	</div>
		 	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		 		{!! Form::text('address_2')
			        -> label(trans('Myaccount::selfmanage.label.address_2'))
			        -> placeholder(trans('Myaccount::selfmanage.placeholder.address_2'))	        
			    !!}
		 	</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				{!! Form::text('city')
			        -> label(trans('Myaccount::selfmanage.label.city'))
			        -> placeholder(trans('Myaccount::selfmanage.placeholder.city'))	    
			        -> required('required')
			    !!}
		   	</div>	 
		    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				{!! Form::select('state')			                    
					-> options(trans('Myaccount::selfmanage.options.states'))
					-> label(trans('Myaccount::selfmanage.label.state'))
					-> placeholder(trans('Myaccount::selfmanage.placeholder.state'))			
					-> required('required')!!}
		    </div>      
		</div>
		<div class="row">
		   	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		        {!! Form::select('country')			       
			        -> label(trans('Myaccount::selfmanage.label.country'))             
					-> options(trans('Myaccount::selfmanage.options.countries'))		
					-> placeholder(trans('Myaccount::selfmanage.placeholder.country'))			
					-> required('required')
				!!}
		  	</div>	 
			<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		        {!! Form::text('postcode')
			        -> label(trans('Myaccount::selfmanage.label.postcode'))
			        -> placeholder(trans('Myaccount::selfmanage.placeholder.postcode'))	    
			        -> required('required')
		        !!}	
		  	</div>      
		</div>
	</div>



