<div class="box-header with-border">
    <h3 class="box-title"> {{ trans('cms.view') }}   {{ trans('user::user.user.name') }}  [{{ $user->name }}]  </h3>
       <div class="box-tools pull-right">
        <button type="button" class="btn btn-success btn-sm" data-action='NEW' data-load-to='#entry-user' data-href='{{Trans::to('selfmanage/admin/customer/create')}}'><i class="fa fa-times-circle"></i> {{ trans('cms.new') }}</button>
        @if($user->id)
        <button type="button" class="btn btn-primary btn-sm" data-action="EDIT" data-load-to='#entry-user' data-href='{{ trans_url('/selfmanage/admin/customer') }}/{{$user->id}}/edit'><i class="fa fa-pencil-square"></i> {{ trans('cms.edit') }}</button>
        
        @endif
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body" >
    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        
        {!!Form::vertical_open()
        ->id('user-user-show')
        ->method('PUT')
        ->action(trans_url('selfmanage/admin/customer/'. $user->getRouteKey()))!!}
            <div class="tab-content">
                @include('Admin::user.partial.entry')
            </div>
        {!! Form::close() !!}
    </div>
</div>
<div class="box-footer" >
    &nbsp;
</div>
