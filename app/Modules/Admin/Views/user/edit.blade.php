<div class="box-header with-border">
    <h3 class="box-title"> {{ trans('cms.edit') }}  {{ trans('user::user.user.name') }} [{!!$user->name!!}] </h3>
    <div class="box-tools pull-right">
        <button type="button" class="btn btn-primary btn-sm" data-action='UPDATE' data-form='#edit-user'  data-load-to='#entry-user' data-datatable='#main-list'><i class="fa fa-floppy-o"></i> {{ trans('cms.save') }}</button>
        <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#entry-user' data-href='{{Trans::to('selfmanage/admin/customer/0')}}'><i class="fa fa-times-circle"></i> {{ trans('cms.cancel') }}</button>
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body" >
    <div class="nav-tabs-custom">
        
        {!!Form::vertical_open()
        ->id('edit-user')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->action(URL::to('selfmanage/admin/customer/'. $user->getRouteKey()))!!}
        
             @include('Admin::user.partial.entry')
        
        {!!Form::close()!!}
    </div>
</div>
<script>
$(document).ready(function(){
	$("#password").removeAttr("required");
	$('label[for="password"]').find("sup").empty();
	$("#password_confirmation").removeAttr("required");
	$('label[for="password_confirmation"]').find("sup").empty();
});
</script>
<div class="box-footer" >
    &nbsp;
</div>
