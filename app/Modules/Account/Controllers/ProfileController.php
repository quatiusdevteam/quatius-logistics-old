<?php namespace App\Modules\Account\Controllers;

use Illuminate\Http\Request;
use Auth;
use Gate;
use App\Http\Controllers\PublicWebController;
use Hash;
use Lang;

class ProfileController extends PublicWebController{
	
	public function __construct()
    {
        parent::__construct();
        $this->setupTheme(config('theme.map.registered.theme'), config('theme.map.registered.layout'));
    }

	public function changePassword(Request $request)
	{
		return $this->theme->of('Account::password')->render();
	}
	
	public function updatePassword(Request $request)
	{
		$this->validate($request, [
			'old_password' => 'required',
			'password' => 'required|confirmed|min:6',    
		]);

		$user = Auth::user();
		if (Hash::check($request->old_password, $user->password)) { 
		   $user->fill([
			'password' => Hash::make($request->password)
			])->save();
			
			if ($request->expectsJson()) {
				return response()->json([
					'message'  => "Password has been changed.",
					'code'     => 204,
				], 201);
			}

			flash('Password has been changed.');
			return redirect('home');
		} else {
			if ($request->expectsJson()) {
				return response()->json([
					'message'  => Lang::get('auth.failed'),
					'code'     => 401,
				], 301);
			}

			return redirect()->back()
			//->withErrors("Unknown Error");
			->withErrors([
				"old_password" => Lang::get('auth.failed'),
			]);
		}
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function show()
	{
		return $this->theme->of('Account::profile',["user"=> Auth::user()])->render();
	}
	
	public function editSingle(Request $request)
	{				
		
		if( $request->has('name') && $request->has('value')) {
			$field_name = $request->get('name');
			$field_value = $request->get('value');
			
			$user = Auth::user()						
			->update([$field_name => $field_value]);
			return response()->json([ 'code'=>200], 200);
		}
	
		return response()->json([ 'Require' ], 400);	
	}
}
