<?php

namespace App\Modules\Account\Controllers;

use App\Http\Controllers\AdminWebController;
use Illuminate\Http\Request;

use Form;
use Quatius\Account\Repositories\AccountRepository;
use Quatius\Account\Repositories\Presenters\AccountIndexPresenter;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Validator;

class AdminAccountController extends AdminWebController
{
    private $repo;

    public function __construct(AccountRepository $repo)
    {
        //$this->setupTheme(config('theme.map.registered.theme'), config('theme.map.registered.layout'));
        parent::__construct();
        $this->repo = $repo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            $this->repo->setPresenter(AccountIndexPresenter::class);
            return response()->json($this->repo->withCount('users')->all());
        }

        return $this->theme->of('Account::admin.index')->render();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function users(Request $request)
    {
        if ($request->wantsJson()) {
            $requestData = $request->all();
            $query = \App\User::whereAccountId(app('account')->getAccountId());
            return response()->json(dataTableQuery($requestData, $query));
        }

        return $this->theme->of('Account::users')->render();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $account = $this->repo->findOrFail($id);
        Form::populate($account);
        return view('Account::admin.show', compact('account'));
    }

    /**
     * Show the form for creating a new user.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $account = $this->repo->newInstance([]);

        Form::populate($account);

        return view('Account::admin.create', compact('account'));
    }

    /**
     * Create new user.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $attributes = $request->all();
            
            $account = $this->repo->create($attributes);

            return response()->json([
                'message'  => trans('messages.success.created', ['Module' => "Account"]),
                'code'     => 204,
                'redirect' => trans_url('/admin/accounts/' . $account->getRouteKey()),
            ], 201);

        } catch (\Exception $e) {
             return response()->json([
                'message' => $e->getMessage(),
                'code'    => 400,
            ], 400);
        }

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $account = $this->repo->findOrFail($id);
        Form::populate($account);
        return view('Account::admin.edit', compact('account'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $attributes = $request->all();
            $this->repo->setPresenter(null);
            $account = $this->repo->update($attributes, $id);
            
            return response()->json([
                'message'  => trans('messages.success.updated', ['Module' => "Account"]),
                'code'     => 204,
                'redirect' => trans_url('/admin/accounts/' . $account->getRouteKey()),
            ], 201);

        } catch (\Exception $e) {

            return response()->json([
                'message'  => $e->getMessage(),
                'code'     => 400
            ], 400);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            $account = $this->repo->findOrFail($id);
            foreach($account->users as $user){
                $user->update(['status'=>"Suspended"]);
            }
            $this->repo->delete($id);

            return response()->json([
                'message'  => trans('messages.success.deleted', ['Module' => "Account"]),
                'code'     => 202,
                'redirect' => trans_url('/admin/accounts/new'),
            ], 202);

        } catch (\Exception $e) {

            return response()->json([
                'message'  => $e->getMessage(),
                'code'     => 400,
                'redirect' => trans_url('/admin/accounts/' . hashids_encode($id)),
            ], 400);
        }
    }
    
    public function invitationSend(Request $request, $id)
    {
        try{
            $account = $this->repo->findOrFail($id);
            Validator::make($request->all(), [
                'role'		=> 'required|not_in:0',	
                'email'     => ['required', 'string', 'email', 'max:255'],
            ])->validate();
            $email = $request->get('email', '');
            $role = $request->get('role', config('quatius.account.activate-role','staff'));
            
            abort_if(!!\App\User::whereEmail($email)->count(), 303, trans('Account::invite.email-already-registered', ["email"=>$email]));

            $account->sendInvitation($email, $role);
            return response()->json([
                'message'  => trans('Account::invite.sent-to', ["email"=>$email]),
                'code'     => 204,
                ], 201);

        } catch (HttpException $e) {
            return response()->json([
                    'message' => $e->getMessage(),
                    'code'    => $e->getStatusCode(),
                    ], $e->getStatusCode());
        
        } catch (\Exception $e) {
            return response()->json([
                    'message' => $e->getMessage(),
                    'code'    => 400,
                    ], 400);
        }
    }
}
