<?php

namespace App\Modules\Account\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB;
use Form;
use Illuminate\Support\Str;
use Quatius\Account\Repositories\AccountRepository;
use Quatius\Account\Repositories\Presenters\AccountIndexPresenter;
use Quatius\Address\Models\Address;
use Event;
use Illuminate\Validation\ValidationException;
use Laracasts\Flash\Flash;
use Quatius\Account\Models\Account;
use Quatius\Account\Notifications\ActiveAccount;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Validator;

class AccountController extends Controller
{
    private $repo;

    public function __construct(AccountRepository $repo)
    {
        $this->setupTheme(config('theme.map.registered.theme'), config('theme.map.registered.layout'));
        $this->repo = $repo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            $requestData = $request->all();
            $query = $this->repo->statusDateOf('submitted')->manifestNo()->latestStatus();
            return response()->json(dataTableQuery($requestData, $query));
        }

        return $this->theme->of('Account::index')->render();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function users(Request $request)
    {
        if ($request->wantsJson()) {
            
            $query = \App\User::whereAccountId(app('account')->getAccountId());
            $users = $query->get();
            $datas = $users->toArray();;
            $account = $this->repo->detail();
            $tmpNewList = [];
            $hasAccepted = false;
            foreach($account->getParams('invites',[]) as $invite){
                
                if (!!$users->where("email", $invite["email"])->count()){
                    $hasAccepted = true;
                }else{
                    $tmpNewList[] = $invite;
                }

                $datas[] = [
                    "email"=> $invite["email"],
                    "designation"=> $invite["designation"],
                    "name"=>"",
                    "id"=>0,
                    "status"=>"Invited",
                    "created_at"=> $invite["created_at"]
                ];
            }
            if ($hasAccepted){
                if ($tmpNewList){
                    $account->setParams('invites', $tmpNewList);
                }
                else{
                    
                    $account->unsetParams('invites');
                }
                $account->save();
            }
            return response()->json(["data"=>$datas]);
        }

        return $this->theme->of('Account::users')->render();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $account = app('account')->detail();
        
        if ($request->wantsJson()) {
            $this->repo->setPresenter(AccountIndexPresenter::class);
            return response()->json($this->repo->parserResult($account));
        }
        
        return $this->theme->of('Account::show', compact($account))->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $account = app('account')->detail();
        Form::populate($account);
        return $this->theme->of('Account::edit', compact($account))->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->repo->setPresenter(null);
        $account = $this->repo->update($request->all(), app('account')->detail()->id);
        app('account')->setDetail($account);
        if ($request->wantsJson()) {
            return response()->json([
                "success"=>true
            ]);
        }

        Form::populate($account);
        return $this->theme->of('Logistic::edit', compact($account))->render();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function invitationStore(Request $request)
    {
        $data = $request->all();
      
        Validator::make($data, [
            'first_name'           => 'required|max:255',
            'last_name'            => 'required|max:255',
            'phone' 			   => 'min:8',
            'address_1'			   => 'required|max:255',
            'city'				   => 'required|max:255',
            'state'				   => 'required|not_in:0',	
            'postcode'             => 'required|max:4|min:4',
            'country'			    => 'required|not_in:0',
            'password'             => 'required|confirmed|min:6'
        ])->validate();

        $errorInputs = [];
        $data = app('address')->formatSimplify($data, function($inputField, $msg, $val) use(&$errorInputs, &$errorMsg){
            $errorInputs[$inputField] = $msg;
        });

        if ($errorInputs)
            throw ValidationException::withMessages(array_map(function($val){return trans($val);}, $errorInputs));

        $dataToken = ActiveAccount::getDataFromToken($request->get('token',''));
        if(!!\App\User::whereEmail($dataToken["e"])->count()){
            flash("Account has already been registered.");
            return redirect('login');
        }
        
        $account = app('account')->find($dataToken['a']);

        abort_unless($account, 404);
        $invites = $account->getParams('invites',[]);
        $index = collect($invites)->where("email", $dataToken["e"])->keys()->first();
        
        if ($index===null || !isset($invites[$index])){
            flash(trans('Account::invite.account-invite-expired', ["email"=>$dataToken["e"]]));
            return redirect('/');
        }
        $invite = $invites[$index];

        $data["email"] = $dataToken["e"];
        $data["phone_1"] = $data["phone"];
        $contact = $account->addresses()->save(new Address($data));
        $data["contact_id"] = $contact->id;
        
        $user = new \App\User();
        $user->setParams('logistic.prefer.sender', $contact->id);
        $user->setParams('logistic.prefer.pickup', $contact->id);


        if (isset($invite["designation"]))
            config()->set('quatius.account.activate-role', $invite["designation"]);

        $user->forceFill([
            'name' => $contact->first_name,
            'email' => $contact->email,
            'status' => "Active",
            'password' => bcrypt($request->get('password','')),
            'api_token' => Str::random(60),
            'remember_token' => Str::random(60),
            'designation' => config('quatius.account.activate-designation', config('quatius.account.activate-role','staff')),
            'account_id' => $account->id
        ])->save();
        
        $user->syncRoles(config('quatius.account.role-types.'.config('quatius.account.activate-role','staff'), [5]));
        
        array_splice($invites, $index, 1); // remove index from invites

        if ($invites){
            $account->setParams('invites', $invites);
        }else{
            $account->unsetParams('invites');
        }
        $account->save();

        Event::dispatch('auth.register', [$user, $data]);

        auth()->guard($this->getGuard())->login($user, false);
        flash(trans('Account::invite.account-activated'));
        return redirect('home')->withCode(201)->withMessage(trans('Account::invite.account-activated'));
    }
    
    public function invitationForm(Request $request, $token = null)
    {
        if (is_null($token)) {
            return redirect(url('/'));
        }
        $data = ActiveAccount::getDataFromToken($token);
        
        $account = app('account')->find($data['a']);
        abort_unless($account, 404);
        $email = $data['e'];
        $company = $account->name;

        if(!!\App\User::whereEmail($email)->count()){
            flash(trans('Account::invite.account-already-registered', ["email"=>$email]));
            return redirect('login');
        }
        
        if (!collect($account->getParams('invites',[]))->where("email",$email)->count()){
            flash(trans('Account::invite.account-invite-expired', ["email"=>$email]));
            return redirect('/');
        }

        Form::populate([
            "company"=>$company,
            "email"=>$email
        ]);
        $this->setupTheme(config('theme.map.admin.theme'), config('theme.map.admin.blank'));
        return $this->theme->of("Account::invitation", compact('token', 'account', 'email', 'company'))->render();
    }

    public function invitationSend(Request $request)
    {
        $data = $request->all();

        Validator::make($data, [
            'role'		=> 'required|not_in:0',	
            'email'     => ['required', 'string', 'email', 'max:255'],
        ])->validate();

        try{
            $email = $request->get('email', '');
            $role = $request->get('role', config('quatius.account.activate-role','staff'));
            abort_unless($email, 201, 'Email required');
            
            abort_if(!!\App\User::whereEmail($email)->count(), 303, trans('Account::invite.email-already-registered', ["email"=>$email]));

            app('account')->detail()->sendInvitation($email, $role);
            return response()->json([
                'message'  => trans('Account::invite.sent-to', ["email"=>$email]),
                'code'     => 204,
                //'redirect' => trans_url('/selfmanage/booking/' . $thisBooking->id),
                ], 201);

        } catch (HttpException $e) {
            return response()->json([
                    'message' => $e->getMessage(),
                    'code'    => $e->getStatusCode(),
                    ], $e->getStatusCode());
        
        } catch (\Exception $e) {
            return response()->json([
                    'message' => $e->getMessage(),
                    'code'    => 400,
                    ], 400);
        }
    }
}
