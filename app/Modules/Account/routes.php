<?php

Route::group(array('module' => 'Account','middleware' => ['web', 'auth'], 'namespace' => 'App\Modules\Account\Controllers'), function() {

	Route::post('account/edit/password', 'ProfileController@updatePassword');
	Route::get('account/edit/password', 'ProfileController@changePassword');
	Route::get('profile', 'ProfileController@show');
	Route::get('account/profile', 'ProfileController@show');
});	

Route::group(array('module' => 'Account','middleware' => ['web'], 'namespace' => 'App\Modules\Account\Controllers'), function() {
	Route::post('account/invitation', 'AccountController@invitationStore');
	Route::get('account/invitation/{token}', 'AccountController@invitationForm');
});	