
<section class="content-header">
	<h1 style="padding: 10px 0;">Change Password</h1>
</section>
<section class="content-body">
	<div class="panel panel-default">
		<div class="panel-body">
			@include('public::notifications')

			{!! Form::horizontal_open()->method('POST') !!}
				{{ csrf_field() }}

				@include('Account::partials/password')
				
				<div class="row">
					<div class="col-xs-12 text-center">
							{!! Form::submit(trans('user::user.changepword'))->class('btn btn-primary')!!}
					</div>				      	
				</div>

			{!! Form::close() !!}
		</div>	    	
	</div>	
</section>