<section class="content">

    <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="box-title">Manage Users</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-primary btn-sm" onclick="sendInvite()" type="button"><i class="fa fa-user"></i>&nbsp; Invite User</a>
            </div>
        </div>
        <div class="box-body" style="min-height:550px">

        <!-- empty dialog -->
            <div id="remoteDialogCreate" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog  modal-lg" role="document">
                <div class="modal-content">
                    
                </div>
            </div>
            </div>
            <div id="remoteDialogEdit" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog  modal-lg" role="document">
                <div class="modal-content">
                    
                </div>
            </div>
            </div>
        <!-- end empty dialog -->
            <table id="dt-users" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Name</th>   
                        <th>Email</th>
                        <th>Designation</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>                              
                </tbody> 
            </table>
        </div>
    </div>
</section>


@includeScript('vendors/js/sweetalert2-11.js')
@script
<script type="text/javascript">
    var send = null;
    function sendInvite(){
        Swal.fire ({
            title: "Send Invitation",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonText: "Confirm",
            backdrop: true,
            html: '<div class="input-group"><span class="input-group-addon">Email: </span>{!!Form::email("email","")!!}</div>'+
            '<br><div class="input-group"><span class="input-group-addon">Role: </span>{!!Form::select("role-type","")->placeholder("Select Role")->options(array_combine(array_keys(config("quatius.account.role-types",[])),array_keys(config("quatius.account.role-types",[]))))->value("staff")!!}</div>',
            allowOutsideClick: () => !Swal.isLoading(),
            preConfirm: (data) => {
                if ($('#email').val().trim()==""){
                    Swal.showValidationMessage(
                        "Email Required!"
                    )
                    return;
                }
                return new Promise((resolve, reject) => {
                    $.post("{{URL::to('account/send-invite')}}", {email:$('#email').val(),role:$('#role-type').val()}, function(response){
                        resolve(response);
                    },'json')
                    .fail(function(xhr, status, error) {
                        reject(xhr.responseJSON);
                    });
                }).catch(error => {
                    Swal.showValidationMessage(
                        ` ${error.message}`
                        )
                    });
                
                }
            })
        .then((result) => {
            if (result.isConfirmed) {
                Swal.fire("", result.value.message, 'success')
                $('#dt-users').DataTable().ajax.reload( null, false );
            }
        });
    }

    $(document).ready(function() {
         $('#dt-users').DataTable({
            "columns": [                  
                { "data": "name" },
                { "data": "email" },
                { "data": "designation" },
                { "data": "status" },
                { "data": "id" },
            ],
            "columnDefs": [      
                {
                    "render": function ( data, type, row ) {
                        let str = "<a target='_self' class='btn-link' onclick=\"\"><i class='fa fa-edit'></i>&nbsp; Edit</a>";
                            str += " &nbsp;  &nbsp; <a target='_self'><i class='fa fa-trash'></i>&nbsp; Delete</a>";
                            // str += " | <a target='_self' href='#'><i class='fa fa-edit'></i></a> ;";

                        return str;
                        //return "";
                    },
                    "orderable": false,
                    "targets": 4
                },
            ],
            //serverSide: false,
            //processing: true,

            ajax: {
                url: "{{ URL::to('account/users') }}",
                "dataType": "json",
                "type": "GET",
                "data":{
                    extra_fields : [
                        "id"
                    ],
                    hash:{
                        from: "id",
                        to: "eid"
                    }
                }
                
            },

            "pageLength": 10,
        });
    
        $('#dt-users tbody').on( 'click', 'tr', function () {
            $(this).toggleClass("selected").siblings(".selected").removeClass("selected");
            var d = $('#dt-users').DataTable().row( this ).data();
            //$('#remoteDialogEdit').modal('show').find('.modal-content').load('{{URL::to("address")}}' + '/' + d.id);
        });
    });
</script>
@endscript