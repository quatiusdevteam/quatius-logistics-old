<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
    
    <body style="margin: 0; padding: 0;">
		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background: #eee; -webkit-font-smoothing:antialiased; -webkit-text-size-adjust:none; padding: 20px 0;">
			<tr> 
				<td> 
					<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="background: #fff; border-collapse: collapse; font-family: 'Arial', sans-serif; font-size: 14px; line-height: 1.5em; width: 600px; height: 100%; margin: 0 auto;"> 
											
						<!-- YOUR ORDER HAS SHIPPED CONTENT -->
						<tr> 
							<td align="center" style="padding: 58px 0 30px 0;">
								<h1 style="font-size: 24px; text-transform: uppercase; line-height: 2em;">Verify Your Email Address</h1>
								You registration is successful verify your email before proceeding by clicking the link below. 
								<br><br><br>
								<a style="padding: 12px; color: #ffffff; background-color: #337ab7; border-color: #2e6da4; color:#fff; text-decoration: none; text-align: center;" href="{{ URL::to($guard . '/verify/' . $confirmation_code) }}">
					            	ACTIVATE ACCOUNT
					            </a>.<br/><br/><br/>
							</td>
						</tr> 
					</table>
				</td>
			</tr>
		</table>
    </body>
</html>
