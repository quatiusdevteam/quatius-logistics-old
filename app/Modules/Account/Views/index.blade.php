@includeStyle('vendors/css/logistic.css')
@includeStyle('vendors/css/timeline-simple.css')
@includeStyle('vendors/css/jquery-ui.css')

@includeScript('vendors/js/Chart.min.js')

<?php
config()->set('quatius.logistic.history.status-active', config('quatius.logistic.history.status-admin', 1));
?>

<!-- use Carbon\Carbon; -->

<section class="content-header">
    <h1>
    <i class="fa fa-dashboard"></i> Dashboard 
    </h1>
    <ol class="breadcrumb">
        <li><i class="fa fa-dashboard"></i> Home </li>
    </ol>
</section>


<section class="content">
<?php
    $conns = app('connote')->byUserAccount()->latestStatus()->get();
    // foreach ($conns as $i => $conn){
    //     dump(($i+1).", ". $conn->status."  , ".$conn->remark);
    // }
    

    
    // $conns_today = $conns;
    $conns_today = $conns->where('created_at','>=', Carbon\Carbon::today());
    // $num_conns_today_delayed = $conns_today->where('status','delayed')->count();    
    $num_conns_today_cancelled = $conns_today->where('status','cancelled')->count();
    $num_conns_today_warning    = $conns_today->where('remark','<>','')->count();    

    // $num_conns_today_created    = $conns_today->filter(function ($conn) {
    //     return $conn->status == null || $conn->remark != "";
    //     // return $conn->status == null || in_array($conn->status, ['ready to print', 'submitted', 'booking pending']);
    //     // return $conn->status != null && (! in_array($conn->status, ['ready to print', 'submitted', 'booking pending']));

    // });

    // $i=0;
    // foreach ($conns_today as $conn){
    //     dump((++$i).", ". $conn->status."  , ".$conn->remark);
    // }

    // $num_conns_today_pending   = $conns_today->filter(function ($conn) { return ($conn->status == null || $conn->remark != "") || ( in_array($conn->status, config('quatius.logistic.status.group.pending',[])) ); })->count();
    $num_conns_today_pending   = $conns_today->filter(function ($conn) { return ($conn->status == null && $conn->remark == "") || ( in_array($conn->status, config('quatius.logistic.status.group.pending',[])) ); })->count();
    $num_conns_today_transit    = $conns_today->whereIn('status',config('quatius.logistic.status.group.transit',[]))->count();
    $num_conns_today_undeliverable = $conns_today->whereIn('status',config('quatius.logistic.status.group.undeliverable',[]))->count();
    $num_conns_today_complete   = $conns_today->whereIn('status',config('quatius.logistic.status.group.complete',[]))->count();    

    // dump($conns_today->filter(function ($conn) { return ($conn->status == null || $conn->remark != "") || ( in_array($conn->status, config('quatius.logistic.status.group.pending',[])) ); })->pluck('status'));
    // dump($num_conns_today_warning);
    // dump($num_conns_today_created);
    // dump($num_conns_today_pending);
    // dump($num_conns_today_transit);
    // dump($num_conns_today_undeliverable);
    // dump($num_conns_today_complete);

    $mfs = app('connote')->manifestModel()->byUserAccount()->latestStatus()->get();

?>
    <!-- Customer Info -->
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
            <div class="small-box bg-primary">
                @php
                    $account = app('account')->detail();
                @endphp
                <div class="inner">
                    <h3><small style="color: inherit; text-transform:uppercase;">WELCOME </small></h3>
                    <h4>{{$account->name}}</h4>                    
                    <p>
                    <a href="mailto:{{$account->email}}"><i class="fa fa-envelope-o"> </i>&nbsp; {{app('account')->user()->email}}</a>
                    <!-- &nbsp;  &nbsp; <a href="tel:{{$account->phone_1}}"><i class="fa fa-phone"></i>&nbsp; {{$account->phone_1}}</a> -->
                    </p>
                </div>
                <div class="icon"><i class="fa fa-user"></i></div>
                <div class="small-box-footer">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 border-right">
                            <div class="description-block">
                                <h5 class="description-header">{{$account->account_code}}</h5>
                                <span class="description-text">CUSTOMER CODE</span>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 border-right">
                            <div class="description-block">
                                <h5 class="description-header">{{$account->getParams('pronto.status', 'OK')}}</h5>
                                <span class="description-text">STATUS</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="account-credits col-xs-12 col-sm-6 col-md-6 col-lg-4">
            @includeStyle('css/slick.css')
            @includeStyle('css/slick-theme.css')
            @includeScript('js/slick.min.js')
            <style>
                .account-credits .price-forcast:not(.slick-initialized) > div:nth-child(n+4) {
                    display: none;
                }
            </style>
            @script
            <script>
                $(document).ready(()=>{
                    $('.account-credits .price-forcast').slick({
                        slidesToShow: 3,
                        autoplay: true,
                        arrows: false,
                        dots: false
                    });
                });

            </script>
            @endscript
            @php
                $account = app('account')->detail();
            @endphp
            <div class="small-box bg-primary">
                <div class="inner">
                    <h3 title="CURRENT BALANCE">$ {{number_format($account->getParams('pronto.Balance', '0'),2)}}</h3>
                    <!-- <p>CURRENT BALANCE</p> -->
                    <p>CREDIT LIMIT: $ {{number_format($account->credit_limit, 0)}}</p>
                    <p>Terms: {{$account->getParams('pronto.Terms', ' ')}}</p>
                </div>
                <div class="icon"><i class="fa fa-usd"></i></div>
                <div class="small-box-footer">
                    <div class="row price-forcast">
                         <div class="col-xs-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header">$ {{number_format($account->getParams('pronto.Up To 7 Days', '0'),2)}}</h5>
                                <span class="description-text">Up To 7 Days</span>
                            </div>
                        </div>
                        <div class="col-xs-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header">$ {{number_format($account->getParams('pronto.7 - 14 Days', '0'),2)}}</h5>
                                <span class="description-text">7 - 14 Days</span>
                            </div>
                        </div>
                        <div class="col-xs-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header">$ {{number_format($account->getParams('pronto.14 - 30 Days', '0'),2)}}</h5>
                                <span class="description-text">14 - 30 Days</span>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="description-block">
                                <h5 class="description-header">$ {{number_format($account->getParams('pronto.30 - 60 Days', '0'),2)}}</h5>
                                <span class="description-text">30 - 60 Days</span>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="description-block">
                                <h5 class="description-header">$ {{number_format($account->getParams('pronto.60 - 90 Days', '0'),2)}}</h5>
                                <span class="description-text">60 - 90 Days</span>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="description-block">
                                <h5 class="description-header">$ {{number_format($account->getParams('pronto.Over 90 Days', '0'),2)}}</h5>
                                <span class="description-text">Over 90 Days</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    
        <!-- 
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">           
            <div class="small-box  bg-primary">
                <div class="inner" style="display: inline-block">
                    <h3>{{$mfs->count()}}</h3>
                    <p>MANIFEST</p>
                    <p>&nbsp;</p>
                </div>                
                <div class="icon"><i class="fa fa-cubes"></i></div>
                <?php
                    $mfs_pending = $mfs->whereIn('status', ["submitted", "pending", "ready to print"])->count();
                    $mfs_transit = $mfs->whereIn('status', ["picked-up", "transit"])->count();
                    $mfs_complete = $mfs->whereIn('status', ["delivered", "completed"])->count();
                    $mfs_others = $mfs->count() - ($mfs_pending + $mfs_transit + $mfs_complete);
                    
                ?>
                <div class="small-box-footer">
                    <div class="row">                        
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 border-right">
                            <div class="description-block">
                                <h5 class="description-header">{{ $mfs_pending }}</h5>
                                <span class="description-text">PENDING</span>
                            </div>
                        </div>     
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 border-right">
                            <div class="description-block">
                                <h5 class="description-header">{{$mfs_transit}}</h5>
                                <span class="description-text">IN TRANSIT</span>
                            </div>
                        </div>                   
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 border-right">
                            <div class="description-block">
                                <h5 class="description-header">{{$mfs_complete}}</h5>
                                <span class="description-text">COMPLETE</span>
                            </div>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 border-right">
                            <div class="description-block">
                                <h5 class="description-header">{{$mfs_others}}</h5>
                                <span class="description-text">OTHERS</span>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div> -->
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">           
            <div class="small-box  bg-primary">
                <div class="inner" style="display: inline-block">
                    <h3>{{$conns->count()}}</h3>
                    <p>CONNOTES</p>
                    <p>&nbsp;</p>
                </div>
                <div class="icon"><i class="fa fa-files-o"></i></div>
                <div class="small-box-footer">
                    <div class="row">
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 border-right">
                            <div class="description-block">
                                @php
                                    $num_conns_pending = app('connote')->numConnotesIndicator();
                                @endphp
                                <h5 class="description-header">{{$conns->filter(function ($conn) { return ($conn->status == null && $conn->remark == "") || ( in_array($conn->status, config('quatius.logistic.status.group.pending',[])) ); })->count()}}
                                @if($num_conns_pending)
                                    <span class="bg-gray badge text-blue" title="Warning"><i class="fa fa-exclamation-triangle"></i> {{$num_conns_pending}}</span>
                                @endif    
                                </h5>
                                <span class="description-text">PENDING</span>
                            </div>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 border-right">
                            <div class="description-block">
                                <h5 class="description-header">{{$conns->whereIn('status',config('quatius.logistic.status.group.transit',[]))->count()}}</h5>
                                <span class="description-text">IN TRANSIT</span>
                            </div>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 border-right">
                            <div class="description-block">
                                <h5 class="description-header">{{$conns->whereIn('status',config('quatius.logistic.status.group.complete',[]))->count()}}</h5>
                                <span class="description-text">COMPLETE</span>
                            </div>
                        </div>
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 border-right">
                            <div class="description-block">
                                <h5 class="description-header">{{$conns->whereIn('status',config('quatius.logistic.status.group.undeliverable',[]))->count()}}</h5>
                                <span class="description-text">FAILED</span>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Today summary -->
    <div class="row">
        <div id="dialy-summary" class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title ">
                        <i class="fa fa-calendar"></i>&nbsp; Daily Report of Connotes Created on &nbsp;
                        <div class="input-group date" id='date' style="width:169px;"  onclick="$('#datepicker-1').datepicker();$('#datepicker-1').datepicker('show');">
                            <input type='text' class="form-control" id = "datepicker-1"/>
                                <span class="input-group-addon">
                                <span class="fa fa-calendar"></span>
                            </span>
                        </div>
                    </h3>
                    <div class="box-tools">
                        <span data-toggle="tooltip" title="" class="badge bg-status-warning {{($num_conns_today_warning >0? '':'hidden')}}" data-original-title="warning connotes">{{$num_conns_today_warning}}</span>
                        <span data-toggle="tooltip" title="" class="badge bg-status-cancelled {{($num_conns_today_undeliverable >0? '':'hidden')}}" data-original-title="2 cancelled connotes">{{$num_conns_today_undeliverable}}</span>
                        &nbsp;
                        <!-- <button id="btn-create-manifest" type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalSenderPickup" disabled title="Please, select at least one connote to create a manifest!"><i class="fa fa-send"></i>&nbsp; Create Manifest</button> -->
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body" style="min-height:130px; margin-top:1em;">
                    <div class="row">  
                        <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                            <div class="timeline-steps aos-init aos-animate" data-aos="fade-up">
                                <div class="timeline-step">
                                    <div class="timeline-content " data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="2003">
                                        <div class="inner-circle bg-status-cancelled"></div>
                                        <p class="h5 mt-3 mb-1">{{$num_conns_today_undeliverable}}</p>
                                        <p class="h5 mb-0 mb-lg-0">Undeliverable</p>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
                            <div class="timeline-steps aos-init aos-animate" data-aos="fade-up">
                                <div class="timeline-step">
                                    <div class="timeline-content " data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="2003">
                                        <div class="inner-circle bg-status-warning"></div>
                                        <p class="h5 mt-3 mb-1">{{$num_conns_today_warning}}</p>
                                        <p class="h5 mb-0 mb-lg-0">Warning</p>
                                    </div>
                                </div>
                            </div>
                        </div>     
                                                 
                        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                            <div class="timeline-steps aos-init aos-animate" data-aos="fade-up">                                
                                <div class="timeline-step">
                                    <div class="timeline-content" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="2004">
                                        <div class="inner-circle"></div>
                                        <p class="h5 mt-3 mb-1">{{$num_conns_today_pending}}</p>
                                        <p class="h5 mb-0 mb-lg-0"><i class="fa fa-clock-o"></i>&nbsp; Pending</p>
                                    </div>
                                </div>
                                <div class="timeline-step">
                                    <div class="timeline-content" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="2005">
                                        <div class="inner-circle"></div>
                                        <p class="h5 mt-3 mb-1">{{$num_conns_today_transit}}</p>
                                        <p class="h5 mb-0 mb-lg-0"><i class="fa fa-truck"></i>&nbsp; In Transit</p>
                                    </div>
                                </div>
                                <div class="timeline-step">
                                    <div class="timeline-content" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="2010">
                                        <div class="inner-circle"></div>
                                        <p class="h5 mt-3 mb-1">{{$num_conns_today_complete}}</p>
                                        <p class="h5 mb-0 mb-lg-0"><i class="fa fa-check"></i>&nbsp; Complete</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div id="weekly-summary" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">        
            <div class="box box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title ">
                        <i class="fa fa-calendar"></i>&nbsp; Weekly Report of Connotes &nbsp; &nbsp;      
                        <div class="input-group num-weeks" id='num-weeks' style="width:169px;">
                            <select id="num-weeks-report" class="custom-select" style="border:1px solid #d2d6de; border-radius:3px; padding:5px 10px; font-size:0.85em;">
                                <option value="1" selected>1 week report</option>
                                <option value="2">2 weeks report</option>
                                <option value="3">3 weeks report</option>
                                <option value="4">4 weeks report</option>
                                <option value="5">5 weeks report</option>
                                <option value="6">6 weeks report</option>
                                <option value="7">7 weeks report</option>
                                <option value="8">8 weeks report</option>
                                <option value="9">9 weeks report</option>
                                <option value="10">10 weeks report</option>
                                <option value="11">11 weeks report</option>
                                <option value="12">12 weeks report</option>
                            </select>                                
                        </div>
                                                    
                    </h3>
                    <div class="box-tools">                        
                        <!-- <button id="btn-create-manifest" type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalSenderPickup" disabled title="Please, select at least one connote to create a manifest!"><i class="fa fa-send"></i>&nbsp; Create Manifest</button> -->
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body" style="min-height:236px; margin-top:20px;">
                    <div class="row"> 
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9"> 
                            <div class="chart-container">
                                <canvas id="reportTrackingChart" style="width:100%;"></canvas>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3"> 
                            <div class="chart-pie-container text-center">
                                <canvas id="reportPieChart" ></canvas>
                            </div>
                        </div>                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@script

<script type="text/javascript">

    const GROUP_CONNOTE_STATUS = <?= json_encode(config('quatius.logistic.status.group',[])); ?>;

    $(function() {
        
        $( "#datepicker-1" ).datepicker({
            autoclose: true,
            format: "mm-dd-yyyy",
            immediateUpdates: true,
            todayBtn: true,
            todayHighlight: true
        }).datepicker("setDate", "0");
        
        var js_conns = <?= json_encode($conns); ?>;        

        var input_date = new Date();        
        assignDailyActivity( load_conns_date(js_conns, input_date) );
        assignWeeklyActivities( js_conns, input_date, $( "#num-weeks-report" ).val() );

        $( "#datepicker-1" ).on("change",function(e){
            assignDailyActivity( load_conns_date(js_conns, $("#datepicker-1").val()) );
            assignWeeklyActivities( js_conns, $("#datepicker-1").val(), $( "#num-weeks-report" ).val() );
        });

        $( "#num-weeks-report" ).on("change",function(e){
            assignWeeklyActivities( js_conns, $("#datepicker-1").val(), $( "#num-weeks-report" ).val() );
        });
    });

    function load_conns_date(list_conns, selected_date){
        var list_conns_selected = list_conns.filter(function (conn) {
            var d1 = new Date(selected_date);
            var d2 = new Date(conn.created_at);
            if ((d1.getYear()== d2.getYear()) && (d1.getMonth()== d2.getMonth()) && (d1.getDate()== d2.getDate()) ){            
                return conn;
            }
        });
        return list_conns_selected;
    }

    function list_conn_by_status(list_conns_selected){
        let arr_results = [];       

        arr_results [0] = list_conns_selected.filter(   function (conn){ if (GROUP_CONNOTE_STATUS["undeliverable"].includes(conn.status)) return conn; });
        arr_results [1] = list_conns_selected.filter(   function (conn){ if (conn.remark != "") return conn; });        

        // arr_results [2] = list_conns_selected.filter(   function (conn){ if ( ((conn.status == null) || (conn.remark != "")) || (GROUP_CONNOTE_STATUS["pending"].includes(conn.status)) ) return conn; });
        arr_results [2] = list_conns_selected.filter(   function (conn){ if ( ((conn.status == null) && (conn.remark == "")) || (GROUP_CONNOTE_STATUS["pending"].includes(conn.status)) ) return conn; });
        arr_results [3] = list_conns_selected.filter(   function (conn){ if (GROUP_CONNOTE_STATUS["transit"].includes(conn.status)) return conn; });
        arr_results [4] = list_conns_selected.filter(   function (conn){ if (GROUP_CONNOTE_STATUS["complete"].includes(conn.status)) return conn; });
        

        return arr_results;
    }

    function assignDailyActivity(list_conns_selected){

        arr_conn_status = list_conn_by_status(list_conns_selected);
        // console.log(arr_conn_status);

        list_conns_undeliverable= arr_conn_status[0];
        list_conns_warning      = arr_conn_status[1];        
        list_conns_pending    = arr_conn_status[2];
        list_conns_transit    = arr_conn_status[3];
        list_conns_completed    = arr_conn_status[4];
        

        
        $('#dialy-summary [class*="col-"]:nth-child(1) .timeline-steps:nth-child(1) .timeline-step .timeline-content .inner-circle + p').html(list_conns_undeliverable.length);
        $('#dialy-summary [class*="col-"]:nth-child(2) .timeline-steps:nth-child(1) .timeline-step .timeline-content .inner-circle + p').html(list_conns_warning.length);

        $('#dialy-summary .timeline-steps:nth-child(3) .timeline-step:nth-child(1) .timeline-content .inner-circle + p').html(list_conns_pending.length);
        $('#dialy-summary .timeline-steps:nth-child(3) .timeline-step:nth-child(2) .timeline-content .inner-circle + p').html(list_conns_transit.length);
        $('#dialy-summary .timeline-steps:nth-child(3) .timeline-step:nth-child(3) .timeline-content .inner-circle + p').html(list_conns_completed.length);

       
        if (list_conns_warning.length > 0){  
            $('#dialy-summary .box-tools span.badge.bg-status-warning').removeClass('hidden').html(list_conns_warning.length); 
        }else{
            $('#dialy-summary .box-tools span.badge.bg-status-warning').addClass('hidden').html(list_conns_warning.length); 
        }        
        if (list_conns_undeliverable.length > 0){  
            $('#dialy-summary .box-tools span.badge.bg-status-cancelled').removeClass('hidden').html(list_conns_undeliverable.length); 
        }else{
            $('#dialy-summary .box-tools span.badge.bg-status-cancelled').addClass('hidden').html(list_conns_undeliverable.length); 
        }
    }


    function assignWeeklyActivities(js_conns, todayDate, num_week = 1){        

        //Draw bar chart for weekly report
        //------------------------------------------
        // var barColors = ["#7ce895","#00afef","#28a745","#07bfff","#007bff","#ffa007", "#a81ae9","#dc3545"];
        // var barLabels = ["Created","Ready-to-Print","Submitted","Picked-up","Delivered","Warning","Delayed","Cancelled"];
        const barColors = ["#dc3545", "#ffa007", "#a4a4a4", "#28a745",  "#007bff"];
        // const barLabels = ["Submitted","Picked-up","Delivered (Line bar)","Warning","Delayed","Cancelled"];
        const barLabels = ["Undeliverable", "Warning", "Pending","In Transit","Complete (Line bar)"];

        const NUM_DAYS = 7 * num_week;

        var xValues = [];
        for (let i=0; i< NUM_DAYS; i++){
            xValues[i] = moment(todayDate).subtract(NUM_DAYS - (1+i), 'day').format('MM/D/YYYY');           
        }
        
        var valuesByStatus = [];        
        for (let i=0; i< NUM_DAYS; i++){
            valuesByStatus[i] = list_conn_by_status( load_conns_date(js_conns, xValues[i]) ).map( function(ele){ return ele.length; } );
        }    
        var yValues = valuesByStatus[0].map((_, colIndex) => valuesByStatus.map(row => row[colIndex])); //transpose a matrix
        // console.log(valuesByStatus);
        // console.log(yValues);
        
        var dataSetsss = [];
        for (let i=0; i< yValues.length; i++){
            if (i != 4){
                dataSetsss[i] = {
                    type: 'bar',
                    label: barLabels[i],
                    backgroundColor: barColors[i]+"",
                    borderColor: barColors[i],
                    borderWidth: 2,
                    // hoverBackgroundColor: barColors[i]+"c0",
                    data: yValues[i],
                    fill: false,
                    order: i+10
                }
            }else{
                dataSetsss[i] = {
                    type: 'line',
                    label: barLabels[i],
                    backgroundColor: barColors[i]+"",
                    borderColor: barColors[i],
                    borderWidth: 3,
                    // hoverBackgroundColor: barColors[i]+"c0",
                    data: yValues[i],
                    fill: false,
                    order: 1
                }
            }
        }

        // console.log(dataSetsss);
        $('.chart-container').html('<canvas id="reportTrackingChart" style="width:100%;"></canvas>');
        
        new Chart("reportTrackingChart", {
            type: "bar",
            data: {
                labels: xValues,
                datasets: dataSetsss,
            },
            options: {
                legend: {
                    position: "top",
                    align: "middle",
                    labels: {
                        boxWidth: 30,
                        boxHeight: 10,
                        padding: 25
                    }
                },
                title: {
                    display: false,
                    // text: "World Wine Production 2018"
                },
                responsive: true,
                scales: {
                    yAxes: [{
                        ticks: {
                            stepSize: 5,
                            beginAtZero: true
                        }
                    }]
                },                
                tooltips: {
                    enabled: true
                },
                hover: {
                    animationDuration: 1
                },
                // onClick(e) {
                //     const activePoints = myChart.getElementsAtEventForMode(e, 'nearest', {
                //         intersect: true
                //     }, false)
                //     const [{
                //         index
                //     }] = activePoints;
                //     // console.log(dataSetsss[0].data[index]);
                //     console.log(activePoints);
                // },
                maintainAspectRatio: false,
            },
            plugins: [{
                beforeInit: function(chart, options) {
                    chart.legend.afterFit = function() {
                        this.height = this.height + 20;
                    };
                }
            }] 
        });
        

        // //Draw pie chart for weekly report
        // //------------------------------------------
        pieValues = [];
        for (let i=0; i< yValues.length; i++){
            pieValues[i] = yValues[i].reduce(function(a,b){ return a + b; });
        }
        var totalSum = pieValues.reduce(function(a,b){ return a + b; });
        for (let i=0; i< pieValues.length; i++){
            pieValues[i] = (pieValues[i] * 100 / totalSum).toFixed(2) ;
        }
        
        // console.log(yValues);
        // console.log(pieValues);

        const datas = {
            labels: barLabels,
            datasets: [{
                data: pieValues,
                backgroundColor: barColors,
                hoverOffset: 4
            }]
        };

        var options = {
            legend: {                    
                display: false
            },
            tooltips: {
                enabled: true
            },
            // plugins: {
            //     datalabels: {
            //     formatter: (value, ctx) => {

            //         // console.log(ctx);
            //         // let sum = ctx.dataset._meta[0].total;
            //         // let percentage = (value * 100 / sum).toFixed(2) + "%";
            //         // return percentage;
            //         return value + "==";

            //     },
            //     color: '#fff',
            //     }
            // }
        };

        // console.log(dataSetsss);
        $('.chart-pie-container').html('<canvas id="reportPieChart"></canvas>');

        var ctx = document.getElementById("reportPieChart").getContext('2d');
        // console.log(ctx);
        // console.log(ctx.canvas);
        // console.log(ctx.canvas.dataset);

        var myChart = new Chart(ctx, {
            type: 'pie',
            data: datas,
            options: options
        });

        // new Chart("reportPieChart", {
        //     type: "pie",
        //     data: datas,
        //     options: {
        //         legend: {                    
        //             display: false
        //         },
        //     //     title: {
        //     //         display: false,
        //     //         text: "World Wine Production 2018"
        //     //     },
        //     //     scales: {
        //     //         yAxes: [{
        //     //             ticks: {
        //     //                 stepSize: 5,
        //     //                 beginAtZero: true
        //     //             }
        //     //         }]
        //     //     },
        //         tooltips: {
        //             enabled: true
        //         },
        //         // hover: {
        //         //     animationDuration: 1
        //         // },
        //         // maintainAspectRatio: false,
        //     },
        //     plugins: [{
        //         datalabels: {
        //             formatter: (value, ctx) => {
        //                 let sum = 0;
        //                 let dataArr = "reportPieChart".chart.data.datasets[0].data;
        //                 dataArr.map(data => { sum += data; });
        //                 let percentage = (value*100 / sum).toFixed(2)+"%";
        //                 return percentage;
        //             },
        //             color: '#fff',
        //         }
        //     }] 
        // });
    }

</script>
@endscript
