<style>
	label{
		color:#fff
	}
</style>

<div class="">
<div class="container">
	<div class="col-md-6 col-md-offset-3">
		<div class="row">
			<div class="panel panel-default">
			<div class="panel-body custom_box">
				<div class="col-md-12 col-xs-12">					
					<div class="row">
						<div class="col-xs-12">
							<div class="panel-logo">
								<a href="home"><img src="../../images/quatius-logo.png"></a>
							</div>
						</div>
						<div class="col-xs-12">
							<div class="row">
								<h3 class="login-box-msg" >{{trans('Account::invite.account-registraion-heading')}}</h3>
							</div>
						</div>
						<div class="col-xs-12" style="margin-top: -50px;">
							<h2 class="text-center" style="color: #fff;">{{$company}} <br><br></h2>
							<div class="row">
								
								{!!Form::vertical_open()
			                    ->id('reset')
			                    ->method('POST')
			                    ->action('/account/invitation')!!}
			                    {!! csrf_field() !!}
			                    {!! Form::hidden('token')->value($token) !!}
			                    {!! Form::hidden(config('user.params.type'))!!}
								<div class="row">
									<div class="col-sm-6">
								{!! Form::text('first_name')->required()
			                    -> placeholder(trans('user::user.user.placeholder.first_name'))!!}
									</div>
									<div class="col-sm-6">
								{!! Form::text('last_name')
			                    -> placeholder(trans('user::user.user.placeholder.last_name'))!!}
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
									{!! Form::tel('phone')->required()
									-> placeholder(trans('user::user.user.placeholder.phone'))!!}
									</div>
									<div class="col-sm-6">
										{!! Form::email('email')->disabled()->required()
											-> placeholder(trans('Address::fields.placeholder.email'))!!}
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
									{!! Form::password('password')->required()
									-> placeholder(trans('user::user.user.placeholder.password'))!!}
									</div>
									<div class="col-sm-6">
									{!! Form::password('password_confirmation')->required()
									-> placeholder(trans('user::user.user.placeholder.password_confirmation'))!!}
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
									{!! Form::text('address_1','address')->required()
									-> placeholder(trans('Address::fields.placeholder.address_1'))!!}
									</div>
								</div>
								<div class="row">
									<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
									{!! Form::text('city','City/Suburb')->required()
									-> placeholder(trans('Address::fields.placeholder.city'))!!}
									</div>	 
									<div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
									{!! Form::select('state')->required()
									-> options(trans('Address::fields.options.states'))
									-> placeholder(trans('Address::fields.placeholder.state'))!!}
									</div>
									<div class="col-xs-6 col-sm-6 col-md-2 col-lg-2">
									{!! Form::text('postcode')->required()
									-> placeholder(trans('Address::fields.placeholder.postcode'))!!}
									</div>  
									<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
									{!! Form::select('country')->required()   
									-> options(trans('Address::fields.options.countries'))
									-> placeholder(trans('Address::fields.placeholder.country'))->value('AU')!!}
									</div>	     
								</div>
					   			<button type="submit" class="hvr-sweep-to-right btn btn-primary btn-block ">Submit</button>
								{!! Form::hidden('email')!!}
			                    {!! Form::close() !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
</div>
</div>