@extends('admin::curd.index')
@section('heading')
<i class="fa fa-users"></i> Accounts <small> Manage Accounts</small>
@stop
@section('title')
Manage Accounts
@stop
@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{!! URL::to('admin') !!}"><i class="fa fa-dashboard"></i> {!! trans('cms.home') !!} </a></li>
    <li class="active">Manage Accounts</li>
</ol>
@stop
@section('entry')
<div class="box box-warning" id='entry-account'>
    @include('Account::admin.new')
</div>
@stop
@section('tools')
<h4>
<a href='#' class="label label-primary filter-role" data-role=''>All</a>

</h4>
@stop
@section('content')
<table id="main-list" class="table table-striped table-bordered">
    <thead>
        <th>Name</th>
        <th>Email</th>
        <th>Code</th>
        <th>Users</th>
        <th>Date</th>
    </thead>
</table>
@stop
@section('script')
<script type="text/javascript">
var oTable;
$(document).ready(function(){
    //$('#entry-account').load('{{URL::to('admin/accounts/0')}}');
    oTable = $('#main-list').DataTable( {
        "ajax": '{{ URL::to("/admin/accounts") }}',
        "columns": [
        { "data": "name" },
        { "data": "email" },
        { "data": "account_code" },
        { "data": "users_count" },
        { "data": "created_at" }],
        "columnDefs": [
            {
                "render": function ( data, type, row ) {
                    return data +(row.pending_users=="0"?"":" (+"+row.pending_users+")");
                },
                "searchable": false,
                "targets": 3
            }
        ],
        "userLength": 50
    });
    $('#main-list tbody').on( 'click', 'tr', function () {
        $(this).toggleClass("selected").siblings(".selected").removeClass("selected");
        var d = $('#main-list').DataTable().row( this ).data();
        $('#entry-account').load('{{URL::to("admin/accounts")}}' + '/' + d.id);
    });

    // $('.filter-role').on( 'click', function (e) {
    //     role = $( this ).data( "role" );

    //     oTable.ajax.url('{!! URL::to('/admin/user/user?role=') !!}' + role).load();
    //     e.preventDefault();
    // });
});

function sendInvite($id){
    var sel = '<input class="class="form-control" placeholder="Email Address" style="display:block" id="email-invite" type="email" name="email" />';
    sel += '{!!Form::select("role-type","")->placeholder("Select Role")->options(array_combine(array_keys(config("quatius.account.role-types",[])),array_keys(config("quatius.account.role-types",[]))))->value("admin")!!}';
    swal({confirmButtonText: 'Send Invite', showCancelButton: true, html:true, title:'<i>Enter User Email</i>', text:sel,
        closeOnConfirm: false
        },function(isConfirm){
        if (isConfirm)
        {
            $.post("{{URL::to('admin/accounts')}}/"+$id+"/send-invite", {email:$('#email-invite').val(),role:$('#role-type').val()}, function(jsonData){
                swal("{!! trans('Account::invite.sent-success')!!}", jsonData.message, "success");
            },'json')
            .fail(function(xhr, status, error) {
                swal("{!! trans('Account::invite.sent-failed')!!}", xhr.responseJSON.message, (xhr.status >= 300 && xhr.status <=400)?"warning":"error");
            });
        }
    });
}

</script>
@stop
@section('style')
@stop
