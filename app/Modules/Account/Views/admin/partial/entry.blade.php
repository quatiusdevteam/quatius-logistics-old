<div class="tab-pane active" id="details">
    <div class="row">
        <div class='col-md-12 col-sm-12'>
            {!! Form::text('name')
            -> label('Account name')
            -> placeholder('Account name') !!}
        </div>
        <div class='col-md-6 col-sm-12'>
            {!! Form::email('email')
            -> placeholder('Email') !!}
        </div>
        <div class='col-md-6 col-sm-12'>
            {!! Form::text('phone_1','Phone')!!}
        </div>
        
        <div class='col-md-6 col-sm-12'>
            {!! Form::text('account_code')!!}
        </div>
        <div class='col-md-6 col-sm-12'>
            {!! Form::text('credit_limit')!!}
        </div>
    </div>
</div>
<div class="tab-pane " id="users">
    <div class="row">
        <div class="col-xs-12">
                <table id="account-users" class="table table-striped">
                    <thead>
                        <tr><th>Name</th><th>Email</th><th>Destination</th><th>Status</th><th>Date</th></tr>
                    </thead>
                    <tbody>
                        @foreach($account->getParams('invites',[]) as $invite)
                            <tr><td></td><td>{{$invite["email"]}}</td><td>{{$invite["designation"]}}</td><td>Invited</td><td>{{$invite["created_at"]}}</td></tr>
                        @endforeach
                        @foreach ($account->users as $user)
                        <tr><td>{{$user->name}}</td><td>{{$user->email}}</td><td>{{$user->designation}}</td><td>{{$user->status}}</td><td>{{$user->created_at}}</td></tr>
                        @endforeach
                    </tbody>
                </table>
        </div>
    </div>
</div>
<script>
    $(document).ready(()=>{
        $('[href="#users"]').on('shown.bs.tab', function (e) {
            if (!$('#account-users').hasClass('dataTable'))
                $('#account-users').DataTable({"pageLength": 5});
        });
    });

</script>