<div class="box-header with-border">
    <h3 class="box-title"> {{ trans('cms.edit') }} Account - [{!!$account->name!!}] </h3>
    <div class="box-tools pull-right">
        <button type="button" class="btn btn-primary btn-sm" data-action='UPDATE' data-form='#edit-account'  data-load-to='#entry-account' data-datatable='#main-list'><i class="fa fa-floppy-o"></i> {{ trans('cms.save') }}</button>
        <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#entry-account' data-href='{{Trans::to("admin/accounts/".$account->getRouteKey())}}'><i class="fa fa-times-circle"></i> {{ trans('cms.cancel') }}</button>
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body" >
    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#details" data-toggle="tab">Details</a></li>
            <li><a href="#users" data-toggle="tab">Users</a></li>
        </ul>
        {!!Form::vertical_open()
        ->id('edit-account')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->action(URL::to('admin/accounts/'. $account->getRouteKey()))!!}
        <div class="tab-content">
            @include('Account::admin.partial.entry')
        </div>
        {!!Form::close()!!}
    </div>
</div>
<div class="box-footer" >
    &nbsp;
</div>
