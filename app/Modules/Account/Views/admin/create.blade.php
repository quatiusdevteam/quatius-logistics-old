<div class="box-header with-border">
    <h3 class="box-title"> {{ trans('cms.new') }} Account </h3>
    <div class="box-tools pull-right">
        <button type="button" class="btn btn-primary btn-sm" data-action='CREATE' data-form='#create-account'  data-load-to='#entry-account' data-datatable='#main-list'><i class="fa fa-floppy-o"></i> {{ trans('cms.save') }}</button>
        <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#entry-account' data-href='{{Trans::to("admin/accounts/new")}}'><i class="fa fa-times-circle"></i> {{ trans('cms.cancel') }}</button>
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body" >
    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#details" data-toggle="tab">Details</a></li>
            <li><a href="#users" data-toggle="tab">Users</a></li>
        </ul>
        {!!Form::vertical_open()
        ->id('create-account')
        ->method('POST')
        ->enctype('multipart/form-data')
        ->action(URL::to('admin/accounts/'. $account->getRouteKey()))!!}
        <div class="tab-content">
            @include('Account::admin.partial.entry')
        </div>
        {!!Form::close()!!}
    </div>
</div>
<div class="box-footer" >
    &nbsp;
</div>
