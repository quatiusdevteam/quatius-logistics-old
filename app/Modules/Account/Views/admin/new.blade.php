<div class="box-header with-border">
    <h3 class="box-title"> Account </h3>
    <div class="box-tools pull-right">
        <button type="button" class="btn btn-primary btn-sm" id="btn-new-account"><i class="fa fa-plus-circle"></i> {{ trans('cms.new') }} </button>
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body" style="min-height:100px">
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <h1 class="text-center">
            <small>
            <button type="button" class="btn btn-app" data-toggle="tooltip" data-placement="top" title=""  id="btn-new-account-icn">
            
            <i class="fa fa-plus-circle  fa-3x"></i>
            {{ trans('cms.create') }} Account
            </button>
            <br>{{ trans('user::user.user.text.preview') }}
            </small>
            </h1>
        </div>
    </div>
</div>
<div class="box-footer" >
    &nbsp;
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('#btn-new-account, #btn-new-account-icn').click(function(){
        $('#entry-account').load('{{URL::to("admin/accounts/create")}}');
    });
});
</script>
