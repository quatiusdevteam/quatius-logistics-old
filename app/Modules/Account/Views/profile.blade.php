@extends('admin::curd.index')
@section('heading')
<i class="fa fa-user"></i> Profile <small> Details</small>
@stop

@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{!! URL::to('home') !!}"><i class="fa fa-dashboard"></i> {!! trans('cms.home') !!} </a></li>
    <li class="active">Profile</li>
</ol>
@stop
@section('entry')
<div class="box box-warning " id='entry-user'>
	<div class="box-header  with-border">
		<h4 class="box-title">Details</h4>
		<div class="box-tools pull-right">
			<button type="button" data-toggle="modal" data-target="#passwordModal" class="btn btn-default btn-sm">Change Password</button>
			<!-- data-action="EDIT"  data-load-to='#entry-user' data-href='{{ trans_url("account/".$user->getRouteKey()."/edit")}}' -->
			<button type="button" class="btn btn-primary btn-sm" ><i class="fa fa-pencil-square"></i> {{ trans('cms.edit') }}</button>
		</div>
	</div>
	<div class="box-body">
		Name : {{$user->name}} <br>
		Email : {{$user->email}} <br>
		Role:  {{$user->designation}} <br>
	</div>
	<!-- <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		<button type="button" class="btn btn-primary btn-sm" data-action='UPDATE' data-dismiss="modal" data-form='#create-address' data-datatable='#dt-contacts'><i class="fa fa-floppy-o"></i> {{ trans('cms.save') }}</button>

	</div> -->
	
	<div id="passwordModal" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Change Password</h4>
			</div>
			{!!Form::horizontal_open()
						->action(url('account/edit/password'))
						->attributes(["onsubmit"=>"return changePassword(this)"])
						->class('form-horizontal')
						->method('POST')!!}
			<div class="modal-body">
				@include('Account::partials/password')
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary">Apply</button>
			</div>
			{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@stop

@section('title')
	Activity Logs
@stop

@section('content')

@stop
@section('script')
<script type="text/javascript">
$(document).ready(function() {
    $('#passwordModal').on('show.bs.modal', function(){
		$(this).find('[type="password"]').val('');
        
    });
});

function changePassword(formItm){
    $.post($(formItm).attr( 'action' ),
    	$(formItm).serialize(),
    	function(result){
    		$('#passwordModal').modal('hide');
		}
	);
	return false;
}

</script>
@stop
@section('style')
@stop



