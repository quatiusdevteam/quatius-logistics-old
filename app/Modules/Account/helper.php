<?php
 /**
 *	Account Helper  
 */

if (!function_exists('doBasicLogin'))
{
    function doBasicLogin($user){
        if (!$user) return;

        // if (config('cybercast.basic-auth.assign_role_id', 0) > 0){
        //     $role = \Litepie\User\Models\Role::find(config('cybercast.basic-auth.assign_role_id', 0));
        //     if ($role)
        //         $user->roles->add($role);
        // }
        //$user
        //$user->permissions = config('cybercast.basic-auth.user-permissions', []);

        //if (!$user->hasPermission('access.api.frontend')) return;
        
        // foreach (config('cybercast.basic-auth.configs') as $setting=>$val){
        //     config()->set($setting, $val);
        // }

        Illuminate\Support\Facades\Auth::login($user);
        return $user;
    }
}
