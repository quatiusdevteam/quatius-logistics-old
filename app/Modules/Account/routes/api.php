<?php

Route::group(['prefix'=>'api','module' => 'Account', 'middleware' => ['auth:api', 'bindings'], 'namespace' => 'App\Modules\Account\Controllers'], function() {
    Route::get('account', 'AccountController@show');
});