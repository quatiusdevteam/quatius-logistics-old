<?php

Route::group(array('module' => 'Account','middleware' => ['web', 'auth'], 'namespace' => 'App\Modules\Account\Controllers'), function() {
    Route::get('home', 'AccountController@index');
    Route::get('selfmanage', 'AccountController@index');

    Route::post('account/send-invite', 'AccountController@invitationSend');
	Route::get('account/users', 'AccountController@users');
    Route::get('account', 'AccountController@show');
});	

Route::group(array('prefix'=>'admin','module' => 'Account', 'namespace' => 'App\Modules\Account\Controllers'), function() {
    
	Route::post('accounts/{account_id}/send-invite', 'AdminAccountController@invitationSend');
	Route::get('accounts/new', function(){return view('Account::admin.new');});
    Route::resource('accounts', 'AdminAccountController', ['parameters' =>['accounts' => 'account_id']]);
});	