<?php

return [
    'account-registraion-heading'=>"Account Registration",
    'account-registraion'=>"Account Registration",
    'account-already-registered'=>"Account has already been registered.",
    'email-already-registered'=>"The email(:email) has already been registered.",
    'account-invite-expired'=>"The email(:email) invitation has been cancelled. Please contact your account manager",
    'account-activated'=>"Your account is activated.",
    'sent-to'=>"Invitation send to :email",
    'sent-success'=>'Invitation Sent!',
    'sent-failed'=>'Invitation failed!',
];
