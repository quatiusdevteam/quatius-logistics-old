<?php namespace App\Modules\Home\Controllers;

use App\Http\Controllers\PublicApiController;
use Illuminate\Http\Request;

class HomeApiController extends PublicApiController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		return response()->json([]);
	}
}
