<?php 

return [
    'POSTCODE_ERROR' => "Postcode and Suburb do not match.",
    'STATE_NOT_FOUND' => "State not found",
    'COUNTRY_NOT_FOUND' => "Country not found"
];
