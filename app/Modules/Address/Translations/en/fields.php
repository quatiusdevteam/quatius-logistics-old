<?php 

return [
	'label'       => [
		'date'			=> 'Date',
		'address'		=> 'Address',
		'company_name'	=> 'Company name',
		'contact_name'	=> 'Contact name',
		'title'			=> 'Title',
		'first_name'	=> 'First name',
		'middle_name'	=> 'Middle name',
		'last_name'		=> 'Last name',
		'phone'			=> 'Phone',
		'email'			=> 'Email',
		'address_1'		=> 'Address 1',
		'address_2'		=> 'Address 2',
		'city'			=> 'City',
		'state'			=> 'State',
		'postcode'		=> 'Postcode',
		'country'		=> 'Country',
	 ],
	'placeholder' => [
		'address'		=> 'Enter address',
		'company_name'	=> 'Enter company name',
		'first_name'	=> 'Enter first name',
		'middle_name'	=> 'Enter Middle name',
		'last_name'		=> 'Enter last name',
		'phone'			=> 'Enter phone number',
		'email'			=> 'Enter email address',
		'address_1'		=> 'Enter address 1',
		'address_2'		=> 'Enter address 2',
		'city'			=> 'Enter City',
		'state'			=> 'Select state',
		'postcode'		=> 'Postcode',
		'country'		=> 'Select country',
	 ],
	'options'     => [
		'states'		=> [
			'VIC' 	=> 'Victoria',
			'ACT' 	=> 'Australian Capital Territory',
			'NSW' 	=> 'New South Wales',
			'NT' 	=> 'Northern Territory',
			'QLD' 	=> 'Queensland',
			'SA' 	=> 'South Australia',
			'TAS' 	=> 'Tasmania',
			'WA' 	=> 'Western Australia',
							],
		'countries'		=> [
			'AU'	=> 'Australia',
		],
	]	
];
