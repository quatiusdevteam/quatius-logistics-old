<?php

namespace App\Modules\Address\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB;
use Form;
use Quatius\Address\Models\Address;
use Quatius\Address\Repositories\Presenters\AddressIndexPresenter;

class AddressController extends Controller
{
    private $repo;

    public function __construct(\Quatius\Address\Repositories\AddressRepository $repo)
    {
        $this->setupTheme(config('theme.map.registered.theme'), config('theme.map.registered.layout'));
        $this->repo = $repo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            $requestData = $request->all();
            $query = app('account')->detail()->addresses();
            return response()->json(dataTableQuery($requestData, $query));
        }

        return $this->theme->of('Address::index')->render();
    }

    public function all(Request $request)
    {
        //if ($request->wantsJson()) {
            $this->repo->setPresenter(AddressIndexPresenter::class);
            return response()->json($this->repo->parserResult(app('account')->detail()->addresses()->get()));
        //}

        //return $this->theme->of('Account::address.index')->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $address = $this->repo->newInstance([]);
        Form::populate($address);
        return view('Address::create', compact($address));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $addr = $this->repo->where('first_name', '=', $request->input('first_name'))
        ->where('last_name', '=', $request->input('last_name'))
        ->where('company', '=', $request->input('company'))
        ->where('phone_1', '=', $request->input('phone_1'))
        ->where('email', '=', $request->input('email'))
        ->where('address_1', '=', $request->input('address_1'))
        ->where('address_2', '=', $request->input('address_2'))
        ->where('city', '=', $request->input('city'))
        ->where('state', '=', $request->input('state'))
        ->where('postcode', '=', $request->input('postcode'))
        ->first();

        if ($addr === null) {        
            
            $data = $request->all();
            $errorInputs = [];
            $data = $this->repo->formatSimplify($data, function($inputField, $msg, $val) use(&$errorInputs){
                $errorInputs[$inputField] = $msg;
            });
            if ($request->wantsJson()) {
                if ($errorInputs){
                    $address = $data;
                    Form::populate($data);
                    
                    Form::withErrors(new \Illuminate\Support\MessageBag(array_map(function($val){return trans($val);}, $errorInputs)));
                    $viewForm = view('Address::create', ["address" => new Address($address)])->render();

                    return response()->json([
                        "view"=>$viewForm, 
                        'message'  => "Saving Address Failed",
                        'code'     => 403
                    ], 303);
                }

                $contact = app('account')->detail()->addresses()->save(new Address($data));
                return response()->json([
                    //"data"=>$this->repo->parserResult($connote),
                    'message'  => "Address Updated",
                    'code'     => 204
                ], 201);
            }
            
            $contact = app('account')->detail()->addresses()->save(new Address($data));
            return redirect()->back();

        }else{
            return response()->json([
                "data" => $addr,
                'message'  => "This profile has already exist.",
                'code'     => 404
            ], 404);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $address = $this->repo->findOrFail($id);

        if ($request->wantsJson()) {
            return response()->json(['data'=>$address->transform()]);
        }

        Form::populate($address);
        return view('Address::show', ["address" => $address]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $address = $this->repo->findOrFail($id);
        Form::populate($address);
        return view('Address::edit', ["address" => $address]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->repo->setPresenter(null);

        $data = $request->all();
        $errorInputs = [];
        $data = $this->repo->formatSimplify($data, function($inputField, $msg, $val) use(&$errorInputs){
            $errorInputs[$inputField] = $msg;
        });
        
        if ($errorInputs){
            Form::populate($data);
            
            Form::withErrors(new \Illuminate\Support\MessageBag(array_map(function($val){return trans($val);}, $errorInputs)));
            $address = $this->repo->findorFail($id);
            $viewForm = view('Address::edit', ["address" => $address])->render();

            return response()->json([
                "view"=>$viewForm, 
                'message'  => "Saving Address Failed",
                'code'     => 403
            ], 303);
        }
        $address = $this->repo->update($request->all(), $id);
        return response()->json([
            //"data"=>$this->repo->parserResult($connote),
            'message'  => "Address Updated",
            'code'     => 204
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        app('address')->delete($id);
        return response()->json([
            "success"=>true
        ]);
    }

        /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addressPostal(Request $request, $code = 'AU')
    {
        $data = app('address')->getPostalCodes($code);

        return response()->json([
            "success"=>true,
            "data"=>$data
        ]);
    }
}
