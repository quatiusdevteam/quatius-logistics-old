<?php

namespace Quatius\Address\Providers;

use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\ServiceProvider;
use Route;

class AddressServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
	 public function boot()
    {	
        Route::bind('address_id',function($value) {
	        return hashids_decode($value)?:abort(404,'Route Key Invalid');
	    });
    }
    
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        
        // $this->loadMigrationsFrom(__DIR__.'/../Databases/migrations');
        // $this->app->make(Factory::class)->load(__DIR__.'/../Databases/factories');

        $this->app->singleton('Quatius\Address\Repositories\AddressRepository', 'Quatius\Address\Repositories\Handlers\AddressHandler');
        $this->app->bind('address', 'Quatius\Address\Repositories\AddressRepository');
    }
}
