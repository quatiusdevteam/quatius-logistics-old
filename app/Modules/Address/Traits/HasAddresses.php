<?php
namespace Quatius\Address\Traits;

use Quatius\Address\Models\Address;

trait HasAddresses
{
    public function addresses()
	{
		return $this->morphMany(Address::class, "link");
	}

    public function getAddressTypeRelation($address_type)
	{
        if (isset($this->getRelations()[$address_type]))
            return $this->getRelations()[$address_type];

        $relation = $this->addresses->where('address_type',$address_type)->first();
        $this->setRelation($address_type, $relation?:new Address());
        return $this->getRelations()[$address_type];
	}
}
