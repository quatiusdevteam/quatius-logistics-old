<section class="content">
    <div class="box box-warning">
        <div class="box-header with-border">
            <h3 class="box-title">Manage Addresses</h3>
            <div class="box-tools pull-right">
                <a class="btn btn-primary btn-sm" data-toggle="modal" href="{{url('address/create')}}" data-target="#remoteDialogCreate"><i class="fa fa-plus-circle"></i>&nbsp; New Address</a>
            </div>
        </div>
        <div class="box-body" style="min-height:550px">

        <!-- empty dialog -->
            <div id="remoteDialogCreate" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog  modal-lg" role="document">
                <div class="modal-content">
                    
                </div>
            </div>
            </div>
            <div id="remoteDialogEdit" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog  modal-lg" role="document">
                <div class="modal-content">
                    
                </div>
            </div>
            </div>
        <!-- end empty dialog -->
            <table id="dt-contacts" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Company</th>    
                        <th>Name</th>   
                        <th>Middle Name</th>   
                        <th>Last Name</th>                        
                        <th>Phone(s)</th>
                        <th>Phone 2</th>
                        <th>Address(es)</th>
                        <th>Address 2</th>
                        <th>City</th>
                        <th>State</th>
                        <th>Postcode</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>                              
                </tbody> 
            </table>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
         $('#dt-contacts').DataTable({
            "columns": [                
                { "data": "company" },              
                { "data": "first_name" },
                { "data": "middle_name" },
                { "data": "last_name" },
                { "data": "phone_1" },
                { "data": "phone_2" },
                { "data": "address_1" },
                { "data": "address_2" },
                { "data": "city" },              
                { "data": "state" },              
                { "data": "postcode" },              
                { "data": "id" },
            ],

            serverSide: true,
            processing: true,

            ajax: {
                url: "{{ URL::to('address') }}",
                "dataType": "json",
                "type": "GET",
                "data":{
                    extra_fields : [
                        "id",
                    ],
                }
                
            },

            "pageLength": 10,
            "columnDefs": [
                // {"className": "align-top text-center", "targets": "_all"},
                {"className": "align-top text-center", "targets": [0,1,3,4,5,11]},
                {"className": "align-top text-right", "targets": [6,9]},
                {
                    "render": function ( data, type, row ) {
                        return data +' '+ row.middle_name+' '+ row.last_name;
                    },
                    "targets": 1   
                },
                {
                    "render": function ( data, type, row ) {
                        return (data != ""? '<i class="fa fa-phone color-1"></i>&nbsp; <a href="tel:'+ data +'">'+ data +'</a>' : "") + (row.phone_2 != ""? '</br><i class="fa fa-phone color-1"></i>&nbsp; <a href="tel:'+ row.phone_2 +'">'+ row.phone_2 +'</a>' : "");
                    },
                    "targets": 4   
                },
                {
                    "render": function ( data, type, row ) {
                        return (data != ""? '<i class="fa fa-address color-0"></i>'+ data : "") + (row.address_2 != ""? '</br>'+ row.address_2 : "");
                    },
                    "targets":   6
                },
                {
                    "render": function ( data, type, row ) {
                        // return (data);
                        let str = "<a target='_self' class='btn-link'><i class='fa fa-eye'></i>&nbsp; View Detail</a>";
                            // str += " &nbsp;  &nbsp; <a target='_self'><i class='fa fa-trash'></i>&nbsp; Delete</a>";
                            // str += " | <a target='_self' href='#'><i class='fa fa-edit'></i></a> ;";

                        return str;
                    },
                    "targets":   11
                },
                { "visible": false,  "targets": [ 2,3,5,7 ] }                
            ],
            
            "createdRow": function ( row, data, index ) {
                // if ( (data.address_type).includes("Sender")) {
                //     $('td', row).eq(0).addClass('text-success');  
                //     $(row).addClass('status-submitted');
                //     // $('td', row).addClass('status-submitted');  
                // }else if ( (data.address_type).includes("Pick-up")) {
                //     $('td', row).eq(0).addClass('text-primary'); 
                //     $(row).addClass('status-picked-up');
                // }else if ( (data.address_type).includes("Receiver")) {
                //     $('td', row).eq(0).addClass('text-warning'); 
                //     $(row).addClass('status-delivered');
                // }
            }
        });
    
        $('#dt-contacts tbody').on( 'click', 'tr', function () {
        
            $(this).toggleClass("selected").siblings(".selected").removeClass("selected");
            var d = $('#dt-contacts').DataTable().row( this ).data();
            $('#remoteDialogEdit').modal('show').find('.modal-content').load('{{URL::to("address")}}' + '/' + d.id);
        });
    });

    function saveAddress(frm){

        event.preventDefault();

        var jFrm = $(frm);
        $.post(jFrm.attr('action'), 
            jFrm.serialize(), function( data ) {                        
                        
            }, 'json')
            .done(function (data) {
                $('#dt-contacts').DataTable().ajax.reload( null, false );
                $('#remoteDialogCreate').modal('hide');
                $('#remoteDialogEdit').modal('hide');
            })
            .fail(function (jqXHR, textStatus, errorThrown) { 
                $('#dt-contacts').DataTable().ajax.reload( null, false );
                if (jqXHR.responseJSON.view && jFrm.parents('.modal-content').length){
                    console.log(jqXHR.responseJSON.view);
                    jFrm.parents('.modal-content').html(jqXHR.responseJSON.view);
                }
            });

        return false;        
    }
</script>