{!!Form::open()
        ->attributes(['onsubmit'=>'saveAddress(this);'])
        ->class("form-horizontal")
        ->id('edit-address')
        ->method('POST')
        ->action(URL::to('address/'.$address->getRouteKey()))!!}
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">Edit Address</h4>
</div>
<div class="modal-body">
    @include('Address::partials.entry')
    <input type="hidden" name="_method" value="patch"/>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" data-action="DELETE" data-datatable='#dt-contacts' data-href="{{ trans_url('address/'.$address->getRouteKey())}}" ><i class="fa fa-trash"></i>&nbsp; {{ trans('cms.delete') }}</button>
    
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp; Save</button>
</div>
{!! Form::close() !!}
