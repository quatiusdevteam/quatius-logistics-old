{!!Form::open()
        ->attributes(['onsubmit'=>'saveAddress(this);'])
        ->class("form-horizontal")
        ->id('create-address')
        ->method('POST')
        ->action(URL::to('address'))!!}
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">Create New Address</h4>
</div>
<div class="modal-body">
    @include('Address::partials.entry')
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Close</button>
    <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp; {{ trans('cms.save') }}</button>

</div>
{!! Form::close() !!}
