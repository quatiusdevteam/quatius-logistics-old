
<div class="row">
    {{--<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
    {!! Form::select('address_type')->required()           
    -> options(["sender"=>"Sender","pickup"=>"Pickup","warehouse"=>"Warehouse","reciever"=>"Reciever"])
    -> label("Type")
    -> placeholder("Select Address Type")!!}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">--}}

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    {!! Form::text('nick')
    -> label("Nick Name")
    -> placeholder("Address Nick Name")!!}
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    {!! Form::text('company')
    -> label(trans('Address::fields.label.company_name'))
    -> placeholder(trans('Address::fields.placeholder.company_name'))!!}
    </div>
</div>    
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
    {!! Form::text('first_name')->required()
    -> label(trans('Address::fields.label.first_name'))
    -> placeholder(trans('Address::fields.placeholder.first_name'))!!}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
    {!! Form::text('last_name')
    -> label(trans('Address::fields.label.last_name'))
    -> placeholder(trans('Address::fields.placeholder.last_name'))!!}
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
    {!! Form::text('phone_1')->required()
    -> label(trans('Address::fields.label.phone'))
    -> placeholder(trans('Address::fields.placeholder.phone'))!!}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
    {!! Form::text('email')->required()
    -> label(trans('Address::fields.label.email'))
    -> placeholder(trans('Address::fields.placeholder.email'))!!}
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
    {!! Form::text('address_1')->required()
    -> label(trans('Address::fields.label.address_1'))
    -> placeholder(trans('Address::fields.placeholder.address_1'))!!}
    </div>	 
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
    {!! Form::text('address_2')
    -> label(trans('Address::fields.label.address_2'))
    -> placeholder(trans('Address::fields.placeholder.address_2'))!!}
    </div>      
</div>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
    {!! Form::text('city')->required()
    -> label(trans('Address::fields.label.city'))
    -> placeholder(trans('Address::fields.placeholder.city'))!!}
    </div>	 
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
    {!! Form::select('state')->required()
    -> options(trans('Address::fields.options.states'))
    -> label(trans('Address::fields.label.state'))
    -> placeholder(trans('Address::fields.placeholder.state'))!!}
    </div>      
</div>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
    {!! Form::select('country')->required()
    -> label(trans('Address::fields.label.country'))             
    -> options(app('address')->getAvailableCountries())
    -> placeholder(trans('Address::fields.placeholder.country'))
    -> value(config('quatius.address.country.default',''))!!}
    </div>	 
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
    {!! Form::text('postcode')->required()
    -> label(trans('Address::fields.label.postcode'))
    -> placeholder(trans('Address::fields.placeholder.postcode'))!!}
    </div>      
</div>