{!!Form::open()
        ->attributes(['onsubmit'=>'saveAddress(this);'])
        ->class("form-horizontal")
        ->id('show-address')
        ->method('POST')
        ->action(URL::to('address/'.$address->getRouteKey()))!!}
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title"><i class="fa fa-user"></i>&nbsp; Contact Details</h4>
</div>
<div class="modal-body">
    @include('Address::partials.entry')
    <input type="hidden" name="_method" value="patch"/>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" data-action="DELETE" data-datatable='#dt-contacts' data-href="{{ trans_url('address/'.$address->getRouteKey())}}" ><i class="fa fa-trash"></i>&nbsp; {{ trans('cms.delete') }}</button>
    
    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Close</button>
    <button type="button" class="btn btn-primary" onclick="$('#show-address input,select,textarea').prop('disabled', false).hide;$(this).hide();$(this).next().show();"><i class="fa fa-edit"></i>&nbsp; Click to Edit Now</button>
    <button type="submit" class="btn btn-primary" style="display:none"><i class="fa fa-floppy-o"></i>&nbsp; {{ trans('cms.save') }}</button>
</div>
{!! Form::close() !!}

<script>
    $('#show-address input,select,textarea').prop('disabled', true);
</script>