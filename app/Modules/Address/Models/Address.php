<?php

namespace Quatius\Address\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Quatius\Framework\Traits\ModelFinder;
use Quatius\Framework\Traits\ParamSetter;

class Address extends Model implements Transformable{

    use ModelFinder, ParamSetter, SoftDeletes;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
        'title',
        'company',
        'first_name',
        'middle_name',
        'last_name',
        'phone_1',
        'phone_2',
        'email',
        'address_1',
        'address_2',
        'city',
        'postcode',
        'state',
        'country',
        'nick',
        'address_type',
	];

    public function transform()
    {
        $data = $this->toArray();
        
        if (isset($data['id']))
            $data['id'] = $this->getRouteKey();

        $data = array_map(function($v){
            return $v ?: '';
        },$data);
        
        unset($data['link_id']);
        unset($data['link_type']);
        return $data;
    }

    public function getName()
    {
        $name = ($this->nick != null ? $this->nick : $this->first_name." ".$this->last_name);
        return $name;
    }

    public function getFullname()
    {
        return $this->first_name." ".$this->last_name;
    }

}
