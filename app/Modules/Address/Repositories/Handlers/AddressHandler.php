<?php

namespace Quatius\Address\Repositories\Handlers;

use Quatius\Address\Repositories\AddressRepository;
use Quatius\Framework\Repositories\QuatiusRepository;

class AddressHandler extends QuatiusRepository implements AddressRepository
{
    private $_postalDatas = [];
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return \Quatius\Address\Models\Address::class;
    }

    //!!app('account')->getPostalCodes('AU')->where(0,"2611")->where(1,"URIARRA")->where(2,"NSW")->count()
    public function getStates($countryCode = ''){
        $states = country($countryCode)->getDivisions();
        array_walk($states, function(&$state){$state = $state["name"];});

        return $states;
    }

    public function formatSimplify($data, $callback=null){
        $countries = $this->getAvailableCountries();
        
        $country = abbreviate(isset($data['country'])?$data['country']:'', $countries);
        if ($country)
            $data['country'] = $country;
        else{
            if ($callback) $callback('country', "Address::address.COUNTRY_NOT_FOUND", $country);
            return $data;
        }
        
        $state = abbreviate(isset($data['state'])?$data['state']:'',$this->getStates($data['country']));
        if ($state)
            $data['state'] = $state;
        else{
            if ($callback) $callback('state', "Address::address.STATE_NOT_FOUND", $state);
            return $data;
        }

        if (!isset($data['postcode']) || !$this->getPostalCodes($data['country'])->where(0,$data['postcode'])->where(1,strtoupper($data['city']))->where(2,$data['state'])->count()){
            if ($callback) $callback('postcode', "Address::address.POSTCODE_ERROR", isset($data['postcode'])?$data['postcode']:"");
        }

        return $data;
    }

    public function getAvailableCountries($field='name',$key='iso_3166_1_alpha2'){
        return collect(countries())->whereIn('iso_3166_1_alpha2', config('quatius.address.country.available',['AU']))->pluck($field, $key)->sort()->toArray();
    }

    public function getPostalCodes($countryCode = ''){

        if (isset($this->_postalDatas[$countryCode]))
        return $this->_postalDatas[$countryCode];

        $postJs = realpath(__DIR__.'/../../Assets/'.strtolower($countryCode).'_postcodes.json'); // TODO: change to config

        if (file_exists($postJs)){
            $this->_postalDatas[$countryCode] = collect(json_decode(file_get_contents($postJs), true));
        }
        else
            $this->_postalDatas[$countryCode] = [];
        
        return $this->_postalDatas[$countryCode];
    }
}
