<?php
namespace Quatius\Address\Repositories\Presenters;

use Prettus\Repository\Presenter\FractalPresenter;

class AddressIndexPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AddressIndexTransformer();
    }
}
