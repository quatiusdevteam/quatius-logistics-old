<?php

namespace Quatius\Address\Repositories\Presenters;

use League\Fractal\TransformerAbstract;
use Quatius\Address\Models\Address;

/**
 * Class PostTransformer
 * @package namespace App\Modules\GenerateTransformers;
 */
class AddressIndexTransformer extends TransformerAbstract
{

    /**
     * Transform the Post entity
     * @param Quatius\Address\Models\Address $model
     *
     * @return array
     */
    public function transform(Address $model)
    {
        return $model->transform();
    }
}
