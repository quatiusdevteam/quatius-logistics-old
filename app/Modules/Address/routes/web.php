<?php

Route::group(['module' => 'Address', 'middleware' => ['web', 'auth'], 'namespace' => 'App\Modules\Address\Controllers'], function() {

    //Route::resource('Address', 'AddressController');

   
    Route::get('address/all', 'AddressController@all');
    Route::get('address/postal/{country_code?}', 'AddressController@addressPostal');
    Route::resource('address', 'AddressController', ['parameters' =>['address' => 'address_id']]);
});
