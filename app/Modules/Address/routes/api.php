<?php

Route::group(['prefix'=>'api', 'module' => 'Address', 'middleware' => ['auth:api', 'bindings'], 'namespace' => 'App\Modules\Address\Controllers'], function() {

    //Route::resource('Address', 'AddressController');
    Route::get('address/postal/{country_code?}', 'AddressController@addressPostal');
    Route::resource('address', 'AddressController', ['parameters' =>['address' => 'address_id'], 'except' => ['edit','create']]);
});
