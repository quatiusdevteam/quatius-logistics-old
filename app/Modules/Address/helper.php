<?php

/**
 *	Address Helper  
 */

if (!function_exists('abbreviate'))
{
    function abbreviate($abb, $list){
        array_walk($list, function(&$val){$val = strtoupper($val);});

        $abb = strtoupper($abb);

        if (isset($list[$abb])){
            return $abb;
        }else{
            $foundAb = array_search($abb ,$list);
            if ($foundAb !== false)
                return $foundAb;
        }

        return null;
    }
}
