<?php namespace App\Modules\Myaccount\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use Illuminate\Http\Request;
use DB;
class MyaccountController extends Controller {

	public function __construct()
	{
		$this->middleware('web');
		$this->middleware('auth:web');

		$this->setupTheme(config('theme.map.registered.theme'), config('theme.map.registered.layout'));
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        if (Auth::user()->pronto_account === '*'){
            return redirect(url('selfmanage/proof-of-delivery'));
        }
		return $this->theme->of('Myaccount::index')->render();
		//return view("Myaccount::index");
	}

	public function loadBooking(){
		return $this->theme->of('Myaccount::partials._new_booking')->render();
	}

	public function loadManageBooking(){
		return $this->theme->of('Myaccount::partials._manage_booking')->render();
	}
	public function loadBookingHistory(){
		return $this->theme->of('Myaccount::partials._booking_history')->render();
	}
	public function manageAddresses(Request $request){

		if ($request->wantsJson()) {
			$query = DB::table('manage_address')->get();
			return response()->json(['data' => $query], 200);

		}

		return $this->theme->of('Myaccount::manage_address.index')->render();
	}

	public function loadProfile(){
		//$use = auth();
		return $this->theme->of('Myaccount::partials._profile')->render();
	}
	public function loadChangePassword(){

		return $this->theme->of('Myaccount::partials._changepassword')->render();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
