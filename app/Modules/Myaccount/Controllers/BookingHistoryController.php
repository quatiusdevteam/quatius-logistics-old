<?php namespace App\Modules\Myaccount\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modules\Myaccount\Models\ManageAddress;

use Form;
use Auth;
use Validator;
use App\Modules\Myaccount\Models\BookingAddress;
use App\Modules\Myaccount\Models\Booking;
class ManageBookingController extends Controller{

	public function __construct(){
		$this->middleware('web');
		$this->middleware('auth:web');

		$this->setupTheme(config('theme.map.registered.theme'), config('theme.map.registered.layout'));
	}
	
	protected function validator(array $data){
		return Validator::make($data, [
				'first_name' => 'required|max:255',
				'last_name' => 'required|max:255',
				'name' => 'required|max:255',
				'phone_1' => 'min:8|max:10',
				'email' => 'required|email|max:255',
				'address_1' => 'required|max:255',
				'city' => 'required|max:255',
				'postcode' => 'required|max:4|min:4',
				]);
	}
	
	public function index(Request $request){
	
		if ($request->wantsJson()) {
			$user_id = Auth::user()->id;
			$query = Booking::whereUserId($user_id)->get();
			
			return response()->json(['data' => $query], 200);
	
		}
	
		return $this->theme->of('Myaccount::booking.index')->render();
	}
	
	/**
	 * Display address.
	 *
	 * @param Request $request
	 * @param int     $id
	 *
	 * @return Response
	 */
	public function show(Request $request, $booking_id)
	{			
		
		$user = Auth::user();
		Form::populate($user);
		$booking = Booking::findOrNew($booking_id);
		$sender_address = $booking->addresses()->whereAddressType('source')->first();
		$receiver_address = $booking->addresses()->whereAddressType('destination')->first();
		/*
		$sender_address = BookingAddress::whereAddressType('source');
		$receiver_address = BookingAddress::whereAddressType('destination');
		*/
		if (!$booking->exists) {
			return response()->view('Myaccount::booking.new', compact('booking', 'sender_address', 'receiver_address'));
		}
		return response()->view('Myaccount::booking.show', compact('booking', 'sender_address', 'receiver_address'));
		
	}
	
	
	/**
	 * Display booking form.
	 *
	 * @param Request $request
	 * @param int     $id
	 *
	 * @return Response
	 */
	
	public function create(Request $request)
	{
		$user = Auth::user();
		Form::populate($user);
		 $sender_address = [];// = BookingAddress::whereAddress_type('source')->first();
		 $receiver_address =[];// = BookingAddress::whereAddress_type('destination')->first();
		 $booking = [];// = Booking::first();
		//dd($sender_address);
		return response()->view('Myaccount::booking.create', compact('booking', 'sender_address', 'receiver_address'));
				
	}
	
	/**
	 * Create new address.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		try {			
			$attributes            = $request->all();
			
			/* Prepare Booking data */
			$booking['user_id'] 		= Auth::user()->id;
			$booking['status'] 			= 'Pending';
			$booking['tracking_number'] = $this->generateTrackingNumber();
			$booking['number_of_item']	= $attributes['item_qty'];
			$booking['length']			= $attributes['item_length'];
			$booking['width']			= $attributes['item_width'];
			$booking['height']			= $attributes['item_height'];
			$booking['weight']			= $attributes['item_weight'];
			$booking['type']			= $attributes['item_type'];
			$booking['remark']			= $attributes['item_remark'];
			
			/* Prepare sender data */
			$sender['address_type'] = 'source';
			$sender['company_name'] = $attributes['sender_company_name'];
			$sender['first_name'] 	= $attributes['sender_first_name'];
			$sender['last_name'] 	= $attributes['sender_last_name'];
			$sender['phone'] 		= $attributes['sender_phone'];
			$sender['email'] 		= $attributes['sender_email'];
			$sender['address_1'] 	= $attributes['sender_address_1'];
			$sender['address_2'] 	= $attributes['sender_address_2'];
			$sender['city'] 		= $attributes['sender_city'];
			$sender['state'] 		= $attributes['sender_state'];
			$sender['postcode'] 	= $attributes['sender_postcode'];
			$sender['country'] 		= $attributes['sender_country'];
			
			/* Prepare receiver data */
			$receiver['address_type'] 	= 'destination';
			$receiver['company_name'] 	= $attributes['receiver_company_name'];
			$receiver['first_name'] 	= $attributes['receiver_first_name'];
			$receiver['last_name'] 		= $attributes['receiver_last_name'];
			$receiver['phone'] 			= $attributes['receiver_phone'];
			$receiver['email'] 			= $attributes['receiver_email'];
			$receiver['address_1'] 		= $attributes['receiver_address_1'];
			$receiver['address_2'] 		= $attributes['receiver_address_2'];
			$receiver['city'] 			= $attributes['receiver_city'];
			$receiver['state'] 			= $attributes['receiver_state'];
			$receiver['postcode'] 		= $attributes['receiver_postcode'];
			$receiver['country'] 		= $attributes['receiver_country'];
			
			/* validate data? */
			
			/* Save data to database */
			$thisBooking = Booking::create($booking);
			$thisBooking->addresses()->create($sender);
			$thisBooking->addresses()->create($receiver);
						
			return response()->json([
					'message'  => trans('messages.success.create', ['Module' => trans('Myaccount::selfmanage.label.booking')]),
					'code'     => 204,
					'redirect' => trans_url('/selfmanage/booking/' . $thisBooking->id),
					], 201);
	
		} catch (Exception $e) {
			return response()->json([
					'message' => $e->getMessage(),
					'code'    => 400,
					], 400);
		}
	
	}
	
	/**
	 * Show booking for editing.
	 *
	 * @param Request $request
	 * @param int     $id
	 *
	 * @return Response
	 */
	public function edit(Request $request, $booking_id)
	{
		$user = Auth::user();
		Form::populate($user);
		$booking = Booking::findOrNew($booking_id);
		$sender_address = $booking->addresses()->whereAddressType('source')->first();
		$receiver_address = $booking->addresses()->whereAddressType('destination')->first();
	
		return response()->view('Myaccount::booking.edit', compact('booking', 'sender_address', 'receiver_address'));
	}
	
	/**
	 * Update the booking.
	 *
	 * @param Request $request
	 * @param int     $id
	 *
	 * @return Response
	 */
	public function update(Request $request, $booking_id)
	{
	try {			
			$attributes            = $request->all();
			
			/* Prepare Booking data */
			
			$booking['number_of_item']	= $attributes['item_qty'];
			$booking['length']			= $attributes['item_length'];
			$booking['width']			= $attributes['item_width'];
			$booking['height']			= $attributes['item_height'];
			$booking['weight']			= $attributes['item_weight'];
			$booking['type']			= $attributes['item_type'];
			$booking['remark']			= $attributes['item_remark'];
			
			/* Prepare sender data */
			
			$sender['company_name'] = $attributes['sender_company_name'];
			$sender['first_name'] 	= $attributes['sender_first_name'];
			$sender['last_name'] 	= $attributes['sender_last_name'];
			$sender['phone'] 		= $attributes['sender_phone'];
			$sender['email'] 		= $attributes['sender_email'];
			$sender['address_1'] 	= $attributes['sender_address_1'];
			$sender['address_2'] 	= $attributes['sender_address_2'];
			$sender['city'] 		= $attributes['sender_city'];
			$sender['state'] 		= $attributes['sender_state'];
			$sender['postcode'] 	= $attributes['sender_postcode'];
			$sender['country'] 		= $attributes['sender_country'];
			
			/* Prepare receiver data */
			
			$receiver['company_name'] 	= $attributes['receiver_company_name'];
			$receiver['first_name'] 	= $attributes['receiver_first_name'];
			$receiver['last_name'] 		= $attributes['receiver_last_name'];
			$receiver['phone'] 			= $attributes['receiver_phone'];
			$receiver['email'] 			= $attributes['receiver_email'];
			$receiver['address_1'] 		= $attributes['receiver_address_1'];
			$receiver['address_2'] 		= $attributes['receiver_address_2'];
			$receiver['city'] 			= $attributes['receiver_city'];
			$receiver['state'] 			= $attributes['receiver_state'];
			$receiver['postcode'] 		= $attributes['receiver_postcode'];
			$receiver['country'] 		= $attributes['receiver_country'];
			
			/* validate data? */
			
			/* Save data to database */
			$thisBooking = Booking::find($booking_id);
			$thisBooking->update($booking);
			$source_address_id = $thisBooking->addresses()->whereAddressType('source')->pluck('id');
			$destination_address_id = $thisBooking->addresses()->whereAddressType('destination')->pluck('id');
			$thisBooking->addresses()->find($source_address_id)->update($sender);
			$thisBooking->addresses()->find($destination_address_id)->update($receiver);
						
			return response()->json([
					'message'  => trans('messages.success.updated', ['Module' => trans('Myaccount::selfmanage.label.booking')]),
					'code'     => 204,
					'redirect' => trans_url('/selfmanage/booking/' . $thisBooking->id),
					], 201);
	
		} catch (Exception $e) {
			return response()->json([
					'message' => $e->getMessage(),
					'code'    => 400,
					], 400);
		}
	
	}
	
	/**
	 * Remove the address.
	 *
	 * @param int $id
	 *
	 * @return Response
	 */
	public function destroy(Request $request, $booking_id)
	{
		$thisBooking = Booking::find($booking_id);
		try {
			$thisBooking->addresses()->delete();
			$thisBooking->delete();
	
			return response()->json([
					'message'  => trans('messages.success.deleted', ['Module' => trans('Myaccount::selfmanage.label.address')]),
					'code'     => 202,
					'redirect' => trans_url('/selfmanage/booking/0'),
					], 202);
	
		} catch (Exception $e) {
	
			return response()->json([
					'message'  => $e->getMessage(),
					'code'     => 400,
					'redirect' => trans_url('/selfmanage/booking/' . $thisBooking->id),
					], 400);
		}
	
	}
	
	function generateTrackingNumber(){
		$tracking_number = strtoupper(uniqid());
		if(Booking::whereTrackingNumber($tracking_number)->count()){
			return generateInvoiceNumber();
		}
		return $tracking_number;
	}
}	