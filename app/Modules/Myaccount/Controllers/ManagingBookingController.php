<?php namespace App\Modules\Myaccount\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Myaccount\Models\BookingSession;
use App\Modules\Supplier\Models\QLRate;
use App\Modules\Supplier\Models\QLZone;
use App\Modules\SupplierOperation\Models\SupplierOperation;
use Illuminate\Http\Request;
use Form;
use Auth;
use Validator;
use stdClass;
use App\Modules\Myaccount\Models\Booking;

class ManagingBookingController extends Controller{

	public function __construct(){
		$this->middleware('web');
		$this->middleware('auth:web');

		$this->setupTheme(config('theme.map.registered.theme'), config('theme.map.registered.layout'));
	}
	
	protected function validator(array $data){
		return Validator::make($data, [
				'first_name' => 'required|max:255',
				'last_name' => 'required|max:255',
				'name' => 'required|max:255',
				'phone_1' => 'min:8|max:10',
				'email' => 'required|email|max:255',
				'address_1' => 'required|max:255',
				'city' => 'required|max:255',
				'postcode' => 'required|max:4|min:4',
				]);
	}
	
	public function index(Request $request){
	
		if ($request->wantsJson()) {
			$user_id = Auth::user()->id;
			$query = Booking::whereUserId($user_id)->withStatus()->get();			
			return response()->json(['data' => $query], 200);
	
		}
		
		return $this->theme->of('Myaccount::booking.index')->render();
	}
	
	
	/**
	 * Display address.
	 *
	 * @param Request $request
	 * @param int     $id
	 *
	 * @return Response
	 */
	public function show(Request $request, $booking_id)
	{
        $user = Auth::user();

		Form::populate($user);
		//bsooking = Booking::findOrNew($booking_id);
		$booking = Booking::whereId($booking_id)->withStatus()->first();		
		
		if (is_null($booking)) {			
			return response()->view('Myaccount::booking.new', compact('booking', 'sender_address', 'receiver_address'));
		}
		$can_edit = false;
		if(($user->hasRole('admin') && ($booking->allow_staff_change == 1)) || ($booking->allow_customer_change ==1)){
			$can_edit = true;
		}
        
		$sender_address = $booking->addresses()->whereAddressType('source')->first();
		$receiver_address = $booking->addresses()->whereAddressType('destination')->first();

		return response()->view('Myaccount::booking.show', compact('can_edit', 'booking', 'sender_address', 'receiver_address'));
		
	}
	
	/**
	 * Display address.
	 *
	 * @param Request $request
	 * @param int     $id
	 *
	 * @return Response
	 */
	public function display(Request $request, $booking_id)
	{
	
		$user = Auth::user();
		Form::populate($user);
		//bsooking = Booking::findOrNew($booking_id);
		$booking = Booking::whereId($booking_id)->withStatus()->first();				
		$source_address = $booking->addresses()->whereAddressType('source')->first();
		$destination_address = $booking->addresses()->whereAddressType('destination')->first();

		return response()->view('Myaccount::booking.display', compact('booking', 'source_address', 'destination_address'));
	
	}

    /**
     * @return mixed
     */
	public function confirm(){

	    if(BookingSession::hasBooking()) {
            $booking = BookingSession::getBooking();
            //$booking = session('customer-booking');
            $source_address = (object)$booking->getOriginationAddress();
            $destination_address = (object)$booking->getDestinationAddress();
            return $this->theme->of('Myaccount::booking.confirm', compact('booking', 'source_address', 'destination_address'))->render();
        }else{
	        return redirect(url('selfmanage/booking'));
        }
    }

    /**
     * @param Request $request
     */
	public function complete(Request $request){
        $booking = BookingSession::getBooking();
        $booked = $booking->saveToDatabase();
        $tracking_number = $booked->tracking_number;
        $source = (object)$booking->getOriginationAddress();
        $destination = (object)$booking->getDestinationAddress();
        self::exportBookingforProntto($booked, $source, $destination);
        email_booking_confirmaion_to_admin($booked, $source, $destination);
        BookingSession::clearBooking();
        return redirect(url('selfmanage/finish/' . $tracking_number));
    }

    /**
     * @param string $tracking_number
     * @return mixed
     */
    public function finished($tracking_number = ""){

        return $this->theme->of('Myaccount::booking.complete', compact('tracking_number'))->render();
    }


	/**
	 * Display booking form.
	 *
	 * @param Request $request
	 * @param int     $id
	 *
	 * @return Response
	 */
	
	public function create(Request $request)
	{
		$user = Auth::user();
		Form::populate($user);
		 $sender_address = [];// = BookingAddress::whereAddress_type('source')->first();
		 $receiver_address =[];// = BookingAddress::whereAddress_type('destination')->first();
		 $booking = [];// = Booking::first();
		//dd($sender_address);
		return response()->view('Myaccount::booking.create', compact('booking', 'sender_address', 'receiver_address'));
				
	}
	
	/**
	 * Create new address.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
	    try {
            $data = $this->verifyData($request->all());

            if($data->status_id == 0){
                return response()->json([
                    'message'  => $data->output,
                    'code'     => 500,
                ], 500);
            }
            return response()->json([

                'code'     => 204,
                'pageRedirect' => trans_url('/selfmanage/confirm/'),
                'redirect' => trans_url('/selfmanage/confirm/'),
            ], 202);

		} catch (Exception $e) {
			return response()->json([
					'message' => $e->getMessage(),
					'code'    => 400,
					], 400);
		}
	
	}
	
	/**
	 * Show booking for editing.
	 *
	 * @param Request $request
	 * @param int     $id
	 *
	 * @return Response
	 */
	public function edit(Request $request, $booking_id)
	{
		
		$user = Auth::user();
		Form::populate($user);
		//$booking = Booking::findOrNew($booking_id);
		$booking = Booking::whereId($booking_id)->withStatus()->first();
		$sender_address = [];
		$receiver_address = [];

		if(!is_null($booking)){
			$sender_address = $booking->addresses()->whereAddressType('source')->first();
			$receiver_address = $booking->addresses()->whereAddressType('destination')->first();			
		}
		
		return response()->view('Myaccount::booking.edit', compact('booking', 'sender_address', 'receiver_address'));
	}
	
	/**
	 * Update the booking.
	 *
	 * @param Request $request
	 * @param int     $id
	 *
	 * @return Response
	 */
	public function update(Request $request, $booking_id)
	{

	try {

            $user_id = Auth::user()->id;
            $data = $this->verifyData($request->all());

            if($data->status_id == 0){
                return response()->json([
                    'message'  => $data->output,
                    'code'     => 500,
                ], 500);
            }

            return response()->json([

                    'code'     => 204,
                    'pageRedirect' => trans_url('/selfmanage/confirm/'),
                    'redirect' => trans_url('/selfmanage/confirm/'),
                    ], 202);

		} catch (Exception $e) {
			return response()->json([
					'message' => $e->getMessage(),
					'code'    => 400,
					], 400);
		}

	}
	
	/**
	 * Remove the address.
	 *
	 * @param int $id
	 *
	 * @return Response
	 */
	public function destroy(Request $request, $booking_id)
	{
		$thisBooking = Booking::find($booking_id);
		try {
			/* Prepare history data */
			$history['user_id']	= Auth::user()->id;
			$history['tracking_number'] = $thisBooking->tracking_number;
			$history['title'] = trans('Myaccount::selfmanage.title.delete_booking');
			$history['description'] = ' <p>Delete booking:</p>';				
			$thisBooking->activitylogs()->create($history);
			
			$thisBooking->addresses()->delete();
			$thisBooking->delete();
	
			return response()->json([
					'message'  => trans('messages.success.deleted', ['Module' => trans('Myaccount::selfmanage.label.address')]),
					'code'     => 202,
					'redirect' => trans_url('/selfmanage/booking/0'),
					], 202);
	
		} catch (Exception $e) {
	
			return response()->json([
					'message'  => $e->getMessage(),
					'code'     => 400,
					'redirect' => trans_url('/selfmanage/booking/' . $thisBooking->id),
					], 400);
		}
	
	}

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
	public function history(Request $request){
		if ($request->wantsJson()) {
			$user_id = Auth::user()->id;
			$query = Booking::whereUserId($user_id)->withStatus()->withDestinationAddress()->orderBy('bookings.created_at', 'desc')->get();
				
			return response()->json(['data' => $query], 200);
		
		}
		
		return $this->theme->of('Myaccount::booking.history')->render();
	}

    /**
     * @param Request $request
     * @return mixed
     */
	public function search(Request $request)
	{							
			$q = $request->get('q');
			$user = Auth::user();
			if($user->hasRole('admin'))
				$booking = Booking::whereTrackingNumber($q)->withStatus()->first();	
			else{ 		
				$booking = Booking::whereTrackingNumber($q)->whereUserId($user->id)->withStatus()->first();			
			}
			if($booking){
				$source_address = $booking->addresses()->whereAddressType('source')->first();
				$destination_address = $booking->addresses()->whereAddressType('destination')->first();
				$activities = $booking->activitylogs()->get();
				return $this->theme->of('Myaccount::booking.search', compact('q', 'booking', 'source_address', 'destination_address', 'activities'))->render();
			}						
		$booking = "not found";
		return $this->theme->of('Myaccount::booking.search', compact('q', 'booking'))->render();
	
	}

    /**
     * @param $data
     * @return mixed
     */
	public function verifyData($attributes = []){
        $data = new stdClass;

	    if(sizeof($attributes) > 0) {
            //Check valid postcode
            $origination_zone = $this->getZoneCodeFromPostcode($attributes['sender_postcode']);
            if (!$origination_zone) {
                $data->status_id = 0;
                $data->output = "Sorry, we do not operate in the origination postcode.";
                return $data;
            }

            $destination_zone = $this->getZoneCodeFromPostcode($attributes['receiver_postcode']);
            if (!$destination_zone) {
                $data->status_id = 0;
                $data->output = "Sorry, we do not operate in the destination postcode.";
                return $data;
            }

            if(intval($attributes['item_weight']) > 350){
                $data->status_id = 0;
                $data->output = "Sorry, the maximum weight is 350kg.";
                return $data;
            }
            
            // Check item type
            if (strtolower($attributes['item_type']) == "pallet") {
                $package_type = strtolower(self::getPackageType($attributes['item_length'], $attributes['item_width'], $attributes['item_height']));
                if ($package_type === "unknown") {
                    $data->status_id = 0;
                    $data->output = "Cannot define package type base on provided dimension";
                    return $data;
                }
                $package_price = $this->getPackagePrice($origination_zone->zone_code, $destination_zone->zone_code, $package_type);

                if(sizeof($package_price) >0){
                    $subtotal = $package_price[$package_type];
                }else{
                    $subtotal = 0;
                }
                // applied discount as request from Peter

                $user = Auth::user();
                $discount = $user->discount;
                $discount_amount = ($subtotal * $discount) / 100;
                $subtotal = $subtotal - $discount_amount;
                $booking_data = self::formatData($attributes);
                $booking_item = $booking_data->item;
                $subtotal *= $booking_item['number_of_item'];
                $fuel_levy = floatval($subtotal * self::getFuelLevyRate());
                $booking_item['fuel_levy']  = $fuel_levy;
                $booking_item['subtotal']   = $subtotal;
                $booking_item['tax_total']  = floatval(($subtotal + $fuel_levy) * self::getVATRate());
                $booking_item['total']      = floatval(($subtotal + $fuel_levy) * (1 + self::getVATRate()));

                $booking_origination = $booking_data->sender;
                $booking_destination = $booking_data->receiver;

                $customer = Auth::user();

                BookingSession::clearBooking();
                $bs = BookingSession::getBooking(new Booking($booking_item));
                $bs->setBookingCustomer($customer);
                $bs->setOriginationAddress($booking_origination);
                $bs->setDestinationAddress($booking_destination);

                $data->status_id = 1;
                $data->output = $bs;
                return $data;

            } else {
                $data->status_id = 0;
                $data->output = "Sorry, we only accept item type pallet at the moment.";
                return $data;
            }

        }

        $data->status_id = 0;
        $data->output = "Data not provided";
        return $data;
    }

    /**
     * @param array $attributes
     * @return stdClass
     */
    private function formatData($attributes = []){
        $output = new stdClass;

        $booking['number_of_item']  = $attributes['item_qty'];
        $booking['shipment_method'] = $attributes['shipment_method'];
        $booking['length']          = $attributes['item_length'];
        $booking['width']           = $attributes['item_width'];
        $booking['height']          = $attributes['item_height'];
        $booking['weight']          = $attributes['item_weight'];
        $booking['type']            = $attributes['item_type'];
        $booking['warehouse_code']  = $attributes['warehouse_code'];
        $booking['quote_reference'] = $attributes['quote_reference'];
        $booking['ql_contact']      = $attributes['ql_contact'];
        $booking['time_ready']      = $attributes['time_ready'];
        $booking['remark']          = $attributes['item_remark'];

        /* Prepare sender data */
        $sender['address_type']     = 'source';
        $sender['company_name']     = $attributes['sender_company_name'];
        $sender['first_name']       = $attributes['sender_first_name'];
        $sender['last_name']        = $attributes['sender_last_name'];
        $sender['phone']            = $attributes['sender_phone'];
        $sender['email']            = $attributes['sender_email'];
        $sender['address_1']        = $attributes['sender_address_1'];
        $sender['address_2']        = $attributes['sender_address_2'];
        $sender['city']             = $attributes['sender_city'];
        $sender['state']            = $attributes['sender_state'];
        $sender['postcode']         = $attributes['sender_postcode'];
        $sender['country']          = $attributes['sender_country'];

        /* Prepare receiver data */
        $receiver['address_type']   = 'destination';
        $receiver['company_name']   = $attributes['receiver_company_name'];
        $receiver['first_name']     = $attributes['receiver_first_name'];
        $receiver['last_name']      = $attributes['receiver_last_name'];
        $receiver['phone']          = $attributes['receiver_phone'];
        $receiver['email']          = $attributes['receiver_email'];
        $receiver['address_1']      = $attributes['receiver_address_1'];
        $receiver['address_2']      = $attributes['receiver_address_2'];
        $receiver['city']           = $attributes['receiver_city'];
        $receiver['state']          = $attributes['receiver_state'];
        $receiver['postcode']       = $attributes['receiver_postcode'];
        $receiver['country']        = $attributes['receiver_country'];

        $output->sender     = $sender;
        $output->receiver   = $receiver;
        $output->item       = $booking;
        return $output;
    }



    /**
     * @param $origination
     * @param $destination
     * @param $item
     * @return int
     */
	private function getSupplier($origination, $destination, $item){
        $so = new SupplierOperation();
        $supplier_id = $so->getSupplierId($origination, $destination);
        return $supplier_id;
    }

    /**
     * @param $item
     * @return mixed
     */
    private function calculateItemWeight($item){
	    $width = $item['width'];
	    $length = $item['length'];
        $height = $item['height'];
	    $weight = $item['weight'];
	    $cube = $width * $length * $height * 250;
        return ($weight > $cube) ? $weight : $cube;
    }

    /**
     * @param $item
     * @return string
     */
    private function getPackageType($length, $width, $height){
        $palletType ="unknown";
        if($height <= 170){
            if($width<=120){
                if($length<=120){               //0-120 (L) x 0-120 (W) x 0-170 (h)
                    $palletType = "standard";
                }elseif($length <=140){         //121-140 (l) x 0-120 (w) x 0-170 (h)
                    $palletType = "os";
                }elseif($length <= 150){        //141-150 (l) x 0-120 (w) x 0-170 (h)
                    $palletType = "xl";
                }else{
                    $palletType = "unknown";
                }
            }elseif($width <= 140){
                if($length <= 120){
                    $palletType = "os";
                }else{
                    $palletType = "unknown";
                }
            }elseif($width <= 150){
                if($length <= 120){
                    $palletType = "xl";
                }else{
                    $palletType = "unknown";
                }
            }else{
                $palletType = "unknown";
            }
        }elseif($height <= 190){
            if($width <= 120){
                if($length <= 120){
                    $palletType = "os";
                }elseif($length <= 140){
                    $palletType = "xl";
                }elseif($length <= 180) {
                    $palletType = "oh";
                }elseif($length <= 232){
                    $palletType = "dp";
                }else{
                    $palletType = "unknown";
                }
            }elseif($width <= 140){
                if($length <= 120){
                    $palletType = "xl";
                }elseif($length <= 140){
                    $palletType = "oh";
                }else{
                    $palletType = "unknown";
                }
            }elseif($width <= 180){
                if($length <= 120){
                    $palletType = "oh";
                }else{
                    $palletType = "unknown";
                }
            }elseif($width <= 232){
                if($length <= 120){
                    $palletType = "dp";
                }else{
                    $palletType = "unknown";
                }
            }else{
                $palletType = "unknown";
            }
        }else{
            if($width <= 120){
                if($length <= 120){
                    $palletType = "dp";
                }else{
                    $palletType = "unknown";
                }
            }else{
                $palletType = "unknown";
            }
        }
         return $palletType;
    }

    /**
     * @param $origination_zone
     * @param $destination_zone
     * @param $item_type
     * @return mixed
     */
    private function getPackagePrice($origination_zone, $destination_zone, $item_type){
        return QLRate::whereFromZone($origination_zone)->whereToZone($destination_zone)->select($item_type)->first();
    }

    /**
     * @param $postcode
     * @return mixed
     */
    private function getZoneCodeFromPostcode($postcode){
        return QLZone::wherePostCode($postcode)->whereStatusId(1)->select('zone_code')->first();
    }
    /**
     * @param $postcode
     * @return bool
     */
    protected function isValidPostcode($postcode){
        if(substr($postcode, 0, 1) == 3)
            return true;
        else
            return false;
    }

    private function getVATRate(){
        return 0.1;
    }

    private function getFuelLevyRate(){
        return (app('settings')->get('fuel_levy', 'additional_charge')) ? app('settings')->get('fuel_levy', 'additional_charge') : 0;
    }

    /**
     * @return string
     */
    public static function generateTrackingNumber(){
        $tracking_number = strtoupper(uniqid());
        if(Booking::whereTrackingNumber($tracking_number)->count()){
            return generateInvoiceNumber();
        }
        return $tracking_number;
    }

    /**
     * @param $booked
     * @param $source
     * @param $destination
     * @return bool
     */
    private function exportBookingforProntto($booked, $source, $destination){
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=booking.csv");
        header("Pragma: no-cache");
        header("Expires: 0");
        $title = array('Account Code', 'Whse Code', 'Creation Date', 'Company Name', 'Quote Reference', 'QL Contact', 'Time Ready', 'Sender Address', 'Sender Suburb', 'Sender Postcode', 'Sender State', 'Contact/Special Instructions', 'Receiver Address', 'Receiver Suburb', 'Receiver Postcode', 'Receiver State', 'Receiver Instruction', 'General/Express', 'UOM', 'Qty', 'Weight', 'Length', 'Width', 'Height', 'Cubic', 'Vehicle Specs', 'Pallet Type');
        $file = fopen('php://output', 'w');
        fputcsv($file, $title);

        return true;
    }
    
    	/*
	public function page(Request $request, $slug, $param=''){
		return $this->getPageWithParam($slug, $param); 
	}
	
	/**
     * Show page.
     *
     * @param string $slug
     *
     * @return response
     */
   /*protected function getPageWithParam($slug, $param)
    {
        // get page by slug
        $page = app('page')->getPage($slug);

        // if (is_null($page)) {
        //     //abort(404);
        //     $slug = '404';
        //     $page = app('page')->getPage($slug);
        // }

        // // Get view
        // $view = $page->view;
        // $view = view()->exists('public::page.' . $view) ? $view : 'page';

        // Component::renderPositions($page->slug.".html", ['page'=>$page, 'param'=>$param]);
        
        // //Set theme variables
        // $this->theme->setTitle(strip_tags($page->getTranslation($page->title)));
        // $this->theme->setKeywords(strip_tags($page->getTranslation($page->keyword)));
        // $this->theme->setDescription(strip_tags($page->getTranslation($page->description)));
        return PagePublicWebController::renderPage($this->theme, $page);
        // display page
        // if (view()->exists('public::page.' . $view))
        // 	return $this->theme->of('public::page.' . $view, compact('page'))->render();
        // else 
        // 	return $this->theme->of('Admin::content.page.public.page', compact('page'))->render();
    }*/
}	