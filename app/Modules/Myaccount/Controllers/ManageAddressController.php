<?php namespace App\Modules\Myaccount\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Modules\Myaccount\Models\ManageAddress;
use Form;
use Auth;
use GuzzleHttp\json_encode;

class ManageAddressController extends Controller{
	
	public function __construct()
	{
		$this->middleware('web');
		$this->middleware('auth:web');
	
		$this->setupTheme(config('theme.map.registered.theme'), config('theme.map.registered.layout'));
	}
	
	public function index(Request $request){
		
		if ($request->wantsJson()) {
			$user_id = Auth::user()->id;
			$query = ManageAddress::whereUserId($user_id)->get();
			return response()->json(['data' => $query], 200);
		
		}
		
		return $this->theme->of('Myaccount::manage_address.index')->render();
	}
	
	/**
	 * Display address.
	 *
	 * @param Request $request
	 * @param int     $id
	 *
	 * @return Response
	 */
	public function show(Request $request, $address_id)
	{
		
		$address = ManageAddress::findOrNew($address_id);
		if (!$address->exists) {
			return response()->view('Myaccount::manage_address.new', compact('address'));
		}
	
		Form::populate($address);
		return response()->view('Myaccount::manage_address.show', compact('address'));
	}
	
	/**
	 * Show the form for creating a new address.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function create(Request $request){
		$address = new ManageAddress();
	
		Form::populate($address);
			
		return response()->view('Myaccount::manage_address.create', compact('address'));
	
	}
		
	/**
	 * Create new address.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		try {
			$attributes            = $request->all();
			$attributes['user_id'] = Auth::user()->id;
			$address               = ManageAddress::create($attributes);
			
			return response()->json([
					'message'  => trans('messages.success.updated', ['Module' => trans('Myaccount::selfmanage.label.address')]),
					'code'     => 204,
					'redirect' => trans_url('/selfmanage/manage-address/' . $address->id),
					], 201);
	
		} catch (Exception $e) {
			return response()->json([
					'message' => $e->getMessage(),
					'code'    => 400,
					], 400);
		}
	
	}
	
	/**
	 * Show address for editing.
	 *
	 * @param Request $request
	 * @param int     $id
	 *
	 * @return Response
	 */
	public function edit(Request $request, $address_id)
	{
		$address = ManageAddress::findOrNew($address_id);
		Form::populate($address);
	
		return response()->view('Myaccount::manage_address.edit', compact('address'));
	}
	
	/**
	 * Update the address.
	 *
	 * @param Request $request
	 * @param int     $id
	 *
	 * @return Response
	 */
	public function update(Request $request, $address_id)
	{
		try {
	
			$attributes = $request->all();
			
			$address = ManageAddress::findOrNew($address_id);
			$address->update($attributes);
			
			return response()->json([
					'message'  => trans('messages.success.updated', ['Module' => trans('Myaccount::selfmanage.label.address')]),
					'code'     => 204,
					'redirect' => trans_url('/selfmanage/manage-address/' . $address->id),
					], 201);
	
		} catch (Exception $e) {
	
			return response()->json([
					'message'  => $e->getMessage(),
					'code'     => 400,
					'redirect' => trans_url('/selfmanage/manage-address/' . $address->id),
					], 400);
	
		}
	
	}
	
	/**
	 * Remove the address.
	 *
	 * @param int $id
	 *
	 * @return Response
	 */
	public function destroy(Request $request, $address_id)
	{
		$address = ManageAddress::find($address_id);
		try {
			
			$t = $address->delete();
	
			return response()->json([
					'message'  => trans('messages.success.deleted', ['Module' => trans('Myaccount::selfmanage.label.address')]),
					'code'     => 202,
					'redirect' => trans_url('/selfmanage/manage-address/0'),
					], 202);
	
		} catch (Exception $e) {
	
			return response()->json([
					'message'  => $e->getMessage(),
					'code'     => 400,
					'redirect' => trans_url('/selfmanage/manage-address/' . $address->id),
					], 400);
		}
	
	}
	
	public function getAvailableAddress(Request $request){
		if ($request->wantsJson()) {
			$user = Auth::user();
			if($user->hasRole('admin'))
				$query = ManageAddress::get();
			else
				$query = ManageAddress::whereUserId($user->id)->get();
			
			return response()->json(['data' => $query], 200);
		
		}
				
	}
	
	
}