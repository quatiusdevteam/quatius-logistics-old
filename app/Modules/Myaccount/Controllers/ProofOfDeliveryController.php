<?php namespace App\Modules\Myaccount\Controllers;

use App\Http\Controllers\Controller;
use Auth;

class ProofOfDeliveryController extends Controller {

    public function __construct () {
        $this->middleware('web');
        $this->middleware('auth:web');

        $this->setupTheme(config('theme.map.registered.theme'), config('theme.map.registered.layout'));
    }

    public function displayPODs () {
        $pronto_account = Auth::user()->pronto_account;

        $csv      = fopen('pronto/POD/webpodsnd.csv', 'r');
        $header   = fgetcsv($csv);
        $contents = [];
        while ($row = fgetcsv($csv)) {
            $contents[] = array_combine($header, $row);
        }
        fclose($csv);
        $listing = json_encode($contents);

        return $this->theme->of('Myaccount::proof_of_delivery.index', compact('pronto_account', 'listing'))->render();
    }
}
