<?php
Route::group(array('module' => 'Myaccount','middleware' => ['web'], 'namespace' => 'App\Modules\Myaccount\Controllers'), function() {
	// Route::get('/home', 'MyaccountController@index');
});

Route::group(array('prefix'=>'selfmanage', 'module' => 'Myaccount','middleware' => ['web'], 'namespace' => 'App\Modules\Myaccount\Controllers'), function() {
	// Route::get('/', 'MyaccountController@index');
	
	Route::get('/profile', 'MyaccountController@loadProfile');
	Route::get('/changepassword', 'MyaccountController@loadChangePassword');
	
	//Route::get('/new-booking', 'ManageBController@loadBooking');
	//Route::get('/manage-booking', 'MyaccountController@search');
	//Route::get('/booking-history', 'MyaccountController@loadBookingHistory');
	
	//Route::get('/booking/search/{tracking_number}', 'ManageBookingController@search');
	Route::resource('/booking/search', 'ManagingBookingController@search');
	
	Route::get('/booking/history', 'ManagingBookingController@history');
	Route::resource('/booking', 'ManagingBookingController');
	Route::resource('/booking/create', 'ManagingBookingController@create');
	Route::resource('/booking/display', 'ManagingBookingController@display');
    Route::get('/confirm', 'ManagingBookingController@confirm');
    Route::get('/complete', 'ManagingBookingController@complete');
    Route::get('/finish/{tracking_number}', 'ManagingBookingController@finished');
    Route::get('/proof-of-delivery', 'ProofOfDeliveryController@displayPODs');
    Route::get('/POD/{id}', 'ProofOfDeliveryController@viewPODFiles');


	Route::get('/manage-address/available-address', 'ManageAddressController@getAvailableAddress');
	Route::resource('/manage-address', 'ManageAddressController');
	Route::resource('/manage-address/create', 'ManageAddressController@create');
	
	//Route::get('/page/{slag}/{param?}', 'ManagingBookingController@page');
});	