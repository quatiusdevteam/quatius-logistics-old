<?php namespace App\Modules\Myaccount\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model {

	protected $fillable =[  'name', 
							'email',
							'password',
							'status',
							'api_token',
							'remember_token',
							'sex',
							'first_name',
							'middle_name',
							'last_name',
                            'company_name',
							'designation',							
							'phone_1',
							'phone_2',
							'address_1',
							'address_2',
							'city',
							'postcode',
							'state',
							'country',
							'photo',
							'permission',
							'pronto_user_id',
                            'discount',
					];

}
