<?php namespace App\Modules\Myaccount\Models;

use Illuminate\Database\Eloquent\Model;

class BookingStatus extends Model {

	//
	protected $fillable =[  'pronto_status_id', 
							'status',
							'allow_customer_change',
							'allow_staff_change'													
					];
}