<?php namespace App\Modules\Myaccount\Models;
use DB;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model {

	//
	protected $fillable =[  'user_id', 
							'consignment_number',
							'tracking_number',
							'status_id',
							'number_of_item',
							'length',
							'width',
							'height',
							'weight',
							'type',
                            'total',
                            'subtotal',
                            'fuel_levy',
                            'tax_total',
                            'discount',
                            'discount_info',
                            'warehouse_code',
                            'quote_reference',
                            'ql_contact',
                            'time_ready',
                            'shipment_method',
                            //'uom',
                            'vehicle_spec',
							'remark',
                            'booked_by'
					];
	
	public function addresses(){
		return $this->hasMany(BookingAddress::class);
	}
	
	public function activitylogs(){
		return $this->hasMany(BookingActivityLog::class);
	}
	public function statuses(){
		return $this->belongsTo(BookingStatus::class);
	}
	
	public function scopeWithStatus($query){
		$query->join(DB::raw('(select id as statusid, pronto_status_id, status, allow_customer_change, allow_staff_change FROM booking_statuses) AS bs'), function($join)
		{
			$join->on('status_id','=','bs.statusid');
		});
    	//$query->leftJoin('booking_statuses as bs','status_id','=','bs.id');
    	return $query;    
	}

	public function getOriginationAddress(){
	    return $this->addresses()->whereAddressType('source')->first();
    }

    public function getDestinationAddress(){
	    return $this->addresses()->whereAddressType('destination')->first();
    }

	public function scopeWithOriginationAddress($query){
		$query->join('booking_addresses', function($join){
			$join->on('bookings.id', '=', 'booking_addresses.booking_id')
				 ->where('booking_addresses.address_type', '=', 'source');
			});
		return $query;
	}

    public function scopeWithDestinationAddress($query){
        $query->join('booking_addresses', function($join){
            $join->on('bookings.id', '=', 'booking_addresses.booking_id')
                ->where('booking_addresses.address_type', '=', 'destination');
        });
        return $query;
    }
	
}
