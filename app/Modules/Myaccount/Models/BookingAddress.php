<?php namespace App\Modules\Myaccount\Models;

use Illuminate\Database\Eloquent\Model;

class BookingAddress extends Model {

	//
	protected $fillable =[  'booking_id', 
							'address_type',
							'company_name',
							'first_name',
							'last_name',
							'phone',
							'email',
							'address_1',
							'address_2',
							'city',
							'state',
							'postcode',
							'country',
                            'shipment_instruction',
					];
	
	public function booking(){
		return $this->belongsTo(Booking::class);
	}
}
