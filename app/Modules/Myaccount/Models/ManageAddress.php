<?php namespace App\Modules\Myaccount\Models;

use Illuminate\Database\Eloquent\Model;

class ManageAddress extends Model {

	//
	protected $fillable =[  'user_id', 
							'company_name',
							'first_name',
							'last_name',
							'phone',
							'email',
							'address_1',
							'address_2',
							'city',
							'state',
							'postcode',
							'country'
					];
}
