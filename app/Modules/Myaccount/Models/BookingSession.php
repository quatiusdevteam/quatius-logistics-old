<?php
/**
 * Created by PhpStorm.
 * User: vey.chea
 * Date: 2/10/2017
 * Time: 3:08 PM
 */

namespace App\Modules\Myaccount\Models;
use Session;
use Auth;
class BookingSession
{
    protected $session_key = "customer-booking";
    protected $origination_address;
    protected $destination_address;

    protected $fillable = [
        'consignment_number',
        'tracking_number',
        'status_id',
        'number_of_item',
        'type',
        'length',
        'total',
        'subtotal',
        'fuel_levy',
        'tax_total',
        'discount',
        'discount_info',
        'width',
        'height',
        'weight',
        'warehouse_code',
        'quote_reference',
        'ql_contact',
        'time_ready',
        'shipment_method',
        'remark',
        'booked_by'];

    protected $user = null;


   /* function __construct(Booking $booking) {
        if ($booking == null)
            $booking = new Booking();

        $this->loadFillableDatas($booking->toArray());


    }*/

    /**
     * @param array $data
     */
    function loadFillableDatas($data = []) {

        foreach ($data as $key=>$value) {
            $this->$key = $value;
            $this->fillable[] = $key;
        }
    }
    function toArray() {
        $data = [];
        foreach ($this->fillable as $key) {
            if(isset($this->$key)) {
                $data[$key] = $this->$key;
            }
        }
        return $data;
    }
    /**
     * @return mixed
     */
    public function getTotal(){
        return $this->total;
    }

    /**
     * @param bool $with_tax
     * @return mixed
     */
    public function getSubtotal($with_tax = true){
        if($with_tax)
            return $this->subtotal * 0.1;
        else
            return $this->subtotal;

    }

    /**
     * @return string
     */
    public function getBookingSessionKey(){
        return $this->session_key;
    }

    /**
     * @param null $session_key
     */
    public function setBookingSessionKey($session_key=null)
    {
        $this->session_key = ($session_key == null) ? $this->session_key : $session_key;
    }

    /**
     * @param null $booking
     * @param null $session_key
     * @return BookingSession|Session
     */
    public static function getBooking($booking = null, $session_key = 'customer-booking')
    {
        $store_booking = session($session_key, null);

        if($store_booking != null){
            if($store_booking->getBookingSessionKey() ==$session_key){
                return $store_booking;
            }
        }

        $store_booking = new BookingSession();
        if ($booking != null) {
            $store_booking->loadFillableDatas($booking->toArray());
        }

        $store_booking->setBookingSessionKey($session_key);
        $store_booking->saveBooking();

        return session($session_key, null);
    }

    /**
     * @param null $session_key
     * @return bool
     */
    public static function hasBooking($session_key = 'customer-booking'){
        return Session::has($session_key);
    }

    /**
     * @param null $session_key
     */
    public static function clearBooking($session_key = 'customer-booking'){
        session()->forget($session_key);
        session()->save();
    }

    /**
     *
     */
    protected function saveBooking(){
        session([$this->session_key => $this]);
        session()->save();
    }

    /**
     * @param int $status_id
     */
    public function saveToDatabase($status_id = 1){
        $this->saveBooking();
        $booking = new Booking();
        $this->status_id = $status_id;
        foreach($this->fillable as $key){
            if(isset($this->$key))
            $booking->$key =$this->$key;
        }
        $customer = $this->getBookingCustomer();
        if($customer){
            $loginUser = Auth::user();
            if($customer->id != $loginUser->id){
                $booking->user_id = $loginUser->id;
                $booking->booked_by = $loginUser->name;
            }else{
                $booking->user_id = $customer->id;
                $booking->booked_by = $customer->name;
            }
        }
        $booking->tracking_number = self::generateTrackingNumber();
        $booking->save();
        $booking->addresses()->create($this->origination_address);
        $booking->addresses()->create($this->destination_address);
        return $booking;
    }


    /**
     * @param $customer
     */
    public function setBookingCustomer($customer){
        $this->user = $customer;
    }

    /**
     * @return null
     */
    public function getBookingCustomer(){
        return $this->user;
    }

    /**
     * @return bool
     */
    public function hasOriginationAddress(){
        return !!$this->origination_address;
    }

    /**
     * @return bool
     */
    public function hasDestinationAddress(){
        return !!$this->destination_address;
    }

    /**
     * @param $sender
     */
    public function setOriginationAddress($sender){
        $this->origination_address = $sender;
        $this->saveBooking();
    }

    /**
     * @param $receiver
     */
    public function setDestinationAddress($receiver){
        $this->destination_address = $receiver;
        $this->saveBooking();
    }

    /**
     * @return mixed
     */
    public function getOriginationAddress(){
        return $this->origination_address;
    }

    /**
     * @return mixed
     */
    public function getDestinationAddress(){
        return $this->destination_address;
    }

    /**
     * @return string
     */
    public static function generateTrackingNumber(){
        $tracking_number = strtoupper(uniqid());
        if(Booking::whereTrackingNumber($tracking_number)->count()){
            return generateInvoiceNumber();
        }
        return $tracking_number;
    }
}
