<?php namespace App\Modules\Myaccount\Models;

use Illuminate\Database\Eloquent\Model;

class BookingActivityLog extends Model {

	//
	protected $fillable =[  'user_id', 
							'booking_id',
							'tracking_number',
							'title',
							'description',
							'remark',													
					];
	
	public function booking(){
		return $this->belongsTo(Booking::class);
	}
}
