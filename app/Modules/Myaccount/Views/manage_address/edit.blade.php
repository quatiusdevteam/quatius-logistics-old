<div class="box-header with-border">
    <h3 class="box-title"> {{ trans('cms.edit') }}  {{ trans('Myaccount::selfmanage.label.address') }} [{!!$address->company_name!!}] </h3>
    <div class="box-tools pull-right">
        <button type="button" class="btn btn-primary btn-sm" data-action='UPDATE' data-form='#edit-user'  data-load-to='#entry-user' data-datatable='#main-list'><i class="fa fa-floppy-o"></i> {{ trans('cms.save') }}</button>
        <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#entry-user' data-href='{{Trans::to('selfmanage/manage-address/0')}}'><i class="fa fa-times-circle"></i> {{ trans('cms.cancel') }}</button>
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body" >
    
        {!!Form::vertical_open()
        ->id('edit-user')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->action(URL::to('selfmanage/manage-address/'. $address->id))
        ->rules([
            'address_type'         => 'required|not_in:0
            'first_name'           => 'required|max:64',
            'phone_1' 			   => 'required|min:8|max:15',
            'address_1'			   => 'required|max:64',
            'city'				   => 'required|max:64',
            'state'				   => 'required|not_in:0',
            'postcode' 			   => 'required|max:4|min:4',
            'country'			   => 'required|not_in:0',
        ])!!}
        <div class="tab-content">
            @include('Myaccount::manage_address.partials.entry')
        </div>
        {!!Form::close()!!}
    
</div>
<div class="box-footer" >
    &nbsp;
</div>
