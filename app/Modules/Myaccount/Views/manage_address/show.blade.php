<div class="box-header with-border">
    <h3 class="box-title"> {{ trans('cms.view') }}   {{ trans('Myaccount::selfmanage.label.address') }}  [{{ $address->company_name}}]  </h3>
       <div class="box-tools pull-right">
        <button type="button" class="btn btn-success btn-sm" data-action='NEW' data-load-to='#entry-user' data-href='{{Trans::to('selfmanage/manage-address/create')}}'><i class="fa fa-times-circle"></i> {{ trans('cms.new') }}</button>
        @if($address->id)
        <button type="button" class="btn btn-primary btn-sm" data-action="EDIT" data-load-to='#entry-user' data-href='{{ trans_url('/selfmanage/manage-address') }}/{{$address->id}}/edit'><i class="fa fa-pencil-square"></i> {{ trans('cms.edit') }}</button>
        <button type="button" class="btn btn-danger btn-sm" data-action="DELETE" data-load-to='#entry-user' data-datatable='#main-list' data-href='{{ trans_url('/selfmanage/manage-address') }}/{{$address->id}}' >
            <i class="fa fa-times-circle"></i> {{ trans('cms.delete') }}
        </button>
        @endif
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body" >
    
        {!!Form::vertical_open()
        ->id('user-user-show')
        ->method('PUT')
        ->action(trans_url('selfmanage/manage-address/'. $address->id)).
        ->rules([
            'address_type'         => 'required|not_in:0
            'first_name'           => 'required|max:64',
            'phone_1' 			   => 'required|min:8|max:15',
            'address_1'			   => 'required|max:64',
            'city'				   => 'required|max:64',
            'state'				   => 'required|not_in:0',
            'postcode' 			   => 'required|max:4|min:4',
            'country'			   => 'required|not_in:0',
        ])!!}
            <div class="tab-content">
                @include('Myaccount::manage_address.partials.entry')
            </div>
        {!! Form::close() !!}
    
</div>
<div class="box-footer" >
    &nbsp;
</div>
