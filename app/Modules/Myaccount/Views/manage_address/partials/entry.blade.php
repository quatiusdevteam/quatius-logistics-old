<div class="box box-success">
    	<div class="row">
	        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	        {!! Form::text('company_name')
	        -> label(trans('Myaccount::selfmanage.label.company_name'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.company_name'))!!}
	        </div>       
	    </div>    
	    <div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('first_name')
	        -> label(trans('Myaccount::selfmanage.label.first_name'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.first_name'))!!}
	        </div>
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('last_name')
	        -> label(trans('Myaccount::selfmanage.label.last_name'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.last_name'))!!}
	        </div>
	    </div>
		<div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('phone')
	        -> label(trans('Myaccount::selfmanage.label.phone'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.phone'))!!}
	        </div>
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('email')
	        -> label(trans('Myaccount::selfmanage.label.email'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.email'))!!}
	        </div>
	    </div>
	    <div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('address_1')
	        -> label(trans('Myaccount::selfmanage.label.address_1'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.address_1'))!!}
	        </div>	 
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('address_2')
	        -> label(trans('Myaccount::selfmanage.label.address_2'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.address_2'))!!}
	        </div>      
	    </div>
	   <div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('city')
	        -> label(trans('Myaccount::selfmanage.label.city'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.city'))!!}
	        </div>	 
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::select('state')			                    
			-> options(trans('Myaccount::selfmanage.options.states'))
			-> label(trans('Myaccount::selfmanage.label.state'))
			-> placeholder(trans('Myaccount::selfmanage.placeholder.state'))!!}
	        </div>      
	    </div>
	    <div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::select('country')			       
	        -> label(trans('Myaccount::selfmanage.label.country'))             
			-> options(trans('Myaccount::selfmanage.options.countries'))
			-> placeholder(trans('Myaccount::selfmanage.placeholder.country'))!!}
	        </div>	 
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('postcode')
	        -> label(trans('Myaccount::selfmanage.label.postcode'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.postcode'))!!}
	        </div>      
	    </div>	    
    </div>