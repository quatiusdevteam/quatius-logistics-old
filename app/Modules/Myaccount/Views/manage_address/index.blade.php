@extends('admin::curd.index')
@section('heading')
<i class="fa fa-flag-checkered"></i> {!! trans('Myaccount::selfmanage.label.address') !!} <small> {!! trans('cms.manage') !!} {!! trans('Myaccount::selfmanage.label.address') !!}</small>
@stop
@section('title')
{!! trans('Myaccount::selfmanage.label.address') !!}
@stop
@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{!! URL::to('admin') !!}"><i class="fa fa-dashboard"></i> {!! trans('cms.home') !!} </a></li>
    <li class="active">{!! trans('Myaccount::selfmanage.manage_address')!!}</li>
</ol>
@stop
@section('entry')
<div class="box box-warning" id='entry-user'>
</div>
@stop

@section('content')
<table id="main-list" class="table display responsive nowrap">
    <thead>
        <th>{!! trans('Myaccount::selfmanage.label.company_name')!!}</th>
        <th>{!! trans('Myaccount::selfmanage.label.contact_name')!!}</th>
        <th>{!! trans('Myaccount::selfmanage.label.phone')!!}</th>
        <th>{!! trans('Myaccount::selfmanage.label.address')!!}</th>
        <th>{!! trans('Myaccount::selfmanage.label.last_update')!!}</th>
    </thead>
</table>
@stop
@section('script')
<script type="text/javascript">
var oTable;
$(document).ready(function(){
    $('#entry-user').load('{{URL::to('selfmanage/manage-address/0')}}');
    oTable = $('#main-list').DataTable( {
        "ajax": '{{ URL::to('/selfmanage/manage-address') }}',
        "columns": [
        { "data": "company_name" },
        { "data": "first_name", "className": "hidden-xs"},
        { "data": "phone", "className": "hidden-xs" },
        { "data": "address_1" },
        { "data": "updated_at", "className": "hidden-xs" },],
        responsive: {
            details: {
                type: 'column'
            }
        },
        "columnDefs": [
						{
							"render": function ( data, type, row ) {
									return data + ' ' + row.last_name;
						 	},"targets": 1
						},
						{
							"render": function ( data, type, row ) {
									var address_1 = (data != null)?data : '';
									var address_2 = (row.address_2 != null)?row.address_2 : '';
									var city = (row.city != null)?row.city : '';
									var state = (row.state != null)?row.state : '';
									var country = (row.country != null)?row.country : '';
									var postcode = (row.postcode != null)?row.postcode : '';
									return address_1 + ' ' + address_2 + ' ' + city + ' ' + state + ' ' + country + ' ' + postcode;
						 	},"targets": 3
						}
                       ],
        "userLength": 50
    });
    $('#main-list tbody').on( 'click', 'tr', function () {
        
        $(this).toggleClass("selected").siblings(".selected").removeClass("selected");
        var d = $('#main-list').DataTable().row( this ).data();
        $('#entry-user').load('{{URL::to('selfmanage/manage-address')}}' + '/' + d.id);
        
    });

   
});
</script>
@stop
@section('style')
@stop
