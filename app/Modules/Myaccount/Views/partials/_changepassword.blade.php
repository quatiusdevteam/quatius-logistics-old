
<div class="content-wrapper">
	<section class="content-header">
		<h1 style="padding: 10px 0;">Change Password</h1>
	</section>
	<section class="content-body">
	    @include('public::notifications')
		<div class="panel panel-default">
		    <div class="panel-body">
		    	{!!Form::vertical_open()
			      ->id('contact')
			      ->method('POST')
			      ->class('change-password')!!}
			      <div class="row">
				      	<div class="col-xs-12">
				      		{!! Form::password('old_password')
						      -> label(trans('user::user.user.label.current_password'))
						      -> placeholder(trans('user::user.user.placeholder.current_password'))!!}
				      	</div>				      	
			      </div>
			      <div class="row">
				      	<div class="col-xs-12">
				      		{!! Form::password('password')
						      -> label(trans('user::user.user.label.new_password'))
						      -> placeholder(trans('user::user.user.placeholder.new_password'))!!}
				      	</div>				      	
			      </div>
			      <div class="row">
				      	<div class="col-xs-12">
				      		 {!! Form::password('password_confirmation')
						      -> label(trans('user::user.user.label.new_password_confirmation'))
						      -> placeholder(trans('user::user.user.placeholder.new_password_confirmation'))!!}
				      	</div>				      	
			      </div>
			      <div class="row">
				      	<div class="col-xs-12">
				      		 {!! Form::submit(trans('user::user.changepword'))->class('btn btn-primary')!!}
				      	</div>				      	
			      </div>
		    </div>	    	
		</div>	
	</section>	

</div>

