
<div class="content-wrapper">
	<section class="content-header">
		<h1 style="padding: 10px 0;">{!!trans('Myaccount::selfmanage.manage_booking')!!}</h1>
	</section>
	<br />
	<section class="content-body">    	
		<div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4 ">
			<div>
				@include('public::notifications')
		    	
		    	{!!Form::vertical_open()
			    ->id('contact')
			    ->method('POST')
			   
			    !!}
			    <div class="form-group">
			    <label for="start_date">{{trans('Myaccount::selfmanage.label.start_date')}}</label>
			    <input type="date" class="form-control" id="start_date" name="start_date">
			  </div>
			  <div class="form-group">
			    <label for="end_date">{{trans('Myaccount::selfmanage.label.end_date')}}</label>
			    <input type="date" class="form-control" id="end_date" name="end_date" >
			  </div>
			 
			  <button type="submit" class="btn btn-flat btn-info">Submit</button>
			</div>			
		</div>
		<div class="clearfix"></div>
		<br />
		<div class="col-xs-12" id="booking_list" style="display: none;">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body">
					</div>
				</div>
			</div>			
		</div>
	</section>
</div>