
<div class="content-wrapper">
	<section class="content-header">
		<h1 style="padding: 10px 0;">{!!trans('Myaccount::selfmanage.new_booking')!!}</h1>
	</section>
	<section class="content-body">
    @include('public::notifications')
<div class="panel panel-default">
   
    <div class="panel-body">

    {!!Form::vertical_open()
    ->id('contact')
    ->method('POST')
    ->class('update-profile')!!}
    <div class="row">
    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-title">
    		<h4>{!!trans('Myaccount::selfmanage.your_detail')!!}</h4>
    	</div>
    </div>
    <div class="box box-warning">
    	<div class="row">
	        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	        {!! Form::text('company_name')
	        -> label(trans('Myaccount::selfmanage.label.company_name'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.company_name'))!!}
	        </div>       
	    </div>    
	    <div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('first_name')
	        -> label(trans('Myaccount::selfmanage.label.first_name'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.first_name'))!!}
	        </div>
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('last_name')
	        -> label(trans('Myaccount::selfmanage.label.last_name'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.last_name'))!!}
	        </div>
	    </div>
		<div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('phone')
	        -> label(trans('Myaccount::selfmanage.label.phone'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.phone'))!!}
	        </div>
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('email')
	        -> label(trans('Myaccount::selfmanage.label.email'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.email'))!!}
	        </div>
	    </div>	   
    </div>
    
    <!-- sender detail -->
    
     <div class="row">
    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-title">
    		<h4>{!!trans('Myaccount::selfmanage.sender_detail')!!}</h4>
    		<span class="pull-right">
    			[<a href="#">Choose from address list</a>]
    		</span>
    	</div>
    </div>    
    <div class="box box-success">
    	<div class="row">
	        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	        {!! Form::text('sender_company_name')
	        -> label(trans('Myaccount::selfmanage.label.company_name'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.company_name'))!!}
	        </div>       
	    </div>    
	    <div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('sender_first_name')
	        -> label(trans('Myaccount::selfmanage.label.first_name'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.first_name'))!!}
	        </div>
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('sender_last_name')
	        -> label(trans('Myaccount::selfmanage.label.last_name'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.last_name'))!!}
	        </div>
	    </div>
		<div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('sender_phone')
	        -> label(trans('Myaccount::selfmanage.label.phone'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.phone'))!!}
	        </div>
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('sender_email')
	        -> label(trans('Myaccount::selfmanage.label.email'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.email'))!!}
	        </div>
	    </div>
	    <div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('sender_address1')
	        -> label(trans('Myaccount::selfmanage.label.address1'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.address1'))!!}
	        </div>	 
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('sender_address_2')
	        -> label(trans('Myaccount::selfmanage.label.address2'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.address2'))!!}
	        </div>      
	    </div>
	   <div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('sender_city')
	        -> label(trans('Myaccount::selfmanage.label.city'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.city'))!!}
	        </div>	 
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::select('sender_state')			                    
			-> options(trans('Myaccount::selfmanage.options.states'))
			-> label(trans('Myaccount::selfmanage.label.state'))
			-> placeholder(trans('Myaccount::selfmanage.placeholder.state'))!!}
	        </div>      
	    </div>
	    <div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::select('sender_country')			       
	        -> label(trans('Myaccount::selfmanage.label.country'))             
			-> options(trans('Myaccount::selfmanage.options.countries'))
			-> placeholder(trans('Myaccount::selfmanage.placeholder.country'))!!}
	        </div>	 
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('sender_postcode')
	        -> label(trans('Myaccount::selfmanage.label.postcode'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.postcode'))!!}
	        </div>      
	    </div>	    
    </div>
    
    <!-- Consignment detail -->
    <div class="row">
    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-title">
    		<h4>{!!trans('Myaccount::selfmanage.consignment_detail')!!}</h4>
    	</div>
    </div> 
   <div class="box box-danger">
    	<div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::number('item_qty')
	        -> label(trans('Myaccount::selfmanage.label.num_item'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.num_item'))
	        -> min('1')!!}
	        </div>
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        	<div class="row">
	        		<div class="col-xs-4">
		        		{!! Form::number('item_length')
				        -> label(trans('Myaccount::selfmanage.label.length'))
				        -> placeholder(trans('Myaccount::selfmanage.placeholder.length'))!!}
		        	</div>
		        	<div class="col-xs-4">
		        		{!! Form::number('item_width')
				        -> label(trans('Myaccount::selfmanage.label.width'))
				        -> placeholder(trans('Myaccount::selfmanage.placeholder.width'))!!}
		        	</div>
		        	<div class="col-xs-4">
		        		{!! Form::number('item_height')
				        -> label(trans('Myaccount::selfmanage.label.height'))
				        -> placeholder(trans('Myaccount::selfmanage.placeholder.height'))!!}
		        	</div>
	        	</div>
	        </div>     
	    </div>
	    <div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('item_weight')
	        -> label(trans('Myaccount::selfmanage.label.weight'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.weight'))!!}
	        </div>
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::label('hazardous') !!}<br />
	        
	        	<div class="pull-left" style="margin-right:50px;">
	        		<input type="radio" name="item_hazardous" id="hazard_yes" value="yes" required="required">
	        		<label for="hazard_yes"> YES</label>
	        	</div>
	        	<div>
	        		<input type="radio" name="item_hazardous" id="hazard_no" value="no" required="required">
	        		<label for="hazard_no"> NO</label>
	        	</div>
	                        
	        </div>     
	    </div>
	     <div class="row">
	        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	        	{!! Form::textarea('item_remark')
	        	-> label(trans('Myaccount::selfmanage.label.remark')) !!}
	        </div>
	     </div>
	</div>
    
    <!-- Receiver detail -->
    <div class="row">
    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-title">
    		<h4>{!!trans('Myaccount::selfmanage.receiver_detail')!!}</h4>
    		<span class="pull-right">
    			[<a href="#">Choose from address list</a>]
    		</span>
    	</div>
    </div> 
   <div class="box box-info">
    	<div class="row">
	        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	        {!! Form::text('sender_company_name')
	        -> label(trans('Myaccount::selfmanage.label.company_name'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.company_name'))!!}
	        </div>       
	    </div>    
	    <div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('sender_first_name')
	        -> label(trans('Myaccount::selfmanage.label.first_name'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.first_name'))!!}
	        </div>
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('sender_last_name')
	        -> label(trans('Myaccount::selfmanage.label.last_name'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.last_name'))!!}
	        </div>
	    </div>
		<div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('sender_phone')
	        -> label(trans('Myaccount::selfmanage.label.phone'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.phone'))!!}
	        </div>
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('sender_email')
	        -> label(trans('Myaccount::selfmanage.label.email'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.email'))!!}
	        </div>
	    </div>
	    <div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('sender_address1')
	        -> label(trans('Myaccount::selfmanage.label.address1'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.address1'))!!}
	        </div>	 
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('sender_address_2')
	        -> label(trans('Myaccount::selfmanage.label.address2'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.address2'))!!}
	        </div>      
	    </div>
	   <div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('sender_city')
	        -> label(trans('Myaccount::selfmanage.label.city'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.city'))!!}
	        </div>	 
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::select('sender_state')			                    
			-> options(trans('Myaccount::selfmanage.options.states'))
			-> label(trans('Myaccount::selfmanage.label.state'))
			-> placeholder(trans('Myaccount::selfmanage.placeholder.state'))!!}
	        </div>      
	    </div>
	    <div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::select('sender_country')			       
	        -> label(trans('Myaccount::selfmanage.label.country'))             
			-> options(trans('Myaccount::selfmanage.options.countries'))
			-> placeholder(trans('Myaccount::selfmanage.placeholder.country'))!!}
	        </div>	 
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('sender_postcode')
	        -> label(trans('Myaccount::selfmanage.label.postcode'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.postcode'))!!}
	        </div>      
	    </div>	    
    </div>
    
	
	<div class="pull-right">
      	{!! Form::reset(trans('cms.reset'))->class('btn btn-default')->val('Reset')!!}
      	{!! Form::submit('Submit Booking')->class('btn btn-primary')!!}
      	
    </div>
    <br>
    <br>

    {!! Form::close() !!}
    </div>
</div>
	
	</section>	

</div>

