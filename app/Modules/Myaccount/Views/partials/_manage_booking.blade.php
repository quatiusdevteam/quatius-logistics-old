
<div class="content-wrapper">
	<section class="content-header">
		<h1 style="padding: 10px 0;">{!!trans('Myaccount::selfmanage.manage_booking')!!}</h1>
	</section>
	<br />
	<section class="content-body">    	
		<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-1">
			<div>
				@include('public::notifications')
		    	
		    	{!!Form::vertical_open()
			    ->id('contact')
			    ->method('POST')
			    ->class('form-horizontal')!!}
		    	
		    	<div class="form-group">
				    <label for="inputEmail3" class="col-xs-12 col-sm-4 control-label">Booking number:</label>
				    <div class="col-sm-8">
				      <div class="input-group">
				        <input type="text" name="q" class="form-control" placeholder="Booking number...">
				        <span class="input-group-btn">
				          <button type="submit" name="search" id="search-btn" class="btn btn-info"><i class="fa fa-search"></i></button>
				        </span>
				      </div>
				    </div>
				    
				  </div>
			</div>			
		</div>
		<div class="clearfix"></div>
		<br />
		<div class="col-xs-12" id="booking_list" style="display: none;">
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-body">
					</div>
				</div>
			</div>			
		</div>
	</section>
</div>