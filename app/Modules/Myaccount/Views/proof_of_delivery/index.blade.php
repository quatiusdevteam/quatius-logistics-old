@extends('admin::curd.index')
@section('heading')
	<i class="fa fa-truck"></i> {!! trans('Myaccount::selfmanage.label.proof_of_delivery') !!}
@stop
@section('title')

@stop
@section('tools')

@stop
@section('breadcrumb')
	<ol class="breadcrumb">
		<li><a href="{!! URL::to('admin') !!}"><i class="fa fa-dashboard"></i> {!! trans('cms.home') !!} </a></li>
		<li class="active">{!! trans('Myaccount::selfmanage.label.proof_of_delivery') !!}</li>
	</ol>
@stop
@section('entry')

@stop

@section('content')
	<table id="pod-table" class="table table-striped table-bordered"></table>
@stop
@section('script')
	<script>
      $(document).ready(function () {
        let listing
        if ('{!! $pronto_account !!}' === '*') {
          $('.main-sidebar').css('display', 'none')
          $('.content-wrapper').css('margin-left', 0)
          listing = {!! $listing !!}
        } else {
          listing = {!! $listing !!}.filter(entry => entry['Customer'] === '{!! $pronto_account !!}')
        }
console.log('{!! $pronto_account !!}')
        $('#pod-table').DataTable({
          data: listing,
          columns: [
            {
              title: 'Account',
              data: 'Name'
            },
            {
              title: 'Order No.',
              data: 'Order No'
            },
            {
              title: 'Reference',
              data: 'Reference',
              render: function (data) {
                return `<a href="/pronto/POD/${data}.jpg" target="_blank">${data}</a>`
              }
            }
          ],
          order: [[1, 'desc']]
        })
      })
	</script>
@stop
@section('style')
@stop
