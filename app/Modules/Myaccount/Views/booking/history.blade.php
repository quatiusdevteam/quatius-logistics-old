@extends('admin::curd.index')
@section('heading')
<i class="fa a-location-arrow"></i> {!! trans('Myaccount::selfmanage.booking_history') !!} 
@stop
@section('title')
{!! trans('Myaccount::selfmanage.booking_history') !!}
@stop
@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{!! URL::to('admin') !!}"><i class="fa fa-dashboard"></i> {!! trans('cms.home') !!} </a></li>
    <li class="active">{!! trans('Myaccount::selfmanage.booking_history')!!}</li>
</ol>
@stop
@section('entry')
<div id="entry-user">

</div>
@stop
@section('content')
<table id="main-list" class="display table  nowrap">
    <thead>
        <th>{!! trans('Myaccount::selfmanage.label.tracking_number')!!}</th>
        <th>{!! trans('Myaccount::selfmanage.label.status')!!}</th>
        <th>{!! trans('Myaccount::selfmanage.label.num_item')!!}</th>
        <th>{!! trans('Myaccount::selfmanage.label.shipment_method')!!}</th>
        <th>{!! trans('Myaccount::selfmanage.label.destination')!!}</th>
        
    </thead>
</table>
@stop
@section('script')
<script type="text/javascript">
var oTable;
$(document).ready(function(){
	$('#main-list').DataTable( {
        "ajax": '{{ URL::to('/selfmanage/booking/history') }}',
        "columns": [
			        { "data": "tracking_number" },
			        { "data": "status" },
			        { "data": "number_of_item" },
			        { "data": "shipment_method", "className": "hidden-xs"},
			        { "data": "address_1", "className": "hidden-xs" },			        			        
			       ],
       "columnDefs": [
					{
						"render": function ( data, type, row ) {
								var address_1 = (data != null)?data : '';
								var address_2 = (row.address_2 != null)?row.address_2 : '';
								var city = (row.city != null)?row.city : '';
								var state = (row.state != null)?row.state : '';
								var country = (row.country != null)?row.country : '';
								var postcode = (row.postcode != null)?row.postcode : '';
								return address_1 + ' ' + address_2 + ' ' + city + ' ' + state + ' ' + country + ' ' + postcode;
					 	},"targets": 4
					}
   			       ], 
        "userLength": 50
    });
    $('#main-list tbody').on( 'click', 'tr', function () {
        
        $(this).toggleClass("selected").siblings(".selected").removeClass("selected");
        var d = $('#main-list').DataTable().row( this ).data();
        $('#entry-user').load('{{URL::to('selfmanage/booking/display')}}' + '/' + d.booking_id);
        
    });

   
});
</script>
@stop
@section('style')
@stop
