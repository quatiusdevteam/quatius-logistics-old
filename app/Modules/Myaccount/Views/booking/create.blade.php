<div class="box-header with-border">
    <h3 class="box-title"> {{ trans('cms.new') }}  {{ trans('Myaccount::selfmanage.label.booking') }} </h3>
    <div class="box-tools pull-right">
        <button type="button" class="btn btn-primary btn-sm" data-action='CREATE' data-load-to='#entry-user' data-form='#create-booking' ><i class="fa fa-floppy-o"></i> {{ trans('cms.save') }}</button>
        <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#entry-user' data-href='{{Trans::to('selfmanage/booking/0')}}'><i class="fa fa-times-circle"></i> {{ trans('cms.cancel') }}</button>
       
    </div>
</div>
<div class="box-body" >
           
        {!!Form::vertical_open()
        ->id('create-booking')
        ->method('POST')
        ->files('true')
        ->action(URL::to('selfmanage/booking'))!!}
        <div class="tab-content">
            @include('Myaccount::booking.partials.entry')
        </div>
    {!! Form::close() !!}
   
</div>

<script>

	$(document).ready(function(){        
        $(".showSavedAddress").show();
    });
	
</script>
<div class="box-footer" >
    &nbsp;
</div>
