<div class="box-header with-border">
   
</div>
<div class="box-body" >
    	
        {!!Form::vertical_open()
        ->id('user-user-show')
        ->method('PUT')
        ->action(trans_url('selfmanage/booking/'. $booking->id))!!}
            <div class="tab-content">
                @include('Myaccount::booking.partials.review')
            </div>
        {!! Form::close() !!}
    
</div>
<script>
$(document).ready(function(){
    var cur_url = window.location.href;
    var ref = cur_url.lastIndexOf("/");
    var is_search = cur_url.substring((ref +1), (ref + 7)).toLowerCase();
   
    if(is_search == "search"){
    	location.reload();
    }
});
</script>
<div class="box-footer" >
    &nbsp;
</div>
