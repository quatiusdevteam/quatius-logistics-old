<div class="box-header with-border">
    <h3 class="box-title"> {{ trans('cms.view') }}   {{ trans('Myaccount::selfmanage.label.booking') }}  [{{ $booking->tracking_number}}]  </h3>
       <div class="box-tools pull-right">
        <button type="button" class="btn btn-success btn-sm" data-action='NEW' data-load-to='#entry-user' data-href='{{Trans::to('selfmanage/booking/create')}}'><i class="fa fa-times-circle"></i> {{ trans('cms.new') }}</button>
        
        @if($can_edit)
        <button type="button" class="btn btn-primary btn-sm" data-action="EDIT" data-load-to='#entry-user' data-href='{{ trans_url('/selfmanage/booking') }}/{{$booking->id}}/edit'><i class="fa fa-pencil-square"></i> {{ trans('cms.edit') }}</button>
        <button type="button" class="btn btn-danger btn-sm" data-action="DELETE" data-load-to='#entry-user' data-datatable='#main-list' data-href='{{ trans_url('/selfmanage/booking') }}/{{$booking->id}}' >
            <i class="fa fa-times-circle"></i> {{ trans('cms.delete') }}
        </button>
        @endif
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body" >
    	
        {!!Form::vertical_open()
        ->id('user-user-show')
        ->method('PUT')
        ->action(trans_url('selfmanage/booking/'. $booking->id))!!}
            <div class="tab-content">
                @include('Myaccount::booking.partials.entry')
            </div>
        {!! Form::close() !!}
    
</div>
<script>
$(document).ready(function(){
    var cur_url = window.location.href;
    var ref = cur_url.lastIndexOf("/");
    var is_search = cur_url.substring((ref +1), (ref + 7)).toLowerCase();
   
    if(is_search == "search"){
    	location.reload();
    }
});
</script>
<div class="box-footer" >
    &nbsp;
</div>
