<div class="box-header with-border">
    <h3 class="box-title"> {{ trans('cms.edit') }}  {{ trans('Myaccount::selfmanage.label.booking') }} [{!!$booking->tracking_number!!}] </h3>
    <div class="box-tools pull-right">
        <button type="button" class="btn btn-primary btn-sm" data-action='UPDATE' data-form='#edit-user'  data-load-to='#entry-user' data-datatable='#main-list'><i class="fa fa-floppy-o"></i> {{ trans('cms.save') }}</button>

        <span id='how-return'></span>
        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
    </div>
</div>
<div class="box-body" >
    
        {!!Form::vertical_open()
        ->id('edit-user')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->action(URL::to('selfmanage/booking/'. $booking->id))!!}
        	{!! Form::hidden('return')
	        -> value('')
	       	!!}
        <div class="tab-content">
            @include('Myaccount::booking.partials.entry')
        </div>
        {!!Form::close()!!}
    
</div>
<script>
    $(document).ready(function(){
        var cur_url = window.location.href;
        var ref = cur_url.lastIndexOf("/");
        var is_search = cur_url.substring((ref +1), (ref + 7)).toLowerCase();
        var key = cur_url.substring((cur_url.lastIndexOf("q=") + 2), cur_url.length);
        if(key.indexOf("&") == -1)
            var val = key;
        else
            var val = key.substring(0, key.indexOf("&")); 
        
        if(is_search == "search"){
            $('span#how-return').html('<a href="' + cur_url + '" class="btn btn-default btn-sm">' + '<i class="fa fa-times-circle"></i> {{ trans('cms.cancel') }} ' + '</a>');
            $('input[name="return"]').val(val); 
        }else{
        	$('span#how-return').html('<button type="button" class="btn btn-default btn-sm" data-action="CANCEL" data-load-to="#entry-user" data-href="{{Trans::to('selfmanage/booking/0')}}"><i class="fa fa-times-circle"></i> {{ trans('cms.cancel') }}</button>');
        	$('input[name="return"]').val('');
        }
        $(".showSavedAddress").show();
    });
	
</script>
<div class="box-footer" >
    &nbsp;
</div>
