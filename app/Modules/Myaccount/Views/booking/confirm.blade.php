@extends('admin::curd.index')
@section('heading')
    <i class="fa fa-location-arrow"></i> {!! trans('Myaccount::selfmanage.label.booking') !!}
@stop
@section('title')

title
@stop
@section('breadcrumb')bread
    <ol class="breadcrumb">
        <li><a href="{!! URL::to('admin') !!}"><i class="fa fa-dashboard"></i> {!! trans('cms.home') !!} </a></li>
        <li class="active">{!! trans('Myaccount::selfmanage.new_booking')!!}</li>
    </ol>
@stop
@section('tools')
tool
@stop


@section('content')
    <div class="box-header with-border">
        <h3 class="box-title"> {{ trans('cms.new') }}  {{ trans('Myaccount::selfmanage.label.booking') }} </h3>
        <div class="box-tools pull-right">
            <a href="{{Trans::to('selfmanage/complete')}}" type="button" class="btn btn-primary">
                {!! trans('Myaccount::selfmanage.title.confirm_booking') !!}
            </a>
            <a href="{{Trans::to('selfmanage/booking')}}" class="btn btn-default">
                <i class="fa fa-times-circle"></i> {{ trans('cms.cancel') }}
            </a>
        </div>
    </div>
    <div class="clearfix"></div><br />
        <div id="search-result" class="box box-warning" >
        @include('Myaccount::booking.partials.review')
        </div>/
@stop

@section('script')

@stop
@section('style')
@stop