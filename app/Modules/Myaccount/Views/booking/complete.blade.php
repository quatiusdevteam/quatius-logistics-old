<?php
/**
 * Created by PhpStorm.
 * User: vey.chea
 * Date: 4/10/2017
 * Time: 9:38 AM
 */
?>
@extends('admin::curd.index')
@section('heading')
    <i class="fa fa-location-arrow"></i> {!! trans('Myaccount::selfmanage.label.booking') !!}
@stop
@section('title')


@stop
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{!! URL::to('admin') !!}"><i class="fa fa-dashboard"></i> {!! trans('cms.home') !!} </a></li>
        <li class="active">{!! trans('Myaccount::selfmanage.new_booking')!!}</li>
    </ol>
@stop



@section('content')
    <div style="text-align: center">
        <h1>
            Thank you for your booking.
        </h1>
        <br>
        <h4>Your tracking number is: {!! $tracking_number !!}</h4>
        <br><br>
        <a href="{!! url('selfmanage/booking') !!}" class="btn btn-lg btn-primary">Make new booking</a>
    </div>


@stop

@section('script')

@stop
@section('style')
@stop