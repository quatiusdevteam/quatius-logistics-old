@extends('admin::curd.index')
@section('heading')
<i class="fa fa-location-arrow"></i> {!! trans('Myaccount::selfmanage.label.booking') !!} 
@stop
@section('title')

@stop
@section('tools')

@stop
@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{!! URL::to('admin') !!}"><i class="fa fa-dashboard"></i> {!! trans('cms.home') !!} </a></li>
    <li class="active">SEARCH</li>
</ol>
@stop
@section('entry')

@stop

@section('content')
<div id='entry-user'>	  	
	<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-1">
		<div>
			@include('public::notifications')
	    	
	    	{!!Form::open()
		    ->id('search-booking')
		    ->method('GET')		    
		    ->action(URL::to('selfmanage/booking/search'))
		    ->class('form-horizontal')!!}
	    	
	    	<div class="form-group">
			    <label for="inputEmail3" class="col-xs-12 col-sm-4 control-label">Tracking number:</label>
			    <div class="col-sm-8">
			      <div class="input-group">
			        <input type="text" name="q" id="search" class="form-control" placeholder="Tracking number..." autocomplete="off" value="{{$q}}">
			        <span class="input-group-btn">			          
			          <button type="submit" class="btn btn btn-info search-booking"><i class="fa fa-search"></i></button>
			        </span>
			      </div>
			    </div>
			    
			  </div>
		</div>			
	</div>		
<div class="clearfix"></div><br />
<div id="search-result" class="box box-warning" style='display:<?php echo ($q != "")?"show":"none";?>'>
<br />
@if($booking == "not found")
	<div class="col-md-12 col-xs-12">
		Not recound found
	</div>
@else
		@include('Myaccount::booking.partials.review')
@endif
</div>

@stop
@section('script')
<script type="text/javascript">
$(document).ready(function(){
	//$('#search-result').load('{{URL::to('selfmanage/booking/search')}}' + '/0');
	$(".search-booking").click(function(){

		//search();
	});
});

function search(){
	var q = $('input[name="q"]').val().trim();
	console.log(q);
	//$('#search-result').load('{{URL::to('selfmanage/booking/search')}}' + '/' + q);
}

</script>
@stop
@section('style')
@stop
