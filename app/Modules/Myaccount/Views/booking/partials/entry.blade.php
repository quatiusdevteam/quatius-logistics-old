@if(user('web')->hasRole('admin_tmp'))
<div class="box box-success">
    <div class="row">
    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-title">
    		<h4>{!!trans('Myaccount::selfmanage.your_detail')!!}</h4>
    	</div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        {!! Form::text('first_name')
        -> label(trans('Myaccount::selfmanage.label.first_name'))
        -> placeholder(trans('Myaccount::selfmanage.placeholder.first_name'))
        -> required('required')!!}
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        {!! Form::text('last_name')
        -> label(trans('Myaccount::selfmanage.label.last_name'))
        -> placeholder(trans('Myaccount::selfmanage.placeholder.last_name'))
        -> required('required')!!}
        </div>
    </div>
	<div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        {!! Form::text('phone_1')
        -> label(trans('Myaccount::selfmanage.label.phone'))
        -> placeholder(trans('Myaccount::selfmanage.placeholder.phone_1'))
        -> required('required')!!}
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        {!! Form::text('email')
        -> label(trans('Myaccount::selfmanage.label.email'))
        -> placeholder(trans('Myaccount::selfmanage.placeholder.email'))
        -> required('required')!!}
        </div>
    </div>	   
</div> 
@endif
    <!-- sender detail -->
   
     <div class="row showSavedAddress" style="display:none;">
    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-title">
    		<h4>{!!trans('Myaccount::selfmanage.sender_detail')!!}</h4>
    		<span class="pull-right">
    			[<span style="cursor:pointer" class="loadSelectAddress" data-target="sender">Choose from address list</span>]
    		</span>
    	</div>
    </div> 
   
   <div class="box box-info" id="sender">
    	<div class="row">
	        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	        	<div class="form-group required">
	        		<label for="sender_company_name" class="control-label">{!!trans('Myaccount::selfmanage.label.company_name')!!} <sup>*</sup></label>
	        		<input class="form-control company_name" placeholder="{!!trans('Myaccount::selfmanage.placeholder.company_name')!!}" required="required" id="sender_company_name" type="text" name="sender_company_name" value="{{ (sizeof($sender_address)>0)?$sender_address->company_name:null }}">
	        	</div>
	        </div>       
	    </div>    
	    <div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        	<div class="form-group required">
	        		<label for="sender_first_name" class="control-label">{!!trans('Myaccount::selfmanage.label.first_name')!!} <sup>*</sup></label>
	        		<input class="form-control first_name" placeholder="{!!trans('Myaccount::selfmanage.placeholder.first_name')!!}" required="required" id="sender_first_name" type="text" name="sender_first_name" value="{{ (sizeof($sender_address)>0)?$sender_address->first_name:null }}">
	        	</div>	       
	        </div>
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        	<div class="form-group required">
	        		<label for="sender_last_name" class="control-label">{!!trans('Myaccount::selfmanage.label.last_name')!!} <sup>*</sup></label>
	        		<input class="form-control last_name" placeholder="{!!trans('Myaccount::selfmanage.placeholder.last_name')!!}" required="required" id="sender_last_name" type="text" name="sender_last_name" value="{{ (sizeof($sender_address)>0)?$sender_address->last_name:null }}">
	        	</div>
	        </div>
	    </div>
		<div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        	<div class="form-group required">
	        		<label for="sender_phone" class="control-label">{!!trans('Myaccount::selfmanage.label.phone')!!} <sup>*</sup></label>
	        		<input class="form-control phone" placeholder="{!!trans('Myaccount::selfmanage.placeholder.phone')!!}" required="required" id="sender_phone" type="tel" name="sender_phone" value="{{ (sizeof($sender_address)>0)?$sender_address->phone:null }}">
	        	</div>
	        </div>
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		        <div class="form-group required">
	        		<label for="sender_email" class="control-label">{!!trans('Myaccount::selfmanage.label.email')!!} <sup>*</sup></label>
	        		<input class="form-control email" placeholder="{!!trans('Myaccount::selfmanage.placeholder.email')!!}" required="required" id="sender_email" type="email" name="sender_email" value="{{ (sizeof($sender_address)>0)?$sender_address->email:null }}">
	        	</div>
	        </div>
	    </div>
	    <div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        	<div class="form-group required">
	        		<label for="sender_address_1" class="control-label">{!!trans('Myaccount::selfmanage.label.address_1')!!} <sup>*</sup></label>
	        		<input class="form-control address_1" placeholder="{!!trans('Myaccount::selfmanage.placeholder.address_1')!!}" required="required" id="sender_address_1" type="text" name="sender_address_1" value="{{ (sizeof($sender_address)>0)?$sender_address->address_1:null }}">
	        	</div>
	        </div>	 
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
		       <div class="form-group">
	        		<label for="sender_address_2" class="control-label">{!!trans('Myaccount::selfmanage.label.address_2')!!} </label>
	        		<input class="form-control address_2" placeholder="{!!trans('Myaccount::selfmanage.placeholder.address_2')!!}" id="sender_address_2" type="text" name="sender_address_2" value="{{ (sizeof($sender_address)>0)?$sender_address->address_2:null }}">
	        	</div>
	        </div>      
	    </div>
	   <div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        	<div class="form-group required">
	        		<label for="sender_city" class="control-label">{!!trans('Myaccount::selfmanage.label.city')!!} <sup>*</sup></label>
	        		<input class="form-control city" placeholder="{!!trans('Myaccount::selfmanage.placeholder.city')!!}" required="required" id="sender_city" type="text" name="sender_city" value="{{ (sizeof($sender_address)>0)?$sender_address->city:null }}">
	        	</div>
	        </div>	 
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        	{!! Form::select('sender_state')			       
		        -> label(trans('Myaccount::selfmanage.label.state'))             
				-> options(trans('Myaccount::selfmanage.options.states'))
				-> placeholder(trans('Myaccount::selfmanage.placeholder.state'))
				-> value((sizeof($sender_address)>0)?$sender_address->state:null)
				-> addclass('state')
				-> required('required')!!}
	        </div>      
	    </div>
	    <div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::select('sender_country')			       
	        -> label(trans('Myaccount::selfmanage.label.country'))             
			-> options(trans('Myaccount::selfmanage.options.countries'))
			-> placeholder(trans('Myaccount::selfmanage.placeholder.country'))
			-> value((sizeof($sender_address)>0)?$sender_address->country:null)
			-> addclass('country')
			-> required('required')!!}
	        </div>	 
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('sender_postcode')
	        -> label(trans('Myaccount::selfmanage.label.postcode'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.postcode'))
	        -> addclass('postcode')
	        -> value((sizeof($sender_address)>0)?$sender_address->postcode:null)
	        -> required('required')!!}
	        </div>      
	    </div>
       <!--
	   <div class="row">
		   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			   {!! Form::textarea('sender_instruction')
               -> label(trans('Myaccount::selfmanage.label.sender_instruction'))
               -> value((sizeof($sender_address)>0)?$sender_address->shipment_instruction:null)
               -> required('required')!!}
		   </div>
	   </div>
	   -->
    </div>

    <!-- Consignment detail -->
    <div class="row">
    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-title">
    		<h4>{!!trans('Myaccount::selfmanage.consignment_detail')!!}</h4>
    	</div>
    </div> 
   <div class="box box-danger">
	   <div class="row">
		   <div class="col-xs-12 col-sm-6 col-lg-6">
			   {!! Form::select('shipment_method')
			   ->label(trans('Myaccount::selfmanage.label.shipment_method'))
			   ->options(trans('Myaccount::selfmanage.options.shipment_method'))
			   ->placeholder(trans('Myaccount::selfmanage.placeholder.shipment_method'))
			   ->value((sizeof($booking)>0)?$booking->shipment_method:null)
			   ->required('required')
			   !!}
		   </div>

		   <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
			   {!! Form::select('item_type')
                 -> label(trans('Myaccount::selfmanage.label.item_type'))
                 -> options(trans('Myaccount::selfmanage.options.item_types'))
                 -> value((sizeof($booking)>0)?$booking->type:null)
                 -> placeholder(trans('Myaccount::selfmanage.placeholder.item_type'))
                 -> required('required')!!}
		   </div>

           <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
               {!! Form::number('item_qty')
               -> label(trans('Myaccount::selfmanage.label.num_item'))
               -> placeholder(trans('Myaccount::selfmanage.placeholder.num_item'))
               -> min('1')
               -> value((sizeof($booking)>0)?$booking->number_of_item:null)
               -> required('required')!!}
           </div>
	   </div>
    	<div class="row">

	        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	        	<div class="row">
	        		<div class="col-xs-12 col-sm-3">
		        		{!! Form::number('item_length')
				        -> label(trans('Myaccount::selfmanage.label.length'))
				        -> placeholder(trans('Myaccount::selfmanage.placeholder.length'))
				        -> value((sizeof($booking)>0)?$booking->length:null)
				        -> required('required')!!}
		        	</div>
		        	<div class="col-xs-12 col-sm-3">
		        		{!! Form::number('item_width')
				        -> label(trans('Myaccount::selfmanage.label.width'))
				        -> placeholder(trans('Myaccount::selfmanage.placeholder.width'))
				        -> value((sizeof($booking)>0)?$booking->width:null)
				        -> required('required')!!}
		        	</div>
		        	<div class="col-xs-12 col-sm-3">
		        		{!! Form::number('item_height')
				        -> label(trans('Myaccount::selfmanage.label.height'))
				        -> placeholder(trans('Myaccount::selfmanage.placeholder.height'))
				        -> value((sizeof($booking)>0)?$booking->height:null)
				        -> required('required')!!}
		        	</div>
                    <div class="col-xs-12 col-sm-3">
                        {!! Form::text('item_weight')
                        -> label(trans('Myaccount::selfmanage.label.weight'))
                        -> placeholder(trans('Myaccount::selfmanage.placeholder.weight'))
                        -> value((sizeof($booking)>0)?$booking->weight:null)
                        -> required('required')!!}
                    </div>
	        	</div>
	        </div>     
	    </div>
	    <div class="row">
	    </div>
	   <div class="row">
		   <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			   {!! Form::text('warehouse_code')
               -> label(trans('Myaccount::selfmanage.label.warehouse_code'))
               -> placeholder(trans('Myaccount::selfmanage.placeholder.warehouse_code'))
               -> value((sizeof($booking)>0)?$booking->warehouse_code:null)
               -> required('required')!!}
		   </div>
		   <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			   {!! Form::text('quote_reference')
               -> label(trans('Myaccount::selfmanage.label.quote_reference'))
               -> placeholder(trans('Myaccount::selfmanage.placeholder.quote_reference'))
               -> value((sizeof($booking)>0)?$booking->quote_reference:null)
               -> required('required')!!}
		   </div>
	   </div>
	   <div class="row">
		   <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			   {!! Form::text('ql_contact')
               -> label(trans('Myaccount::selfmanage.label.ql_contact'))
               -> placeholder(trans('Myaccount::selfmanage.placeholder.ql_contact'))
               -> value((sizeof($booking)>0)?$booking->ql_contact:null)
               -> required('required')!!}
		   </div>
		   <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
			   {!! Form::text('time_ready')
			   -> label(trans('Myaccount::selfmanage.label.time_ready'))
               -> placeholder(trans('Myaccount::selfmanage.placeholder.time_ready'))
			   -> value((sizeof($booking)>0)?$booking->time_ready:null)
               -> required('required')
			   !!}
			   
		   </div>
	   </div>
       <!--
	     <div class="row">
	        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	        	{!! Form::text('vehicle_spec')
	        	-> label(trans('Myaccount::selfmanage.label.vehicle_spec'))
	        	-> value((sizeof($booking)>0)?$booking->vehicle_spec:null)
	        	 !!}
	        </div>
	     </div>
	    -->
		   <div class="row">
			   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				   {!! Form::textarea('item_remark')
				   -> label(trans('Myaccount::selfmanage.label.item_remark'))
				   -> value((sizeof($booking)>0)?$booking->remark:null)
				   
					!!}
			   </div>
		   </div>
	</div>
    
    <!-- Receiver detail -->
   
    <div class="row showSavedAddress" style="display:none;">
    	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-title">
    		<h4>{!!trans('Myaccount::selfmanage.receiver_detail')!!}</h4>
    		<span class="pull-right">
    			[<span style="cursor:pointer" class="loadSelectAddress" data-target="receiver">Choose from address list</span>]
    		</span>
    	</div>
    </div> 
   
   <div class="box box-info" id="receiver">
    	<div class="row">
	        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	        {!! Form::text('receiver_company_name')
	        -> label(trans('Myaccount::selfmanage.label.company_name'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.company_name'))
	        -> value((sizeof($receiver_address)>0)?$receiver_address->company_name:null)
	        -> addclass('company_name')
	        -> required('required')!!}
	        </div>       
	    </div>    
	    <div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('receiver_first_name')
	        -> label(trans('Myaccount::selfmanage.label.first_name'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.first_name'))
	        -> value((sizeof($receiver_address)>0)?$receiver_address->first_name:null)
	        -> addclass('first_name')
	        -> required('required')!!}
	        </div>
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('receiver_last_name')
	        -> label(trans('Myaccount::selfmanage.label.last_name'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.last_name'))
	        -> value((sizeof($receiver_address)>0)?$receiver_address->last_name:null)
	        -> addclass('last_name')
	        -> required('required')!!}
	        </div>
	    </div>
		<div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('receiver_phone')
	        -> label(trans('Myaccount::selfmanage.label.phone'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.phone'))
	        -> value((sizeof($receiver_address)>0)?$receiver_address->phone:null)
	        -> addclass('phone')
	        -> required('required')!!}
	        </div>
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('receiver_email')
	        -> label(trans('Myaccount::selfmanage.label.email'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.email'))
	        -> value((sizeof($receiver_address)>0)?$receiver_address->email:null)
	        -> addclass('email')
	        -> required('required')!!}
	        </div>
	    </div>
	    <div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('receiver_address_1')
	        -> label(trans('Myaccount::selfmanage.label.address_1'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.address_1'))
	        -> value((sizeof($receiver_address)>0)?$receiver_address->address_1:null)
	        -> addclass('address_1')
	        -> required('required')!!}
	        </div>	 
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('receiver_address_2')
	        -> label(trans('Myaccount::selfmanage.label.address_2'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.address_2'))
	        -> value((sizeof($receiver_address)>0)?$receiver_address->address_2:null)
	        -> addclass('address_2')
	       !!}
	        </div>      
	    </div>
	   <div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('receiver_city')
	        -> label(trans('Myaccount::selfmanage.label.city'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.city'))
	        -> value((sizeof($receiver_address)>0)?$receiver_address->city:null)
	        -> addclass('city')
	        -> required('required')!!}
	        </div>	 
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::select('receiver_state')			                    
			-> options(trans('Myaccount::selfmanage.options.states'))
			-> label(trans('Myaccount::selfmanage.label.state'))
			-> placeholder(trans('Myaccount::selfmanage.placeholder.state'))
			-> value((sizeof($receiver_address)>0)?$receiver_address->state:null)
			-> addclass('state')
			-> required('required')!!}
	        </div>      
	    </div>
	    <div class="row">
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::select('receiver_country')			       
	        -> label(trans('Myaccount::selfmanage.label.country'))             
			-> options(trans('Myaccount::selfmanage.options.countries'))
			-> value((sizeof($receiver_address)>0)?$receiver_address->country:null)
			-> placeholder(trans('Myaccount::selfmanage.placeholder.country'))
			-> addclass('country')
			-> required('required')!!}
	        </div>	 
	        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
	        {!! Form::text('receiver_postcode')
	        -> label(trans('Myaccount::selfmanage.label.postcode'))
	        -> placeholder(trans('Myaccount::selfmanage.placeholder.postcode'))
	        -> value((sizeof($receiver_address)>0)?$receiver_address->postcode:null)
	        -> addclass('postcode')
	        -> required('required')!!}
	        </div>      
	    </div>
       <!--
	   <div class="row">
		   <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			   {!! Form::textarea('receiver_instruction')
               -> label(trans('Myaccount::selfmanage.label.receiver_instruction'))
               -> value((sizeof($receiver_address)>0)?$receiver_address->shipment_instruction:null)
               !!}
		   </div>
	   </div>
	   -->
    </div>    

<script type="text/javascript">
$(document).ready(function(){
    $('.loadSelectAddress').click(function(){
    	var dest = $(this).attr('data-target');
    	var modal = $('#available_address');
    	modal.find("input#dest").val(dest);
    	
    	modal.find('.modal-title').html('Choose address for ' + dest);
		$.getJSON('{{URL::to('selfmanage/manage-address/available-address')}}', function(result){
			
		});
    	$('#available_address').modal('show');        
    });
	
    
});
$(function () {
	$('#time_ready').datetimepicker({format: 'YYYY-MM-DD HH:mm:ss'});
});
</script>