<div class="col-md-4 col-xs-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">{!! trans('Myaccount::selfmanage.consignment_detail')!!}</h4>
        </div>
        <div class="panel-body">
            <table class="table">
                <tr>
                    <td>{!! trans('Myaccount::selfmanage.label.tracking_number')!!}:</td>
                    <td>{{$booking->tracking_number}}</td>
                </tr>
                <tr>
                    <td>{!! trans('Myaccount::selfmanage.label.status')!!}:</td>
                    <td>{{$booking->status}}
                        @if($booking->allow_customer_change == 1)
                            <button type="button" class="btn btn-primary btn-sm" data-action="EDIT" data-load-to='#entry-user' data-href='{{ trans_url('/selfmanage/booking') }}/{{$booking->id}}/edit'><i class="fa fa-pencil-square"></i> {{ trans('cms.edit') }}</button>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>{!! trans('Myaccount::selfmanage.label.shipment_method')!!}:</td>
                    <td>{{$booking->shipment_method}}</td>
                </tr>
                <tr>
                    <td>{!! trans('Myaccount::selfmanage.label.num_item')!!}:</td>
                    <td>{{$booking->number_of_item}}</td>
                </tr>
                <tr>
                    <td>{!! trans('Myaccount::selfmanage.label.length')!!}:</td>
                    <td>{{$booking->length}}</td>
                </tr>
                <tr>
                    <td>{!! trans('Myaccount::selfmanage.label.width')!!}:</td>
                    <td>{{$booking->width}}</td>
                </tr>
                <tr>
                    <td>{!! trans('Myaccount::selfmanage.label.height')!!}:</td>
                    <td>{{$booking->height}}</td>
                </tr>
                <tr>
                    <td>{!! trans('Myaccount::selfmanage.label.weight')!!}:</td>
                    <td>{{$booking->weight}}</td>
                </tr>

                <tr>
                    <td>{!! trans('Myaccount::selfmanage.label.item_type')!!}:</td>
                    <td>{{$booking->type}}</td>
                </tr>

                <tr>
                    <td>{!! trans('Myaccount::selfmanage.label.warehouse_code')!!}:</td>
                    <td>{{$booking->warehouse_code}}</td>
                </tr>
                <tr>
                    <td>{!! trans('Myaccount::selfmanage.label.quote_reference')!!}:</td>
                    <td>{{$booking->quote_reference}}</td>
                </tr>
                <tr>
                    <td>{!! trans('Myaccount::selfmanage.label.ql_contact')!!}:</td>
                    <td>{{$booking->ql_contact}}</td>
                </tr>
                <tr>
                    <td>{!! trans('Myaccount::selfmanage.label.time_ready')!!}:</td>
                    <td>{{$booking->time_ready}}</td>
                </tr>
                <tr>
                    <td>{!! trans('Myaccount::selfmanage.label.item_remark')!!}:</td>
                    <td>{{$booking->remark}}</td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="col-md-4 col-xs-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">{!! trans('Myaccount::selfmanage.sender_detail')!!}</h4>
        </div>
        <div class="panel-body">
            <table class="table">
                <tr>
                    <td>{!! trans('Myaccount::selfmanage.label.company_name')!!}:</td>
                    <td>{{$source_address->company_name}}</td>
                </tr>
                <tr>
                    <td>{!! trans('Myaccount::selfmanage.label.contact_name')!!}:</td>
                    <td>{{$source_address->first_name}} {{$source_address->last_name}}</td>
                </tr>
                <tr>
                    <td>{!! trans('Myaccount::selfmanage.label.address')!!}:</td>
                    <td>{{$source_address->address_1}} {{$source_address->address_2}},
                        {{$source_address->city}},
                        {{$source_address->state}},
                        {{$source_address->country}},
                        {{$source_address->postcode}}</td>
                </tr>
                <tr>
                    <td>{!! trans('Myaccount::selfmanage.label.phone')!!}:</td>
                    <td>{{$source_address->phone}}</td>
                </tr>
                <tr>
                    <td>{!! trans('Myaccount::selfmanage.label.email')!!}:</td>
                    <td>{{$source_address->email}}</td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="col-md-4 col-xs-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">{!! trans('Myaccount::selfmanage.receiver_detail')!!}</h4>
        </div>
        <div class="panel-body">
            <table class="table">
                <tr>
                    <td>{!! trans('Myaccount::selfmanage.label.company_name')!!}:</td>
                    <td>{{$destination_address->company_name}}</td>
                </tr>
                <tr>
                    <td>{!! trans('Myaccount::selfmanage.label.contact_name')!!}:</td>
                    <td>{{$destination_address->first_name}} {{$source_address->last_name}}</td>
                </tr>
                <tr>
                    <td>{!! trans('Myaccount::selfmanage.label.address')!!}:</td>
                    <td>{{$destination_address->address_1}} {{$destination_address->address_2}},
                        {{$destination_address->city}},
                        {{$destination_address->state}},
                        {{$destination_address->country}},
                        {{$destination_address->postcode}}</td>
                </tr>
                <tr>
                    <td>{!! trans('Myaccount::selfmanage.label.phone')!!}:</td>
                    <td>{{$destination_address->phone}}</td>
                </tr>
                <tr>
                    <td>{!! trans('Myaccount::selfmanage.label.email')!!}:</td>
                    <td>{{$destination_address->email}}</td>
                </tr>
            </table>
        </div>
    </div>
</div>
