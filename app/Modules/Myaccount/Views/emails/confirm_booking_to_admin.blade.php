<?php
/**
 * Created by PhpStorm.
 * User: vey.chea
 * Date: 5/10/2017
 * Time: 3:43 PM
 */
?>
        <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Confirmed Order</title>
</head>

<style media="screen" type="text/css">

    * {
        margin:0;
        padding:0;
        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    }

    html {
        background: #eee;
    }

    a {
        text-decoration: none;
        color:#337ab7;
    }

    a:hover, a:active {
        text-decoration: none;
        color: #23527c;
    }
    /*
    .order > tbody > tr > td {
        border: 1px solid #ddd;
    }
    */
</style>


<body style="margin: 0; padding: 0;">
<br>
<table class="order" border="0" cellpadding="0" cellspacing="0" width="1000" style="background: #fff; border-collapse: collapse; font-family: 'Arial', sans-serif; font-size: 14px; line-height: 1.5em; width: 600px; height: 100%; margin: 0 auto;">
    <tbody>
    <tr>
        <td style="text-align: center;padding: 30px;">
            <h1 style="color: #193889; font-weight: bold; font-size: 18px;">BOOKING CONFIRMED</h1>
            <strong>TRACKING NUMBER:</strong> {{$booking->tracking_number}}<br>
            <strong>DATE:</strong> {{ date('d/m/Y', strtotime($booking->created_at))}}
        </td>
    </tr>
    <tr style="font-size:13px;">
        <td style="padding: 20px 20px 20px 20px; ">
            <table style="width:100%">
                <tr><td style="width:50% ;padding: 0px 5px 0px 0px">
                        <h3>SENDER DETAIL:</h3>
                        <span style="text-transform: uppercase; ">{{ $source->company_name}}</span><br>
                        <strong>ATTN:</strong> <span style="text-transform: uppercase; ">{{ $source->first_name}} {{ $source->last_name}}</span><br><br>
                        {{$source->address_1}}{!! $source->address_2 !=""?"<br>":"" !!}
                        {{$source->address_2}}<br />
                        {{$source->city}}&nbsp;
                        {{$source->state}}&nbsp;
                        {{$source->postcode}}<br />
                        <strong>Phone:</strong> {{$source->phone}}<br />
                        <strong>Email:</strong> {{$source->email}}<br />
                    </td>

                    <td style="width:50% ;padding: 0px 0px 0px 5px">
                        <h3>RECEIVER DETAIL:</h3>
                        <span style="text-transform: uppercase; ">{{ $destination->company_name}}</span><br>
                        <strong>ATTN:</strong> <span style="text-transform: uppercase; ">{{ $destination->first_name}} {{ $destination->last_name}}</span><br><br>
                        {{$destination->address_1}}{!! $destination->address_2 !=""?"<br>":"" !!}
                        {{$destination->address_2}}<br />
                        {{$destination->city}}&nbsp;
                        {{$destination->state}}&nbsp;
                        {{$destination->postcode}}<br />
                        <strong>Phone:</strong> {{$destination->phone}}<br />
                        <strong>Email:</strong> {{$destination->email}}<br />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><td><br><br></td></tr>
    <tr style="padding: 20px 20px 20px 20px;">
        <td>
            <table width="100%" style="border-collapse: collapse; margin: 0; padding: 0; font-family: 'Arial', sans-serif; font-size: 14px;">
                <thead style="background: #eee;">
                <tr>
                    <!--<td class="t_15"></td>	-->
                    <td style="border: 1px solid #ddd; padding: 8px 10px; text-transform: uppercase; font-weight: bold;">Length (cm)</td>
                    <td style="border: 1px solid #ddd; padding: 8px 10px; text-transform: uppercase; font-weight: bold;">Width (cm)</td>
                    <td style="border: 1px solid #ddd; padding: 8px 10px; text-transform: uppercase; font-weight: bold;">Height (cm)</td>
                    <td style="border: 1px solid #ddd; padding: 8px 10px; text-transform: uppercase; font-weight: bold;">Weight (kg)</td>
                    <td style="border: 1px solid #ddd; padding: 8px 10px; text-transform: uppercase; font-weight: bold;">Number of item</td>
                    <td style="border: 1px solid #ddd; width: 15%; padding: 8px 10px; text-transform: uppercase; font-weight: bold; text-align:center">Item Charge</td>
                    <td style="border: 1px solid #ddd; width: 15%; padding: 8px 10px; text-transform: uppercase; font-weight: bold; text-align:center">Fuel Levy</td>
                    <td style="border: 1px solid #ddd; width: 15%; padding: 8px 10px; text-transform: uppercase; font-weight: bold; text-align:center">GST</td>
                    <td style="border: 1px solid #ddd; width: 15%; padding: 8px 10px; text-transform: uppercase; font-weight: bold; text-align:center">Total</td>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="border: 1px solid #ddd; padding: 8px 10px;">{{$booking->length}}</td>
                        <td style="border: 1px solid #ddd; padding: 8px 10px;">{{$booking->width}}</td>
                        <td style="border: 1px solid #ddd; padding: 8px 10px;">{{$booking->height}}</td>
                        <td style="border: 1px solid #ddd; padding: 8px 10px;">{{$booking->weight}}</td>
                        <td style="border: 1px solid #ddd; padding: 8px 10px;">{{$booking->number_of_item}}</td>
                        <td style="border: 1px solid #ddd; padding: 8px 10px; text-align: right">$ {{number_format($booking->subtotal, 2)}}</td>
                        <td style="border: 1px solid #ddd; padding: 8px 10px; text-align: right">$ {{number_format($booking->fuel_levy, 2)}}</td>
                        <td style="border: 1px solid #ddd; padding: 8px 10px; text-align: right">$ {{number_format($booking->tax_total, 2)}}</td>
                        <td style="border: 1px solid #ddd; padding: 8px 10px; text-align: right">$ {{number_format($booking->total, 2)}}</td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr><td><br><br><br></td></tr>
    <tr style="font-size:13px;">
        <td style="padding: 20px 20px 10px 20px; ">
            <table style="width:50%; border-collapse: collapse;" BORDER="1px solid #ddd;" >
                <tr>
                    <td colspan="2" style="border: 1px solid #ddd; padding: 8px 10px;"><h3>REFERENCES:</h3></td>
                </tr>
                <tr>
                    <td style="border: 1px solid #ddd; padding: 8px 10px;">Shipment method:</td>
                    <td style="border: 1px solid #ddd; padding: 8px 10px;">{{$booking->shipment_method}}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #ddd; padding: 8px 10px;">Warehouse code:</td>
                    <td style="border: 1px solid #ddd; padding: 8px 10px;">{{$booking->warehouse_code}}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #ddd; padding: 8px 10px;">Quote reference:</td>
                    <td style="border: 1px solid #ddd; padding: 8px 10px;">{{$booking->quote_reference}}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #ddd; padding: 8px 10px;">QL contact person:</td>
                    <td style="border: 1px solid #ddd; padding: 8px 10px;">{{$booking->ql_contact}}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #ddd; padding: 8px 10px;">Time ready:</td>
                    <td style="border: 1px solid #ddd; padding: 8px 10px;">{{$booking->time_ready}}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #ddd; padding: 8px 10px;">Special instruction:</td>
                    <td style="border: 1px solid #ddd; padding: 8px 10px;"><i>{{$booking->remark}}</i></td>
                </tr>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>

