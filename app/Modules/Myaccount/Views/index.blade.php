@includeStyle('vendors/css/logistic.css')
@includeStyle('vendors/css/timeline-simple.css')

<?php
config()->set('quatius.logistic.history.status-active', config('quatius.logistic.history.status-admin', 1));
?>

<!-- use Carbon\Carbon; -->

<section class="content-header">
    <h1>
    <i class="fa fa-dashboard"></i> Dashboard 
    </h1>
    <ol class="breadcrumb">
        <li><i class="fa fa-dashboard"></i> Home </li>
        <!-- <li class="active">Dashboard</li> -->
    </ol>
</section>



<section class="content">
<?php
    // $conns = app('connote')->byUserAccount()->latestStatus()->whereNotNull('status')->get();
    $conns = app('connote')->byUserAccount()->latestStatus();
    // $conns_today = $conns->whereDate('created_at', Carbon\Carbon::today());
    $conns_created = $conns->whereStatus(null)->whereRemark("");
    // $conns_submitted = $conns->where('status', "submitted");
    // $conns_picked_up = $conns->where('status', "picked-up");
    // $conns_delivered = $conns->where('status', "delivered");
    // $conns_cancelled = $conns->where('status', "cancelled");
    // $conns_warning = $conns->where('remark', '<>', "");

    dump($conns->get());
    dump($conns->pluck('status','id'));
    dump($conns->pluck('remark','id'));

    // dump($conns_today->get());
    // dump($conns_warning->get());

    dump($conns_created->get());
    // dump($conns_submitted->get());
    // dump($conns_picked_up->get());
    // dump($conns_delivered->get());
    // dump($conns_cancelled->get());
    
?>
    <!-- Customer Info -->
    <div class="row">
        <div class="col-md-4">
            <div class="small-box bg-logo-1">
                <div class="inner">
                    <h3><small style="color: inherit; text-transform:uppercase;">WELCOME {{user()->name}}</small></h3>
                    <h4>{{app('account')->detail()->name}}</h4>                    
                    <p>
                        @if (user('web')->email != null)
                            <i class="fa fa-envelope">&nbsp;</i> {{user('web')->email}}
                        @endif
                        @if (user('web')->phone_1 != null)
                            &nbsp;&nbsp; <i class="fa fa-phone ">&nbsp;</i> {{ user('web')->phone_1 }}    
                        @endif 
                    </p>
                </div>
                <div class="icon"><i class="fa fa-user"></i></div>
                <div class="small-box-footer">
                    <div class="row">
                        <div class="col-sm-6 border-right">
                            <div class="description-block">
                                <h5 class="description-header">{{app('account')->detail()->account_code}}</h5>
                                <span class="description-text">CUSTOMER CODE</span>
                            </div>
                        </div>
                        <div class="col-sm-6 border-right">
                            <div class="description-block">
                                <h5 class="description-header">{{app('account')->detail()->getParams('pronto.status', 'OK')}}</h5>
                                <span class="description-text">STATUS</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="small-box  bg-logo-3">
                <div class="inner">
                    <h3>$ {{app('account')->detail()->getParams('pronto.current_balance', '0')}}</h3>
                    <p>CURRENT BALANCE</p>
                    <p>&nbsp;</p>
                </div>
                <div class="icon"><i class="fa fa-usd"></i></div>
                <div class="small-box-footer">
                    <div class="row">
                        <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header">$ {{app('account')->detail()->getParams('pronto.future_balance', '0')}}</h5>
                                <span class="description-text">FUTURE BALANCE</span>
                            </div>
                        </div>
                        <div class="col-sm-5 border-right">
                            <div class="description-block">
                                <h5 class="description-header">$ {{app('account')->detail()->getParams('pronto.outstanding_balance', '0')}}</h5>
                                <span class="description-text">OUTSTANDING BALANCE</span>
                            </div>
                        </div>
                        <div class="col-sm-3 border-right">
                            <div class="description-block">
                                <h5 class="description-header">$ {{app('account')->detail()->getParams('pronto.credit_limit', '0')}}</h5>
                                <span class="description-text">CREDIT LIMIT</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">           
            <div class="small-box  bg-logo-4">
                <div class="inner" style="display: inline-block">
                    <h3>{{$conns->count()}}</h3>
                    <p>CONNOTES</p>
                    <p>&nbsp;</p>
                </div>
                <!-- <div class="inner" style="display: inline-block;">
                    <h3>|&nbsp; 30</h3>
                    <p> &nbsp; MANIFEST</p>
                </div> -->
                <div class="icon"><i class="fa fa-files-o"></i></div>
                <div class="small-box-footer">
                    <div class="row">
                        <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header">{{$conns->count()}}</h5>
                                <span class="description-text">CREATED</span>
                            </div>
                        </div>
                        <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header">{{$conns->count()}}</h5>
                                <span class="description-text">SUBMITTED</span>
                            </div>
                        </div>
                        <div class="col-sm-4 border-right">
                            <div class="description-block">
                                <h5 class="description-header">{{$conns->count()}}</h5>
                                <span class="description-text">CANCELLED</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Today summary -->
    <div class="row">
        <div class="col-md-8">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title text-primary"><i class="fa fa-calendar"></i> Today's Summary</h3>
                    <div class="box-tools">
                        <span data-toggle="tooltip" title="" class="badge bg-status-warning" data-original-title="15 warning connotes">15</span>
                        <span data-toggle="tooltip" title="" class="badge bg-status-cancelled" data-original-title="2 cancelled connotes">2</span>
                        &nbsp;
                        <!-- <button id="btn-create-manifest" type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalSenderPickup" disabled title="Please, select at least one connote to create a manifest!"><i class="fa fa-send"></i>&nbsp; Create Manifest</button> -->
                        <!-- <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button> -->
                    </div>
                </div>
                <div class="box-body" style="min-height:236px; margin-top:20px;">
                    <div class="row">                            
                        <div class="col-md-8">
                            <div class="timeline-steps aos-init aos-animate" data-aos="fade-up">
                                <div class="timeline-step">
                                    <div class="timeline-content status-created" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="2003">
                                        <div class="inner-circle bg-status-created"></div>
                                        <p class="h5 mt-3 mb-1">15</p>
                                        <p class="h5 mb-0 mb-lg-0"><i class="fa fa-thumbs-o-up"></i>&nbsp; Created</p>
                                    </div>
                                </div>
                                <div class="timeline-step">
                                    <div class="timeline-content status-submitted" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="2004">
                                        <div class="inner-circle bg-status-submitted"></div>
                                        <p class="h5 mt-3 mb-1">15</p>
                                        <p class="h5 mb-0 mb-lg-0"><i class="fa fa-send"></i>&nbsp; Submitted</p>
                                    </div>
                                </div>
                                <div class="timeline-step">
                                    <div class="timeline-content status-picked-up" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="2005">
                                        <div class="inner-circle bg-status-picked-up"></div>
                                        <p class="h5 mt-3 mb-1">10</p>
                                        <p class="h5 mb-0 mb-lg-0"><i class="fa fa-truck"></i>&nbsp; Picked-Up</p>
                                    </div>
                                </div>
                                <div class="timeline-step">
                                    <div class="timeline-content status-delivered" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="2010">
                                        <div class="inner-circle bg-status-delivered"></div>
                                        <p class="h5 mt-3 mb-1">7</p>
                                        <p class="h5 mb-0 mb-lg-0"><i class="fa fa-check"></i>&nbsp; Delivered</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="timeline-steps aos-init aos-animate" data-aos="fade-up">
                                <div class="timeline-step">
                                    <div class="timeline-content status-warning" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="2003">
                                        <div class="inner-circle bg-status-warning"></div>
                                        <p class="h5 mt-3 mb-1">15</p>
                                        <p class="h5 mb-0 mb-lg-0">Warning</p>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <div class="col-md-2">
                            <div class="timeline-steps aos-init aos-animate" data-aos="fade-up">
                                <div class="timeline-step">
                                    <div class="timeline-content status-cancelled" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="2003">
                                        <div class="inner-circle bg-status-cancelled"></div>
                                        <p class="h5 mt-3 mb-1">2</p>
                                        <p class="h5 mb-0 mb-lg-0">Cancelled</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <!-- Summary of Connotes -->
            <div class="row">        
                <div class="col-md-12">
                    <div class="info-box" id="created">
                        <span class="info-box-icon bg-status-created"><i class="fa fa-cube"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-number status-ready"><small><i class="fa fa-thumbs-o-up"></i> CREATED:</small> <span class="var_quantity">0</span></span>
                            <span class="info-box-number status-ready-to-print"><small><i class="fa fa-print"></i> READY-TO-PRINT:</small> <span class="var_quantity">0</span></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="info-box" id="submiited">
                        <span class="info-box-icon bg-status-submitted"><i class="fa fa-cubes"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-number status-submitted"><small><i class="fa fa-send"></i> SUBMITTED:</small> <span class="var_quantity">0</span></span>
                            <span class="info-box-number status-picked-up"><small><i class="fa fa-truck"></i> PICKED-UP:</small> &nbsp;<span class="var_quantity">0</span></span>
                            <span class="info-box-number status-delivered"><small><i class="fa fa-check"></i> DELIVERED:</small> &nbsp;<span class="var_quantity">0</span></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="info-box" id="cancelled">
                        <span class="info-box-icon bg-status-cancelled"><i class="fa fa-exclamation-triangle"></i></span>
                        <div class="info-box-content">
                            <!-- <span class="info-box-text status-cancelled">CANCELLED</span> -->
                            <span class="info-box-number status-warning"><small><i class="fa fa-exclamation-circle"></i> WARNING:</small> &nbsp;&nbsp;<span class="var_quantity">0</span></span>
                            <span class="info-box-number status-delayed"><small><i class="fa fa-clock-o"></i> DELAYED:</small> &nbsp;&nbsp;<span class="var_quantity">0</span></span>
                            <span class="info-box-number status-cancelled"><small><i class="fa fa-times"></i> CANCELLED:</small> <span class="var_quantity">0</span></span>
                            <!-- <span class="info-box-number">$ <span class="var_amount">0</span></span> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
    
</section>

