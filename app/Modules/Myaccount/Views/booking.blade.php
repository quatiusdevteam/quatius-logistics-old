<div class="form-group">
	<div class="row">
		<div class="col-md-12">
			 {!!Form::vertical_open()
	        ->id('new_booking')
	        ->method('PUT')
	        ->enctype('multipart/form-data')
	        ->action(url('selfmanage/booking'))!!}
	        <div class="col-md-12 col-xs-12">
	        	<div class="sub-title">
	        		YOUR DETAILS
	        	</div>
	        </div>
	        <div class="col-md-12 col-xs-12">
	        {!!
	        	Form::text('company_name', 'Company Name')
	        	->placeHolder('Company Name')
	        	->id('company_name')
	        !!}
	        </div>	        
	        <div class="col-md-6 col-xs-12">
	        {!!
	        	Form::text('first_name', 'First Name')
	        	->placeHolder('First Name')
	        	->id('first_name')
	        !!}
	        </div>
	        <div class="col-md-6 col-xs-12">
	        {!!
	        	Form::text('last_name', 'Last Name')
	        	->placeHolder('Last Name')
	        	->id('last_name')
	        !!}	
	        </div>
	        <div class="col-md-6 col-xs-12">
	        {!!
	        	Form::tel('phone', 'Phone Number')
	        	->placeHolder('Phone Number')
	        	->id('phone_number')
	        !!}
	        </div>
	        <div class="col-md-6 col-xs-12">
	        {!!
	        	Form::email('email', 'E-mail')
	        	->placeHolder('E-mail')
	        	->id('email')
	        !!}	
	        </div>
	        <div class="col-md-12 col-xs-12">
	        	<div class="sub-title">
	        		SENDER DETAILS
	        	</div>
	        </div>
	        <div class="col-md-12 col-xs-12">
	        {!!
	        	Form::text('company_name', 'Company Name')
	        	->placeHolder('Company Name')
	        	->id('company_name')
	        !!}
	        </div>	        
	        <div class="col-md-6 col-xs-12">
	        {!!
	        	Form::text('first_name', 'First Name')
	        	->placeHolder('First Name')
	        	->id('first_name')
	        !!}
	        </div>
	        <div class="col-md-6 col-xs-12">
	        {!!
	        	Form::text('last_name', 'Last Name')
	        	->placeHolder('Last Name')
	        	->id('last_name')
	        !!}	
	        </div>
	        <div class="col-md-6 col-xs-12">
	        {!!
	        	Form::tel('phone', 'Phone Number')
	        	->placeHolder('Phone Number')
	        	->id('phone_number')
	        !!}
	        </div>
	        <div class="col-md-6 col-xs-12">
	        {!!
	        	Form::email('email', 'E-mail')
	        	->placeHolder('E-mail')
	        	->id('email')
	        !!}	
	        </div>
	        <div class="col-md-12 col-xs-12">
	        	<div class="sub-title">
	        		DESPATCH DETAILS
	        	</div>
	        </div>
	        <div class="col-md-12 col-xs-12">
	        {!!
	        	Form::text('company_name', 'Company Name')
	        	->placeHolder('Company Name')
	        	->id('company_name')
	        !!}
	        </div>	        
	        <div class="col-md-6 col-xs-12">
	        {!!
	        	Form::text('first_name', 'First Name')
	        	->placeHolder('First Name')
	        	->id('first_name')
	        !!}
	        </div>
	        <div class="col-md-6 col-xs-12">
	        {!!
	        	Form::text('last_name', 'Last Name')
	        	->placeHolder('Last Name')
	        	->id('last_name')
	        !!}	
	        </div>
	        <div class="col-md-6 col-xs-12">
	        {!!
	        	Form::tel('phone', 'Phone Number')
	        	->placeHolder('Phone Number')
	        	->id('phone_number')
	        !!}
	        </div>
	        <div class="col-md-6 col-xs-12">
	        {!!
	        	Form::email('email', 'E-mail')
	        	->placeHolder('E-mail')
	        	->id('email')
	        !!}	
	        </div>
	        <div class="col-md-12 col-xs-12">
	        	<div class="sub-title">
	        		CONSIGNMENT DETAILS
	        	</div>
	        </div>
	        <div class="col-md-12 col-xs-12">
	        {!!
	        	Form::text('company_name', 'Company Name')
	        	->placeHolder('Company Name')
	        	->id('company_name')
	        !!}
	        </div>	        
	        <div class="col-md-6 col-xs-12">
	        {!!
	        	Form::text('first_name', 'First Name')
	        	->placeHolder('First Name')
	        	->id('first_name')
	        !!}
	        </div>
	        <div class="col-md-6 col-xs-12">
	        {!!
	        	Form::text('last_name', 'Last Name')
	        	->placeHolder('Last Name')
	        	->id('last_name')
	        !!}	
	        </div>
	        <div class="col-md-6 col-xs-12">
	        {!!
	        	Form::tel('phone', 'Phone Number')
	        	->placeHolder('Phone Number')
	        	->id('phone_number')
	        !!}
	        </div>
	        <div class="col-md-6 col-xs-12">
	        {!!
	        	Form::email('email', 'E-mail')
	        	->placeHolder('E-mail')
	        	->id('email')
	        !!}	
	        </div>
	        {!!Form::close()!!}
		</div>
	</div>
</div>