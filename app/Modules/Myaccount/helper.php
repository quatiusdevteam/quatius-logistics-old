<?php
 /**
 *	Myaccount Helper  
 */
if (!function_exists('email_order_confirmaion_to_user'))
{
    function email_booking_confirmaion_to_admin($booking, $source, $destination)
    {
        /**
         * Send email to sales rep if applicable
         */
        $admin_email = "john.jiang@quatiuslogistics.com";
        if($booking->tracking_number) {
            sendMailView(
                "A booking with tracking number." . $booking->tracking_number . " has been Confirmed",
                'Myaccount::emails.confirm_booking_to_admin',
                compact('booking', 'source', 'destination'),
                $admin_email,
                config('quatius.mailer.admin')
            );
            // send to me for monitoring period
            sendMailView(
                "A booking with tracking number." . $booking->tracking_number . " has been Confirmed",
                'Myaccount::emails.confirm_booking_to_admin',
                compact('booking', 'source', 'destination'),
                'vey.chea@soniq.com',
                config('quatius.mailer.admin')
            );
        }

    }
}