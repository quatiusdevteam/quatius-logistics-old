<?php namespace App\Modules\SupplierOperation\Models;

use App\Modules\Supplier\Models\Supplier;
use Illuminate\Database\Eloquent\Model;

class SupplierOperation extends Model {

	protected $fillable = [
	    'origination_zone',
        'destination_zone',
        'status_id',
        'supplier_id',
    ];

	public function supplier(){
	    return $this->belongsTo(Supplier::class);
    }

    /**
     * @param $origination_postcode
     * @param $destination_postcode
     * @return int
     */
    public function getSupplierId($origination_postcode, $destination_postcode){
	    $orig = SupplierOperation::where('origination_postcode', '<=', $origination_postcode)->where('destination_postcode', '>=', $origination_postcode)->get();
        $dest = SupplierOperation::where('origination_postcode', '<=', $destination_postcode)->where('destination_postcode', '>=', $destination_postcode)->get();
        $origination_zone = "";
        $destination_zone = "";

        foreach($orig as $from){
            if(strtolower(trim($from->zone_code)) == "v1"){
                $origination_zone = "metro";
                break;
            }
            $origination_zone = "regional";
        }

        foreach($dest as $to){
            if(strtolower(trim($to->zone_code)) == "v1"){
                $destination_zone = "metro";
                break;
            }
            $destination_zone = "regional";
        }

        if($origination_zone == "" || $destination_zone == "")
            return -1;

        if($origination_zone == "metro" && $destination_zone == "metro")
            return 1;
        else
            return 2;
    }
}
