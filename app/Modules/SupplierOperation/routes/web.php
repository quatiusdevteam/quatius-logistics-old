<?php

Route::group(array('module' => 'SupplierOperation', 'middleware' => ['web'], 'namespace' => 'App\Modules\SupplierOperation\Controllers'), function() {

    Route::resource('supplier_operation', 'SupplierOperationController');
    
});	
