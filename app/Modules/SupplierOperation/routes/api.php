<?php

Route::group(array('module' => 'SupplierOperation', 'middleware' => ['api'], 'namespace' => 'App\Modules\SupplierOperation\Controllers'), function() {

    Route::resource('supplier_operation', 'SupplierOperationController');
    
});	
