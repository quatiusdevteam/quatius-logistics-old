<?php

Route::group(array('module' => 'Supplier', 'middleware' => ['web'], 'namespace' => 'App\Modules\Supplier\Controllers'), function() {

    Route::resource('supplier', 'SupplierController');
    
});	
