<?php

Route::group(array('module' => 'Supplier', 'middleware' => ['api'], 'namespace' => 'App\Modules\Supplier\Controllers'), function() {

    Route::resource('supplier', 'SupplierController');
    
});	
