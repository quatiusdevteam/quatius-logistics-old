<?php namespace App\Modules\Supplier\Models;

use App\Modules\SupplierOperation\Models\SupplierOperation;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model {

	protected $fillable = [
	        'name',
            'status_id',
	    ];

	public function operations(){
	    return $this->hasMany(SupplierOperation::class);
    }

    public function qlRates(){
	    return $this->hasMany(QLRate::class);
    }

    public function scopeQLRate($query, $from, $to){
        return $query->whereFromZone($from)->whereToZone($to);
    }
}
