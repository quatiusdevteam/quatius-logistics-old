<?php
/**
 * Created by PhpStorm.
 * User: vey.chea
 * Date: 28/09/2017
 * Time: 4:00 PM
 */

namespace App\Modules\Supplier\Models;
use Illuminate\Database\Eloquent\Model;

class QLRate extends Model
{
    protected $table = 'ql_rates';
    protected $fillable = [
        'from_zone',
        'to_zone',
        'status_id',
        'standard',
        'os',
        'xl',
        'oh',
        'dp',
        'supplier_id'
    ];

    public function supplier(){
        return $this->belongsTo(Supplier::class);
    }


}