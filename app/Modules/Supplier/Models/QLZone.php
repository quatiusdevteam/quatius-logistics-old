<?php
/**
 * Created by PhpStorm.
 * User: vey.chea
 * Date: 2/10/2017
 * Time: 11:53 AM
 */

namespace App\Modules\Supplier\Models;
use Illuminate\Database\Eloquent\Model;

class QLZone extends Model
{
    protected $table = 'zone_codes';
}