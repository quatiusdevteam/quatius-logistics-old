<?php

/**
 *	Logistic Helper  
 */

if (!function_exists('logistic_msg'))
{
	function logistic_msg($code, $replace=[]){
		if ($code instanceof \Quatius\Logistic\Models\LogisticHistory)
		{
			$replace = ['value'=>$code->value, 'location'=>$code->occur_at];
			$code = $code->comments;
		}

		if (trans()->has('Logistic::msg.'.$code)){
			$msg = trans('Logistic::msg.'.$code, $replace);
			return $msg?:$code;
		}
		return $code;
	}
}

if (!function_exists('generateConsignment'))
{
	function generateConsignment($connote = null){
		$last = config("quatius.logistic.connote.genlast", -1) + 1;
		$inc = strtotime('now') - config("quatius.logistic.gendateoff",1648639000) + $last;

		config()->set("quatius.logistic.connote.genlast", $last++);
		$gen = config("quatius.logistic.connote.prefix", "21").str_pad($inc, 10, "0", STR_PAD_LEFT);
		
		if (dbExists('connotes', 'consignment', $gen)){
			return generateConsignment($connote);
		}
		return config("quatius.logistic.connote.prefix", "21").str_pad($inc, 10, "0", STR_PAD_LEFT);
	}
}

if (!function_exists('generateCourierConsignment'))
{
	function generateCourierConsignment($connote = null){
		return rand(3000000, 3999999).""; //TODO: change this
	}
}

if (!function_exists('generateManifect'))
{
	function generateManifect($manifect = null){
		$last = config("quatius.logistic.manifest.genlast", -1) + 1;
		$inc = strtotime('now') - config("quatius.logistic.gendateoff",1648639000) + $last;
		config()->set("quatius.logistic.manifest.genlast", $last);

		$gen = config("quatius.logistic.manifest.prefix", "21").str_pad($inc, 10, "0", STR_PAD_LEFT);
		if (dbExists('manifests', 'manifest_no', $gen)){
			return generateManifect($manifect);
		}

		return config("quatius.logistic.manifest.prefix", "12").str_pad($inc, 10, "0", STR_PAD_LEFT);
	}
}

if (!function_exists('dbExists'))
{
	function dbExists($table, $field, $gen) {
		return !!DB::table($table)->where($field, $gen)->count();
	}
}

if (!function_exists('getBarcode'))
{
	function getBarcode($number, $type='C128'){
		//DNS1D::getBarcodePNGPath('4445645656', 'C39+', 3,200, [0,0,0], true);
		//DNS2D::getBarcodePNGPath('4445645656', 'QRCODE');
		if (!is_dir(storage_path("app/barcodes"))) mkdir(storage_path("app/barcodes"));
		
		$path = storage_path("app/barcodes/".$number.'.png');
		if (file_exists($path)) return $path;

		return DNS1D::getBarcodePNGPath($number, $type, 2, 40, [0,0,0], true);
	}
}

