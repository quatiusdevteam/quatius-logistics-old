<?php
Route::group(['prefix' => 'admin/logistic', 'module' => 'Logistic', 'middleware' => ['web', 'auth:admin.web'], 'namespace' => 'App\Modules\Logistic\Controllers\Admin'], function() {

    Route::get('connote', 'AdminConnoteController@index');
    Route::get('connote/sync-tracking', 'AdminConnoteController@sycnAllRemoteTracking');
    Route::get('connote/{connote_id}/sync-tracking', 'AdminConnoteController@sycnRemoteTracking');
    Route::get('connote/{connote_id}', 'AdminConnoteController@show');
    Route::post('connote/status/{status}/{comment?}', 'AdminConnoteController@statusChange');
});


Route::group(['module' => 'Logistic', 'middleware' => ['web', 'auth'], 'namespace' => 'App\Modules\Logistic\Controllers'], function() {
    Route::get('connote/submitted', 'ConnoteController@submitted');
    Route::get('connote-create', 'ConnoteController@creations');
    if (Env("APP_DEBUG", false)){
        Route::get('connote/import', 'ConnoteController@importExcelTest');
        
        Route::post('connote/status/{status}/{comment?}', 'ConnoteController@statusChange');
    }
    Route::post('connote/import', 'ConnoteController@importExcel');
    Route::get('connote/export/{status?}', 'ConnoteController@exportExcel');
    Route::get('connote/{connote_id}/label-pdf', 'ConnoteController@printLabelPdf');
    Route::get('connote/{connote_id}/label', 'ConnoteController@viewLabel');
    Route::get('connote/{connote_id}/pod', 'ConnoteController@viewPOD');
    Route::post('connote/{connote_id}/cancel', 'ConnoteController@cancel');

    Route::get('connote/{connote_id}/enquiry', 'ConnoteController@loadFormEnquiry');
    Route::post('connote/enquiry', 'ConnoteController@sendEnquiry');    
    Route::get('connote/{connote_id}/testview', 'ConnoteController@testView');    


    // Route::get('connote/home', 'ConnoteController@home');
    Route::get('connote/template.xls', 'ConnoteController@downloadTemplate');
    
    Route::get('connote/numConnsIndicator', 'ConnoteController@numConnotesIndicator');
    Route::get('connote/numConnotesDanger', 'ConnoteController@numConnotesDanger');

    Route::post('history/updateValue', 'HistoryController@updateValue');
    
    Route::resource('connote', 'ConnoteController', ['parameters' =>['connote' => 'connote_id']]);



    // Route::get('manifest', 'ManifestController@manifest');
    // Route::get('manifest/{manifest_id}', 'ManifestController@manifestShow');
    Route::get('manifest/{manifest_id}/label-pdf', 'ManifestController@printLabelPdf');
    Route::get('manifest/{manifest_id}/label', 'ManifestController@viewLabel');
    // Route::get('manifest/{manifest_id}', 'ManifestController@show');
    Route::resource('manifest', 'ManifestController', ['parameters' =>['manifest' => 'manifest_id'], 'except' => ['edit','create','update','delete']]);

});

Route::group(['module' => 'Logistic', 'middleware' => ['web'], 'namespace' => 'App\Modules\Logistic\Controllers'], function() {
    // Route::get('connote/tracking/{consignment?}', 'ConnoteController@tracking');
    Route::post('connote/tracking', 'ConnoteController@tracking');
});