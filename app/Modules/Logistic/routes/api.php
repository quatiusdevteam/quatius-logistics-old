<?php

Route::group(['prefix'=>'api','module' => 'Logistic', 'middleware' => ['api', 'auth:api', 'bindings'], 'namespace' => 'App\Modules\Logistic\Controllers'], function() {
    Route::get('connote/unsubmitted', 'ConnoteController@unsubmitted');
    Route::get('connote/submitted', 'ConnoteController@submitted');
    Route::get('connote-create', 'ConnoteController@creations');
    Route::post('connote/import', 'ConnoteController@importExcel');
    Route::get('connote/export/{status?}', 'ConnoteController@exportExcel');
    Route::post('connote/status/{status}/{comment?}', 'ConnoteController@statusChange');
    Route::resource('connote', 'ConnoteController', ['parameters' =>['connote' => 'connote_id'], 'except' => ['edit','create']]);

    Route::post('history/updateValue', 'HistoryController@updateValue');



    Route::get('manifest/{manifest_id}/label-pdf', 'ManifestController@printLabelPdf');
    Route::get('manifest/{manifest_id}/label', 'ManifestController@viewLabel');

    Route::resource('manifest', 'ManifestController', ['parameters' =>['manifest' => 'manifest_id'], 'except' => ['edit','create','update','delete']]);
});

Route::group(['prefix'=>'api','module' => 'Logistic', 'middleware' => ['api', 'bindings'], 'namespace' => 'App\Modules\Logistic\Controllers'], function() {
    Route::get('connote/{connote_id}/label-pdf', 'ConnoteController@printLabelPdf');
    Route::get('connote/{connote_id}/label', 'ConnoteController@viewLabel');
    Route::get('connote/{connote_id}/pod', 'ConnoteController@viewPOD');

    Route::get('courier/{courier_api_key}/export/{action}/{filename?}', 'CourierController@export');
    
    Route::post('courier/{courier_api_key}/import/{action}/{filename?}', 'CourierController@import');    
    Route::get('courier/{courier_api_key}/import/{action}/{filename?}', 'CourierController@import');
    // Route::post('courier/{courier_api_key}/import/{action}/{value?}', 'CourierController@import');
    ///notify/connote-status/900433000020/delayed/Easter Break Holiday
    //api/courier/6jYvYj0An1/notify/connote-status/900433000020/delayed/Easter%20Break%20Holiday
    Route::get('courier/{courier_api_key}/notify/{action}/{id}/{value?}/{extra?}', 'CourierController@notify');
});
