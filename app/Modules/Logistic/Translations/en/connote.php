<?php 

return [
    'PARCEL-TOO-SMALL' => 'Parcel size too small',
    'PARCEL-INVALID' => 'Parcel invalid'
];
