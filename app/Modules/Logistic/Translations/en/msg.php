<?php

return [
    "MSG_L_RPIF" => "Shipment information received",
    "MSG_C_SPQS" => "FPX received shipment.",
    "MSG_C_SPLS" => "Fpx picked up shipment.",
    "MSG_C_AAF" => "Shipment arrived at facility and measured.",
    "MSG_C_AAL" => "Shipment arrived at :value facility",
    "MSG_C_RDFF" => "Shipment operation completed",
    "MSG_C_ADFF" => "Depart from facility to service provider.",
    "MSG_C_DT" => "Shipment has been destroyed.",
    "MSG_C_DTTM" => "shipment has been destroyed due to tradmark.",
    "MSG_C_DTCQ" => "shipment has been destroyed as request.",
    "MSG_C_HF" => "Held in facility.",
    "MSG_C_HFNP" => "Held in facility - Due to payment not ready.",
    "MSG_C_BTC" => "Return to customers.",
    "MSG_F_AF" => "Shipment arrived at facility.",
    "MSG_F_DF" => "Shipment departed from facility.",
    "MSG_Q_ECI" => "Export customs inspection.",
    "MSG_Q_UEPC" => "Under export process in China.",
    "MSG_Q_ECC" => "Relased from export Customs.",
    "MSG_Q_AAEC" => "Arrived at export center.",
    "MSG_Q_DFEC" => "Departed from export center.",
    "MSG_M_AAA" => "Arrive at transit airport.",
    "MSG_M_DFA" => "Depart from transit airport.",
    "MSG_M_AAHK" => "Arrived at Hong Kong hub.",
    "MSG_M_HA" => "Hand over to airline.",
    "MSG_M_DAHK" => "Departed from Hong Kong international airport.",
    "MSG_M_TDMF" => "Transport delay,late Arrival and miss the flight.",
    "MSG_M_TDNF" => "Transport delay,Consignment not found, airline atart investigating.",
    "MSG_M_TDPH" => "Transport delay,Due to public holiday.",
    "MSG_M_HCSC" => "Held in transit customs, Due to security check.",
    "MSG_M_TDDB" => "Transport delay,Due to destination airport was blocked.",
    "MSG_M_TD" => "Transport delay.",
    "MSG_M_TD" => "Transport delay.",
    "MSG_M_TDFM" => "Transport delay,due a flight delay occurred due to mechanical reasons.",
    "MSG_M_TDFC" => "Transport delay,due a flight was canceled and caused delay.",
    "MSG_M_TDOL" => "Transport delay,due the airline off loaded Shipments causing delay.",
    "MSG_M_TDHM" => "Transport delay,A hazardous material irregularity has occurred with the shipment.",
    "MSG_M_TDBW" => "Transport delay,Due to bad weather.",
    "MSG_M_TDSC" => "Transport delay,Due to security check.",
    "MSG_M_DC" => "Shipment arrived at destination country.",
    "MSG_M_PT" => "Pending Transportation.",
    "MSG_M_STUA" => "Shipment in transit,Consignment unloaded from the airline.",
    "MSG_M_STTC" => "Shipment in transit,To the customs.",
    "MSG_M_TDOP" => "Transport delay, due transit to other provinces.",
    "MSG_M_TDAC" => "Transport delay, Awaiting collection from agents.",
    "MSG_M_TDWF" => "Transport delay, Wait for flight.",
    "MSG_M_TDFD" => "Transport delay, due the flight delay.",
    "MSG_M_TDSA" => "Transport delay,to special areas.",
    "MSG_M_PRTS" => "Shipment returned.",
    "MSG_M_SE" => "Shipment exception.",
    "MSG_M_LDTP" => "Leave down the batch for next transit.",
    "MSG_I_CPC" => "Clearance processing completed.",
    "MSG_I_RCUK" => "Released from customs - customs cleared.",
    "MSG_I_CR" => "Customs return.",
    "MSG_I_CP" => "Customs clearance in progress.",
    "MSG_I_HC" => "Held by custom",
    "MSG_I_HCIT" => "Held in customs, Need recipient confirm duty and tax.",
    "MSG_I_HCTI" => "Held in customs, Need a valid tax identification number.",
    "MSG_I_HCPW" => "Held in customs, Need paperwork.",
    "MSG_I_HCPA" => "Held in customs, Need consignee authority.",
    "MSG_I_HCWI" => "Held in customs, Awaiting clearance instructions from consignee.",
    "MSG_I_HCPI" => "Held in customs, Pending inspection and quarantine.",
    "MSG_I_HCFE" => "Held in customs, for formal entry submission.",
    "MSG_I_HCCB" => "Held in customs, Due to customs computer system breakdown.",
    "MSG_I_HCDN" => "Held in customs, Due to data not available.",
    "MSG_I_HCLE" => "Held in customs, Due to live animal entry.",
    "MSG_I_HCVC" => "Held in customs, for verify commodity description.",
    "MSG_I_HCVV" => "Held in customs, for verify value description.",
    "MSG_I_HCPR" => "Held in customs, Pending customs released.",
    "MSG_I_HCCI" => "Held in customs, Need consignee's information.",
    "MSG_I_ICI" => "Import customs inspection.",
    "MSG_I_HCRC" => "Held in customs, Recipient will arrange clearance, awaiting confirmation.",
    "MSG_I_HCCD" => "Held in customs, Recipient confirm to pay for the duty and tax.",
    "MSG_I_HCAD" => "Held in customs, Custom appraising the duty and tax.",
    "MSG_I_HCUG" => "Held in customs, Unacceptable goods.",
    "MSG_I_HCAI" => "Held in customs, Awaiting information from Customs.",
    "MSG_I_HCCR" => "Held in customs, Paperworks rejected by customs.",
    "MSG_I_HCAP" => "Held in customs, Awaiting clearance paperwork from Recipient.",
    "MSG_I_HCWC" => "Held in customs, Wrong commodity declaration.",
    "MSG_I_HCRR" => "Held in customs, Recipient refuse to provide instruction.",
    "MSG_I_HCNR" => "Held in customs, No response from recipient.",
    "MSG_I_HCPC" => "Held in customs, Fine and penalty imposed from customs.",
    "MSG_I_HCCC" => "Held in customs, Recipient will contact the custom directly to handle.",
    "MSG_I_HCCF" => "Held in customs, Awaiting clearance file from consignee.",
    "MSG_I_CCHB" => "Handed over to recipient broker for clearance.",
    "MSG_I_CDSH" => "clearance delay due to customs strike or holiday.",
    "MSG_D_DIP" => "Delivery in progress.",
    "MSG_D_AAD" => "Arrive at the destination distribution center.",
    "MSG_D_DFD" => "Depart from the destination distribution center.",
    "MSG_D_STPP" => "In transit, it's progressing through Post network",
    "MSG_D_TNF" => "In transit to next facility",
    "MSG_D_HQ" => "Reach destination delivery station.",
    "MSG_D_SHRP" => "Shipment on hold, receiver refuse to pay fees.",
    "MSG_D_CCNS" => "Hand over to third party for delivery, no signature expected.",
    "MSG_D_SD" => "Scheduled for delivery.",
    "MSG_D_DD" => "Delivery delay.",
    "MSG_D_DDIA" => "Delivery delay, Incorrect or incomplete address.",
    "MSG_D_DDBT" => "Delivery delay, Telephone no response or bad number.",
    "MSG_D_DDRH" => "Delivery delay, Recipient not at home.",
    "MSG_D_DDOH" => "Delivery delay, Recipients on holiday.",
    "MSG_D_DDRR" => "Delivery delay, Recipients request to hold for another delivery date.",
    "MSG_D_DDMD" => "Delivery delay, Missed delivery cycle.",
    "MSG_D_DDMS" => "Delivery delay, Due to missort.",
    "MSG_D_DDRS" => "Delivery delay, Wrong destination and resend to right destination.",
    "MSG_D_DDSR" => "Delivery delay, Due to strike or riots.",
    "MSG_D_SH" => "Shipment on hold.",
    "MSG_D_SHNP" => "Shipment on hold, Payment not ready.",
    "MSG_D_SHRC" => "Shipment on hold, due to refuse pay COD charge.",
    "MSG_D_SHRD" => "Shipment on hold, Consignee refused to pay duty.",
    "MSG_D_SHND" => "Shipment on hold, Duty not pay.",
    "MSG_D_SHWC" => "Shipment on hold, Warehouse DOC and charge confirmation needed.",
    "MSG_D_SR" => "Return as shipper's request.",
    "MSG_D_RR" => "Receipient refused to accept shipment.",
    "MSG_D_PD" => "Partial Delivered.",
    "MSG_D_AP" => "Awaiting pick up by recipient as requested.",
    "MSG_D_DDCM" => "Delivery delay, consignee moved.",
    "MSG_D_DDCN" => "Delivery delay, consignee not available.",
    "MSG_D_DDSC" => "Delivery delay, Shipper contacted.",
    "MSG_D_DSRT" => "Scheduled for delivery, Remote area, delivery by third party.",
    "MSG_D_ODBP" => "Out for delivery, To beyond point.",
    "MSG_D_DDAR" => "Delivery delay, Awaiting reply from recipient.",
    "MSG_D_DDTP" => "Delivery delay, Due to traffic problems.",
    "MSG_D_DDBW" => "Delivery delay, Due to bad weather.",
    "MSG_D_DDNF" => "Delivery delay - Consignment not found, investigation started.",
    "MSG_D_DDNI" => "Delivery delay, Consignee not in the country.",
    "MSG_D_DDCA" => "Delivery delay, Consignee request to change the address.",
    "MSG_D_DDRC" => "Shipment on hold, Recipient contacted.",
    "MSG_D_SHWD" => "Shipment on hold, Due to weight discrepancy.",
    "MSG_D_SHCC" => "Shipment on hold, Need recipient confirm ODA charge.",
    "MSG_D_SHPD" => "Shipment on hold, Payment deposited.",
    "MSG_D_SHAP" => "Shipment on hold, Awaiting paperwork.",
    "MSG_D_CCPH" => "Paperwork handed over to recipient broker for clearance.",
    "MSG_D_DDCC" => "Delivery delay, countacting Consignee.",
    "MSG_D_APBR" => "Payment paid by receipient.",
    "MSG_D_DDDC" => "Delivery delay, Company closed.",
    "MSG_D_SHOA" => "Shipment on hold, out of delivery area.",
    "MSG_D_DDSS" => "Delivery delay,consignment not found,keep searching.",
    "MSG_S_SFBR" => "Booked",
    "MSG_S_CC" => "Case closed, due to overtime.",
    "MSG_S_OK" => "Shipment delivered.",
    "MSG_S_OKGP" => "Shipment delivered, Pod return by GSM short message.",
    "MSG_S_OKVP" => "Shipment delivered, Verbal POD confirmation from recipient.",
    "MSG_S_OKPO" => "Shipment delivered, Forwarded to post office.",
    "MSG_S_OKCC" => "Shipment delivered, Consignee collection.",
    "MSG_S_OKSC" => "Shipment delivered, Signature by consignee.",
    "MSG_S_OKSR" => "Shipment delivered, Signature by :value",
    "MSG_D_FAD" => "Awaiting collection at :value",
    "MSG_S_OKRC" => "Handed over to recipient broker for clearance.",
    "MSG_M_RNA" => "Redirected to new address",
    "MSG_Y_DS" => "Overseas service provider destroyed shipment.",
    "MSG_Y_CCMC" => "Shipment missing.",
    "MSG_Y_CCSC" => "Held in customs, consignment seized by customs.",
    "MSG_O_SIS" => "Shipment information sent to service provider.",
    "MSG_O_RT" => "Returned to the place of shipper.",
    "MSG_O_SD" => "Shipment damaged.",
    "MSG_O_PRUC" => "Parcel returned, Unclaimed.",
    "MSG_O_PRRR" => "Parcel returned, Receiver refused.",
    "MSG_O_PRIA" => "Parcel returned, Insufficient address.",
    "MSG_O_PRRF" => "Parcel returned, Receiver not found.",
    "MSG_O_SCBS" => "Shipment cancelled by sender.",
    "MSG_O_SC" => "Order has been cancelled.",
    "MSG_O_IR" => "Manifest accepted by system",
    "MSG_O_IRS" => "Manifest accepted by :value",
    "MSG_O_RTOC" => "Being returned to Country of origin.",
    "MSG_O_RTHM" => "Shipment return back -A hazardous material irregularity has occurred with the shipment.",
    "MSG_O_RR" => "Service provider received the shipment.",
    "MSG_D_APC" => "Arrival at Processing Center.",
    "MSG_I_DELAY" => "Customs clearance delay.",
    "MSG_D_FD" => "Delivery failed.",
    "MSG_D_TPTDL" => "In transit to receiver's location.",
    "MSG_Y_COBH" => "Case closed by hand.",
    "MSG_D_SASPS" => "Shipment arrived at self pickup site.",
    "MSG_D_HODP" => "Hand over to third party for delivery.",
    "MSG_D_VN" => "Abnormal delivery",
    "MSG_I_DG" => "Abnormal clearance",
    "MSG_Y_ADPR" => "Abandon due to prohibited or restricted items",
    "MSG_M_PCSM" => "package change shipping method",
    "MSG_D_ADC" => "Arrival in destination country",
    "MSG_D_OFD" => "Out For Delivery To Consignee",
    "MSG_C_PP" => "Picking the operating point",
    "MSG_O_RFLM" => "Last mile sp returned the package to FPX.",
    "MSG_F_AMSC" => "Arrived at transit center for international main line transport.",
    "MSG_M_DFOA" => "shipment departed from airport of origin country",
    "MSG_M_ADCA" => "Shipment arrived at airport of destination country",
    "MSG_D_TDA" => "Out for delivery again.",
    "MSG_D_FDPL" => "Delivery failed, the package lost.",
    "MSG_D_FDIA" => "Delivery failed, Incorrect or incomplete address.",
    "MSG_D_FDLO" => "Delivery failed, Recipient not at home long time.",
    "MSG_D_FDVO" => "Delivery failed, the volume is too large for courier cabinet or delivery site.",
    "MSG_M_PPRL" => "Packaged into air pallet and ready to loading plane.",
    "MSG_M_ATD" => "depart from airport.",
    "MSG_M_ATA" => "Arrived at the destination airport",
    "MSG_D_LPRS" => "The shipment is progressing through Post network of the country of destination, and will be delivered in the coming days.",
    "MSG_L_ATP" => "Already assigned courier to pick up shipment.",
    "MSG_L_PKED" => "Have picked the shipment.",
    "MSG_D_FDNC" => "Delivery failed,Unsuccessful call to recipient.",
    "MSG_D_DFCR" => "Delivery failed,Customer refused",
    "MSG_D_FDSD" => "Delivery failed,Damaged",
    "MSG_O_SPHS" => "Service provider hold the shipment",
    "MSG_O_SHFC" => "Service held the shipment for consignee collection",
    "MSG_O_CRLB" => "Shipping Label Created",
    "MSG_M_DFIC" => "Shipment departed from international transit center.",
    "MSG_D_RA" => "Service provider abnormal receive",
    "MSG_D_SPRA" => "Pickup site abnormal receive",
    "MSG_D_POD" => "Package expired at pickup site.",
    "MSG_D_FDNR" => "Attempted delivery, Unable to gain access",
    "MSG_D_ADWP" => "Awaiting collection at local post office.",
    "MSG_D_FDWP" => "Attempted delivery, redirected to Post Office",
    "MSG_F_ZGWC" => "The loading is completed and the goods will be sent to the port of shipment",
    "MSG_M_DFSP" => "Departure from departure port",
    "MSG_M_AADP" => "Arriving at destination port",
    "MSG_C_ZDCH" => "Total order out",
    "MSG_M_OTH" => "",
];