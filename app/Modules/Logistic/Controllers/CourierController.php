<?php

namespace App\Modules\Logistic\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Quatius\Logistic\Models\Courier;

class CourierController extends Controller
{
    public function __construct()
    {
        $this->setupTheme(config('theme.map.registered.theme'), config('theme.map.registered.layout'));
    }

    /**
     * explort of the connotes
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function export(Request $request, $key, $action, $file="import.json")
    {
        try {
            app('debugbar')->disable();

            config()->set('quatius.logistic.history.status-active', config('quatius.logistic.history.status-system', 0));

            $courier = app('courier')->findwhere(['access_key'=>$key])->first();
            abort_unless($courier, 404, "Invalid Key");

            $action = strtolower($action);
            $ext = "json";
            $extPos = strpos($file,".");

            if ($extPos !== false){
                $ext = strtolower(substr($file,$extPos+1));
                $file = substr($file,0, $extPos);
            }

            $datas = $courier->export($action, $file, $ext);

            if (is_string($datas)){
                return response($datas);
            }

            $headings = array_keys(array_shift($datas));

            if ($ext == 'json'){
                return response()->json($datas);
            }

            $tmp_handle = fopen('php://temp', 'r+');
            
            fputcsv($tmp_handle, $headings);
            
            foreach ($datas as $data){
                fputcsv($tmp_handle, array_values($data));
            }
            rewind($tmp_handle);
            
            $file_contents = stream_get_contents($tmp_handle);

            // clean up your temporary storage handle
            fclose($tmp_handle);

            $filename = $file.'.'.$ext;
            $headers = [
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename='. $filename. ';'
            ];

            return response()->stream(function () use ($file_contents)  {
                echo $file_contents;
            }, 200, $headers);
        }  catch (\Symfony\Component\HttpKernel\Exception\HttpException $e) {
            return response()->json([
                    'message' => $e->getMessage(),
                    'code'    => $e->getStatusCode(),
                    ], $e->getStatusCode());
        
        } catch (\Exception $e) {
             return response()->json([
                'message' => $e->getMessage(),
                'code'    => 400,
            ], 400);
        }
    }
    
    public function import(Request $request, $key, $action, $filename="import.json"){
        try {
            app('debugbar')->disable();

            config()->set('quatius.logistic.history.status-active', config('quatius.logistic.history.status-system', 0));

            $courier = app('courier')->findwhere(['access_key'=>$key])->first();
            abort_unless($courier, 404, "Invalid Key");

            $isSuccess = $courier->import($action, $filename, $request);

            return response()->json(["success"=>$isSuccess]);

        }  catch (\Symfony\Component\HttpKernel\Exception\HttpException $e) {
            return response()->json([
                    'message' => $e->getMessage(),
                    'code'    => $e->getStatusCode(),
                    ], $e->getStatusCode());
        
        } catch (\Exception $e) {
             return response()->json([
                'message' => $e->getMessage(),
                'code'    => 400,
            ], 400);
        }
    }
    
///notify/{action}/{id}/{value?}/{extra?}', 'CourierController@notify');
    public function notify(Request $request, $key, $action, $id, $value="", $extra=""){
        try {
            app('debugbar')->disable();
            config()->set('quatius.logistic.history.status-active', config('quatius.logistic.history.status-system', 0));
            
            $courier = app('courier')->findwhere(['access_key'=>$key])->first();
            abort_unless($courier, 404, "Invalid Key");

            $isSuccess = $courier->notify($action, $id, $value, $extra);
            
            return response()->json(["success"=>$isSuccess]);
        }  catch (\Symfony\Component\HttpKernel\Exception\HttpException $e) {
            return response()->json([
                    'message' => $e->getMessage(),
                    'code'    => $e->getStatusCode(),
                    ], $e->getStatusCode());
        
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
                'code'    => 400,
            ], 400);
        }
    }
}
