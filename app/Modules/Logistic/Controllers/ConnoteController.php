<?php

namespace App\Modules\Logistic\Controllers;

use App\Http\Controllers\Controller;
use PDF;
use Illuminate\Http\Request;

use Quatius\Logistic\Repositories\ConnoteRepository;
use Quatius\Logistic\Repositories\Presenters\ConnoteIndexPresenter;
use DB;
use Form;
use Excel;
use Dompdf\Dompdf;
use Mpdf\Output\Destination;
use Quatius\Logistic\Repositories\Presenters\ConnoteSubmitPresenter;
use Mail;

class ConnoteController extends Controller
{
    private $repo;

    public function __construct(ConnoteRepository $repo)
    {
        $this->setupTheme(config('theme.map.registered.theme'), config('theme.map.registered.layout'));
        $this->repo = $repo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            $query = $this->repo->byUserAccount()->statusDateOf('submitted')->manifestNo()->latestStatus()->whereNotNull('status');
            return response()->json(dataTableQuery($request->all(), $query));
        }
        //$this->theme->layout('ajax');
        return $this->theme->of('Logistic::connote.index')->render();
    }

    public function submitted(Request $request)
    {
        $query = $this->repo->byUserAccount()->with('addresses')->statusDateOf('submitted')->manifestNo()->latestStatus()
            ->where(function($query){
                return $query->whereNotNull('status')
                    ->whereNotIn('status',config('quatius.logistic.status.group.pending',['ready to print', 'submitted', 'booking pending']));
            });
            
        if ($request->get('status',''))
            $query->whereIn('status',config('quatius.logistic.status.group.'.$request->get('status',''),[]));
            // $query->whereStatus($request->get('status',''));

        $query->distinct();
        return response()->json(dataTableQuery($request->all(), $query, new ConnoteSubmitPresenter()));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function creations(Request $request)
    {
        if ($request->wantsJson()) {
            $data = $this->repo->byUser()->with('addresses')->statusDateOf('submitted')->manifestNo()->latestStatus()
            ->where(function($query){
                return $query->whereStatus(null)
                    ->orWhereIn('status', config('quatius.logistic.status.group.pending',['ready to print', 'submitted', 'booking pending']));
            })->distinct()->get();
            
            $this->repo->setPresenter(ConnoteSubmitPresenter::class);
            return response()->json($this->repo->parserResult($data));
        }
        //$this->theme->layout('ajax');
        return $this->theme->of('Logistic::connote.creation')->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $connote = $this->repo->newInstance([]);
        $user = app('account')->user();
        if ($user){
            if ($user->getParams('logistic.prefer.sender',0))
                $connote->sender_address_id = hashids_encode($user->getParams('logistic.prefer.sender',0));
            
            if ($user->getParams('logistic.prefer.pickup',0))
                $connote->pickup_address_id = hashids_encode($user->getParams('logistic.prefer.pickup',0));
        }
        $courier = app('courier')->findWhere(['type'=>config('quatius.logistic.default_courier',''), "status"=>2])->first();

        $connote->courier_id = hashids_encode($courier->id);

        Form::populate($connote);
        // return $this->theme->of('Logistic::create', compact($connote))->render();
        // return $this->theme->of('Logistic::connote.create', compact($connote))->render();
        //$this->theme->layout('ajax');
        return view('Logistic::connote.create', compact($connote))->render();
        //return view('Logistic::connote.create', compact($connote));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datas = $request->except('account_id');

        $datas['courier_id'] = $request->get('courier_id','')?hashids_decode($request->get('courier_id','')):null;
        $datas['sender_address_id'] = $request->get('sender_address_id','')?hashids_decode($request->get('sender_address_id','')):null;
        $datas['pickup_address_id'] = $request->get('pickup_address_id','')?hashids_decode($request->get('pickup_address_id','')):null;

        $user = app('account')->user();
        if ($user){
            $user->setParams('logistic.prefer.courier', $datas['courier_id']);
            $user->setParams('logistic.prefer.sender', $datas['sender_address_id']);
            $user->setParams('logistic.prefer.pickup', $datas['pickup_address_id']);
            $user->save();
        }

        $connote =  $this->repo->saveFormData($datas);

        $courier = app('courier')->load($datas['courier_id']);
        if ($courier) $courier->validate([$connote], []);

        $InputError = $connote->getParams("error.inputs",[]);
        if ($InputError){
            $data = $connote->toArray();
            $data = array_merge($data, $connote->receiver->toArray());
            Form::populate($data);
            
            Form::withErrors(new \Illuminate\Support\MessageBag(array_map(function($val){return trans($val);}, $InputError)));
            $viewForm = view('Logistic::connote.edit', ['connote'=>$connote])->render();
            return response()->json([
                "view"=>$viewForm, 
                'message'  => "Connote Created with Errors",
                'code'     => 403
            ], 303);
        }

            $courier->createConnotes(collect([$connote]));

       // if ($request->wantsJson()) {
            $this->repo->setPresenter(ConnoteIndexPresenter::class);

            return response()->json([
                "data"=>$this->repo->parserResult($connote),
                'message'  => "Connote Created",
                'code'     => 204
            ], 201);
        //}
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $connote = $this->repo->with(['histories','histories.status','manifest','manifest.pickup','manifest.sender','addresses'])->findOrFail($id);
        $connote->setRelation('histories', 
            $connote->histories->filter(function ($history) { 
                return $history->status->status >= config('quatius.logistic.history.status-active', 2);
            })
        );

        if ($request->wantsJson()) {
            $this->repo->setPresenter(ConnoteIndexPresenter::class);
            return response()->json($this->repo->parserResult($connote));
        }
        
        return view('Logistic::connote.show', ['connote'=>$connote]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $connote = $this->repo->findOrFail($id);
        $data = $connote->toArray();
        $data = array_merge($data, $connote->receiver->toArray());

        $data['sender_address_id'] = $data['sender_address_id']?hashids_encode($data['sender_address_id']):null;
        $data['pickup_address_id'] = $data['pickup_address_id']?hashids_encode($data['pickup_address_id']):null;
        $data['courier_id'] = $data['courier_id']?hashids_encode($data['courier_id']):null;

    
        Form::populate($data);
        $InputError = $connote->getParams("error.inputs",[]);
        
        Form::withErrors(new \Illuminate\Support\MessageBag(array_map(function($val){return trans($val);}, $InputError)));
        return view('Logistic::connote.edit', ['connote'=>$connote]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $connote = $this->repo->findOrFail($id);
        
        $datas = $request->except('account_id');

        $datas['courier_id'] = $request->get('courier_id','')?hashids_decode($request->get('courier_id','')):null;
        $datas['sender_address_id'] = $request->get('sender_address_id','')?hashids_decode($request->get('sender_address_id','')):null;
        $datas['pickup_address_id'] = $request->get('pickup_address_id','')?hashids_decode($request->get('pickup_address_id','')):null;

        $user = app('account')->user();
        if ($user){
            $user->setParams('logistic.prefer.courier', $datas['courier_id']);
            $user->setParams('logistic.prefer.sender', $datas['sender_address_id']);
            $user->setParams('logistic.prefer.pickup', $datas['pickup_address_id']);
            $user->save();
        }

        $this->repo->setPresenter(null);
        $connote =  $this->repo->saveFormData($datas, $connote);

        $courier = app('courier')->load($datas['courier_id']);
        
        if ($courier) $courier->validate([$connote], []);

        $InputError = $connote->getParams("error.inputs",[]);
        if ($InputError){
            $data = $connote->toArray();
            $data = array_merge($data, $connote->receiver->toArray());
            Form::populate($data);

            Form::withErrors(new \Illuminate\Support\MessageBag(array_map(function($val){return trans($val);}, $InputError)));
            $viewForm = view('Logistic::connote.edit', ['connote'=>$connote])->render();
            return response()->json([
                "view"=>$viewForm, 
                'message'  => "Connote Update Failed",
                'code'     => 403
            ], 303);
        }
        
        $courier->createConnotes(collect([$connote]));

        $this->repo->setPresenter(ConnoteIndexPresenter::class);

        return response()->json([
            "data"=>$this->repo->parserResult($connote),
            'message'  => "Connote Updated",
            'code'     => 204
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cancel(Request $request, $id)
    {
        $connote = $this->repo->latestStatus()->findOrFail($id);

        $isSuccess = $connote->status != "booking pending"?$this->repo->bookingRollback($connote):false;
        if ($isSuccess){
            $connote->setStatus("booking cancel", []);
        }
        
        return response()->json(["success" => $isSuccess]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $deleteId = $this->repo->delete($id);
        
        if ($request->wantsJson()) {
            return response()->json(["success" => $deleteId]);
        }

        return redirect('connote'); // index
    }

    /**
     * Change the status of the connote
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function statusChange(Request $request, $status, $comment="")
    {
        
        if ($status == config('quatius.logistic.manifest.create-status','submitted')) {
            return app(ManifestController::class)->store($request);
        }

        $data = ['comments'=>$comment];
        
        $connoteIds = array_map(function ($val){return hashids_decode($val);}, $request->get('ids',[]));

        $connotes = $this->repo->statusChange($connoteIds, $status, $data);
        
        $this->repo->setPresenter(ConnoteIndexPresenter::class);
        return response()->json($this->repo->parserResult($connotes));
    }
       /**
     * Change the status of the connote
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function importExcelTest(Request $request)
    {
        try{
            $data = Excel::toArray(new \Quatius\Logistic\Models\ConnotesImport(),  base_path('docs/ImportConnotesTest.xls'));
            if (isset($data[0]) && $data[0]){
                $savedIds = [];
                $mapData = $this->mappingCellHeader($data[0]);
                
                foreach ($mapData as $row){
                    $savedIds[] = $this->repo->saveFormData($row)->id;
                }
                $this->repo->setPresenter(ConnoteIndexPresenter::class);
                $datas = $this->repo->find($savedIds);

                $datas['success'] = true;
                $datas['message'] = "Import success";
                $datas['code']  = 204;

                return response()->json($mapData);
            }
            
            return response()->json($data, 201);

        }  catch (\Symfony\Component\HttpKernel\Exception\HttpException $e) {
            return response()->json([
                    'message' => $e->getMessage(),
                    'code'    => $e->getStatusCode(),
                    ], $e->getStatusCode());
        
        } catch (\Exception $e) {
             return response()->json([
                'message' => $e->getMessage(),
                'code'    => 400,
            ], 400);
        }   
    }
    /**
     * Change the status of the connote
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function importExcel(Request $request)
    {
        /*if ($request->hasFile('import_file'))
    	{
            try{*/
                $courier_id = $request->get('courier_id','')?hashids_decode($request->get('courier_id','')):null;
                $sender_address_id = $request->get('sender_address_id','')?hashids_decode($request->get('sender_address_id','')):null;
                $pickup_address_id = $request->get('pickup_address_id','')?hashids_decode($request->get('pickup_address_id','')):null;

                $data = Excel::toArray(new \Quatius\Logistic\Models\ConnotesImport(),$request->file('import_file'));
                if (isset($data[0]) && $data[0]){
                    $savedIds = [];
                    $mapData = $this->mappingCellHeader($data[0]);
                    
                    foreach ($mapData as $row){
                        $row['courier_id'] = $courier_id;
                        $row['sender_address_id'] = $sender_address_id;
                        $row['pickup_address_id'] = $pickup_address_id;

                        $savedIds[] = $this->repo->saveFormData($row)->id;
                    }
                    
                    $connotes = $this->repo->with(['addresses', 'pickup', 'sender'])->whereIn('id',$savedIds)->get();
                    $courier = app('courier')->find($courier_id);
                    if ($courier){
                        $courier->createConnotes($connotes->filter(function ($connote) {return !($connote->remark);}));
                    }
                    $this->repo->setPresenter(ConnoteIndexPresenter::class);
            
                    $user = app('account')->user();
                    if ($user){
                        $user->setParams('logistic.prefer.courier', $courier_id);
                        $user->setParams('logistic.prefer.sender', $sender_address_id);
                        $user->setParams('logistic.prefer.pickup', $pickup_address_id);
                        $user->save();
                    }

                    $datas = $this->repo->parserResult($connotes);
                    $datas['success'] = true;
                    $datas['message'] = "Import success";
                    $datas['code']  = 204;
    
                    return response()->json($datas);
                }
                
                return response()->json([
                    "success" => false,
                    'message'  => "Import Failed",
                    'code'     => 204
                ], 201);
    /*
            }  catch (\Symfony\Component\HttpKernel\Exception\HttpException $e) {
                return response()->json([
                        'message' => $e->getMessage(),
                        'code'    => $e->getStatusCode(),
                        ], $e->getStatusCode());
            
            } catch (\Exception $e) {
                 return response()->json([
                    'message' => $e->getMessage(),
                    'code'    => 400,
                ], 400);
            }   
        }*/
        return response()->json(["success"=>false,"data"=>[]]);
    }
    
    private function mappingCellHeader($data){
        $mapData = [];
        $mapFields = [
            "citysuburbtown" => "city",
            "stateprovince" => "state",
            "address_line_1" => "address_1",
            "address_line_2" => "address_2",
            "weighkg" => "weight",
            "lenthcm" => "length",
            "widthcm" => "width",
            "heigthcm" => "height",
            "reference_number" => "reference",
        ];
        
        foreach ($data as $row){
            $tmpData = [];
            foreach ($row as $cellfield => $value){
                $field = isset($mapFields[$cellfield])?$mapFields[$cellfield]:$cellfield;

                if (array_search($field,['width','height','length']) !== false)
                    $value = intval($value);
                elseif (array_search($field,['weight','declared_value']) !== false)
                    $value = floatval($value);

                $tmpData[$field] = $value;
            }
            if (!isset($tmpData["country"]))
                $tmpData["country"] = 'AU';
                
            unset($tmpData['account_id']);
            unset($tmpData["remark"]);
            $mapData[] = $tmpData;
        }
        return $mapData;
    }

    /**
     * explort of the connotes
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function exportExcel(Request $request, $status="")
    {
        $connotes = $this->repo->with(['pickup','addresses'])->latestStatus()->get();

        $file = fopen(storage_path('app/export.csv'),"w");
        $datas = [];

        $datas['data'] = $connotes;

        $headings = [
            "receiverFirstName",
            "receiverStreetAddress1",
            "receiverCity",
            "receiverCountry",
            "receiverProvince",
            "receiverPostCode",
            "receiverContact",
            "receiverPhone1",
            "receiverEmail",
            "senderFirstName",
            "senderStreetAddress",
            "senderCity",
            "senderProvince",
            "senderCountry",
            "senderPostCode",
            "senderPhone1",
            "pickupFirstName",
            "pickupStreetAddress",
            "pickupCity",
            "pickupProvince",
            "pickupCountry",
            "pickupPostCode",
            "pickupPhone1",
            "pickupEmail",
            "parcelType",
            "No of Items",
            "refNo",
            "length",
            "width",
            "height",
            "weight"
		];
		fputcsv($file, $headings);
		$csvData = [];		
		foreach ($connotes as $connote){
            $clean_message = preg_replace("/\r\n/", " ", $connote->instruction);
            $data = [
                "receiverFirstName" => $connote->receiver->first_name,
                "receiverStreetAddress1" => $connote->receiver->address_1,
                "receiverCity" => $connote->receiver->city,
                "receiverCountry" => $connote->receiver->country,
                "receiverProvince" => $connote->receiver->state,
                "receiverPostCode" => $connote->receiver->postcode,
                "receiverContact" => $connote->receiver->first_name,
                "receiverPhone1" => $connote->receiver->phone_1,
                "receiverEmail" => $connote->receiver->email,
                "senderFirstName" => $connote->sender->first_name,
                "senderStreetAddress" => $connote->sender->address_1,
                "senderCity" => $connote->sender->city,
                "senderProvince" => $connote->sender->state,
                "senderCountry" => $connote->sender->country,
                "senderPostCode" => $connote->sender->postcode,
                "senderPhone1" => $connote->sender->phone_1,
                "pickupFirstName" => $connote->first_name,
                "pickupStreetAddress" => $connote->pickup->address_1,
                "pickupCity" => $connote->pickup->city,
                "pickupProvince" => $connote->pickup->state,
                "pickupCountry" => $connote->pickup->country,
                "pickupPostCode" => $connote->pickup->postcode,
                "pickupPhone1" => $connote->pickup->phone_1,
                "pickupEmail" => $connote->pickup->email,
                "parcelType" => $connote->parcel_type,
                "No of Items" => $connote->items,
                "refNo" => $connote->reference,
                "length" => $connote->length,
                "width" => $connote->width,
                "height" => $connote->height,
                "weight" => round($connote->weight, 1)
            ];
            fputcsv($file, array_values($data));
            $csvData[] = $data;
		}
		$datas['data'] = $csvData;
		fclose($file);

        $datas['success'] = true;
        return response()->json($datas);
    }

    public function printLabelPdf(Request $request, $id)
    {
        app('debugbar')->disable();
        $connote = $this->repo->statusDateOf('ready to print', 'submitted')->with(['manifest','addresses'])->findOrFail($id);
        if (!$connote->is_printed){
            $connote->courier->labelPrinted($connote);
        }
        
        $file = $this->createLabelPdf($connote);
        return response()->stream(function() use ($file){
            echo file_get_contents($file);

        }, 200, [
            'Cache-Control' => 'no-cache',
            'Content-Type' => 'application/pdf',
        ]);
    }

    private function createLabelPdf($connote){
        $path = storage_path('app/logistic/connote-labels');

        if (!is_dir($path)) mkdir($path, 0777, true);

        $filePath = $path."/".$connote->consignment.".pdf";

        if (file_exists($filePath)) return $filePath;

        if (!$connote->submitted_at)
            $connote->submitted_at = $connote->created_at;
        
        $html = view('Logistic::connote.label-pdf', ['connote'=>$connote])->render();
                
        $mpdf = new \Mpdf\Mpdf([
            'margin_left' => 0,
            'margin_right' => 0,
            'margin_top' => 0,
            'margin_bottom' => 0,
            'mode' => 'utf-8',
            'format' => 'A6']
        );
        $mpdf->WriteHTML($html);

        $mpdf->Output($filePath, Destination::FILE);
        
        return $filePath;
    }

    public function viewLabel(Request $request, $id)
    {
        $connote = $this->repo->statusDateOf('submitted')->with(['manifest','addresses'])->findOrFail($id);
        
        $path = storage_path('app/logistic/connote-labels');
        if (!is_dir($path)) mkdir($path, 0777, true);
        $fileImg = $path.'/'.$connote->consignment.'.jpg';

        if (!file_exists($fileImg)){
            $file = $this->createLabelPdf($connote);

            $imgExt = new \Imagick();
            $imgExt->setResolution(100, 100);
            $imgExt->readImage($file.'[0]');
            
            $imgExt->setImageFormat('jpeg');
            //$imgExt->setImageCompression(\Imagick::COMPRESSION_JPEG); 
            //$imgExt->setImageCompressionQuality(100);
            if (method_exists($imgExt,'flattenImages'))
               $imgExt = $imgExt->flattenImages();
            $imgExt->writeImages($fileImg, true);
        }
        
        return response()->stream(function() use($fileImg){
            echo file_get_contents($fileImg);
        }, 200, [
            'Cache-Control' => 'no-cache',
            'Content-Type' => 'image/jpg',
        ]);
        
    }

    public function downloadTemplate(Request $request)
    {   
        return response()->stream(function() {
            echo file_get_contents(__DIR__."/../Assets/ImportConnotes.xls");
        }, 200, [
            'Cache-Control' => 'no-cache',
            'Content-Type' => 'application/vnd.ms-excel',
        ]);
        
    }

    public function viewPOD(Request $request, $id)
    {
        return response()->stream(function(){
            echo file_get_contents("https://www.track-pod.com/assets/Uploads/Delivery-Software-and-Proof-of-Delivery/proof-of-delivery-app.png");
        }, 200, [
            'Cache-Control' => 'no-cache',
            'Content-Type' => 'image/png',
        ]);
        
    }


    public function numConnotesIndicator(){
        return response()->json([
            "num_conns" => $this->repo->numConnotesIndicator()
        ]);

    }

    public function numConnotesDanger(){
        return response()->json([
            "num_conns" => $this->repo->numConnotesDanger()
        ]);

    }

    
    /**
     * Display a tracking info.
     *
     * @return \Illuminate\Http\Response
     */
    public function tracking(Request $request, $consignment = "")
    {
        $consignment = $request->get('consignment', $consignment);
        abort_unless($consignment, 404, "Require Consignment Reference");

        $connote = $this->repo->whereConsignment($consignment)->with(['manifest.pickup','histories.status'])->first();
        abort_unless($connote, 404, "Not found");
        $connote->setRelation('histories', 
            $connote->histories->filter(function ($history) { 
                return $history->status->status >= config('quatius.logistic.history.status-active', 2);
            })
        );
        
        if ($request->wantsJson()) {
            return $connote->transform();
        }

        if ($connote != null){
            return response()->json([
                'data' => $connote,
                'view'  => view('Logistic::connote.partials.tracking-frontend', ["connote" => $connote])->render(),
                'code'    => 200
            ], 200);
        }else{
            return response()->json([
                'message' => "Not found",
                'data' => $consignment,
                'code'    => 404
            ], 404);
        }

    }

    /**
     * Load Enquiry Input form 
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function loadFormEnquiry(Request $request, $id)
    {
        $connote = $this->repo->latestStatus()->findOrFail($id);

        // app('log')->info('id = ',[$id]);
        // app('log')->info('connote = ',[$connote]);
        
        $data = $connote->toArray();
        $data = array_merge($data, $connote->sender->toArray());
        Form::populate($data);
        
        return view('Logistic::connote.partials.enquiry-form', ['connote'=>$connote]);
    }

    /**
     * Send Enquiry Email for a corresponding connote
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sendEnquiry(Request $request)    
    {        
        $connote = $this->repo->latestStatus()->whereConsignment($request->get('consignment',''))->first();
        
        $data = [];
        $data = $connote->toArray();

        $data = array_merge($data, ['msg_content' => $request->get('enquiry', '')]);

        $data = array_merge($data, ['sender_fullname' => $connote->sender->getFullname()]);
        $data = array_merge($data, ['sender_address_1' => $connote->sender->address_1]);
        $data = array_merge($data, ['sender_address_2' => $connote->sender->address_2]);
        $data = array_merge($data, ['sender_city' => $connote->sender->city]);
        $data = array_merge($data, ['sender_state' => $connote->sender->state]);
        $data = array_merge($data, ['sender_postcode' => $connote->sender->postcode]);
        $data = array_merge($data, ['sender_phone_1' => $connote->sender->phone_1]);

        $data = array_merge($data, ['pickup_fullname' => $connote->pickup->getFullname()]);
        $data = array_merge($data, ['pickup_address_1' => $connote->pickup->address_1]);
        $data = array_merge($data, ['pickup_address_2' => $connote->pickup->address_2]);
        $data = array_merge($data, ['pickup_city' => $connote->pickup->city]);
        $data = array_merge($data, ['pickup_state' => $connote->pickup->state]);
        $data = array_merge($data, ['pickup_postcode' => $connote->pickup->postcode]);
        $data = array_merge($data, ['pickup_phone_1' => $connote->pickup->phone_1]);

        $data = array_merge($data, ['receiver_fullname' => $connote->receiver->getFullname()]);
        $data = array_merge($data, ['receiver_company' => $connote->receiver->company]);
        $data = array_merge($data, ['receiver_address_1' => $connote->receiver->address_1]);
        $data = array_merge($data, ['receiver_address_2' => $connote->receiver->address_2]);
        $data = array_merge($data, ['receiver_city' => $connote->receiver->city]);
        $data = array_merge($data, ['receiver_state' => $connote->receiver->state]);
        $data = array_merge($data, ['receiver_postcode' => $connote->receiver->postcode]);
        $data = array_merge($data, ['receiver_phone_1' => $connote->receiver->phone_1]);

        $status_histories = [];
        foreach($connote->histories->sortByDesc('created_at') as $ind => $history){
            $status_histories[$ind] = array_merge($history->toArray(), $history->status->toArray());
        }
        $data = array_merge($data, ['histories' => $status_histories]);        

        // $data = array_merge($data, ['courier_logo' => ($connote->courier->hasLogo() ? $connote->courier->getLogoBase64():"")]);


        Mail::send('Logistic::connote.emails.enquiry', $data, function($message) use ($request){
            $message->from($request->get('email', ''),'Online - '.$request->get('company', '').' - '.$request->get('first_name', '').' '.$request->get('last_name', ''));
            $message->to(env('APP_EMAIL_INFO_ADDRESS','info@quatiuslogistics.com'));
            $message->subject($request->get('subject', ''));            
        });

        return response()->json([
            "message" => "Enquiry has successfully submitted!",
            "code" => 200
        ], 200);
    }



    /**
     * To test a connote view
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function testView(Request $request, $id)
    {
        $connote = $this->repo->latestStatus()->findOrFail($id);
        return view('Logistic::connote.emails.submitted-notification', ['connote'=>$connote]);
        // return view('Logistic::connote.emails.delivered-notification', ['connote'=>$connote]);
        // return view('Logistic::connote.emails.with-driver-notification', ['connote'=>$connote]);
        // return view('Logistic::connote.emails.delayed-notification', ['connote'=>$connote]);
        // return view('Logistic::connote.emails.cancelled-notification', ['connote'=>$connote]);
        // return view('Logistic::connote.emails.damaged-notification', ['connote'=>$connote]);
    }
}
