<?php

namespace App\Modules\Logistic\Controllers;

use App\Http\Controllers\Controller;
use PDF;
use Illuminate\Http\Request;

use Quatius\Logistic\Repositories\ConnoteRepository;
use Quatius\Logistic\Repositories\Presenters\ConnoteIndexPresenter;
use DB;
use Form;
use Excel;
use Dompdf\Dompdf;
use Mpdf\Output\Destination;
use Quatius\Logistic\Repositories\Presenters\ConnoteSubmitPresenter;

class ManifestController extends Controller
{
    private $repo;

    public function __construct()
    {
        $this->setupTheme(config('theme.map.registered.theme'), config('theme.map.registered.layout'));
        $this->repo = app('connote');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            $requestData = $request->all();
            $query = $this->repo->manifestModel()->byUserAccount()->numConnotes()->latestStatus();

            return response()->json(dataTableQuery($requestData, $query));
        }

        return $this->theme->of('Logistic::manifest.index')->render();
    }

        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = [];
            
            $connoteIds = array_map(function ($val){return hashids_decode($val);}, $request->get('ids',[]));

            $manifests = $this->repo->createManifest($connoteIds, config('quatius.logistic.manifest.create-status','submitted'), $data);
            $success = !!$manifests->count();
            //abort_unless($success, 303, "Validate Error");

            return response()->json([
                "success" => $success,
                'message'  => "Create Manifest success",
                'code'     => 204
            ], 201);

        }  catch (\Symfony\Component\HttpKernel\Exception\HttpException $e) {
            return response()->json([
                    'message' => $e->getMessage(),
                    'code'    => $e->getStatusCode(),
                    ], $e->getStatusCode());
        
        } catch (\Exception $e) {
             return response()->json([
                'message' => $e->getMessage(),
                'code'    => 400,
            ], 400);
        }

        
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $manifest = $this->repo->manifestModel()->with(['connotes'=>function($query){
            return $query->with('addresses')->latestStatus();
        }])->findOrfail($id);
        
        $manifest->setRelation('connotes', $manifest->connotes->unique('id'));//remove duplicate
        
        if ($request->wantsJson()) {
            $data = $manifest->transform();
            if (isset($manifest->getRelations()['connotes'])){
                $this->repo->setPresenter(ConnoteIndexPresenter::class);
                $data['connotes'] = $this->repo->parserResult($manifest->getRelations()['connotes'])['data'];
            }
            return response()->json(["data"=>$data]);
        }

        return view('Logistic::manifest.show', ['manifest'=>$manifest]);
    }

    public function viewLabel(Request $request, $id)
    {
        $manifest = $this->repo->manifestModel()->with('connotes')->findOrfail($id);

        // $connote = $this->repo->statusDateOf('submitted')->with(['manifest','contacts'])->findOrFail($id);
        
        $path = storage_path('app/logistic/manifest-labels');
        if (!is_dir($path)) mkdir($path, 0777, true);
        $fileImg = $path.'/'.$manifest->manifest_no.'.jpg';

        if (!file_exists($fileImg)){
            
            $file = $this->createLabelPdf($manifest);

            $imgExt = new \Imagick();
            $imgExt->readImage($file.'[0]');
            $imgExt->setImageFormat('jpeg');
            if (method_exists($imgExt,'flattenImages'))
                $imgExt = $imgExt->flattenImages();
            $imgExt->writeImages($fileImg, true);
        }
        return response()->stream(function() use($fileImg){
            echo file_get_contents($fileImg);
        }, 200, [
            'Cache-Control' => 'no-cache',
            'Content-Type' => 'image/jpg',
        ]);
    }

    public function printLabelPdf(Request $request, $id)
    {
        // $connote = $this->repo->statusDateOf('submitted')->with(['manifest','contacts'])->findOrFail($id);
        $manifest = $this->repo->manifestModel()->with('connotes')->statusDateOf('submitted')->findOrfail($id);

        //return view('Logistic::manifest.label-pdf', ['manifest'=>$manifest]);
        $file = $this->createLabelPdf($manifest);

        return response()->stream(function() use ($file){
            echo file_get_contents($file);

        }, 200, [
            'Cache-Control' => 'no-cache',
            'Content-Type' => 'application/pdf',
        ]);
    }

    private function createLabelPdf($manifest){
        $path = storage_path('app/logistic/manifest-labels');

        if (!is_dir($path)) mkdir($path, 0777, true);

        $filePath = $path."/".$manifest->manifest_no.".pdf";

        if (file_exists($filePath)) return $filePath;

        $html = view('Logistic::manifest.label-pdf', ['manifest'=>$manifest])->render();
                
        $mpdf = new \Mpdf\Mpdf([
            'margin_left' => 0,
            'margin_right' => 0,
            'margin_top' => 0,
            'margin_bottom' => 0,
            'mode' => 'utf-8',
            'format' => 'A6']
        );
        $mpdf->WriteHTML($html);

        $mpdf->Output($filePath, Destination::FILE);
        
        return $filePath;
    }

}
