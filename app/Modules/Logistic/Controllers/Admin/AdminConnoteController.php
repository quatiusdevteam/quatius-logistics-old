<?php

namespace App\Modules\Logistic\Controllers\Admin;

use App\Http\Controllers\AdminWebController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Quatius\Logistic\Repositories\ConnoteRepository;
use Quatius\Logistic\Repositories\Presenters\ConnoteSubmitPresenter;

class AdminConnoteController extends AdminWebController
{
    private $repo;

    public function __construct(ConnoteRepository $repo)
    {
        parent::__construct();
        $this->repo = $repo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            $query = $this->repo->manifestNo()->latestStatus()->whereNotNull('courier_consignment')->whereNotNull('status')->accountField('name')->accountField('account_code');
            $query->distinct();
            //return response()->json(dataTableQuery($request->all(), $query, new ConnoteSubmitPresenter()));
            $this->repo->setPresenter(ConnoteSubmitPresenter::class);
            return response()->json($this->repo->parserResult($query->get()));
        }
        //$this->theme->layout('ajax');
        return $this->theme->of('Logistic::admin.connote.index')->render();
    }
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function sycnRemoteTracking(Request $request, $id)
    {
        try{
            $connote =  app('connote')->with(['histories','manifest.courier'])->findOrFail($id);
            $changes = app('courier')->syncARemoteTracking($connote);
            
            return response()->json([
                'message'  => $changes ." tracking received from ".$connote->manifest->courier->label,
                'code'     => 204,
                ], 201);

        } catch (\Symfony\Component\HttpKernel\Exception\HttpException $e) {
            return response()->json([
                    'message' => $e->getMessage(),
                    'code'    => $e->getStatusCode(),
                    ], $e->getStatusCode());
        
        } catch (\Exception $e) {
            return response()->json([
                    'message' => $e->getMessage(),
                    'code'    => 400,
                    ], 400);
        }
    }

    public function sycnAllRemoteTracking(Request $request)
    {
        try{
            $numData = app('courier')->syncRemoteTracking();
            
            return response()->json([
                'message'  => ($numData) ." tracking received from Remote Server",
                'code'     => 204,
                ], 201);

        } catch (\Symfony\Component\HttpKernel\Exception\HttpException $e) {
            return response()->json([
                    'message' => $e->getMessage(),
                    'code'    => $e->getStatusCode(),
                    ], $e->getStatusCode());
        
        } catch (\Exception $e) {
            return response()->json([
                    'message' => $e->getMessage(),
                    'code'    => 400,
                    ], 400);
        }
    }

    public function show(Request $request, $id)
    {
        $connote = $this->repo->with(['histories','histories.status','manifest','manifest.pickup','manifest.sender','addresses'])->findOrFail($id);
        $connote->setRelation('histories', 
            $connote->histories->filter(function ($history) { 
                return $history->status->status >= config('quatius.logistic.history.status-active', 2);
            })
        );

        if ($request->wantsJson()) {
            $this->repo->setPresenter(ConnoteIndexPresenter::class);
            return response()->json($this->repo->parserResult($connote));
        }
        
        return view('Logistic::admin.connote.show', ['connote'=>$connote]);
    }

    /**
     * Change the status of the connote
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function statusChange(Request $request, $status, $comment="")
    {
        try{
            $data = ['comments'=>$comment];
        
            $connoteIds = array_map(function ($val){return hashids_decode($val);}, $request->get('ids',[]));
            $connotes = $this->repo->statusChange($connoteIds, $status, $data);
            
            
            return response()->json([
                'message'  => "connote status changed",
                'code'     => 204,
                ], 201);

        } catch (\Symfony\Component\HttpKernel\Exception\HttpException $e) {
            return response()->json([
                    'message' => $e->getMessage(),
                    'code'    => $e->getStatusCode(),
                    ], $e->getStatusCode());
        
        } catch (\Exception $e) {
            return response()->json([
                    'message' => $e->getMessage(),
                    'code'    => 400,
                    ], 400);
        }
    }
}
