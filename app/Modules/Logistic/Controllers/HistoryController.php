<?php

namespace App\Modules\Logistic\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Quatius\Logistic\Models\LogisticHistory;

class HistoryController extends Controller
{
    /**
     * Update the value (is connote seen?) in history_logistic.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateValue(Request $request)
    {
        $history = LogisticHistory::find(hashIds_decode($request->id));
        abort_unless($history,404, "History not found");

        $history->update(["value" => $request->value]);
        
        return response()->json([        
            'message'  => "History value is updated",
            'code'     => 204
        ], 201);
    }
}