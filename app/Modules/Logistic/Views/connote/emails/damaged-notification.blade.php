@extends('Logistic::connote.emails.layout-notification')

@section('notification-content')
	<tr>
		<td>
			<table class="center details" style="padding:1em 3em;">
				<!--A sentence/message-->
				<tr>
					<td colspan="2" style="text-align:left; ">						
						<h1>Your shipment has been damaged or lost.</h1>
					</td>
				</tr>
				<!--Content of the email-->
				<tr>
					<td colspan="2" style="text-align:left; font-size:1.1em">
						<p>
						Hi <b>{{$connote->receiver->first_name}}</b>,
						</p>
							
						<p>
						Your shipment is has been damaged or lost along the way.<br/>
						We apologise for this inconvenient.<br/>
						We will let you know its update as soon as possible.<br/>
						</p>

						<p style="margin:2em 0;">
							<strong>Tracking number: <a href='{{URL("/")}}/tracking?consignment={{$connote->consignment}}' style="text-decoration:none; color:#000">{{$connote->consignment}}</a></strong>
							<!-- <button onclick='location.href="{{URL("/")}}/tracking?consignment={{$connote->consignment}}"'>Track your package</button> -->
						</p>
						
						<p>
						Thanks for your patience and understanding.<br/>&nbsp;<br/>
						</p> 
					</td>
				</tr>							
			</table>
		</td>
	</tr>
@stop