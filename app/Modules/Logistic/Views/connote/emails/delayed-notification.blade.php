@extends('Logistic::connote.emails.layout-notification')

@section('notification-content')
	<tr>
		<td>
			<table class="center details" style="padding:1em 3em;">
				<!--A sentence/message-->
				<tr>
					<td colspan="2" style="text-align:left; ">						
						<h1>Your shipment has been delayed.</h1>
					</td>
				</tr>
				<!--Content of the email-->
				<tr>
					<td colspan="2" style="text-align:left; font-size:1.1em">
						<p>
						Hi <b>{{$connote->receiver->first_name}}</b>,
						</p>
							
						<p>
						Your shipment is has been delayed because of some unexpected congestions. We apologise for this inconvenient.
						We will deliver it as soon as possible to the destination and will let you know its update thru our tracking system.<br/>									
						</p>

						<p style="margin:2em 0;">
							<strong>Tracking number: <a href='{{URL("/")}}/tracking?consignment={{$connote->consignment}}' style="text-decoration:none; color:#000">{{$connote->consignment}}</a></strong><br/>&nbsp;<br/>
							<button onclick='location.href="{{URL("/")}}/tracking?consignment={{$connote->consignment}}"'>Track your package</button>
						</p>
						
						<p>
						Thanks for your patience and understanding.<br/>&nbsp;<br/>
						</p> 
					</td>
				</tr>							
			</table>
		</td>
	</tr>
@stop