<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
	<!--[if mso]>	
	<style type="text/css">
		body, table, td {font-family: Arial, Helvetica, sans-serif !important;}
	</style>
	<![endif]-->
    </head>
    
    <body style="margin: 0; padding: 1.5% 10%;">	

		@php
		$formDatas = get_defined_vars()['__data'];
		@endphp

		<p><pre>{{$formDatas["msg_content"]}}</pre></p>
		<p style="color:#c4c4c4">
			--------------------------------<br/>
			{{$formDatas['sender_fullname']}}<br/>
		</p>				

		<table style="width: 700px; font-size:12px; margin-top:50px;">
			<tr>
				<td colspan = "3" style="padding: 10px 5px; text-align:left;">
				<strong>Details of Connote: </strong> {{$formDatas["consignment"]}}
				</td>
			</tr>	
			<tr>
				<td colspan = "3" style="padding: 10px 5px; text-align:left; border-top: 1px solid #000000;  border-bottom: 1px solid #000000">
					<strong>Reference Number:</strong> {{$formDatas['reference']}}
				</td>
			</tr>	
			<tr>
				<td colspan = "3" style="padding: 10px 5px; border-bottom: 1px solid #000000">
					<table style="width: 100%; font-size: 12px;">
						<tr>
							<td style="width: 33%; text-align:left;">
								<strong>Created At:</strong> {{date('Y/m/d',strtotime($formDatas['created_at']))}}
							</td>            
							<td style="width: 33%; text-align:center;">
								<strong>Weight:</strong> {{round($formDatas['weight'],1)}} Kg
							</td>							
							<td style="width: 33%; text-align:right;">
								<strong>Dimensions (L*W*H):</strong> {{$formDatas['length']}} * {{$formDatas['width']}} * {{$formDatas['height']}} (cm)
							</td>
						</tr>
					</table>
				</td>
			</tr>	
			<tr>				
				<td style="padding: 10px 5px 0px 5px; width:33.33%; border-right: 1px solid #000000; vertical-align: top;">
					<table style="width: 100%; text-align:left; font-size:12px;">
						<tr><th style="text-align:left;">Sender</th></tr>
						<tr><td style="text-align:left;">&nbsp;</td></tr>
						<tr><td style="padding-left: 10px; padding-bottom: 5px;">{{$formDatas['sender_fullname']}}</td></tr>
						<tr><td style="padding-left: 10px; padding-bottom: 5px;">{{$formDatas['sender_phone_1']}}</td></tr>
						<tr>
							<td style="padding-left: 10px; padding-bottom: 5px;">
								{{$formDatas['sender_address_1']}} {{$formDatas['sender_address_2']}}<br>
								{{$formDatas['sender_city']}}<br>
								{{$formDatas['sender_state']}} {{$formDatas['sender_postcode']}}
							</td>
						</tr>
					</table>					
				</td>
				<td style="padding: 10px 5px 0px 5px; width:33.33%; border-right: 1px solid #000000; vertical-align: top;">
					<table style="width: 100%; text-align:left; font-size:12px;">
						<tr><th style="text-align:left;">Pickup</th></tr>
						<tr><td style="text-align:left;">&nbsp;</td></tr>
						<tr><td style="padding-left: 10px; padding-bottom: 5px;">{{$formDatas['pickup_fullname']}}</td></tr>
						<tr><td style="padding-left: 10px; padding-bottom: 5px;">{{$formDatas['pickup_phone_1']}}</td></tr>
						<tr>
							<td style="padding-left: 10px; padding-bottom: 5px;">
								{{$formDatas['pickup_address_1']}} {{$formDatas['pickup_address_2']}}<br>
								{{$formDatas['pickup_city']}}<br>
								{{$formDatas['pickup_state']}} {{$formDatas['pickup_postcode']}}
							</td>
						</tr>
					</table>					
				</td>
				<td style="padding: 10px 5px 0px 5px; width:33.33%; vertical-align: top;">
					<table style="width: 100%; text-align:left; font-size:12px;">
						<tr><th style="text-align:left;">Receiver</th></tr>
						<tr><td style="text-align:left;">&nbsp;</td></tr>
						<tr><td style="padding-left: 10px; padding-bottom: 5px;">{{$formDatas['receiver_fullname']}}</td></tr>
						<tr><td style="padding-left: 10px; padding-bottom: 5px;">{{$formDatas['receiver_phone_1']}}</td></tr>
						<tr><td style="padding-left: 10px; padding-bottom: 5px;">
							{{$formDatas['receiver_address_1']}} {{$formDatas['receiver_address_2']}}<br>
							{{$formDatas['receiver_city']}}<br>
							{{$formDatas['receiver_state']}} {{$formDatas['receiver_postcode']}}
						</td></tr>
						<tr><td style="padding-left: 10px; padding-bottom: 5px;">{{$formDatas['receiver_company']}}</td></tr>
						<tr><td style="padding-left: 10px; padding-bottom: 5px;">ALT: {{$formDatas['atl']?"YES":"NO"}}</td></tr>
						<tr><td style="padding-left: 10px; vertical-align: top;">Instruction: {{($formDatas['instruction'])}}</td></tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan = "3" style="text-transform:capitalize; padding: 10px 5px; text-align:left; border-top: 1px solid #000000;  border-bottom: 1px solid #000000">
					<strong>Lastest Tracking Status:</strong> {{$formDatas['status']}} </br>&nbsp;</br>&nbsp;
					

					<table style="width: 100%; font-size: 12px;">
					@php
                    $prev_arrData=["","",""];
                    $curr_arrData=["","",""];
                    @endphp

					@foreach($formDatas['histories'] as $ind => $history)	
					
						@php
						$curr_arrData[0]= $history['type'];
						$curr_arrData[1]= logistic_msg($history['comments']);
						$curr_arrData[2]= date("j F Y", strtotime($history['created_at']));
						@endphp	

						<tr>
							<td style="width: 80%; text-align:left;">
								{{$curr_arrData[0]}}, 
								<span style="font-size:0.85em; color:#c4c4c4;">{{$curr_arrData[01]}}</span>
							</td>
							<td style="width: 20%; text-align:right;">
								{{$curr_arrData[2] == $prev_arrData[2]? "":$curr_arrData[2]}}
							</td> 
						</tr>

						@php
						$prev_arrData = $curr_arrData;
						@endphp

					@endforeach
					</table>
				</td>
			</tr>
		</table>

    </body>
</html>