<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
	<!--[if mso]>	
	<style type="text/css">
		body, table, td {font-family: Arial, Helvetica, sans-serif  !important;}
		button{
			padding: 6px 12px;
			margin-bottom: 0;
			font-size: 14px;
			font-weight: 400;
			line-height: 1.42857143;
			text-align: center;
		}
	</style>
	<![endif]-->
	<style type="text/css">
		body.email-notif, .email-notif table, .email-notif td {font-family: Arial, Helvetica, sans-serif  !important;}		
		.email-notif button{
			border-radius: 3px;
			border: 1px solid #367fa9;
			background-color:#3c8dbc;
			color: #f4f4f4;

			padding: 1em 2em;

			margin-bottom: 0;
			font-size: 14px;
			font-weight: 400;
			line-height: 1.42857143;
			text-align: center;

			cursor:pointer; 
			display:inline-block;
		}

		.email-notif strong{
			color:gray;
		}

		.email-notif table.details td, .email-notif table.details th{
			padding:5px;
			vertical-align: top;
			text-align: left;
		}

		.email-notif .footer td a.menu{
			color:#d4d4d4;
			padding:0 1em;
			font-size:0.8em;
		}
		.email-notif .footer td:nth-child(n+2) a.menu{
			border-left: 1px solid #d4d4d4;
		}

		.email-notif .container {
			position: relative;
			text-align: center;
			color: #00000090;
			font-size: 1.1em;
			font-weight: bold;
			text-decoration: none;
		}
		.email-notif .container img{
			opacity: 0.5;		
			width:100%; 
			border-radius: 5px;	
		}
		.email-notif .centered {
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%, -50%);			
		}

	</style>
    </head>
    	
    <body class="email-notif" style="margin: 0; padding: 1.5% 10%; text-align: center; background-color:#333333;">

		<div>
			<a href="{{url('/')}}"><img src="{{url('/')}}/images/quatius-logo.png" style="max-width: 100%; height: auto;"></a><br/>
		</div>

		
		<div style=" display: flex; justify-content: center;">
			<table class="center details" style="align-self:center; width:700px; margin-top:30px; background-color:#f4f4f4; font-size:12px; border: 1px solid #fff; border-radius:5px;">
				@yield('notification-content')
				<tr style="background-color: #e8e8e8">
					<td>
						<table class="center details" style="padding:1em 3em;">
							<tr>
								<td colspan="4">
									<h2>More services you'll love at unbeatable prices!</h2>
								</td>
							</tr>
							<tr>
								<td style="width:25%; text-align:center; ">									
									<div class="container">
										<a class="container" href="{{url('/')}}/services"><img src="{{url('/')}}/images/freight-management-01.jpg" alt="Snow"></a>
										<div class="centered"><a class="container" href="{{url('/')}}/services">Freight Managment & Logistics Specialist</a></div>
									</div>
								</td>
								<td style="width:25%; text-align:center;">
									<div class="container">
										<a class="container" href="{{url('/')}}/services"><img src="{{url('/')}}/images/warehouse-distribution.jpg" alt="Snow"></a>
										<div class="centered"><a class="container" href="{{url('/')}}/services">Warehousing / Distribution</a></div>
									</div>									
								</td>
								<td style="width:25%; text-align:center;">
									<div class="container">
										<a class="container" href="{{url('/')}}/services"><img src="{{url('/')}}/images/consultation-services.jpg" alt="Snow"></a>
										<div class="centered"><a class="container" href="{{url('/')}}/services">Consultation Services</a></div>
									</div>									
								</td>
								<td style="width:25%; text-align:center;">
									<div class="container">
										<a class="container" href="{{url('/')}}/services"><img src="{{url('/')}}/images/b2b-logistics.jpg" alt="Snow"></a>
										<div class="centered"><a class="container" href="{{url('/')}}/services">B2B Logistics in Asia</a></div>
									</div>									
								</td>								
							</tr>
							<tr>
								<td colspan="4" style="width:100%; text-align:center;">									
									<strong>Call Us Now</strong><br/>&nbsp;<br/>
									<strong>Hotline:</strong> <a href="tel:1300899299" style="text-decoration:none; color:gray">1300 899 299</a>&nbsp;&nbsp;&nbsp;
									<strong>Tel:</strong> <a href="tel:+61387878459" style="text-decoration:none; color:gray">(+61)3 8787 8459</a>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>

		<!--Footer section-->
		<div class="footer" style="display: flex; justify-content: center; margin-top:1em;">
			<table class="center" style="align-self:center; ">
				<tr>
					<td><a class="menu " href="{{url('/')}}">Home</a></td>
					<td><a class="menu " href="{{url('/')}}/about-us">About Us</a></td>
					<td><a class="menu " href="{{url('/')}}/services">Services</a></td>
					<td><a class="menu " href="{{url('/')}}/career">Career</a></td>
					<td><a class="menu " href="{{url('/')}}/contact-us">Contact Us</a></td>
				</tr>
			</table>			
		</div>
    </body>
</html>