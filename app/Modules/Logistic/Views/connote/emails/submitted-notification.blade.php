@extends('Logistic::connote.emails.layout-notification')

@section('notification-content')
	<tr>
		<td>
			<table class="center details" style="padding:1em 3em;">
				<!--A sentence/message-->
				<tr>
					<td colspan="2" style="text-align:left; ">						
						<h1>Your consignment is successfully submitted.</h1>						
					</td>
				</tr>
				<!--Content of the email-->
				<tr>
					<td colspan="2" style="text-align:left; font-size:1.1em">
						Hi <b>{{$connote->receiver->first_name}}</b>, <br/>&nbsp;<br/>

						Thanks for your purchase with us!<br/>
						We'll let you know when it's on its way.									

					</td>
				</tr>
				<!-- <tr>
					<td  colspan="2" style="text-align:left; font-size:1.3em">
						<button onclick='location.href="{{URL("/")}}/tracking?consignment={{$connote->consignment}}"'>Track your package</button>
					</td>
				</tr>				 -->
			</table>
		</td>
	</tr>				
	<tr>
		<td style="border-top: 1px solid #d4d4d4">
			<table class="center details" style="padding:1em 3em 2em 3em;">
				<tr>
					<td colspan="2" style="text-align:left;">
						<h2>Details of consignment</h2>
					</td>
				</tr>
				<tr>
					<td colspan="2" style="text-align:left; padding-left:1em;">
						<strong>Consignment number:</strong> {{$connote->consignment}}
					</td>
				</tr>
				<tr>
					<td colspan="2" style="text-align:left; padding-left:1em;">
						<strong>Reference:</strong> {{$connote->reference}}
					</td>
				</tr>
				<tr>
					<td colspan="2" style="text-align:left; padding-left:1em;">
						<strong>Parcel Type:</strong> <span style="text-transform:capitalize;">{{$connote->parcel_type}}</span><br/>
						<strong>Dimensions L*W*H (cm):</strong> {{$connote->length}} * {{round($connote->weight,2)}} * {{$connote->height}}
					</td>
				</tr>							
				<tr >
					<td style="text-align:left; width:50%">
						<table style="min-height:110px; width:100%; border:1px dashed #d4d4d4; border-radius:3px; padding:1em;">
							<tr style="vertical-align: top;" >
								<td><strong>From:</strong></td>
								<td>
									<b>{{$connote->manifest->sender->getFullName()}}</b><br/>&nbsp;<br/>
									{{$connote->manifest->sender->city}}, {{$connote->manifest->sender->state}} {{$connote->manifest->sender->postcode}}<br/>
									{{$connote->manifest->sender->country}}<br/>
								</td>
							</tr>
						</table>												
					</td>
					<td style="text-align:left; width:50%">
						<table style="min-height:110px; width:100%; border:1px dashed #d4d4d4; border-radius:3px; padding:1em;">
							<tr style="vertical-align: top;" >
								<td><strong>Delivery To:</strong></td>
								<td>
									<b>{{$connote->receiver->getFullName()}}</b><br/>&nbsp;<br/>
									{{$connote->receiver->address_1}} {{$connote->receiver->address_2}}<br/>
									{{$connote->receiver->city}}, {{$connote->receiver->state}} {{$connote->receiver->postcode}}<br/>
									{{$connote->receiver->country}}<br/>
								</td>
							</tr>
						</table>												
					</td>
				</tr>
			</table>
		</td>
	</tr>
@stop