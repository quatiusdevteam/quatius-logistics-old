@extends('Logistic::connote.emails.layout-notification')

@section('notification-content')
	<tr>
		<td>
			<table class="center details" style="padding:1em 3em;">
				<!--A sentence/message-->
				<tr>
					<td colspan="2" style="text-align:left; ">						
						<h1>Your shipment has been delivered.</h1>						
					</td>
				</tr>
				<!--Content of the email-->
				<tr>
					<td colspan="2" style="text-align:left; font-size:1.1em;">
						Hi <b>{{$connote->receiver->first_name}}</b>, <br/>&nbsp;<br/>

						Your shipment <strong>(Tracking number: <a href='{{URL("/")}}/tracking?consignment={{$connote->consignment}}' style="text-decoration:none; color:#000">{{$connote->consignment}}</a>)</strong> has been delivered to <br/>&nbsp;<br/>
						
						<table style="min-height:110px; width:400px; border:1px dashed #d4d4d4; border-radius:3px; margin-left:80px; padding:1em;">
							<tr style="vertical-align: top;" >											
								<td>
									<b>{{$connote->receiver->getFullName()}}</b><br/>&nbsp;<br/>
									{{$connote->receiver->address_1}} {{$connote->receiver->address_2}}<br/>
									{{$connote->receiver->city}}, {{$connote->receiver->state}} {{$connote->receiver->postcode}}<br/>
									{{$connote->receiver->country}}<br/>
								</td>
							</tr>
						</table>	

						<br/>Thanks for using our service!<br/>
						We're looking forward to serving you again soon!

					</td>
				</tr>
				<tr>
					<td  colspan="2" style="text-align:left; font-size:1.3em">
						<button onclick='location.href="{{URL("/")}}/tracking?consignment={{$connote->consignment}}"'>Track your package</button>
					</td>
				</tr>				
			</table>
		</td>
	</tr>
@stop