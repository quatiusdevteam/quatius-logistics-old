@includeStyle('vendors/css/logistic.css')
@includeStyle('vendors/css/print.min.css')
@includeScript('vendors/js/print.min.js')
@includeScript('vendors/js/logistic.js')
@includeScript('vendors/js/ct-address.js')

<div class="modal fade" id="modalShowManifest" tabindex="-1" role="dialog" aria-labelledby="modalShowManifest" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="border-radius: 3px;">
    </div>
  </div>
</div>

<div class="modal fade" id="modalShowConnote" tabindex="-1" role="dialog" aria-labelledby="addNewConnote" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="border-radius: 3px;">
    </div>
  </div>
</div>

<div class="modal fade" id="modalEnquiry" tabindex="-1" role="dialog" aria-labelledby="sendEnquiry" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="border-radius: 3px;">
    </div>
  </div>
</div>

<div class="modal fade" id="modalLabelConnote" tabindex="-1" role="dialog" aria-labelledby="addNewConnote" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 3px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="selectClientLabel"><i class="fa fa-file"></i> &nbsp; Print Label : connote----</h4>
        </div>
        <div class="modal-body" style="text-align: center; background-color:#666666; height: 75vh; overflow:auto;"> 
            <img id="connote-label" src="" style="display:inline-block" class="img-responsive">
        </div>
        <div class="modal-footer">
            <a id="btn-printPDF" href="#" target="_new" class="btn btn-primary" onclick=""><i class="fa fa-print"></i> Print Now</a>
            <a id="btn-viewPDF" href="#" target="_new" class="btn btn-primary"><i class="fa fa-file-pdf-o"></i> View PDF</a>
        </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalPODConnote" tabindex="-1" role="dialog" aria-labelledby="addNewConnote" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 3px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="selectClientLabel"><i class="fa fa-file"></i> &nbsp; Proof Of Delivery (POD): connote----</h4>
        </div>
        <div class="modal-body" style="text-align: center;"> 
            <img id="connote-label" src="" width="80%"/>
        </div>
    </div>
  </div>
</div>


<section class="content">
    <div class="box box-info">        
        <div class="box-header with-border">
            <h3 class="box-title">Tracking Connotes </h3>
            <div class="box-tools">                
                <div class="dropdown" style="display: inline;">
                    <button class="btn btn-primary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                        <i class="fa fa-print"></i>&nbsp; Print  &nbsp;&nbsp;<i class="fa fa-caret-down"></i>
                    </button>
                    <ul class="dropdown-menu" style="margin-top: 0.8em; background-color:#40739EF2; left:auto; right:0; margin-right:-1px">
                        <li class="user-footer small">
                            <a id="btn-print-options" href="{{ URL::to('connote') }}/create" class="color-0" onclick="alert('print all option'); return false;">
                                <span class="fa fa-print"></span> Print All
                            </a>
                            <a id="btn-print-options" href="{{ URL::to('connote') }}/create" class="color-0" onclick="alert('print all option'); return false;">
                                <span class="fa fa-print"></span> Print Selected Connote Labels
                            </a>
                            <a id="btn-print-options" href="{{ URL::to('connote') }}/create" class="color-0" onclick="alert('print all option'); return false;">
                                <span class="fa fa-print"></span> Print Selected Manifests
                            </a>
                            <!-- <a id="btn-import-connotes" href="#" class="color-0" data-toggle="modal" data-target="#modalImportConnotes">
                                <span class="fa fa-upload"></span> Import Connotes
                            </a>                         -->
                        </li>
                    </ul>
                </div>
                &nbsp;
                <button id="btn-downloads" type="button" class="btn btn-primary btn-sm" onclick="createManifest(this)" disabled title="Please, select at least one connote to create a manifest!"><i class="fa fa-download"></i>&nbsp; Downloads</button>                
            </div>
        </div>
        <div class="box-body" style="min-height:100px">
            <div style="margin-bottom: 1em;">
                <span id="product_filter">
                    <div class="btn-group" role="group" aria-label="...">
                        <button type="button" class="btn btn-default trash-filter active" data-filter_status="" onclick="setStatusFilter(this)"><i class="fa fa-files-o" aria-hidden="true"></i>&nbsp; All</button>
                    </div>

                    <div class="btn-group" role="group" aria-label="...">
                    <!-- <button type="button" class="btn btn-default status-submitted" onclick="setStatusFilter(this)" data-filter_status="submitted"><i class="fa fa-send" aria-hidden="true"></i>&nbsp; Submitted</button>
                    <button type="button" class="btn btn-default status-picked-up" onclick="setStatusFilter(this)" data-filter_status="picked-up"><i class="fa fa-truck" aria-hidden="true"></i>&nbsp; Picked-Up</button>
                    <button type="button" class="btn btn-default status-delivered" onclick="setStatusFilter(this)" data-filter_status="delivered"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp; Delivered</button>
                    <button type="button" class="btn btn-default status-cancelled" onclick="setStatusFilter(this)" data-filter_status="cancelled"><i class="fa fa-times" aria-hidden="true"></i>&nbsp; Cancelled</button> -->

                    <button type="button" class="btn btn-default status-transit" onclick="setStatusFilter(this)" data-filter_status="transit"><i class="fa fa-truck" aria-hidden="true"></i>&nbsp; In Transit</button>
                    <button type="button" class="btn btn-default status-complete" onclick="setStatusFilter(this)" data-filter_status="complete"><i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp; Complete</button>
                    <button type="button" class="btn btn-default status-undeliverable" onclick="setStatusFilter(this)" data-filter_status="undeliverable"><i class="fa fa-times" aria-hidden="true"></i>&nbsp; Undeliverable</button>
                    </div>

                    <!-- <div class="btn-group" role="group" aria-label="...">
                    <button type="button" class="btn btn-default trash-filter" onclick="getTrashProds(this)"><i class="fa fa-trash" aria-hidden="true"></i>&nbsp; Deleted</button>
                    </div> -->

                    

                    <!-- <div class="btn-group" role="group" aria-label="...">
                    <button type="button" class="btn btn-default" onclick="$(this).toggleClass('active'); oTable.draw();" data-filter_source="pronto"><i class="fa fa-life-ring" aria-hidden="true"></i> Pronto</button>
                    <button type="button" class="btn btn-default" onclick="$(this).toggleClass('active'); oTable.draw();" data-filter_source="other"><i class="fa fa-futbol-o" aria-hidden="true"></i> Other</button>
                    </div> -->
                    
                    
                    <!-- <div class="btn-group" role="group" aria-label="...">
                    <button type="button" class="btn btn-default" onclick="setGroupFilter(this)" data-filter_group="unique"><i class="fa fa-cubes" aria-hidden="true"></i> Grouped Only</button>
                    </div>
                    <div class="btn-group" role="group" aria-label="...">
                    
                    </div> -->
                </span> 
            </div> 


            {!!Form::open()->attributes(['onsubmit'=>'createManifest(this); return false;'])->id('frm-selected-connotes')!!}

                <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>Status</th>                        
                            <th>Consignment</th>
                            <th>Manifest</th>
                            <th>Courier</th>                        
                            <th>Customer Ref.</th>                            
                            <th>Pickup Location </th>
                            <th>Receiver</th>
                            <th>Price</th>
                            <th>Created At</th>  
                            <th>Updated At</th>  
                            <th>Actions</th>   
                        </tr>
                    </thead>
                    <tbody>                              
                    </tbody>
                </table>

            {!! Form::close() !!}	

        </div>
    </div>
</section>

@script
<style>
    th, td{
        font-size: 1em;
    }

    input[type=file]::file-selector-button {
        /* border-left: 1px solid rgb(209,211,226); */
        border: none;
        margin-top: -.4em;
        margin-right: -.8em;
        padding: .4em 1em;
        border-radius: 0 5px 5px 0px;        
        transition: 1s;
        float: right;
        z-index: 2000;
    }
    
</style>



<script type="text/javascript">

    var dataSets = []; 
    statusFilter = "";
    var myTable;

    var groupPending = <?php echo json_encode(config('quatius.logistic.status.group.pending',[])); ?>;
    var groupTransit = <?php echo json_encode(config('quatius.logistic.status.group.transit',[])); ?>;
    var groupComplete = <?php echo json_encode(config('quatius.logistic.status.group.complete',[])); ?>;
    var groupUndeliverable = <?php echo json_encode(config('quatius.logistic.status.group.undeliverable',[])); ?>;

    $(document).ready(function() {
        // $('#example tbody').on( 'click', 'tr', function () {
            
        //     $(this).toggleClass("selected").siblings(".selected").removeClass("selected");
        //     var d = $('#example').DataTable().row( this ).data();
        //     $('#modalShowConnote').modal('show').find('.modal-content').load('{{URL::to("connote")}}' + '/' + d.id);
        // });

        myTable = $('#example').DataTable({

            // dom: 'Bfrtip',
            // buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ],

            "columns": [                
                { "data": "status" },                
                { "data": "consignment" },
                { "data": "manifest_no" },
                { "data": "courier_id"},                 
                { "data": "reference" },               
                { "data": "pickup_address_id"}, 
                { "data": "weight"}, 
                { "data": "weight"}, 
                { "data": "created_at" , "className":"created-at"},
                { "data": "status_at" },
                { "data": "id" },
                // { "data": "receiver"},
                // { "data": "remark" },
            ],
            "order": [[ 9, "desc" ]],

            serverSide: true,
            processing: true,

            responsive: true,

            ajax: {
                url: "{{ URL::to('connote/submitted') }}?status="+statusFilter,

                "dataType": "json",
                "type": "GET",
                "data":{
                    extra_fields : [
                        // "length", "width", "height", 
                        // "weight",
                        "parcel_type", "price", "currency", 
                        "id", "status_at", "manifest_id",
                        "is_printed",                        
                        "courier_id", 
                        "sender_address_id", "pickup_address_id"
                    ],
                    // hash:{
                    //     from: "manifest_id",
                    //     to: "mft_id"
                    // }
                }
            },            

            "pageLength": 10,
            "columnDefs": [
                // {"className": "align-top text-left", "targets": "_all"},
                {"className": "align-top text-center", "targets": [1,2,3,4]},
                {"className": "align-top text-right", "targets": [7]},
                // {"className": "align-top text-left", "targets": [6]},
                {
                    "render": function ( data, type, row ) {          
                        if ((row.remark != "") && (row.remark != null) ) {
                            return '<span class="status-warning"><i class="fa fa-warning" title="'+row.remark+'"></i>&nbsp; warning!!!</span>';
                        }else{
                            return data;
                        }
                    },
                    "targets": 0
                },
                {
                    "render": function ( data, type, row ) {                    
                        return "<a target='_blank' onclick=\"$('#modalShowConnote').modal('show').find('.modal-content').load('{{ URL::to('connote') }}/"+ row.id +"');\"  title='View a Connote'>"+ data +"";
                    },
                    "targets": 1
                },
                {
                    "render": function ( data, type, row ) {                    
                        if ((data != "") && (data != null)) {
                            return "<a target='_self' class='btn-link' onclick=\"$('#modalShowManifest').modal('show').find('.modal-content').load('{{ URL::to('manifest') }}/"+ row.manifest_id +"');\"><i class='fa fa-files-o'></i>&nbsp; "+ data +"</a>";
                        }else{
                            return "";
                        }
                    },
                    "targets": 2
                },
                {
                    "render": function ( data, type, row ) {                              
                        return $('.ct-addresses [data-id="'+data+'"] #ct-first-name').html() +" "+ $('.ct-addresses [data-id="'+data+'"] #ct-last-name').html();
                    },                    
                    "targets": 5
                },                
                {
                    // "className":"align-top text-center parcel-dimension",
                    "render": function ( data, type, row ) {
                        // return  row.parcel_type +' ('+ row.length +' <small>x</small> '+ row.width +' <small>x</small> '+ row.height +')  &nbsp; , &nbsp; '+ parseFloat(data).toFixed( 2 ) +"Kg";
                        return  row.receiver;
                    },
                    "targets": 6
                },
                {
                    "className":"align-top text-center",
                    "render": function ( data, type, row ) {
                        return moment.utc(data, 'YYYY-MM-DD hh:mm:ss').local().format('YYYY/MM/DD  hh:mm A');                        
                    },
                    "targets": [8,9]
                },
                {
                    "render": function ( data, type, row ) {
                        let str = "";
                        str += "<a target='_blank' class='btn btn-default btn-sm btn-connote-action btn-edit-connote'  onclick=\"$('#modalShowConnote').modal('show').find('.modal-content').load('{{ URL::to('connote') }}/"+ row.id +"');\"  title='View a Connote'><i class='fa fa-eye'></i> View</a>"
                        str += "&nbsp; <a target='_blank' class='btn btn-default btn-sm btn-connote-action' onclick=\"clickLabel('"+ row.id +"', '"+ row.consignment +"')\" title='Print Label'><i class='fa fa-print'></i> Label</a>"
                        str += "&nbsp; <a target='_blank' class='btn btn-default btn-sm btn-connote-action btn-send-enquiry'  onclick=\"$('#modalEnquiry').modal('show').find('.modal-content').load('{{ URL::to('connote') }}/"+ row.id +"/enquiry');\"  title='Send Enquiry'><i class='fa fa-envelope-o'></i> Enquiry</a>"
                        // str += "&nbsp; <a target='_blank' class='btn btn-danger btn-sm btn-connote-action' href='{{ URL::to('connote') }}/"+ row.id +"/testview''><i class='fa fa-cube'></i>&nbsp; testView</a>";


                        // if (row.status == "delivered")
                        //     str += "&nbsp; <a target='_blank'  onclick=\"clickPOD('"+ row.id +"', '"+ row.consignment +"')\"  title='Proof Of Delivery'><i class='fa fa-picture-o'></i> POD</a>"
                        //     // str += "&nbsp;&nbsp;&nbsp; <a target='_blank'  onclick=\"$('#modalShowConnote').modal('show').find('.modal-content').load('{{ URL::to('connote') }}/"+ row.id +"/pod');\"  title='Proof Of Delivery'><i class='fa fa-picture-o'></i> POD</a>"
                        
                        return str;
                    },
                    "targets": 10
                },
            ],
            
            "createdRow": function ( row, data, index ) {
                $(row).attr("data-id", data.id);
                
                if (data.status != null){                    
                    if ( groupPending.includes(data.status)) {
                        $('td', row).eq(0).prepend('<i class="fa fa-send"></i> &nbsp;');
                        $('td', row).eq(0).addClass('status-submitted text-capitalize');
                    }else if ( groupTransit.includes(data.status)) {
                        $('td', row).eq(0).prepend('<i class="fa fa-truck"></i> &nbsp;');
                        $('td', row).eq(0).addClass('status-picked-up text-capitalize');
                    }else if ( groupComplete.includes(data.status)) {
                        $('td', row).eq(0).prepend('<i class="fa fa-check-circle"></i> &nbsp;');
                        $('td', row).eq(0).addClass('status-delivered text-capitalize');
                    }else if ( groupUndeliverable.includes(data.status)) {
                        $('td', row).eq(0).prepend('<i class="fa fa-times"></i> &nbsp;');
                        $('td', row).eq(0).addClass('status-cancelled text-capitalize');                    
                    }
                }
            }
        });

        myTable.on( 'xhr', function () {
            dataSets = myTable.ajax.json();
            // console.log(dataSets);
        });
  
    });

    function setStatusFilter(itm){
        $('button[data-filter_status]').removeClass('active');        
        $(itm).addClass('active');
        statusFilter = $(itm).attr('data-filter_status');        
        myTable.ajax.url( "{{ URL::to('connote/submitted') }}?status="+statusFilter).load();
    }

    //--------------------------
    function clickLabel(id, consig){
        $('#modalLabelConnote').modal('show').find('.modal-content img#connote-label').prop('src', "{{ URL::to('connote') }}/"+ id +"/label");
        $('#modalLabelConnote #selectClientLabel').html('<i class="fa fa-file"></i> &nbsp; Print Label : connote-'+ consig); 
        $('#modalLabelConnote #btn-viewPDF').attr('href', "{{ URL::to('connote') }}/"+ id +"/label-pdf");
        $('#modalLabelConnote #btn-printPDF').attr('onclick', "printPDF('{{ URL::to('connote') }}/"+ id +"/label-pdf'); return false;");
    }

    //--------------------------
    function clickPOD(id, consig){
        $('#modalPODConnote').modal('show').find('.modal-content img#connote-label').prop('src', "{{ URL::to('connote') }}/"+ id +"/pod");
        $('#modalPODConnote #selectClientLabel').html('<i class="fa fa-file"></i> &nbsp; Proof Of Delivery (POD) : connote-'+ consig); 
    }

</script>
@endscript

