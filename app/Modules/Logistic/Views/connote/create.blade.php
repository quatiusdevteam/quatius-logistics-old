{!!Form::vertical_open()
    ->attributes(['autocomplete'=>'off','onsubmit'=>'saveConnote(this); return false;'])
    ->id('create-connote')
    ->method('POST')
    ->action(URL::to('connote'))!!}
    
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="selectClientLabel"><i class="fa fa-file"></i> &nbsp; Add Connote</h4>
</div>
<div class="modal-body">    
        @include('Logistic::connote.partials.entry')
</div>
<div class="modal-footer">
    <button id="btn-save-connote" type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp; Save Now</button>
    <button id="btn-reset"    type="reset" class="btn btn-secondary" ><i class="fa fa-refresh"></i>&nbsp; Reset</button>
</div>

{!! Form::close() !!}

<script type="text/javascript">
    setTimeout(() => {
        $('#create-connote input').attr('autocomplete','none');
    }, 2000);
</script>