<html>
<head>
    <style>
        html{
            margin: 0px;
        }
    </style>
</head>
<body style="margin: 0px; font-family: Arial, Helvetica, sans-serif;">
<table style="width: 395px; font-size:12px;">
<tr><td colspan = "2" style="text-align:left; border-bottom: 2px solid #000000">
    <table style="width: 100%"><tr>
            <td style="width: 50%">
                <img style="height:30px;" src="data:image/png;base64,{{base64_encode(file_get_contents(config('quatius.logistic.logo-print','')))}}"/></td>
            <td style="width: 50% ;text-align:right;">
                @if($connote->courier->hasLogo())
                <img style="height:20px;" src="data:image/png;base64,{{$connote->courier->getLogoBase64()}}"/>
                @endif
            </td>
        </tr>
    </table>
</td></tr>
<tr><td colspan = "2" style="padding: 5px 0px; text-align:center; border-bottom: 2px solid #000000">
    <img style="height:40px;" src="data:image/png;base64,{{base64_encode(file_get_contents(getBarcode($connote->courier_consignment)))}}"/>
    <table style="width: 100%; padding-top: 5px; font-size:14px; text-align:center;"><tr><td>{{$connote->courier_consignment}}</td></tr></table>
</td></tr>
<tr>
    <td style="padding: 10px 5px 0px 5px; width:60%; border-right: 2px solid #000000; vertical-align: top;">
        <table style="width: 100%; text-align:left; font-size:12px;">
            <tr><th style="text-align:left;">Receiver</th></tr>
            <tr><td style="padding-left: 10px; padding-bottom: 5px;">{{$connote->receiver->getFullname()}}</td></tr>
            <tr><th style="text-align:left;">Phone</th></tr>
            <tr><td style="padding-left: 10px; padding-bottom: 5px;">{{$connote->receiver->phone_1}}</td></tr>
            <tr><th style="text-align:left;">Address</th></tr>
            <tr><td style="padding-left: 10px; padding-bottom: 5px;">{{$connote->receiver->address_1}} {{$connote->receiver->address_2}}<br>
            {{$connote->receiver->city}}<br>
            {{$connote->receiver->state}} {{$connote->receiver->postcode}}</td></tr>
            <tr><th style="text-align:left;">Company</th></tr>
            <tr><td style="padding-left: 10px; padding-bottom: 5px;">{{$connote->receiver->company}}</td></tr>
            <tr><th style="text-align:left;">ATL</th></tr>
            <tr><td style="padding-left: 10px; padding-bottom: 5px;">{{$connote->atl?"YES":"NO"}}</td></tr>
            <tr><th style="text-align:left;">Instruction</th></tr>
            <tr><td style="padding-left: 10px; vertical-align: top;">{{($connote->instruction)}}</td></tr>
        </table>
    </td>
    <td style="width: 40%; vertical-align: top;">
       <table style="width:100%; font-size: 12px;">
            <tr>
                <td style="padding: 10px 5px;">
                    {{$connote->receiver->city}}
                </td>
            </tr>
            <tr>
                <td style="padding: 10px 5px; border-top: 2px solid #000000">
                    {{$connote->receiver->postcode}}
                </td>
            </tr>
            <tr>
                <td style="vertical-align: middle; font-size:10px; padding: 0px 5px; border-top: 2px solid #000000;height: 260px;">
                {{$connote->sender->first_name}} {{$connote->sender->last_name}}<br>
                {{$connote->sender->address_1}}  {{$connote->sender->address_2}}<br>
                {{$connote->sender->city}}<br>
                {{$connote->sender->state}} {{$connote->sender->postcode}}<br><br>

                {{$connote->sender->phone_1}}<br><br>
                </td>
            </tr>
       </table>
    </td>
</tr>
<tr><td colspan = "2" style="padding: 10px 5px; text-align:left; border-top: 2px solid #000000;  border-bottom: 2px solid #000000">
    <strong>Reference Number:</strong> {{$connote->reference}}
</td></tr>
<tr><td colspan = "2" style="padding: 10px 5px;">
    <table style="width: 100%; font-size: 12px;">
        <tr>
            <td style="width: 33%; text-align:left;">
                {{round($connote->weight,1)}}Kg
            </td>
            <td style="width: 33%; text-align:center;">
                {{date('Y/m/d',strtotime($connote->submitted_at))}}
            </td>            
            <td style="width: 33%; text-align:center;">
                {{$connote->length}}*{{$connote->width}}*{{$connote->height}}(cm)
            </td>
        </tr>
    </table>
</td></tr>
</table>
</body>
</html>