{!!Form::vertical_open()
        ->attributes(['onsubmit'=>'saveConnote(this);'])
        ->id('edit-connote')
        ->method('POST')
        ->action(URL::to('connote/'.$connote->getRouteKey()))!!}
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="selectClientLabel"><i class="fa fa-file"></i> &nbsp; Edit Connote</h4>
</div>
<div class="modal-body">    
    @if($connote->remark)
    <div class="alert alert-warning" role="alert"><span class="glyphicon glyphicon-exclamation-sign"></span> {{$connote->transform()["remark"]}}</div>
    @endif
        @include('Logistic::connote.partials.entry')
        <input type="hidden" name="_method" value="patch"/>
</div>
<div class="modal-footer">
   
    <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Update</button>
    <!-- <button id="btn-save-connote" type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp; Update</button> -->
    <button id="btn-reset" type="button" class="btn btn-secondary" data-dismiss="modal" ><i class="fa fa-times"></i>&nbsp; Cancel</button>
</div>
{!! Form::close() !!}

<script type="text/javascript">
   
$(document).ready(function() {
    setTimeout(() => {
        $('#edit-connote input').attr('autocomplete','none');
    }, 2000);
    
    let strCSP = toCapitalizeStr($('#city').val())+", "+$('#state').val()+" "+$('#postcode').val()
    $('#city_state_code').val(strCSP);
    
    setTimeout(() => {
        // if($('#city').parent(".form-group").hasClass('has-error') || $('#state').parent(".form-group").hasClass('has-error') || $('#postcode').parent(".form-group").hasClass('has-error')){
        // $('#city_state_code').focus();
        $('#city_state_code').select();
        $('#city_state_code').blur();   //to check to address
        // }
    }, 600);
    // $('#modalEditConnote #state').prop('value', $('#modalEditConnote #state').val() ).trigger('change');

});

</script>
