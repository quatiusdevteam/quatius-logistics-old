<section class="content-header">
        <h1>
        <i class="fa fa-file-text-o"></i> Connotes <small> Manage Connotes</small>
        </h1>
        <ol class="breadcrumb">
    <li><a href="http://worklocal/quatius/quatius-old/public"><i class="fa fa-dashboard"></i> Home </a></li>
    <li class="active">Users</li>
</ol>
</section>


<section class="content">

    <div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">Connotes</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-primary btn-sm" id="btn-new-user"><i class="fa fa-plus-circle"></i> New </button>
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body" style="min-height:100px">
        <div class="row">
            <div id="logistic-content" class="col-xs-12">
                
            </div>
        </div>
    </div>
    <div class="box-footer">
        &nbsp;
    </div>
    </div>
</section>


@script

<script type="text/Javascript">

    @if(auth()->user())
        $.ajaxSetup({ 
        crossDomain: true,
            headers: { 'Authorization': 'Basic {{base64_encode(auth()->user()->email.":".auth()->user()->api_token)}}'} 
        });
    @endif
        $(document).ready(function() {
            $("#logistic-content").load("{{url('connote')}}");
        });
</script>
@endscript