@includeStyle('vendors/css/logistic.css')
@includeStyle('vendors/css/ct-address.css')
@includeScript('vendors/js/ct-address.js')
@includeStyle('vendors/css/print.min.css')
@includeScript('vendors/js/print.min.js')
@includeScript('vendors/js/logistic.js')

@includeStyle('vendors/css/jquery-ui.css')

<div class="modal fade" id="modalShowManifest" tabindex="-1" role="dialog" aria-labelledby="modalShowManifest" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="border-radius: 3px;">
    </div>
  </div>
</div>

<div class="modal fade" id="modalAddConnote" tabindex="-1" role="dialog" aria-labelledby="addNewConnote" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="border-radius: 3px;">
    </div>
  </div>
</div>

<!-- <div class="modal fade" id="modalEditConnote" tabindex="-1" role="dialog" aria-labelledby="addNewConnote" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="border-radius: 3px;">
    </div>
  </div>
</div> -->

<div class="modal fade" id="modalShowConnote" tabindex="-1" role="dialog" aria-labelledby="addNewConnote" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="border-radius: 3px;">
    </div>
  </div>
</div>

<div class="modal fade" id="modalEnquiry" tabindex="-1" role="dialog" aria-labelledby="sendEnquiry" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="border-radius: 3px;">
    </div>
  </div>
</div>


<div class="modal fade" id="modalImportConnotes" tabindex="-1" role="dialog" aria-labelledby="uploadConnotesCSV" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="border-radius: 3px;">
        {!!Form::open()->attributes(['onsubmit'=>'uploadConnotes(this); return false;'])->id('frm-import-connotes')!!}

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="selectClientLabel"><i class="fa fa-files-o"></i> &nbsp; Import Connote(s) From XLS File</h4>
        </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="form-group required sender-info" data-toggle="modal" data-target="#selectAddress">
                            <label for="sender" class="control-label"><i class="fa fa-user"></i>&nbsp; Sender<sup>*</sup></label>
                            <a class="btn" style="float: right;">Change</a>
                            <div class="small">&nbsp;</div>
                            <div class="address-area"></div>
                            {!! Form::hidden('sender_address_id')->id('sender_address_id')-> required('required')!!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="form-group required pickup-info" data-toggle="modal" data-target="#selectAddress">
                            <label for="sender" class="control-label"><i class="fa fa-map-marker"></i>&nbsp; Pick-up<sup>*</sup></label>
                            <a class="btn" style="float: right;">Change</a>
                            <div class="small">&nbsp;</div>
                            <div class="address-area"></div>
                            {!! Form::hidden('pickup_address_id') ->id('pickup_address_id')-> required('required')!!}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <div class="form-group required pickup-info" data-toggle="modal" data-target="#selectAddress" style="min-height:172px;">
                            <label for="sender" class="control-label"><i class="fa fa-truck"></i>&nbsp; Courier<sup>*</sup></label><br/><br/>                
                            {!! Form::select('courier_id')->label('')->options(app('courier')->whereStatus(2)->get()->pluck('label','hashId'))
                            ->placeholder('Select courier')-> required('required')!!}
                        </div>        
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="control-label" style="float: left; padding-left: 0;" for="customFile">Upload XLS file:</label>
                            &nbsp;
                            <div class="" style="display: inline-block;">
                                <input type="file" class="form-control" id="customFile" name="import_file" style="width: 400px;" />
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        <div class="modal-footer">
            <span style="float:left;"><a href="{{url('connote/template.xls')}}"><i class="fa fa-file-excel-o"></i>&nbsp; Download XLS template</a></span>
            <button id="btn-upload-connotes" type="submit" class="btn btn-primary"><i class="fa fa-upload"></i>&nbsp; Upload</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>

        {!! Form::close() !!}	

    </div>
  </div>
</div>


<!-- Sender & Pickup -->
<div class="modal fade" id="modalSenderPickup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="border-radius: 3px;">

            {!!Form::horizontal_open()->attributes(['onsubmit'=>'addNewConnote(this); return false;'])->class('form-inline')->id('frm-sender-pickup')!!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="fa fa-cubes"></i> &nbsp; Create Manifest</h4>
                </div>
                <div class="modal-body">
                    <!-- <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group required sender-info" data-toggle="modal" data-target="#selectAddress">
                                <label for="sender" class="control-label"><i class="fa fa-user"></i>&nbsp; Sender<sup>*</sup></label>
                                <a class="btn" style="float: right;">Change</a>
                                <div class="small">&nbsp;</div>
                                <div class="address-area"></div>
                                <input type="hidden" id="sender_address_id" name="sender_address_id" value="">
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group required pickup-info" data-toggle="modal" data-target="#selectAddress">
                                <label for="sender" class="control-label"><i class="fa fa-map-marker"></i>&nbsp; Pick-up<sup>*</sup></label>
                                <a class="btn" style="float: right;">Change</a>
                                <div class="small">&nbsp;</div>
                                <div class="address-area"></div>
                                <input type="hidden" id="pickup_address_id" name="pickup_address_id" value="">
                            </div>
                        </div>
                    </div>
                    <div class="row"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">&nbsp;</div></div> -->
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <table id="tbl-manifest-connotes" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Ref.</th>  
                                        <th>Parcel Type</th>
                                        <th>Receiver</th>
                                        <th>Receiver's Address</th>
                                        <th>Created At</th>  
                                    </tr>
                                </thead>
                                <tbody>                              
                                </tbody> 
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <!-- <button id="btn-submit-manifest" type="submit" class="btn btn-primary"><i class="fa fa-send"></i>&nbsp; Send Manifest</button> -->
                </div>

            {!! Form::close() !!}	

        </div>
    </div>
</div>

<div id="selectAddress" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content"  style="border-radius: 3px;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Select Address</h4>
      </div>
      <div class="modal-body">
        <div class="list-address-area" style="max-height: 400px; overflow:auto">
            
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="modalLabelConnote" tabindex="-1" role="dialog" aria-labelledby="addNewConnote" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 3px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="selectClientLabel"><i class="fa fa-file"></i> &nbsp; Print Label : connote----</h4>
        </div>
        <div class="modal-body" style="text-align: center; background-color:#666666; height: 75vh; overflow:auto;"> 
            <img id="connote-label" src="" style="display:inline-block" class="img-responsive">
        </div>
        <div class="modal-footer">
            <a id="btn-printPDF" href="#" target="_new" class="btn btn-primary"><i class="fa fa-print"></i> Print Now</a>
            <a id="btn-viewPDF" href="#" target="_new" class="btn btn-primary" onclick="updatePrintStatus(viewLabelRow);"><i class="fa fa-file-pdf-o"></i> View PDF</a>
        </div>
    </div>
  </div>
</div>

<section class="content">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Create Connotes</h3>
            <div class="box-tools">
                <span style="float:left;">
                    <a class="btn" href="{{url('connote/template.xls')}}"><i class="fa fa-file-excel-o"></i>&nbsp; Download Template</a>
                </span>
                &nbsp;
                <div class="dropdown" style="display: inline;">
                    <button class="btn btn-primary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-plus-circle"></i>&nbsp; New Connote  &nbsp;&nbsp;<i class="fa fa-caret-down"></i>
                    </button>
                    <ul class="dropdown-menu" style="margin-top: 0.8em; background-color:#40739EF2;">
                        <li class="user-footer small">
                            <a id="btn-new-connote" href="{{ URL::to('connote') }}/create" class="color-0" onclick="$('#modalAddConnote').modal('show').find('.modal-content').load($('#btn-new-connote').prop('href'),()=>{$('#modalAddConnote').trigger('loaded.bs.modal')}); return false;">
                                <span class="fa fa-file-o"></span> New Connote
                            </a>
                            <a id="btn-import-connotes" href="#" class="color-0" data-toggle="modal" data-target="#modalImportConnotes">
                                <span class="fa fa-upload"></span> Import Connotes
                            </a>                        
                        </li>
                    </ul>
                </div>
                &nbsp;
                <div class="dropdown" style="display: inline;">
                    <button class="btn btn-primary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-print"></i>&nbsp; Print  &nbsp;&nbsp;<i class="fa fa-caret-down"></i>
                    </button>
                    <ul class="dropdown-menu" style="margin-top: 0.8em; background-color:#40739EF2;">
                        <li class="user-footer small">
                            <a id="btn-print-options" href="{{ URL::to('connote') }}/create" class="color-0" onclick="alert('print all option'); return false;">
                                <span class="fa fa-print"></span> Print All Unprinted
                            </a>
                            <a id="btn-download-all" href="#" class="color-0" data-toggle="modal" data-target="#modalImportConnotes">
                                <span class="fa fa-download"></span> Download All
                            </a>                        
                        </li>
                    </ul>
                </div>
                &nbsp;
                <button id="btn-create-manifest" type="button" class="btn btn-primary btn-sm" onclick="createManifest(this)" disabled title="Please, select at least one connote to create a manifest!"><i class="fa fa-send"></i>&nbsp; Create Manifest</button>
            </div>
        </div>
        <div class="box-body" style="min-height:100px">
            @include('Logistic::connote.partials.step-timeline')
            
            <!-- Datatables -->            
            {!!Form::open()->attributes(['onsubmit'=>'createManifest(this); return false;'])->id('frm-selected-connotes')!!}
                <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th>                            
                                <input type='checkbox' class='select-all-connotes' name='select-all-connotes' onchange='checkItAll(this);'>
                            </th>  
                            <th>Status</th>                        
                            <th>Consignment</th>
                            <th>Manifest</th>
                            <th>Customer Ref.</th>
                            <th>Sender</th>
                            <th>Pick-up</th>
                            <th>Receiver</th>                            
                            <th>Parcel(L x W x H), Weight</th>
                            <!-- <th>Created By</th>   -->
                            <!-- <th>Created At</th>   -->
                            <th>Updated At</th>  
                            <th>Printed</th>   
                            <th>Actions</th>   
                        </tr>
                    </thead>
                    <tbody>                              
                    </tbody>
                </table>
            {!! Form::close() !!}	

        </div>
    </div>
</section>



@includeScript('vendors/js/sweetalert2-11.js')
        
@script

<style>
    
    th, td{
        font-size: 1em;
    }

    input[type=file]::file-selector-button {
        /* border-left: 1px solid rgb(209,211,226); */
        border: none;
        margin-top: -.4em;
        margin-right: -.8em;
        padding: .4em 1em;
        border-radius: 0 5px 5px 0px;
        /* background-color: #a29bfe;
        background-color: #029FDA; */
        transition: 1s;
        float: right;
        z-index: 2000;
    }

    @media screen and (min-width: 798px) {
        #modalAddConnote .modal-lg{
            width: 90%;
        }
    }
    
</style>
@php
    $courier = config('quatius.logistic.default_courier','')?app('courier')->findWhere(['type'=>config('quatius.logistic.default_courier',''), "status"=>2])->first():null;
@endphp

<script type="text/javascript">
    var viewLabelRow = null;
    var myTable;

    var groupPending = <?php echo json_encode(config('quatius.logistic.status.group.pending',[])); ?>;
    var groupTransit = <?php echo json_encode(config('quatius.logistic.status.group.transit',[])); ?>;
    var groupComplete = <?php echo json_encode(config('quatius.logistic.status.group.complete',[])); ?>;
    var groupUndeliverable = <?php echo json_encode(config('quatius.logistic.status.group.undeliverable',[])); ?>;

    $(document).ready(function() {

        
        // $('#modalSenderPickup').modal('show');
        

        myTable = $('#example').DataTable({
          
            "columns": [                
                { "data": "id" },
                { "data": "status" },                
                { "data": "consignment" },
                { "data": "manifest_no" },
                { "data": "reference" },
                { "data": "sender_address_id" },                
                { "data": "pickup_address_id" }, 
                { "data": "receiver" },                 
                { "data": "weight", "className":"text-capitalize text-center"},                
                { "data": "updated_at" , "className":"created-at"},          
                { "data": "is_printed"},
                { "data": "consignment" },
            ],
            "order": [[ 9, "desc" ]],

            processing: true,

            ajax: {
                url: "{{ URL::to('connote-create') }}",
                "dataType": "json",
                "type": "GET",
                "data":{
                    extra_fields : [
                        "length", "width", "height", "parcel_type", "price", "currency", 
                        "remark", "created_at", "manifest_id"
                    ],
                },
                "dataSrc": function ( json ) {
                    let results = json.data;    //console.log(results);
                    let init_step = 1;
                    if (results.length > 0){
                        init_step = 2;
                        $.each(results, function(i, item) {                            
                            if (item.status == "{{config('quatius.logistic.connote.create-status','ready to print')}}"){
                                init_step = (init_step < 2 ? 2 : init_step );
                            }
                        });
                    }
                    activate_steps(init_step);

                    return results;
                }  
            },
            // "userLength": 10,

            "pageLength": 10,
            "columnDefs": [
                {
                    "render": function ( data, type, row ) {
                        if ((row.is_printed == 1) && (row.status == null || row.status == "" || row.manifest_id == null) && (row.remark == "" || row.remark == null)) {
                            return "<input type='checkbox' class='select-connotes' name='select-connotes[]' value='"+ data +"' onchange='checkIt(this);'>";
                        }else{
                            return "";
                        }
                    },
                    "orderable": false,
                    "targets": 0
                },
                {
                    "width": 110,
                    "render": function ( data, type, row ) {          
                        if ((row.remark != "") && (row.remark != null) ) {
                            let st =  '<span class="status-warning text-capitalize">';
                                st +=     '<span><i class="fa fa-warning"></i>&nbsp; warning </span>';
                                st +=     '<span id="tooltip-span">';
                                st +=          '<strong>WARNING</strong>: '+row.remark;
                                st +=          '<br/><i class="fa fa-arrow-circle-right"></i>&nbsp; <strong>ACTION</strong>: Click on "Edit"';
                                st +=     '</span>';
                                st += '</span>';
                            
                            return st;
                        }/*else if((row.remark == "" && data == null) || row.manifest_id == null){
                            return '<span class="status-created text-capitalize"><i class="fa fa-file-o" title="Connote is created for submitting a manifest"></i>&nbsp; created </span>';
                        }else */
                        
                        if( groupPending.includes(row.status)) {
                            if(row.status == "booking pending"){
                                return '<span class="status-booking-pending text-capitalize"><i class="fa fa-clock-o"></i>&nbsp; '+row.status +' </span>';
                            }else if(row.status == "ready to print"){
                                if (row.is_printed == 1)
                                    return '<span class="status-ready-to-submit text-capitalize"><i class="fa fa-send"></i>&nbsp; Ready To Submit </span>';
                                else
                                    return '<span class="status-ready-to-print text-capitalize"><i class="fa fa-print"></i>&nbsp; '+row.status +' </span>';
                            }else
                                return '<span class="status-submitted text-capitalize"><i class="fa fa-send"></i>&nbsp; '+row.status +' </span>';
                        }else{
                            return data;
                        }
                    },
                    "targets": 1
                },
                {
                    "render": function ( data, type, row ) {                    
                        if ((data != "") && (data != null)) {
                            return "<a target='_self' class='btn-link' onclick=\"$('#modalShowManifest').modal('show').find('.modal-content').load('{{ URL::to('manifest') }}/"+ row.manifest_id +"',()=>{$('#modalShowManifest').trigger('loaded.bs.modal')});\"><i class='fa fa-files-o'></i>&nbsp; "+ data +"</a>";
                        }else{
                            return "";
                        }
                    },
                    "targets": 3
                }, 
                {
                    "render": function ( data, type, row ) {                              
                        return $('.ct-addresses [data-id="'+data+'"] #ct-first-name').html() +" <span style='color:#a4a4a4; font-size:0.8em'>("+ $('.ct-addresses [data-id="'+data+'"] #ct-state').html()+" "+ $('.ct-addresses [data-id="'+data+'"] #ct-postcode').html()+")</small>";
                    },                    
                    "targets": 5
                },               
                {
                    "render": function ( data, type, row ) {                              
                        return $('.ct-addresses [data-id="'+data+'"] #ct-first-name').html() +" <span style='color:#a4a4a4; font-size:0.8em'>("+ $('.ct-addresses [data-id="'+data+'"] #ct-city').html()+", "+ $('.ct-addresses [data-id="'+data+'"] #ct-state').html()+" "+ $('.ct-addresses [data-id="'+data+'"] #ct-postcode').html()+")</small>";
                    },                    
                    "targets": 6
                },               
                {
                    "className":"align-top text-center parcel-dimension",
                    "render": function ( data, type, row ) {
                        return  row.parcel_type +' ('+ row.length +' <small>x</small> '+ row.width +' <small>x</small> '+ row.height +'), '+ parseFloat(data).toFixed( 1 );
                    },
                    "targets": 8
                },
                {
                    "className":"align-top text-center parcel-dimension",
                    "render": function ( data, type, row ) {
                        // var str = data;
                        // var str = moment.utc(data, 'YYYY-MM-DD hh:mm:ss').local().format('YYYY/MM/DD  hh:mm A');

                        // var startTime = moment(row.created_at, 'YYYY-MM-DD hh:mm:ss');
                        // var endTime = moment(row.updated_at, 'YYYY-MM-DD hh:mm:ss');
                        // var diff_mn = endTime.diff(startTime, 'hours');
                        
                        // return data + (diff_mn/60 >1 ? ", &nbsp; <span class='small text-gray'> created "+Math.floor(diff_mn/24)+" days before last update </span>" : "");
                        return moment.utc(data, 'YYYY-MM-DD hh:mm:ss').local().format('YYYY/MM/DD  hh:mm A');
                    },
                    "targets": 9
                },  
                {
                    "render": function ( data, type, row ) {
                       
                        let str = ((row.is_printed == 1)?"<i class='fa fa-check text-success'></i>":""); 
                        return  str;                        
                    },
                    "targets": 10
                },              
                {
                    "render": function ( data, type, row ) {
                        let str = "";                       
                        if (! (row.remark != "" && row.remark != null) ) { 
                            str += "<a target='_blank' class='btn "+ ((row.is_printed == 1)?"btn-default":"btn-success") +" btn-sm btn-connote-action' onclick=\"clickLabel('"+ row.id +"', '"+ row.consignment +"', this);  viewLabelRow=this; \"  title='Print Label'><i class='fa fa-print'></i> Label</a>&nbsp; "; 
                        }
                        if ((row.remark == "" && data == null) || row.remark != "" ) {                            
                            str += "<a target='_self' class='btn btn-warning btn-sm btn-edit-connote btn-connote-action' onclick=\"$('#modalAddConnote').modal('show').find('.modal-content').load('{{ URL::to('connote') }}/"+ row.id +"/edit',()=>{$('#modalAddConnote').trigger('loaded.bs.modal')});\"><i class='fa fa-edit'></i> Edit</a>";
                            str += "&nbsp; <a target='_self' class='btn btn-default btn-sm btn-connote-action' onclick=\"deleteConnote('"+ row.id +"',this);\"><i class='fa fa-trash'></i> Delete</a>";                            

                            return str;
                        }else{                            
                            str += "<a class='btn btn-default btn-sm btn-connote-action' onclick=\"cancelConnote('"+ row.id +"', '"+ row.receiver +"', (success)=>{myTable.ajax.reload( null, false );});\" title='Cancel Connote'><i class='fa fa-times-circle'></i> Cancel</a>";
                            str += "&nbsp; <a target='_blank' class='btn btn-default btn-sm btn-connote-action btn-send-enquiry'  onclick=\"$('#modalEnquiry').modal('show').find('.modal-content').load('{{ URL::to('connote') }}/"+ row.id +"/enquiry',()=>{$('#modalEnquiry').trigger('loaded.bs.modal')});\"  title='Send Enquiry'><i class='fa fa-envelope-o'></i> Enquiry</a>";                            
                            // str += "&nbsp; <a target='_blank' class='btn btn-danger btn-sm btn-connote-action' href='{{ URL::to('connote') }}/"+ row.id +"/testview''><i class='fa fa-cube'></i>&nbsp; testView</a>";
                            return  str;
                        }
                    },
                    "targets": 11
                },
                {"className": "align-top text-center", "targets": [ 0,2,3,4,10]},
                // {"className": "align-top text-center", "targets": "_all"},                
            ],
            
            "createdRow": function ( row, data, index ) {
                $(row).attr("data-id", data.id);
                
                if ((data.remark != "") && (data.remark != null) ) {                    
                    $(row).addClass("tooltip-moving");                    
                    // $(row).addClass("bg-status-warning");
                }
            },
            "drawCallback": function( settings ) {
                //activate the tooltips
                setTimeout(() => {
                    var tooltipSpans = $(".tooltip-moving #tooltip-span");

                    window.onmousemove = function (e) {
                        var x = e.clientX,
                            y = e.clientY;
                        for (var i = 0; i < tooltipSpans.length; i++) {
                            tooltipSpans[i].style.top = (y + 20) + 'px';
                            tooltipSpans[i].style.left = (x + 10) + 'px';
                        }
                    };

                    $('input').iCheck({
                        checkboxClass: 'icheckbox_square-blue',
                        radioClass: 'iradio_square-blue',
                        increaseArea: '20%' // optional
                    }).on('ifChanged', function (e) {
                            $(this).trigger('change');
                    });

                }, 1000); 
            }
        });

        $("#btn-submit-manifest").on( "click", function() {
            // console.log($('#sender_address_id').val());
            // console.log($('#pickup_address_id').val());

            if (($('#sender_address_id').val() != "")&&($('#sender_address_id').val() != null)&&($('#pickup_address_id').val() != "")&&($('#pickup_address_id').val() != null)){
                createManifest($("#frm-selected-connotes"));
            }else{
                swal({
                title: "Please select a sender and pickup !", 
                text: "",
                icon : "warning",
                // confirmButtonColor: "#286090",
            });
            }
        }); 

        $("#selectAddress").on('show.bs.modal', function (e) {
            
            if ($('#selectAddress .list-address-area .ct-address').length == 0){
                $('#selectAddress .list-address-area').append($('.ct-addresses').html());
            }
            addressSelector = $(e.relatedTarget);
            $('#selectAddress .list-address-area .ct-address').removeClass('active');
            
            var activeId = "";
            if (addressSelector.hasClass('sender-info')){
                activeId = senderId;
                $('#selectAddress .modal-title').html('Select Sender');
            }else if (addressSelector.hasClass('pickup-info')){
                activeId = pickupId;
                $('#selectAddress .modal-title').html('Select Pickup Location');
            }else if (addressSelector.hasClass('receiver-info')){
                $('#selectAddress .modal-title').html('Select Receiver');
            }
            
            $('#selectAddress .list-address-area .ct-address[data-id="'+activeId+'"]').addClass('active');
        });

        $("#modalSenderPickup").on('show.bs.modal', function (e) {
            if (senderId == "")
                getDefaultSender();

            var sender = $('.ct-address[data-id="'+senderId+'"]');
            $('#sender_address_id').val(senderId);
            if (sender.length){
                $('#modalSenderPickup .sender-info .address-area').empty().append(sender.html());
            }else{
                $('#modalSenderPickup .sender-info .address-area').empty();
            }

            var pickup =  $('.ct-address[data-id="'+pickupId+'"]');
            $('#pickup_address_id').val(pickupId);
            if (pickup.length){
                $('#modalSenderPickup .pickup-info .address-area').empty().append(pickup.html());
            }else{
                $('#modalSenderPickup .sender-info .address-area').empty();
            }

            $('#tbl-manifest-connotes tbody').empty();
            myTable.$('[name="select-connotes[]"]:checked').each(function(index, itm){
                var dataRow = $(this).parents('tr');
                var d = myTable.row(dataRow).data();
                $('#tbl-manifest-connotes tbody').append(
                    $('<tr><td>'+d.reference+'</td><td>'+d.parcel_type+'</td><td>'+d.receiver+'</td><td>'+d.destination+'</td><td>'+dataRow.find('.created-at').html()+'</td></tr>')
                );
            });
        });

        $('#modalAddConnote').on('loaded.bs.modal', function (e) {
            addressSelector = $('#modalAddConnote .sender-info');
            onclickAddress($('.ct-addresses [data-id="'+$("#modalAddConnote form [name=sender_address_id]").val()+'"]'));
            
            addressSelector = $('#modalAddConnote .pickup-info');
            onclickAddress($('.ct-addresses [data-id="'+$("#modalAddConnote form [name=pickup_address_id]").val()+'"]'));
        });

        $("#modalImportConnotes").on('shown.bs.modal', function (e) {
            $("#modalImportConnotes form").get(0).reset();
            $("#modalImportConnotes form [name=courier_id]").val(courierId);

            addressSelector = $('#modalImportConnotes .sender-info');
            onclickAddress($('.ct-addresses [data-id="'+senderId+'"]'));
            
            addressSelector = $('#modalImportConnotes .pickup-info');
            onclickAddress($('.ct-addresses [data-id="'+pickupId+'"]'));
        });
    });


    function cancelConnote(id, reciever, callback){
        Swal.fire ({
            title: reciever? "Cancel Connote for "+reciever+"?": "Cancel Connote?",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            icon: 'warning',
            confirmButtonText: "Yes",
            confirmButtonColor: "#286090",
            denyButtonText: "No",
            reverseButtons: true,
            backdrop: true,
            allowOutsideClick: () => !Swal.isLoading(),
            preConfirm: (data) => {
                return new Promise((resolve, reject) => {
                    $.post(baseUrl+'/connote/'+id+'/cancel', {email:$('#email').val(),role:$('#role-type').val()}, function(response){
                        resolve(response);
                    },'json')
                    .fail(function(xhr, status, error) {
                        reject(xhr.responseJSON);
                    });
                }).catch(error => {
                    Swal.showValidationMessage(
                        ` ${error.message}`
                        )
                    });
                }
            })
        .then((result) => {
            if (callback)
                callback(result.value, result.isConfirmed);

            if (result.isConfirmed) {
                if (result.value.success){
                    Swal.fire("Connote Canceled", "Please discard previous printed label", 'success');
                }else{
                    Swal.fire("Connote unable to cancel", "Booking already placed, please phone in directly", 'warning');
                }
            
            }
        });
    }

    function removeRow(itm){
        var row = $(itm).parents('tr');
        row.parents('table').DataTable().row(row).remove().draw();
    }

    function updateRow(itm, field, value){
        var row = $(itm).prop('tagName')=='TR'?$(itm):$(itm).parents('tr');

        var dataTBRow = row.parents('table').DataTable().row(row);
        var data = dataTBRow.data();
        data[field] = value;
        dataTBRow.data( data ).draw();
    }

    function updatePrintStatus(itm){
        updateRow(viewLabelRow,'is_printed','1');
        $('#created-indicator').text(parseInt($('#created-indicator').text()) - 1);
        activate_steps(3);
    }

    var senderId = '{{hashids_encode(app("account")->user()->getParams("logistic.prefer.sender",""))}}';
    var pickupId = '{{hashids_encode(app("account")->user()->getParams("logistic.prefer.pickup",""))}}';
    var courierId = '{{$courier?hashids_encode($courier->id):""}}';    

    function getDefaultSender(){
        senderId = '';
        pickupId = '';
        
        var senders = $('.ct-address');
        if (senders.length)
            senderId = $(senders.get(0)).data('id');
        
        var pickups = $('.ct-address');
        if (pickups.length)
            pickupId = $(pickups.get(0)).data('id');

    }

    var addressSelector;
    
    function onclickAddress(itm){
        var selected = $(itm);
        var parentForm = addressSelector.parent('form');
        if (addressSelector.hasClass('sender-info')){
            senderId = selected.attr('data-id')
            parentForm.find('[name="sender_address_id"]').val(senderId);
        }else if (addressSelector.hasClass('pickup-info')){
            pickupId = selected.attr('data-id');
            parentForm.find('[name="pickup_address_id"]').val(pickupId);
        }else if (addressSelector.hasClass('receiver-info')){
            //for entry modal
            ele_content = selected.children('.spacer');

            if ($('#modalAddConnote').hasClass('in')){
                $('#modalAddConnote #first_name').prop('value', ele_content.children('#ct-first-name').html());
                $('#modalAddConnote #last_name').prop('value', ele_content.children('#ct-last-name').html());
                $('#modalAddConnote #company').prop('value', ele_content.children('#ct-company').html());
                $('#modalAddConnote #phone_1').prop('value', ele_content.children('#ct-phone-1').html());
                $('#modalAddConnote #email').prop('value', ele_content.children('#ct-email').html());
                $('#modalAddConnote #address_1').prop('value', ele_content.children('#ct-address-1').html());
                $('#modalAddConnote #address_2').prop('value', ele_content.children('#ct-address-2').html());
                $('#modalAddConnote #state').prop('value', ele_content.children('#ct-state').html()).trigger('change');
                // setTimeout(() => {
                $('#modalAddConnote #city').prop('value', toCapitalizeStr(ele_content.children('#ct-city').html().trim()));
                $('#modalAddConnote #postcode').prop('value', ele_content.children('#ct-postcode').html());
                // $('#modalAddConnote #city_state_code').prop('value', $('#city').val()+", "+$('#state').val()+" "+$('#postcode').val());                   
                $('#btn-save-address').prop('disabled', true);
                // }, 1500);                
            
            }
        }
        
        addressSelector.find('.address-area').empty().append(selected.html());
        $('#selectAddress').modal('hide');
        
    }


    // Submit a single connote
    //---------------------------------------
    function addNewConnote(frm){
        event.preventDefault();

        $.post("{{ URL::to('connote/') }}", { 

            reference   : frm["reference"].value, 
            weight      : frm["weight"].value, 
            width       : frm["width"].value, 
            height      : frm["height"].value, 
            length      : frm["length"].value, 
            parcel_type : frm["parcel_type"].value, 
            declared_value: frm["declared_value"].value, 
            atl         : frm["atl"].value, 
            instruction : frm["instruction"].value,
            title       : frm["title"].value,
            first_name  : frm["f_name"].value,
            middle_name : frm["m_name"].value,
            last_name   : frm["l_name"].value,
            company     : frm["company"].value,
            phone_1     : frm["phone_1"].value,
            phone_2     : frm["phone_2"].value,
            address_1   : frm["addr_1"].value,
            email       : frm["email"].value,
            city        : frm["city"].value,
            state       : frm["state"].value,
            country     : frm["country"].value,
            postcode    : frm["postcode"].value,
            // remark      : frm["remark"].value,

        }, function( data ) {                        
            // alert(data);                
        }, 'json')
        .done(function (data) {
            myTable.ajax.reload( null, false );

            updateConnIndicator('{{URL("connote/numConnsIndicator")}}', "created-indicator");            

            $('#modalAddConnote').modal('hide');

        })
        .fail(function (jqXHR, textStatus, errorThrown) { 
            // serrorFunction(); 
        });

        return false;
    }



    // Delete a connote
    //------------------------
    function deleteConnote(id, itm){
        event.preventDefault();

        sweetAlert({
            title: 'Delete a Connote?',
            text: "Do you really want to delete this connote?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        },
        function(isConfirm) {
            if (isConfirm) {

                $.post("{{ URL::to('connote/') }}/"+id, { 
                    _method   : "delete", 
                }, function( data ) {                        
                    // alert(data);                
                }, 'json')
                .done(function (data) {
                    swal({
                        title: "Deleted!", 
                        text: "The connote has been successfully deleted.",
                        icon : "success",
                        confirmButtonColor: "#286090",
                    });

                    removeRow(itm);
                    setTimeout(function () { 
                        updateConnIndicator('{{URL("connote/numConnsIndicator")}}', "created-indicator");
                    }, 500);
                })
                .fail(function (jqXHR, textStatus, errorThrown) { 
                    // serrorFunction(); 
                });
            } else {
                
                // setTimeout(function () { 
                //     swal({
                //         title: "Cancelled!", 
                //         text: "",
                //         icon : "error",
                //         confirmButtonColor: "#286090",
                //     });
                    
                //  }, 500);
            }
        });
        
        return false;
    }

    
    
    // Import multiple connotes from an uploaded file
    //----------------------------------------------------------
    function uploadConnotes(frm){

        event.preventDefault();

        var form_data = new FormData($(frm).get(0));
        //form_data.append("import_file", frm["customFile"].files[0]);

        $.ajax({
            url:'{{URL("connote/import")}}',
            method:'POST',
            data:form_data,
            contentType:false,
            cache:false,
            processData:false,
            // beforeSend:function(){
            //     //console.log('loading....');
            // },
            success:function(datas){
                myTable.ajax.reload( null, false );
                
                updateConnIndicator('{{URL("connote/numConnsIndicator")}}', "created-indicator");             
                
                $('#modalImportConnotes').modal('hide');
                $('#modalImportConnotes form').get(0).reset();
            }
        });
        
        return false;
    }


    // Create Manifest
    //--------------------------------
    function createManifest(frm){
       
        event.preventDefault();

        var selectedIDs = $('.select-connotes:checked').map(function(){
            return $(this).val();
        }).get();

        // console.log(selectedIDs);
        // console.log(selectedIDs.length);

        var form_data = new FormData();
        // form_data.append("ids[]", frm["customFile"].files[0]);
        // form_data.append("ids", selectedIDs);
        for (var i = 0; i < selectedIDs.length; i++) {
            form_data.append('ids[]', selectedIDs[i]);
        }

        form_data.append('sender_address_id', $('#sender_address_id').val());
        form_data.append('pickup_address_id', $('#pickup_address_id').val());
        Swal.fire({
                title: 'Please Wait !',
                html: 'Creating Manifest, please waiting...',// add html attribute if you want or remove
                allowOutsideClick: false,
                showConfirmButton: false,
                // onBeforeOpen: () => {
                //     Swal.showLoading()
                // },
            });
        $.ajax({
            url:'{{URL("manifest")}}',
            method:'POST',
            data:form_data,
            contentType:false,
            cache:false,
            processData:false,
            beforeSend:function(){
            },
            success:function(data){
                //console.log(data);                                
                myTable.ajax.reload( null, false );

                updateConnIndicator('{{URL("connote/numConnsIndicator")}}', "created-indicator");

                $('#modalSenderPickup').modal('hide');

                $("#btn-create-manifest").prop('disabled', true);   //diable the submit manifest button
                Swal.close()
                Swal.fire({
                        title: "Manifest created", 
                        text: data.message,
                        icon : "success",
                        confirmButtonColor: "#286090",
                });

                setTimeout(() => {
                    myTable.ajax.reload( null, false );
                }, 3000); 
            },
            error: function(xhr, status, error){
                myTable.ajax.reload( null, false );
                $('#modalSenderPickup').modal('hide');
                Swal.close()
                Swal.fire({
                        title: "Manifest failed", 
                        text: xhr.responseJSON.message,
                        icon : (xhr.status >= 300 && xhr.status <=400)?"warning":"error",
                        confirmButtonColor: "#286090",
                    });
            }
        });
        
        return false;
    }

    function saveConnote(frm){

        event.preventDefault();

        var jFrm = $(frm);
        $.post(jFrm.attr('action'), 
            jFrm.serialize(), function( data ) {                        
                        
            }, 'json')
            .done(function (data) {
                myTable.ajax.reload( null, false );

                updateConnIndicator('{{URL("connote/numConnsIndicator")}}', "created-indicator");
                
                $('#modalShowConnote').modal('hide');
                $('#modalAddConnote').modal('hide');
            })
            .fail(function (jqXHR, textStatus, errorThrown) { 
                myTable.ajax.reload( null, false );
                if (jqXHR.responseJSON.view && jFrm.parents('.modal-content').length){
                    jFrm.parents('.modal-content').html(jqXHR.responseJSON.view);
                }
            });

        return false;        
    }

    // Check if any checkbox is checked.
    //-------------------------------
    function checkIt(ele){
        if ($('.select-connotes:checked').length > 0){
            $("#btn-create-manifest").prop('disabled', false);
            activate_steps(4);
        }else{
            $("#btn-create-manifest").prop('disabled', true);
            activate_steps(3);
        }
    }

    // Check all
    //-------------------------------
    
    function checkItAll(ele){
        
        if (ele.checked) {            
            console.log("select all");
            console.log(myTable.rows());
            console.log(myTable.rows().data());
            // console.log(myTable.fnGetNodes());
            
            // myTable.rows().select();
            $('.select-connotes').each(function() {
                // console.log(this.parent().checked);
                $(this).parent().addClass('checked');
                this.checked = true;
                // this.select();
                // $(this).prop('checked', 'checked');
            });
            // myTable.rows().each(function() {
            //     console.log($(this));
            //     // $(this).parent().addClass('checked');
            //     // this.checked = true;
            //     // this.select();
            //     // $(this).prop('checked', 'checked');
            // });
        } else {
            console.log("--unselect all");
            // $('.select-connotes').each(function() {                
            //     $(this).parent().removeClass('checked');
            //     this.checked=false;
            // });
        }
        // console.log($('.select-connotes:checked').length);
        

        // if ($('.select-connotes:checked').length > 0){
        //     $("#btn-create-manifest").prop('disabled', false);
        //     activate_steps(3);
        // }else{
        //     $("#btn-create-manifest").prop('disabled', true);
        //     activate_steps(2);
        // }
    }
    

    //--------------------------
    function clickLabel(id, consig, itm){
        $(this).toggleClass("selected").siblings(".selected").removeClass("selected");

        $('#modalLabelConnote').modal('show').find('.modal-content img#connote-label').prop('src', "{{ URL::to('connote') }}/"+ id +"/label");

        $('#modalLabelConnote #selectClientLabel').html('<i class="fa fa-file"></i> &nbsp; Print Label : connote-'+ consig); 
        
        $('#modalLabelConnote #btn-printPDF').attr('onclick', "updatePrintStatus(viewLabelRow); printPDF('{{ URL::to('connote') }}/"+ id +"/label-pdf'); return false;");
        $('#modalLabelConnote #btn-viewPDF').attr('href', "{{ URL::to('connote') }}/"+ id +"/label-pdf");
    }

</script>
@endscript

