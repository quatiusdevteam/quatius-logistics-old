<link href="vendors/css/timeline-simple.css" rel="stylesheet" />
<?php
$flagCurrStatus = false;
?>
<div id="timeline_area section_padding_130">
    <div class="row">                            
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- Timeline Area-->
            <div class="apland-timeline-area">
                    <div class="single-timeline-area">
                        <div class="timeline-date wow fadeInLeft" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInLeft;">                           
                            <div class="d-flex wow fadeInLeft " data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft; text-transform:capitalize; font-size:2.5em;">
                                <div class="timeline-icon"><i class="fa fa-home" aria-hidden="true"></i></div>                                
                            </div>  
                        </div>
                        <div class="row"> 
                            <div class="col-12 col-md-11 col-lg-11">
                                <div class=" d-flex wow fadeInLeft" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft;">
                                    <!-- <div class="timeline-icon"><i class="fa fa-user" aria-hidden="true"></i></div> -->
                                    <div class="timeline-text">
                                        <h5 style="font-weight: bold;">Delivery Address</h5>                                        
                                        <p style="font-weight: bold;">
                                            @php
                                            $destCountry = country($connote->receiver->country);
                                            @endphp

                                            <i class="fa fa-map-marker"></i>&nbsp; {{$connote->receiver->city}}, {{$connote->receiver->state}} {{$connote->receiver->postcode}}, 
                                            {{$destCountry->getName()}} &nbsp;&nbsp;<span class='flag'>{!!$destCountry->getFlag()!!}</span>
                                        </p>                          
                                    </div>
                                </div>
                            </div>                            
                        </div>
                    </div>
                    @php
                    $prev_arrData=["","",""];
                    $curr_arrData=["","",""];
                    @endphp


                    @foreach($connote->histories->sortByDesc('created_at') as $ind => $history)
                        @if (in_array($history->status->type, array_merge(
                            config('quatius.logistic.status.group.transit',[]),
                            config('quatius.logistic.status.group.complete',[]),
                            config('quatius.logistic.status.group.undeliverable',[])
                            )))  
                            
                            @php
                            $curr_arrData[0]= (($history->status->type == "unknown" || $history->status->type == "other")?"":$history->status->type);
                            $curr_arrData[1]= logistic_msg($history);
                            $curr_arrData[2]= date("j F Y", strtotime($history->created_at));
                            @endphp

                            @if(($curr_arrData[0] != $prev_arrData[0]) || ($curr_arrData[1] != $prev_arrData[1]) || ($curr_arrData[2] != $prev_arrData[2]))
                            <div class="single-timeline-area" >
                                <div class="timeline-date wow fadeInLeft" data-wow-delay="0.1s" style=" visibility: visible; animation-delay: 0.1s; animation-name: fadeInLeft;">
                                @if($flagCurrStatus == false)
                                    <div class=" d-flex wow fadeInLeft" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft; text-transform:capitalize;">
                                        <div class="timeline-text text-primary">
                                            <h5>Current Status &nbsp; &nbsp;<i class="fa fa-arrow-circle-right" aria-hidden="true" style="font-size:1.2em;"></i></h5>
                                        </div>
                                    </div>  
                                    @php
                                        $flagCurrStatus = true;
                                    @endphp                             
                                @else
                                    &nbsp;
                                @endif
                                </div>                        
                                <div class="row">                         
                                    <div class="col-12 col-md-11 col-lg-11">
                                        <div class="single-timeline-content d-flex wow fadeInLeft " data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft; text-transform:capitalize;">
                                            <div class="timeline-text">
                                                <h5  style="text-transform:capitalize;">{{$curr_arrData[0]}}
                                                    <span style="font-size:0.85em; color:#c4c4c4">, {{$curr_arrData[1]}}</span>
                                                    <span class="pull-right">{{$curr_arrData[2] == $prev_arrData[2]? "":$curr_arrData[2]}}</span>
                                                </h5>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            @endif

                            @php
                            $prev_arrData = $curr_arrData;
                            @endphp
                        @endif
                    @endforeach   
                    
                    <div class="single-timeline-area">
                        <div class="timeline-date wow fadeInLeft" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInLeft;">                           
                            <div class="d-flex wow fadeInLeft " data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft; text-transform:capitalize; font-size:2.5em;">
                                <div class="timeline-icon"><i class="fa fa-truck" aria-hidden="true"></i></div>                                
                            </div>  
                        </div>
                        <div class="row"> 
                            <div class="col-12 col-md-11 col-lg-11">
                                <div class=" d-flex wow fadeInLeft" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft;">
                                    <div class="timeline-text">
                                        <!-- <h5>Pick-up</h5>                                         -->
                                        <h5 style="font-weight: bold;">
                                            @php
                                            $country = country($connote->manifest->pickup->country);
                                            @endphp

                                            <i class="fa fa-map-marker"></i>&nbsp; {{($country->getDivision($connote->manifest->pickup->state))['name']}}, 
                                            {{$country->getName()}} &nbsp;&nbsp;<span class='flag'>{!!$country->getFlag()!!}</span>
                                        </h5>                                        
                                    </div>
                                </div>
                            </div>                            
                        </div>
                    </div>
                    
            </div>
        </div>
    </div>
    <div class="row">&nbsp;</div>
</div>

<style>
.flag svg {
    width: 30px;
    height: 20px;
}

.timeline_area {
    position: relative;
    z-index: 1;
}
.single-timeline-area {
    position: relative;
    z-index: 1;
    padding-left: 200px;
}
@media only screen and (max-width: 575px) {
    .single-timeline-area {
        padding-left: 100px;
    }
}
.single-timeline-area .timeline-date {
    position: absolute;
    width: 200px;
    height: 100%;
    top: 0;
    left: 0;
    z-index: 1;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    -ms-grid-row-align: center;
    align-items: center;
    -webkit-box-pack: end;
    -ms-flex-pack: end;
    justify-content: flex-end;
    padding-right: 60px;
}
@media only screen and (max-width: 575px) {
    .single-timeline-area .timeline-date {
        width: 100px;
    }
}
.single-timeline-area .timeline-date::after {
    position: absolute;
    width: 3px;
    height: 100%;
    content: "";
    background-color: #ebebeb;
    top: 0;
    right: 30px;
    z-index: 1;
}
.single-timeline-area .timeline-date::before {
    position: absolute;
    width: 11px;
    height: 11px;
    border-radius: 50%;
    background-color: #c4c4c4;
    content: "";
    top: 50%;
    right: 26px;
    z-index: 5;
    margin-top: -5.5px;
}
.single-timeline-area .timeline-date p {
    margin-bottom: 0;
    color: #020710;
    font-size: 1em;
    text-transform: uppercase;
    /* font-weight: 500; */
}
.single-timeline-area .single-timeline-content {
    position: relative;
    z-index: 1;
    padding: 0.1em 1em;
    /* padding: 30px 30px 25px; */
    min-width: 150px;
    /* border-radius: 6px; */
    /* margin-bottom: 0px; */
    /* margin-top: 0px; */
    /* margin-bottom: 15px;
    margin-top: 15px;
    -webkit-box-shadow: 0 0.25rem 1rem 0 rgba(47, 91, 234, 0.125);
    box-shadow: 0 0.25rem 1rem 0 rgba(47, 91, 234, 0.125); */
    /* border: 1px solid #ebebeb; */
}
@media only screen and (max-width: 575px) {
    .single-timeline-area .single-timeline-content {
        padding: 0.1em 1em;
    }
}
.single-timeline-area .single-timeline-content .timeline-icon {
    -webkit-transition-duration: 500ms;
    transition-duration: 500ms;
    width: 30px;
    height: 30px;
    background-color: #c4c4c4;
    -webkit-box-flex: 0;
    -ms-flex: 0 0 30px;
    flex: 0 0 30px;
    text-align: center;
    max-width: 30px;
    border-radius: 50%;
    margin-right: 15px;
}
.single-timeline-area .single-timeline-content .timeline-icon i {
    color: #ffffff;
    line-height: 30px;
}
.single-timeline-area .single-timeline-content .timeline-text h5 {
    -webkit-transition-duration: 500ms;
    transition-duration: 500ms;
}
.single-timeline-area .single-timeline-content .timeline-text p {
    font-size: 1em;
    margin-bottom: 0;
}
.single-timeline-area .single-timeline-content:hover .timeline-icon,
.single-timeline-area .single-timeline-content:focus .timeline-icon {
    background-color: #FFCB08;
}
.single-timeline-area .single-timeline-content:hover .timeline-text h5,
.single-timeline-area .single-timeline-content:focus .timeline-text h5 {
    color: #a4a4a4;
}
</style>
