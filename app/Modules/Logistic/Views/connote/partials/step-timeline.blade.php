@includeStyle('vendors/css/timeline-simple.css')

<div id="timeline-step-connotes">
    <div>&nbsp;</div>
    <div class="row">                            
        <div class="col-md-12">
            <div class="timeline-steps aos-init aos-animate" data-aos="fade-up">
                <div class="timeline-step active">
                    <div class="timeline-content" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="2003">
                        <div class="inner-circle"></div>
                        <p class="h5 mt-3 mb-1">Step 1:</p>
                        <p class="h5 mb-0 mb-lg-0">Create Connote(s)</p>
                    </div>
                </div>
                <div class="timeline-step">
                    <div class="timeline-content" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="2010">
                        <div class="inner-circle"></div>
                        <p class="h5 mt-3 mb-1">Step 2:</p>
                        <p class="h5 mb-0 mb-lg-0">Print Label(s)</p>
                    </div>
                </div>
                <div class="timeline-step">
                    <div class="timeline-content" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="2004">
                        <div class="inner-circle"></div>
                        <p class="h5 mt-3 mb-1">Step 3:</p>
                        <p class="h5 mb-0 mb-lg-0">Select Connote(s)</p>
                    </div>
                </div>
                <div class="timeline-step">
                    <div class="timeline-content" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="2005">
                        <div class="inner-circle"></div>
                        <p class="h5 mt-3 mb-1">Step 4:</p>
                        <p class="h5 mb-0 mb-lg-0">Create Manifest</p>
                    </div>
                </div>                
                <div class="timeline-step">
                    <div class="timeline-content" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="And here's some amazing content. It's very engaging. Right?" data-original-title="2010">
                        <div class="inner-circle"></div>
                        <p class="h5 mt-3 mb-1">Step 5:</p>
                        <p class="h5 mb-0 mb-lg-0">Finish</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">


let itm_step = $('#timeline-step-connotes .timeline-steps .timeline-step');

// Check if any checkbox is checked.
//-------------------------------
function activate_steps(nbSteps){
    desactivate_steps();
    for (let i=0; i < nbSteps; i++) {
        itm_step.eq(i).addClass('active');
    }    
}

function desactivate_steps(){
    for (let i=0; i < itm_step.length; i++) {
        itm_step.eq(i).removeClass('active');
    }
}

</script>