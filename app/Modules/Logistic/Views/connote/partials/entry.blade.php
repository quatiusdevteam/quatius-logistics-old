@includeStyle('vendors/css/logistic.css')

<fieldset>
    <div class="row">
        <div class="col-xs-12 col-sm-4">
            <div class="form-group required sender-info" data-toggle="modal" data-target="#selectAddress">
                <label for="sender" class="control-label"><i class="fa fa-user"></i>&nbsp; Sender<sup>*</sup></label>
                <a class="btn" style="float: right;">Change</a>
                <div class="small">&nbsp;</div>
                <div class="address-area"></div>
                {!! Form::hidden('sender_address_id')-> required('required')!!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="form-group required pickup-info" data-toggle="modal" data-target="#selectAddress">
                <label for="sender" class="control-label"><i class="fa fa-map-marker"></i>&nbsp; Pick-up<sup>*</sup></label>
                <a class="btn" style="float: right;">Change</a>
                <div class="small">&nbsp;</div>
                <div class="address-area"></div>
                {!! Form::hidden('pickup_address_id') -> required('required')!!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-4">   
            <div class="form-group required pickup-info" data-toggle="modal" data-target="#selectAddress" style="min-height:172px;">
                <label for="sender" class="control-label"><i class="fa fa-truck"></i>&nbsp; Courier<sup>*</sup></label><br/><br/>                
                {!! Form::select('courier_id')->label('')->options(app('courier')->whereStatus(2)->get()->pluck('label','hashId'))
                ->placeholder('Select courier')-> required('required')!!}
            </div>
        </div>
    </div>
</fieldset>

<div class="row">
    <div class="col-xs-12 col-sm-6">
        <fieldset>
            <legend class="color-0b">
                <i class="fa fa-user"></i>&nbsp; Receiver Details
                <span style="float:right;"><a class="btn btn-secondary receiver-info" id="selectReceiver" data-toggle="modal" data-target="#selectAddress">Select a contact &nbsp;<i class="fa fa-caret-right"></i></a></span>
            </legend>

            @include('Logistic::connote.partials.entry-address')

        </fieldset>        
    </div>
    <div class="col-xs-12 col-sm-6">
        <fieldset>
            <legend class="color-0b"><i class="fa fa-cube"></i>&nbsp; Parcel Details</legend>
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                {!! Form::select('parcel_type')
                    -> placeholder('Please select package type.')
                    -> options(config('quatius.logistic.parcel.availables',[]))
                    -> value('carton')
                    -> required('required') !!}
                
                </div>    
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                {!! Form::number('weight') -> label('weight (Kg)')
                    ->step("0.01")
                    ->min("0.01")
                    ->inlineHelp('Min: 0.01')
                -> required('required')!!}
                </div>    
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                {!! Form::number('length') -> label('length (cm)')        
                    ->min("5")->inlineHelp('Min: 5')
                -> required('required')!!}
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                {!! Form::number('width') -> label('width (cm)')
                    ->min("5")->inlineHelp('Min: 5')
                -> required('required')!!}
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                {!! Form::number('height') -> label('height (cm)')
                    ->min("5")->inlineHelp('Min: 5')
                -> required('required')!!}
                </div>    
            </div>
        </fieldset>

        <fieldset>
            <legend class="color-0b"><i class="fa fa-info-circle"></i>&nbsp; Extra</legend>
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                {!! Form::text('reference')->required('required')!!}   
                {!! Form::radio('atl','Authorize to leave')->radios([
                    'Yes' => ['name' => 'atl', 'value' => '1'],
                    'No' => ['name' => 'atl', 'value' => '0']
                ])-> required('required')!!}        
                </div>
                <div class="col-xs-12 col-sm-3">
                {!! Form::number('declared_value') -> label('declared value ($)') ->value('0.00')!!}   
                </div>
                <div class="col-xs-12 col-sm-5">
                {!! Form::textarea('instruction')->rows(3)!!}
                </div>
            </div>
            
        </fieldset>
    </div>
</div>




<style>
    .radio{
        display: inline;
    }
</style>

<script>

// $(document).ready(function() {

//     if (list_state_city_postcode.length == 0){
//         $.ajax({
//             url:'{{URL("address/postal/AU")}}',
//             method:'GET',            
//             success:function(datas){
//                 let arr = datas.data;
//                 for (let i=0; i< arr.length; i++){
//                     list_state_city_postcode[i] = toCapitalizeStr(arr[i][1])+", "+arr[i][2]+" "+ arr[i][0];                    
//                 }            
//             }
//         });

//         setTimeout(() => {        
//             $('#city_state_code').autocomplete({ source: list_state_city_postcode });
//         }, 600);        
//     }else{
//         $('#city_state_code').autocomplete({ source: list_state_city_postcode });
//     }   


    
//     $('#city_state_code').on('change focusout', function(e){        

//         let frm_parent = $('#city_state_code').parent(".form-group");
//         let item = $('#city_state_code').val().trim();

//         if (( item == "") || (! list_state_city_postcode.includes(item))){
//             this.focus();
//             this.select();
            
//             frm_parent.addClass('has-error');   
//             if (frm_parent.children('span.help-block').length <= 0){
//                 frm_parent.append('<span class="help-block">Address is not match</span>');                
//             }
//             return false;
//         }else{
//             let arr1 = $('#city_state_code').val().trim().split(",");
//             if (arr1.length > 1){
//                 let arr2 = arr1[1].trim().split(" ");
//                 if (arr1.length == 2){
//                     $('#city').val(arr1[0]);
//                     $('#state').val(arr2[0]);
//                     $('#postcode').val(arr2[1]);
//                 }
//                 frm_parent.removeClass('has-error');
//                 frm_parent.children('.help-block').remove();
//             }            
//         }
//     }); 
// });
    
</script>

