<li  {{ $menu->active or '' }}>
    <a href="{{trans_url($menu->url)}}">
        <i class="{{{ $menu->icon or 'fa fa-angle-double-right' }}}"></i>
        <span>{{$menu->name}}</span>&nbsp;&nbsp;
        <?php 
        $num_conns_undeliv = app('connote')->numConnotesDanger();
        ?>  
        <span class="badge bg-status-undeliverable {{($num_conns_undeliv>0 ? '' : 'hidden')}}"  id="undeliverable-indicator">
        {{ $num_conns_undeliv }}
        </span>
    </a>
</li>
