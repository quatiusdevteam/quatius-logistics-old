<div class="row">
    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
    {!! Form::text('first_name')
    ->placeholder('Search for address, name here ...')
    -> required('required')!!}
    </div>
    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
    {!! Form::text('last_name')
    -> required('required')!!}
    </div>
    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
    <div style="height: 25px;">&nbsp;</div>
    <button id="btn-save-address" type="button" class="btn btn-primary" style="float:right;" onclick="saveNewAddress();" disabled>
        <i class="fa fa-save"></i>&nbsp; Save To Address Book
    </button>    
    </div>
    
</div>
<div class="row">
    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
    {!! Form::text('company')!!}
    </div>
    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
    {!! Form::text('phone_1', 'Phone')-> required('required')!!}
    </div>
    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
    {!! Form::text('email')
    -> placeholder('Email')
    -> required('required')!!}
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
    {!! Form::text('address_1')
    ->label('Address 1 <span class="text-gray">&nbsp;&nbsp; e.g., house number, street/road</span>')
    ->placeholder('house number, ...')
    -> required('required')!!}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
    {!! Form::text('address_2')!!}
    </div>
</div>
<div class="row">
    <div class="col-xs-6 c-3 col-sm-md-3 col-lg-3">
    {!! Form::select('country')->required()
    -> label(trans('Address::fields.label.country'))           
    -> options(app('address')->getAvailableCountries())
    -> placeholder(trans('Address::fields.placeholder.country'))
    ->value(config('quatius.address.country.default',''))!!}
    </div>
    <div class="col-xs-6 c-3 col-sm-md-3 col-lg-3">    
    {!! Form::text('city','City/Suburb')->id("city")
    -> required('required')!!}
    </div>
    <div class="col-xs-6 c-3 col-sm-md-3 col-lg-3">
    {!! Form::select('state','State/Region')->id("state")
    -> required('required')
    -> options(app('address')->getStates(config('quatius.address.country.default','')))
    -> placeholder('Select state')!!}
    </div>
    <div class="col-xs-6 c-3 col-sm-md-3 col-lg-3">
    {!! Form::text('postcode','Postal Code')->id("postcode")
    -> required('required')!!}
    </div>    
</div>
<!-- <div class="row">
    <div class="col-xs-6 c-8 col-sm-md-8 col-lg-8">        
    {!! Form::text('city_state_code')
    -> required('required')!!}
    </div>
    
</div> -->





<style>
ul.ui-widget {
  font-size: 13px;
  border: 0;
  max-width: 880px;
  max-height: 220px;
  overflow-y: scroll;
}

ul.ui-widget li.ui-menu-item {
  list-style-image: none;
  font-family: "Helvetica Neue",HelveticaNeue,helvetica,arial,sans-serif;
  font-size: 92%;
  font-weight: 300;
  font-style: normal;
  font-size-adjust: none;
  
  margin: 0;
  padding: 5px 5px;
  
  white-space: nowrap;
}
.ui-widget-content a {
  color: #444444;
}

ul.ui-widget li.ui-menu-item b {
  font-weight: 700;
}

.ui-widget-content .ui-state-focus
{
  background-color: #D6F7FF;
  background-image: none;
  border: 0;
}
</style>

<script>



$(document).ready(function() {
   
    $.widget('custom.mcautocomplete', $.ui.autocomplete, {
        _create: function () {
            this._super();
            this.widget().menu("option", "items", "> :not(.ui-widget-header)");
        },
        _renderMenu: function (ul, items) {
            var self = this,
                thead;
            if (this.options.showHeader) {
                table = $('<div class="ui-widget-header" style="width:100%"></div>');
                $.each(this.options.columns, function (index, item) {
                    table.append('<span style="padding:0 4px; float:left; width:' + item.width + ';">' + item.name + '</span>');
                });
                table.append('<div style="clear: both;"></div>');
                ul.append(table);
            }
            $.each(items, function (index, item) {
                self._renderItem(ul, item);
            });
        },
        _renderItem: function (ul, item) {
            var t = '',
                result = '';
            $.each(this.options.columns, function (index, column) {
                var whole_word = item[column.valueField ? column.valueField : index];
                var current_search_string = document.getElementById("first_name").value;
    
                // Highlight current search term.
                var regex = new RegExp( '(' + current_search_string + ')', 'gi' );
                whole_word = whole_word.replace( regex, "<span class='text-red' style='font-weight:bold'>$1</span>" );
                t += '<span style="padding:0 4px;float:left;width:' + column.width + ';">' + whole_word + '</span>';
            });

            result = $('<li></li>')
                .data('ui-autocomplete-item', item)
                .append('<a class="mcacAnchor">' + t + '<div style="clear: both;"></div></a>')
                .appendTo(ul);
            return result;
        }
    });

    var columns = [
        {name: 'First Name'   , width: '150px'},
        {name: 'Last Name'    , width: '150px'},
        {name: 'Company'      , minWidth: '100px'},
        {name: 'Phone'        , Width: '100px'},
        {name: 'Email'        , Width: '150px'},
        {name: 'Address 1'    , minwidth: '100px'},
        {name: 'Address 2'    , minwidth: '100px'},
        {name: 'City'         , minWidth: '100px'},
        {name: 'State'        , minwidth: '50px'},
        {name: 'Postal Code'  , minWidth: '50px'}  
    ];

        
    //---autocomplete for personal detail
    $('#first_name').on('focus', function(e){
        loadRemoteAddress((response)=>{});
        setTimeout(() => {
            let personal_details = getRawListAddresses(true);

            // Sets up the multicolumn autocomplete widget.
            $("#first_name").mcautocomplete({
                showHeader: false,
                columns: columns,
                source: personal_details,

                // Event handler for when a list item is selected.
                select: function (event, ui) {
                    var result_text = (ui.item) ? (ui.item[0] + ', ' + ui.item[1] + ', ' + ui.item[2]) : '';        
                    // this.value = (ui.item ? (ui.item[0]  + ', ' + ui.item[1] + ', ' + ui.item[2]): '');
                    // this.value = (ui.item ? ui.item[0] : '');

                    $('#first_name').val(ui.item[0]);
                    $('#last_name').val(ui.item[1]);
                    $('#company').val(ui.item[2]);
                    $('#phone_1').val(ui.item[3]);
                    $('#email').val(ui.item[4]);
                    $('#address_1').val(ui.item[5]);
                    $('#address_2').val(ui.item[6]);

                    $('#city').val(toCapitalizeStr(ui.item[7]));
                    $('#state').val(ui.item[8]);
                    $('#postcode').val(ui.item[9]);

                    // $('#city').select(); //to detect if the city_state_code match?

                    $('#parcel_type').focus();
                    $('#btn-save-address').prop('disabled', true);
                    
                    return false;
                },
                minLength: 1
            });
        }, 500);
    });
    $('#first_name').on('click', function(e){
        $('#first_name').select();
    });
    

    $('#first_name, #last_name, #company, #phone_1, #email, #address_1, #address_2, #country, #state, #city, #postcode').each(function () {
        $(this).change(function () {
            let personal_details_str = getRawListAddresses(false);
            let selectedInput = [$("#first_name").val(), $("#last_name").val(), $("#company").val(), $("#phone_1").val(), $("#email").val(), $("#address_1").val(), $("#address_2").val(), $("#city").val(), $("#state").val(), $("#postcode").val()].join(', ');

            if (! personal_details_str.includes(selectedInput)){
                $('#btn-save-address').prop('disabled', false);
            }else{
                $('#btn-save-address').prop('disabled', true);
            }
        });
    });

    
    //---autocomplete for country
    if ($("#country").val() && $("#country").val().trim().length > 0){
        if (list_state_city_postcode.length == 0){
            loadRemotePostalCodes($("#country").val(), (response)=>{});
            setTimeout(() => {                

                $('#city, #postcode').each(function () {
                    $(this).autocomplete({ 
                        source: list_state_city_postcode, 
                        select: function (a, b) {                                  
                            $('#city, #state, #postcode').each(function () {    $(this).prop('style','background-color: #f4f4f4'); });
                            setTimeout(() => {
                                let item = $(this).val().trim();    //setTimeout is needed to get the fully selected data from autocomplete

                                if (( item == "") || (! list_state_city_postcode.includes(item))){            
                                    return false;
                                }else{
                                    let arr1 = $(this).val().trim().split(",");
                                    if (arr1.length > 1){
                                        let arr2 = arr1[1].trim().split(" ");
                                        if (arr1.length == 2){
                                            $('#city').val(arr1[0]);
                                            $('#state').val(arr2[0]);
                                            $('#postcode').val(arr2[1]);
                                        }

                                        $('#city, #postcode').each(function () {
                                            let frm_parent = $(this).parent(".form-group");
                                            frm_parent.removeClass('has-error');
                                            frm_parent.children('.help-block').remove();
                                        });
                                    }
                                }
                                $('#city, #state, #postcode').each(function () {    $(this).prop('style','background-color: none'); });
                            }, 300);   
                        } 
                    });
                });
            }, 600);                  
        }else{
            $('#city, #postcode').each(function () {
                $(this).autocomplete({ 
                    source: list_state_city_postcode, 
                    select: function (a, b) {                                  
                        $('#city, #state, #postcode').each(function () {    $(this).prop('style','background-color: #f4f4f4'); });
                        setTimeout(() => {
                            let item = $(this).val().trim();    //setTimeout is needed to get the fully selected data from autocomplete

                            if (( item == "") || (! list_state_city_postcode.includes(item))){            
                                return false;
                            }else{
                                let arr1 = $(this).val().trim().split(",");
                                if (arr1.length > 1){
                                    let arr2 = arr1[1].trim().split(" ");
                                    if (arr1.length == 2){
                                        $('#city').val(arr1[0]);
                                        $('#state').val(arr2[0]);
                                        $('#postcode').val(arr2[1]);
                                    }

                                    $('#city, #postcode').each(function () {
                                        let frm_parent = $(this).parent(".form-group");
                                        frm_parent.removeClass('has-error');
                                        frm_parent.children('.help-block').remove();
                                    });
                                }
                            }
                            $('#city, #state, #postcode').each(function () {    $(this).prop('style','background-color: none'); });
                        }, 300);   
                    } 
                });
            });
        }   
    }

    //---onChange action trigger on #country
    $('#country').on('change', function(e){
        loadRemotePostalCodes($("#country").val(), (response)=>{});
        setTimeout(() => {

            console.log('bug here while changing country');
            OPTIONS = "<option value=''>Select state</option>";
            
            // $('#state').autocomplete({ source: list_state_city_postcode });
            $('#state').empty().append(OPTIONS);

            $('#city, #postcode').each(function () {
                $(this).autocomplete({ 
                    source: list_state_city_postcode, 
                    select: function (a, b) {                                  
                        $('#city, #state, #postcode').each(function () {    $(this).prop('style','background-color: #f4f4f4'); });
                        setTimeout(() => {
                            let item = $(this).val().trim();    //setTimeout is needed to get the fully selected data from autocomplete

                            if (( item == "") || (! list_state_city_postcode.includes(item))){            
                                return false;
                            }else{
                                let arcr1 = $(this).val().trim().split(",");
                                if (arr1.length > 1){
                                    let arr2 = arr1[1].trim().split(" ");
                                    if (arr1.length == 2){
                                        $('#city').val(arr1[0]);
                                        $('#state').val(arr2[0]);
                                        $('#postcode').val(arr2[1]);
                                    }

                                    $('#city, #postcode').each(function () {
                                        let frm_parent = $(this).parent(".form-group");
                                        frm_parent.removeClass('has-error');
                                        frm_parent.children('.help-block').remove();
                                    });
                                }
                            }
                            $('#city, #state, #postcode').each(function () {    $(this).prop('style','background-color: none'); });
                        }, 300);   
                    } 
                });
            });

            $('#city, #state, #postcode').each(function () {
                $(this).val("");
            });

        }, 600);
    });
    
    
    $('#city, #state, #postcode').each(function () {
        $(this).click(function () {
           $(this).select();
        });
   
        $(this).focusout(function () {
            $('.ui-autocomplete.ui-widget.ui-widget-content').attr('style','display:none;')
        });
    });
    
});


function saveNewAddress(){

    event.preventDefault();
    
    var form_data = new FormData();

    form_data.append("address_type", "");
    form_data.append("nick", "");
    form_data.append("company", $('#company').val());
    form_data.append("first_name", $('#first_name').val());
    form_data.append("last_name", $('#last_name').val());
    form_data.append("phone_1", $('#phone_1').val());
    form_data.append("email", $('#email').val());
    form_data.append("address_1", $('#address_1').val());
    form_data.append("address_2", $('#address_2').val());
    form_data.append("city", $('#city').val());
    form_data.append("state", $('#state').val());
    form_data.append("country", $('#country').val());
    form_data.append("postcode", $('#postcode').val());
       
    $.ajax({
        url:'{{URL("address")}}',
        method:'POST',
        data:form_data,
        contentType:false,
        cache:false,
        processData:false,
        success:function(response){            
            swal({
                title: "Successfully Added!", 
                text: "The address has been successfully added.",
                icon : "success",
                confirmButtonColor: "#286090",
            });
            loadRemoteAddress((response)=>{});
        }
    });  
    return false;        
}
    
</script>