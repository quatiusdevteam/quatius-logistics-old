<link href="vendors/css/timeline-simple.css" rel="stylesheet" />

<div id="timeline_area section_padding_130">
    <div class="row">                            
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- Timeline Area-->
            <div class="apland-timeline-area">
                    <!-- Single Timeline Content-->
                    <div class="single-timeline-area">
                        <div class="timeline-date wow fadeInLeft" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInLeft;">
                            @foreach($connote->histories->sortBy('created_at') as $history)
                                @if (in_array($history->status->type, ["completed","delivered"]))
                                <div class="single-timeline-content d-flex wow fadeInLeft " data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft; text-transform:capitalize;">
                                    <div class="timeline-icon bg-status-{{str_replace(' ','-',$history->status->type)}}"><i class="fa fa-check" aria-hidden="true"></i></div>
                                    <div class="timeline-text">
                                        <h6>{{$history->status->type}}</h6>
                                        <p>{{date("M j, Y, g:i A", strtotime($history->created_at))}}</p>
                                        <p>{{logistic_msg($history)}}</p>
                                    </div>
                                </div>
                                @endif
                            @endforeach
                        </div>
                        <div class="row"> 
                            <div class="col-12 col-md-8 col-lg-9">
                                <div class="single-timeline-content d-flex wow fadeInLeft" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft;">
                                    <div class="timeline-icon"><i class="fa fa-user" aria-hidden="true"></i></div>
                                    <div class="timeline-text">
                                        <h6>Receiver</h6>
                                        <p>{{$connote->receiver->Company?$connote->receiver->Company:''}}
                                            {{$connote->receiver->first_name}} {{$connote->receiver->last_name}}
                                        </p>
                                        <p>
                                            <a href="mailto:{{$connote->receiver->email}}"><i class="fa fa-envelope-o"> </i>&nbsp; {{$connote->receiver->email}}</a>
                                            &nbsp;  &nbsp; <a href="tel:{{$connote->receiver->phone_1}}"><i class="fa fa-phone"></i>&nbsp; {{$connote->receiver->phone_1}}</a><br>
                                            <a target="_blank" href="http://maps.google.com/maps?q={{$connote->receiver->address_1}} {{$connote->receiver->address_2}},{{$connote->receiver->city}},{{$connote->receiver->state}},{{$connote->receiver->postcode}}" rel="nofollow ugc">
                                                <i class="fa fa-map-marker"></i>&nbsp; {{$connote->receiver->address_1}} {{$connote->receiver->address_2}}, 
                                                {{$connote->receiver->city}}, {{$connote->receiver->state}}. {{$connote->receiver->postcode}}
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>                            
                        </div>
                    </div>

                    @foreach($connote->histories->reverse() as $history)
                        @if (in_array($history->status->type, config('quatius.logistic.status.group.undeliverable',[])))                        
                        <div class="single-timeline-area">
                            <div class="timeline-date wow fadeInLeft" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInLeft;">
                                <div class="single-timeline-content d-flex wow fadeInLeft " data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft; text-transform:capitalize;">
                                    <div class="timeline-icon bg-status-{{str_replace(' ','-',$history->status->type)}}"><i class="fa fa-times" aria-hidden="true"></i></div>
                                    <div class="timeline-text">
                                        <h6>{{$history->status->type}}</h6>
                                        <p>{{date("M j, Y, g:i A", strtotime($history->created_at))}}</p>
                                        <p>{{logistic_msg($history)}}</p>
                                    </div>
                                </div>
                            </div>                        
                            <div class="row">                         
                                <div class="col-12 col-md-8 col-lg-9">
                                    <br/>&nbsp; <br/>&nbsp; <br/>&nbsp; <br/>&nbsp; <br/>&nbsp;
                                </div>
                            </div>
                        </div>
                        @endif
                    @endforeach

                    <div class="single-timeline-area">
                        <div class="timeline-date wow fadeInLeft" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInLeft;">
                            @foreach($connote->histories->reverse() as $history)
                                @if (in_array($history->status->type, ["with driver","picked-up"]))
                                <div class="single-timeline-content d-flex wow fadeInLeft " data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft; text-transform:capitalize;">
                                    <div class="timeline-icon bg-status-{{str_replace(' ','-',$history->status->type)}}"><i class="fa fa-truck" aria-hidden="true"></i></div>
                                    <div class="timeline-text">
                                        <h6>{{$history->status->type}}</h6>
                                        <p>{{date("M j, Y, g:i A", strtotime($history->created_at))}}</p>
                                        <p>{{logistic_msg($history)}}</p>
                                    </div>
                                </div>
                                @endif
                            @endforeach
                        </div>
                        <div class="row">                         
                            <div class="col-12 col-md-8 col-lg-9">
                                <div class="single-timeline-content d-flex wow fadeInLeft" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft;">
                                    <div class="timeline-icon"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                                    <div class="timeline-text">
                                        <h6>Pick Up</h6>
                                        <p>{{$connote->pickup->Company?$connote->pickup->Company:''}}
                                            {{$connote->pickup->first_name}} {{$connote->pickup->last_name}}</p>
                                        <p>
                                            <a href="mailto:{{$connote->pickup->email}}"><i class="fa fa-envelope-o"></i>&nbsp; {{$connote->pickup->email}}</a>
                                            &nbsp;  &nbsp; <a href="tel:{{$connote->pickup->phone_1}}"><i class="fa fa-phone"></i>&nbsp; {{$connote->pickup->phone_1}}</a><br>
                                            <a target="_blank" href="http://maps.google.com/maps?q={{$connote->pickup->address_1}} {{$connote->pickup->address_2}},{{$connote->pickup->city}},{{$connote->pickup->state}},{{$connote->pickup->postcode}}" rel="nofollow ugc">
                                                <i class="fa fa-map-marker"></i>&nbsp; {{$connote->pickup->address_1}}  {{$connote->pickup->address_2}}, 
                                                {{$connote->pickup->city}}, {{$connote->pickup->state}}. {{$connote->pickup->postcode}}
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
                    <div class="single-timeline-area">
                        <div class="timeline-date wow fadeInLeft" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInLeft;">
                            @foreach($connote->histories->reverse() as $history)
                                @if (in_array($history->status->type, ["submitted"]))
                                <div class="single-timeline-content d-flex wow fadeInLeft " data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft; text-transform:capitalize;">
                                    <div class="timeline-icon bg-status-{{str_replace(' ','-',$history->status->type)}}"><i class="fa fa-send" aria-hidden="true"></i></div>
                                    <div class="timeline-text">
                                        <h6>{{$history->status->type}}</h6>
                                        <p>{{date("M j, Y, g:i A", strtotime($history->created_at))}}</p>
                                        <p>{{logistic_msg($history)}}</p>
                                    </div>
                                </div>
                                @endif
                            @endforeach
                        </div>
                        <div class="row">                            
                            <div class="col-12 col-md-8 col-lg-9">
                                <div class="single-timeline-content d-flex wow fadeInLeft" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft;">
                                    <div class="timeline-icon"><i class="fa fa-user" aria-hidden="true"></i></div>
                                    <div class="timeline-text">
                                        <h6>Sender</h6>
                                        <p>{{$connote->sender->first_name}} {{$connote->receiver->last_name}}</p>
                                        <p>
                                            <a href="mailto:{{$connote->sender->email}}"><i class="fa fa-envelope-o"></i>&nbsp; {{$connote->sender->email}}</a>
                                            &nbsp;  &nbsp; <a href="tel:{{$connote->sender->phone_1}}"><i class="fa fa-phone"></i>&nbsp; {{$connote->sender->phone_1}}</a><br/>
                                            <a target="_blank" href="http://maps.google.com/maps?q={{$connote->sender->address_1}} {{$connote->sender->address_2}},{{$connote->sender->city}},{{$connote->sender->state}},{{$connote->sender->postcode}}" rel="nofollow ugc">
                                                <i class="fa fa-map-marker"></i>&nbsp; {{$connote->sender->address_1}}  {{$connote->sender->address_2}}, 
                                                {{$connote->sender->city}}, {{$connote->sender->state}}. {{$connote->sender->postcode}}
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
            </div>
        </div>
    </div>
    <div class="row">&nbsp;</div>
</div>

<style>
.timeline_area {
    position: relative;
    z-index: 1;
}
.single-timeline-area {
    position: relative;
    z-index: 1;
    padding-left: 380px;
}
@media only screen and (max-width: 575px) {
    .single-timeline-area {
        padding-left: 100px;
    }
}
.single-timeline-area .timeline-date {
    position: absolute;
    width: 380px;
    height: 100%;
    top: 0;
    left: 0;
    z-index: 1;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    -ms-grid-row-align: center;
    align-items: center;
    -webkit-box-pack: end;
    -ms-flex-pack: end;
    justify-content: flex-end;
    padding-right: 60px;
}
@media only screen and (max-width: 575px) {
    .single-timeline-area .timeline-date {
        width: 100px;
    }
}
.single-timeline-area .timeline-date::after {
    position: absolute;
    width: 3px;
    height: 100%;
    content: "";
    background-color: #ebebeb;
    top: 0;
    right: 30px;
    z-index: 1;
}
.single-timeline-area .timeline-date::before {
    position: absolute;
    width: 11px;
    height: 11px;
    border-radius: 50%;
    background-color: #c4c4c4;
    content: "";
    top: 50%;
    right: 26px;
    z-index: 5;
    margin-top: -5.5px;
}
.single-timeline-area .timeline-date p {
    margin-bottom: 0;
    color: #020710;
    font-size: 1em;
    text-transform: uppercase;
    /* font-weight: 500; */
}
.single-timeline-area .single-timeline-content {
    position: relative;
    z-index: 1;
    padding: 0.5em 1em;
    /* padding: 30px 30px 25px; */
    min-width: 150px;
    border-radius: 6px;
    margin-bottom: 15px;
    margin-top: 15px;
    -webkit-box-shadow: 0 0.25rem 1rem 0 rgba(47, 91, 234, 0.125);
    box-shadow: 0 0.25rem 1rem 0 rgba(47, 91, 234, 0.125);
    border: 1px solid #ebebeb;
}
@media only screen and (max-width: 575px) {
    .single-timeline-area .single-timeline-content {
        padding: 20px;
    }
}
.single-timeline-area .single-timeline-content .timeline-icon {
    -webkit-transition-duration: 500ms;
    transition-duration: 500ms;
    width: 30px;
    height: 30px;
    background-color: #c4c4c4;
    -webkit-box-flex: 0;
    -ms-flex: 0 0 30px;
    flex: 0 0 30px;
    text-align: center;
    max-width: 30px;
    border-radius: 50%;
    margin-right: 15px;
}
.single-timeline-area .single-timeline-content .timeline-icon i {
    color: #ffffff;
    line-height: 30px;
}
.single-timeline-area .single-timeline-content .timeline-text h6 {
    -webkit-transition-duration: 500ms;
    transition-duration: 500ms;
}
.single-timeline-area .single-timeline-content .timeline-text p {
    font-size: 1em;
    margin-bottom: 0;
}
.single-timeline-area .single-timeline-content:hover .timeline-icon,
.single-timeline-area .single-timeline-content:focus .timeline-icon {
    background-color: #FFCB08;
}
.single-timeline-area .single-timeline-content:hover .timeline-text h6,
.single-timeline-area .single-timeline-content:focus .timeline-text h6 {
    color: #3f43fd;
}
</style>
