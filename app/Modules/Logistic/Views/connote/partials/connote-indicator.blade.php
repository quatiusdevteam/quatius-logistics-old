<li id="c-indicator" class="{{ $menu->active ?: '' }}">
    <a href="{{trans_url($menu->url)}}">
        <i class="{{{ $menu->icon ?: '' }}}"></i>
        <span>{{$menu->name}}</span>&nbsp;&nbsp;      
        <?php 
        $num_conns_pending = app('connote')->numConnotesIndicator();
        ?>  
        <span class="badge bg-status-warning {{($num_conns_pending>0 ? '' : 'hidden')}}" id="created-indicator" title="Number of created and warning connotes to take action">
            {{ $num_conns_pending }}
        </span>
    </a>
</li>