
@if(! $connote)
    There is no connote
@else

{!!Form::vertical_open()
    ->id('frm-enquiry')
    ->method('POST')
    ->action(URL::to('connote')."/enquiry")
    !!}

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="selectClientLabel"><i class="fa fa-envelope-o"></i> &nbsp; Customer Enquiry for Consignment - {{$connote->consignment}}</h4>
</div>
<div class="modal-body">  

    {!! Form::hidden('consignment') -> required('required')!!}    
    {!! Form::hidden('email') -> required('required')!!}    
    {!! Form::hidden('first_name') -> required('required')!!}    
    {!! Form::hidden('last_name') -> required('required')!!}  
    {!! Form::hidden('phone_1','Phone')->readonly() !!}  
    <div class="row">       
        <div class="col-xs-12 col-sm-6 col-md-8 col-lg-8">
            {!! Form::text('from','From',$connote->manifest->sender->first_name." ".$connote->manifest->sender->last_name." <".$connote->manifest->sender->email.">") 
                -> readonly()
                -> required('required')!!}
        </div>
        <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
            {!! Form::text('company')->readonly() !!}
        </div>        
    </div>
    <div class="row">       
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            {!! Form::text('subject')-> value('Customer Enquiry: Connote-'.$connote->consignment) 
                -> readonly()
                -> required('required')!!}            
        </div>        
    </div>  
    <div class="row">        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        {!! Form::textarea('enquiry','Enquiry')->rows(10)
            -> placeholder('What would you like to know ?')
            -> required('required')!!}
        </div>
    </div>
        
</div>
<div class="modal-footer">
    <button id="btn-send-enquiry" type="submit" class="btn btn-primary"><i class="fa fa-send"></i>&nbsp; Send Now</button>
    <button id="btn-send-enquiry" type="reset" class="btn btn-secondary"><i class="fa fa-refresh"></i>&nbsp; Reset</button>
</div>

{!! Form::close() !!}

@endif

@includeScript('vendors/js/sweetalert2-11.js')

<script>

$(document).ready(function() {

    event.preventDefault();

    $( "#frm-enquiry").submit(function( event ) {
        event.preventDefault();

        $.post('{{URL("connote/enquiry")}}',  $('#frm-enquiry').serialize())
        .done(function(data){
            swal({
                title: "This enquiry is successfully submitted!", 
                text: "",
                icon : "success",
                confirmButtonColor: "#286090",
            });

            $('#modalEnquiry').modal('hide');
        })
        .fail(function(xhr, textStatus, errorThrown) {
            console.log(xhr.responseJSON);            
        });

        return false;
    });    
});