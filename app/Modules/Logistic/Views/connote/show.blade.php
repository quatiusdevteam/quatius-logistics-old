<?php
    
    //detect the undeliverable connotes to be counted and displayed on the menu sidebar
    //.......tracking?consignment=210004211396
    //--------------------------------------------------
    
    $history_id = hashids_encode($connote->latestHistory()->id);

    $flag_new_unseen = false;
    if (in_array($connote->latestHistory()->status->type, config('quatius.logistic.status.group.undeliverable',[])) && is_null($connote->latestHistory()->value)){
        $flag_new_unseen = true;
    }
    
?>

{!!Form::vertical_open()
        ->id('show-connote')
        ->method('POST')
        ->action(URL::to('connote/'.$connote->getRouteKey()))!!}
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="selectClientLabel"><i class="fa fa-file"></i> &nbsp; Connote: {{$connote->consignment}}</h4>
</div>
<div class="modal-body" style="height: 75vh; overflow:auto">
    <div class="row">
        <div class="col-sm-4">
            <div class="panel panel-info">
                <div class="panel-heading"><i class="fa fa-send"></i> Sender</div>
                <div class="panel-body">
                    {!!$connote->sender->company?$connote->sender->company.'<br/>':''!!}
                    <b>{{$connote->sender->getFullName()}}</b></br>
                    <a href="mailto:{{$connote->sender->email}}" style="font-size:0.9em;"><i class="fa fa-envelope-o"></i> {{$connote->sender->email}}</a><br/>
                    <a href="tel:{{$connote->sender->phone_1}}"><i class="fa fa-phone"></i> {{$connote->sender->phone_1}}</a><br/>
                    <br/>
                    
                    <a target="_blank" href="http://maps.google.com/maps?q={{$connote->sender->address_1}} {{$connote->sender->address_2}},{{$connote->sender->city}},{{$connote->sender->state}},{{$connote->sender->postcode}}" rel="nofollow ugc">
                    <i class="fa fa-map-marker"></i> {{$connote->sender->address_1}}  {{$connote->sender->address_2}}<br>
                    {{$connote->sender->city}},
                    {{$connote->sender->state}}. {{$connote->sender->postcode}}</a>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-info">
                <div class="panel-heading"><i class="fa fa-cube"></i>  Pickup From</div>
                <div class="panel-body">
                    {!!$connote->pickup->company?$connote->pickup->company.'<br/>':''!!}
                    <b>{{$connote->pickup->getFullName()}}</b></br>
                    <a href="mailto:{{$connote->pickup->email}}" style="font-size:0.9em;"><i class="fa fa-envelope-o"></i> {{$connote->pickup->email}}</a><br/>
                    <a href="tel:{{$connote->pickup->phone_1}}"><i class="fa fa-phone"></i> {{$connote->pickup->phone_1}}</a><br/>
                    <br/>
                    
                    <a target="_blank" href="http://maps.google.com/maps?q={{$connote->pickup->address_1}} {{$connote->pickup->address_2}},{{$connote->pickup->city}},{{$connote->pickup->state}},{{$connote->pickup->postcode}}" rel="nofollow ugc">
                    <i class="fa fa-map-marker"></i> {{$connote->pickup->address_1}}  {{$connote->pickup->address_2}}<br>
                    {{$connote->pickup->city}},
                    {{$connote->pickup->state}}. {{$connote->pickup->postcode}}</a>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-home"></i> Receiver</div>
                <div class="panel-body">
                    {!!$connote->receiver->company?$connote->receiver->company.'<br/>':''!!}
                    <b>{{$connote->receiver->first_name}} {{$connote->receiver->last_name}}</b><br>
                    <a href="mailto:{{$connote->receiver->email}}" style="font-size:0.9em;"><i class="fa fa-envelope-o"> </i> {{$connote->receiver->email}}</a><br>
                    <a href="tel:{{$connote->receiver->phone_1}}"><i class="fa fa-phone"></i> {{$connote->receiver->phone_1}}</a><br><br>

                    <a target="_blank" href="http://maps.google.com/maps?q={{$connote->receiver->address_1}} {{$connote->receiver->address_2}},{{$connote->receiver->city}},{{$connote->receiver->state}},{{$connote->receiver->postcode}}" rel="nofollow ugc">
                <i class="fa fa-map-marker"></i> {{$connote->receiver->address_1}} {{$connote->receiver->address_2}}<br>
                    {{$connote->receiver->city}},
                    {{$connote->receiver->state}}. {{$connote->receiver->postcode}}</a>
                </div>
            </div>
        </div>
    </div>

    @include('Logistic::connote.partials.tracking-frontend')

</div>

{!! Form::close() !!}

<script>

// $('#show-connote input,select,textarea').prop('disabled', true);

    $(document).ready(function() {

        event.preventDefault();

        // change the indicator/number of undeliverable connotes
        //------------------------------------------------------------------
        var js_history_id = <?= json_encode($history_id); ?>;
        var js_flag_new_unseen = <?= json_encode($flag_new_unseen); ?>;
       
        if (js_flag_new_unseen == true){
            var form_data = new FormData();
            form_data.append("id", js_history_id);
            form_data.append("value", 1);   //to indicate that this undeliverable connote has already viewed/seen once.
                                            //it will be removed from the count of undeliverable connotes
            $.ajax({
                url:'{{URL("history/updateValue")}}',
                method:'POST',
                data:form_data,
                contentType:false,
                cache:false,
                processData:false,
                success:function(datas){
                    let num_conn_undeliverable = parseInt($("#undeliverable-indicator").html()) - 1;                    
                    if (num_conn_undeliverable == 0){
                        $("#undeliverable-indicator").addClass("hidden");
                    }else if (num_conn_undeliverable > 1){
                        $("#undeliverable-indicator").html(num_conn_undeliverable);
                    }
                }
            });            
        }
        
    });
</script>