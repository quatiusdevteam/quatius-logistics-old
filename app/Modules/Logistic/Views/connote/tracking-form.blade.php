
<?php
$consignment = isset($consignment)?$consignment : "";
if($consignment == ""){
    $consignment = Request::get('consignment', "");
}
?>

{!!Form::open()
        ->id('track-connote')
        ->method('POST')
        ->action(URL::to('connote/tracking'))
        !!}
<div class="w-100 p3" style="padding:50px; font-size:1.5em;"> 
    <div class="row">
        <div class="col-md-10 text-nowrap" >
            {!! Form::text('consignment', "Tracking Number", "", ['class' => 'btn-primary', 'style' => 'float:right; max-width:650px; border-radius: 3px; height: 2.5em; font-size:inherit; padding:0.5em 1em; color:#222d32']) 
                ->value($consignment)  
                !!}
        </div>        
        <div class="col-md-2">
            {!! Form::submit('submit', null, ['class' => 'btn-primary', 'style' => 'border-radius: 3px; font-size:inherit; padding:0.5em 2em;' ])!!}
        </div>        
    </div>
</div>
<div class="container" style="padding-bottom:50px;"> 
    <div class="row">
        <div class="col-md-12 text-center" id="tracking-chart">            
            
            <img src='{{URL("images")}}/banner-tracking.png'>
            
        </div>
    </div>
</div>

{!! Form::close() !!}

<script>

$(document).ready(function() {

    event.preventDefault();

    $( "#track-connote").submit(function( event ) {
        event.preventDefault();

        $.post('{{URL("connote/tracking")}}',  $('#track-connote').serialize())
        .done(function(data){
            // console.log(data.data);
            if ((data.data != null)||(data.data != "")){                
                $('#tracking-chart').removeClass('text-center');
                $('#tracking-chart').html(data.view);
            }
        })
        .fail(function(xhr, textStatus, errorThrown) {
            if (xhr.responseJSON.code == 404){
                let msg = "<h2>Sorry, we couldn't find your tracking number</h2><br/>";
                msg += "<h4>Tracking number "+ xhr.responseJSON.data +" might be incorrect or not in our system yet. It can take a while to show up here, so check back later</h4>";

                $('#tracking-chart').addClass('text-center');                
                $('#tracking-chart').html(msg);
            }
        });

        return false;
    });

    @if($consignment != "")
        $( "#track-connote").submit();
    @endif
});
</script>
