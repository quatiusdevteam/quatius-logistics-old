@extends('public::curd.index')
@section('heading')

<i class="fa fa-cubes"></i> Connotes <small> Tracking</small>
@stop
@section('title')
Connotes
@stop
@section('breadcrumb')
<ol class="breadcrumb">
    <li><a href="{!! URL::to('admin') !!}"><i class="fa fa-dashboard"></i> {!! trans('cms.home') !!} </a></li>
    <li class="active">Connotes</li>
</ol>
@stop
@section('entry')
<div class="modal fade" id="modalShowConnote" tabindex="-1" role="dialog" aria-labelledby="addNewConnote" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="border-radius: 3px;">
    </div>
  </div>
</div>
@stop
@section('tools')
<button type="button" class="btn btn-warning btn-sm" onclick="syncAllConnotes(this)"><i class="fa fa-compress"></i> Sync All Tracking</button>

@stop
@section('content')
<table id="main-list" class="table table-striped table-bordered">
    <thead>
        <th>Account</th>         
        <th>Account Code</th>                
        <th>Consignment</th>             
        <th>External</th>
        <th>Manifest</th>                        
        <th>Reference</th>
        <th>Created At</th>
        <th>Status At</th>
        <th>Status</th>                 
        <th>Actions</th>
    </thead>
</table>
@stop
@section('script')
<script type="text/javascript">
var oTable;
$(document).ready(function(){
    //$('#entry-account').load('{{URL::to('admin/accounts/0')}}');
    oTable = $('#main-list').DataTable( {
        
        "ajax": '{{ URL::to("/admin/logistic/connote") }}',
        "columns": [                
            { "data": "name" },   
            { "data": "account_code" },   
            { "data": "consignment" },
            { "data": "courier_consignment" },
            { "data": "manifest_no" },                
            { "data": "reference" },  
            { "data": "created_at" , "className":"created-at"},  
            { "data": "status_at" },
            { "data": "status" },              
            { "data": "id" },
            // { "data": "receiver"},
            // { "data": "remark" },
        ],
        "order": [[ 7, "desc" ]],
        "columnDefs": [
            {
                "render": function ( data, type, row ) {
                    let str = " <a target='_self' class='btn btn-primary btn-xs btn-edit-connote btn-connote-action' onclick=\"$('#modalShowConnote').modal('show').find('.modal-content').load('{{ URL::to('admin/logistic/connote') }}/"+ row.id +"');\"><i class='fa fa-eye'></i> view</a>";
                        str += " <button type='button' target='_self' class='btn btn-warning btn-xs btn-connote-action' onclick=\"syncConnote('"+ row.id +"',this);\"><i class='fa fa-compress'></i> Sync</button>";
                            
                    if (row.status == "delivered")
                        str += " <a target='_blank' class='btn btn-info btn-xs ' onclick=\"clickPOD('"+ row.id +"', '"+ row.consignment +"')\"  title='Proof Of Delivery'><i class='fa fa-picture-o'></i> POD</a>"
                        // str += "&nbsp;&nbsp;&nbsp; <a target='_blank'  onclick=\"$('#modalShowConnote').modal('show').find('.modal-content').load('{{ URL::to('connote') }}/"+ row.id +"/pod');\"  title='Proof Of Delivery'><i class='fa fa-picture-o'></i> POD</a>"
                    
                    return str;
                },
                "searchable": false,
                "targets": 9
            }
        ]
        
    });
});

function syncConnote($id, item){
    $(item).find('i').removeClass('fa-compress').addClass('fa-spinner');
    $(item).prop('disabled', true);
    jQuery.getJSON("{{ URL::to('admin/logistic/connote') }}/"+$id+"/sync-tracking",(response)=>{
        $('#main-list').DataTable().ajax.reload( null, false );
        $(item).find('i').removeClass('fa-spinner').addClass('fa-compress');
        $(item).prop('disabled', false);
    }).fail(function(hx){
        $(item).find('i').removeClass('fa-spinner').addClass('fa-compress');
        $(item).prop('disabled', false);
    });;
}

function syncAllConnotes(item){
    $(item).find('i').removeClass('fa-compress').addClass('fa-spinner');
    $(item).prop('disabled', true);
    jQuery.getJSON("{{ URL::to('admin/logistic/connote') }}/sync-tracking",(response)=>{
        $('#main-list').DataTable().ajax.reload( null, false );
        $(item).find('i').removeClass('fa-spinner').addClass('fa-compress');
        $(item).prop('disabled', false);
    }).fail(function(hx){
        $(item).find('i').removeClass('fa-spinner').addClass('fa-compress');
        $(item).prop('disabled', false);
    });
}

function statusConnote($id, item){
    jQuery.post("{{ URL::to('admin/logistic/connote/status') }}/"+$(item).val(),{ids:[$id]},(response)=>{
        $('#main-list').DataTable().ajax.reload( null, false );
    },'json');
}

</script>
@stop
@section('style')
<style>
  
</style>
@stop
