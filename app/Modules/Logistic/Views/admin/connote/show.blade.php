<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="selectClientLabel"><i class="fa fa-file"></i> &nbsp; Connote: {{$connote->consignment}}</h4>
</div>
<div class="modal-body" style="height: 75vh; overflow:auto">
    <div class="row">
        <div class="col-sm-4">
            <div class="panel panel-info">
                <div class="panel-heading"><i class="fa fa-send"></i> Sender</div>
                <div class="panel-body">
                    {!!$connote->manifest->sender->company?$connote->manifest->sender->company.'<br/>':''!!}
                    <b>{{$connote->manifest->sender->getFullName()}}</b></br>
                    <a href="mailto:{{$connote->manifest->sender->email}}" style="font-size:0.9em;"><i class="fa fa-envelope-o"></i> {{$connote->manifest->sender->email}}</a><br/>
                    <a href="tel:{{$connote->manifest->sender->phone_1}}"><i class="fa fa-phone"></i> {{$connote->manifest->sender->phone_1}}</a><br/>
                    <br/>
                    
                    <a target="_blank" href="http://maps.google.com/maps?q={{$connote->manifest->sender->address_1}} {{$connote->manifest->sender->address_2}},{{$connote->manifest->sender->city}},{{$connote->manifest->sender->state}},{{$connote->manifest->sender->postcode}}" rel="nofollow ugc">
                    <i class="fa fa-map-marker"></i> {{$connote->manifest->sender->address_1}}  {{$connote->manifest->sender->address_2}}<br>
                    {{$connote->manifest->sender->city}},
                    {{$connote->manifest->sender->state}}. {{$connote->manifest->sender->postcode}}</a>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-info">
                <div class="panel-heading"><i class="fa fa-cube"></i>  Pickup From</div>
                <div class="panel-body">
                    {!!$connote->manifest->pickup->company?$connote->manifest->pickup->company.'<br/>':''!!}
                    <b>{{$connote->manifest->pickup->getFullName()}}</b></br>
                    <a href="mailto:{{$connote->manifest->pickup->email}}" style="font-size:0.9em;"><i class="fa fa-envelope-o"></i> {{$connote->manifest->pickup->email}}</a><br/>
                    <a href="tel:{{$connote->manifest->pickup->phone_1}}"><i class="fa fa-phone"></i> {{$connote->manifest->pickup->phone_1}}</a><br/>
                    <br/>
                    
                    <a target="_blank" href="http://maps.google.com/maps?q={{$connote->manifest->pickup->address_1}} {{$connote->manifest->pickup->address_2}},{{$connote->manifest->pickup->city}},{{$connote->manifest->pickup->state}},{{$connote->manifest->pickup->postcode}}" rel="nofollow ugc">
                    <i class="fa fa-map-marker"></i> {{$connote->manifest->pickup->address_1}}  {{$connote->manifest->pickup->address_2}}<br>
                    {{$connote->manifest->pickup->city}},
                    {{$connote->manifest->pickup->state}}. {{$connote->manifest->pickup->postcode}}</a>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading"><i class="fa fa-home"></i> Receiver</div>
                <div class="panel-body">
                    {!!$connote->receiver->company?$connote->receiver->company.'<br/>':''!!}
                    <b>{{$connote->receiver->first_name}} {{$connote->receiver->last_name}}</b><br>
                    <a href="mailto:{{$connote->receiver->email}}" style="font-size:0.9em;"><i class="fa fa-envelope-o"> </i> {{$connote->receiver->email}}</a><br>
                    <a href="tel:{{$connote->receiver->phone_1}}"><i class="fa fa-phone"></i> {{$connote->receiver->phone_1}}</a><br><br>

                    <a target="_blank" href="http://maps.google.com/maps?q={{$connote->receiver->address_1}} {{$connote->receiver->address_2}},{{$connote->receiver->city}},{{$connote->receiver->state}},{{$connote->receiver->postcode}}" rel="nofollow ugc">
                <i class="fa fa-map-marker"></i> {{$connote->receiver->address_1}} {{$connote->receiver->address_2}}<br>
                    {{$connote->receiver->city}},
                    {{$connote->receiver->state}}. {{$connote->receiver->postcode}}</a>
                </div>
            </div>
        </div>
    </div>
    
    @include('Logistic::connote.partials.tracking-frontend')

</div>
<div class="modal-footer">
    @php
    $statuses = [];

    foreach(config('quatius.logistic.status.group',[]) as $group=>$stats){
        $statuses[$group] = array_combine($stats, $stats);
    }
    
    @endphp
    <div class="input-group pull-left" style="width: 250px;">
        <span class="input-group-addon">Change Status</span>
        {!!Form::select('change_status','')->attributes(['onchange'=>"statusConnote('".$connote->getRouteKey()."', this)"])->options($statuses)->value($connote->getStatus())!!}
    </div>
    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp; Close</button>
    <button type="submit" class="btn btn-primary" style="display:none"><i class="fa fa-floppy-o"></i>&nbsp; {{ trans('cms.save') }}</button>
</div>