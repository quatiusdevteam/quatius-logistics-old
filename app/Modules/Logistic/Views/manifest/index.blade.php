@includeStyle('vendors/css/logistic.css')
@includeStyle('vendors/css/print.min.css')
@includeScript('vendors/js/print.min.js')
@includeScript('vendors/js/logistic.js')

<section class="content-header">
        <h1>
        <i class="fa fa-file-text-o"></i> Manifest <small> Manage Manifest</small>
        </h1>
        <ol class="breadcrumb">
    <li><a href="http://worklocal/quatius/quatius-old/public"><i class="fa fa-dashboard"></i> Home </a></li>
    <li class="active">Manifest</li>
</ol>
</section>


<div class="modal fade" id="modalShowManifest" tabindex="-1" role="dialog" aria-labelledby="showManifest" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="border-radius: 3px;">
    </div>
  </div>
</div>

<section class="content">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">List of Manifests</h3>
            <div class="box-tools">
                <!-- <div class="dropdown" style="display: inline;">
                    <button class="btn btn-primary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-plus-circle"></i>&nbsp; List of Manifests  &nbsp;&nbsp;<i class="fa fa-caret-down"></i>
                    </button>
                    <ul class="dropdown-menu" style="margin-top: 0.8em; background-color:#40739EF2;">
                        <li class="user-footer small">
                            <a id="btn-new-connote" href="connote/create" class="color-0" data-toggle="modal" data-target="#modalAddConnote">
                                <span class="fa fa-file-o"></span> New Connote
                            </a>
                            <a id="btn-import-connotes" href="#" class="color-0" data-toggle="modal" data-target="#modalImportConnotes">
                                <span class="fa fa-upload"></span> Import Connotes
                            </a>                        
                        </li>
                    </ul>
                </div> -->

                &nbsp;
                <!-- <button id="btn-create-manifest" type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modalSenderPickup" disabled title="Please, select at least one connote to create a manifest!"><i class="fa fa-send"></i>&nbsp; Create Manifest</button> -->
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body" style="min-height:100px">

            <!-- Datatables -->
            
            {!!Form::open()->attributes(['onsubmit'=>'createManifest(this); return false;'])->id('frm-selected-connotes')!!}

                <table id="example" class="table table-striped table-bordered" style="width:100%">
                <!-- <table id="example" class="stripe row-border order-column" style="width:100%"> -->
                    <thead>
                        <tr>
                            <!-- <th>&nbsp;</th>   -->
                            <th>Status</th>                        
                            <th>Manifest</th>     
                            <th>Number of Connotes</th>                        
                            <th>Created At</th>  
                            <th>Status Updated</th>    
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>                              
                    </tbody> 
                    <!-- <tfoot>
                        <tr>
                            
                        </tr>
                    </tfoot> -->
                </table>

            {!! Form::close() !!}	

        </div>
        <!-- <div class="box-footer">
            &nbsp;
        </div> -->
    </div>
</section>

@script

<!-- Option 1 : display searchPanel as button -->
<!-- <link  href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css" rel="stylesheet"> -->
<!-- <link  href="https://cdn.datatables.net/searchpanes/1.3.0/css/searchPanes.dataTables.min.css" rel="stylesheet">
<link  href="https://cdn.datatables.net/select/1.3.3/css/select.dataTables.min.css" rel="stylesheet">
<link  href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.dataTables.min.css" rel="stylesheet"> -->


<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script> -->

<!-- <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script> -->
<!-- <script src="https://cdn.datatables.net/searchpanes/1.3.0/js/dataTables.searchPanes.min.js"></script>
<script src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
 -->

<!-- For Select2 -->
<!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> -->

<style>
    th, td{
        font-size: 1em;
    }

    input[type=file]::file-selector-button {
        /* border-left: 1px solid rgb(209,211,226); */
        border: none;
        margin-top: -.4em;
        margin-right: -.8em;
        padding: .4em 1em;
        border-radius: 0 5px 5px 0px;
        /* background-color: #a29bfe;
        background-color: #029FDA; */
        transition: 1s;
        float: right;
        z-index: 2000;
    }


    /* #new-connote div{
        text-align: left;
    }
    #new-connote div label{
        text-align: left;
    } */

    /* input[type=file]::file-selector-button:hover {
        background-color: #81ecec;
        border: 2px solid #00cec9;
    } */
</style>

<script type="text/javascript">
    
    // $('#example tfoot td').load('{{URL::to("/connote/")}}');
    // $('#example tbody td').load('{{URL::to("connote")}}');

    // $('#example tfoot th:not(:first-child)').each(function() {
    //     var title = $(this).text();
    //     // $(this).html('<input type="text" placeholder="Search ' + title + '" />');
    //     $(this).html('<input type="text" style="width:60px; font-family: FontAwesome;" placeholder="&#xF002; "/>');
    // });
    var myTable;
    $(document).ready(function() {
        
        myTable = $('#example').DataTable({
            "columns": [                
                { "data": "status" },                
                { "data": "manifest_no" },    
                { "data": "num_connotes" },                
                { "data": "created_at" },
                { "data": "status_at" },
                { "data": "id" },
            ],
            "order": [[ 3, "desc" ]],

            serverSide: true,
            processing: true,

            ajax: {
                url: "{{ URL::to('manifest') }}",
                // url: "{{ URL::to('connote/') }}",

                "dataType": "json",
                "type": "GET",
                "data":{
                    extra_fields : [
                        // "id", 
                        // "account_id", "params", "sender_address_id", "pickup_address_id", "courier_id", 
                        // "deleted_at", "updated_at", "history_link_id",
                        // "is_printed"
                    ],
                //     // hash:{
                //     //     from: "id",
                //     //     to: "eid"
                //     // }
                }
            },
            // "userLength": 10,

            "pageLength": 10,
            "columnDefs": [
                {"className": "align-top text-center", "targets": "_all"},
                // {"className": "align-top text-center", "targets": [ 0,1,2,3,4,5,6,7,8]},
                // {"className": "align-top text-right", "targets": [ 11,13]},
                // {
                //     "render": function ( data, type, row ) {
                //         // console.log(row.remark);
                //         if (((row.status == "")||(row.status == null)) && ((row.remark == "") || (row.remark == null) )) {
                //             return "<input type='checkbox' class='select-connotes' name='select-connotes[]' value='"+ data +"' onchange='checkIt(this);'>";
                //         }else{
                //             return "";
                //         }
                //     },
                //     "orderable": false,
                //     "targets": 0
                // },
                // {
                //     "render": function ( data, type, row ) {          
                //         if ((row.remark != "") && (row.remark != null) ) {
                //             return '<span class="status-warning"><i class="fa fa-warning" title="'+row.remark+'"></i>&nbsp; warning!!!</span>';
                //         }else{
                //             return data;
                //         }
                //     },
                //     "targets": 1
                // },
                {
                    "render": function ( data, type, row ) {                    
                        // if ((row.manifest_no != "") && (row.manifest_no != null)) {
                        //     return "<a target='_new' href='{{ URL::to('manifest') }}/"+ row.id +"/label' class=''><i class='fa fa-print'></i></a> &nbsp; "+ data +"";
                        // }else{
                        //     return "";
                        // }

                        let str = "";
                        if ((row.manifest_no != "") && (row.manifest_no != null)) {
                            // str += "<a target='_blank' class='btn-link btn-edit-connote'  onclick=\"$('#modalShowConnote').modal('show').find('.modal-content').load('{{ URL::to('connote') }}/"+ row.id +"');\"  title='View a Connote'><i class='fa fa-eye'></i> View</a>"
                            str += "&nbsp;&nbsp;&nbsp; <a target='_blank' class='btn-link' onclick=\"$('#modalShowManifest').modal('show').find('.modal-content').load('{{ URL::to('manifest') }}/"+ row.id +"');\"  title='View a manifest'><i class='fa fa-files-o'></i>&nbsp; "+ row.manifest_no +"</a>"

                            // if (row.status == "delivered")
                            //     str += "&nbsp;&nbsp;&nbsp; <a target='_blank'  onclick=\"$('#modalShowConnote').modal('show').find('.modal-content').load('{{ URL::to('connote') }}/"+ row.id +"/pod');\"  title='Proof Of Delivery'><i class='fa fa-picture-o'></i> POD</a>"
                        }
                        return str;
                    },
                    "targets": 1
                },
                // {
                //     "render": function ( data, type, row ) {
                //         return  row.parcel_type +' ('+ row.length +' <small>x</small> '+ row.width +' <small>x</small> '+ row.height +')  &nbsp; , &nbsp; '+ parseFloat(data).toFixed( 2 );
                //     },
                //     "targets": 5
                // },
                // // {
                // //     "render": function ( data, type, row ) {
                // //         return  '<small>'+row.currency +'</small> '+ parseFloat(data).toFixed( 2 ) ;
                // //     },
                // //     "targets": 8
                // // },
                // {
                //     "render": function ( data, type, row ) {
                //         // console.log(data);
                //         // console.log(type);
                //         // console.log(row.consignment);
                //         // console.log(row.remark);
                //         // if ((row.consignment != "")) {
                //             // let url_const = 'connote/' + data +'/label'; 
                //             // console.log(url_const);
                //             let str = ""
                //             str += "<a href='{{ URL::to('manifest') }}/"+ row.id +"' data-toggle='modal' data-target='#modalShowManifest' title='Detail'><i class='fa fa-info-circle'></i></a>";
                //             // str += "<a target='_self' href='{{ URL::to('connote') }}/"+ row.id +"/edit' data-toggle='modal' data-target='#modalEditConnote'><i class='fa fa-edit'></i></a>";
                //             // str += " &nbsp; | &nbsp; <a target='_self' href='#' onclick=\"deleteConnote('"+ row.id +"');\"><i class='fa fa-trash'></i></a>";
                //             // str += " | <a target='_self' href='#'><i class='fa fa-edit'></i></a>";

                //             return str;
                //         // }else{
                //         //     return "";
                //         // }
                //     },
                //     "targets": 4
                // },
                {
                    "render": function ( data, type, row ) {
                        let str = "";
                        str += "<a target='_blank' class='btn-link btn-edit-connote'  onclick=\"$('#modalShowManifest').modal('show').find('.modal-content').load('{{ URL::to('manifest') }}/"+ row.id +"');\"  title='View a manifest'><i class='fa fa-print'></i>&nbsp; Print Report </a>"
                        // str += "&nbsp;&nbsp;&nbsp; <a target='_blank'  onclick=\"clickLabel('"+ row.id +"', '"+ row.consignment +"')\"  title='Contact Us'><i class='fa fa-envelope-o'></i>&nbsp; Contact Us</a>"
                        // str += "&nbsp;&nbsp;&nbsp; <a target='_blank'  onclick=\"clickLabel('"+ row.id +"', '"+ row.consignment +"')\"  title='Print Label'><i class='fa fa-print'></i> Label</a>"
                        // str += "&nbsp;&nbsp;&nbsp; <a target='_blank'  onclick=\"clickManifestLabel('"+ row.id +"', '"+ row.manifest_no +"')\"  title='Print Label'><i class='fa fa-print'></i> Label</a>"

                        // if (row.status == "delivered")
                        //     str += "&nbsp;&nbsp;&nbsp; <a target='_blank'  onclick=\"clickPOD('"+ row.id +"', '"+ row.consignment +"')\"  title='Proof Of Delivery'><i class='fa fa-picture-o'></i> POD</a>"
                        //     // str += "&nbsp;&nbsp;&nbsp; <a target='_blank'  onclick=\"$('#modalShowConnote').modal('show').find('.modal-content').load('{{ URL::to('connote') }}/"+ row.id +"/pod');\"  title='Proof Of Delivery'><i class='fa fa-picture-o'></i> POD</a>"
                        // str += '<a href="#" title="Header" data-toggle="popover" data-trigger="hover" data-content="Some content">Hover over me</a>';
                        
                        return str;
                    },
                    "targets": 5
                },
                // { "visible": false,  "targets": [ 7,8,11,14 ] }                
            ],
            
            "createdRow": function ( row, data, index ) {
                // console.log(data.status);
                if (data.status != null){
                    if ( (data.status).includes("ready to print")) {
                        $('td', row).eq(0).prepend('<i class="fa fa-print"></i> &nbsp;');
                        $('td', row).eq(0).addClass('status-ready-to-print');  
                    }else if ( (data.status).includes("submitted")) {
                        $('td', row).eq(0).prepend('<i class="fa fa-send"></i> &nbsp;');
                        $('td', row).eq(0).addClass('status-submitted');  
                    }else if ( (data.status).includes("picked-up")) {
                        $('td', row).eq(0).prepend('<i class="fa fa-truck"></i> &nbsp;');
                        $('td', row).eq(0).addClass('status-picked-up');
                    }else if ( (data.status).includes("delivered")) {
                        $('td', row).eq(0).prepend('<i class="fa fa-check-circle"></i> &nbsp;');
                        $('td', row).eq(0).addClass('status-delivered');
                    }else if ( (data.status).includes("cancelled")) {
                        $('td', row).eq(0).prepend('<i class="fa fa-times"></i> &nbsp;');
                        $('td', row).eq(0).addClass('status-cancelled');
                    }else{
                        $('td', row).eq(0).prepend('<i class="fa fa-thumbs-o-up"></i> &nbsp;');
                        $('td', row).eq(0).addClass('status-ready');
                    }
                }
            }
        });


        // myTable.ajax.reload( null, false );

        // setTimeout(function () { 
        //     $('[data-toggle="popover"]').popover();   
        // }, 500);

    });
  
</script>





@endscript

