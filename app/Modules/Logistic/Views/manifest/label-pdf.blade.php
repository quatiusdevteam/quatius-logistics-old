<html>
<head>
    <style>
        html{
            margin: 0px;
        }
    </style>
</head>
<body style="margin: 0px; font-family: Arial, Helvetica, sans-serif;">
<table style="width: 395px; font-size:12px;">
<tr><td colspan = "2" style="text-align:left;">
    <table style="width: 100%"><tr>
            <td style="width: 33%">
                <img style="height:30px;" src="data:image/png;base64,{{base64_encode(file_get_contents(config('quatius.logistic.logo-print','')))}}"/></td>
            
            <th style="width: 34%; font-size:12px;">
                Run Sheet
            </th>
            <td style="width: 33% ;text-align:right;">
                @if($manifest->courier->hasLogo())
                <img style="height:20px;" src="data:image/png;base64,{{$manifest->courier->getLogoBase64()}}"/>
                @endif
            </td>
        </tr>
    </table>
</td></tr>
<tr>
    <td style="padding: 10px 5px 0px 5px; width:60%; vertical-align: top;">
        <table style="width:100%; font-size: 10px;">
            <tr><th style="text-align:left; font-size: 12px; padding-left: 2px;">Date:</th></tr>
            <tr>
                <td style="padding: 5px;">
                    {{date('Y/m/d H:i:s',strtotime($manifest->submitted_at))}}
                </td>
            </tr>
            <tr><th style="text-align:left; font-size: 12px; padding-left: 2px;">From:</th></tr>
            <tr>
                <td style="vertical-align: top; padding: 5px; height: 50px;">
                {{$manifest->sender->first_name}} {{$manifest->sender->last_name}} {{$manifest->sender->company?", ".$manifest->sender->company:""}}<br> 
                {{$manifest->sender->address_1}}  {{$manifest->sender->address_2}}<br>
                {{$manifest->sender->city}} {{$manifest->sender->state}} {{$manifest->sender->postcode}}<br>

                {{$manifest->sender->phone_1}}
                </td>
            </tr>
        </table>   
    </td>
    <td style="width: 40%;">
       
    </td>
</tr>
<tr>
    <td colspan="2">
        <table style="width:100%; font-size: 10px;">
        <tr><th style="text-align:left; font-size: 12px;">Instruction:</th></tr>
            <tr>
                <td style="vertical-align: top; padding: 5px; height: 35px;">
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td colspan="2" style="">
    <table style="width:100%; font-size: 10px;">
        <tr>
        <th style="width:100px; text-align:left; font-size: 12px;">Manifest No: </th>
        <td>{{$manifest->manifest_no}}</td>
        </tr>
    </table>
    <table style="width:100%; font-size: 10px; border: 1px solid #000000;">
        <tr style="font-size: 12px;">
            <th style="padding: 5px; border-bottom: 1px solid #000000;">No</th>
            <th style="padding: 5px; border-bottom: 1px solid #000000;">Parcels</th>
            <th style="padding: 5px; border-bottom: 1px solid #000000;">Consignment</th>
            <th style="padding: 5px; border-bottom: 1px solid #000000;">Label</th>
        </tr>
        @php
        $counter = 0;
        $parcels = 0;
        @endphp
        @foreach($manifest->connotes as $connote)
            @php
            $counter++;
            $parcels += $connote->items;
            @endphp
        <tr>
            <td style="text-align:center;">{{$counter}} </td>
            <td style="text-align:center;">{{$connote->items}} </td>
            <td style="text-align:center;">{{$connote->consignment}}</td>
            <td style="text-align:center; padding-top: 5px;"> 
                <img style="height:20px;" src="data:image/png;base64,{{base64_encode(file_get_contents(getBarcode($connote->courier_consignment)))}}"/>
                <br/>
                {{$connote->courier_consignment}}
            </td>
        </tr>
        @endforeach            
    </table>
    </td>
</tr>
<tr>
    <td style="font-size: 10px; text-align:left; padding: 5px;">
        Parcel Total: {{$parcels}}
    </td>
    <td>
    </td>
</tr>
</table>
</body>
</html>