@includeStyle('vendors/css/print.min.css')
@includeScript('vendors/js/print.min.js')
@includeScript('vendors/js/logistic.js')

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="selectClientLabel"><i class="fa fa-file"></i> &nbsp; Manifest: {{$manifest->manifest_no}}</h4>
</div>
<div class="modal-body" style="height: 75vh; overflow:auto">
    <!-- <div class="panel panel-default">
            <div class="panel-heading"><i class="fa fa-send"></i> Sender</div>
        <div class="panel-body">
                            
            <b>{{$manifest->sender->Company?$manifest->sender->Company:''}} </b>
            <b>{{$manifest->sender->first_name}} {{$manifest->sender->last_name}}</b>  &nbsp;  &nbsp;  <a href="mailto:{{$manifest->sender->email}}"><i class="fa fa-envelope-o"></i> {{$manifest->sender->email}}</a>
            &nbsp;  &nbsp; <a href="tel:{{$manifest->sender->phone_1}}"><i class="fa fa-phone"></i> {{$manifest->sender->phone_1}}</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-info">
                <div class="panel-heading"><i class="fa fa-cube"></i>  Pickup From</div>
                <div class="panel-body">
                <b>{{$manifest->pickup->Company?$manifest->pickup->Company:''}} 
                {{$manifest->pickup->first_name}} {{$manifest->pickup->last_name}} </b><br>
                <a href="mailto:{{$manifest->pickup->email}}"><i class="fa fa-envelope-o"></i> {{$manifest->pickup->email}}</a><br>
                <a href="tel:{{$manifest->pickup->phone_1}}"><i class="fa fa-phone"></i> {{$manifest->pickup->phone_1}}</a><br><br>

                <a target="_blank" href="http://maps.google.com/maps?q={{$manifest->pickup->address_1}} {{$manifest->pickup->address_2}},{{$manifest->pickup->city}},{{$manifest->pickup->state}},{{$manifest->pickup->postcode}}" rel="nofollow ugc">
                <i class="fa fa-map-marker"></i> {{$manifest->pickup->address_1}}  {{$manifest->pickup->address_2}}<br>
                {{$manifest->pickup->city}},
                {{$manifest->pickup->state}}. {{$manifest->pickup->postcode}}</a>
                </div>
            </div>
        </div>
    </div>
    <h4 style="margin-top:0px"><i class="fa fa-cubes"></i> List of Connotes</h4> -->
    @include('Logistic::manifest.partials.listConnotes')

</div>

<div class="modal-footer">
    <?php
    $link = URL::to('manifest') ."/". $manifest->getRouteKey() ."/label-pdf";
    ?>
    <a id="btn-printPDF-manifest" href="#" target="_new" class="btn btn-primary" onclick="printPDF('{{$link}}'); return false;"><i class="fa fa-file-pdf-o"></i>&nbsp; Print Manifest Report</a>
    <a id="btn-viewPDF" href="{{$link}}" target="_new" class="btn btn-primary"><i class="fa fa-file-pdf-o"></i>&nbsp; View PDF</a>

</div>

