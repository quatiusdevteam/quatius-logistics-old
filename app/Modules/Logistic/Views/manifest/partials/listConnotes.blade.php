<link href="vendors/css/timeline-simple.css" rel="stylesheet" />

<div id="timeline_area section_padding_130">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <table id="tbl-manifest-connotes" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr class="text-center">
                        <th class="text-center">Status</th>
                        <th class="text-center">Consignment</th> 
                        <th class="text-center">Reference</th>                       
                        <th class="text-center">Receiver</th>
                        <th class="text-center">Price</th>                    
                        <th class="text-center">Created At</th>
                        <th class="text-center">Courier Consignment</th>
                    </tr>                
                </thead>
                <tbody> 
                    @foreach($manifest->connotes as $connot)
                    <tr class="text-center">
                        <td class="status-{{str_replace(' ','-',$connot->status)}} text-capitalize"> {{$connot->status}} </td>
                        <td> {{$connot->consignment}} </td>
                        <td> {{$connot->reference}} </td>
                        <td> 
                            <a tabindex="0" role="button" data-html="true" data-toggle="popover" data-trigger="focus" title="<i class='fa fa-user'></i>&nbsp; <b>{{$connot->addresses->first()->getName()}}</b>"  
                            data-content='<a href="mailto:{{$connot->addresses->first()->email}}"><i class="fa fa-envelope-o"> </i>&nbsp; {{$connot->addresses->first()->email}}</a>
                                        &nbsp;  &nbsp; <a href="tel:{{$connot->addresses->first()->phone_1}}"><i class="fa fa-phone"></i>&nbsp; {{$connot->addresses->first()->phone_1}}</a><br>
                                        <a target="_blank" href="http://maps.google.com/maps?q={{$connot->addresses->first()->address_1}} {{$connot->addresses->first()->address_2}},{{$connot->addresses->first()->city}},{{$connot->addresses->first()->state}},{{$connot->addresses->first()->postcode}}" rel="nofollow ugc">
                                            <i class="fa fa-map-marker"></i>&nbsp; {{$connot->addresses->first()->address_1}}  {{$connot->addresses->first()->address_2}}, 
                                            {{$connot->addresses->first()->city}}, {{$connot->addresses->first()->state}}. {{$connot->addresses->first()->postcode}}
                                        </a>'>
                                {{$connot->addresses->first()->getName()}}
                            </a>
                        </td>
                        <td> {{$connot->currency}} {{$connot->price}} </td>
                        <td> {{date("M j, Y, g:i a", strtotime($connot->created_at))}} </td>
                        <td> 
                            <img style="height:20px;" src="data:image/png;base64,{{base64_encode(file_get_contents(getBarcode($connot->courier_consignment)))}}"/>
                            <br/>
                            {{$connot->courier_consignment}}
                        </td>
                    </tr>
                    @endforeach             
                </tbody> 
            </table>
        </div>
    </div>
    <div class="row">&nbsp;</div>
    <div class="row">                            
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- Timeline Area-->            
            <div class="apland-timeline-area">
                <!-- Single Timeline Content-->
                <div class="single-timeline-area">
                    <div class="timeline-date wow fadeInLeft" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInLeft;">                        
                        @foreach($manifest->histories->sortByDesc('created_at') as $history)
                            @if (in_array($history->status->type, config('quatius.logistic.status.group.complete',[]) ))
                            <div class="single-timeline-content d-flex wow fadeInLeft " data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft; text-transform:capitalize;">
                                <div class="timeline-icon bg-status-{{str_replace(' ','-',$history->status->type)}}"><i class="fa fa-check" aria-hidden="true"></i></div>
                                <div class="timeline-text">
                                    <h6>{{$history->status->type}}</h6>
                                    <p>{{date("M j, Y, g:i a", strtotime($history->created_at))}}</p>
                                    <p>{{$history->comments}}</p>
                                </div>
                            </div>
                            @endif
                        @endforeach                        
                    </div>
                    <div class="row"> 
                        <div class="col-12 col-md-8 col-lg-9">
                            <div class="single-timeline-content d-flex wow fadeInLeft" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft;">
                                <div class="timeline-icon"><i class="fa fa-user" aria-hidden="true"></i></div>
                                <div class="timeline-text">
                                    <h6>Receiver(s)</h6>
                                    <p>As listed in the table above ...</p>                                    
                                </div>
                            </div>
                        </div>                            
                    </div>
                </div> 
                <div class="single-timeline-area">
                    <div class="timeline-date wow fadeInLeft" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInLeft;">                        
                        @foreach($manifest->histories->sortByDesc('created_at') as $history)
                            @if (in_array($history->status->type, config('quatius.logistic.status.group.transit',[]) ))  
                            <div class="single-timeline-content d-flex wow fadeInLeft " data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft; text-transform:capitalize;">
                                <div class="timeline-icon bg-status-{{str_replace(' ','-',$history->status->type)}}"><i class="fa fa-send" aria-hidden="true"></i></div>
                                <div class="timeline-text">
                                    <h6>{{$history->status->type}}</h6>
                                    <p>{{date("M j, Y, g:i a", strtotime($history->created_at))}}</p>
                                    <p>{{$history->comments}}</p>
                                </div>
                            </div>
                            @endif
                        @endforeach                        
                    </div>
                    <div class="row"> 
                        <div class="col-12 col-md-8 col-lg-9">
                            <div class="single-timeline-content d-flex wow fadeInLeft" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft;">
                                <div class="timeline-icon"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                                <div class="timeline-text">
                                    <h6>Pick Up</h6>
                                    <p>{{$manifest->pickup->Company?$manifest->pickup->Company:''}}
                                    {{$manifest->pickup->first_name}} {{$manifest->pickup->last_name}}
                                    </p>
                                    <p>
                                        <a href="mailto:{{$manifest->pickup->email}}"><i class="fa fa-envelope-o"> </i>&nbsp; {{$manifest->pickup->email}}</a>
                                        &nbsp;  &nbsp; <a href="tel:{{$manifest->pickup->phone_1}}"><i class="fa fa-phone"></i>&nbsp; {{$manifest->pickup->phone_1}}</a><br>
                                        <a target="_blank" href="http://maps.google.com/maps?q={{$manifest->pickup->address_1}} {{$manifest->pickup->address_2}},{{$manifest->pickup->city}},{{$manifest->pickup->state}},{{$manifest->pickup->postcode}}" rel="nofollow ugc">
                                            <i class="fa fa-map-marker"></i>&nbsp; {{$manifest->pickup->address_1}}  {{$manifest->pickup->address_2}}, 
                                            {{$manifest->pickup->city}}, {{$manifest->pickup->state}}. {{$manifest->pickup->postcode}}
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>                            
                    </div>
                </div> 
                <div class="single-timeline-area">
                    <div class="timeline-date wow fadeInLeft" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInLeft;">                        
                        @foreach($manifest->histories->sortByDesc('created_at') as $history)
                            @if (in_array($history->status->type, ["submitted"]))
                            <div class="single-timeline-content d-flex wow fadeInLeft " data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft; text-transform:capitalize;">
                                <div class="timeline-icon bg-status-{{str_replace(' ','-',$history->status->type)}}"><i class="fa fa-send" aria-hidden="true"></i></div>
                                <div class="timeline-text">
                                    <h6>{{$history->status->type}}</h6>
                                    <p>{{date("M j, Y, g:i a", strtotime($history->created_at))}}</p>
                                    <p>{{$history->comments}}</p>
                                </div>
                            </div>
                            @endif
                        @endforeach                        
                    </div>
                    <div class="row"> 
                        <div class="col-12 col-md-8 col-lg-9">
                            <div class="single-timeline-content d-flex wow fadeInLeft" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft;">
                                <div class="timeline-icon"><i class="fa fa-user" aria-hidden="true"></i></div>
                                <div class="timeline-text">
                                    <h6>Sender</h6>
                                    <p>{{$manifest->sender->Company?$manifest->sender->Company:''}}
                                    {{$manifest->sender->first_name}} {{$manifest->sender->last_name}}
                                    </p>
                                    <p>
                                        <a href="mailto:{{$manifest->sender->email}}"><i class="fa fa-envelope-o"> </i>&nbsp; {{$manifest->sender->email}}</a>
                                        &nbsp;  &nbsp; <a href="tel:{{$manifest->sender->phone_1}}"><i class="fa fa-phone"></i>&nbsp; {{$manifest->sender->phone_1}}</a><br>
                                        <a target="_blank" href="http://maps.google.com/maps?q={{$manifest->sender->address_1}} {{$manifest->sender->address_2}},{{$manifest->sender->city}},{{$manifest->sender->state}},{{$manifest->sender->postcode}}" rel="nofollow ugc">
                                            <i class="fa fa-map-marker"></i>&nbsp; {{$manifest->sender->address_1}}  {{$manifest->sender->address_2}}, 
                                            {{$manifest->sender->city}}, {{$manifest->sender->state}}. {{$manifest->sender->postcode}}
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>                            
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover({
        container: 'body'
    });   
});
</script>


<style>
.popover{
    max-width: 100%; /* Max Width of the popover (depending on the container!) */
}
.timeline_area {
    position: relative;
    z-index: 1;
}
.single-timeline-area {
    position: relative;
    z-index: 1;
    padding-left: 380px;
}
@media only screen and (max-width: 575px) {
    .single-timeline-area {
        padding-left: 100px;
    }
}
.single-timeline-area .timeline-date {
    position: absolute;
    width: 380px;
    height: 100%;
    top: 0;
    left: 0;
    z-index: 1;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    -ms-grid-row-align: center;
    align-items: center;
    -webkit-box-pack: end;
    -ms-flex-pack: end;
    justify-content: flex-end;
    padding-right: 60px;
}
@media only screen and (max-width: 575px) {
    .single-timeline-area .timeline-date {
        width: 100px;
    }
}
.single-timeline-area .timeline-date::after {
    position: absolute;
    width: 3px;
    height: 100%;
    content: "";
    background-color: #ebebeb;
    top: 0;
    right: 30px;
    z-index: 1;
}
.single-timeline-area .timeline-date::before {
    position: absolute;
    width: 11px;
    height: 11px;
    border-radius: 50%;
    background-color: #c4c4c4;
    content: "";
    top: 50%;
    right: 26px;
    z-index: 5;
    margin-top: -5.5px;
}
.single-timeline-area .timeline-date p {
    margin-bottom: 0;
    color: #020710;
    font-size: 1em;
    text-transform: uppercase;
    /* font-weight: 500; */
}
.single-timeline-area .single-timeline-content {
    position: relative;
    z-index: 1;
    padding: 0.5em 1em;
    /* padding: 30px 30px 25px; */
    min-width: 150px;
    border-radius: 6px;
    margin-bottom: 15px;
    margin-top: 15px;
    -webkit-box-shadow: 0 0.25rem 1rem 0 rgba(47, 91, 234, 0.125);
    box-shadow: 0 0.25rem 1rem 0 rgba(47, 91, 234, 0.125);
    border: 1px solid #ebebeb;
}
@media only screen and (max-width: 575px) {
    .single-timeline-area .single-timeline-content {
        padding: 20px;
    }
}
.single-timeline-area .single-timeline-content .timeline-icon {
    -webkit-transition-duration: 500ms;
    transition-duration: 500ms;
    width: 30px;
    height: 30px;
    background-color: #c4c4c4;
    -webkit-box-flex: 0;
    -ms-flex: 0 0 30px;
    flex: 0 0 30px;
    text-align: center;
    max-width: 30px;
    border-radius: 50%;
    margin-right: 15px;
}
.single-timeline-area .single-timeline-content .timeline-icon i {
    color: #ffffff;
    line-height: 30px;
}
.single-timeline-area .single-timeline-content .timeline-text h6 {
    -webkit-transition-duration: 500ms;
    transition-duration: 500ms;
}
.single-timeline-area .single-timeline-content .timeline-text p {
    font-size: 1em;
    margin-bottom: 0;
}
.single-timeline-area .single-timeline-content:hover .timeline-icon,
.single-timeline-area .single-timeline-content:focus .timeline-icon {
    background-color: #FFCB08;
}
.single-timeline-area .single-timeline-content:hover .timeline-text h6,
.single-timeline-area .single-timeline-content:focus .timeline-text h6 {
    color: #3f43fd;
}
</style>

