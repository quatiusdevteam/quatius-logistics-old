<?php
/**
 * Created by PhpStorm.
 * User: vey.chea
 * Date: 28/09/2017
 * Time: 3:50 PM
 */

namespace App\Modules\Pronto\Models;

use DB;
use App\Modules\Supplier\Models\QLRate;
use App\Modules\Supplier\Models\Supplier;

class LoadQLRate
{
    function __construct(){

    }

    /**
     * @param $rate_file
     * @return string
     */
    public function import($file){
        if(!is_file($file)) {
            return 'CSV File is not valid';
        }
        $importer = new CsvImporter($file);
        self::clearTable();
        $num_row = 0;
        while($data_row = $importer->get(500)){
            foreach ($data_row as $row)
            {
                if(strtolower($row[0]) == "supplier") continue;
                self::loadQLRateRow($row);
                $num_row++;
            }
        }
        return $num_row . " have been imported";
    }

    /**
     * @param array $row
     */
    private function loadQLRateRow($row=[]){
        $supplier_name = trim($row[0]);
        $rate_data= [
            'from_zone' => trim(strtoupper($row[1])),
            'to_zone'   => trim(strtoupper($row[2])),
            'standard'  => floatval($row[3]),
            'os'        => floatval($row[4]),
            'xl'        => floatval($row[5]),
            'oh'        => floatval($row[6]),
            'dp'        => floatval($row[7]),
        ];
        $supplier = Supplier::whereName($supplier_name)->first();
        if($supplier) {
            $supplier->qlRates()->save(new QLRate($rate_data));
        }
    }

    private function clearTable(){
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::statement('TRUNCATE ql_rates;');
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}