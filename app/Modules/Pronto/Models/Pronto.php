<?php
namespace App\Modules\Pronto\Models;

use GuzzleHttp\json_encode;
use App\Modules\Product\Models\Product;
use Cache;
use App\Modules\Product\Models\Warehouse;
use DB;
use App\Modules\Product\Models\Dictionary;
use App\Modules\Product\Models\Category;
use App;
use App\Modules\Account\Models\ShopperGroup;
use App\Modules\Product\Models\ProductPrice;
use Lavalite\Settings\Models\Setting;

class Pronto{
	private $proRepo = null;
	private $styles = null;
	private $colours = null;
	private $sizes = null;
	private $warehouse = null;
	private $rootCate = null;
	private $specIds = null;
	public $message = "";
	
	public function __construct()
	{
		$this->warehouse = Warehouse::all()->first();
		$this->styles = Dictionary::group("style");
		$this->colours = Dictionary::group("colour");
		$this->sizes = Dictionary::group("size");
		
		$this->proRepo = App::make('App\Modules\Product\Repositories\ProductRepository');
		$this->rootCate = Category::where('name','shoes')->first();
		$this->specIds = $this->rootCate->specifications()->pluck("id")->toArray();
		$this->guestAndRegisterShoppers = ShopperGroup::whereIn('default',[1,2])->pluck('id')->toArray();
	}
	
	public function loadStockExport($stockPath=null)
	{
		$this->message = 'Executing or has failed';
		$setting = Setting::whereSkey('Pronto')->whereName("sync_message")->first();
		if ($setting){
			$setting->value = $this->message;
			$setting->save();
		}else {
			Setting::create(['skey'=>'Pronto', 'name'=>'sync_message', 'value'=>$this->message]);
		}
		
		if ($stockPath == null)
			$stockPath = config('pronto.stock_sync_file');
		
		if (!file_exists($stockPath))
			return false;
		
		$timeStart = microtime(true);
		
		$importer = new CsvImporter($stockPath,true);
		
		$this->initProductsData();
		
		set_time_limit(config('pronto.max_time_limit', 360)); // 1 hours
		ini_set('memory_limit', config('pronto.memory_limit', '512M'));
		
		$loadCsv = 0;
		while($dataRows = $importer->get(500))
		{
			foreach ($dataRows as $row)
			{
				$this->loadProductRow($row);
				$loadCsv++;
			}
		}
		$this->message = "$loadCsv Products loaded,";
		
		$diff = microtime(true) - $timeStart;
		$sec = intval($diff);
		$micro = $diff - $sec;
		$this->message .= " Execute Time: " . round($micro * 1000, 4) . " ms";
			
		$setting = Setting::whereSkey('Pronto')->whereName("sync_message")->first();
		if ($setting){
			$setting->value = $this->message;
			$setting->save();
		}else {
			Setting::create(['skey'=>'Pronto', 'type'=>'System', 'name'=>'sync_message', 'value'=>$this->message]);
		}
		
		return true;
	}
	
	public function clearProductsData(){
		Cache::flush();
		
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		
		DB::statement('TRUNCATE product_specification;');
		DB::statement('TRUNCATE category_product;');
		DB::statement('TRUNCATE product_warehouse;');
		
		DB::delete('DELETE FROM assignables WHERE assignable_type=\'App\\\\Modules\\\\Product\\\\Models\\\\Product\' ;');
		DB::delete('DELETE FROM assignables WHERE assignable_type=\'App\\\\Modules\\\\Product\\\\Models\\\\ProductPrice\' ;');
		
		ProductPrice::truncate();
		Product::truncate();
		
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
		
	}
	
	private function initProductsData()
	{
		Product::whereStatus(2)->update(array('status' => 1));
	
		DB::table('product_warehouse')->update([
		'stock'=>0,
		'low_stock'=>0,
		'reserve'=>0,
		'ordered'=>0
		]);
	
		Cache::flush();
		
		DB::delete('DELETE FROM assignables WHERE assignable_type=\'App\\\\Modules\\\\Product\\\\Models\\\\ProductPrice\' ;');
		
		ProductPrice::truncate();
	}
	
	public function loadProductRow($datas = [])
	{
		// update Date Dictionaries
		$style = substr($datas['Item'], 0, 6);
		$colour = substr($datas['Item'], 6, 4);
		$size = substr($datas['Item'], -3);
		
		$name = $datas['Style Desc']." ".$datas['Colour Desc'];
		
		$prodData = [
			'name' => $name,
			'description' => $datas['Item Desc'],
			'slug' => str_replace(['/',' '],'-',strtolower($name)),
			'sku' => $datas['Item'],
			'barcode' => $datas['Item'],
			'status' => 2,
		];
		
		$product = Product::whereSku($datas['Item'])->first();
		
		if ($product) {
			$product->update($prodData);
		}
		else {
			$product = Product::create($prodData);
			$this->proRepo->setCategoryToProduct($this->rootCate->id,$product->id);
		}
		
		
		if (!isset($this->styles[$style])){
			Dictionary::create(['group'=>'style', 'value'=>$style, 'label'=>$datas['Style Desc'], 'ordering'=>count($this->styles)]);
			$this->styles[$style] = $datas['Style Desc'];
		}
		
		if (!isset($this->colours[$colour])){
			Dictionary::create(['group'=>'colour', 'value'=>$colour, 'label'=>$datas['Colour Desc'] , 'ordering'=>count($this->colours)]);
			$this->colours[$colour] = $datas['Colour Desc'];
		}
		
		if (!isset($this->sizes[$size])){
			$sizeLabel = trim(substr($datas['Item Desc'],strrpos($datas['Item Desc'], ' ')));
			Dictionary::create(['group'=>'size', 'value'=>$size, 'label'=>$sizeLabel, 'ordering'=>count($this->sizes)]);
			$this->sizes[$sizes] = $sizeLabel;
		}
		
		
		//update Specifications
		$values = [$style,$colour,$size];
		foreach($this->specIds as $index => $id){
			$product->specifications()->updateExistingPivot($id,['value'=>$values[$index]]);
		}
		
		
		//update Shopper Group
		$product->shoppers()->sync($this->guestAndRegisterShoppers);
		
		
		//update warehouse
		$product->warehouses()->sync([$this->warehouse->id=>['stock'=>$datas['Avail Qty']]]);
		
		
		//update Shopper Group
		$price = new ProductPrice(["price"=>$datas['Item RRP Ex Tax']]);
		$product->prices()->save($price);
		$price->shoppers()->sync($this->guestAndRegisterShoppers);
		
	}
}
