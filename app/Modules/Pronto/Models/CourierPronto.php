<?php

namespace App\Modules\Pronto\Models;

use Exception;
use Quatius\Logistic\Models\Courier;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Quatius\Framework\Models\CsvImporter;

use function GuzzleHttp\json_decode;

class CourierPronto extends Courier{

    private $minSizeCm = 5;
    private $maxSizeCm = 100;
    private $minWeightKg = 0.01;
    private $maxWeightKg = 22;

    protected $table = "couriers";

    protected static $singleTableType = 'pronto';
    
    public function apiEncryptRaw($method, $body, $time=null){
        if(!is_string($body))
            $body = json_encode($body);
        
        $time = $time?:time();
        //plaintext = "app_key" + environment.APP_KEY + "format" + format + "method" + method + "timestamp
        return "app_key" . $this->getParams("crediental.APP_KEY","") . "format" . $this->getParams("crediental.format","json") . "method" . $method . "timestamp" . $time . "v" . $this->getParams("crediental.version","1.0.1") . $body . $this->getParams("crediental.APP_SECRET","");
    }

    public function apiSignBody($body){
        return MD5($body);
    }
    
    public function apiPostUrl($method, $sign, $time=null){
        ////?method={{METHOD}}&app_key={{APP_KEY}}&v={{version}}&timestamp={{timestamp}}&format={{format}}&sign={{sign}}
        return $this->getParams("api.url","http://open.au.4px.com/router/api/service")."?method=$method&app_key=".$this->getParams("crediental.APP_KEY","")."&v=".$this->getParams("crediental.version","1.0.1")."&timestamp=$time&format=".$this->getParams("crediental.format","json")."&sign=$sign";
    }

    public function apiPostRequest($method, $data){
        if (!$this->getParams("crediental.APP_KEY","") || !$this->getParams("crediental.APP_SECRET",""))
            throw new \Exception("4px Courier API KEY Required");

        $timestamp= time();
        $plaintext = $this->apiEncryptRaw($method,$data, $timestamp);
        $urlQuery = $this->apiPostUrl($method, $this->apiSignBody($plaintext), $timestamp);

        $response = $this->getHttpClient()->request('POST', $urlQuery, ['body' => json_encode($data)]);
        $content = "";
        if($response->getStatusCode()==200){
            $content = $response->getBody()->getContents();
            if ($this->getParams('api.log.success',false))
                app('log')->info("courier api(".$response->getStatusCode()."): ".$response->getReasonPhrase(), [self::$singleTableType, $method, $data, $content]);
        }else{
            if ($this->getParams('api.log.failure',true))
                app('log')->error("courier api(".$response->getStatusCode()."): ".$response->getReasonPhrase(), [self::$singleTableType, $method, $data]);
        }
        return $content;
    }
    
    public function updateRemoteTracking($connote){
        $postData = ["deliveryOrderNo"=>$connote->courier_consignment];
        $data = $this->apiPostRequest("tr.order.tracking.get", $postData);
        $dataArr = json_decode($data, true);
        $consignment = Arr::get($dataArr, "data.deliveryOrderNo","");
        
        if (!$consignment)
            throw new \Exception($connote->courier_consignment. " - Record not found on 4PX Server");

        if ($consignment != $connote->courier_consignment)
            return;

        $tracking = $this->tranformAConnoteTracking(Arr::get($dataArr, "data.trackingList",[]), $connote);
        if ($tracking){
            foreach($tracking as $histdata){
                $connote->setStatus($histdata['status'], $histdata);
            }
        }
    }

    public function prepAddress($address){
        $countryInfo = country($address->country);
        $division = $countryInfo->getDivision($address->state);
        $addressData = [
            'first_name' => $address->first_name, //String(32)	Yes		
            'phone' => $address->phone_1, //	String(32)	Yes	
            'post_code' => $address->postcode, //	String(32)	No	
            'country' => $countryInfo->getName(), //	String(10)	Yes		
            'state' => $division && isset($division['name'])? $division['name'] : $address->state, //	String(64)	Yes		
            'city' => $address->city, //	String(64)	Yes		
            'district' => '', //	String(64)	Yes		
            'street' => $address->address_1, //	String(64)	Yes		
        ];
        
        if ($address->last_name)
            $addressData['last_name'] = $address->last_name; // String(32)	No		
        if ($address->company)
            $addressData['company'] = $address->company; //	String(64)	No
        if ($address->phone_2)
            $addressData['Phone2'] = $address->phone_2; //	String(32)	No		
        if ($address->email)
            $addressData['email'] = $address->email; //	String(32)	No		
        if ($address->address_2)
            $addressData['street'] =  $addressData['street'].' '.$address->address_2;

        return $addressData;
    }

    public function prepCreate($connote){
        //http://open.au.4px.com/apiInfo/apiDetail?itemId=9&mainId=45
        //ds.pds.order.create
        return [
            '4px_tracking_no' => $connote->courier_consignment,
            'ref_no' => $connote->reference,

                // Service Type，Internal 4PX scheduling requirements, if any, will be indicated. Default: LOCAL
            'business_type' => 'LOCAL',
                // Types of Goods (1-goods；2-document；3-products；4-others)，default：1-others
            'cargo_type' => '1',
            'logistics_service_info' => ['logistics_product_code'=>''], //??
            'return_info' =>['is_return_on_domestic' => 'N'],
            'parcel_list'=>[
                'weight' => intval($connote->weight * 1000), // grams
                'length' => $connote->length, // (cm)
                'width' => $connote->width, // (cm)
                'height' => $connote->height, // (cm),
                'parcel_value' => $connote->declared_value,
                'currency' =>$connote->currency,
            ],
            'is_insure'=>'N',
            'sender'=> $this->prepAddress($connote->manifest->sender),
            'recipient_info'=> $this->prepAddress($connote->receiver),
            'deliver_type_info'=>[
                // way to 4PX warehouse (pick up: 1, courier to 4PX Warehouse: 2, express to 4px warehouse: 3，courier to 4PX station：5)
                'deliver_type' => '1', 
                'pick_up_info' => [
                    'pick_up_address_info'=> $this->prepAddress($connote->manifest->pickup),
                ]
            ],
            'deliver_to_recipient_info'=>[
                // Delivery type: HOME_DELIVERY- delivery to the door;SELF_PICKUP_STATION- self-lifting point;SELF_SERVICE_STATION- self lifting counter (self-service point) default: HOME_DELIVERY
                'deliver_type' => 'HOME_DELIVERY'
            ]
        ];
    }

    public function prepForecastAddress($address, $prefix = ''){
        if (!$address->country)
            throw new Exception("No Address assigned");

        $countryInfo = country($address->country);
        $division = $countryInfo->getDivision($address->state);
        $addressData = [
            'firstName' => $address->first_name, //String(128)	Yes		
            'phone1' => $address->phone_1, //	String(40)	Yes	
            'postCode' => $address->postcode, //	String(32)	No	
            'country' => $countryInfo->getName(), //	String(64)	Yes		
            'province' => $division && isset($division['name'])? $division['name'] : $address->state, //	String(64)	Yes		
            'suburb' => $address->city, //	String(64)	Yes
            'address1' => $address->address_1, //	String(256)	Yes		
        ];
        
        if ($address->company)
            $addressData['company'] = $address->company; //	String(64)	No
        if ($address->phone_2)
            $addressData['phone2'] = $address->phone_2; //	String(32)	No		
        if ($address->email)
            $addressData['email'] = $address->email; //	String(32)	No		
        if ($address->address_2)
            $addressData['address2'] = $address->address_2;

        if ($prefix){
            $addressPrefix = [];
            foreach ($addressData as $key => $value){
                $addressPrefix[$prefix.ucfirst($key)] = $value;
            }
            return $addressPrefix;
        }
        return $addressData;
    }

    public function prepForecastConnotes($connotes, $pickup, $sender){
        $datas = [
            'pickupType'=>'HOME_PICKUP',// HOME_PICKUP, CUSTOMER_TO_WAREHOUSE
            'parcelAddVoList' => [],
            'pickupAddress'=>$this->prepForecastAddress($pickup),
            'senderAddress'=>$this->prepForecastAddress($sender)
        ];

        foreach ($connotes as $connote){
            $data = $this->prepForecastAddress($connote->receiver, 'receiver');

            $data['weight'] = number_format($connote->weight, 2); // kg must be accurate to 2 decimals
            $data['length'] = $connote->length; // (cm)
            $data['width'] = $connote->width; // (cm)
            $data['height'] = $connote->height; // (cm),

            $data['refNo'] = $connote->reference;
            $data['remark'] = $connote->instruction;
            $data['parcelType'] = $connote->parcel_type;
            $data['declaredValue'] = $connote->declared_value;
            $data['atlTag'] = $connote->atl?'Y':'N';
            $datas['parcelAddVoList'][] = $data;
        }
        return $datas;
    }

    public function prepForecastManifest($menifest){
        //http://open.au.4px.com/apiInfo/apiDetail?itemId=9&mainId=45
        //gos.api.forecastOrder
        return $this->prepForecastConnotes($menifest->connotes, $menifest->pickup, $menifest->sender);
    }

    public function apiCreateConnote($connote){
        $postData = $this->prepCreate($connote);

        $dataResponse = $this->apiPostRequest("ds.pds.order.create", $postData);
        return $dataResponse;
        //$dataArr = json_decode($dataResponse, true);

    }

    public function initalise($connote){
        parent::initalise($connote);
    }
    
    public function getMSGTransCode($tracking, $connote){
        
        $code = Arr::get($tracking, "businessLinkCode","");
        $commentMsg = Arr::get($tracking, "trackingContent","");

        $codeNew = trans('Pronto::msg-map.'.$code,[],'en');
        $comment = strtolower($commentMsg);
        $value=null;
        if ( $comment == "arrived at facility"){
            $codeNew = 'MSG_F_AF';
        }
        elseif ($comment == "transferred"){
            $codeNew = 'MSG_O_RR';
        }elseif ($comment == "in transit to next facility"){
            $codeNew = 'MSG_D_TNF';
        }elseif ($comment == "order has been created."){
            $codeNew = 'MSG_L_RPIF';
        }elseif ($comment == "order has been cancelled."){
            $codeNew = 'MSG_O_SC';
        }elseif ($code=="FPX_O_IR" && Str::startsWith($comment, "manifest accepted by ")){
            $codeNew=="MSG_O_IRS";
            $value = substr($commentMsg, strlen("manifest accepted by "));
        }elseif ($comment == "redirected to new address"){
            $codeNew = 'MSG_M_RNA';
        }elseif ($code=="FPX_S_OK" && Str::startsWith($comment, "delivered with signature from ")){
            $codeNew = 'MSG_S_OKSR';
            $value = substr($commentMsg, strlen("delivered with signature from "));
        }elseif($codeNew == 'Pronto::msg-map.'.$code){ //unknown
            if (Str::startsWith($comment, 'your parcel has arrived at our ')){
                $codeNew = 'MSG_C_AAL';
                $value = substr($commentMsg, strlen("your parcel has arrived at our "));
                $value = ucwords(Trim(Str_replace("4PX-","",$value)));
            }elseif (Str::startsWith($comment, 'awaiting collection at ')){
                $codeNew = 'MSG_D_FAD';
                $value = substr($commentMsg, strlen("awaiting collection at "));
            }
        }

        $code = $this->checkMsgCode($codeNew);
        
        return [
            "occur_at"=> ucwords(Trim(Str_replace("4PX-","",Arr::get($tracking, "occurLocation","")))),
            "created_at"=> str_replace(": ",":",Arr::get($tracking, "occurDatetime","")),
            "comments"=> $code!='MSG_M_OTH'?$code:Arr::get($tracking, "trackingContent",""),
            "status"=> $this->getMSGStatus($code),
            "value"=>$value,
            "data"=>Arr::only($tracking,["businessLinkCode","trackingContent"])
        ];
    }

    public function getItemCost($connote){
        return 0.0;
    }

    public function validate($connotes, $datas){
        $isSuccess = parent::validate($connotes, $datas);
        foreach($connotes as $connote){
            $inputs = [];
            $remark = "";
            if ($connote->width < $this->minSizeCm )
                $inputs["width"] = $remark = "Size too small, minimum of ". $this->minSizeCm ."cm";

            if ($connote->width > $this->maxSizeCm)
                $inputs["width"] = $remark = "Size too large, maximun of ". $this->maxSizeCm ."cm";

            if ($connote->height < $this->minSizeCm)
                $inputs["height"] = $remark = "Size too small, minimum of ". $this->minSizeCm ."cm";

            if ($connote->height > $this->maxSizeCm)
                $inputs["height"] = $remark = "Size too large, maximun of ". $this->maxSizeCm ."cm";
            
            if ($connote->length < $this->minSizeCm)
                $inputs["length"] = $remark = "Size too small, minimum of ". $this->minSizeCm ."cm";

            if ($connote->length > $this->maxSizeCm)
                $inputs["length"] = $remark = "Size too large, maximun of ". $this->maxSizeCm ."cm";

            if ($connote->weight < $this->minWeightKg)
                $inputs["weight"] = $remark = "Weight too light, minimum of ". $this->minWeightKg ."kg";
            
            if ($connote->weight > $this->maxWeightKg)
                $inputs["weight"] = $remark = "Weight too heavy, maximun of ". $this->maxWeightKg ."kg";
            
            if (!$connote->reference)
                $inputs["reference"] = $remark = "Shipment Reference is Blank";
                

            if ($remark){
                $connote->mergeParams("error.inputs", $inputs);
                $connote->remark = $remark;//= $connote->reference?"Logistic::connote.PARCEL-TOO-SMALL":"Logistic::connote.PARCEL-INVALID";
                $connote->save();
                $isSuccess = false;
            }
        }
        return $isSuccess;
    }

    public function getConsigmentNo($connote){
        $current = $this->getParams('consignment.current', $this->getParams('consignment.init', 0));
        $current++;
        $this->setParams('consignment.current', $current);
        return $current."";
    }

    
    public function createConnotes($connotes, $pickup=null, $sender=null, $historyData=[]){
        if (!$connotes->count())
            return;

        $connotes = $connotes->values();

        $data = $this->prepForecastConnotes($connotes, $pickup?:$connotes->first()->pickup, $sender?:$connotes->first()->sender);
        
        if ($this->getParams('api.simulate', true)){
            $content = File::get(base_path('tests/Support/stubs/4px/response-gos.api.forecastOrder.json'));
            $jsonData = json_decode($content, true);
            $jsonData['data']['fpxTrackNos'] = [];
            foreach($connotes as $connote){
                $jsonData['data']['fpxTrackNos'][]="".$this->getConsigmentNo($connote);
            }
            
            $this->save();
            $this->setSimulateResponse(json_encode($jsonData));
        }

        $dataResponse = $this->apiPostRequest("gos.api.forecastOrder", $data);
        $jsonData = json_decode($dataResponse, true);

        if (Arr::get($jsonData, "result", '0') == '1'){
            foreach (Arr::get($jsonData, "data.fpxTrackNos", []) as $index=>$conNum){
                $connote = $connotes->get($index);
                $connote->courier_consignment  =  $conNum;
                $connote->save();
                $connote->setStatus(Courier::$PRINT_LABEL, $historyData);
            }
        }else{
            $repo = app('connote');

            foreach ($connotes as $connote){
                $repo->bookingRollback($connote, "", ['comments'=>Arr::get($jsonData, "msg", '')?:'Input Errors']);
            }
        }
    }

    /*
    public function createManifest($manifest, $historyData=[]){
        
        if ($this->getParams('api.enable', false)){
            
            $data = $this->prepForecastManifest($manifest);
        
            $dataResponse = $this->apiPostRequest("gos.api.forecastOrder", $data);
            $jsonData = json_decode($dataResponse, true);
            
            $manifest->setParams('data.create',$jsonData);
            $manifest->save();

            if (Arr::get($jsonData, "result", '0') == '1'){
                foreach (Arr::get($jsonData, "data.fpxTrackNos", []) as $index=>$conNum){
                    $connote = $manifest->connotes->get($index);
                    $connote->courier_consignment  =  $conNum;
                    $connote->save();
                    $connote->setStatus(Courier::$PRINT_LABEL, $historyData);
                }
            }else{
                $repo = app('connote');

                foreach ($manifest->connotes as $connote){
                    $repo->bookingRollback($connote, "", ['comments'=>Arr::get($jsonData, "msg", '')?:'Input Errors']);
                }
            }
            return;
        }

        foreach ($manifest->connotes as $connote){
            $connote->courier_consignment = $this->getConsigmentNo($connote);
            $connote->setStatus(Courier::$PRINT_LABEL, $historyData);
        }
        
        $this->save();
        
    }
    */

    public function submitManifest($manifest){
        if (!$this->getParams('api.enable', false)) return;

        $connoteNos= $manifest->connotes->pluck('courier_consignment')->toArray();
        $dataResponse = $this->apiPostRequest("gos.api.submitOrder", ["fpxTrackNos"=>$connoteNos]);
        $jsonData = json_decode($dataResponse, true);
        $manifest->setParams('data.submit',$jsonData);
        $manifest->save();
        if (Arr::get($jsonData, "result", '0') == '1'){
            foreach ($manifest->connotes as $connote){
                $connote->setStatus(Courier::$ACCEPTED);
            }
            // foreach (Arr::get($jsonData, "data.consignmentNoList", []) as $index=>$conNum){
            //     $connote = $manifest->connotes->where('courier_consignment', $conNum)->first();
            //     $connote->setStatus(Courier::$ACCEPTED);
            // }
            // foreach (Arr::get($jsonData, "data.failFpxNoList", []) as $index=>$conNum){
            //     $connote = $manifest->connotes->where('courier_consignment', $conNum)->first();
            //     $errorMsg = Arr::get($jsonData, "errors.$index.error_msg", '');
            //     $errorData = Arr::get($jsonData, "errors.$index",[]);

            //     $status = app('connote')->bookingRollback($connote, "", ['comments'=>$errorMsg])?Courier::$INPUT_ERROR:Courier::$DELIVERY_ERROR;
                
            //     $connote->setStatus($status,['comments'=>$errorMsg, 'data'=> $errorData]);
                
            // }
        }
        else{
            foreach ($manifest->connotes as $connote){
                $connote->setStatus(Courier::$INPUT_ERROR);
            }
        }
        
            
    }

    public function cancelConnote($connote){
        if (!$this->getParams('api.enable', false) || !$connote->courier_consignment) return true;

        if ($this->getParams('api.simulate', true)){
            return true;
        }

        $dataResponse = $this->apiPostRequest("gos.api.cancelOrder", ["fpxTrackNo"=>$connote->courier_consignment]);
        $jsonData = json_decode($dataResponse, true);
        if (Arr::get($jsonData, "result", '0') == '1'){
            return true;
        }
        return false;
    }

    public function importConnoteStatusMap(){
        return [
            "consignment"=>"Connote",
            "status"=>"Status",
            "comments"=>"Comments",
            "date"=>"Date/Time",
            "timezone"=>"Timezone",
        ];
    }

    public function setConnoteStatus($connote, $status, $data=[]){
        if (isset($data["created_at"])){//"27-APR-2022 12:36:56"
            $data["created_at"] =  date('Y-m-d H:i:s', strtotime($data["created_at"]));
        }

        if (strtolower($status) == 'error'){
            // An error was detected in the file during connote creation or 4PX retrieval and the file could not be processed 
            // The description will list the details of the error associated with the connote
            
            $status = app('connote')->bookingRollback($connote, "", $data)?Courier::$INPUT_ERROR:Courier::$DELIVERY_ERROR;
            
            // if (strtolower($data['comments']) == "shipment reference is blank"){
            //     $connote->mergeParams("error.inputs", ["reference"=>$data['comments']]);
            //     $status = Courier::$INPUT_ERROR;
            //     
            // }
        }
        elseif (strtolower($status) == 'accepted'){
            // Connote has been processed by 4PX and is confirmed for delivery by them
            // This notification will be sent once we have received confirmation from 4PX that the connote has been successfully processed on their end
            $status = Courier::$ACCEPTED;
        }
        elseif (strtolower($status) == 'delivered'){
            // Connote has been delivered by 4PX
            // Description will list the tracking link for the connote
            //$data['value'] = $data['comments'];
            //$data['comments'] = "";
            $status = Courier::$DELIVERED;
        }
        else{
            $status = Courier::$COURIER_ERROR;
        }
        
        parent::setConnoteStatus($connote, strtolower($status), $data);
    }

    public function import($action, $filename , $request){
        if ($action == "carriers-rates"){
            $file = requestFile($request, 'local', $filename, 'logistic/imports/');
            if($file)
                return $this->updateRates($file);
        }
        elseif($action == "accounts-update"){
            $file = requestFile($request, 'local', $filename, 'logistic/imports/');
            if($file)
                return $this->updateAccount($file);
        }
        elseif($action == "connote-status"){
            $file = requestFile($request, 'local', $filename, 'logistic/imports/');

            file_put_contents(storage_path("app/logistic/imports/".time().'_'.$filename), json_encode($request->all()));
            return true;   
        }
        return parent::import($action, $filename , $request);;
    }

    private function updateRates($file){
        $impCsv = new CsvImporter($file, true);

		while($dataRows = $impCsv->get(500))
		{
			foreach ($dataRows as $row){

            }
        }

        return true;
    }

    private function updateAccount($file){
        $impCsv = new CsvImporter($file, true);
        $accounts = app('account')->get()->keyBy('account_code');
		while($dataRows = $impCsv->get(500))
		{
			foreach ($dataRows as $row){
                $account = $accounts->get($row['Account']);
                if ($account){
                    $account->setParams('pronto', $row);
                    $account->credit_limit =  preg_replace( '/[^0-9.\-]/', '', $account->getParams('pronto.Credit Limit',0));
                    $account->balance =  preg_replace( '/[^0-9.\-]/', '', $account->getParams('pronto.Balance',0));
                    $account->name = $account->getParams('pronto.Name', $account->name);
                    $account->save();
                }
            }
        }

        return true;
    }
    
    public function export($action, $file="", $ext="csv"){
        if ($action == "connote"){
            return $this->exportConnotes(["courier_consignment"=>$file],  
                function($connote, $connoteData){
                    $accountCode = "";
                    if ($connote){ // need to check if headers.
                        $account = $connote->manifest->account;
                        $accountCode = $account->getParams('logistic.carrier.pronto.account', $account->account_code);
                        $connoteData["customCode"] = $accountCode?:"";
                        $connoteData["consignment"] = $connote->courier_consignment;
                        $connote->update(['is_exported'=> 1]);
                        if (!$this->getParams('api.enable', false)){
                            $connote->setStatus(Courier::$PENDING);
                            
                            \Quatius\Logistic\Jobs\JobPullRemoteTracking::dispatch($connote)->delay(now()->addMinutes(5));
                        }
                    }else{
                        $connoteData["customCode"] = "";
                        $connoteData["consignment"] = "";
                    }
                    return $connoteData;
                }
            );
        }
        else if ($action == "submitted"){
            if ($ext != "csv" && $ext != "json"){
                $query =  app('connote')->latestStatus()->whereCourierId($this->id)->whereNotNull('courier_consignment');

                if ($this->getParams('api.enable', false)){
                    $query->whereIsExported(0);
                }else{
                    $query->whereStatus($action);
                }
                
                $connotes = $query
                    ->select('courier_consignment')->get()->pluck('courier_consignment')->toArray();
                return implode("\n",$connotes)."\n"; // added extra line pronto read error
            }
        }

        return parent::export($action, $file, $ext);
    }
}
