<?php
/**
 * Created by PhpStorm.
 * User: vey.chea
 * Date: 18/09/2017
 * Time: 4:49 PM
 */

namespace App\Modules\Pronto\Models;

use App\Modules\Supplier\Models\Supplier;
use App\Modules\SupplierOperation\Models\SupplierOperation;
use DB;
class LoadSupplierOperation{

    function __construct(){

    }

    /**
     * @param $file
     * @return string
     */
    public function import($file){
        if(!is_file($file)) {
            return 'Supplier operation file not found';
        }
        $importer = new CsvImporter($file, true);

        self::clearOperation();
        $num_row = 0;
        while($data_row = $importer->get(500)){
            foreach ($data_row as $row)
            {
                if(strtolower($row[0]) == "supplier") continue;
                $this->loadOperationRow($row);
                $num_row++;
            }
        }
        return $num_row . " have been imported";
    }

    private function loadOperationRow($row = []){

        $supplier_name = $row[0];
        $operation = [
                        'origination_zone'  => trim(strtoupper($row[1])),
                        'destination_zone'  => trim(strtoupper($row[2])),
                        'status_id'         => 1
        ];
        $supplier = Supplier::whereName($supplier_name)->first();
        if($supplier) {
            $supplier->operations()->save(new SupplierOperation($operation));
        }
    }

    private function clearOperation(){
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        SupplierOperation::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

}