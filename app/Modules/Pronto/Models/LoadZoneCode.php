<?php
/**
 * Created by PhpStorm.
 * User: vey.chea
 * Date: 28/09/2017
 * Time: 3:23 PM
 */

namespace App\Modules\Pronto\Models;

use Carbon\Carbon;
use DB;
class LoadZoneCode{

    function __construct(){

    }

    /**
    * @param $rate_file
    * @return string
    */
    public function import($file){

        if(!is_file($file)) {
            return 'CSV File is not valid';
        }

        $importer = new CsvImporter($file);
        self::clearOperation();
        $num_row = 0;
        while($data_row = $importer->get(500)){
            foreach ($data_row as $row)
            {
                if(strtolower($row[0]) == "postcode") continue;
                self::loadZoneRow($row);
                $num_row++;
            }
        }
        return $num_row . " have been imported";
    }

    private function loadZoneRow($row=[]){

            DB::table('zone_codes')->insert([
                'post_code'     => trim($row[0]),
                'zone_code'     => trim(strtoupper($row[3])),
                'state_code'    => trim(strtoupper($row[4])),
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ]);

    }
    /**
     *
     */
    private function clearOperation(){
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::statement('TRUNCATE zone_codes;');
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }


}