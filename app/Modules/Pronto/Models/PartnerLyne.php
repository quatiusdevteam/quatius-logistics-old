<?php
namespace App\Modules\Pronto\Models;

use Illuminate\Database\Eloquent\Model;

Class PartnerLyne extends Model
{
    protected $table = 'partner_lyne';
    
    protected $fillable = [
        "label_number",
        "status",
        "data",
    ];
}