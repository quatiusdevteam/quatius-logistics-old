<?php

if (config('app.debug'))
{
    Route::group(array('module' => 'Pronto', 'middleware' => ['web', 'auth:admin.web'], 'namespace' => 'App\Modules\Pronto\Controllers'), function() {
        Route::get('pronto/sync', 'ProntoController@sync');
        
        
        //Route::post('pronto/testupload', 'ProntoController@testupload');
    });
}
Route::group(['module' => 'Pronto', 'middleware' => 'api', 'namespace' => 'App\Modules\Pronto\Controllers'], function() {
    
    Route::get('pronto/{pronto_key}/key', 'ProntoController@keySync');

    Route::post('pronto/{pronto_key}/import/csv/{filename}', 'ProntoController@importCsv');
    //Route::get('pronto/{pronto_key}/export/csv/{filename}', 'ProntoController@exportCsv');
});	

Route::group(array('module' => 'Pronto', 'namespace' => 'App\Modules\Pronto\Controllers'), function() {
    
    Route::get('pronto/{pronto_key}/edi/order/{order_number}', 'ProntoController@generateOrderEdi');
    
    Route::get('pronto/load/supplierzone', 'ProntoController@loadSupplierOperation');
    Route::get('pronto/load/zonecode', 'ProntoController@loadZoneCode');
    Route::get('pronto/load/qlrate', 'ProntoController@loadQLRate');

    Route::post('pronto/POD/files', 'ProntoController@savePODFiles');
});
    
    
Route::group(array('prefix' => 'api/thirdparty', 'module' => 'Pronto', 'middleware' => ['api'], 'namespace' => 'App\Modules\Pronto\Controllers'), function() {
    Route::get('/machship/consignment', 'MachShipController@getTracking');
    Route::post('/machship/email/dispatch', 'MachShipController@dipatchNofication');
    Route::post('/machship/email/dispatches', 'MachShipController@dipatchesNofication');
    
    Route::post('/machship/test', 'MachShipController@test');
    
});
    
Route::group(array('prefix' => 'partner/api', 'module' => 'Pronto', 'middleware' => ['api'], 'namespace' => 'App\Modules\Pronto\Controllers'), function() {
    Route::post('/lyne/labelling', 'LyneController@labelling');
    Route::post('/lyne/booking', 'LyneController@booking');
    Route::post('/lyne/download_label', 'LyneController@downloadFile');
    Route::post('/lyne/tracking', 'LyneController@trackingHistory');
    Route::get('/lyne/tracking/{label}/{target}', 'LyneController@trackingHistory');
    Route::get('/lyne/test/{label_number}', 'LyneController@test');
	Route::get('/lyne/ab', 'LyneController@ABRequest');
	
});
    
Route::group(array('prefix' => 'partner/webhook', 'module' => 'Pronto', 'namespace' => 'App\Modules\Pronto\Controllers'), function() {
    Route::post('/lyne/tracking', 'LyneWebhookController@tracking');
});
                
                