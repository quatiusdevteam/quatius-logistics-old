<?php
 /**
 *	Pronto Helper  
 */
if (!function_exists('sendProntoMail'))
{
	function sendProntoMail($subject, $body, $email_to, $email_from,$attachments=[]){
		\Illuminate\Support\Facades\Mail::raw($body, function ($message) use($subject, $email_to, $email_from,$attachments){
			$message
			  ->to($email_to)
			  ->from($email_from)
			  ->attach($attachments)
			  ->subject($subject);
			  
			if (env('APP_EMAIL_BCC', ''))
				$message->bcc(env('APP_EMAIL_BCC', ''));
		});
	}
}

if (!function_exists('csv'))
{
	function csv($field, $delimiter = ',', $enclosure = '"', $mysql_null = false){
		$delimiter_esc = preg_quote($delimiter, '/');
		$enclosure_esc = preg_quote($enclosure, '/');
		
		return preg_match("/(?:${delimiter_esc}|${enclosure_esc}|\s)/", $field) 
				? ($enclosure . str_replace($enclosure, $enclosure . $enclosure, $field) . $enclosure) 
				: $field;
	}
}

if (!function_exists('calUnitDiscount'))
{
	function calUnitDiscount($unitPrice, $qty, $total, $discount=0, $gst=1.1, $invertGst=11){
		$discountPercentage=$total/($total + $discount);
		
		$subTotal = (($unitPrice*$gst) * $qty) * $discountPercentage;
		
		
		$newUnitPrice = $subTotal/$qty;	//incl GST
		return sprintf("%.4f",$newUnitPrice - ($newUnitPrice / $invertGst)); //excl GST
	}
}