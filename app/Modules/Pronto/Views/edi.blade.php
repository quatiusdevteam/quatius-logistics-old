H|E|09|ORDERS|ZZ|{{$edi->customer_number}}|ZZ|130310048|||{{$edi->order_date}}|{{$edi->order_time}}|{{$order->order_number}}|{{$order->order_number}}|{{$order->order_number}}|P|{{$edi->order_date}}|{{$edi->order_time}}||220|{{$order->order_number}}||9|||{{$edi->order_date}}|||DTS|DTS||||||{{$edi->order_date}}||||1|||||||||{{$edi->customer_note}}|||||||||||{{$order->getBillingDetail()->email}}|{{$order->getShipmentDetail()->phone_1}}|{{$edi->shipment_name}}||||||||||||||||||DTS|{{$order->getBillingDetail()->first_name}} {{$order->getBillingDetail()->last_name}}:{{$order->getBillingDetail()->phone_1}}|{{$order->getBillingDetail()->address_1}}|{{$order->getBillingDetail()->address_2}}|{{$order->getBillingDetail()->city}}|{{$order->getBillingDetail()->state}}|{{$order->getBillingDetail()->postcode}}|||||||||||||1783195|||
@foreach ($order->getProducts() as $item)
<?php
$item->barcode = "";
$edi->item_no++;
$edi->item_qty_total += $item->qty;
?>D|E|09|ORDERS|{{$edi->item_no}}||{{$item->barcode}}||{{$item->sku}}|{{$item->description}}|||{{$item->barcode}}|{{$item->qty}}|EA|{{sprintf("%0.4f",$item->price)}}|||||||||||||||||||||||||||{{($order->total - $order->discount)/$order->total}}||||||||||
@endforeach
<?php $edi->item_no++;?>D|E|09|ORDERS|{{$edi->item_no}}||||ZVOUCHER|{{$edi->coupon_code}}||||-1|EA|{{sprintf("%0.4f",$edi->coupon_discount/1.1)}}|||||||||||||||||||||||||||||||||||||
<?php $edi->item_no++;?>D|E|09|ORDERS|{{$edi->item_no}}||||ZFREIGHT|Freight||||1|EA|{{$order->shipping_cost}}|||||||||||||||||||||||||||||||||||||
<?php $edi->item_no++;?>S|E|09|ORDERS|{{$edi->item_no}}|{{$edi->item_qty_total}}|{{$order->subtotal}}|