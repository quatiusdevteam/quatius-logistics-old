<?php namespace App\Modules\Pronto\Controllers;


use App\Http\Controllers\Controller;


use App\Modules\Pronto\Models\LoadQLRate;
use App\Modules\Pronto\Models\LoadSupplierOperation;
use App\Modules\Pronto\Models\LoadZoneCode;
use Illuminate\Http\Request;
use App\Modules\Pronto\Models\CsvImporter;
use App\Modules\Order\Models\Order;
use Response;
use Lavalite\Settings\Models\Setting;
use Event;

class ProntoController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view("Pronto::index");
	}

	public function keySync(Request $request, $pronto_key="")
	{
		// setting_save('Pronto','sync_key_disabled', 'false');

		// setting_save('Pronto','sync_key', str_replace(" ","_",str_random(8)));
		return response(setting('Pronto','sync_key',config('pronto.secret'))."\r\n", 200)
				->header('Content-Type', 'text/plain');
	}

	public function importCsv(Request $request, $pronto_key, $filename=""){
		Event::dispatch("pronto.import_csv", [$filename, $request]);

		$files = array_values($request->allFiles());
		$file = isset($files[0])?$files[0]:null;

		if ($file && $file->isValid()){
			$file = requestFile($request, 'local', $filename, 'pronto/imports/');
			return response("Success\r\n", 200) ->header('Content-Type', 'text/plain');
		}

		return response("Failed\r\n", 200) ->header('Content-Type', 'text/plain');
	}

    /**
     *
     */
	public function loadSupplierOperation(){
	    $supplierOperation = new LoadSupplierOperation();
	    $supplierOperation->import(config('pronto.supplier_zone'));

     }

     public function loadZoneCode(){
	    self::setProcessingEnvironment();
	    $zoneCode = new LoadZoneCode();
	    $zoneCode->import(config('pronto.zone_code_file'));
     }

    public function loadQLRate(){
        self::setProcessingEnvironment();
        $ql_rate = new LoadQLRate();
        $ql_rate->import(config('pronto.ql_rate_file'));
    }

    /**
     * @param Request $request
     * @param $pronto_key
     * @param $order_number
     * @return \Illuminate\Http\Response|string
     */
	public function generateOrderEdi(Request $request,$pronto_key, $order_number)
	{
		if (!Setting::whereSkey('Pronto')->whereName("sync_key")->whereValue($pronto_key)->count())
			return "Invalid key\r\n";

		$order = Order::whereOrderNumber($order_number)->first();
		if (!$order)
			return "Invalid Order Number\r\n";

		$customer_accounts = config('pronto.accounts');

		$customer_number = $customer_accounts[1];
		$edi = (object) [];
		$edi->customer_number = $customer_number;
		$edi->customer_note="";
		$edi->shipment_name="";
		$edi->coupon_code="";
		$edi->coupon_discount=0;
		$edi->order_date = date ( 'Ymd', strtotime ( $order->created_at ) );
		$edi->order_time = date ( 'His', strtotime ( $order->created_at ) );
		$edi->item_no = 0;
		$edi->item_qty_total = 0;

		return Response::make(view('Pronto::'.config('pronto.order_type', 'edi'), compact('edi','order')) , 200, [
				'Content-Type' => 'text/csv',
				'Content-Disposition' => 'inline; filename="'.$order->order_number.'.csv"'
		]);
	}

	public function testupload(Request $request)
	{
		echo "<pre>";
		if ($request->hasFile('stock_file'))
		{

			$file = $request->file('stock_file');
			//$destinationPath = 'uploads'; // upload path

			echo "\n Key:".$request->get('key');
			echo "\n Filename:".$file->getClientOriginalName();
			echo "\n Size:".$file->getSize();
			echo "\n valid:".$request->file('stock_file')->isValid();
			echo "\n path: ".$file->getPathname();
			echo "\n";
			echo "--Contents--\n";
			//$csv = array_map('str_getcsv', file($file->getPathname()));
			//var_dump($csv);

			$importer = new CsvImporter($file->getPathname(),true);
			while($dataRows = $importer->get(2))
			{
				foreach ($dataRows as $row)
				{
					print_r($row);
				}
			}

			//echo file_get_contents($file->getPathname());
			//echo $file->openFile();

			//
			/* $file = $request->file('file');
			// TODO should do a validation here first

			if ($request->get('media_type', 0) == 1)
			{
			$this->repo->upload($file,'images/others',  $request->get('media_type', 0));
			}
			else
			{
			if($f = $this->repo->upload($file,'products/images',  $request->get('media_type', 0)))
			{
			$this->repo->resize($f, 120, 90);
			$this->repo->resize($f, 320, 240);
			}
			} */
			}
			return "\n\nDone \n</pre>";
	}
    /**
     *
     */
    private function setProcessingEnvironment(){
        set_time_limit(config('pronto.max_time_limit', 360)); // 1 hours
        ini_set('memory_limit', config('pronto.memory_limit', '512M'));
    }

    public function savePODFiles (Request $request) {
        $type = $request->file('data')->getClientOriginalExtension();
        $path = $request->file('data')->move('pronto/POD', $request->file('data')->getClientOriginalName());
        // fix image orientation
        if ($type == 'jpg') {
            $image = imagecreatefromjpeg($path);
            imagejpeg($image, public_path() . '/' . $path, 90);
            imagedestroy($image);
        }
    }
}
