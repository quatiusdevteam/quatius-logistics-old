<?php namespace App\Modules\Pronto\Controllers;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class MachShipController extends Controller {
    	
    function getTracking(Request $request)
    {
        $cust_ref = $request->get('ref_number', '');
        $target = $request->get('target', '');
		$temp = "Error! ";
        if(trim($cust_ref) == "")
        {
            echo "Oop, no customer reference number";
        }else
        {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://live.machship.com/apiv2/consignments/returnConsignmentsByReference1',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_2_0,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>"[$cust_ref]",
                CURLOPT_HTTPHEADER => array(
                    'token: aiDLYdeFyUKbiZdhjCVlVQ4QhlJC9EEUCmqQuOADGdpQ',
                    'Content-Type: application/json'
                ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            if($response == "")
			{
				echo "Oop! No data coming from Machship!"; 
			}
			else
			{				
				$result = json_decode($response);
				
				if($result->errors != null)
				{
					echo "Oop! No consignment is found!";
				}else{
					
					$data = $result->object[0];
					$temp = "Success! ";
					if(trim(strtolower($target) == "screen"))
					{
						$output = new \stdClass();
						$output->tracking_number = $data->carrierConsignmentId;
						$output->status = $data->status->description;
						$output->carrierName = $data->carrierName;
						echo(json_encode($output));
					}else{
						$ft = fopen("php://output", 'w');
						$file_name = $cust_ref . ".csv" ;
						$this->download_csv_header($file_name);
						$output[] = $data->carrierConsignmentId;
						$output[] = $data->status->description;
						$output[] = $data->carrierName;
						
						fputcsv($ft, ["Tracking number", "Status", "Carrier name"]);
						fputcsv($ft, [$data->carrierConsignmentId, $data->status->description, $data->carrierName]);
						fclose($ft);
					}
					
				}
			}
        }
		
		$body = $temp	. "Ref number :" . $cust_ref . ", Target: " . $target;
        sendProntoMail($temp . " Quatiuslogistics MachShip Tracking", $body, "vey.chea@soniq.com", "enquiries@quatiuslogistics.com"); 

    }
    
    function dipatchNofication(Request $request)
    {
        $reply_to = $request->get("from", "enquiries@quatiuslogistics.com");
        $email_from = "no-reply@quatiuslogistics.com";
        $email_to = $request->get("to", "");
        if(trim($email_to) == "" )
        {
            echo "No destination email provided";
            $body = "<p>Route: https://quatiuslogistics.com/api/thirdparty/machship/email/dispatch </p>";
            $body .= "<p>No destination email provided.</p>";
            $body .= json_encode($request->all());
            sendProntoMail("Error-- Quatiuslogistics MachShip Email Notification", $body, "vey.chea@soniq.com", "enquiries@quatiuslogistics.com");
        }else
        {
            if($request->hasFile("body"))
            {
                $email_attached = false;
                if($request->hasFile("attach"))
                {
                    $email_attached = true;
                    $attach_file = $request->file('attach', "");
                    $path = "tmp_upload/";
                    $upload_file = $path . "/" . $attach_file->getClientOriginalName();
                    $attach_file->move($path, $attach_file->getClientOriginalName());
                }
                /*
                 * Handling email content
                 */
                $subject = $request->get("subject", "Your order has been shipped");
                $tracking_number = $request->get("tracking_number", "");
                $carrier = $request->get("carrier", "");
                $tracking_site = $request->get("tracking_site", "");
                $customer_name = $request->get("customer_name", "");
                            
                $body_file = $request->file('body', "");
                $body = file_get_contents($body_file->getPathname());
                $body = str_replace("{TRACKING_NUMBER}", $tracking_number, $body);
                $body = str_replace("{CARRIER}", $carrier, $body);
                $body = str_replace("{WEBSITE}", $tracking_site, $body);
                $body = str_replace("{CUSTOMER_NAME}", $customer_name, $body);
                $body = str_replace("{FROM_ADDRESS}", $reply_to, $body);
                
                if($email_attached)
                {
                    sendProntoMail($subject, $body, $email_to, $email_from,$upload_file);
                    unlink($upload_file);
                }else{
                    sendProntoMail($subject, $body, $email_to, $email_from);
                }
            }else{
                echo "No body template provided";
                $body = "<p>Route: https://quatiuslogistics.com/api/thirdparty/machship/email/dispatch </p>";
                $body .= "<p>No body template provided.</p>";
                $body .= json_encode($request->all());
                sendProntoMail("Error-- Quatiuslogistics MachShip Email Notification", $body, "vey.chea@soniq.com", "enquiries@quatiuslogistics.com");
            }
        }
        
    }
    
    function download_csv_header($filename)
    {
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }
	
	function dipatchesNofication(Request $request)    
	{               
		$reply_to = $request->get("from", "enquiries@quatiuslogistics.com");        
		$email_from = "no-reply@quatiuslogistics.com";        
		$email_to = $request->get("to", "");        
		if(trim($email_to) == "" )        
		{            
			echo "No destination email provided";            
			$body = "<p>Route: https://quatiuslogistics.com/api/thirdparty/machship/email/dispatch </p>";            
			$body .= "<p>No destination email provided.</p>";            
			$body .= json_encode($request->all());            
			sendProntoMail("Error-- Quatiuslogistics MachShip Email Notification", $body, "vey.chea@soniq.com", "enquiries@quatiuslogistics.com");        
		}else        
		{            
			if($request->hasFile("body"))            
			{                
				$email_attached = false;                
				if($request->hasFile("attach"))                
				{                    
					$email_attached = true;                    
					$attach_files = $request->file('attach', "");                    
					$path = "tmp_upload/";                    
					$attachments = [];                    
					foreach ($attach_files as $attach_file)                     
					{                        
						$attachment = $path . "/" . $attach_file->getClientOriginalName();                        
						$attach_file->move($path, $attach_file->getClientOriginalName());                        
						$attachments[] = $attachment;    
					}						
				}                                                        
			                
				/** Handling email content*/                
				$subject = $request->get("subject", "Your order has been shipped");                
				$tracking_number = $request->get("tracking_number", "");                
				$carrier = $request->get("carrier", "");                
				$tracking_site = $request->get("tracking_site", "");                
				$customer_name = $request->get("customer_name", "");                                            
				$body_file = $request->file('body', "");                
				$body = file_get_contents($body_file->getPathname());                
				$body = str_replace("{TRACKING_NUMBER}", $tracking_number, $body);                
				$body = str_replace("{CARRIER}", $carrier, $body);                
				$body = str_replace("{WEBSITE}", $tracking_site, $body);                
				$body = str_replace("{CUSTOMER_NAME}", $customer_name, $body);                
				$body = str_replace("{FROM_ADDRESS}", $reply_to, $body);                                
				if($email_attached)                
				{                    
					sendProntoMail($subject, $body, $email_to, $email_from,$attachments);                    
					foreach($attachments as $delete_file)                    
					{                        
						unlink($delete_file);                    
					}                
				}else
				{                    
					sendProntoMail($subject, $body, $email_to, $email_from);                
				}            
			}else
			{                
				echo "No body template provided";                
				$body = "<p>Route: https://quatiuslogistics.com/api/thirdparty/machship/email/dispatch </p>";                
				$body .= "<p>No body template provided.</p>";                
				$body .= json_encode($request->all());                
				sendProntoMail("Error-- Quatiuslogistics MachShip Email Notification", $body, "vey.chea@soniq.com", "enquiries@quatiuslogistics.com");            
			}        
		}    
	}
	function test_direct(Request $request)
	{
		
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://live.machship.com/apiv2/consignments/returnConsignmentsByReference1',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_2_0,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS =>'["10000855"]',
		  CURLOPT_HTTPHEADER => array(
			'token: aiDLYdeFyUKbiZdhjCVlVQ4QhlJC9EEUCmqQuOADGdpQ',
			'Content-Type: application/json'
		  ),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		echo $response;
	}	
}