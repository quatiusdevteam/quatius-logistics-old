<?php namespace App\Modules\Pronto\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modules\Pronto\Models\PartnerLyne;
use Carbon\Carbon;

class LyneController extends Controller{
    
    protected $client_id = "7ohlithik1uhodc3d275ueu1o3";
    protected $client_secret = "3nf7ca595voneqja68n0116pvv88qqq056it7vf1o8qeapgg5q5";
    protected $pronto_key = "f1ce9c5649127895d8e531677e6deb413c128e2fd76e1afe";
    private $_token;
    public $_token_expire = 0;
    
    public function booking(Request $request)
    {                
        if(!$this->validateProntoRequest($request))
        {
            echo "Oop, the key you provided is not valid.";
            return;
        }
        
        $label_number = $request->get('label_number', '');
        if(trim($label_number == ""))
        {
            echo "Oop, no label number provided.";
            return;
        }
        
        $labelled = PartnerLyne::where('label_number', $label_number)->first();
        if($labelled == null)
        {
            echo "Oop, Unable to process your request. Label number is not valid. ";
            return;
        }
        
        $lyne_access_toke = $this->getToken();
        if($lyne_access_toke == "")
        {
            echo "Oop, something went wrong with access token. Please contact administrator.";
            return;
        }
                   
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://gateway.lynedirect.com/api/v1/shipment/bookings',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_2_0,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Authorization: Bearer ' . $lyne_access_toke
            ),
            CURLOPT_POSTFIELDS => '{"labelNumbers": ["' . $label_number . '"]}',
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        if($response === "")
        {
            try{
                PartnerLyne::insert([
                    "label_number"  =>  $label_number,
                    "status"        => "Booked",
                    "description"   => "successfully booked label.",
                    "created_at"    => Carbon::now("Australia/Melbourne"),
                    "updated_at"    => Carbon::now("Australia/Melbourne"),
                ]);
                echo "$label_number is successfully booked.";
                
            }catch(\Exception $e)
            {
                echo $e->getMessage();
            }
        }else
        {
            $result = json_decode($response);
            echo $result[0]->error;
        }
    }
    
    /**
     * 
     * @param Request $request
     */
    public function labelling(Request $request)
    {
       
        if(!$this->validateProntoRequest($request))
        {            
            echo "Oop, the key you provided is not valid.";
            return;
        }
        if(!$request->hasFile("shipment"))
        {
            echo "Oop, unable to process your request. Please attach the shipment file.";
            return;
        }
        $shipment = $request->file('shipment');
        $data = file_get_contents($shipment);
        
        $lyne_access_toke = $this->getToken();
        if($lyne_access_toke == "")
        {
            echo "Oop, something went wrong with access token. Please contact administrator.";
            return;
        }
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://gateway.lynedirect.com/api/v1/labels',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_2_0,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Authorization: Bearer ' . $lyne_access_toke
            ),
            CURLOPT_POSTFIELDS => "$data",
        ));
        $response = curl_exec($curl);
        curl_close($curl);
		if($response == "")
		{
			echo "Cannot get data from Lyne.";
		}else
		{
			$result = json_decode($response);
			if(isset($result->statusCode))
			{
				echo $result->message;
			}else
			{
				$label_number = $result->labelNumber;
				$label_content = $result->labelContent;
				try{
					 PartnerLyne::insert([
						"label_number"  =>  $label_number,
						"status"        => "labelled",
						"description"   => "successfully created label.", 
						"pdf_label"     => $label_content,
						"raw_result"    => $response,
						"created_at"    => Carbon::now("Australia/Melbourne"),
						"updated_at"    => Carbon::now("Australia/Melbourne"),
					]); 
					 echo "label_number: " . $label_number;
					$this->downloadLabel($label_number, $label_content);
				}catch(\Exception $e)
				{
					echo $e->getMessage();
				}
			}				
		}
		                    
    }
    
    /**
     * 
     * @param Request $request
     */
    public function trackingHistory(Request $request)
    {
        
        if(!$this->validateProntoRequest($request))
        {
            echo "Oop, the key you provided is not valid.";
            return;
        }
        
        $label_number = $request->get('label_number', '');
        if(trim($label_number == ""))
        {
            echo "Oop, no label number provided.";
            return;
        }
        
        $target = $request->get('target', 'download');
        
        $labelled = PartnerLyne::where('label_number', $label_number)->select('label_number', 'created_at', 'status', 'description')->get();
        if(strtolower($target) == "screen")
        {
            echo $labelled;
        }else
        {                        
            $file = fopen("php://output", 'w');
            $file_name = $label_number . ".csv" ;
            $this->download_header($file_name);
            fputcsv($file, ["Label number", "Created at", "Status", "Description"]);
            foreach ($labelled as $row)
            {
                fputcsv($file, [$row->label_number, $row->created_at, $row->status, $row->description]);
            }
            
            fclose($file);
        }
        
    }
    /**
     * 
     * @param Request $request
     */
    public function downloadFile(Request $request)
    {
        if(!$this->validateProntoRequest($request))
        {
            echo "Oop, the key you provided is not valid.";
            return;
        }
        
        $label_number = $request->get('label_number', '');
        if(trim($label_number == ""))
        {
            echo "Oop, no label number provided.";
            return;
        }
        $this->downloadLabel($label_number);
    }
    
    /**
     * 
     * @return unknown
     */
    private function getToken()
    {                  
        $now = strtotime("now");              
        if(setting('LyneAPI','token_expire', 0) <= $now)
        {
            $access_token = json_decode($this->getAccessToken());  
            setting_save("LyneAPI", "token", $access_token->access_token);
            setting_save("LyneAPI", "token_expire", ($access_token->expires_in + $now));                       
        } 
                
        return setting('LyneAPI','token', '');
    }
    
    /**
     * 
     * @return mixed
     */
    private function getAccessToken()
    {
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://lyne-api-prd.auth.ap-southeast-2.amazoncognito.com/oauth2/token',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_2_0,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'grant_type=client_credentials&scope=https%3A%2F%2Fgateway.lynedirect.com%2Fapi%2Faccess',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded',
                'Authorization: Basic ' . $this->getLyneClientKey()
            ),
        ));
        
        $response = curl_exec($curl);        
        curl_close($curl);              
        
        return $response;
    }
    
    /**
     * 
     * @return string
     */
    private function getLyneClientKey()
    {        
        return base64_encode($this->client_id . ":" . $this->client_secret);
    }
    
    /**
     * 
     * @param Request $request
     * @return boolean
     */
    private function validateProntoRequest(Request $request)
    {
        if($request->hasHeader("QL_PRONTO"))
        {            
            $pronto_key = $request->server->getHeaders()['QL_PRONTO'];            
            return $pronto_key === $this->pronto_key ? true : false;
        }
        
        return false;
    }
 
    private function downloadLabel($label, $content = "")
    {
        
        $filename = $label . ".pdf";
        if($content === "")
        {
            $result = PartnerLyne::where('label_number', $label)->where('status', 'labelled')->select('pdf_label')->first();
            if($result == null)
            {
                echo "Oop, Unable to process your request. Label number is not valid.";
                return;
            }
            $content = $result->pdf_label;
        }
               
        $this->download_header($filename);        
        $file = fopen("php://output", 'w');
        fwrite($file, base64_decode($content));
        fclose($file);
        
    }
    
    private function download_header($filename)
    {
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
    }
    
    public function test($label_number)
    {		$this->downloadLabel($label_number);	/*
        file_put_contents('label.pdf', base64_decode("JVBERi0xLjMKJf////8KNyAwIG9iago8PAovVHlwZSAvUGFnZQovUGFyZW50IDEgMCBSCi9NZWRpYUJveCBbMCAwIDI5Ny42NCA0MTkuNTNdCi9Db250ZW50cyA1IDAgUgovUmVzb3VyY2VzIDYgMCBSCj4+CmVuZG9iago2IDAgb2JqCjw8Ci9Qcm9jU2V0IFsvUERGIC9UZXh0IC9JbWFnZUIgL0ltYWdlQyAvSW1hZ2VJXQovWE9iamVjdCA8PAovSTEgOCAwIFIKL0kyIDExIDAgUgo+PgovRm9udCA8PAovRjIgOSAwIFIKL0YzIDEwIDAgUgo+Pgo+PgplbmRvYmoKNSAwIG9iago8PAovTGVuZ3RoIDgzMwovRmlsdGVyIC9GbGF0ZURlY29kZQo+PgpzdHJlYW0KeJy1Vs1u2zAMvvsp9AJRRYr6A4oc2m7DdhiwNbdhh8RxgAHrhq3A9vqjJNp1nDRwgiUBA4WWzI/Uxx9Qhr8L4B+CpJ1V7VPzq7FRkzPBUH1snUJntKOEySsKedPNe1APP5tPvBuOveRuJXpQgIoMaXTonVcrPvsWFRi12jVfbo0xwIIsloVYHItnCVm3VEDaUyQbVd4c5UGSQ3njWtb54GapzFe1+tC8Wc0FZxOy3iYj4OwpcCTrAeRSLbyO1iUbCkAnIL2AzP/XF4CKRkdw4I6B2rC0LNtxpMRWDwxk3S1V0hiCJSr4diLuAkw+aZM8vRqouFReWwr5nm/rxWS12U30vM+hRvImFVAAFTSgOCUy6G01ALQHmiF5pyIpcup3N8MHdMqi1c7ZTCdhIsbBhz6m3Z6ZxwZMOtMSIz5tikMC1mu0hnwqQcn+molhNO5cF/G04RLSHEqcmLp56P58a7vP7+7U/WNjNHFaMPuix8C1IEXFKgrGxYjWYSKAotrTBE/q8f4jg/nbMBgO91NDWFff2cYslmGw2qYAcUDPytENZTIApyG4UzXi3Drg4oFhOzJcjHXCxiyh1oVCb0k3iKM0xAvsu6StJR/Dof2c8iU7WsGQlooib0/xJa9hfcX8zwDJ8ZsQjgGETbVVjub7zrq2woCt4BulOXSz03weNgsauTgdDV62sdsvLYdypk0kw4whbZHbIg5UhSEghSU5+LnsSUDinpH/n3K5XtSUy6t5Kce8gOQr88NhyuVCwUTjVItQfEiVaPnCh2LSlhRkl6FWeOCOtwjcwKrfJcChqsFqny25ofAHYce2GrNBu9S3ikwT6E8Lncoa5WArdNq97ENpOYsx7/ObwfAQUvo8KzDj3l77Piz198GreffhFfj4+rTU1cKDWKsg2klvxZrbi6hDZFex+EoSNRoNJn2lyi9x9X95Ph5eNteOTxr4mnq+YjA1QkyROgjVeRPrvDkC0T4zCPSTj5qre25/XMEl5pe4lFezu17h5ukBGT1TGPSQPP1V75V3mYnLjaKMiuHM0sYTxwTOeNIryd9Ku6M+3SSh1pLLpk5/pdN09dmxToQ4cWl3fF+JgH2xMyHmP4V9mmEKZW5kc3RyZWFtCmVuZG9iagoxMyAwIG9iagooUERGS2l0KQplbmRvYmoKMTQgMCBvYmoKKFBERktpdCkKZW5kb2JqCjE1IDAgb2JqCihEOjIwMjEwNjA0MDMyNDI2WikKZW5kb2JqCjEyIDAgb2JqCjw8Ci9Qcm9kdWNlciAxMyAwIFIKL0NyZWF0b3IgMTQgMCBSCi9DcmVhdGlvbkRhdGUgMTUgMCBSCj4+CmVuZG9iagoxNyAwIG9iago8PAovVHlwZSAvRm9udERlc2NyaXB0b3IKL0ZvbnROYW1lIC9CWlpaWlorUm9ib3RvLU1lZGl1bQovRmxhZ3MgNAovRm9udEJCb3ggWy03MzIuNDIxODc1IC0yNzAuOTk2MDk0IDExNjkuOTIxODc1IDEwNTYuMTUyMzQ0XQovSXRhbGljQW5nbGUgMAovQXNjZW50IDkyNy43MzQzNzUKL0Rlc2NlbnQgLTI0NC4xNDA2MjUKL0NhcEhlaWdodCA3MTAuOTM3NQovWEhlaWdodCA1MjguMzIwMzEzCi9TdGVtViAwCi9Gb250RmlsZTIgMTYgMCBSCj4+CmVuZG9iagoxOCAwIG9iago8PAovVHlwZSAvRm9udAovU3VidHlwZSAvQ0lERm9udFR5cGUyCi9CYXNlRm9udCAvQlpaWlpaK1JvYm90by1NZWRpdW0KL0NJRFN5c3RlbUluZm8gPDwKL1JlZ2lzdHJ5IChBZG9iZSkKL09yZGVyaW5nIChJZGVudGl0eSkKL1N1cHBsZW1lbnQgMAo+PgovRm9udERlc2NyaXB0b3IgMTcgMCBSCi9XIFswIFs5MDggNjI0LjAyMzQzOCA1MzYuNjIxMDk0IDMzMi41MTk1MzEgNTU1LjY2NDA2MyAzNTEuNTYyNSA1NTYuMTUyMzQ0IDI0OS4wMjM0MzggNTY5LjMzNTkzOCA1MTYuMTEzMjgxIDU2NC40NTMxMjUgMjY1LjEzNjcxOSA2MDMuNTE1NjI1IDYwOS4zNzUgNjUzLjMyMDMxMyA1NDEuMDE1NjI1IDcwOS45NjA5MzggNjkwLjQyOTY4OCA2NjUuNTI3MzQ0IDU1NS4xNzU3ODEgMjU1LjM3MTA5NCA1NjIuOTg4MjgxIDU2OC4zNTkzNzUgNTIxLjk3MjY1NiA1NjYuODk0NTMxIDY1MS44NTU0NjkgNjA2LjkzMzU5NCA3MTAuNDQ5MjE5IDI4Mi4yMjY1NjMgNTY1LjQyOTY4OCA2NDYuOTcyNjU2IDU0OS4zMTY0MDYgNjM5LjE2MDE1NiA2NTIuODMyMDMxIDI1NS4zNzEwOTQgNDk0LjYyODkwNiA0ODYuODE2NDA2IDUyMy40Mzc1IDM1NC40OTIxODhdXQovQ0lEVG9HSURNYXAgL0lkZW50aXR5Cj4+CmVuZG9iagoxOSAwIG9iago8PAovTGVuZ3RoIDMxNAovRmlsdGVyIC9GbGF0ZURlY29kZQo+PgpzdHJlYW0KeJxdkstugzAQRfd8xSzTRURCeLQSQqrSDYs+VNpVlQXYQ2SpGMuQBX9f4+smVS3B1fj6jEczjo/1U63VTPGbHUXDM/VKS8vTeLGCqeOz0tE+IanEHCL/F0NrotjBzTLNPNS6H6ksI6L43dnTbBfaPMqx47t179VKtkqfafN5bPxOczHmmwfWM+2iqiLJvUv33JqXdmCKPbqtpfPVvGwddTvxsRimxMd7lCRGyZNpBdtWnzkqd25VZe9WFbGW/+wAdf3f0+QkySv6uoVZ4iXPvBQpJETBY4AA8h7eARGAQ4tk2MwevKTwUgFBlhR4ugd+DwFQ4IZD8DpIgZwoKQs5waXhouDliCAZkqWhThRRwCvA5cHLq9Pawd9erc1cB38dlLhY62bkX4cfzjoWpfn6gMxoVsp/P8ojqYAKZW5kc3RyZWFtCmVuZG9iago5IDAgb2JqCjw8Ci9UeXBlIC9Gb250Ci9TdWJ0eXBlIC9UeXBlMAovQmFzZUZvbnQgL0JaWlpaWitSb2JvdG8tTWVkaXVtCi9FbmNvZGluZyAvSWRlbnRpdHktSAovRGVzY2VuZGFudEZvbnRzIFsxOCAwIFJdCi9Ub1VuaWNvZGUgMTkgMCBSCj4+CmVuZG9iagoyMSAwIG9iago8PAovVHlwZSAvRm9udERlc2NyaXB0b3IKL0ZvbnROYW1lIC9DWlpaWlorUm9ib3RvLVJlZ3VsYXIKL0ZsYWdzIDQKL0ZvbnRCQm94IFstNzM2LjgxNjQwNiAtMjcwLjk5NjA5NCAxMTQ4LjQzNzUgMTA1Ni4xNTIzNDRdCi9JdGFsaWNBbmdsZSAwCi9Bc2NlbnQgOTI3LjczNDM3NQovRGVzY2VudCAtMjQ0LjE0MDYyNQovQ2FwSGVpZ2h0IDcxMC45Mzc1Ci9YSGVpZ2h0IDUyOC4zMjAzMTMKL1N0ZW1WIDAKL0ZvbnRGaWxlMiAyMCAwIFIKPj4KZW5kb2JqCjIyIDAgb2JqCjw8Ci9UeXBlIC9Gb250Ci9TdWJ0eXBlIC9DSURGb250VHlwZTIKL0Jhc2VGb250IC9DWlpaWlorUm9ib3RvLVJlZ3VsYXIKL0NJRFN5c3RlbUluZm8gPDwKL1JlZ2lzdHJ5IChBZG9iZSkKL09yZGVyaW5nIChJZGVudGl0eSkKL1N1cHBsZW1lbnQgMAo+PgovRm9udERlc2NyaXB0b3IgMjEgMCBSCi9XIFswIFs5MDggNTkzLjI2MTcxOSA1NTAuNzgxMjUgMjQyLjY3NTc4MSA1NjEuMDM1MTU2IDMyNi42NjAxNTYgMjQ3LjU1ODU5NCA2MzAuODU5Mzc1IDQ3My4xNDQ1MzEgNTM4LjA4NTkzOCA1NjMuOTY0ODQ0IDU2MS41MjM0MzggNTYxLjUyMzQzOCA1NjEuNTIzNDM4IDMzOC4zNzg5MDYgNTI5Ljc4NTE1NiA1NTEuNzU3ODEzIDE5Ni4yODkwNjMgNTYxLjUyMzQzOCA3MTIuODkwNjI1IDg4Ny4yMDcwMzEgODczLjA0Njg3NSA1NTEuNzU3ODEzIDU3MC4zMTI1IDg3Ni40NjQ4NDQgNTk2LjY3OTY4OCA1MTUuNjI1IDY4Ny41IDU0My45NDUzMTMgNTYxLjAzNTE1NiA1NjEuNTIzNDM4IDU2MS41MjM0MzggMjQyLjY3NTc4MSA0NTEuMTcxODc1IDM0Ny4xNjc5NjkgNTIzLjQzNzVdXQovQ0lEVG9HSURNYXAgL0lkZW50aXR5Cj4+CmVuZG9iagoyMyAwIG9iago8PAovTGVuZ3RoIDMwOAovRmlsdGVyIC9GbGF0ZURlY29kZQo+PgpzdHJlYW0KeJxdUstugzAQvPMVe0wPEQnm0UoIqUovHPpQaU9VDsReIkvFWIYc+PsaD02qWoLR7szswq7jQ/1UGz1R/OYG2fBEnTbK8ThcnGQ68VmbaJ+Q0nJao/CWfWuj2JubeZy4r003UFlGRPG7p8fJzbR5VMOJ75bcq1PstDnT5vPQhExzsfabezYT7aKqIsWdL/fc2pe2Z4qDdVsrz+tp3nrXTfExW6YkxHt8khwUj7aV7Fpz5qjc+VOVnT9VxEb9o1fTqfurJg+JqOjrFmYiQH4PeAhQgCtSOFblmoQklTBAIvYAFBOrMoEkAzCKwSfApUhmBSIFaGHoAEhmaFSgQ7pyaJvDLvAPAsocjbJVmQNEdVyG9TuWZW7Ljq87kRfn/DrCRQh7WDagDV/vih3s4grPDxCnpcoKZW5kc3RyZWFtCmVuZG9iagoxMCAwIG9iago8PAovVHlwZSAvRm9udAovU3VidHlwZSAvVHlwZTAKL0Jhc2VGb250IC9DWlpaWlorUm9ib3RvLVJlZ3VsYXIKL0VuY29kaW5nIC9JZGVudGl0eS1ICi9EZXNjZW5kYW50Rm9udHMgWzIyIDAgUl0KL1RvVW5pY29kZSAyMyAwIFIKPj4KZW5kb2JqCjQgMCBvYmoKPDwKPj4KZW5kb2JqCjMgMCBvYmoKPDwKL1R5cGUgL0NhdGFsb2cKL1BhZ2VzIDEgMCBSCi9OYW1lcyAyIDAgUgo+PgplbmRvYmoKMSAwIG9iago8PAovVHlwZSAvUGFnZXMKL0NvdW50IDEKL0tpZHMgWzcgMCBSXQo+PgplbmRvYmoKMiAwIG9iago8PAovRGVzdHMgPDwKICAvTmFtZXMgWwpdCj4+Cj4+CmVuZG9iagoxNiAwIG9iago8PAovTGVuZ3RoIDQxNjkKL0ZpbHRlciAvRmxhdGVEZWNvZGUKPj4Kc3RyZWFtCnicfVgLfFNFuv9mziNJk7ZJmkfpgyZNmyI0tpCmha5A0ZZHQQuU1XalUBaKPFQqUlrUQkGeFVQQ1L2rq6ugri54cqwIIsoqulcvF+oD9q5y/ekKileQhQpom3N6vzlJSlL6M+2ZmW/OmZn/fO+ZZUubGsAAbcCBeUHDnHkQ/j2FT/EC7IjQn+CTc+eSuVH6Mj7T75rT0hgmyUQsXHOXL3NF6AVYzGxc2hB9fxif6jvuXDE/THNOALtpwV3LWsJ0qgeLZ+c33nFXhH4Pv38LCDapx1pyruLb2ck3XIJBeu3t4X9brmf1cTL9bHeOctwwU38vkgag4cUAdHeqJuxY3J2jjjPM1OaJ/Q3Bpx7eJAXkfvIuBToJ/57kdFwDdw/3FW/hb+dX8QcEh3Cb8ITwsVgszhNf0eXp5uu26o7p/k+fr6/Tb9d/ZSgyVBlWGQ4l2BNmJ6zX1hgCj4ADbgEhsmISwgdaCCLSQ+EIbIU/4XMWloIMrXArzMZv6wkPLwPyB2xqPdjoH8HcOwoS1SfAyrnByJeCjd8CZkECm5gKdvEQmOntYNXxIIhfhteRTMMkGAYySaLDiJxMsHwjubQwN90M+mHwBhl5fU6aBZtv0FvKRnjsrMVN/M31bhtr8X19QuGQLEcSa4lzp431pbOWrqwoMlZ/e2XpdYNYy/DgvFtKPayV0Pz7KcVu1jJubZ4Z7jMtr59cnMlaifk5GTYTayXdVDIsy8rAyOYkHYJ1VbQuTC2X22zkXllixVQbWcbIZfLYDCRns2IVKwoysK+KFUtY8QgrXmVFLyuyMkgTG9HERjSxEU1ysguHLXExkrXOsyLLhR/PZsUjrDjGil5WjGXfFWSzEawoGIrFWCxQhGgK3BBkO4fiM4AJkmGJbDJbLNZRksksQScrRa00aGViJ0hQUSPRgvQgdY2p1QhAAsxjamWeAo6UhXClC1d6rZISTsqmcEdiuIOapaSThcPdboubsxBiIZybBIibG6LcQA8Xqz+qbxLTKcqpKqGKIkjdOwWd0kqbeyy0RZlFZ22ks1DvtgPwXyL+JHDCTFlIHcRmFsySOYKTR2h8FKcGmuFMpjz7LrkgSIiUMKzG/T/pP9XKRpIcxdUp8eag8+q7wuHEbfFY3CN4u03UEbubuEcUB4q8eW76ivqtQiFErFtWkgnqBeVdwrc9um6teokOV44J0r9ObHs/oPw5kYZWNza1UWYr1b1neSN/C9ghE9bLjsFZbFWHWdJHMOsRpj6jP2/TQM++SyvoeCftWBqt65idtiSN3tORlVbAqvNpvVjJm9NIXdCk6wMugTlojtmjVa/xXm8OOmJ3J2RDIFBUXOwfYbXY3Q6Hf0SJUxS5bFHnDni9tPY79evWfz30vxeU0canm19c+OC4f+6cd7+FHNMvtRHPxcHP9T6sfquqv1//1IbWOYu5Zza3Wu9rRbPV956lS4VxYIMZMtgduHJ4hym4qZToDhORSMQdSilmiXQynYMCiZiD/FWAUqI5qI8heSZglInFb/dYbAytXfS4LBZPwG8hu995Z/jYocOqb1bPyLIwTv2lQ9k5piThLScJ0IYOokNczb1nuSvodMyIK8Fi7cOlQyi6OFwZ/VRHI5KRSGYyAZoY1phgQiw3c0VPtjdg8VtsiAwrD2da/L70etcrwTFdpxetPPoePaiMP7qKM/R8wBz5A6gUHagPiTBBNiQl96GJ0wMjEsYoISAhMDRGc5DGSNdgFHA0MsbrydahvvqdKFEnfa3Q/2Jp1xmx5qmibG6mvk3J4EtXbElia69A+/8Z12acMMZwgsMVuDgdjBJJSCRFOaExjHGCJHGME5w5aIyzGgTgSkEkRRFEZJau5T0yXeyqbXhsYhdf2rpVbVUC9OAdc9eFVJQIhSq0jtGIKBnSoEI2pWdEfFFqrEVHwZiQMOH6QWucdgRTY0GkaIoNTqdNRMFAwGlzaMbryRarVp7a/gWxGMn9px8/rZ7rar+8acuK5s0075neTeq33496NtROhquGFw8e2r/z0EHk12TUnI8QnRcWy4a8IX38MiAQQxRVBhIZjCs0wxDxKI5OyRBviYkOM77rMCVmJNI6KcMczIl56c6h7CXnTnHTOmRjkdeb50UD9Y9w2D0acrvNwTsc6I1Q1bwBPzPZYvrF4p+r957d++75rgW3Tm2YRQY/X3Xx8LrOZWeFlrn184h3XHnRkJoXN7916A/j6yrHjBwz9rb7btv2ev1Lc26tmwQR3nMiC+mDYL7sTEtn6J1miUPeiwXXKkaU/VfVle1a4EzMzSdoFYouTjhcP7+aUmRFFWVGkodboSWaNVs02Zx576+Luk6a7tr79g9dq5p2VNy0Y3kbze0mBS10aDc0riUjLu7av44cXXMAenthTe8l8jQ/DSWT14O5EeT19nDTUGKVuKd24TDk4Y4sQ66Ll1g0MmQikalJLJNJTE4yZDLsKDJLDHYah13O1r6VMs3B3DidL/LmMGHloLD6S4v3ZOegtNgmi7ktG9RnF/ydZJ1+8seNoUfWPbBpO5lxtF79/vyf1CuPKTv/sHYdaa5fsGhc64fSqcZjK5euXjR76uKG1S827v1H09FND2xcjLvTYfw7gfFPD8PD2yK4E+KKNc8ooRmO5rUIH/YSxE2YSbrpElJFUtVUVXWSQVSlcmiBcoamck9CJMJ+gisYMV5NlKnDGdFoe+cAC0ajFfPfJhIXhRJjeeRnodRqt9so7+EsyIwSZo/byZ6/kWE7d6nH3n9h19+P06OHBemv6uEjk46pH7zMn+r59pfvp53rxl0zTF7EJMB1vxLmgxDnlN12t8VPttIdobuVjz7C0do8ghNbBpgikwRjn25o/tUVq9fXbLDPFjStEcIJj1ZhSuNhfPVjeBKcitKsKPShHt7Qc0WQerr4ROTp/N6zwmDUSTMMRp7yWS7GUwxqtghPNbvqLzdmSbZ4N5cep3eMmdk5NC9QZM1x8VanroiiRensNrQwa0AYvF0991pQ/eEJ8gRJIfNIyuNEeeOlnfvpgRdeep3KJ9SDu3eTso+nHyfjd7+ivnNcuEio2vvDjH+r3QSPQhSsyK0bkVs6SIAJv8J3jRCREBlvDDzVTKkgVhxygtbL0irM+vyoiG6Oe085slz9hXq/oXnqz8pT5IVPSbW6GzO/qTSLzuyLVuifzHBTX9w2F1xd/JqUIhbJNWGa9ItNfnJn817yO6FrzqxtE7u6uMPtapMymr7ZWLcm1CMyW2hFEI8KxzFbToSxsqjFaklkaQtcC0IzjPC6kgmT54KgmBizupstWFQ8gvnwbC9Z1dWlqhPWTpy4dgJfSgaNrKwcWTJpEq75gFrPF0biYaOcEo6HKWzKGF2JrhkXhrSNW/vrqmDiNO+sVUynUuK9c2o/nfJk50UzCaslL8wr5r74wlOH5MVdXyUs3vfB1xdXt+wYd+Pjyx6k1h71eJPSK3x+9xr1n2q3+Nzb96uulQeY9F5CFvYKr+NJo3QAM7t6iABBM4eCIBejMJQTIrZlEQK5ftp7Uf0jHZnFb73vvz/GuVEyfDHKyATVMZqZHWvCA6ZyWlqlOUW9FnEXwX1A62QxTDWITaIWfzX1sJSgnuL/yTu7uqaQYSRHvZscVL8jnUvUZ0QItc8m09TRyiaU2XT1Vt6EMhsEOZjhZ+d62YayzZKxc4DsShNTXJoXlZkTCWc4ompe1xgXjuRkC+uV7eGXTnMwM16S2dfG2RLMSdFJePNKmAgDgX4hd/q5w/sXGb9Qr3zd/M1v7m3cuWLzwlffvnBx3fLHJlQ81rKO5oaIb/VdPd8dvzx36rZ1D66a1ESuv/z8gZXk5P1vMwm3Yqw9iB7CDKNlPiabjIu1ZiTM/Z1bfHggFhuwTI2l0A4/Jvb8QfUfV7rVz0PbHyU5ghTyn1EvEOM57khoxLb/IEWclkf7UAckzT8VwFXOXuOdRHOsJ5L4zujhLuDmJbW0Rx3DP4jBJoM/dQQiMSJVi383DzBrAhIJAwaHvnBrECHiACmJt3+3plUYlYTUkNLEwkSIblBWCJLyHB5uce3nsXhGO51n/sqxO+JLngmF8NNIdJyiIZ42AOI+XEZROwQbC+LZEXeiYMzSx0kGT+v2yMN9p7xMD4QmckFlCr2VtqtlGFWPqHczDCxWZCIGEUbKoNP3aUJcthBn/30OCgjXl554MDnZQZYSh/LTJeUysZPR/L3dU6Oxmx+uxe4bf+2oP2D4YbsUrz3da5v6QtmrcE8r9bSSLlSeYBuqimRBn+FqCZACVXKCza5daVz1whp6Vz/PLxu1rfRnMRfnc5m2R24WeE9u9FqBbFWo7yeSpX6nqJ+Swg0PP7pS/ZBmKt8IkvrxiR//a1XL8i0UuYCZBP8DZhKZcKPMaTcJA2QQUdYO4O3T4oCI0JdAsAOTx8ucAiYQzFmU8D841M9+2aNeeZTbRiyvXSDe1JD1sYd3Belfnt/xuI18/r3as4sUfrjxKCl9We0+//RDF75fffbn9U8xfdiMxSr0z/GanD2wJmMsFLUrTgcO+BxPhQNqUdxhOOMaLRIiWpTiRy3yk+7TPeqmT9X1IfQWYs8HOCnOb1IncyewmQxlMmi3XnD1gB83v1WbUov3ydo1hWAOGuJ4l+ImLFYGnMgqFiiunCScOn3uuUDZ9TPr0j3qWiKSmfTLnmz1vPUt7qaqOj6ACPDExV3CSJEBN8uQOXhgBNGTVVwGxekZGjklWR8GFX+hk4so/EkICCyBIsCTSIww6ajbW/jT/PKPmr5RLxP9qb91iaf55pnL28iqPYtuaXh1PvESbtAVct3Xb9S1vLInT9od8a70hGgHG/xW1sXc5WjgsmOdS/+rq/B1TZzmIQP11xwJPAG/dsR16hCp3c7udj7Ztes/O8omikMCe06e5NZ3zO04bH1b/1p9R+g+hmkDnvmeI4BW6ZHBaOrDFE35ghCbcZERkfNyUTGxlVdWlo+bUkng5tJRkyuX3cxthnm8Ctv5yVBN/wJ6fik00054gL8XVhAVrf9hqOLdMJlfC1U0C9bwn0El/xXohFIcMwO2C3vwWQ/zhWqw8vfACmqEVn41jt8ML/E+bG+C6UIJ1l+AT3gVttMb4Xl+PY7x4fcNWGfiMwrm47jN3CVwcKfBxBfiWifAR2+ADRLkT5YMU2uChDxcu4/0rpPKM4MGbvYsn0TyXa6KheUSqfdJNF8iQ90+ict3jZe43PHTazy1rnZX+6R57a7xrgVz5kl8rlbji4b22gKXBNU1C7GcUeOWymrT+5oNtbWlPoln0/DaNO21OMGiyASLtAlwvOKThPzJLonzTq2ZViO1ladLZeW16W63q0I6NLVGOlSe7q6t9UliH0ZX+FpcQ6vLl8ShPkkfnqG6RipLl6C2vT1MedxSW3t7ejvuIEofiqf3EejfURbbgRyo2Efapmpv2jzudNbhcXvciLC23CcZ8idX11QgRDdCTMiX8it8kjFf8mFlyg/mkY2u9uqa/WXAw9x9etg4o2Y/5HNnGmvTJQ9O7tq4zwx9fWyXiflS2cZ9LvhdTdAH5en7wcedKa/1/T8IdqbVCmVuZHN0cmVhbQplbmRvYmoKMjAgMCBvYmoKPDwKL0xlbmd0aCA0MzMyCi9GaWx0ZXIgL0ZsYXRlRGVjb2RlCj4+CnN0cmVhbQp4nIVYC1xU1bpfaz/mDczADCMDyAyDMz4gkHlQZIYn6aUV+YqxUFQw8UggoNIx85WKI0qmByxfdY7nJGa5Z2cp2FW4qSW90LTSjPJU11J6nm6aMJvzrTUzOIP+7oXf/tb69uy91vf8f9/aNVULSpESLUMs0s4pnVGCAn/b4HLPgRtB/hRcafMqZoX43+G6v3xGbWWAxXlAzLMW1piDfDGQSZVVpaHf98N13+Pznpwd4Bk/QoaSOeU1tQF+EFn32dmVj5cH+V/g+bcQJo9aYy9XtGVPjxn1vyhBQX89+rPuFjKewRN+u5boP6PMUlQDq0RMYDOE5PMkDdy471qidL8yi64T/pcG1wz0AY7D47GP4eC/gGlkWtkE9h72MKfh7ude5yQ+mS/nm3lJNl62VPahPFdeKj+psCteUiYpNyrPKH9WqVXTVCJdOw01oHg0CfHBnaJBbMSkIRnww9F2sK4X1aLTqAQ9iKaiNagYTUFOZhR6C4loAzoKb+ilYqRntiIza0FqLhfpufVIywtILxuEDPifSCdrQ9Gys7Ai/AmaEQIagUSsZUZgUYeBHtDlZg1J1CLFCHQA33v7LRY9TA8whffeOpTO2Al3OYfEkxk37YFRIxLIjHenpybEkJlsftHd2YlkJt+46LFcK5kpnn684DYTmSkn5bttdBXVwuJx7mQyU68seTDwnOas6J01isyi9FqNUkZm0aOy7Uk6MovJcw5Lpu9qC8YEpEJirFoOCpjzl5QNGiveacLV4nRClhKSacI14kOEVBDSQMg+QvoISTHhBeSNBeSNBeSNBWLMYPIuIT8RkjIYnptOSAMhHxHSR8idg+HhCkIyzfBcBRBwGAQ8OxQMzYJplUiDYtB8UaPV6WJvEzRaAXUSKqNUSWlUJxJQfqHAZCb6mMGjPZRBwKDY0R6RYxC8KfKBQR4YFHTwqTS/iZrAnSg6CIzWF635LWukxaKzsDqMdZi1YBe2sEP9o5ijbulHqRVrvmFYScKM388L13bxcv8SZlGPjqn1T2Om1THTIMAK+rq5Kv4osqESEdmHwsIBCWUglCwkoQkYE0goyLSCuhOoLxoLqhGFls8Sf/OINpOMyGPT+vTX7womrZDSCdSXev1m1khss7mcaY7s+HiD1WmzpsoM+ngOGD1nTU2zuRz6eEe222ljply5gOPON15e3r77+Q07m/CfP5gldV9slHrXt7/99y1/a2TW3vPRlr0Xat5/8pmmJRWexbMX76rwnal+d+kzzz/1yQLQqx488yXEuw7dL6pi46he2szrxjcFVeOB4WODTDQw0cQTiOGDJlZFSO92mON0Vp2NSC6HiQPnLz6GH2FbcFnF1DpbSwt7oEla4ncx7y+oLH6w108SjkHrAVXmgI1ZiJA7RVlUNFkbTInDo8E0IBpgc0HVKagyfTI+TAQLbMo63dnxelmqDU9uaXk3Z96tt87L4XJxSsbo0VNHjSI71kvF3GDuQYhHE6oU4xKTyI5xWkET3FEDm2hCOyqBUYYY6nd9kGGBYYk9eA1LIk9FBwhkX1yYp1mtb1CElfSMNdVOrGN0O7JjdXbiaLmOeJYb/K/2Y/NbmpXzT7z9dcu2ut2TJr6yajujuyKdWuK/wp+rrZfOSde4A6f/6u/Z/DEBY2VfNzOFz0MGNEOMjjf2xycG0XBI6Dhg4kJMFDBRxICYZiDKhImPDZM3SutTRIgv6DqF2E4QXOcwWHUkBHMMMmuqzaWzuhw6vKajwz3GfOt9+U89fewYnydda/DPGDNG06hv9DI7G7AM7N2MENcGOKBCceghUaU3EDOprtubGjKUTVR0YlU1i8lz6kwfijRnbIQ5wZKWbI6kyBBLttvltNktuKyNSfgJx0hXrkjN2PPCrl0N0jYm13+cF347cfrb7RvXrtjGgvXipXGsAAUhBrIAUVy6ifVo1Efkg56kQDRJAZ8yTDJEAAwEinNgxmq3u4zZ7pwc7MA/9h6XHig9b83PnlaWOkx6ugPHsCN6UqRf2ahGbnzpE9wtxJPERklgIx4NCwjBwVZcyCg0B8Ao4abIGjnEYrBAks1lREj87dvhbQZN7+tmf4bEIrE9WxwUiO1BWoEFW8syr5s7pBGN9VCGK4BR0IhmNTSi6fD/RXScM9aRbdQ5dFY7xAXjMtIY0REUmN595HBFyx5lxfG3fmjZslp4eOLeui2M7Q+cuYJxXkM1ddh5VX6wcwf+5flTxAp7ID3P8gegXuSi6wa/sRQgCkBcZnjkigxL7lIQ4F1DHMzZVmkdE5vMnVy7+x1YexpY5g+K5YvR9VQIrZ0ATAIFt6gEsjbKFD9CuEjcifB8MZPMpiNcRbJDFpEspHQhrc8WJkecjVagBK2Q1AnUlxIRrw6326Ul0G51Qv4DtsvkFOHNaUF8JyFsxavXrWzsq21o8b/94aXFc2tX9iFpttTX2vj06g3bNq1js5nVVRitnf/qt+f+e7qYbhOWHv2frjervfUrl9YxBOOqoXKpQNsolIhuD+irAhVVgweAF+ShT/d/wZUW4IphXVrwcWwcBSvAdipoDqfq6v7+K67rh8tdbMszDRtWMGvXrV3FMuXSIekoFFvHFTwG3yp9LB2PuvzpJ13SZ90XTn8TqD5ME+BvFLpH5KJj+vNODTKpTeGxeEMdUmt9TJipleqAyzHF0CCmGpmm4SPfHNOyn330ZXcSu1m+1Y+43CUbaedYBFHwb9jbgJLRPFExOIW4SqEV4oNYFLFtf8CZFNSlpsz9R0wfmZginyYi8X3acPcHnlVoffHhhuRTkcvldFPMN1jiqQGNMgzWtLhsNuaB81L34i+Wf3zJb+Ve986sc8yvk85WbollBivq9Njya+pL/gbpkuR/4MVjBX8qPMl2/H1T9PqtKFTLQSMdmiSqg7X8hjSnmkRUsVDOxwATQ3TELG1UIADUEQFgHFjW8e2yp94NK+tcbt228LIOiIpRstSFnehzpECDENRQH8eHGYzUHcANuzvH5YAEWF02jXtjltR178p1L28s7YC3iwANHRC9RvRQQBktSKkdiAM+Y5gXmE6R1ZIKKGi1EbisJYVGNBi1AXCwuLCb5FggBdNoCmJs4Ry9d+OrlZO91d7nWzB79r1uaBH/wny+islaunNy1XPb609cPeP7VPpU8gTQmlcA3irReBGr1P02j4ArGkoDZY7sHBg+0MzSgSAXpm2TzsEr2v3W9nbmyyPczJ4dvNDzPPc47HsHQrIfYN9Y5LlJ6YzYUAeMLsTQ1L+Jx1Vkd1lgiKIDcTipWsYcqGNWbJcDceCvk4Zh899wwtCUf7VLO/dJp41G6cQ+6cU2/F7bq+zVXoVwlP3mWgE3pKKi53OQj1pINgFm0SFJqQEi6lmIoZVuYAmKKE4iRhy1U2BQ0SFrpJKBmqOLAwlJfOKX8Hl8/o+2WCl1k2SNg4aj5zFu17UCJocp6Snltvh3+k+AQCCbCzDgfZjGoMybyNaPNYDsvDY80wl66/S09wlAdQ6rbu85efbXtrUrFm3CsOEfJ7u73l1S37gGkOZRQGAn7TITUb6oSUoOnnviOsMK/MAyHFFoOa0vIbLQEvhABkgZEAIFCm0wlh9d8mXDF1j35IXnzks/tb5cv/6fzfVrdzP2nZJX+lCK2tFTj7N7lfvPffGO+MU5sME68E8tSJeMakWOouBNUONGMAwxFKpDbtIDow8xicAkUp8p1PS8RgcCh2EgKQzqDCBLttEdhBdnCF6cASjHufyijq8nuF9diEtkLXOXPF6nbr345l2ANrX1rz1YLK3xj2A6aqoXz/FnM8e6t/ZeAtgBvUYCcjSBb+Uh30Yc1UJ9FDmkhXeVHBHHQlpJl4VrktKPSBncbD722o987A6yKumqDoG1yDlQGXYOjDgcJAGTRDRnk5REZSVpCIFGKA7waghjk7SCpROoLy0izJy2NHIUpA1C5EmQRB+EH60fLDNfuviPhy7sO/ht6/KZpVVzsWHvxO9aVrw7v4VfV1W2FKeMmzhqUk3BqoOHN49/ovCeu8aOnvLkIxv3PfaP4qLyyaQaLuzr5pWAszqUEuoSIrqi/tSMihQbIjNpwKGGHFEZu8sZC5DKxRrstG+QG/Skb8jhlc9KV4W90u+bmOeweu8+rH72SOehN0+yH7e0vs8yzZ9I7bub8W0flp3Cf3qlWTpyhsEsNkiXf/9zj3QBx/iJrOXgg0vgg8FoKHpKNA4bTmxs1ArmYD4ZQVaj6WaY0l/GGSPFDzUdIIbhdV9MWOmOVdMCwkX0bKTnsIYra7HZKQZA8XD0FxKSmXAA0xsNQwAXXFYzOIwdqlUtf/3FDzD+fn/N/FmrW6uPLTx0mrNJ6ke2WTdKe2vME1a/sa750OQZ1SX3PNxUeGiXFP3XQu36qfd2vfPITKJzsTSF/RV0TkBpaLWYOsRGxEslnxlukq000iMSNHRSpaYJ9PVEQTHGSHNSHdH6iQb6I7FJcmTApt7Y7ecEgdCeQyHIRRr/nOt9f/Gl9uMVyuY/Ti346vbiRXvXNFW0Hb7c2rhm34TJe9bAAcCPR9TX9nx16teSRyo2bfEWLcPZ/z5wcif+cespxKJKQM5oWv0taAQqCahqAB0MobikeUcUsivpYdIO3bodF/lMYeqwJjvV1W4KJGOETw0Dv7wE2/CAY905oexjnbZUGYVZTHOQfH5hu6tnVq3q6zzlX141o7K7ve2Hpq3XmjatXLFZulS+ZlXXKi/nLN+TNfKtRf/11YW3Fh4embVn3sHPPut96S8vbLm6oYEzrampWLu2qx786wK0KqAn42h0fxikDMw/EStpyOLM8PaXfDzDJBhEhYxibZQyUBgxOakTMIPSbdHJ2dUdHa3+Mqb+mH85PhaPv2+SXsMTytlfem9jOoYSfNsGZDKcGlmoB9eB0jwgfwLfV8hnFRn9FMz1lbFbEHkrKSwcbzim8gCq7JbeeezmpiZ4i3yC6ZAZoAMvFWPCvlnQYmMO75BM4a3MDV+AIDBxmC2gwESFsZgUWkEPuO4g3ymoN41ymqwGYp2Lr7zSuifvTlWma+rMixfZVxoqXjusa1SWzaxq6J0MUj4GaNNN6+M4URtWH2kjahrQrETUEgU5bJOuU09oeI8KJ3bABkc0ICPSuYKQYbUFhNLHM6NnbVbs4xcee+ILqafm7OY3f1HsUzSUrd/6wsraqUW7S7Ado5Qdv9ede61szXtt1kMdiK1HJdwRVMDNQfX4F7SeW4Lqme1IiXugP9WgZvYEigcdmrmVaDr3NNoD1zS4qpk3UD13Byri5sJ7z6FkuFfE70fN8gR0h6wXnp+LXNyz6FE5i9Zxx9FIzoym88vQQm4qKoc9iuH5SrhcsOc2dhvimHfQKC4LPSag9HGCsqAQHLPBcxD3rRKeSfYp2enTMgScbjbnl40VcHGGwKQLeLglQ2DTzXcL7JC7JxRaPWav2Xtfidd8t3nOjBKBG0JH+KHU68k0C2hiYRnQSYUWIc+T2D8t9XhyMwSOLMPRZbweWGBucIG5dAF4358h8OnjzAJrKyh8uFBYNjZRyBvrSbRYzPlCW0Gh0DY20eLxZAiyfhnNgQ/oVFp5uiAbniEoAitMLBTyEgXk8XoDnNUiLPN6E72gQYhvi+QPYjTwRl74DbBA/kG8rID+ssxqSSQ3rBarBST0jM0QlOnjJhbmg4gWEFGVLgzNzxDU6cIwGDTpPjuuM3snFrbkIQ7NOqhAdZMKW9BQ9rtKT6JghcXNdQe1qP8e0TIqXcirO2hGUwt9w9DYxBY0jP1urCfjP8BP5foKZW5kc3RyZWFtCmVuZG9iagoyNCAwIG9iago8PAovVHlwZSAvWE9iamVjdAovU3VidHlwZSAvSW1hZ2UKL0hlaWdodCA3MQovV2lkdGggNzgKL0JpdHNQZXJDb21wb25lbnQgOAovRmlsdGVyIC9GbGF0ZURlY29kZQovQ29sb3JTcGFjZSAvRGV2aWNlR3JheQovRGVjb2RlIFswIDFdCi9MZW5ndGggMTE2Mgo+PgpzdHJlYW0KeJytWNuB4ygQxBngDOQMNBloIlhdBNJGIGdgMrAiGByBNREMF4GVgcnAZCAd/eAh793OnqX+sI2AoqmubrCEeDbZu9kUvzx+zQo7e3PlhmDzbDZBu8xsWzhXBbD5uAGaAaARPvR6sBJwbLURGrKmim3QJDJWoIf9arSWnKq3iYIhafTwVa0FQ75GITCmci0a+tQQ6PpcwKQqKLCraQsyY9CVhj79oMBus1HLrjVrwXijp5lAVxputMKArneNN2pZcyuNNnqaNwkoSXdAMLUajNJp3maflE5odoPTrw1gm5x9Q0CrNwATjsFabMlW6z7bcWc1tE4ahCg7rbI+2fZ66BcKrRZgnVsS2FLmnkHekk7cU5hoAt2pvDYu5+zEAz5Dt8VIU90Lm8CSJSNBifDwjJ0JYDFba8rgds7NiXTNIKNTrubl/paLTSfhGcqPxVRMGLN4AIvLc2gd07YWxz0XvLiK7Y6E1lHb1EXFaGVasUpzLX42GXwT7ztjIRBN8t2sCuQYHjrjXmWaiwOdzOBjslwk7XCkDgdck5MNTraqHCJLOERlNxHyQIUYXARnoabMPkaugLaiBnDo+Iyu2WJgmkVIkhCDAR5lddVDtC5GhY2Wj65lJyrJhW8T8yjjiqybqN42JUdsEmuXGJQ6BAhdJEFyhFQkHL4TGBHxI3iiEfMOHW9uzmLA6h6J/HinpTg/oXlfDjiZ1OMjtPuLl24wRXE9vjFqUSVpOSrWsioSWiv2I/HXE6m7oGsrw1kLRr1dhgVkVqcxBBJ50+8Idpckp/0xLq5FLmbodTGnxk56p3rLA5Nkyern9AM3OVNj/DOzNk4OJUzFTiWyOkxmxOIiPP+XxXooH/RgSqLDpqMdIvGhGgbPe7UEU+n+WN6neZrufCioCVqPcxHIt9m5SKlmPHibCLJqeRetVZdqcdH5liQnj9justGt6rjO1r0xZui7Pzo0c/Jfs12yA5Lvf0SXg/0h1OFnP4yWzc3fmPOD/Fa1VqqtquUau72y383/vZ0T1tuzaF+wALY/fz/29+asCX8LDmGPXma36wcYunr7uH59fZl/tUFrYEy1bV0tAsNg0+OjjY+zav7/jMHuuURf/3+EYNMyObiav72/v+8FlEKfG2UFn9jgX2XrT6kCm+Heh/X7sfwLia7dhUTNnaAShCP1GMpCQRckw3WAawyl8dOVEl1r/Dx3xxuM9aMvWCXxSPeiVaKBsmkbCTK3oR7RqbcEa8k1BOk9WkVHqp9JldhrH1ztJS+dCAZHn/4KSb57IMiIbhkAcQ5qoqHOS5hV5H9+3PzLG4YzE+FBdr4oFgUcWAhiCpjsNSb9PF/zpCBOozG1mWFVfeA8Z6EgXygGpYEteUq9dAFttuOnIDoWaDltOxVi52vwNN1qGA8bG4FCOg1h/pE0RHREg14Ta5jYawT78D+9vkoYiGKDkuOVlYmtqDFzyirXA4pBH3ZYKQ+Kqtrt1XcOdAV1g1J6CAXyZTCOYG7Tdc3bED0twB4rX18c7xFveqjVr2lke735Qnm/nle9P/oHFpdgkAplbmRzdHJlYW0KZW5kb2JqCjggMCBvYmoKPDwKL1R5cGUgL1hPYmplY3QKL1N1YnR5cGUgL0ltYWdlCi9CaXRzUGVyQ29tcG9uZW50IDgKL1dpZHRoIDc4Ci9IZWlnaHQgNzEKL0ZpbHRlciAvRmxhdGVEZWNvZGUKL0NvbG9yU3BhY2UgL0RldmljZVJHQgovU01hc2sgMjQgMCBSCi9MZW5ndGggMzkKPj4Kc3RyZWFtCnic7cExAQAAAMKg9U9tCy+gAAAAAAAAAAAAAAAAAAAAAB4GQOYAAQplbmRzdHJlYW0KZW5kb2JqCjI1IDAgb2JqCjw8Ci9UeXBlIC9YT2JqZWN0Ci9TdWJ0eXBlIC9JbWFnZQovSGVpZ2h0IDIyNgovV2lkdGggODcwCi9CaXRzUGVyQ29tcG9uZW50IDgKL0ZpbHRlciAvRmxhdGVEZWNvZGUKL0NvbG9yU3BhY2UgL0RldmljZUdyYXkKL0RlY29kZSBbMCAxXQovTGVuZ3RoIDc1NjMKPj4Kc3RyZWFtCnic7Z1/rO91XcfP5bcIIpYxLUttjiBnTFlp9kOnTVholGu0ysYELHOzGZXWMtti1TSCuSxXCEmZhEkrlZZMwjUrEJhOi5KKJBpl3UBILj8u99333HvPPYf7/j5e9/l8v9777p733q8/9Huffl4PX48n95xzz7nnXkrZnLW9s/X11qRUT9b/KyVKriR1Ts8reWzdRsj3MAaB2qMtl0xJTSAjhazzyd29IXYnfq/2FGs3j63bCPkexiBQe7TlkimpCWSkkHU+ubs3xO7E79WeYu3msXUbId/DGARqj7ZcMiU1gYwUss4nd/eG2J34vdpTrN08tm4j5HsYg0Dt0ZZLpqQmkJFC1vnk7t4QuxO/V3uKtZvH1m2EfA9jEKg92nLJlNQEMlLIOp/c3Rtid+L3ak+xdvPYuo2Q72EMArVHWy6ZkppARgpZ55O7e0PsTvxe7SnWbh5btxHyPYxBoPZoyyVTUhPISCHrfHJ3b4jdid+rPcXazWPrNkK+hzEI1B5tuWRKagIZKWSdT+7uDbE78Xu1p1i7eWzdRsj3MAaB2qMtl0xJTSAjhazzyd29IXYnfq/2FGs3j63bCPkexiBQe7TlkimpCWSkkHU+ubs3xO7E79WeYu3msXUbId/DGARqj7ZcMiU1gYwUss4nd/eG2J34vdpTrN08tm4j5HsYg0Dt0ZZLpqQmkJFC1vnk7t4QuxO/V3uKtZvH1m2EfA9jEKg92nLJlNQEMlLIOp/c3Rtid+L3ak+xdvPYuo2Q72EMArVHWy6ZkppARgpZ55O7e0PsTvxe7SnWbh5btxHyPYxBoPZoyyVTUhPISCHrfHJ3b4jdid+rPcXazWPrNkK+hzEI1B5tuWRKagIZKWSdT+7uDbE78Xu1p1i7eWzdRsj3MAaB2qMtl0xJTSAjhazzyd29IXYnfq/2FGs3j63bCPkexiBQe7TlkimpCWSkkHU+ubs3xO7E79WeYu3msXUbId/DGARqj7ZcMiU1gYwUss4nd/eG2J34vdpTrN08tm4j5HsYg0Dt0ZZLpqQmkJFC1vnk7t4QuxO/V3uKtZvH1m2EfA9jEKg92nLJlNQEMlLIOp/c3Rtid+L3ak+xdvPYuo2Q72EMArVHWy6ZkppARgpZ55O7e0PsTvxe7SnWbh5btxHyPYxBoPZoyyVTUhPISCHrfHJ3b4jdid+rPcXazWPrNkK+hzEI1B5tuWRKagIZKWSdT+7uDbE78Xu1p1i7eWzdRsj3MAaB2qMtl0xJTSAjhazzyd29IXYnfq/2FGs3j63bCPkexiBQe7TlkimpCWSkkHU+ubs3xO7E79WeYu3msXUbId/DGARqj7ZcMiU1gYwUss4nd/eG2J34vdpTrN08tm4j5HsYg0Dt0ZZLpqQmkJFC1vnk7t4QuxO/V3uKtZvH1m2EfA9jEKg92nLJlNQEMlLIOp/c3Rtid+L3ak+xdvPYuo2Q72EMArVHWy6ZkppARgpZ55O7e0PsTvxe7SnWbh5btxHyPYxBoPZoyyVTUhPISCHrfHJ3b4jdid+rPcXazWPrNkK+hzEI1B5tuWRKagIZKWSdT+7uDbE78Xu1p1i7eWzdRsj3MAaB2qMtl0xJTSAjhazzyd29IXYnfq/2FGs3j63bCPkexiBQe7TlkimpCWSkkHU+ubs3xO7E79WeYu3msXUbId/DGARqj7ZcMiU1gYwUss4nd/eG2J34vdpTrN08tm4j5HsYg0Dt0ZZLpqQmkJFC1vnk7t4QuxO/V3uKtZvH1m2EfA9jEKg92nLJlNQEMlLIOp/c3Rtid+L3ak+xdvPYuo2Q72EMArVHWy6ZkppARgpZ55O7e0PsTvxe7SnWbh5btxHyPYxBoPZoyyVTUhPISCHrfHJ3b4jdid+rPcXazWPrNkK+hzEI1B5tuWRKagIZKWSdT+7uDbE78Xu1p1i7eWzdRsj3MAaB2qMtl0xJTSAjhazzyd29IXYnfq/2FGs3j63bCPkexiBQe7TlkimpCWSkkHU+ubs3xO7E79WeYu3msXUbId/DGARqj7ZcMiU1gYwUss4nd/eG2J34vdpTrN08tm4j5HsYg0Dt0ZZLpqQmkJFC1vnk7t4QuxO/V3uKtZvH1m2EfA9jEKg92nLJlNQEMlLIOp/c3Rtid+L3ak+xdvPYuo2Q72EMArVHWy6ZkppARgpZ55O7e0PsTvxe7SnWbh5btxHyPYxBoPZoyyVTUhPISCHrfHJ3b4jdid+rPcXazWPrNkK+hzEI1B5tuWRKagIZKWSdT+7uDbE78Xu1p1i7eWzdRsj3MAaB2qMtl0xJTSAjhazzyd29IXYnfq/2FGs3j63bCPkexiBQe7TlkimpCWSkkHU+ubs3xO7E79WeYu3msXUbId/DGARqj7ZcMiU1gYwUss4nd/eG2J34vdpTrN08tm4j5HsYg0Dt0ZZLpqQmkJFC1vnk7t4QuxO/V3uKtZvH1m2EfA9jEKg92nLJlNQEMlLIOp/c3Rtid+L3ak+xdvPYuo2Q72EMArVHWy6ZkppARgpZ55O7e0PsTvxe7SnWbh5btxHyPYxBoPZoyyVTUhPISCHrfHJ3b4jdid+rPcXazWPrNkK+hzEI1B5tuWRKagIZKWSdT+7uDbE78Xu1p1i7eWzdRsj3MAaB2qMtl0xJTSAjhazzyd29IXYnfq/2FGs3j63bCPkexiBQe7TlkimpCWSkkHU+ubs3xO7E79WeYu3msXUbId/DGARqj7ZcMiU1gYwUss4nd/eG2J34vdpTrN08tm4j5HsYg0Dt0ZZLpqQmkJFC1vnk7t4QuxO/V3uKtZvH1m2EfA9jEKg92nLJlNQEMlLIOp/c3Rtid+L3ak+xdvPYuo2Q72EMArVHWy6ZkppARgpZ55O7e0PsTvxe7SnWbh5btxHyPYxBoPZoyyVTUhPISCHrfHJ3b4jdid+rPcXazWPrNkK+hzEI1B5tuWRKagIZKWSdT+7uDbE78Xu1p1i7eWzdRsj3MAaB2qMtl0xJTSAjhazzyd29IXYnfq/2FGs3j63bCPkexiBQe7TlkimpCWSkkHU+ubs3xO7E79WeYu3msXUbId/DGARqj7ZcMiU1gYwUss4nd/eG2J34vdpTrN08tm4j5HsYg0Dt0ZZLpqQmkJFC1vnk7t4QuxO/V3uKtZvH1m2EfA9jEKg92nLJlNQEMlLIOp/c3Rtid+L3ak+xdvPYuo2Q72EMArVHWy6ZkppARgpZ55O7e0PsTvxe7SnWbh5btxHyPYxBoPZoyyVTUhPISCHrfHJ3b4jdid+rPcXazWPrNkK+hzEI1B5tuWRKagIZKWSdT+7uDbE78Xu1p1i7eWzdRsj3MAaB2qMtl0xJTSAjhazzyd29IXYnfq/2FGs3j63bCPkexiBQe7TlkimpCWSkkHU+ubs3xO7E79WeYu3msXUbId/DGARqj7ZcMiU1gYwUss4nd/eG2J34vdpTrN08tm4j5HsYg0Dt0ZZLpqQmkJFC1vnk7t4QuxO/V3uKtZvH1m2EfA9jEKg92nLJlNQEMlLIOp/c3Rtid+L3ak+xdvPYuo2Q72EMArVHWy6ZkppARgpZ55O7e0PsTvxe7SnWbh5btxHyPYxBoPZoyyVTUhPISCHrfHJ3b4jdid+rPcXazWPrNkK+hzEI1B5tuWRKagIZKWSdT+7uDbE78Xu1p1i7eWzdRsj3MAaB2qMtl0xJTSAjhazzyd29IXYnfq/2FGs3j63bCPkexiBQe7TlkimpCWSkkHU+ubs3xO7E79WeYu3msXUbId/DGARqj7ZcMiU1gYwUss4nd/eG2J34vdpTrN08tm4j5HsYg0Dt0ZZLpqQmkJFC1vnk7t4QuxO/V3uKtZvH1m2EfA9jEKg92nLJlNQEMlLIOp/c3Rtid+L3ak+xdvPYuo2Q72EMArVHWy6ZkppARgpZ55O7e0PsTvxe7SnWbh5btxHyPYxBoPZoyyVTUhPISCHrfHJ3b4jdid+rPcXazWPrNkK+hzEI1B5tuWRKagIZKWSdT+7uDbE78Xu1p1i7eWzdRsj3MAaB2qMtl0xJTSAjhazzyd29IXYnfq/2FGs3j63bCPkexiBQe7TlkimpCWSkkHU+ubs3xO7E79WeYu3msXUbId/DGARqj7ZcMiU1gYwUss4nd/eG2J34vdpTrN08tm4j5HsYg0Dt0ZZLpqQmkJFC1vnk7t4QuxO/V3uKtZvH1m2EfA9jEKg92nLJlNQEMlLIOp/c3Rtid+L3ak+xdvPYuo2Q72EMArVHWy6ZkppARgpZ55O7e0PsTvxe7SnWbh5btxHyPYxBoPZoyyVTUhPISCHrfHJ3b4jdid+rPcXazWPrNkK+hzEI1B5tuWRKagIZKWSdT+7uDbE78Xu1p1i7eWzdRsj3MAaB2qMtl0xJTSAjhazzyd29IXYnfq/2FGs3j63bCPkexiBQe7TlkimpCWSkkHU+ubs3xO7E79WeYu3msXUbId/DGARqj7ZcMiU1gYwUss4nd/eG2J34vdpTrN08tm4j5HsYg0Dt0ZZLpqQmkJFC1vnk7t4QuxO/V3uKtZvH1m2EfA9jEKg92nLJlNQEMlLIOp/c3Rtid+L3ak+xdvPYuo2Q72EMArVHWy6ZkppARgpZ55O7e0PsTvxe7SnWbh5btxHyPYxBoPZoyyVTUhPISCHrfHJ3b4jdid+rPcXazWPrNkK+hzEI1B5tuWRKagIZKWSdT+7uDbE78Xu1p1i7eWzdRsj3MAaB2qMtl0xJTSAjhazzyd29IXYnfq/2FGs3j63bCPkexiBQe7TlkimpCWSkkHU+ubs3xO7E79WeYu3msXUbId/DGARqj7ZcMiU1gYwUss4nd/eG2J34vdpTrN08tm4j5HsYg0Dt0ZZLpqQmkJFC1vnk7t4QuxO/V3uKtZvH1m2EfA9jEKg92nLJlNQEMlLIOp/c3Rtid+L3ak+xdvPYuo2Q72EMArVHWy6ZkppARgpZ55O7e0PsTvxe7SnWbh5btxHyPYxBoPZoyyVTUhPISCHrfHJ3b4jdid+rPcXazWPrNkK+hzEI1B5tuWRKagIZKWSdT+7uDbE78Xu1p1i7eWzdRsj3MAaB2qMtl0xJTSAjhazzyd29IXYnfq/2FGs3j63bCPkexiBQe7TlkimpCWSkkHU+ubs3xO7E79WeYu3msXUbId/DGARqj7ZcMiU1gYwUss4nd/eG2J34vdpTrN08tm4j5HsYg0Dt0ZZLpqQmkJFC1vnk7t4QuxO/V3uKtZvH1m2EfA9jEKg92nLJlNQEMlLIOp/c3Rtid+L3ak+xdvPYuo2Q72EMArVHWy6ZkppARgpZ55O7e0PsTvxe7SnWbh5btxHyPYxBoPZoyyVTUhPISCHrfHJ3b4jdid+rPcXazWPrNkK+hzEI1B5tuWRKagIZKWSdT+7uDbE78Xu1p1i7eWzdRsj3MAaB2qMtl0xJTSAjhazzyd29IXYnfq/2FGs3j63bCPkexiBQe7TlkimpCWSkkHU+ubs3xO7E79WeYu3msXUbId/DGARqj7ZcMiU1gYwUss4nd/eG2J34vdpTrN08tm4j5HsYg0Dt0ZZLpqQmkJFC1vnk7t4QuxO/V3uKtZvH1m2EfA9jEKg92nLJlNQEMlLIOp/c3Rtid+L3ak+xdvPYuo2Q72EMArVHWy6ZkppARgpZ55O7e0PsTvxe7SnWbh5btxHyPYxBoPZoyyVTUhPISCHrfHJ3b4jdid+rPcXazWPrNkK+hzEI1B5tuWRKagIZKWSdT+7uDbE78Xu1p1i7eWzdRsj3MAaB2qMtl0xJTSAjhazzyd29IXYnfq/2FGs3j63bCPkexiBQe7TlkimpCWSkkHU+ubs3xO7E79WeYu3msXUbId/DGARqj7ZcMiU1gYwUss4nd/eG2J34vdpTrN08tm4j5HsYg0Dt0ZZLpqQmkJFC1vnk7t4QuxO/V3uKtZvH1m2EfA9jEKg92nLJlNQEMlLIOp/c3Rtid+L3ak+xdvPYuo2Q72EMArVHWy6ZkppARgpZ55O7e0PsTvxe7SnWbh5btxHyPYxBoPZoyyVTUhPISCHrfHJ3b4jdid+rPcXazWPrNkK+hzEI1B5tuWRKagIZKWSdT+7uDbE78Xu1p1i7eWzdRsj3MAaB2qMtl0xJTSAjhazzyd29IXYnfq/2FGs3j63bCPkexiBQe7TlkimpCWSkkHU+ubs3xO7E79WeYu3msXUbId/DGARqj7ZcMiU1gYwUss4nd/eG2J34vdpTrN08tm4j5HsYg0Dt0ZZLpqQmkJFC1vnk7t4QuxO/V3uKtZvH1m2EfA9jEKg92nLJlNQEMlLIOp/c3Rtid+L3ak+xdvPYuo2Q72EMArVHWy6ZkppARgpZ55O7e0PsTvxe7SnWbh5btxHyPYxBoPZoyyVTUhPISCHrfHJ3b4jdid+rPcXazWPrNkK+hzEI1B5tuWRKagIZKWSdT+7uDbE78Xu1p1i7eWzdRsj3MAaB2qMtl0xJTSAjhazzyd29IXYnfq/2FGs3j63bCPkexiBQe7TlkimpCWSkkHU+ubs3xO7E79WeYu3msXUbId/DGARqj7ZcMiU1gYwUss4nd/eG2J34vdpTrN08tm4j5HsYg0Dt0ZZLpqQmkJFC1vnk7t4QuxO/V3uKtZvH1m2EfA9jEKg92nLJlNQEMlLIOp/c3Rtid+L3ak+xdvPYuo2Q72EMArVHWy6ZkppARgpZ55O7e0PsTvxe7SnWbh5btxHyPYxBoPZoyyVTUhPISCHrfHJ3b4jdid+rPcXazWPrNkK+hzEI1B5tuWRKagIZKWSdT+7uDbE78Xu1p1i7eWzdRsj3MAaB2qMtl0xJTSAjhazzyd29IXYnfq/2FGs3j63bCPkexiBQe7TlkimpCWSkkHU+ubs3xO7E79WeYu3msXUbId/DGARqj7ZcMiU1gYwUss4nd/eG2J34vdpTrN08tm4j5HsYg0Dt0ZZLpqQmkJFC1vnk7t4QuxO/V3uKtZvH1m2EfA9jEKg92nLJlNQEMlLIOp/c3Rtid+L3ak+xdvPYuo2Q72EMArVHWy6ZkppARgpZ55O7e0PsTvxe7SnWbh5btxHyPYxBoPZoyyVTUhPISCHrfHJ3b4jdid+rPcXazWPrNkK+hzEI1B5tuWRKagIZKWSdT+7uDbE78Xu1p1i7eWzdRsj3MAaB2qMtl0xJTSAjhazzyd29IXYnfq/2FGs3j63bCPkexiBQe7TlkimpCWSkkHU+ubs3xO7E79WeYu3msXUbId/DGARqj7ZcMiU1gYwUss4nd/eG2J34vdpTrN08tm4j5HsYg0Dt0ZZLpqQmkJFC1vnk7t4QuxO/V3uKtZvH1m2EfA9jEKg92nLJlNQEMlLIOp/c3Rtid+L3ak+xdvPYuo2Q72EMArVHWy6ZkppARgpZ55O7e0PsTvxe7SnWbh5btxHyPYxBoPZoyyVTUhPISCHrfHJ3b4jdid+rPcXazWPrNkK+hzEI1B5tuWRKagIZKWSdT+7uDbE78Xu1p1i7eWzdRsj3MAaB2qMtl0xJTSAjhazzyd29IXYnfq/2FGs3j63bCPkexiBQe7TlkimpCWSkkHU+ubs3xO7E79WeYu3msXUbId/DGARqj7ZcMiU1gYwUss4nd/eG2J34vdpTrN08tm4j5HsYg0Dt0ZZLpqQmkJFC1vnkHpvOmTNnzpw5c+bMmTNnzpw5c+bMmTNnzpw5c+bMmTNnzpw5c+bMmTNnzpw5c+bMmTNnzpw5c+bMmTNnzpw5c+bMmTNnzpw5c+bMmTNnzpw5c+bMmTNnzpw5c+bMmTNnzpw5c+bMmTNnzpw5c+bMmXPYzct//2/+664bfuPZbelZt961OX/+jDBdW3v95zfCL7w9Jix/Nn/v6tP1efJVN37D5o/ImAin/9rH//uLH7nk62runO0xJ9+0/99s8+ivN6U3PuFfnvPLYbp2xIOb4Z6nNTybv3fV6d45+i9KedvmD8EYCDve/NC+9P6LdhwMnrMt5tj1f7CP3/OlPYv/+qWW9Lz/2LV/HlnE7wzTtZMWbzD78wcviwnLn83fu+J07+x4fykPffPmj8F4OWHHtetvYf/+P+v/+Ttrc7bj/FQpu9584tra1162+En9TS3pxhxzfSlfOS1On1XK7XyL8mz+3tWme+eSUnafe2jj5YSLFj941ymLPq5cvDgLy5tz+M5Rd5Vyzr6Xv7r5rtJJN+bIxbvcR151MP+g9Pml3IS3KM/m711tunfOW3xwetOhjZcTnvNAKT+87+VbS7nnycs4cw7veWkpf7n/5ZPuLzt3+On+2XFFKY/9wMH4g9NvL+UjdIr0bP7e1abr88KvlFJ9prbEeDnhF0q5Zn965G2lfNcy0JzDe95Syus3Xn+wlFP9dP/85uIzih+r8AenZ5fyATpFejZ/72rTxZz4z6Vct/TLFgcZLyd8aMtb1tvhY+Kcw3uuLuWFG69/tpQf9dN9847F5ww/WdGr9LzFW85XL79EezZ/72rTxby/lDueohgvJyzeRp+6kb62lPcuI805vOevSnn6xusfKeXn/HTvLD5vL2+t4HX6hvUvlH359g+dW71vF5/N37vadG3th0p54LRjl3wsq4yXE+4o5Ws20h8s5bdq0JzDfRb/DI/YeP2aUt7lp+vzssWn97uvfdURa0+YJemFG79HdPMr2p7N37vadO3YexdvZTv3PHDzVd99KOPlhD8q5cAXFhefo/3E2pxtN18qjx14fVYpV/jp+lyw783hzjOfwF6SPvWaz/3dp/f+zs/j39v0bP7e1abrX4ffP3vee1JsvJxwcSm3Hr3v5fG3lfIda3O23ewsjx54/YpSrvbT9Tn+HTfe/fjip8zD529lL08Xc9Kr7yxl57Nbns3fu9r0iH9cf1Pa9ZkvPLb4r1uODI2XE57yr6Vct/ebsI7+8OLxk9fmbLu5f8s/2Zcf+Jqxk27McW96oJRHTl9T0rXj/riU66tQeDZ/72rTcxZvGPeeu3jrOvanHyrlLaEx9PvS3YtPT6+/7H237VqwPrY2Z/vNfWX3gdeL95/X+unmfP3flvLJ6v9heXrKA+W++isCh342f+9q019ZfFb2rH0vzy7l/w58HWOZMfX72v/c/J7Hbz0YMGcbzD2lHPhlzOL97h/46ZZ55eKnwdPE9BOlLPnW9UM+m793tek1W77a+NFSzo6Msd+vumLj7Wx+KNuWs/i04YSN168r5Xf9dMsccVcp3ymmv1fKa+pzDvls/t7VpreXcsZGenEpF0fGUb9Pf9n3XLCn7JkfyrblfHrLz4K3lfKLfrp1fruU+vs/lqc3lvJtS+451LP5e1eb7ix7jt9Iv+8JX5etjeN+n3F3KZfX63O2wVy55Wf1NQe+LdVJt84nl77pLEtPerQ8ePSSew71bP7e1aafKeXAH9I8v5RLI+Ow3+NvKeVzx9Xrc7bBXFjKn+1/edS9pXyjn26Zkx8pXz5KSz9QyseXnHPIZ/P3rjZd/GL3wG/2vWfZx+ktxlG/63/I7OFvqbbnbIs5tZTdz9v38oJS7m5IN+eYT5TyUSk9/vLFz5kln2Mc+tn8vatN31DKZ4/d9/KMh0t5fmQc9XtJKeVnDl6es13mT0u5Z+/7yJf875ZPzo309He+8cxj1l+84q9Lue+0MN03z3nj4jP+8uMHfuw8m793tenxd5Ry/XPXX52z+LzqD2Pj5dz1ed2eUq466BvY5myfOfXRxXvQz15x6Q27SvmHYxvSW9e/f+Hmq2/4t/W/ruKVa2G69pL3fPimf1n/kvRDW/6+HOfZ/L0rTl/0SCm7PnXltX+/EPnixjfXg/FywmJevPgw+MGt3zgyZ5vN99+78Tue//TMlvTdm79l+vnNPy+/PD1m575o9/u2/nVMzrP5e1edvvqejfRTB76tZbkxEdbWPlbKdfUnvHO20Zx8+e3rf8fLXRef0JQed+EtD6//vLj/5vO3vLddnq5/sXrXHde/e8tfNGM+m7935emJl965e/EB+faLNn/FB8ZEWPv5x//kmINrmLPd5ugXvOCERHrkc8988SnVg8vSo170zKV/ath5Nn/vytMnnfG8g3+9t7wzIpy05MHR5v8BWs49xwplbmRzdHJlYW0KZW5kb2JqCjExIDAgb2JqCjw8Ci9UeXBlIC9YT2JqZWN0Ci9TdWJ0eXBlIC9JbWFnZQovQml0c1BlckNvbXBvbmVudCA4Ci9XaWR0aCA4NzAKL0hlaWdodCAyMjYKL0ZpbHRlciAvRmxhdGVEZWNvZGUKL0NvbG9yU3BhY2UgL0RldmljZVJHQgovU01hc2sgMjUgMCBSCi9MZW5ndGggNTk0Cj4+CnN0cmVhbQp4nO3BMQEAAADCoPVPbQlPoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADgYwCrAAEKZW5kc3RyZWFtCmVuZG9iagp4cmVmCjAgMjYKMDAwMDAwMDAwMCA2NTUzNSBmIAowMDAwMDA0MjM2IDAwMDAwIG4gCjAwMDAwMDQyOTMgMDAwMDAgbiAKMDAwMDAwNDE3NCAwMDAwMCBuIAowMDAwMDA0MTUzIDAwMDAwIG4gCjAwMDAwMDAyNjEgMDAwMDAgbiAKMDAwMDAwMDEyNSAwMDAwMCBuIAowMDAwMDAwMDE1IDAwMDAwIG4gCjAwMDAwMTQzMzUgMDAwMDAgbiAKMDAwMDAwMjYxNSAwMDAwMCBuIAowMDAwMDA0MDA0IDAwMDAwIG4gCjAwMDAwMjIzMDMgMDAwMDAgbiAKMDAwMDAwMTI1MiAwMDAwMCBuIAowMDAwMDAxMTY2IDAwMDAwIG4gCjAwMDAwMDExOTEgMDAwMDAgbiAKMDAwMDAwMTIxNiAwMDAwMCBuIAowMDAwMDA0MzQwIDAwMDAwIG4gCjAwMDAwMDEzMjggMDAwMDAgbiAKMDAwMDAwMTU5NSAwMDAwMCBuIAowMDAwMDAyMjI4IDAwMDAwIG4gCjAwMDAwMDg1ODMgMDAwMDAgbiAKMDAwMDAwMjc2MiAwMDAwMCBuIAowMDAwMDAzMDI4IDAwMDAwIG4gCjAwMDAwMDM2MjMgMDAwMDAgbiAKMDAwMDAxMjk4OSAwMDAwMCBuIAowMDAwMDE0NTU0IDAwMDAwIG4gCnRyYWlsZXIKPDwKL1NpemUgMjYKL1Jvb3QgMyAwIFIKL0luZm8gMTIgMCBSCi9JRCBbPGVlMzM1N2EwNzg4MDIzZWQ5NWQ4N2M1NjZkNGRmN2JlPiA8ZWUzMzU3YTA3ODgwMjNlZDk1ZDg3YzU2NmQ0ZGY3YmU+XQo+PgpzdGFydHhyZWYKMjMwODEKJSVFT0YK"));
        die();
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://lyne-api-prd.auth.ap-southeast-2.amazoncognito.com/oauth2/token',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => 'grant_type=client_credentials&scope=https%3A%2F%2Fgateway.lynedirect.com%2Fapi%2Faccess',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded',
                'Authorization: Basic N29obGl0aGlrMXVob2RjM2QyNzV1ZXUxbzM6M25mN2NhNTk1dm9uZXFqYTY4bjAxMTZwdnY4OHFxcTA1Nml0N3ZmMW84cWVhcGdnNXE1',
                'Cookie: XSRF-TOKEN=cd4b2ec4-8c8b-40d3-aa9f-21b5c74814cb'
            ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        echo $response;	*/
    }	
	
	/**
     * 
     * @param Request $request
     */
	 
    public function ABRequest(Request $request)
    {
                        
        $label_number = $request->get('label_number', '00033535000000228895');
        if(trim($label_number == ""))
        {
            echo "Oop, no label number provided.";
            return;
        }
        
        $target = $request->get('target', 'download');
        
        $labelled = PartnerLyne::where('label_number', $label_number)->where('is_imported', 0)->select('label_number', 'status', 'description', 'created_at')->get();
		echo json_encode($labelled);
		PartnerLyne::where('label_number', $label_number)->update(["is_imported" =>1]);		
        
    }
	
}
